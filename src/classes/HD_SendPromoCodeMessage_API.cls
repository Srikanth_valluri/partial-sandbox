/**********************************************************************************************************************
Description: This API is used for sending promotional email notification for different use cases from 'DAMAC Living' App.
========================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   05-10-2020      | Subin C Antony      | Created. Initial Draft.
***********************************************************************************************************************/

@RestResource(urlMapping='/hd/sendPromoCodeMessage')
global class HD_SendPromoCodeMessage_API {
    public static Map<Integer, String> statusCodeMap;
    static {
        statusCodeMap = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Account ID is not correct',
        6 => 'Something went wrong'
        };
    }
    
    public static final String FNB_TMPL_HEAD = '' + 
     '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">' + 
     '<html xmlns="http://www.w3.org/1999/xhtml">' + 
     '<head>' + 
     '	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' + 
     '	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>' + 
     '	<title>Thank you for downloading the DAMAC Living app!</title>' + 
     '	<link rel="stylesheet" href="https://use.typekit.net/uqm2kot.css">' + 
     '	<!--[if !mso]><!-- -->' + 
     '    <!--<![endif]-->' + 
     '	<style type="text/css">' + 
     '		/*font start*/' + 
     '		/*font end*/' + 
     '		.ExternalClass {width:100%;}' + 
     '		#outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */' + 
     '		body{width:100% !important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;padding:0;color:#ffffff;}' + 
     '		/* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */' + 
     '		.ReadMsgBody, .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */' + 
     '		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;} /* Force Hotmail to display normal line spacing. */' + 
     '		table td{border-collapse:collapse;mso-table-lspace:-1pt;mso-table-rspace:-1pt;}' + 
     '		table tr{border-collapse:collapse;mso-table-lspace:-1pt;mso-table-rspace:-1pt;}' + 
     '		p.MsoNormal{margin:0px}' + 
     '		#backgroundTable{margin:0;padding:0;width:100% !important;line-height:100% !important;}' + 
     '		img{outline:none;text-decoration:none;border:none;-ms-interpolation-mode:bicubic;}' + 
     '		a img{border:none;}' + 
     '		.image_fix{display:block;}' + 
     '		p{margin:0px 0px !important;}' + 
     '		table td{border-collapse:collapse;}' + 
     '		table{border-collapse:collapse;mso-table-lspace:-1pt;mso-table-rspace:-1pt;border:0px !important;}' + 
     '		a{color:#fff;text-decoration:none!important;}' + 
     '		.damac-logo{max-width:170px !important;}' + 
     '		.width50{height:75px !important;}' + 
     '		' + 
     '		/*IPAD STYLES*/' + 
     '		@media screen and (max-width:600px), screen and (max-device-width:600px) {' + 
     '			a[href^="tel"], a[href^="sms"]{text-decoration:none;color:#373737;pointer-events:none;cursor:default;}' + 
     '			.devicewidth{width:100% !important;}' + 
     '			.contentwidth{width:90% !important;}' + 
     '			.xsHeight40{height:40px !important;}' + 
     '			.xsHeight30{height:30px !important;}' + 
     '			.xsHeight20{height:20px !important;}' + 
     '			.xsHeight15{height:15px !important;}' + 
     '			.xsHeight10{height:10px !important;}' + 
     '			.img-responsive{width:100% !important;}' + 
     '		}' + 
     '		@media screen and (max-width:480px), screen and (max-device-width:480px) {' + 
     '			.text-16{font-size:16px !important;line-height:28px !important;}' + 
     '			.text-14{font-size:14px !important;line-height:20px !important;}' + 
     '			.text-12{font-size:12px !important;line-height:17px !important;}' + 
     '			.text-10{font-size:10px !important;line-height:15px !important;}' + 
     '			.text-9{font-size:9px !important;text-align:center !important;line-height:13px !important;}' + 
     '			.text-8{font-size:8px !important;line-height:11px !important;}' + 
     '			.brunch-logos{width:150px !important;margin:0 10px !important;}' + 
     '		}' + 
     '	</style>' + 
     '</head>';
     
     public static final String FNB_TMPL_BODY = '' + 
     '<body class="gmail">' + 
     '	<table width="100%" bgcolor="#f9f9f9" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="banner">' + 
     '		<tbody>' + 
     '			<tr>' + 
     '				<td>' + 
     '					<table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" bgcolor="#ffffff" style="background-color:#ffffff;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;">' + 
     '						<tbody>	' + 
     '							<tr>' + 
     '								<td width="100%" bgcolor="#ffffff" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;">' + 
     '									<table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" bgcolor="#000000">' + 
     '										<tbody>' + 
     '											<tr>' + 
     '												<td align="center" valign="middle" style="color:#333333;font-family:\'Montserrat\', Tahoma, Verdana, sans-serif;mso-line-height-rule:exactly;border:0px;padding:0px;margin:0px;"><img src="http://media.damacgroup.com/EDM/damac/2020/October/images/congratulations-edm-banner.jpg" border="0" alt="CONGRATULATIONS!" align="abstop" style="border:none;outline:none;padding:0px;line-height:40px;font-size:1px;margin:0 auto;max-width:600px;display:block;" class="img-responsive" /></td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td align="center" height="30" class="xsHeight15" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;"></td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td align="center" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;">' + 
     '													<table align="center" width="520" border="1" cellspacing="0" cellpadding="10" class="contentwidth">' + 
     '														<tbody>' + 
     '															<tr>' + 
     '																<td align="center" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;font-family:\'proxima-nova\', Tahoma, Verdana, sans-serif;font-size:16px;font-weight:600;line-height:28px;color:#ffffff;text-align:center;" class="text-10">Thank you for downloading the DAMAC Living app!</td>' + 
     '															</tr>' + 
     '															<tr>' + 
     '																<td align="center" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;font-family:\'proxima-nova\', Tahoma, Verdana, sans-serif;font-size:16px;font-weight:normal;line-height:28px;color:#ffffff;text-align:center;" class="text-10">Your complimentary breakfast at the Stage restaurant is ready!</td>' + 
     '															</tr>' + 
     '															<tr>' + 
     '																<td align="center" height="20" class="xsHeight10" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;"></td>' + 
     '															</tr>' + 
     '															<tr>' + 
     '																<td align="center" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;font-family:\'proxima-nova\', Tahoma, Verdana, sans-serif;font-size:14px;font-weight:normal;line-height:28px;color:#ffffff;text-align:center;" class="text-10">Your voucher code</td>' + 
     '															</tr>' + 
     '															<tr>' + 
     '																<td align="center" height="10" class="xsHeight10" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;"></td>' + 
     '															</tr>' + 
     '															<tr>' + 
     '																<td align="center" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;font-family:\'proxima-nova\', Tahoma, Verdana, sans-serif;text-align:center;" class="text-12"><span style="text-transform:uppercase;color:#ba9b65;background-color:#303030;font-size:24px;font-weight:normal;line-height:40px;display:inline-block;" class="text-16">&nbsp;&nbsp;&nbsp;{0}&nbsp;&nbsp;&nbsp;</span></td>' + 
     '															</tr>' +  
     '														</tbody>' + 
     '													</table>' + 
     '												</td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td align="center" height="40" class="xsHeight30" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;"></td>' + 
     '											</tr>' + 
     '										</tbody>' + 
     '									</table>' + 
     '									<table width="540" cellpadding="0" cellspacing="0" border="0" align="center" class="contentwidth">' + 
     '										<tbody>	' + 
     '											<tr>' + 
     '												<td align="center" height="50" class="xsHeight30" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;"></td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td align="center" valign="middle" style="color:#333333;font-family:\'Montserrat\', Tahoma, Verdana, sans-serif;mso-line-height-rule:exactly;border:0px;padding:0px;margin:0px;"><img src="http://media.damacgroup.com/EDM/damac/2020/October/images/paramount-hotel-logo.png" border="0" alt="Paramount Hotel Dubai" align="abstop" style="border:none;outline:none;padding:0px;line-height:40px;font-size:1px;margin:0 auto;max-width:600px;display:block;" /></td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td align="center" height="30" class="xsHeight20" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;"></td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td align="center" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;font-family:\'proxima-nova\', Tahoma, Verdana, sans-serif;font-size:24px;font-weight:600;line-height:28px;color:#000000;text-align:center;" class="text-14">OTHER REWARDS YOU CAN EXPECT</td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td align="center" height="30" class="xsHeight20" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;"></td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td align="center" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;">' + 
     '													<table width="500" border="0" cellspacing="0" cellpadding="0" style="color:#000000;font-family:\'proxima-nova\', Tahoma, Verdana, sans-serif;text-transform:uppercase;" class="devicewidth">' + 
     '														<tbody>' + 
     '															<tr>' + 
     '																<td align="center">' + 
     '																	<h2 style="margin:0;padding:0;line-height:1.4;" class="text-14">BUY 1 GET 1</h2>' + 
     '																	<h3 style="margin:0;padding:0;line-height:1.4;" class="text-12">BRUNCH OFFERS</h3>' + 
     '																	<p class="text-10">Friday or Saturday at</p><br />' + 
     '																	<img src="http://media.damacgroup.com/EDM/damac/2020/October/images/brunch-logos.png" border="0" alt="Pacific Groove and Malibu Deck" align="abstop" style="border:none;outline:none;padding:0px;line-height:40px;font-size:1px;margin:0 auto;max-width:600px;display:block;" class="brunch-logos" />' + 
     '																</td>' + 
     '																<td width="1" bgcolor="#cccccc"></td>' + 
     '																<td align="center">' + 
     '																	<h2 style="margin:0;padding:0;line-height:1.4;" class="text-14">20% OFF</h2>' + 
     '																	<h3 style="margin:0;padding:0;line-height:1.4;" class="text-12">ACROSS DINING & SPA</h3>' + 
     '																</td>' + 
     '															</tr>' + 
     '														</tbody>' + 
     '													</table>' + 
     '												</td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td align="center" height="50" class="xsHeight30" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;"></td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td align="center" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;font-family:\'proxima-nova\', Tahoma, Verdana, sans-serif;font-size:16px;font-weight:normal;line-height:26px;color:#000000;text-align:center;" class="text-10">To avail this offer, present your voucher code at the venue</td>' + 
     '											</tr>' + 
     '											<!-- <tr>' + 
     '												<td align="center" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;font-family:\'proxima-nova\', Tahoma, Verdana, sans-serif;font-size:14px;font-weight:normal;line-height:26px;color:#acacac;text-align:center;" class="text-10"><a href="tel:+97142466641" style="color:#000000 !important;">+971 4 246 6641</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="mailto:restaurants@paramounthotelsdubai.com" style="color:#000000 !important;">restaurants@paramounthotelsdubai.com</a></td>' + 
     '											</tr> -->' + 
     '											<tr>' + 
     '												<td align="center" height="30" class="xsHeight15" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;"></td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td align="center" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;font-family:\'proxima-nova\', Tahoma, Verdana, sans-serif;font-size:11px;font-weight:300;line-height:16px;color:#000000;" class="text-8">*Terms & conditions apply. Booking in advance is recommended. Offer not valid in conjunction with any other discount or promotion. Breakfast offer is for one-time consumption only.</td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td align="center" height="30" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;"></td>' + 
     '											</tr>' + 
     '										</tbody>' + 
     '									</table>' + 
     '								</td>' + 
     '							</tr>' + 
     '						</tbody>' + 
     '					</table>' + 
     '				</td>' + 
     '			</tr>' + 
     '		</tbody>' + 
     '	</table>' + 
     '	<table width="100%" bgcolor="#f0f0f0" cellpadding="0" cellspacing="0" border="0" id="backgroundTable2" st-sortable="banner">' + 
     '		<tbody>' + 
     '			<tr>' + 
     '				<td>' + 
     '					<table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" bgcolor="#f0f0f0" style="background-color:#f0f0f0;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;">' + 
     '						<tbody>	' + 
     '							<tr>' + 
     '								<td width="100%" bgcolor="#f0f0f0" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;">' + 
     '									<table width="540" cellpadding="0" cellspacing="0" border="0" align="center" class="contentwidth">' + 
     '										<tbody>' + 
     '											<tr>' + 
     '												<td align="center" height="40" class="xsHeight30" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;"></td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td width="100%" align="center" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;">' + 
     '													<a href="https://www.damacproperties.com/" target="_blank" style="border:0;outline:none;"><img src="http://media.damacgroup.com/EDM/damac/2020/February/images/d-logo.png" border="0" style="border:none;outline:none;text-decoration:none;padding:0px;line-height:1px;font-size:1px;margin:0 auto;" alt="DAMAC" /></a>' + 
     '												</td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td align="center" height="20" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;"></td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td width="100%" align="center" bgcolor="#f0f0f0" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;font-family:\'proxima-nova\', Tahoma, Verdana, sans-serif;font-size:14px;font-weight:200;color:#181818;line-height:24px;" class="text-10">' + 
     '													<a style="color:#000000;font-weight:500;">DAMACPROPERTIES.COM</a>' + 
     '												</td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td align="center" height="10" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;"></td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td align="center" valign="middle" bgcolor="#f0f0f0" style="font-size:10px;line-height:10px; color:#333333; font-family:\'Montserrat\', Tahoma, Verdana, sans-serif; mso-line-height-rule: exactly; border:0px; padding:0px; margin:0px;">' + 
     '													<table cellspacing="0" cellpadding="0" border="0" width="100%" style="width:100% !important;">' + 
     '														<tr>' + 
     '															<td align="left" valign="top" width="100%" height="1" style="background-color:#8c8c8c; border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; mso-line-height-rule: exactly; line-height: 1px;"><!--[if gte mso 15]>&nbsp;<![endif]--></td>' + 
     '														</tr>' + 
     '													</table>' + 
     '												</td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td align="center" height="20" class="xsHeight10" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;"></td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td width="100%" align="center" bgcolor="#f0f0f0" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;font-family:\'proxima-nova\', Tahoma, Verdana, sans-serif;font-size:13px;font-weight:200;color:#000000;line-height:22px;" class="text-10">© Copyright 2020. DAMAC Properties. <span style="display:inline-block;">All rights reserved.</span>' + 
     '												</td>' + 
     '											</tr>' + 
     '											<tr>' + 
     '												<td align="center" height="30" class="xsHeight15" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;border-spacing:0;mso-table-lspace:-1pt;mso-table-rspace:-1pt;"></td>' + 
     '											</tr>' + 
     '										</tbody>' + 
     '									</table>' + 
     '								</td>' + 
     '							</tr>' + 
     '						</tbody>' + 
     '					</table>' + 
     '				</td>' + 
     '			</tr>' + 
     '		</tbody>' + 
     '	</table>' + 
     '</body>' + 
     '</html>';
    
    @HTTPPost
    global static void doPost(String account_id, String promo_code, String send_to_list, String cc_list, String bcc_list){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        String errorMsg, exceptionMsg, successMsg;
        
        if(String.isBlank(account_id) || String.isBlank(promo_code)){
            errorMsg = 'Please provide `account_id` and `promo_code`.';
            getErrorResponse(3, statusCodeMap.get(3), errorMsg, exceptionMsg);
            return;
        }
        
        Account party;
        try{
            party = [SELECT id, name, IsPersonAccount, Party_ID__c, Party_Type__c, Country__c, 
                     Email__c, Mobile_Country_Code__c, Mobile_Phone_Encrypt__c, Country__pc, 
                     Email__pc, Mobile_Country_Code__pc, Mobile_Phone_Encrypt__pc 
                     FROM Account WHERE id = :account_id LIMIT 1];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            system.debug(ex.getStackTraceString());
            exceptionMsg = ex.getStackTraceString();
            party = NULL;
        }
        if(NULL == party){
            errorMsg = 'The Account ID provided does not match any Account records.';
            getErrorResponse(2, statusCodeMap.get(2), errorMsg, exceptionMsg);
            return;
        }
        
        String fromAddress = '';
        String fromName = '';
        OrgWideEmailAddress orgWideAddressHelloDamac;
        try{
            orgWideAddressHelloDamac = [SELECT  id, Address, DisplayName 
                                        FROM OrgWideEmailAddress 
                                        WHERE id = :Label.CommunityPortalOrgWideAddressId LIMIT 1];
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            system.debug(ex.getStackTraceString());
            exceptionMsg = ex.getStackTraceString();
            orgWideAddressHelloDamac = NULL;
        }
        
        if(NULL == orgWideAddressHelloDamac){
            errorMsg = 'ERROR: could not get `from_address`.';
            getErrorResponse(6, statusCodeMap.get(6), errorMsg, exceptionMsg);
            return;
        }
        else {
            fromAddress = orgWideAddressHelloDamac.Address;
            fromName = orgWideAddressHelloDamac.DisplayName;
        }
        
        send_to_list = (NULL == send_to_list) ? '' : send_to_list.trim();
        cc_list = (NULL == cc_list) ? '' : cc_list.trim();
        bcc_list = (NULL == bcc_list) ? '' : bcc_list.trim();
        String party_email = party.IsPersonAccount ? party.Email__pc : party.Email__c;
        if(NULL != party_email && !(party_email.trim().equals(''))){
            send_to_list = (party_email.trim() + ',' + send_to_list);
            // send_to_list = 'v-subin@damacgroup.com,' + send_to_list;
        }
        
        if(String.isBlank(party_email) && String.isBlank(send_to_list)){
            errorMsg = 'The Account ID provided does not have an email address. Send to list is empty.';
            getErrorResponse(2, statusCodeMap.get(2), errorMsg, exceptionMsg);
            return;
        }
        
        String toAddress = send_to_list; /* comma seperated list of email addresses */
        String ccAddress = cc_list; /* comma seperated list of email addresses */
        String bccAddress = bcc_list; /* comma seperated list of email addresses */
        String toName, ccName, bccName, replyToName; /* not required */
        String contentType = 'text/html'; /* 'text/plain' */
        
        String subject = 'Greetings from DAMAC: Complementary food coupon for you';
        String contentValue = getMergedMessageContent(party.name, promo_code); /* mergedMessageContent */
        
        String replyToAddress = ''; /* not required */
        String sendGridTemplateId = ''; /* not required */
        String substitutions = ''; /* not required */
        List<Attachment> attachments = new List<Attachment>();
        
        SendGridEmailService.SendGridResponse sendEmailResponse;
        try{
            sendEmailResponse = SendGridEmailService.sendEmailService(
            toAddress, toName, ccAddress, ccName, bccAddress, bccName, 
            subject, substitutions, fromAddress, fromName, replyToAddress, replyToName, 
            contentType, contentValue, sendGridTemplateId, attachments);
            system.debug(sendEmailResponse);
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            system.debug(ex.getStackTraceString());
            exceptionMsg = ex.getStackTraceString();
            errorMsg = 'ERROR: Failed in SendGrid service to send email.';
            getErrorResponse(2, statusCodeMap.get(2), errorMsg, exceptionMsg);
            return;
        }
        
        /* create log for email sent event: insert EmailMessage */
        createEmailSentLog(party, fromAddress, toAddress, ccAddress, bccAddress, 
                           subject, contentValue, sendEmailResponse.messageId);
        
        String sendGridResponseStatusText;
        if(Test.isRunningTest()){
            sendGridResponseStatusText = 
                (toAddress.containsIgnoreCase('success@mailinator')) ? 'Accepted' : 'Rejected';
        }
        else {
            sendGridResponseStatusText = sendEmailResponse.ResponseStatus;
        }
                                  
        ResponseWrapper responseWrapper = new ResponseWrapper();
        cls_meta_data responseMetaData = new cls_meta_data();
        
        if(sendGridResponseStatusText.equalsIgnoreCase('Accepted')){
            responseMetaData.status_code = 1;
            responseMetaData.title = statusCodeMap.get(1);
            responseMetaData.message = 'Success in sending notification by Email';
            responseMetaData.developer_message = null;
        }
        else {
            responseMetaData.status_code = 2;
            responseMetaData.title = statusCodeMap.get(2);
            responseMetaData.message = 'Failed in SendGrid while sending notification by Email';
            responseMetaData.developer_message = null;
        }
        responseWrapper.meta_data = responseMetaData;
        
        cls_data responseData = new cls_data();
        responseData.status = Test.isRunningTest() ? 'Accepted' : sendEmailResponse.ResponseStatus;
        responseData.message_id = Test.isRunningTest() ? 'NA' : sendEmailResponse.messageId;
        responseWrapper.data = responseData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }
    
    private static String getMergedMessageContent(String clientName, String promoCode) {
        List<String> formatArgs = new List<String>();
        formatArgs.add(promoCode);
        String msgBody = String.format(FNB_TMPL_BODY, formatArgs);
        String mergedMessge = FNB_TMPL_HEAD + msgBody;
        
        return mergedMessge;
    }
    
    /*
    private static String getMergedMessageContent_BAK(String clientName, String promoCode) {
        String messageTemplate = '<p>Dear ' + clientName + ', </p>' + 
            '<p>We thank you for supporting DAMAC in our endevours. ' + 
            '<br />As a token of our gratitude, please accept this complementary F&B coupon. ' + 
            'Use this coupon code at any food and beverages outlet to redeem a discount of 50% to a maximum limit of 200 AED. </p>' + 
            '<p><b>' + promoCode + '</b></p>';
        
        List<String> footerTemplArgs = new List<String>();
        footerTemplArgs.add(''); // 0 : HD_NotificationEmailTemplateUtil.GENERAL_FOOT_NOTE 
        footerTemplArgs.add(HD_NotificationEmailTemplateUtil.WHATSAPP_DAMAC); // 1 
        footerTemplArgs.add('DAMAC Customer Service Team'); // 2 
        String msgTempl_footer = String.format(HD_NotificationEmailTemplateUtil.GENERAL_FOOTER, footerTemplArgs);
        
        String mergedMessge = HD_NotificationEmailTemplateUtil.GENERAL_HEADER + messageTemplate + msgTempl_footer;
        
        return mergedMessge;
    } */
    
    private static void createEmailSentLog (Account party, String fromAddress, String toAddress, 
    String ccAddress, String bccAddress, String subject, String messageBody, String responsMsgId){
        EmailMessage emailLog = new EmailMessage();
        emailLog.Subject = subject;
        emailLog.MessageDate = System.Today();
        emailLog.Status = '3';
        emailLog.RelatedToId = party.Id; /* no Case here */
        emailLog.ToAddress = toAddress;
        emailLog.FromAddress = fromAddress;
        emailLog.TextBody = messageBody;
        emailLog.CcAddress = ccAddress;
        emailLog.BccAddress = bccAddress;
        emailLog.Sent_By_Sendgrid__c = true;
        emailLog.SentGrid_MessageId__c = responsMsgId;
        emailLog.Booking_Unit__c = null;
        emailLog.Account__c = party.id;
        
        try{
            insert emailLog;
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
            system.debug(ex.getStackTraceString());
        }
    }
    
    private static void getErrorResponse(Integer statusCode, String title, 
    String responseMessage, String devMessage) {
        ResponseWrapper responseWrapper = new ResponseWrapper();
        cls_meta_data responseMetaData = new cls_meta_data();
        responseMetaData.status_code = statusCode;
        responseMetaData.title = title;
        responseMetaData.message = responseMessage;
        responseMetaData.developer_message = devMessage;
        responseWrapper.meta_data = responseMetaData;
        
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(responseWrapper, false));
    }
    
    /* Wrapper classes for returning reponse */
    public class ResponseWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_data {
        public String status;
        public String message_id;
    }

    public class cls_meta_data {
        public Integer status_code;
        public String message;
        public String title;
        public String developer_message;   
    }
}