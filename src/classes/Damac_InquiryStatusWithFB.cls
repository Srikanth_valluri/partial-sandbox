global class Damac_InquiryStatusWithFB implements Database.Batchable<sObject>, Database.allowsCallouts {

    global Database.QueryLocator start(Database.BatchableContext bc) {
        Set<ID> inqIds = new Set <ID> ();
        for (Inquiry__history his : [SELECT ParentID FROM Inquiry__history 
                                        WHERE Field = 'Inquiry_Status__c' 
                                            AND CreatedDate = TODAY
                                    ]) 
        {
            inqIds.add (his.parentId);
        }
        
        String query = 'SELECT Name, Inquiry_Status__c, Email__c, Mobile_Phone__c, Facebook__c FROM Inquiry__c WHERE Inquiry_Status__c != NULL AND ID IN : inqIds AND Facebook__c != NULL';
        if (Test.isRunningTest ()) {
            query = 'SELECT Name, Inquiry_Status__c, Email__c, Mobile_phone__c, Facebook__c FROM Inquiry__c LIMIT 1';
        }
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<Inquiry__c> scope){
        Map <Id, Inquiry__c> inqMap = new Map <id, Inquiry__c> ();
        
        for (Inquiry__c inq :scope) {
            inqMap.put (inq.Id, inq);
        }
        
        Damac_Facebook_API__mdt details = new Damac_Facebook_API__mdt();
        
        Map <Id, Inquiry__history> inqHisMap = new Map <ID, Inquiry__History> ();
        Map <String, String> inquiryStatusesMap = new Map <String, String> ();
        for (Damac_Facebook_API__mdt detail : [SELECT Label, DeveloperName, FB_Lead_Status__c, Access_Token__c, API_URL__c, Offline_Event_Set_ID__c 
                                                    FROM Damac_Facebook_API__mdt 
                                                    WHERE DeveloperName != NULL]) {
            if (detail.developerName == 'Status_API')
                details = detail;
                
            inquiryStatusesMap.put (detail.Label, detail.FB_Lead_Status__c);
        }
        for (Inquiry__history his : [SELECT ParentID, NewValue, 
                                            OldValue, Field, CreatedDate 
                                        FROM Inquiry__history 
                                        WHERE Field = 'Inquiry_Status__c'
                                        AND ParentId IN: inqMap.keySet() 
                                        Order By CreatedDate DESC
                                    ]) 
        {
            if (!inqHisMap.containsKey (his.ParentId)) {
                inqHisMap.put (his.ParentId, his);
                System.debug (his.newvalue);
                System.debug (inquiryStatusesMap.get (inqMap.get (his.ParentId).Inquiry_status__c));
                sendRequest (inquiryStatusesMap, details, inqMap.get (his.ParentId), Integer.valueOf(String.valueOf(his.createdDate.getTime()).subString (0, 10)));
                break;
            }
        }
    }
    
    global void finish(Database.BatchableContext bc){
    
    }
    
    public void sendRequest (Map <String, String> inquiryStatusesMap, Damac_Facebook_API__mdt  details, Inquiry__c inq, Integer timeStamp) {
        String body = createReqBody(inquiryStatusesMap, inq, timeStamp);
        
        System.debug(body);
        
        HTTPRequest req = new HTTPRequest ();
        HTTP http = new HTTP ();
        HTTPResponse res = new HTTPResponse ();
        
        req.setMethod ('POST');
        req.setEndpoint(details.API_URL__c+'/v5.0/'+details.Offline_Event_Set_ID__c+'/events?access_token='+details.Access_Token__c
                        +'&upload_tag=store_data&data='+EncodingUtil.urlEncode(body, 'UTF-8'));
        
        if (!Test.isRunningTest())
            res = http.send (req);
        System.debug(res.getBody());
    }
    
    public String createReqBody (Map <String, String> inquiryStatusesMap, Inquiry__c inq, Long statusChangeTime) {
        
        Damac_FBJSON obj = new Damac_FBJSON();
        
        Damac_FBJSON.cls_match_keys matchKeys = new Damac_FBJSON.cls_match_keys ();
        matchKeys.lead_id = inq.FaceBook__c != NULL ? inq.FaceBook__c : '';
        
        List<Damac_FBJSON.cls_email> emails = new List <Damac_FBJSON.cls_email> ();
        List<Damac_FBJSON.cls_phone> phones = new List <Damac_FBJSON.cls_phone> ();
        
        Damac_FBJSON.cls_email email = new Damac_FBJSON.cls_email();
        email.email_key = inq.Email__c != NULL ? sha256Convertion (inq.Email__c) : '';
        emails.add (email);
        matchKeys.email = emails;
        
        Damac_FBJSON.cls_phone phone = new Damac_FBJSON.cls_phone();
        phone.phone_key = inq.Mobile_Phone__c != NULL ? sha256Convertion (inq.Mobile_Phone__c) : '';
        phones.add (phone);
        matchKeys.phone = phones;
        
        Damac_FBJSON.cls_Keys keys = new Damac_FBJSON.cls_Keys();
        keys.event_name = 'Other';
        keys.event_time = statusChangeTime;
        
        Damac_FBJSON.cls_custom_data cusData = new Damac_FBJSON.cls_custom_data ();
        cusData.lead_event_source = 'Salesforce';
        cusData.facebook_lifecycle_stage_name = inquiryStatusesMap.containsKey (inq.Inquiry_status__c) ? inquiryStatusesMap.get (inq.Inquiry_status__c) : 'LEAD';
        cusData.lifecycle_stage_name = inq.Inquiry_status__c;
        keys.custom_data = cusData;
        keys.match_keys = matchKeys;
        
        obj.keys = keys;
        String data = JSON.serialize (obj);
        
        data = data.replace ('{"phone_key":', '');
        data = data.replace ('{"email_key":', '');
        data = data.replace ('"}', '"');
        
        data = data.replace ('{"keys":', '[');
        data = data.removeEnd ('}');
        data = data+'}]';
        
        System.debug (data);
        
        return data;
    }
    
    public String sha256Convertion (String data) {
        Blob targetBlob = Blob.valueOf(data);
        Blob hashm = Crypto.generateDigest('SHA256', targetBlob);    
        return EncodingUtil.convertToHex(hashm);
    }
}