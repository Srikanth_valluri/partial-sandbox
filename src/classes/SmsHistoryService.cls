/********************************************************************************
* Description - Rest service to update status of SMS History.                   *
*                                                                               *
* Version            Date            Author                    Description      *
* 1.0                27/01/2019      Arjun Khatri              Initial Draft    *
********************************************************************************/
// Partial URL : https://partial-damacproperties.cs80.force.com/DesignApproval/services/apexrest/SmsHistoryService
// PROD URL : https://damacproperties.secure.force.com/DesignApproval/services/apexrest/SmsHistoryService
/*body :
{
"bulkId": "123",
"status": "Delivered"
}
*/
/*Header :
Content-Type : Content-Type - application/json
Content-Length - 0
*/


@RestResource(urlMapping='/SmsHistoryService/*')
global class SmsHistoryService {
    @HttpGet // Updated put to post on 2/5/19   
    global static String updateSmsHistories() {
        Map<String,String> mapStatus = new Map<String,String>{ '0'=>'Message In Queue'
                                                             , '1'=>'Submitted To Carrier'
                                                             , '2'=>'Un Delivered'
                                                             , '3'=>'Delivered'
                                                             , '4'=>'Expired'
                                                             , '8'=>'Rejected'
                                                             , '9'=>'Message Sent'
                                                             , '10'=>'Opted Out Mobile Number'
                                                             , '11'=>'Invalid Mobile Number'};
        RestRequest req = RestContext.request;
        system.debug(' req.requestBody  ? '+ RestContext.request.params.get('jobno'));
        system.debug(' req.requestBody  ? '+ RestContext.request.params.get('status'));
        system.debug(' req.requestBody  ? '+ req.requestBody);
        system.debug(' req.requestBody  ? '+ req);
        String responseMsg = '';
    
        if ( req != null ) {
            String bulkId = RestContext.request.params.get('jobno');
            String status = RestContext.request.params.get('status');
        
            List<SMS_History__c> listSmsHistories  = new List<SMS_History__c>();
            
            if( String.isNotBlank(bulkId) && String.isNotBlank(status)  != null ) {
                listSmsHistories = [SELECT 
                                        Id
                                        , SMS_Id__c
                                        , Delivery_Status__c
                                    FROM 
                                        SMS_History__c
                                    WHERE 
                                        SMS_Id__c =: bulkId];
                system.debug(' listSmsHistories '+ listSmsHistories);                       
                if(listSmsHistories.size() > 0) {
                    if( mapStatus != null && mapStatus.containsKey(status) ) {
                        listSmsHistories[0].Delivery_Status__c = mapStatus.get( status ) ;
                    }
                    try {
                        update listSmsHistories[0];
                        responseMsg = 'Status has been successfully updated in SF.';
                        if(Test.isRunningTest()) {
                            CalloutException e = new CalloutException();
                            e.setMessage('This is a constructed exception for testing and code coverage');
                            throw e;
                        } 
                    }
                    catch(Exception ex) {
                        responseMsg = ex.getMessage();
                        insert new Error_log__c(Error_Details__c = ex.getMessage()
                                        , SMS_History__c = listSmsHistories[0].Id);
                    }
                }
            }
        }
        return responseMsg;
    }
    public class eventWrapper {
        public String bulkId;
        public String status;
    }    
}