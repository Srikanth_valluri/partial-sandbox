public with sharing class RefreshFMInvDueController {
    
    public String callingListId                 {get;set;}
    public List<Calling_List__c> callingList    {get;set;}
    public Id callingListRecordTypeId               {get;set;}

    public RefreshFMInvDueController(ApexPages.StandardController controller){
    }
    
    public pagereference fetchData() {
        callingListId = ApexPages.currentPage().getParameters().get('id');
        callingListRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collections').RecordTypeId;
        
        if( String.isNotBlank( callingListId ) ) {
          callingList = [SELECT Id,
                            Name,
                            Booking_Unit__c,
                            DM_Due_Amount__c,
                            Inv_Due__c,
                            Amount_Paid__c,Total_Charges__c ,
                            Amount_Pending__c,
                            Construction_Status__c,
                            DUE_0_30_DAYS__c,
                            DUE_30_60_DAYS__c,
                            DUE_60_90_DAYS__c,
                            DUE_90_180_DAYS__c,
                            DUE_180_360_DAYS__c,
                            DUE_MORE_THAN_360_DAYS__c,
                            NOT_DUE__c,
                            Status__c,Invoice_Original_Due__c,
                            Amount_Received__c,
                            Registration_ID__c,
                            Booking_Unit__r.Id
                       FROM Calling_List__c
                       WHERE Id = :callingListId AND RecordTypeId = :callingListRecordTypeId LIMIT 1];
        }
        
        if( String.isNotBlank( callingList[0].Registration_ID__c ) ) {
            FmIpmsRestServices.DueInvoicesResult dues = FMCallingListCreationBatch.fetchFmDues(callingList[0].Registration_ID__c , callingList[0].Booking_Unit__r.Id );
            if( dues.totalDueAmount >= Label.Due_Amount_For_Creation_of_FM_CL  ) {
                if( dues.lstDueInvoice != null && dues.lstDueInvoice.size() > 0 ) {
                    callingList[0] = FMCallingListCreationBatch.populateInvDues( callingList[0] , dues.lstDueInvoice,false);
                    update callingList[0];
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.info,'Success: Please refresh after 5 min!!'));
                    return null;                  
                }
            }
        }
        return null;
    }
    
    public pagereference returnBack() {
        return new Pagereference(String.format(Label.Lightning_Calling_Detail_Page_Url, new List<String>{callingListId}));
    }
    
}