/**********************************************************************************************************************
Description: This is a utility class with CONSTANTS being used in 'HD_SendNotificationByEmail_API' 
			 for sending email notification for different use cases from 'DAMAC Living' App.
========================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   11-08-2020      | Subin C Antony      | Created
1.1     |   01-10-2020      | Subin C Antony	  | Modified template for 'AD-SCH-SET-ALL' and 'AD-SCH-MOD-ALL'.
1.2     |   27-10-2020      | Subin C Antony      | Added template for 'Advanced Payment' Notification.
***********************************************************************************************************************/

global class HD_NotificationEmailTemplateUtil {
    public static final String TMPL_AD_SETUP_CREATE_SUCCESS = 'AD-SCH-SET'; /* Auto-debit setup success (selected invoices) */
    public static final String TMPL_AD_SETUP_MODIFY_SUCCESS = 'AD-SCH-MOD'; /* Auto-debit instruction(s) modified (selected invoices) */
    public static final String TMPL_AD_SETUP_CREATE_SUCCESS_ALL = 'AD-SCH-SET-ALL'; /* Auto-debit setup success (all invoices) */
    public static final String TMPL_AD_SETUP_MODIFY_SUCCESS_ALL = 'AD-SCH-MOD-ALL'; /* Auto-debit instruction(s) modified (all invoices) */
    public static final String TMPL_AD_SCHEDUL_SETUP_FAILED = 'AD-SCH-FAIL'; /* Auto-debit setup failed */
    public static final String TMPL_AD_CARD_DETAILS_UPDATED = 'AD-CRD-UPD'; /* Credit card details updated */
    public static final String TMPL_AD_CARD_EXPIRE_IN_30DAY = 'AD-CRD-EXP'; /* Credit card expires in 30 days */
    public static final String TMPL_AD_PAYMENT_DUE_IN_7DAYS = 'AD-TRX-DUE'; /* Scheduled auto-debit transaction due in 7 days */
    public static final String TMPL_AD_SCHED_TRANS_SUCCESS = 'AD-TRX-SUCC'; /* Scheduled auto-debit transaction successful */
    public static final String TMPL_AD_SCHED_TRANS_MODIFIED = 'AD-TRX-CHGD'; /* Amount/date of auto-debit changed from agreed setup/schedule */
    public static final String TMPL_AD_SCHED_TRANS_FAILED = 'AD-TRX-FAIL'; /* Scheduled auto-debit transaction failed (1st or 2nd or 3rd try) */
    public static final String TMPL_AD_SCHED_TRANS_CANCELED_FAL3 = 'AD-TRX-CANCL-FAL3'; /* Scheduled auto-debit transaction cancelled after 3 fails */
    public static final String TMPL_AD_SCHED_TRANS_CANCELED_PAID = 'AD-TRX-CANCL-PAID'; /* Scheduled auto-debit transaction cancelled due to partial/full payment from other sources */
    public static final String TMPL_AD_ADHOC_TRANS_SUCCESS = 'AD-ADHOC-TRX-SUCC'; /* Ad-hoc remitance transaction success */
    
    public static final String TMPL_AP_ADV_PAY_TRANS_SUCC = 'AP-ADV-PAY-TRANS-SUCC';
    
    public static final String TMPL_DH_SUBM_DOCS_SR = 'DH-SUB-DOC-SR';
    public static final String TMPL_DH_BUK_UNT_INS_APR = 'DH-BUK-UNT-INS-APR';
    public static final String TMPL_DH_BUK_UNT_INS_REJ = 'DH-BUK-UNT-INS-REJ';
    public static final String TMPL_DH_CANC_UNT_INS_APPNT = 'DH-CANC-UNT-INS-APPNT';
    public static final String TMPL_DH_UNT_INS_SNG_REPRTD = 'DH-UNT-INS-SNG-REPRTD';
    public static final String TMPL_DH_UNT_INS_SNG_RESLVD = 'DH-UNT-INS-SNG-RESLVD';
    public static final String TMPL_DH_FIN_PAY_DUE_2W_REC = 'DH-FIN-PAY-DUE-2W-REC';
    public static final String TMPL_DH_FIN_PAY_DUE_7D_PRE = 'DH-FIN-PAY-DUE-7D-PRE';
    public static final String TMPL_DH_FIN_PAY_TRANS_SUCC = 'DH-FIN-PAY-TRANS-SUCC';
    public static final String TMPL_DH_FIN_PAY_TRANS_FAIL = 'DH-FIN-PAY-TRANS-FAIL';
    public static final String TMPL_DH_REQ_HNDOVR_DOCS_SR = 'DH-REQ-HNDOVR-DOCS-SR';
    public static final String TMPL_DH_REQ_SIGNOF_HNDOVR_DOCS_SR_APR = 'DH-REQ-SGNF-HNDVR-DOC-SR-APR';
    public static final String TMPL_DH_REQ_SIGNOF_HNDOVR_DOCS_SR_REJ = 'DH-REQ-SGNF-HNDVR-DOC-SR-REJ';
    public static final String TMPL_DH_CANC_SIGNOF_HNDOVR_DOCS_APPNT = 'DH-CANC-SGNF-HNDVR-DOC-APPNT';
    public static final String TMPL_DH_BUK_KEY_HNDOVR_SR = 'DH-BUK-KEY-HNDOVR-SR';
    public static final String TMPL_DH_BUK_KEY_HNDOVR_SR_APR = 'DH-BUK-KEY-HNDOVR-SR-APR';
    public static final String TMPL_DH_BUK_KEY_HNDOVR_SR_REJ = 'DH-BUK-KEY-HNDOVR-SR-REJ';
    public static final String TMPL_DH_CANC_KEY_HNDOVR_APPNT = 'DH-CANC-KEY-HNDOVR-APPNT';
    
    /* public static final String TMPL_PR_COMPL_FNB_COUPN = 'PR-COMPL-FNB-COUPN'; */
    
    public static final String GENERAL_HEADER = 
    '<!DOCTYPE html>' +  
    '<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">' +  
    '<head>' +  
    '    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">' +  
    '    <meta name="viewport" content="width=device-width">' +  
    '    <meta http-equiv="X-UA-Compatible" content="IE=edge">' +  
    '    <meta name="x-apple-disable-message-reformatting">' +  
    '    <meta name="format-detection" content="telephone=no,address=no,email=no,date=no,url=no">' +  
    '    <meta name="color-scheme" content="light">' +  
    '    <meta name="supported-color-schemes" content="light">' +  
    '    <title></title>' +  
    '    <style>' +  
    '        :root {color-scheme: light; supported-color-schemes: light;}' +  
    '        html, body {margin: 0 auto !important; padding: 0 !important; height: 100% !important; width: 100% !important; }' +  
    '        {-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;}' +  
    '        div[style*="margin: 16px 0"] {margin: 0 !important;}' +  
    '        #MessageViewBody, #MessageWebViewDiv {width: 100% !important;}' +  
    '        table, td {mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;}' +  
    '        table {border-spacing: 0 !important; border-collapse: collapse !important; table-layout: fixed !important; margin: 0 auto !important;}' +  
    '        img {-ms-interpolation-mode: bicubic;}' +  
    '        a {text-decoration: none;}' +  
    '        a[x-apple-data-detectors], .unstyle-auto-detected-links a, .aBn {border-bottom: 0 !important; cursor: default !important; color: inherit !important; ' +  
    '			text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important;}' +  
    '        .im {color: inherit !important;}' +  
    '        .a6S { display: none !important; opacity: 0.01 !important;}' +  
    '        img.g-img+div {display: none !important;}' +  
    '        @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {u~div .email-container {min-width: 320px !important;}}' +  
    '        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {u~div .email-container {min-width: 375px !important;}}' +  
    '        @media only screen and (min-device-width: 414px) {u~div .email-container {min-width: 414px !important;}}' +  
    '    </style>' +  
    '</head>' +  
    '<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly;">' +  
    '    <center role="article" aria-roledescription="email" lang="en" style="width: 100%;">' +  
    '        <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td><![endif]-->' +  
    '        ' +  
    '        <div style="max-height:0; overflow:hidden; mso-hide:all;" aria-hidden="true"></div>' +  
    '        ' +  
    '        <div style="max-width: 680px; margin: 0 auto;" class="email-container">' +  
    '        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-family: Helvetica, sans-serif; font-size: 14px; margin: 0 0 10px 0 !important;">' +  
    '            <tbody>' +  
    '                <tr>' +  
    '                    <td align="center" style="padding: 0 0 10px 0;">' + 
    '                    <span class="banner-wrap" style="display: inline-block;">' +  
    '                        <img src="https://services.damacgroup.com/DCAssets/hello-damac/banners/email-banner.png" alt="Banner" style="width: 100%;">' +  
    '                    </span>' +  
    '                    </td>' +  
    '                </tr>' +  
    '                <tr>' +  
    '                    <td style="background-color: #ffffff; padding-top: 20px;padding-left:20px;padding-right:20px;padding-bottom:20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">' +  
    '                        ';  
    public static final String GENERAL_SALUTATION = 
    '                        <p>Dear {0}, </p>';
    public static final String GENERAL_FOOTER = '' + 
    '                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding:10px 0;font-family: Helvetica, sans-serif;"> ' + 
        '                                <tr><td style="padding:10px 0;font-family: Helvetica, sans-serif;">{0}' + 
    '                                  <p style="text-align: center; margin: 25px 0 0 0;">For any queries please contact us on:' + 
    '                                    <span style="display: block;">' + 
            '										<a href="https://api.whatsapp.com/send?phone={1}" target="_blank" id="" style="text-decoration:none; display: inline-block; margin: 10px 5px 10px 5px; width: 119px">' + 
    '											<img src="https://services.damacgroup.com/DCAssets/hello-damac/banners/watsapp_new.png" border="0" title="WhatsApp" alt="whatsapp" style="width: 118px; height: 38px;vertical-align: middle;" />' + 
    '										</a>' + 
    '									</span>' + 
    '								  </p>' + 
    '                                </td>' + 
    '                                </tr>' + 
    '                                <tr><td style="text-align: center;"><img src="https://services.damacgroup.com/DCAssets/hello-damac/banners/damac_logo.png" title="Damac Logo" alt="Damac Logo" style="margin: 30px 0 0 0; width: 18px; height: 11px;"></td></tr>' + 
    '                                <tr><td style="padding:5px 0 0;font-family: Helvetica, sans-serif; text-align: center;">Kind regards</td></tr>' + 
            '                                <tr><td style="padding:0;font-family: Helvetica, sans-serif;text-align: center;">{2}</td></tr>' + 
    '                        </table>' + 
    '                    </td>' + 
    '                </tr>' + 
    '            </tbody>' + 
    '        </table>' + 
    '   </div>' + 
    '  </center>' + 
    '</body>' + 
    '</html>';
    public static final String TMPL_TABLE_ALL_INV = '' + 
        '<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica, sans-serif; font-size: 12px;">' + 
        '<tr>' + 
        '<td style="width: 25%; height: 15px; background-color: #cdcdcd; color: #000; text-align: center;font-size: 13px; font-weight: bold; padding: 10px;">Unit Name</td>' + 
        '<td style="width: 25%; height: 15px; background-color: #cdcdcd; color: #000; text-align: center;font-size: 13px; font-weight: bold; padding: 10px;">Invoices</td>' + 
        '</tr>' + 
        '{2}</table>';
    public static final String TMPL_TABLE_ALL_INV_ROW = 
        '<tr>' + 
        '<td style="width: 25%; border: solid 1px #cdcdcd; text-align: center; padding:10px; font-size: 12px;">{0}</td>' + 
        '<td style="width: 25%; border: solid 1px #cdcdcd; text-align: center; padding:10px; font-size: 12px;">{1}</td>' +  
        '</tr>';
    public static final String TMPL_TABLE_SEL_INV = '' + 
        '<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica, sans-serif; font-size: 12px;">' + 
        '<tr>' + 
        '<td style="width: 25%; height: 15px; background-color: #cdcdcd; color: #000; text-align: center;font-size: 13px; font-weight: bold; padding: 10px;">Unit Name</td>' + 
        '<td style="width: 25%; height: 15px; background-color: #cdcdcd; color: #000; text-align: center;font-size: 13px; font-weight: bold; padding: 10px;">Invoice No:</td>' + 
        '<td style="width: 25%; height: 15px; background-color: #cdcdcd; color: #000; text-align: center;font-size: 13px; font-weight: bold; padding: 10px;">Due Date</td>' + 
        '<td style="width: 25%; height: 15px; background-color: #cdcdcd; color: #000; text-align: center;font-size: 13px; font-weight: bold; padding: 10px;">Amount</td>' + 
        '</tr>' + 
        '{2}</table>';
    public static final String TMPL_TABLE_SEL_INV_ROW = 
        '<tr>' + 
        '<td style="width: 25%; border: solid 1px #cdcdcd; text-align: center; padding:10px; font-size: 12px;">{0}</td>' + 
        '<td style="width: 25%; border: solid 1px #cdcdcd; text-align: center; padding:10px; font-size: 12px;">{1}</td>' + 
        '<td style="width: 25%; border: solid 1px #cdcdcd; text-align: center; padding:10px; font-size: 12px;">{2}</td>' + 
        '<td style="width: 25%; border: solid 1px #cdcdcd; text-align: center; padding:10px; font-size: 12px;">AED {3}</td>' + 
        '</tr>';
    public static final String DISCLAIM_FOOTNOTE = '' + 
    '                                  <p style="/*text-align: center;*/">If you have not initiated this transaction, or you need any further clarification, please contact' +  
    '                                    <span style="/*display: block; text-align: center; margin: 5px 0 0 0;*/">' + 
    '										<a href="mailto:finance.salesforce@damacgroup.com" target="_blank" id="" style="text-decoration:none;color:#0a25ff;">' + 
    '											<strong style="font-weight:700;">finance.salesforce@damacgroup.com</strong>' + 
    '										</a>' + 
    '                                    </span>' + 
    '                                  </p>';
    public static final String GENERAL_FOOT_NOTE = '' + 
    '                                  <p style="/*text-align: center;*/">For any further information or enquiry, please contact' +  
    '                                    <span style="/*display: block; text-align: center; margin: 5px 0 0 0;*/">' + 
    '										<a href="mailto:finance.salesforce@damacgroup.com" target="_blank" id="" style="text-decoration:none;color:#0a25ff;">' + 
    '											<strong style="font-weight:700;">finance.salesforce@damacgroup.com</strong>' + 
    '										</a>' + 
    '                                    </span>' + 
    '                                  </p>';
    public static final String GENERAL_FOOT_NOTE_DH = '' + 
    '                                  <p style="/*text-align: center;*/">For any further information or enquiry, please contact' +  
    '                                    <span style="/*display: block; text-align: center; margin: 5px 0 0 0;*/">' + 
    '										<a href="mailto:atyourservice@damacproperties.com" target="_blank" id="" style="text-decoration:none;color:#0a25ff;">' + 
    '											<strong style="font-weight:700;">atyourservice@damacproperties.com</strong>' + 
    '										</a>' + 
    '                                    </span>' + 
    '                                  </p>';
    public static final String WHATSAPP_DAMAC = '97143019944';
    public static final String WHATSAPP_LOAMS = '97147049080';
    
    public static Map<String, HD_MailTemplateWrap> TEMPLATE_MAP;
    
    static{
        String subjectTemplate = '';
        String messageTemplate = '';
        String tableRowTemplate = '';
        HD_MailTemplateWrap tmplWrap;
        TEMPLATE_MAP = new Map<String, HD_MailTemplateWrap>();
        
        /* ........................................ AUTO-DEBIT TEMPLATES ........................................ */
        /* Email Template for TMPL_AD_SETUP_CREATE_SUCCESS_ALL */
        subjectTemplate = 'Successful AutoPay authorization setup';
        messageTemplate = '' + 
            '<p>Thank you for submitting an AutoPay authorization online for payment. ' + 
            'Please note the details of your AutoPay authorization to Damac Finance Team.</p>' + 
            TMPL_TABLE_ALL_INV;
        tableRowTemplate = TMPL_TABLE_ALL_INV_ROW;
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate, 
        	tableRowTemplate);
        TEMPLATE_MAP.put(TMPL_AD_SETUP_CREATE_SUCCESS_ALL, tmplWrap);
        
        /* Email Template for TMPL_AD_SETUP_MODIFY_SUCCESS_ALL */
        subjectTemplate = 'AutoPay authorization modification';
        messageTemplate = '' + 
            '<p>Thank you for updating the AutoPay authorization online for payment.' + 
            'Please note the details of your AutoPay authorization to Damac Finance Team.</p>' + 
            TMPL_TABLE_ALL_INV;
        tableRowTemplate = TMPL_TABLE_ALL_INV_ROW;
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate, 
        	tableRowTemplate);
        TEMPLATE_MAP.put(TMPL_AD_SETUP_MODIFY_SUCCESS_ALL, tmplWrap);
        
        /* Email Template for TMPL_AD_SETUP_CREATE_SUCCESS */
        subjectTemplate = 'Successful AutoPay authorization setup';
        messageTemplate = '' + 
            '<p>Thank you for submitting an AutoPay authorization online for payment. ' + 
            'Please note the details of your AutoPay authorization to Damac Finance Team.</p>' + 
            TMPL_TABLE_SEL_INV;
        tableRowTemplate = TMPL_TABLE_SEL_INV_ROW;
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate, 
        	tableRowTemplate);
        TEMPLATE_MAP.put(TMPL_AD_SETUP_CREATE_SUCCESS, tmplWrap);
        
        /* Email Template for TMPL_AD_SETUP_MODIFY_SUCCESS */
        subjectTemplate = 'AutoPay authorization modification';
        messageTemplate = '' + 
            '<p>Thank you for updating the AutoPay authorization online for payment.' + 
            'Please note the details of your AutoPay authorization to Damac Finance Team.</p>' + 
            TMPL_TABLE_SEL_INV;
        tableRowTemplate = TMPL_TABLE_SEL_INV_ROW;
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate, 
        	tableRowTemplate);
        TEMPLATE_MAP.put(TMPL_AD_SETUP_MODIFY_SUCCESS, tmplWrap);
        
        /* Email Template for TMPL_AD_SCHEDUL_SETUP_FAILED */
        subjectTemplate = 'AutoPay authorization setup failed';
        messageTemplate = '' + 
            '<p>Please be advised that your AutoPay authorization setup has failed.</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_AD_SCHEDUL_SETUP_FAILED, tmplWrap);
        
        /* Email Template for TMPL_AD_CARD_DETAILS_UPDATED */
        subjectTemplate = 'AutoPay authorization card updated';
        messageTemplate = '' +  
            '<p>Your card details have been updated as per your request on {3}.</p>';
        /* '<p>Your card number has been successfully updated from {1} to {2} on {3}.</p>' */  /* dont show card details */
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_AD_CARD_DETAILS_UPDATED, tmplWrap);
        
        /* Email Template for TMPL_AD_CARD_EXPIRE_IN_30DAY */
        subjectTemplate = 'AutoPay authorization requires modification';
        messageTemplate = '' + 
            '<p>The credit card configured for AutoPay of your invoices expires on {2}. Kindly update the card details.</p>';
        /* '<p>Your AutoPay credit card {1} expires on {2}. Kindly update the card details.</p>' */  /* dont show card details */
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_AD_CARD_EXPIRE_IN_30DAY, tmplWrap);
        
        /* Email Template for TMPL_AD_PAYMENT_DUE_IN_7DAYS */
        subjectTemplate = 'AutoPay authorization for unit {0} due on {1}';
        messageTemplate = '' + 
            '<p>Please be advised that your AutoPay authorization of AED {1} for payment of {2} ' + 
            'for your unit {3} will be processed for payment on {4}.</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_AD_PAYMENT_DUE_IN_7DAYS, tmplWrap);
        
        /* Email Template for TMPL_AD_SCHED_TRANS_SUCCESS */
        subjectTemplate = 'Successful AutoPay transaction for unit {0}';
        messageTemplate = '' + 
            '<p>Transaction successfully completed for your AutoPay authorisation of AED {1} for payment of ' + 
            '{2} for your unit {3} on {4}. {5}</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_AD_SCHED_TRANS_SUCCESS, tmplWrap);
        
        /* Email Template for TMPL_AD_SCHED_TRANS_MODIFIED */
        subjectTemplate = 'AutoPay authorization setup for unit {0} is modified';
        messageTemplate = '' + 
            '<p>Your AutoPay authorization for payment of {3} for your unit {1} has been modified.</p>' + 
            TMPL_TABLE_SEL_INV;
        tableRowTemplate = TMPL_TABLE_SEL_INV_ROW;
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate, 
        	tableRowTemplate);
        TEMPLATE_MAP.put(TMPL_AD_SCHED_TRANS_MODIFIED, tmplWrap);
        
        /* Email Template for TMPL_AD_SCHED_TRANS_FAILED */
        subjectTemplate = 'AutoPay authorization for unit {0} has failed';
        messageTemplate = '' + 
            '<p>Please be advised that your AutoPay authorization for payment of {3} for your unit {1} has failed.</p>' + 
            TMPL_TABLE_SEL_INV;
        tableRowTemplate = TMPL_TABLE_SEL_INV_ROW;
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate, 
        	tableRowTemplate);
        TEMPLATE_MAP.put(TMPL_AD_SCHED_TRANS_FAILED, tmplWrap);
        
        /* Email Template for TMPL_AD_SCHED_TRANS_CANCELED_FAL3 */
        subjectTemplate = 'AutoPay authorization for unit {0} has failed';
        messageTemplate = '' + 
            '<p>Please be advised that your AutoPay authorization for payment of {3} for your unit {1} has failed.</p>' + 
            TMPL_TABLE_SEL_INV + 
            '<p>As the payment is now overdue, and in order to avoid late payment fees, we request you to settle the charges immediately. ' + 
            'You can use our convenient online payment system by logging in to our community portal, ' + 
            '<a href="https://www.damacliving.com">www.damacliving.com</a>.</p>';
        tableRowTemplate = TMPL_TABLE_SEL_INV_ROW;
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate, 
        	tableRowTemplate);
        TEMPLATE_MAP.put(TMPL_AD_SCHED_TRANS_CANCELED_FAL3, tmplWrap);
        
        /* Email Template for TMPL_AD_SCHED_TRANS_CANCELED_PAID */
        subjectTemplate = 'AutoPay authorization modification for unit {0}';
        messageTemplate = '' + 
            '<p>Please be advised that the payment of {3} for your unit {1} have already been made. ' + 
            'Hence, we will not be processing the payments mentioned below as per the AutoPay authorization provided to us.</p>' + 
            TMPL_TABLE_SEL_INV;
        tableRowTemplate = TMPL_TABLE_SEL_INV_ROW;
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate, 
        	tableRowTemplate);
        TEMPLATE_MAP.put(TMPL_AD_SCHED_TRANS_CANCELED_PAID, tmplWrap);
        
        /* Email Template for TMPL_AD_ADHOC_TRANS_SUCCESS */
        subjectTemplate = 'Successful transaction for unit {0}';
        messageTemplate = '' + 
            '<p>Transaction successfully completed for amount of AED {1} for the payment of ' + 
            '{2} for your unit {3} on {4}. {5}</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_AD_ADHOC_TRANS_SUCCESS, tmplWrap);
        
        /* ........................................ ADVANCE PAYMENTS TEMPLATES ........................................ */
        /* Email Template for TMPL_AP_ADV_PAY_TRANS_SUCC */
        subjectTemplate = 'Advanced Payment - Successful transaction for unit {0}';
        messageTemplate = '' + 
            '<p>Transaction successfully completed for amount of AED {1} for the payment of ' + 
            '{2} for your unit {3} on {4}. {5}</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_AP_ADV_PAY_TRANS_SUCC, tmplWrap);
        
        /* ........................................ DIGITAL HANDOVER TEMPLATES ........................................ */
        /* Email Template for TMPL_DH_SUBM_DOCS_SR */
        subjectTemplate = 'Handover documents submitted for unit {0}';
        messageTemplate = '' + 
            '<p>Thank you for submitting a request regarding your completed handover documents. ' + 
            'Our handover team will review the online versions and arrange a courier to collect the physical copies.</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_DH_SUBM_DOCS_SR, tmplWrap);
        
        /* Email Template for TMPL_DH_BUK_UNT_INS_APR */
        subjectTemplate = 'Unit Inspection for Unit {0}';
        messageTemplate = '' + 
            '<p>Thank you for submitting your request for a unit inspection. Your request has been approved. ' + 
            'Please see the appointment details below: </p>' + 
            '<ul><li><b>Date:&nbsp;</b><i>{1}</i></li>' + 
            	'<li><b>Time:&nbsp;</b><i>{2}</i></li></ul>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_DH_BUK_UNT_INS_APR, tmplWrap);
        
        /* Email Template for TMPL_DH_BUK_UNT_INS_REJ */
        subjectTemplate = 'Unit Inspection for Unit {0}';
        messageTemplate = '' + 
            '<p>Thank you for submitting your request for a unit inspection. Unfortunately, ' + 
            'your request has been rejected because {1}. ' + 
            'Please submit another request to book again.</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_DH_BUK_UNT_INS_REJ, tmplWrap);
        
        /* Email Template for TMPL_DH_CANC_UNT_INS_APPNT */
        subjectTemplate = 'Unit inspection cancelled for unit {0}';
        messageTemplate = '' + 
            '<p>You have cancelled your unit inspection. Please book again at your convenience.</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_DH_CANC_UNT_INS_APPNT, tmplWrap);
        
        /* Email Template for TMPL_DH_UNT_INS_SNG_REPRTD */
        subjectTemplate = 'Snags recorded for unit {0}';
        messageTemplate = '' + 
            '<p>Snags reported during your unit inspection have been recorded and we are working to fix them. ' + 
            'We will notify you once everything has been resolved.</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_DH_UNT_INS_SNG_REPRTD, tmplWrap);
        
        /* Email Template for TMPL_DH_UNT_INS_SNG_RESLVD */
        subjectTemplate = 'Snags resolved for unit {0}';
        messageTemplate = '' + 
            '<p>Snags reported during your unit inspection have been resolved. ' + 
            'Please login to Hello DAMAC app to view the results.</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_DH_UNT_INS_SNG_RESLVD, tmplWrap);
        
        /* Email Template for TMPL_DH_FIN_PAY_DUE_2W_REC */
        subjectTemplate = 'Final payment for unit {0} due on {1}';
        messageTemplate = '' + 
            '<p>Please be advised that your final payment of AED {1} for your unit {2} ' + 
            'will be processed for payment on {3}.</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_DH_FIN_PAY_DUE_2W_REC, tmplWrap);
        
        /* Email Template for TMPL_DH_FIN_PAY_DUE_7D_PRE */
        subjectTemplate = 'Final payment for unit {0} due on {1}';
        messageTemplate = '' + 
            '<p>Please be advised that your final payment of AED {1} for your unit {2} ' + 
            'will be processed for payment on {3}.</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_DH_FIN_PAY_DUE_7D_PRE, tmplWrap);
        
        /* Email Template for TMPL_DH_FIN_PAY_TRANS_SUCC */
        subjectTemplate = 'Final payment for unit {0} - transaction successful';
        messageTemplate = '' + 
            '<p>The final payment of AED {1} for your unit {2} done on {3} was completed successfully. {5}';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_DH_FIN_PAY_TRANS_SUCC, tmplWrap);
        
        /* Email Template for TMPL_DH_FIN_PAY_TRANS_FAIL */
        subjectTemplate = 'Final payment for unit {0} failed';
        messageTemplate = '' + 
            '<p>Please be advised that your final payment of AED {1} for your unit {2} done on {3} has failed.</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_DH_FIN_PAY_TRANS_FAIL, tmplWrap);
        
        /* Email Template for TMPL_DH_REQ_HNDOVR_DOCS_SR */
        subjectTemplate = 'Handover documents for unit {0}';
        messageTemplate = '' + 
            '<p>Thank you for submitting your request regarding your handover documents. ' + 
            'Our handover team will review the request details and get back to you shortly.</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_DH_REQ_HNDOVR_DOCS_SR, tmplWrap);
        
        /* Email Template for TMPL_DH_REQ_SIGNOF_HNDOVR_DOCS_SR_APR */
        subjectTemplate = 'Handover documents for unit {0}';
        messageTemplate = '' + 
            '<p>Thank you for submitting a request to sign off your handover documents. Your request has been approved. ' + 
            'Please see the appointment details below: </p>' + 
            '<ul><li><b>Date:&nbsp;</b><i>{1}</i></li>' + 
            	'<li><b>Time:&nbsp;</b><i>{2}</i></li></ul>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_DH_REQ_SIGNOF_HNDOVR_DOCS_SR_APR, tmplWrap);
        
        /* Email Template for TMPL_DH_REQ_SIGNOF_HNDOVR_DOCS_SR_REJ */
        subjectTemplate = 'Handover documents for unit {0}';
        messageTemplate = '' + 
            '<p>Thank you for submitting a request to sign off your handover documents. ' + 
            'Unfortunately your request has been rejected because {1}. Please book again.</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_DH_REQ_SIGNOF_HNDOVR_DOCS_SR_REJ, tmplWrap);
        
        /* Email Template for TMPL_DH_CANC_SIGNOF_HNDOVR_DOCS_APPNT */
        subjectTemplate = 'Handover documents sign off event cancelled for unit {0}';
        messageTemplate = '' + 
            '<p>You have cancelled your booking to sign-off your handover documents. Please reschedule the event at your convenience.</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_DH_CANC_SIGNOF_HNDOVR_DOCS_APPNT, tmplWrap);
        
        /* Email Template for TMPL_DH_BUK_KEY_HNDOVR_SR */
        subjectTemplate = 'Key handover for unit {0}';
        messageTemplate = '' + 
            '<p>Thank you for submitting a request for your key handover. ' + 
            'Our team will review your request details and confirm with you shortly.</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_DH_BUK_KEY_HNDOVR_SR, tmplWrap);
        
        /* Email Template for TMPL_DH_BUK_KEY_HNDOVR_SR_APR */
        subjectTemplate = 'Key handover for unit {0}';
        messageTemplate = '' + 
            '<p>Thank you for submitting a request to schedule your key handover. Your request has been approved. ' + 
            'Please see the appointment details below: </p>' + 
            '<ul><li><b>Date:&nbsp;</b><i>{1}</i></li>' + 
            	'<li><b>Time:&nbsp;</b><i>{2}</i></li></ul>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_DH_BUK_KEY_HNDOVR_SR_APR, tmplWrap);
        
        /* Email Template for TMPL_DH_BUK_KEY_HNDOVR_SR_REJ */
        subjectTemplate = 'Key handover for unit {0}';
        messageTemplate = '' + 
            '<p>Thank you for submitting a request for your key handover. ' + 
            'Your request has been rejected because {1}. ' + 
            'Please submit another request to re-book key handover.</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_DH_BUK_KEY_HNDOVR_SR_REJ, tmplWrap);
        
        /* Email Template for TMPL_DH_CANC_KEY_HNDOVR_APPNT */
        subjectTemplate = 'Key handover cancelled for unit {0}';
        messageTemplate = '' + 
            '<p>You have cancelled your key handover appointment. Please book again at your convenience.</p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_DH_CANC_KEY_HNDOVR_APPNT, tmplWrap);
        
        /* ........................................ PROMOTIONAL OFFER TEMPLATES ........................................ */
        /* Email Template for TMPL_PR_COMPL_FNB_COUPN */
        /* 
        subjectTemplate = 'Greetings from DAMAC: Complementary food coupon for you';
        messageTemplate = '' + 
            '<p>We thank you for your whole hearted support towards our endeavour to re-define lifestyle experience. ' + 
            '<br />As a token of our gratitude, please accept this complementary F&B coupon. ' + 
            'Use this coupon code at any food and beverages outlet to redeem a discount of 50% to a maximum limit of 200 AED. </p>' + 
            '<p><b>{1}</b></p>';
        tmplWrap = new HD_MailTemplateWrap(
            subjectTemplate, 
        	messageTemplate);
        TEMPLATE_MAP.put(TMPL_PR_COMPL_FNB_COUPN, tmplWrap); */
    } /* static block */
    
    global class HD_MailTemplateWrap {
        public String subjTemplate {get; set;}
        public String bodyTemplate {get; set;}
        public String tableRowTemplate {get; set;}
        
        public HD_MailTemplateWrap(String subjTemplate, String bodyTemplate) {
            this.subjTemplate = subjTemplate;
            this.bodyTemplate = bodyTemplate;
        }
        
        public HD_MailTemplateWrap(String subjTemplate, String bodyTemplate, String tableRowTemplate) {
            this.subjTemplate = subjTemplate;
            this.bodyTemplate = bodyTemplate;
            this.tableRowTemplate = tableRowTemplate;
        }
    }
}