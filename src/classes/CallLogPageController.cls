public class CallLogPageController {

    public CallLogPageController(ApexPages.StandardController controller) {

    }


    public static void sendToPhpUtility () {
        Set<Id> setCallId = new Set<Id>();
        setCallId.add(ApexPages.currentPage().getParameters().get('id'));
        System.debug('callLogId---'+setCallId);
        CallLogTranscriptUtility.sendCallOutData(setCallId); 
    }
}