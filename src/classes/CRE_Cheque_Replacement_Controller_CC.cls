public without sharing class CRE_Cheque_Replacement_Controller_CC extends CRE_Cheque_Replacement_Controller{
    public CRE_Cheque_Replacement_Controller_CC() {
        super(false);
        if (CustomerCommunityUtils.isCurrentView('chequereplacement')) {
            super();
        }
    }
    public override pageReference submitCase(){
        try{
        if(String.isNotBlank(strSelectedProcess)){
            if(String.isBlank(strSelectedBookingUnit)){
                ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Error,'Please select a booking unit'));
            }
            else{
                if(strSelectedProcess == 'Cheque Hold'){
                    lstChequeFinalWrapper = new List<ChequeDetailWrapper>();
                    boolean blnIsSelected = false;
                    boolean blnIsSelectedWithHoldDays = true;

                    for(ChequeDetailWrapper obj : lstChequeDetailWrapper){
                        if(obj.blnIsReplaced){
                            if(string.isNotBlank(obj.strSelectedDayHold)){
                                blnIsSelected = true;
                                lstChequeFinalWrapper.add(obj);
                            }
                            else{
                                blnIsSelectedWithHoldDays = false;
                                ApexPages.addmessage(new ApexPages.message(
                                ApexPages.severity.Error,'Please select Days to Hold for Cheque No: '+obj.strChequeNo));
                            }

                        }
                    }
                    if(blnIsSelected && blnIsSelectedWithHoldDays){

                    }
                    else{
                        ApexPages.addmessage(new ApexPages.message(
                        ApexPages.severity.Error,'Please select Cheque Number and Days to Hold'));
                        return null;
                    }

                }
                if(strSelectedProcess == 'Security Cheque Collection'){
                    lstChequeFinalWrapper = new List<ChequeDetailWrapper>();
                    boolean blnIsSelected = false;

                    for(ChequeDetailWrapper obj : lstChequeDetailWrapper){
                        if(obj.blnIsReplaced){
                            blnIsSelected = true;
                            lstChequeFinalWrapper.add(obj);
                        }
                    }
                    if(blnIsSelected){

                    }
                    else{
                        ApexPages.addmessage(new ApexPages.message(
                        ApexPages.severity.Error,'Please select at least one Cheque Number'));
                        return null;
                    }
                }
                if(lstChequeFinalWrapper.Size()>0){
                    Id devRecordTypeId =
                    Schema.SObjectType.Case.getRecordTypeInfosByName().get(strSelectedProcess).getRecordTypeId();
                    objCase  = new Case();
                    objCase.AccountID = strSelectedAccount ;
                    objCase.RecordTypeID = devRecordTypeId;
                    objCase.Origin = 'Portal';

                    /*if(strSelectedProcess == 'Cheque Replacement'){
                        objCase.SR_type__c = 'Cheque Replacement SR';
                    }
                    else if(strSelectedProcess == 'Cheque Hold'){
                        objCase.SR_type__c = 'Cheque Hold SR';
                    }
                    else if(strSelectedProcess == 'Security Cheque Collection'){
                        objCase.SR_type__c = 'Security Cheque Collection SR';
                    }*/

                    objCase.SR_type__c = strSelectedProcess == 'Cheque Replacement' ? 'Cheque Replacement SR' : (strSelectedProcess == 'Cheque Hold' ? 'Cheque Hold SR' : 'Security Cheque Collection SR');
                    objCase.booking_unit__c = strSelectedBookingUnit;
                    objCase.Status = 'Submitted';
                    objCase.Roles_from_Rule_Engine__c = 'Manager,Director,Sales Admin AVP,HOD';
                    upsert objCase;

                    strCaseID = objCase.Id;

                    List<Case_Post_Dated_Cheque__c> lstCasePDC = new List<Case_Post_Dated_Cheque__c>();
                    for(ChequeDetailWrapper obj : lstChequeFinalWrapper){
                        obj.objPDC.Case__c = objCase.id;
                        obj.objPDC.Receipt_No__c = obj.strReceiptNo ;
                        obj.objPDC.CurrencyISOCode = objBookingUnit.CurrencyIsoCode;

                        obj.objPDC.Reason_For_Replacement__c = obj.strSelectedReason;
                        if(string.isNotBlank(obj.strMaturityDate)){
                            obj.objPDC.New_maturity_date__c = Date.parse(obj.strMaturityDate);
                        }
                        obj.objPDC.Days_to_Hold__c = obj.strSelectedDayHold;

                        /*if(strSelectedProcess == 'Cheque Replacement'){
                            obj.objPDC.Reason_For_Replacement__c = obj.strSelectedReason;
                            if(string.isNotBlank(obj.strMaturityDate)){
                                obj.objPDC.New_maturity_date__c = Date.parse(obj.strMaturityDate);
                            }
                        }*/
                        /*else if(strSelectedProcess == 'Cheque Hold'){
                            obj.objPDC.Days_to_Hold__c = obj.strSelectedDayHold;
                        }*/

                        lstCasePDC.add(obj.objPDC);
                    }

                    insert lstCasePDC;

                    objCase = [Select id,CaseNumber,subject,RecordType.name, ownerID from Case where id=: objCase.ID ];

                    Task objTask = new Task();
                    //if(objCase.RecordType.Name == 'Cheque Replacement'){
                        objTask =  TaskUtility.getTask((SObject)objCase, 'Verify Case Details', 'CRE',
                                   objCase.RecordType.Name, system.today().addDays(1));
                    //}
                    //else if(objCase.RecordType.Name == 'Cheque Hold'){
                    //    objTask =  TaskUtility.getTask((SObject)objCase, 'Hold cheque', 'Finance',
                    //               objCase.RecordType.Name, system.today().addDays(1));
                    //}
                    //else if(objCase.RecordType.Name == 'Security Cheque Collection'){
                    //    objTask =  TaskUtility.getTask((SObject)objCase, 'Confirm dt for security chq coll', 'Finance',
                    //               objCase.RecordType.Name, system.today().addDays(1));
                    //}
                    objTask.OwnerId = Label.DefaultCaseOwnerId;
                    system.debug('--objTask--'+objTask);
                    insert objTask;

                    strCaseID = objCase.Id;

                    return null;
                }

            }
        }
        else{
            ApexPages.addmessage(new ApexPages.message(
                ApexPages.severity.Error,'Please select Type first'));

        }
        return null;

        }
        catch(exception ex){
            ApexPages.addmessage(new ApexPages.message(
                ApexPages.severity.Error,ex.getMessage()));
                return null;
        }

    }
}