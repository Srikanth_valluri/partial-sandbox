/*
 * Description - Test class for EmailMessageTriggerHandler
 *
 * Version        Date            Author            Description
 * 1.0            01/03/18        Vivek Shinde      Initial Draft
 */
@isTest
private class EmailMessageTriggerHandlerTest {

    static testMethod void afterInsertTest() {
        Case objCase = new Case();
        objCase.Origin = 'Email';
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        insert objCase;
        
        objCase.Status = 'Closed';
        update objCase;
        
        EmailMessage[] newEmail = new EmailMessage[0];
        newEmail.add(new EmailMessage(
            FromAddress = 'test@abc.org', 
            Incoming = True, ToAddress= 'hello@670ocglw7xhomi4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com', 
            Subject = 'Test email', 
            TextBody = '23456 ', 
            ParentId = objCase.Id)); 
        insert newEmail;
        
        EmailMessageTriggerHandler.getGroupIdFromDeveloperName('Contact_Center_Queue', 'Queue');
    }
}