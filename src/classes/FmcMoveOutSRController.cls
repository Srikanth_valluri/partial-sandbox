/*----------------------------------------------------------------------
Description: Controller for move out request component

========================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
-----------------------------------------------------------------------
1.0     | 23-07-2018       | Bhanu Gupta   | 1. Initial draft
=======================================================================
*/

public without sharing class FmcMoveOutSRController extends MoveOutSRComponentController{
            String                  caseId              {get;set;}
    public  list<SelectOption>      lstUnits            {get; set;}
    public  List<Booking_Unit__c>   objUnitList         {get; set;}
    public  map<Id,Booking_Unit__c> mapUnitsOwned       {get; set;}
    public  map<Id,Booking_Unit__c> mapMyUnits          {get; set;}
    //@TestVisible
    //public  String                  strUnitId           {get; set;}
    public  List<Id>                objUnitOwner        {get; set;}
    public  List<Id>                objUnitRes          {get; set;}
    public  Boolean                 continueInit        {get; set;}

    public FmcMoveOutSRController() {
        super(false);
        if (FmcUtils.isCurrentView('MoveOut')) {
            super();
            System.debug('FmcMoveOutSRController constructor');
            System.debug('isCurrentViewMoveOut = ' + FmcUtils.isCurrentView('MoveOut'));
            //continueInit = true;
            continueInit = false;
            objLoc = new Location__c();
            unitName = '';
            uploadedDocLst = new Set<String>();
            isTenant = false;
            objFMCase = new FM_Case__c();
            noOfRequDocs =0;
            objBU = new Booking_Unit__c();
            uploadedDocuments = new list<SR_Attachments__c>();
            lstDocuments = new List<FM_Documents__mdt>();
            objAcc = new Account();
            Map<String, String> params = ApexPages.currentPage().getParameters();
            caseId = params.get('id');
            if(String.isBlank(caseId)) {
                strAccountId = CustomerCommunityUtils.customerAccountId;
            } else {
                objFMCase = FM_Utility.getCaseDetails( caseId );
                strAccountId = objFMCase.Account__c ;
                objFMCase = FM_Utility.getCaseDetails(caseId);
            }
            strSRType = 'Move_out_Request';
            //fetchUnits();
             if(String.isNotBlank(caseId)) {
                System.debug('==caseId='+caseId);
                List<FM_Case__c> lstCase = [ SELECT Name
                                                    , Status__c
                                                    , Id
                                                    , Company__c
                                                    , Contractor__c
                                                    , Tenant__c
                                                    , Booking_Unit__c
                                                    , Mobile_no_contractor__c
                                                    , Expected_move_out_date__c
                                             FROM FM_Case__c
                                             WHERE Id=: caseId ];
                if (!lstCase.isEmpty()) {
                    objFMCase = lstCase[0];
                    System.debug('==objFMCase='+objFMCase);
                    strUnitId = objFMCase.Booking_Unit__c;
                    strAccountId = objFMCase.Tenant__c;
                    Integer day, month, year;
                    strDate = '';
                    if(objFMCase.Expected_move_out_date__c != NULL) {
                        day = objFMCase.Expected_move_out_date__c.day();
                        month = objFMCase.Expected_move_out_date__c.month();
                        year = objFMCase.Expected_move_out_date__c.year();
                        // strDate = objFMCase.Expected_move_out_date__c.format();
                        strDate = String.valueOf(month) + '/'+ String.valueOf(day) + '/'+String.valueOf(year);
                    }

                    //get previously uploaded documents
                    uploadedDocuments = [ SELECT Type__c
                                                 , Name
                                                 , View__c
                                                 , Attachment_URL__c
                                          FROM SR_Attachments__c
                                          WHERE FM_Case__c =:objFMCase.Id ];
                    getUploadedDocuments();
                }
            }
            else {
                objFMCase.Raised_Date__c = System.now();
                objFMCase.Origin__c='Portal';
                objFMCase.Status__c='New';
                Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get(
                                        'Move Out').getRecordTypeId();
                objFMCase.RecordTypeId = devRecordTypeId;//Set record type
            }

            if(String.isNotBlank(strAccountId)) {
                List<Account> lstAcc = new List<Account>();
                lstAcc = [ SELECT Id
                                  , Party_ID__c
                                  , Name
                                  , Mobile_Phone__pc
                                  , Email__pc
                                  , isPersonAccount
                           FROM Account
                           WHERE Id =: strAccountId ];
                if(lstAcc.size() >0) {
                    objAcc = lstAcc[0];
                }
            }
            if(String.isNotBlank(strUnitId)){
                getUnitDetails();
                unitName = objBU.Unit_Name__c;
            }
            fetchUnits();

            getDocumentsList();//get documents
            objFMCase.Request_Type_DeveloperName__c = isTenant ? 'Move_out_Request_T'
                                                                    : 'Move_out_Request';
        }
    }

    public void fetchUnits(){
        Set<String> activeStatusSet= Booking_Unit_Active_Status__c.getall().keyset();
        mapUnitsOwned = new map<Id,Booking_Unit__c>();
        objUnitOwner = new List<Id>();
        objUnitRes = new List<Id>();
        mapMyUnits = new map<Id,Booking_Unit__c>();
        lstUnits = new list<SelectOption>();
        lstUnits.add(new selectOption('None','--None--'));
        objUnitList = [SELECT Id
                            , Owner__c
                            , Booking__r.Account__c
                            , Booking__r.Account__r.Name
                            , Owner__r.IsPersonAccount
                            , Tenant__r.IsPersonAccount
                            , Owner__r.Name
                            , Tenant__c
                            , Tenant__r.Name
                            , Resident__c
                            , Resident__r.Name
                            , Unit_Name__c
                            , Registration_Id__c
                            , Property_Name__c
                         FROM Booking_Unit__c
                         WHERE ( Resident__c = :strAccountId
                                OR Booking__r.Account__c = :strAccountId
                                OR Tenant__c = :strAccountId )
                        AND (
                            (Registration_Status__c IN :activeStatusSet
                             AND ( Handover_Flag__c = 'Y' OR Early_Handover__c = true )
                            )
                            OR Dummy_Booking_Unit__c = TRUE
                        )
        ];
        if(!objUnitList.isEmpty() && objUnitList != NULL){
            for( Booking_Unit__c objBU : objUnitList ){
                if( objBU.Owner__c == strAccountId ){
                    objUnitOwner.add(objBU.Id);
                    mapUnitsOwned.put(objBU.Id,objBU);
                }

                if( ( objBU.Tenant__c == strAccountId || objBU.Resident__c == strAccountId )
                            && objBU.Owner__c != strAccountId ) {
                     objUnitRes.add(objBU.Id);
                     mapMyUnits.put(objBU.Id,objBU);
                }
                lstUnits.add(new selectOption(objBU.Id,objBU.Unit_Name__c));
            }
        }else {
            ApexPages.addmessage(new ApexPages.message(
                                ApexPages.severity.Error,'No Booking Units available'));
        }
        system.debug('==lstUnits=='+lstUnits);
    }

    public override String createAlreadySubmittedErrorMessage(List<FM_Case__c> lstSubmittedCases) {
        return 'This Request is already Submitted';
    }

    public void getUnitDetails(){
        //system.debug('==inUnitId=='+inUnitId);
        system.debug('==strUnitId=='+strUnitId);
        unitName = '';
        if(String.isNotBlank(strUnitId) && !strUnitId.equalsIgnoreCase('None')) {

            if (String.isBlank(checkExistingSubmittedCases())) {
                continueInit = false;
                System.debug('Page has messages? = ' + Apexpages.hasMessages());
                for (Apexpages.Message pm : Apexpages.getMessages()) {
                    System.debug('Page message = ' + pm);
                }
            } else {
                continueInit = true;
                objBU = [SELECT Id
                                , Owner__c
                                , Resident__c
                                , Account_Id__c
                                , Booking__r.Account__c
                                , Booking__r.Account__r.Name
                                , Property_City__c
                                , Booking__r.Account__r.Mobile_Phone__pc
                                , Booking__r.Account__r.Email__pc
                                , Tenant__c
                                , Tenant__r.Name
                                , Tenant__r.Mobile_Phone__pc
                                , Tenant__r.Email__pc
                                , Unit_Name__c
                        FROM Booking_Unit__c
                        WHERE Id =: strUnitId ];
                isTenant = objBU.Booking__r.Account__c == objBU.Resident__c ? false : true;
                objFmCase.Initiated_by_tenant_owner__c = isTenant ? 'tenant' : 'owner';
                //Get community name from Location
                if(objBU != NULL) {
                    if(objBU.Resident__c == objAcc.Id) {
                        system.debug('Has Resident');
                        isResident=true;
                        unitName = objBU.Unit_Name__c;
                    }else {
                        system.debug('Dont Have Resident');
                        isResident = false;
                        unitName = '';
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                                                    'You are not resident on the selected unit'));
                    }
                    System.debug('==isResidentisResident==='+isResident);
                    if(objBU.Unit_Name__c != NULL) {
                        list<Location__c> lstLocs = new list<Location__c>();
                         lstLocs = [ SELECT Community_Name__c
                                            , Move_In_Move_Out_security_cheque_amount__c
                                            ,Name
                                     FROM Location__c
                                     WHERE Name =: objBU.Unit_Name__c.substringBefore('/')
                                     LIMIT 1 ];
                        if(lstLocs.size() > 0) {
                            objLoc = lstLocs[0];
                            objFmCase.Building_Name__c = objLoc.Name;
                        }
                    }
                }
            }
        }
        else {
            continueInit = false;
        }
        system.debug('==objBU=='+objBU);
        system.debug('==unitName=='+unitName);
        system.debug('==isTenant=='+isTenant);
        getDocumentsList();
    }

    public override PageReference submit() {
        System.debug('objBU' + objBU);
        if(objFMCase.Id == NULL) {
            insertCase();
        }
        if(objFMCase.Id != NULL &&
            (objFMCase.Approval_Status__c == 'Approved' || objFMCase.Status__c == 'Closed')) {
          PageReference pageRef = new PageReference('/'+objFMCase.Id);
          return pageRef;
        }
        if(objFMCase.Move_in_move_out_type__c == 'Self move-out') {
            objFMCase.Company__c = objFMCase.Contractor__c = objFMCase.Mobile_no_contractor__c = '';
        }
        //Populate Admin from Locaion(FM User)
        System.debug('objBU' + objBU);
        System.debug('objBU.Unit_Name__c' + objBU.Unit_Name__c);

        List<Location__c> lstObjLoc = [SELECT Community_Name__c
                                    , ( SELECT FM_User__c
                                               , FM_User__r.Email
                                               , FM_Role__c
                                               , FM_User__r.Name
                                        FROM FM_Users__r )
                              FROM Location__c
                              WHERE Name =: objBU.Unit_Name__c.substringBefore('/')
                              LIMIT 1 ];

        if(lstObjLoc.size() > 0) {
            objLoc = lstObjLoc[0];
        }
        
        if(objLoc != NULL && objLoc.FM_Users__r.size() > 0) {
            for(FM_User__c obj: objLoc.FM_Users__r) {
                if(obj.FM_Role__c == 'FM Admin') {
                    objFMCase.Admin__c = obj.FM_User__c;
                    // break;
                }
                else if(obj.FM_Role__c == 'Property Manager') {
                    objFMCase.Property_Manager_Email__c = obj.FM_User__r.Email;
                    objFMCase.Property_Manager_Name__c = obj.FM_User__r.Name;
                    // break;
                }
            }
        }
        objFMCase.Request_Type__c = 'Move Out';
        try{
            List<String> lstDate = strDate.split('/');
            System.debug('==lstDate==' + lstDate);
            System.debug('==strDate==' + strDate);
            objFMCase.Expected_move_out_date__c = Date.newinstance(Integer.valueOf(lstDate[2]),
            Integer.valueOf(lstDate[0]),Integer.valueOf(lstDate[1]));
            // objFMCase.Expected_move_out_date__c = Date.parse(strDate);
            list<FM_Approver__mdt> lstApprovers = FM_Utility.fetchApprovers(
                            objFMCase.Request_Type_DeveloperName__c,objBU.Property_City__c);
            System.debug('==lstApprovers==' + lstApprovers);
            String approvingRoles = '';
            for(FM_Approver__mdt mdt : lstApprovers) {
                approvingRoles += mdt.Role__c + ',';
            }
            approvingRoles = approvingRoles.removeEnd(',');
            objFMCase.Approving_Authorities__c = approvingRoles;
            objFMCase.Booking_Unit__c = objBU.Id;
            objFMCase.Tenant__c = objAcc.Id;
            // objFMCase.Account__c = objBU.Owner__c;
            objFMCase.Account__c = objBU.Booking__r.Account__c;
            objFMCase.Status__c = 'Submitted';
            objFMCase.Approval_Status__c='Pending';
            objFMCase.Tenant_Email__c = objAcc.Email__pc;
            objFMCase.Submit_for_Approval__c = true;        //Uncommented this - 17/05/2020
            objFMCase.Submitted__c = true;
            System.debug('=objFMCase==='+objFMCase);
            upsert objFMCase;
            return new PageReference('/' + objFMCase.Id);
        }
        catch(Exception excp) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                                        excp.getMessage() +': ' + excp.getLineNumber()));
        }
        return null;
    }

    public override PageReference saveAsDraft() {
        //objFMCase.Request_Type__c = 'Move Out';
        try{
            objFMCase.Request_Type__c = 'Move Out';
            System.debug('==strDate==='+strDate);
            System.debug('==Move_in_move_out_type__c==' + objFMCase.Move_in_move_out_type__c);
            if(objFMCase.Move_in_move_out_type__c == 'Self move-out') {
                objFMCase.Company__c = objFMCase.Contractor__c = objFMCase.Mobile_no_contractor__c = '';
            }
            List<String> lstDate = strDate.split('/');
            objFMCase.Expected_move_out_date__c = Date.newinstance(Integer.valueOf(lstDate[2]),
            Integer.valueOf(lstDate[0]),Integer.valueOf(lstDate[1]));
            objFMCase.Status__c='Draft Request';
            // strDate = string.valueOfGmt(strDate);
            // objFMCase.Expected_move_out_date__c = Date.parse(strDate);
            upsert objFMCase;
            return new PageReference('/' + objFMCase.Id);
        }
        catch(Exception excp) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,
                                        excp.getMessage() +': ' + excp.getLineNumber()));
        }
        return null;
    }
    public PageReference saveSr() {
       PageReference nextPage = saveAsDraft();
        if (nextPage != NULL) {
            String unitId = nextPage.getUrl().substringAfterLast('/');
            nextPage = new PageReference(ApexPages.currentPage().getUrl());
            nextPage.getParameters().clear();
            nextPage.getParameters().put('view', 'CaseDetails');
            nextPage.getParameters().put('id', unitId);
            nextPage.setRedirect(true);
        }
        //System.debug('nextPage = ' + nextPage);
        //System.debug('nextPage Url = ' + nextPage.getUrl());
        return nextPage;
    }

    public PageReference submitSr() {
       PageReference nextPage = submit();
        if (nextPage != NULL) {
            String unitId = nextPage.getUrl().substringAfterLast('/');
            nextPage = new PageReference(ApexPages.currentPage().getUrl());
            nextPage.getParameters().clear();
            nextPage.getParameters().put('view', 'CaseDetails');
            nextPage.getParameters().put('id', unitId);
            nextPage.setRedirect(true);
        }
        //System.debug('nextPage = ' + nextPage);
        //System.debug('nextPage Url = ' + nextPage.getUrl());
        return nextPage;
    }
}