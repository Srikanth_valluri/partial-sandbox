/*********************************************************************************************************
* Name               : StepStatusTransitionExtension
* Test Class         : StepStatusTransitionExtension_Test
* Description        : Extension class for Step Changes in SR
* Created Date       : 07/04/2019
* ----------------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR            DATE            COMMENTS
* 1.0                          07/04/2019      Initial Draft.
* 1.1         QBurst           28/09/2020      Added updation of Agent Site when VAT status is changed
**********************************************************************************************************/
public without sharing class StepStatusTransitionExtension extends QueuesDescribeSobjectAccess {
    
    public New_Step__c currentStep {get; set;}
    
    public String newStatusTransitionId {get; set;}
    
    public List<SelectOption> toStatusSelectOption {get; set;}
    
    Map<Id, Step_Status_Transition__c> stepStatusTransitionMap;
    
    public List<SelectOption> DpPaymentPercentageOptions {get; set;}
    public String selectedDpPaymentPercentage {get; set;}
    
    public StepStatusTransitionExtension(ApexPages.StandardController std) {
        sObjectAPIName = 'New_Step__c';
        recordID = std.getId();
        getAccess();
        
        initDpPaymentPercentage();
        
        stepStatusTransitionMap = new Map<Id, Step_Status_Transition__c>();
        
        toStatusSelectOption = new List<SelectOption>();
        currentStep = (New_Step__c) std.getRecord();
        newStatusTransitionId = '';
        
        List<String> UKDealList = new List<String>{'All'};
            if(currentStep.Service_Request__r.Is_UK_Deal__c){
                UKDealList.add('UK Only');
            }else{
                UKDealList.add('Non UK');
            }
        
        for (Step_Status_Transition__c transition : [SELECT Id, From_Step_Status__c, To_Step_Status__c, 
                                                     Step_Type__c, SR_External_Status__c, 
                                                     SR_Internal_Status__c, Is_Closed__c
                                                     FROM Step_Status_Transition__c 
                                                     WHERE Step_Type__c =: currentStep.Step_Type__c 
                                                     AND From_Step_Status__c =: currentStep.Step_Status__c
                                                     AND Step_No__c = :currentStep.Step_No__c
                                                     AND Is_VAT_SR__c = :currentStep.Service_Request__r.Is_VAT_SR__c
                                                     AND Service_Request_Type__c = :currentStep.Service_Request__r.RecordType.DeveloperName
                                                     AND UK_Deal__c IN: UKDealList
                                                    ]) 
        {
            stepStatusTransitionMap.put(transition.Id, transition);
            toStatusSelectOption.add(new SelectOption(transition.Id, transition.To_Step_Status__c));
        }
        //toStatusSelectOption.add(new SelectOption('--None--', '--None--'));
    }
    
    private void initDpPaymentPercentage(){
        Schema.DescribeFieldResult dpPaymentPercentage = NSIBPM__Service_Request__c.DP_Payment__c.getDescribe();
        List<Schema.PicklistEntry> dpPaymentPercentageValues = dpPaymentPercentage.getPicklistValues();
        
        selectedDpPaymentPercentage = '';
        DpPaymentPercentageOptions = new List<SelectOption>();
        
        SelectOption option = new SelectOption('', '-- None --');
        DpPaymentPercentageOptions.add(option);
        
        for(Schema.PicklistEntry entry : dpPaymentPercentageValues)
        {
            option = new SelectOption(entry.getValue(), entry.getLabel());
            DpPaymentPercentageOptions.add(option);
        }
    }
    
    public PageReference updateStatus() {
        
        PageReference returnPageReference = null;
        
        if (currentStep.Service_Request__r.NSIBPM__Required_Docs_not_Uploaded__c) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please upload all required documents first.'));
            return null;
        }
        
        try {
            
            Step_Status_Transition__c transition = stepStatusTransitionMap.get(newStatusTransitionId);
            currentStep.Step_Status__c = transition.To_Step_Status__c;
            currentStep.Is_Closed__c = transition.Is_Closed__c;
            
            NSIBPM__Service_Request__c serviceRequest = new NSIBPM__Service_Request__c();
            serviceRequest.Id = currentStep.Service_Request__c;
            if(String.isNotBlank(transition.SR_Internal_Status__c))
                serviceRequest.Internal_Status__c = transition.SR_Internal_Status__c;
            if(String.isNotBlank(transition.SR_External_Status__c))
                serviceRequest.External_Status__c = transition.SR_External_Status__c;
            if(String.isNotBlank(selectedDpPaymentPercentage)){
                serviceRequest.DP_Payment__c = selectedDpPaymentPercentage;              
            }
            
            if(canEditRecord){
                //check if user has permission to edit the step record
                if(currentStep.Step_Status__c == 'Approved' && currentStep.Step_Type__c == 'Agent Executive Review' ){ // 1.1
                    serviceRequest.Internal_Status__c = 'Approved';
                    serviceRequest.External_Status__c = 'Approved';
                }
                update serviceRequest;
                System.debug (currentStep);
                update currentStep;
                if(currentStep.Step_Status__c == 'Approved' && currentStep.Step_Type__c == 'Agent Executive Review'){ // 1.1
                    updateAgentSite();
                }
                returnPageReference = new PageReference('/' + currentStep.Service_Request__c);
            }else{
                //user doesn't have persmission to edit
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'You don\'t have rights to edit this Step'));
            }
        } catch (Exception ex) {
            System.debug('Exception --> ' + ex.getMessage());
            
            if (ex.getMessage().contains('INSUFFICIENT_ACCESS_OR_READONLY')) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'You don\'t have rights to edit this Step'));
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            }
            return null;
        }
        return returnPageReference;
    }
    
    public void updateAgentSite(){ // 1.1
        NSIBPM__Service_Request__c  sr = [select id, Name, Agency__c, VAT_Registration_Certificate_Date__c,
                                            UAE_Tax_Registration_Number__c, Country_of_Sale__c, NSIBPM__Customer__c 
                                          from NSIBPM__Service_Request__c where id =: currentStep.Service_Request__c];
        List<Agent_Site__c> updateAgencySiteList = new List<Agent_Site__c >();
        for( Agent_Site__c ajs :  [select id, Name, Tax_Registration_Number__c, Registration_Certificate_Date__c 
                                   from Agent_Site__c 
                                   where ( Agency__c =: sr.NSIBPM__Customer__c AND Name = 'UAE' AND Agency_ID__c != null )]){
            Agent_Site__c tmpAS = new Agent_Site__c ();
            tmpAS.id = ajs.id;
            tmpAS.Tax_Registration_Number__c = sr.UAE_Tax_Registration_Number__c ;
            tmpAS.Registration_Certificate_Date__c = sr.VAT_Registration_Certificate_Date__c;
            updateAgencySiteList.add(tmpAS);
        }
        if(updateAgencySiteList.size() > 0){
            update updateAgencySiteList;
        }
    }
    
    public PageReference cancelUpdate() {
        return new PageReference('/' + currentStep.Service_Request__c);
    }
}