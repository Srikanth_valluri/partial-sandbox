/******************************************************************************
* Description - Test class developed for CheckEligibleHandover
*
* Version            Date            Author                    Description
* 1.0                05/02/2018           						Initial Draft
********************************************************************************/
@isTest
private class CheckEligibleHandoverTest {
	static CheckEligibleHandover.EligibleHandover  obj = new CheckEligibleHandover.EligibleHandover();
	
	@isTest static void getTest(){

 
        obj.allowed  = 'Test';
        obj.message  = 'Test';
        obj.mortgageNOCfromBank  = 'Test';
        obj.ifPoaTakingHandoverColatePoaPassportResidence  = 'Test';
        obj.corporateValidTradeLicence  = 'Test';
        obj.corporateArticleMemorandumOfAssociation  = 'Test';
        obj.corporateBoardResolution  = 'Test';
        obj.corporatePoa  = 'Test';
        obj.signedForm  = 'Test';
        obj.clearAndValidPassportCopyOfOwner  = 'Test';
        obj.clearAndValidPassportCopyOfJointOwner  = 'Test';
        obj.visaOrEntryStampWithUid  = 'Test';
        obj.copyofValidEmiratesId  = 'Test';
        obj.copyofValidGccId  = 'Test';
        obj.handoverChecklistAndLod  = 'Test';
        obj.keyReleaseForm  = 'Test';
        obj.checkOriginalSpaAndtakeCopyOfFirstFourPagesOfSpa  = 'Test';
        obj.areaVariationAddendum  = 'Test';
        obj.tempOne  = 'Test';
        obj.tempTwo  = 'Test';
        obj.tempThree  = 'Test';
        obj.handoverNoticeAllowed  = 'Test';
        obj.approvalQueueOne  = 'Test';
        obj.approvalQueueTwo  = 'Test';
        obj.approvalQueueThree  = 'Test';
        obj.eligibleforRentalPool  = 'Test';


     
      Test.startTest();
      
      SOAPCalloutServiceMock.returnToMe = new Map<String,EligibleForHandoverUpdated.EligibleForHandoverNoticeResponse_element>();
        EligibleForHandoverUpdated.EligibleForHandoverNoticeResponse_element response1 = 
            new EligibleForHandoverUpdated.EligibleForHandoverNoticeResponse_element ();
            
        response1.return_x ='{"allowed":null,"message":"BCC not available","mortgageNOCfromB'+
        'ank":null,"ifPoaTakingHandoverColatePoaPassportResidence":null,"corporateValidTra'+
        'deLicence":null,"corporateArticleMemorandumOfAssociation":null,"corporateBoard'+
        'Resolution":null,"corporatePoa":null,"signedForm":null,"clearAndValidPassp'+
        'ortCopyOfOwner":null,"clearAndValidPassportCopyOfJointOwner":null,"visaOrE'+
        'ntryStampWithUid":null,"copyofValidEmiratesId":null,"copyofValidGccId":null,"handoverCh'+
        'ecklistAndLod":null,"keyReleaseForm":null,"checkOriginalSpaAndtakeCopyOfFirstFourP'+
        'agesOfSpa":null,"areaVariationAddendum":null,"tempOne":null,"tempTwo":null,"tem'+
        'pThree":null,"handoverNoticeAllowed":"NO","approvalQueueOne":null,"approvalQueu'+
        'eTwo":null,"approvalQueueThree":null,"eligibleforRentalPool":null}'; 
	        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
	        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
         obj = CheckEligibleHandover.getEligibleHandover('RegistrationId', 
      														    'processName',
      														    'subProcessName',
      														    'modeOfRequest',
         													    'noOfMajorSnagsInApartment',
         													    'accessPresent',
	         													'utilitiesAvailable',
	         													'percAptsSnagged',
	         													'EHOCase',
	         													'DaysToEarliestViewing' );
       Test.stopTest();


}
	/*static CheckEligibleHandover.EligibleHandover wrapperObj = new CheckEligibleHandover.EligibleHandover();
     static testMethod void unitTestMethod1(){
     
      Test.startTest();
      
      SOAPCalloutServiceMock.returnToMe = new Map<String,EligibleForHandoverUpdated.EligibleForHandoverNoticeResponse_element>();
        EligibleForHandoverUpdated.EligibleForHandoverNoticeResponse_element response1 = 
            new EligibleForHandoverUpdated.EligibleForHandoverNoticeResponse_element ();
            
        response1.return_x ='{"allowed":null,"message":"BCC not available","mortgageNOCfromB'+
        'ank":null,"ifPoaTakingHandoverColatePoaPassportResidence":null,"corporateValidTra'+
        'deLicence":null,"corporateArticleMemorandumOfAssociation":null,"corporateBoard'+
        'Resolution":null,"corporatePoa":null,"signedForm":null,"clearAndValidPassp'+
        'ortCopyOfOwner":null,"clearAndValidPassportCopyOfJointOwner":null,"visaOrE'+
        'ntryStampWithUid":null,"copyofValidEmiratesId":null,"copyofValidGccId":null,"handoverCh'+
        'ecklistAndLod":null,"keyReleaseForm":null,"checkOriginalSpaAndtakeCopyOfFirstFourP'+
        'agesOfSpa":null,"areaVariationAddendum":null,"tempOne":null,"tempTwo":null,"tem'+
        'pThree":null,"handoverNoticeAllowed":"NO","approvalQueueOne":null,"approvalQueu'+
        'eTwo":null,"approvalQueueThree":null,"eligibleforRentalPool":null}'; 
	        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
	        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
         wrapperObj = CheckEligibleHandover.getEligibleHandover('RegistrationId', 
      														    'processName',
      														    'subProcessName',
      														    'modeOfRequest',
         													    'noOfMajorSnagsInApartment',
         													    'accessPresent',
	         													'utilitiesAvailable',
	         													'percAptsSnagged',
	         													'EHOCase',
	         													'DaysToEarliestViewing' );
       Test.stopTest();
     }*/
      static testMethod void unitTestMethod2(){
     
      Test.startTest();
      
      SOAPCalloutServiceMock.returnToMe = new Map<String,EligibleForHandoverUpdated.EligibleForHandoverNoticeResponse_element>();
        EligibleForHandoverUpdated.EligibleForHandoverNoticeResponse_element response1 = 
            new EligibleForHandoverUpdated.EligibleForHandoverNoticeResponse_element ();
            
        response1.return_x =''; 
	        SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
	        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
         obj = CheckEligibleHandover.getEligibleHandover('RegistrationId', 
      														 'processName',
      														 'subProcessName',
      														 'modeOfRequest',
         													 'noOfMajorSnagsInApartment',
         													 'accessPresent',
         													  'utilitiesAvailable',
         													  'percAptsSnagged',
         													  'EHOCase',
         													  'DaysToEarliestViewing' );
       Test.stopTest();
     }
     /* static testMethod void unitTestMethod3(){
     	CheckEligibleHandover.EligibleHandover response = CheckEligibleHandover.getEligibleHandover( String.valueOf(bUnit.Registration_ID__c),'HANDOVER','','',majorSnags ,
                                                   'Yes','Yes',perApartSnagged ,'No','10');
     }*/
}