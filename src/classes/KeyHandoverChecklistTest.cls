/***********************************************************************************************
* Description - Test class developed for KeyHandoverChecklist
*
* Version            Date            Author                    Description
* 1.0                17/12/17        Naresh Kaneriya (Accely)   Initial Draft
*********************************************************************************************/

@isTest
public class KeyHandoverChecklistTest{

@isTest static void getTest(){

    KeyHandoverChecklist.KayHandover  obj = new KeyHandoverChecklist.KayHandover();
        obj.allowed  = 'Test';
        obj.message  = 'Test';
        obj.mortgageNOCfromBank  = 'Test';
        obj.ifPoaTakingHandoverColatePoaPassportResidence  = 'Test';
        obj.corporateValidTradeLicence  = 'Test';
        obj.corporateArticleMemorandumOfAssociation  = 'Test';
        obj.corporateBoardResolution  = 'Test';
        obj.corporatePoa  = 'Test';
        obj.signedForm  = 'Test';
        obj.clearAndValidPassportCopyOfOwner  = 'Test';
        obj.clearAndValidPassportCopyOfJointOwner  = 'Test';
        obj.visaOrEntryStampWithUid  = 'Test';
        obj.copyofValidEmiratesId  = 'Test';
        obj.copyofValidGccId  = 'Test';
        obj.handoverChecklistAndLod  = 'Test';
        obj.keyReleaseForm  = 'Test';
        obj.checkOriginalSpaAndtakeCopyOfFirstFourPagesOfSpa  = 'Test';
        obj.areaVariationAddendum  = 'Test';
        obj.tempOne  = 'Test';
        obj.tempTwo  = 'Test';
        obj.tempThree  = 'Test';
        obj.handoverNoticeAllowed  = 'Test';
        obj.approvalQueueOne  = 'Test';
        obj.approvalQueueTwo  = 'Test';
        obj.approvalQueueThree  = 'Test';
        obj.eligibleforRentalPool  = 'Test';


     Test.startTest();
      SOAPCalloutServiceMock.returnToMe = new Map<String,DocumentationForKeyHandoverService.DoumentationForKeyHandoverResponse_element>();
      DocumentationForKeyHandoverService.DoumentationForKeyHandoverResponse_element response1 = new DocumentationForKeyHandoverService.DoumentationForKeyHandoverResponse_element();
      response1.return_x = '       {'+
        '      "allowed":"Test",'+
        '        "message":"Test",'+
        '        "mortgageNOCfromBank":"Test",'+
        '        "ifPoaTakingHandoverColatePoaPassportResidence":"Test",'+
        '        "corporateValidTradeLicence":"Test",'+
        '        "corporateArticleMemorandumOfAssociation":"Test",'+
        '        "corporateBoardResolution":"Test",'+
        '        "corporatePoa":"Test",'+
        '        "signedForm":"Test",'+
        '        "clearAndValidPassportCopyOfOwner":"Test",'+
        '        "clearAndValidPassportCopyOfJointOwner":"Test",'+
        '        "visaOrEntryStampWithUid":"Test",'+
        '        "copyofValidEmiratesId":"Test",'+
        '        "copyofValidGccId":"Test",'+
        '        "handoverChecklistAndLod":"Test",'+
        '        "keyReleaseForm":"Test",'+
        '        "checkOriginalSpaAndtakeCopyOfFirstFourPagesOfSpa":"Test",'+
        '        "areaVariationAddendum":"Test",'+
        '        "tempOne":"Test",'+
        '        "tempTwo":"Test",'+
        '        "tempThree":"Test",'+
        '        "handoverNoticeAllowed":"Test",'+
        '        "approvalQueueOne":"Test",'+
        '        "approvalQueueTwo":"Test",'+
        '        "approvalQueueThree":"Test",'+
        '        "eligibleforRentalPool":"Test"'+
        '       '+
        '       }';
      SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
      Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
      KeyHandoverChecklist.getKeyHandover('regId','strSubProcessName','strProjectCity','strProject','strBuildingCode',
      'strPermittedUse','strBedroomType','strAppUnits','strUnitType','strNationality','strTypeOfCustomer','strPOA');
      Test.stopTest();



}

}