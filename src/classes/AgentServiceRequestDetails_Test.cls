@isTest
private class AgentServiceRequestDetails_Test {

    static testMethod void myUnitTest() {
        Test.startTest();
      NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
      SR.Delivery_mode__c='Email';
      SR.Deal_ID__c='1001';
      insert SR;
      
      update SR;
      List<NSIBPM__Service_Request__c > listSR = new List<NSIBPM__Service_Request__c >();
      listSR =[select Name
                 from NSIBPM__Service_Request__c 
                 where ID=: SR.ID];                
                 
       System.debug('...SR...'+listSR[0].Name);
       ApexPages.currentPage().getParameters().put('SRId',listSR[0].Name);
        Booking__c objB = new Booking__c();
        objB.Deal_SR__c = SR.ID;
        insert objB;
        
        Booking_Unit__c obj3 = new Booking_Unit__c();
        obj3.Booking__c = objB.ID;
        insert obj3;
        
        List<Booking_Unit__c> listB = new List<Booking_Unit__c>();
        listB  =[select id,
                        Booking__r.Deal_SR__r.Name
                 from Booking_Unit__c
                 where id=: obj3.ID];                
                 
        System.debug('...listB  ...'+listB);
        
        AgentServiceRequestDetailsController obj = new AgentServiceRequestDetailsController(); 
        obj.Back();
        AgentServiceRequestDetailsController.BookingData onj2 = new AgentServiceRequestDetailsController.BookingData();      
        onj2.Units ='test';
        onj2.RegistrationDate ='test';
        onj2.Price ='test';
        onj2.Area ='test';
        onj2.RequestedTokenamount ='test';
        onj2.TokenPaid ='test';
        onj2.TokenPaidTime ='test';
        onj2.TokenDueDate ='test';
        onj2.DPDueDate ='test';
        onj2.DPOverdue =true;
        onj2.DPOK =true;
        onj2.DOCOK =true;
        onj2.PaymentPlan ='test';
        onj2.PaymentPlanID ='test';
        onj2.SPA ='test';
        
        Test.stopTest();
    }
}