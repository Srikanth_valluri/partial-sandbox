@isTest(seeAllData=false) 
public class AP_ProfileControllerTest {
    public static testMethod void getlstRolesTest(){
        ID RecordTypeIdAGENT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        User eachUser = [SELECT ID FROM User WHERE UserRoleId != null LIMIT 1];
        Account a = new Account();
        a.recordtypeid=RecordTypeIdAGENT;
        a.Name = 'TestAccount';
        a.OwnerId = eachUser.Id;
        a.Agency_Short_Name__c = 'testShrName';
        insert a;

        Contact con = new Contact();
        con.LastName = 'testlnme';
        con.AccountId = a.Id ;
        con.Agent_Representative__c=true;
        insert con;

        Agent_Site__c agentSIte = new Agent_Site__c();
        agentSIte.Start_Date__c=system.today();
        agentSIte.End_Date__c = system.today();
        agentSIte.Agency__c = a.id;
        agentSIte.Tax_Registration_Number__c='123';
        agentSIte.Registration_Certificate_Date__c = System.today();
        agentSIte.Name = 'UAE';
        insert agentSIte;

        profile profId = [SELECT Id FROM Profile WHERE Name='Standard User']; // query on Profile to fetch ProfileId
        String orgId = UserInfo.getOrganizationId();
        Profile p = [select Id from Profile where name LIKE '%Customer Community - Admin%'];
         //user 
        User portalUser = new User(alias = 'test456', email='testusr1@test.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.id, country='United Arab Emirates',IsActive =true,
                ContactId = con.Id,timezonesidkey='America/Los_Angeles', username='testusr1@test.com');

        Test.startTest();
        System.runAs(portalUser){
            AP_ProfileController obj = new AP_ProfileController();
            List<SelectOption> options = obj.getlstRoles();
            obj.getUserPhotoUrl();
        }
        Test.stopTest();
    }

    public static testMethod void clearFieldsTest(){
        Test.startTest();
        AP_ProfileController.clearFields();
        Test.stopTest();
    }

    public static testMethod void updatePasswordTest(){
        Test.startTest();
        String options = AP_ProfileController.updatePassword('123','123','123');
        Test.stopTest();
    }

    public static testMethod void updatePasswordSameTest(){
        Test.startTest();
        String options = AP_ProfileController.updatePassword('123','123','123');
        Test.stopTest();
    }

    public static testMethod void updatePasswordBlankTest(){
        Test.startTest();
        String options = AP_ProfileController.updatePassword('123','','');
        Test.stopTest();
    }

    public static testMethod void getPortalRolesTest(){
        ID RecordTypeIdAGENT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        User eachUser = [SELECT ID FROM User WHERE UserRoleId != null LIMIT 1];
        Account a = new Account();
        a.recordtypeid=RecordTypeIdAGENT;
        a.Name = 'TestAccount';
        a.OwnerId = eachUser.Id;
        a.Agency_Short_Name__c = 'testShrName';
        insert a;

        Contact con = new Contact();
        con.LastName = 'testlnme';
        con.AccountId = a.Id ;
        con.Portal_Administrator__c=true;
        insert con;

        Agent_Site__c agentSIte = new Agent_Site__c(Start_Date__c=system.today(),End_Date__c = system.today(),Agency__c = a.id, Tax_Registration_Number__c='123',Registration_Certificate_Date__c = System.today());
        insert agentSIte;

        profile profId = [SELECT Id FROM Profile WHERE Name='Standard User']; // query on Profile to fetch ProfileId
        String orgId = UserInfo.getOrganizationId();
        Profile p = [select Id from Profile where name LIKE '%Customer Community - Admin%'];
         //user 
        User portalUser = new User(alias = 'test456', email='testusr1@test.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.id, country='United Arab Emirates',IsActive =true,
                ContactId = con.Id,timezonesidkey='America/Los_Angeles', username='testusr1@test.com');

        Test.startTest();
        System.runAs(portalUser){
            AP_ProfileController obj = new AP_ProfileController();
            obj.selectedRole = 'Portal_Administrator__c';
            obj.init();
        }
        Test.stopTest();
    }

    public static testMethod void getAgentRolesTest(){
        ID RecordTypeIdAGENT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        User eachUser = [SELECT ID FROM User WHERE UserRoleId != null LIMIT 1];
        Account a = new Account();
        a.recordtypeid=RecordTypeIdAGENT;
        a.Name = 'TestAccount';
        a.OwnerId = eachUSer.Id;
        a.Agency_Short_Name__c = 'testShrName';
        insert a;

        Contact con = new Contact();
        con.LastName = 'testlnme';
        con.AccountId = a.Id ;
        con.Agent_Representative__c=true;
        insert con;

        Agent_Site__c agentSIte = new Agent_Site__c();
        agentSIte.Start_Date__c=system.today();
        agentSIte.End_Date__c = system.today();
        agentSIte.Agency__c = a.id;
        agentSIte.Tax_Registration_Number__c='123';
        agentSIte.Registration_Certificate_Date__c = System.today();
        agentSIte.Name = 'UAE';
        insert agentSIte;

        profile profId = [SELECT Id FROM Profile WHERE Name='Standard User']; // query on Profile to fetch ProfileId
        String orgId = UserInfo.getOrganizationId();
        Profile p = [select Id from Profile where name LIKE '%Customer Community - Admin%'];
         //user 
        User portalUser = new User(alias = 'test456', email='testusr1@test.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.id, country='United Arab Emirates',IsActive =true,
                ContactId = con.Id,timezonesidkey='America/Los_Angeles', username='testusr1@test.com');

        Test.startTest();
        System.runAs(portalUser){
            AP_ProfileController obj = new AP_ProfileController();
            obj.selectedRole = 'Agent_Representative__c';
            obj.init();
        }
        Test.stopTest();
    }
    public static testMethod void getAuthorisedRolesTest(){
        ID RecordTypeIdAGENT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        User eachUser = [SELECT ID FROM User WHERE UserRoleId != null LIMIT 1];
        Account a = new Account();
        a.recordtypeid=RecordTypeIdAGENT;
        a.Name = 'TestAccount';
        a.OwnerId = eachUser.Id;
        a.Agency_Short_Name__c = 'testShrName';
        insert a;

        Contact con = new Contact();
        con.LastName = 'testlnme';
        con.AccountId = a.Id ;
        con.Authorised_Signatory__c=true;
        insert con;

        Agent_Site__c agentSIte = new Agent_Site__c();
        agentSIte.Start_Date__c=system.today();
        agentSIte.End_Date__c = system.today();
        agentSIte.Agency__c = a.id;
        agentSIte.Tax_Registration_Number__c='123';
        agentSIte.Registration_Certificate_Date__c = System.today();
        agentSIte.Name = 'UAE';
        insert agentSIte;

        profile profId = [SELECT Id FROM Profile WHERE Name='Standard User']; // query on Profile to fetch ProfileId
        String orgId = UserInfo.getOrganizationId();
        Profile p = [select Id from Profile where name LIKE '%Customer Community - Admin%'];
         //user 

        //Id roleId = [SELECT Id FROM UserRole WHERE portalType = 'none' LIMIT 1].Id;
        User portalUser = new User(alias = 'test456', email='testusr1@test.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.id, country='United Arab Emirates',IsActive =true,
                ContactId = con.Id,timezonesidkey='America/Los_Angeles', username='testusr1@test.com');
        Test.startTest();
        System.runAs(portalUser){
            AP_ProfileController obj = new AP_ProfileController();
            obj.selectedRole = 'Authorised_Signatory__c';
            obj.init();
        }
        Test.stopTest();
    }

    public static testMethod void getOwnerRolesTest(){
        ID RecordTypeIdAGENT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        ID srRecordId = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Agent Registration').getRecordTypeId();
        User eachUser = [SELECT ID FROM User WHERE UserRoleId != null LIMIT 1];
        
        Account a = new Account();
        a.recordtypeid=RecordTypeIdAGENT;
        a.Name = 'TestAccount';
        a.OwnerId = eachUser.Id;
        a.Agency_Short_Name__c = 'testShrName';
        insert a;

        Contact con = new Contact();
        con.LastName = 'testlnme';
        con.AccountId = a.Id ;
        con.Owner__c=true;
        insert con;

        Agent_Site__c agentSIte = new Agent_Site__c();
        agentSIte.Start_Date__c=system.today();
        agentSIte.End_Date__c = system.today();
        agentSIte.Agency__c = a.id;
        agentSIte.Tax_Registration_Number__c='123';
        agentSIte.Registration_Certificate_Date__c = System.today();
        agentSIte.Name = 'UAE';
        insert agentSIte;

        profile profId = [SELECT Id FROM Profile WHERE Name='Standard User']; // query on Profile to fetch ProfileId
        String orgId = UserInfo.getOrganizationId();
        Profile p = [select Id from Profile where name LIKE '%Customer Community - Admin%'];
         //user 
        User portalUser = new User(alias = 'test456', email='testusr1@test.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.id, country='United Arab Emirates',IsActive =true,
                ContactId = con.Id,timezonesidkey='America/Los_Angeles', username='testusr1@test.com');
        
        Test.startTest();
        System.runAs(portalUser){
            NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
            sr.NSIBPM__Customer__c = a.Id;
            sr.recordTypeId = srRecordId;
            sr.Country_of_sale__c = 'UAE';
            insert sr;
            
            AP_ProfileController obj = new AP_ProfileController();
            AP_ProfileController.createAgentSR (a.id);
            AP_ProfileController.checkCompanySR (a.Id);
            AP_ProfileController.checkEditSR(a.Id);
            obj.selectedRole = 'Owner__c';
            obj.init();
        }
        Test.stopTest();
    }
}