public with sharing class MoveOutModifySRController {
	public MoveOutModifySRController(ApexPages.StandardController stdController) { }

	public PageReference modifySR(){
		String FMCaseId = ApexPages.currentPage().getParameters().get('id');
        //Recalling existing approval process and cancelling tasks
        List<FM_Case__c> lstCases = new List<FM_Case__c>();
        lstCases = cancelSRHelper(FMCaseId);
        //Submitting for new approval process
        if(lstCases.size() > 0) {
            list<FM_Approver__mdt> lstApprovers = FM_Utility.fetchApprovers(lstCases[0].Request_Type_DeveloperName__c,
                        lstCases[0].Booking_Unit__r.Property_City__c);
            System.debug('==lstApprovers==' + lstApprovers);
            String approvingRoles = '';
            for(FM_Approver__mdt mdt : lstApprovers) {
                approvingRoles += mdt.Role__c + ',';
            }
            approvingRoles = approvingRoles.removeEnd(',');
            lstCases[0].Approving_Authorities__c = approvingRoles;
            lstCases[0].Status__c = 'Submitted';
            lstCases[0].Approval_Status__c='Pending';
            lstCases[0].Submit_for_Approval__c = true;
            lstCases[0].Submitted__c = true;
            update lstCases;
			return new PageReference('/' + lstCases[0].Id);
        }
		return NULL;
    }

	public static List<FM_Case__c> cancelSRHelper(String FMCaseId){
        List<FM_Case__c> lstCases = new List<FM_Case__c>();
        try {
            //Recalling approval process
            lstCases = [SELECT Status__c, Id,
                                Request_Type_DeveloperName__c,
                                Booking_Unit__r.Property_City__c
                        FROM FM_Case__c WHERE Id=: FMCaseId];
            if(lstCases.size() > 0) {
                lstCases[0].Status__c = 'Cancelled';
                lstCases[0].Approval_Status__c='';
                lstCases[0].Submit_for_Approval__c = false;
                lstCases[0].Submitted__c = false;
            }
            List<ProcessInstanceWorkitem> piwi = [SELECT Id, ProcessInstanceId,
                                                        ProcessInstance.TargetObjectId
                                                    FROM ProcessInstanceWorkitem
                                                    WHERE ProcessInstance.TargetObjectId =: FMCaseId];
            System.debug('Target Object ID:' +piwi.size());
            if(piwi.size() > 0) {
                Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                req.setComments('Recalling request');
                req.setAction('Removed');
                req.setWorkitemId(piwi.get(0).Id);
                req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                Approval.ProcessResult result = Approval.process(req,false);
                System.debug('===result==' +result);
            }

            //Cancel tasks
            List<Task> lstTasks = new List<Task>();
            lstTasks = [SELECT Id, Status from Task WHERE WhatID =: FMCaseId];
            for(Task objTask : lstTasks) {
                objTask.Status = 'Cancelled';
            }
            update lstTasks;
        }
        catch(Exception excp) {
            System.debug('===excp==' +excp);
        }
        return lstCases;
    }
}