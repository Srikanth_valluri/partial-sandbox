@isTest
public class ReraPercentUpdUnitLvlTest {
    @isTest
    static void testReraPercentUpdUnit(){
        Property__c objProperty = new Property__c();
        objProperty.Property_ID__c = 12345;
        objProperty.Property_Code__c = 'PARAMOUNT TOWER HOTEL & RESIDENCES';
        insert objProperty;
        
        Inventory__c objInventory = new Inventory__c();
        objInventory.Property__c = objProperty.Id;
        //objInventory.Building__c = objLocation.Id;
        insert objInventory;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        //objBooking.Account__c = objAccount.Id; 
        insert objBooking;
        
        List<Booking_Unit__c> lstBU = new List<Booking_Unit__c>();
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Registration_ID__c = '3901';
        objBU.Unit_Name__c = 'test/test';
        objBU.Anticipated_Completion_Date__c = System.today();
        objBU.Booking__c = objBooking.Id;
        objBU.Inventory__c = objInventory.Id;
        objBU.Recovery__c = 'A';
        insert objBU;
        lstBU.add(objBU);
        
        Test.startTest();
        ReraPercentUpdUnitLvl.ReraPercentUpdUnitLvl('PARAMOUNT TOWER HOTEL & RESIDENCES');
        Test.stopTest();
        
    }
}