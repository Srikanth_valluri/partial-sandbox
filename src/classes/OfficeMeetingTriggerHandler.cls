public without sharing class OfficeMeetingTriggerHandler {

    public OfficeMeetingTriggerHandler(){

    }

    public void UpdateRMMeetingTask(List<Office_Meeting__c> officeMeetingList){
        System.debug('OfficeMeetingTriggerHandler>>>>>>>>>>');
        List<Id> inquiyIdList = new List<Id>();
        List<String> taskIdList = new List<String>();
        List <ID> noShowInqIds = new List <ID> ();
        Map <ID, Office_Meeting__c> preInqToConvertIds = new Map <ID, Office_Meeting__c> ();
        Map <ID, ID> ownerIds = new Map <Id, ID> ();
        for(Office_Meeting__c meeting : officeMeetingList) {
            if (meeting.Recommended_RM__c != NULL) {
                preInqToConvertIds.put (meeting.Inquiry__c, meeting);
            }
            if (meeting.Status__c == 'Completed'
                && meeting.Outcome__c == 'Show'
                && meeting.Bypass_Validation__c == false
            ) {
                inquiyIdList.add(meeting.Inquiry__c);
                taskIdList.add(meeting.Related_Task__c);
            }
            
            if (meeting.Outcome__c != 'Show' ) {
                //noShowInqIds.add (meeting.Inquiry__c);
            }
        }
        if (preInqToConvertIds.size () > 0) {
            List <Inquiry__c> inqToUpdate = new List <Inquiry__c> ();
            
            for (Inquiry__c inq : [SELECT OwnerId, Is_Meeting_Confirmed__c FROM Inquiry__c 
                    WHERE ID IN: preInqToConvertIds.keySet ()
                    AND RecordType.name = 'Inquiry']) {
                inq.Old_Owner_id__c = inq.OwnerId;
                inq.OwnerId = preInqToConvertIds.get (inq.ID).Recommended_RM__c;
                ownerIds.put (inq.ID, preInqToConvertIds.get (inq.ID).Recommended_RM__c);
                inqToUpdate.add (inq);
            }
            for (Inquiry__c inq : [SELECT Is_Meeting_Confirmed__c FROM Inquiry__c 
                    WHERE ID IN: preInqToConvertIds.keySet ()
                    AND RecordType.name = 'Pre Inquiry']) {
                if (preInqToConvertIds.get (inq.ID).outcome__c == 'Show') {
                    inq.Assigned_PC__c = preInqToConvertIds.get (inq.ID).Recommended_RM__c;
                    
                    inq.Inquiry_Status__c = 'Meeting Scheduled';
                    inqToUpdate.add (inq);
                }
            }
            if (!Test.isRunningTest()) {
                if (ownerIds.size () > 0)
                    DAMAC_Constants.skip_InquiryTrigger = true;
                update inqToUpdate;
            }
        }
        
        
        List<Task> taskList = new List<Task>();
        taskList = [SELECT Show__c, OwnerId, Activity_Type_3__c, whatId, Status
                      FROM Task 
                     WHERE Id IN :taskIdList 
                       AND WhatId IN :inquiyIdList AND Activity_Type_3__c = 'Meeting at Office'];
        for (Task meetingTask : taskList) {
                meetingTask.Show__c = true;
            
        }
        Map<Id, List<Task>> preInquiryActivities = getPreInquiryActivities(OwnerIds.keySet());
        
        List<String> meetingTypeList = new List<String>(); // 1.9
        meetingTypeList = Label.Activity_Type_for_Owner_Change.split(','); // 1.9
            
        
        for (Id inqId :preInquiryActivities.keySet ()) {
            for (Task t :preInquiryActivities.get (inqId)) {
                if (t.Status == 'Not Started') {
                    if (meetingTypeList.contains(t.Activity_Type_3__c)) {
                        t.OwnerId = ownerIds.get (inqId);
                        taskList.add(t);
                    }
                }
            }
        }
        if (taskList.size () > 0)
            update taskList;
        
    }
 
    public Map<Id, List<Task>> getPreInquiryActivities(Set<ID> preInquires) {
        Map<Id, List<Task>> preInquiryActivities = new Map<Id, List<Task>> ();
        if (preInquires != null && !preInquires.isEmpty()) {
            for (Task td :[SELECT
                 WhatId, Subject, Status, Activity_Type_3__c
                 FROM Task
                 WHERE WhatId In :preInquires]) {
                if (!preInquiryActivities.containsKey(td.WhatId)) {
                    preInquiryActivities.put(td.WhatId, new List<Task> { });
                }
                preInquiryActivities.get(td.WhatId).add(td);
            }
        }
        return preInquiryActivities;
    }

}