/**
 * Ver       Date            Author                 Modification
 * 1.0    10/3/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version  
**/
public without sharing class HandoverKeyReleaseFormExtension {
    
    Transient public String keyDocName { get; set; }
    Transient public String keyDocBody { get; set; }
    Transient public String keyDocNamePassport { get; set; }
    Transient public String keyDocBodyPassport { get; set; }
    public SR_Attachments__c objKeyAttach { get; set; }
    public Booking_Unit__c selectedUnit { get; set; }
    public Boolean isClientAckFormGen {get;set;}
    public Boolean isError {get;set;}
    public Boolean isSaveAsDraft {get;set;}

    public Integer checkForBothVariable; 
            
    @testVisible private Id selectedUnitId ;
    @testVisible private Boolean isSuccessfull ;
    @testVisible private static final String KEYDOCUMENT = 'Key Release Form' ;
    
    public HandoverKeyReleaseFormExtension( ApexPages.StandardController controller ) {
        selectedUnitId = controller.getId();
        if( String.isNotBlank( selectedUnitId ) ) {
            isSuccessfull = false;
            isSaveAsDraft = false;
            isClientAckFormGen = false;
            isError = false;
            checkForBothVariable = 0;
            init();   
        }
    }
    
    public void init() {
        selectedUnit = [ SELECT Id,Unit_Name__c,Primary_Buyer_s_Name__c,Value__c,Is_Client_Acknowledgment_Form_Generated__c
                        , Authorized_Personnel_Name__c
                        , Authorized_Personnel_Phone_Number__c
                        , Authorized_Personnel_Email__c
                        , Authorized_Personnel_Nationality__c
                        , Authorized_Personnel_Passport_Number__c
                        , Registration_Id__c,Main_door_card_number_Quantity__c,Main_door_card_number_Recieved__c
                        , Name,Parking_Access_Cards_Quantity__c,Parking_Access_Cards_Recieved__c
                        , Keys_Released__c,Parking_access_card_no_Quantity__c,Parking_access_card_no_Recevied__c
                        , Inventory__r.Building_Location__r.FM_Email__c
                        , Inventory__r.Building_Location__r.Loams_Email__c
                        , Main_door_keys_Quantity__c,Building_Access_Quantity__c,Building_Access_Recieved__c
                        , Main_door_keys_Received__c,Building_access_card_no_Quantity__c,Building_access_card_no_Recieved__c
                        , Internal_door_keys_Quantity__c
                        , Internal_door_keys_Received__c
                        , Appliances_manuals_Quantity__c
                        , Appliances_manuals_Received__c
                        , HO_Pack_with_Home_Owner_s_Manual_Quantiy__c
                        , HO_Pack_with_Home_Owners_Manual_Received__c
                        , SRF_Signed_off_Quantity__c
                        , SRF_Signed_off_Received__c
                        , Key_Release_Date__c
                        , Okay_to_release_keys__c
                        , OK_to_release_Keys_LHO__c
                        , Handover_Executive_Name__c
                        , Snag_Rectification__c
                        , Painting_Cleaning__c
                        , Number_of_Keys_Provided__c
                        FROM Booking_Unit__c
                        WHERE Id = :selectedUnitId ];
                        
        list<SR_Attachments__c> lstAttach = [ SELECT Id
                                                   , Name
                                                   , Type__c
                                                   , isValid__c
                                                   , Attachment_URL__c
                                                FROM SR_Attachments__c
                                               WHERE Booking_Unit__c = :selectedUnitId
                                                 AND Type__c = :KEYDOCUMENT
                                            ORDER BY CreatedDate DESC
                                               LIMIT 1];
        if( lstAttach != NULL && !lstAttach.isEmpty() ) {
            //objKeyAttach = lstAttach[0];
        }
    }
    
    
    public PageReference generateDoc() {
        try {
            update selectedUnit ;
            if( selectedUnit.Okay_to_release_keys__c || selectedUnit.OK_to_release_Keys_LHO__c) {
                 ApexPages.addmessage(
                     new ApexPages.message(
                     ApexPages.severity.CONFIRM,'Please wait for 5 minutes, system is generating Client Acknowledgment Letter'));
                if (!Test.isRunningTest()) {Database.executeBatch(new GenerateBUDrawloopDocumentBatch(String.valueOf(selectedUnit.Id), Label.Client_Acknowledgment_Form_DDP_Template_Id,Label.Client_Acknowledgment_Form_DDP_Delivery_Id )); isClientAckFormGen = true;
                }
               
            }else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'You can not generate Client Acknowledgment Letter, as this unit is not marked as either okay to key release or Key Released LHO'));
                isError = true;
            }
        }
        catch( Exception e ) { ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR, e.getMessage() ) );
        }
        return null;
    }

    public PageReference uploadFileToServer() {
        if(String.isNotBlank( keyDocName ) || 
           	String.isNotBlank( keyDocNamePassport )){
            System.debug('-->> first if  inside' );
            UploadMultipleDocController.data objResponse = uploadDoc();
            system.debug('==objResponse=='+objResponse);
            if( objResponse != null ) {
                if( String.isNotBlank( objResponse.status ) && objResponse.status.equalsIgnoreCase('S')
                    && objResponse.data != null && objResponse.data.size() > 0  ) {
                    insertCustomAttachment( objResponse );
                    System.debug('-->> checkForBothVariable: ' + checkForBothVariable);
                    if(checkForBothVariable == 2 ){
                        isSuccessfull = true ; isSaveAsDraft = true;
                    }
                    System.debug('==Inside Save==' + isSaveAsDraft);
                }
                else { 
                    isSuccessfull = false ;
                    isSaveAsDraft = false;
                    //Generate Error Log
                    Error_Log__c objErr = new Error_Log__c();
                    objErr.Booking_Unit__c = selectedUnit.Id;
                    if( objResponse.data != null && 
                       		!objResponse.data.isEmpty()) {
                        objErr.Error_Details__c = objResponse.message + '---' + objResponse.data[0].PROC_MESSAGE ;
                    }else{
                        objErr.Error_Details__c = objResponse.message ;
                    }
                    if( !Test.isRunningTest()){insert objErr ;}
                    ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR, 'Documents were not successfully Uploaded.' ) );
                }
            }
        }
        return null ;
    }
    
    public PageReference saveDraft() {
        System.debug('-->> Save as draft inside' + isSaveAsDraft);
        
        try {
            update selectedUnit ; 
            PageReference objPage; 
            System.debug('-->> isSaveAsDraft: ' + isSaveAsDraft);
            return objPage ; 
        }
        catch( Exception e ) { ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR, e.getMessage() ) );
        }

        return null ;
    }
    
    public PageReference save() {
        if(selectedUnit.Key_Release_Date__c == NULL){
            ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR, 'Please fill the Key release date' ) );
            return null ;
        } else {
            if( isSuccessfull ) {
                try {
                    selectedUnit.Keys_Released__c = true ;
                    selectedUnit.FM_Email__c = selectedUnit.Inventory__r.Building_Location__r.FM_Email__c ;
                    selectedUnit.Loams_Email__c= selectedUnit.Inventory__r.Building_Location__r.Loams_Email__c;
                    //selectedUnit.Key_Release_Date__c = system.today();
                    update selectedUnit ; 
                    System.debug('-->> selectedUnit:'+selectedUnit);
                    PageReference objPage = new PageReference('/'+selectedUnit.Id);
                    objPage.setRedirect( true );
                    return objPage ; 
                }
                catch( Exception e ) {ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR, e.getMessage() ) );
                }
            }
            return null ;
        }
    }
    
    public PageReference back() {
        PageReference objPage = new PageReference('/'+selectedUnit.Id);
        objPage.setRedirect( true );
        return objPage ; 
    }
    
    @testVisible 
    private UploadMultipleDocController.data uploadDoc() {      
        
        //system.debug('==keyDocBody='+keyDocBody);
        system.debug('==keyDocName='+keyDocName);
        
        
        UploadMultipleDocController.data objResponse = new UploadMultipleDocController.data();
        list<UploadMultipleDocController.MultipleDocRequest> lstWrapper = new list<UploadMultipleDocController.MultipleDocRequest>();
        if( String.isNotBlank( keyDocBody ) && 
           String.isNotBlank( keyDocName ) && 
           objKeyAttach == null) {
            lstWrapper.add( PenaltyWaiverHelper.makeWrapperObject( EncodingUtil.Base64Encode( extractBody( keyDocBody ) ) ,
                                                                   'Key Release Form' ,
                                                                   extractName( keyDocName ) ,
                                                                   selectedUnit.Registration_Id__c, selectedUnit.Name, '1' ) );
                
        }
        //System.debug('-->> lstWrapper 1: '+ lstWrapper);
        //System.debug('-->> keyDocBodyPassport: '+keyDocBodyPassport);
        System.debug('-->> keyDocNamePassport: '+keyDocNamePassport);
        if( String.isNotBlank( keyDocBodyPassport ) && 
           String.isNotBlank( keyDocNamePassport )) {
            System.debug('-->> Inside 2: ');
            lstWrapper.add( PenaltyWaiverHelper.makeWrapperObject( EncodingUtil.Base64Encode( extractBody( keyDocBodyPassport ) ) ,
                                                                   'Passport Document' ,
                                                                   extractName( keyDocNamePassport ) ,
                                                                   selectedUnit.Registration_Id__c, selectedUnit.Name, '1' ) );
                
        }
        System.debug('-->> lstWrapper 2: '+ lstWrapper);
        if( !lstWrapper.isEmpty() ) {
            system.debug('==lstWrapper='+lstWrapper);
            objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo( lstWrapper );
        }
        else {
            //objResponse = null ;
        }
        System.debug('-->> objResponse 2: '+ objResponse);
        return objResponse;
    }
    
    @testVisible 
    private void insertCustomAttachment( UploadMultipleDocController.data objResponse ) {
        list<SR_Attachments__c> lstCustomAttachments = new list<SR_Attachments__c>();
        for( UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
            
            SR_Attachments__c objCustAttach = new SR_Attachments__c();
            objCustAttach.Attachment_URL__c = objFile.url;
            objCustAttach.Booking_Unit__c = selectedunit.Id ;
            objCustAttach.Name = 'Key Release Form - Handover team' ;
            if( String.isNotBlank( objFile.PARAM_ID ) && 
                String.isNotBlank( extractName( keyDocName ) ) && 
                objFile.PARAM_ID.contains( extractName( keyDocName ) ) ) {objCustAttach.Type__c = KEYDOCUMENT ;                objCustAttach.isValid__c = true ;
                }
            else {
                System.debug('-->> Inside Else 2: ');
                objCustAttach.Type__c = 'Other';
                objCustAttach.Name = 'Passport Doc' ;
                System.debug('-->> objCustAttach inside : ' + objCustAttach);
            }
            System.debug('-->> objCustAttach 2 : ' + objCustAttach);
            lstCustomAttachments.add( objCustAttach );  
        }
        System.debug('-->> lstCustomAttachments inside : ' + lstCustomAttachments);
        if( !lstCustomAttachments.isEmpty() ) { 
            insert lstCustomAttachments ;
            checkForBothVariable += 1;
            keyDocName = '';
            keyDocBody = '';
            keyDocNamePassport = '';
            keyDocBodyPassport = '';
        }
    }
    
    public static String extractName( String strName ) { 
        return strName.substring( strName.lastIndexOf('\\')+1 ) ;
    }
    
    public static Blob extractBody( String strBody ) {
        strBody = EncodingUtil.base64Decode( strBody ).toString();
        return EncodingUtil.base64Decode( strBody.substring( strBody.lastIndexOf(',')+1 ) );
    }
}