public without sharing class MortgageTaskClosureValidation{
    public void beforeUpdate(map<Id,Task> newMap, map<Id,Task> oldMap){
        set<Id> setCaseIds = new set<Id>();
        for(Task objT : newMap.values()){
            if(objT.Status.equalsIgnoreCase('Completed')
            && objT.Status != oldMap.get(objT.Id).Status
            && objT.Subject == Label.Mortgage_10
            && string.valueOf(objT.WhatId).startsWith('500') ){
                setCaseIds.add(objT.WhatId);
            }
        }
        system.debug('setCaseIds**********'+setCaseIds);
        if(!setCaseIds.isEmpty()){
            map<Id,Id> mapCase_CaseAndBookingUnit = new map<Id,Id>();
            for(Case objC : [Select Id
                                  , RecordTypeId
                                  , RecordType.DeveloperName
                                  , Booking_Unit__c
                             from Case 
                             where Id IN : setCaseIds 
                             and RecordType.DeveloperName = 'Mortgage'
                             ]){
                if(objC.Booking_Unit__c != null){
                    mapCase_CaseAndBookingUnit.put(objC.Id,objC.Booking_Unit__c);
                }
            } // end of for loop
            
            system.debug('mapCase_CaseAndBookingUnit*****'+mapCase_CaseAndBookingUnit);
            for(Task objT : newMap.values()){
                if(objT.Status.equalsIgnoreCase('Completed')
                && objT.Status != oldMap.get(objT.Id).Status
                && objT.Subject == Label.Mortgage_10
                && string.valueOf(objT.WhatId).startsWith('500') ){
                    if(!mapCase_CaseAndBookingUnit.containsKey(objT.WhatId)){
                        system.debug('Error tak aya*********');
                        objT.addError('Error : This task cannot be closed unless a valid unit updated on the Case');
                    }
                }
            } // end of for loop
        } //end of setCaseIds not empty
    } // end of beforeUpdate
}