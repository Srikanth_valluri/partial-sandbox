public with sharing class TranslationSupportController {

    //public String staticResource{get;set;}
    
    /*public  translationSupportController ()
    {
       StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'LanguageTranslator' LIMIT 1];
       staticResource = sr.Body.toString();                     
    }*/

    @RemoteAction
    public static String getInit(){
        StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'LanguageTranslator' LIMIT 1];
        String body = sr.Body.toString();  
        system.debug('body>>>>' + body);          
        return body;
    }
}