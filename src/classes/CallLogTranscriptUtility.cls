public with sharing class CallLogTranscriptUtility{
    
    public static void sendCallOutData(Set<Id> setCallId) {
        HttpRequest req = new HttpRequest();
        req.setHeader('Content-Type','application/json');
        String endpoint = System.Label.VoiceraPHPEndPoint;
        req.setMethod('POST');
        req.setEndpoint(endpoint);
        req.setTimeout(60000); 
        system.debug('setCallId----' +setCallId);
        //SOQL to construct JSON string in set body
        List<Call_Log__c> listCallLog = [Select Id,
                                                Call_Recording_URL__c
                                         From Call_Log__c 
                                         Where id IN :setCallId AND Call_Type__c != 'Outbound Attempted'];
      
        SendJsonData objSendJsonData = new SendJsonData(listCallLog[0].Call_Recording_URL__c, listCallLog[0].Id);
        system.debug('JSON ----'+JSON.serialize(objSendJsonData));
        String JsonString=JSON.serialize(objSendJsonData);
        req.setBody(JsonString);
        system.debug('-->> json string: '+ JsonString);
        Http objhttp = new Http();
        try {
        HTTPResponse res = new HTTPResponse();
        if ( !Test.isRunningTest()) {
           res = objhttp.send(req);
        }
        System.debug('-->> res. body:  '+ res.getBody());
        System.debug('-->> res. toString:  '+ res.toString());
        } catch(System.CalloutException e){
            System.debug('-->> CalloutException Error :  '+ e);
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'This is error:/n ' + e));
            Error_Log__c objErr = new Error_Log__c();
            objErr.Error_Details__c = '' + 'This is error:/n ' + e;
            insert objErr;
            System.debug('-->> objErr: '+ objErr);
        }
    }
    
    public class SendJsonData{
    
        String URL;
        String CALL_ID;

        public SendJsonData(String URL, String CALL_ID){
            this.URL = URL;
            this.CALL_ID = CALL_ID;
        }
    }

}