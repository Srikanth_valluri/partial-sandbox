@isTest
public with sharing class MollakServiceTest {
    

    @isTest
    public static void testGetCases() {
        Account objAcc = new Account( Name = 'Test account'
                                        , Email__c = 'rwqerwe@dfsf.com'
                                        , First_Name__c = 'Test'
                                        , Last_Name__c = 'Test 1'
                                        , Mobile__c = '9405883798' 
                                        , First_Name_Arabic__c = 'abc'
                                        , Middle_Name_Arabic__c = 'abc'
                                        , Last_Name_Arabic__c = 'abc'
                                        , Passport_Number__c = 'abc'
                                    );
        insert objAcc;


        Account objAcc1 = new Account( Name = 'Test account2'
                                        , Email__c = 'rwqerwe@dfsf.com'
                                        , First_Name__c = 'Test'
                                        , Last_Name__c = 'Test 1'
                                        , Mobile__c = '9405883798' 
                                        , First_Name_Arabic__c = 'abc'
                                        , Middle_Name_Arabic__c = 'abc'
                                        , Last_Name_Arabic__c = 'abc'
                                        , Passport_Number__c = 'abc'
                                        , Party_ID__c = 'test'
                                    );
        insert objAcc1;

         NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        //objSR.Account__c = objAcc.id ;
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
                
        Booking__c objBooking = new Booking__c();
        objBooking.Account__c = objAcc.id;
        objBooking.Deal_SR__c = objSR.id;
        
        insert objBooking;
        
        Booking_Unit__c objBookingUnit = new Booking_Unit__c();
        objBookingUnit.Registration_ID__c  = '12345';
        objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
        objBookingUnit.Booking__c  = objBooking.id;
        objBookingUnit.Mortgage__c = true;
        //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
        objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
        objBookingUnit.Registration_Status_Code__c = 'ABS';
        objBookingUnit.MollakId__c = '1005';
        insert objBookingUnit;

        /*List<Buyer__c> lstBuyers = TestDataFactory_CRM.createBuyer(objBooking.Id, 2, objAcc.Id);
        lstBuyers[0].Primary_Buyer__c = true;
        insert lstBuyers;*/
        
        List<Buyer__c> lstBuyers1 = TestDataFactory_CRM.createBuyer(objBooking.Id, 1, null);
        lstBuyers1[0].Primary_Buyer__c = false;
        lstBuyers1[0].First_Name__c = objAcc.First_Name__c;
        lstBuyers1[0].Last_Name__c  = objAcc.Last_Name__c;
        insert lstBuyers1;

        Case objCase = new Case(Customer_First_Name__c = 'Test'
                                , Customer_Middle_Name__c = 'Test'
                                , Customer_Last_Name__c = 'Test'
                                , Customer_First_Name_Arabic__c = 'Test'
                                , Customer_Middle_Name_Arabic__c = 'test'
                                , Customer_Last_Name_Arabic__c = 'test'
                                , Postal_Code__c = 'test'
                                , New_CR__c = 'test'
                                , AccountId = objAcc.Id
                                , Booking_Unit__c = objBookingUnit.Id);
        insert objCase;

        CRMAllinOne__c  objCRMAllinOne = new CRMAllinOne__c();
        objCRMAllinOne.Unit__c = objBookingUnit.Id ;
        objCRMAllinOne.Registration_Id__c = '112233'; 
        objCRMAllinOne.Account__c = objAcc.Id;
        objCRMAllinOne.JB2_Mollak_OwnerNum__c = 'test';
        objCRMAllinOne.JB2_Party_Id__c = 'test';
        objCRMAllinOne.JB3_Mollak_OwnerNum__c = 'test';
        objCRMAllinOne.JB3_Party_Id__c = 'test';
        objCRMAllinOne.JB_Mollak_OwnerNum__c = 'test';
        objCRMAllinOne.JB_Party_Id__c  = 'test';
        objCRMAllinOne.MollakUnitId__c  = 'test';
        insert objCRMAllinOne;

        Test.startTest();
        Test.setMock( HttpCalloutMock.class, new MollakHttpMock());
        MollakService.getCases(new List<Id>{objCase.Id});
        Test.stopTest();
    }
}