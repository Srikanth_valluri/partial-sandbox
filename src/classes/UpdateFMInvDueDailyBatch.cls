/*
*    Batch class to update inv due every 3hr , Batch size should be 10
*/
public class UpdateFMInvDueDailyBatch implements Database.Batchable<sObject>,Database.AllowsCallouts {
    String query;
    
    public Database.QueryLocator start(Database.BatchableContext BC){
    
        Set<Id> recTypeIdSet=new Set<Id>();
        for(RecordType rec :[SELECT DeveloperName,Id,Name 
                               FROM RecordType 
                               WHERE SobjectType = 'Calling_List__c' 
                               And Name='FM Collections' ]){
                                //OR Name= 'Aging Calling List'  )  ]){
            recTypeIdSet.add(rec.Id);
        }
        
        query = 'SELECT Id,Name,Booking_Unit__c,DM_Due_Amount__c,Inv_Due__c,Amount_Paid__c,Total_Charges__c ,Amount_Pending__c,Booking_Unit__r.Id,NOT_DUE__c,Status__c,Amount_Received__c,';
        query += 'Registration_ID__c,Construction_Status__c, DUE_0_30_DAYS__c,DUE_30_60_DAYS__c,DUE_60_90_DAYS__c,DUE_90_180_DAYS__c,DUE_180_360_DAYS__c,DUE_MORE_THAN_360_DAYS__c ';
        query += 'FROM Calling_List__c WHERE RecordTypeId IN :recTypeIdSet AND IsHideFromUI__c = false';
        
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<Calling_List__c> callingLst){
        System.debug('callingLst size:::'+callingLst.size());
        List<Calling_List__c> callingLstToUpdate = new List<Calling_List__c>();
        if( callingLst != null && callingLst.size() > 0 ) {
            for( Calling_List__c callingObj : callingLst ) {
                if( String.isNotBlank( callingObj.Registration_ID__c ) ) {
                    FmIpmsRestServices.DueInvoicesResult dues = FMCallingListCreationBatch.fetchFmDues(callingObj.Registration_ID__c , callingObj.Booking_Unit__r.Id );
                    if( dues.totalDueAmount >= Label.Due_Amount_For_Creation_of_FM_CL  ) {
                        if( dues.lstDueInvoice != null && dues.lstDueInvoice.size() > 0 ) {
                            callingLstToUpdate.add(FMCallingListCreationBatch.populateInvDues( callingObj , dues.lstDueInvoice,false));      
                        }
                    }               
                }
            }
        }
        System.debug('callingLstToUpdate::'+callingLstToUpdate.size());
        if( callingLstToUpdate != null && callingLstToUpdate.size() > 0 ) {
            update callingLstToUpdate;
        }
    }
    
    public void finish(Database.BatchableContext BC){
    }
}