@isTest
/*
* Revision History: 
* Version   Author          Date        Description.
* 1.1       Swapnil Gholap  21/11/2017  Initial Draft
*/

global class RefundsMock implements WebServiceMock {    
    
    global void doInvoke(
       Object stub,
       Object request,
       Map<String, Object> response,
       String endpoint,
       String soapAction,
       String requestName,
       String responseNS,
       String responseName,
       String responseType) {
                     
       Refunds.getTokenRefundsResponse_element   respElement = new Refunds.getTokenRefundsResponse_element  ();
       respElement.return_x = '{"Status":"S","PROC_STATUS":null,"PROC_MESSAGE":null,"Message":"Process Completed Returning 0 Response Message(s)...","PARAM_ID":"81965","Amount_COCA":"0","Excess_Amount":null,"Amount_Paid":"1650"}';
       response.put('response_x', respElement);                  
   }
   
    global class RefundsMock1 implements WebServiceMock { // Status = Error && Amount_Paid = null
        global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
                         
               Refunds.getTokenRefundsResponse_element   respElement = new Refunds.getTokenRefundsResponse_element  ();
               respElement.return_x = '{"Status":"E","PROC_STATUS":null,"PROC_MESSAGE":null,"Message":"Process Error Returning 0 Response Message(s)...","PARAM_ID":"81965","Amount_COCA":"0","Excess_Amount":null,"Amount_Paid":null}';
               response.put('response_x', respElement);
                   
           }
    }
    
    global class RefundsMock2 implements WebServiceMock { // Status = Success && Amount_Paid = null
        global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
                         
               Refunds.getTokenRefundsResponse_element   respElement = new Refunds.getTokenRefundsResponse_element  ();
               respElement.return_x = '{"Status":"S","PROC_STATUS":null,"PROC_MESSAGE":null,"Message":"Process Success Returning 0 Response Message(s)...","PARAM_ID":"81965","Amount_COCA":"0","Excess_Amount":null,"Amount_Paid":""}';
               response.put('response_x', respElement);
                   
           }
    }
   
}