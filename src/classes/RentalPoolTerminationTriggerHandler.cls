/*
* Description - Handler class for case trigger.
*
* Version            Date            Author            Description
* 1.0                28/02/2018      Ashish            Initial Draft
* 1.1                01/07/2019      Arjun             Commented Final termination date and settlement amount condition check 
*/

public without sharing class RentalPoolTerminationTriggerHandler {
    
    public static final map<String, list<String>> mapDocslstOwner = 
                            new map<String, list<String>>{ 'Power of Attorney' => new list<String> { 'Legal' } ,
                                                           'CRF Form' => new list<String> { 'CDC', 'Hospitality Executive' } ,
                                                           'RP Agreement' => new list<String> { 'CDC', 'Hospitality Executive' } };
    
    public static void afterCaseUpdate( map<Id, Case> mapUpdatedCases, map<Id, Case> mapOldCases ) {
        Id terminationCaseRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Rental Pool Termination').getRecordTypeId();
        //list<Task> lstNewTasks = new list<Task>();
        
        set<Id> setCaseIds1 = new set<Id>();
        list<Case> lstCases = new list<Case>();
        //set<Id> setCaseCreate
        
        for( Case objCase : mapUpdatedCases.values() ) {
            if( objCase.RecordTypeId != null && 
                objCase.RecordTypeId == terminationCaseRecTypeId &&
                objCase.Submit_for_Approval__c == true &&
                String.isNotBlank( objCase.Approval_Status__c ) &&
                objCase.Approval_Status__c != mapOldCases.get( objCase.Id ).Approval_Status__c &&
                objCase.Approval_Status__c.equalsIgnoreCase('Approved') &&
                String.isNotBlank( objCase.Rental_Pool_Termination_Status__c ) && 
                objCase.Rental_Pool_Termination_Status__c != mapOldCases.get( objCase.Id ).Rental_Pool_Termination_Status__c &&
                objCase.Rental_Pool_Termination_Status__c.equalsIgnoreCase('Approved By Manager') &&
                String.isNotBlank( objCase.Status ) && 
                objCase.Status != mapOldCases.get( objCase.Id ).Status &&
                objCase.Status.equalsIgnoreCase('Manager Approved') ) {
                setCaseIds1.add( objCase.Id );
            }
            
            if( objCase.RecordTypeId != null && 
                objCase.RecordTypeId == terminationCaseRecTypeId && 
                String.isNotBlank( objCase.Status ) && 
                objCase.Status != mapOldCases.get( objCase.Id ).Status &&
                objCase.Status.equalsIgnoreCase('HE Approved') &&
                String.isNotBlank( objCase.Rental_Pool_Termination_Status__c ) && 
                objCase.Rental_Pool_Termination_Status__c != mapOldCases.get( objCase.Id ).Rental_Pool_Termination_Status__c &&
                objCase.Rental_Pool_Termination_Status__c.equalsIgnoreCase('Approved from Hospitality Executive') /*&&
                objCase.Final_Settlement_Amount__c != mapOldCases.get( objCase.Id ).Final_Settlement_Amount__c && 
                objCase.Final_Settlement_Amount__c != null &&
                objCase.Final_Termination_Date__c != mapOldCases.get( objCase.Id ).Final_Termination_Date__c && 
                objCase.Final_Termination_Date__c != null */) {
                lstCases.add( objCase );
            }
        }
        
        if( !setCaseIds1.isEmpty() ) {
            createReviewTaskUnderCase( setCaseIds1 );
        }
        
        if( !lstCases.isEmpty() ) {
            createTerminationTaskUnderCase( lstCases );
        }
    }
    
    private static void createReviewTaskUnderCase( set<Id> setCaseIds ) {
        list<Task> lstTasksUnderCases = new list<Task>();
        map<String, Task> mapOwnerTask = new map<String, Task>();
        for( Case objCase : [ SELECT Id
                                   , OwnerId
                                   , ( SELECT Id
                                            , Attachment_URL__c
                                            , Document_Type__c
                                            , isValid__c
                                            , Type__c
                                            , OwnerId
                                         FROM SR_Attachments__r ) 
                                FROM Case
                               WHERE Id IN :setCaseIds ] ) {
            for( SR_Attachments__c objAttach : objCase.SR_Attachments__r ) {
                if( String.isNotBlank( objAttach.Type__c ) && mapDocslstOwner.containsKey( objAttach.Type__c ) ) {
                    for( String strDocOwner : mapDocslstOwner.get( objAttach.Type__c ) ) {
                        String strSubject = '';
                        if( strDocOwner.equalsIgnoreCase('Hospitality Executive') ) {
                            strSubject = ' and update termination Date';
                        }
                        if( mapOwnerTask.containsKey( strDocOwner ) ) {
                          mapOwnerTask.get( strDocOwner ).Document_URL__c += ', ' + objAttach.Attachment_URL__c ;
                        }
                        else {
                          Task objTask = RentalPoolTerminationHelper.createTaskObject( strDocOwner ,
                                                                                       'High' ,
                                                                                       'Rental Pool Termination',
                                                                                       'Not Started',
                                                                                       'Review RP Termination' + strSubject ,
                                                                                       objCase,
                                                                                       '' );
                          objTask.Document_URL__c = objAttach.Attachment_URL__c ;
                          
                          objTask.OwnerId = String.isNotBlank( objCase.OwnerId ) && 
                                            String.valueOf( objCase.OwnerId ).startsWithIgnoreCase('005') ? 
                                            objCase.OwnerId :
                                            Label.DefaultCaseOwnerId ;
                          
                          mapOwnerTask.put( strDocOwner, objTask );
                        }
                    }
                }
            }
        }
        lstTasksUnderCases.addAll( mapOwnerTask.values() );
        if( !lstTasksUnderCases.isEmpty() ) {
            insert lstTasksUnderCases ;
        }
    }
    
    private static void createTerminationTaskUnderCase( list<Case> lstCases ) {
        list<Task> lstTasksUnderCases = new list<Task>();
        for( Case objCase : lstCases ) {
            Task objTask = RentalPoolTerminationHelper.createTaskObject( 'CRE' ,
                                                                         'Medium' ,
                                                                         'Rental Pool Termination',
                                                                         'Not Started',
                                                                         'Generate Letter of Termination for the customer',
                                                                         objCase,
                                                                         '' );
            
            objTask.OwnerId = String.isNotBlank( objCase.OwnerId ) && 
                              String.valueOf( objCase.OwnerId ).startsWithIgnoreCase('005') ? 
                              objCase.OwnerId :
                              Label.DefaultCaseOwnerId ;
            
            lstTasksUnderCases.add( objTask );
        }
        
        if( !lstTasksUnderCases.isEmpty() ) {
            insert lstTasksUnderCases ;
        }
    }
}