@isTest(SeeAlldata=false)
public class PreinquiryReshuffleBatchTest {
    public static testmethod void testMethod1() {
        Id preInq = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
        Inquiry__c inqObjNew= new Inquiry__c(RecordTypeId=preInq,
                                          Mobile_CountryCode__c='American Samoa: 001684',
                                          Mobile_Phone__c='1234',Mobile_Phone_Encrypt__c = '123456789123',
                                          Email__c='mk@gmail123.com',Re_Assignment_Pass_No__c = 1,
                                          First_Name__c='Test',Last_Name__c='Last',
                                          Inquiry_Status__c = 'Unreachable - Not responding',
                                          Last_Activity_Date__c = System.now().Date().addDays(-10)
                                          );
        insert inqObjNew;
        PreinquiryReshuffleBatch preInqBatch = new PreinquiryReshuffleBatch(null);
        DataBase.executeBatch(preInqBatch);

        /*
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        
            User u1 = new User(Alias = 'standt', Email='standardgssfduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Tesdssting', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standssdarduser@testorg.com');
            insert u1;
            User u = new User(Alias = 'standt', Email='standardgsduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Tesdsting', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standsdarduser@testorg.com');
            insert u;
             u.ManagerID = u1.id;
             update u;
            
            TS_Assignment_Settings__c csInstance = new TS_Assignment_Settings__c(
                Name = 'Test',
                Owner_Id__c = u1.id,
                Round_Robin_Id__c = 2,
                Telesales_Executive__c = 'abc',
                Type__c = 'Stands'
            );
            insert csInstance;
        List<TSA_Preinquiry_Reshuffle__mdt> preInquiryConditionMDT = [SELECT id,DeveloperName,Based_In__c,
        Days__c,Queue__c,ReassignmentPass__c,Status__c FROM TSA_Preinquiry_Reshuffle__mdt];
         System.runAs(u){
              Id preInq = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
        Inquiry__c inqObj= new Inquiry__c(RecordTypeId=preInq,
                                          Mobile_CountryCode__c='American Samoa: 001684',
                                          Mobile_Phone__c='1234',Mobile_Phone_Encrypt__c = '123456789',
                                          Email__c='mk@gmail.com',
                                          First_Name__c='Test',Last_Name__c='Last',
                                          Inquiry_Status__c = 'Unreachable - Not responding',
                                          Last_Activity_Date__c = System.now()
                                          );
        insert inqObj;
         Inquiry__c inqObjNew= new Inquiry__c(RecordTypeId=preInq,
                                          Mobile_CountryCode__c='American Samoa: 001684',
                                          Mobile_Phone__c='1234',Mobile_Phone_Encrypt__c = '123456789123',
                                          Email__c='mk@gmail123.com',
                                          First_Name__c='Test',Last_Name__c='Last',
                                          Inquiry_Status__c = 'Unreachable - Not responding',
                                          Last_Activity_Date__c = System.now().Date().addDays(-10)
                                          );
        insert inqObjNew;
           Test.setCreatedDate(inqObjNew.Id, System.now().Date().addDays(-10));
          system.debug('>>>System.now()>>>>'+System.now());
          system.debug('>>>System.now().Date().addDays(-10)>>>>'+System.now().Date().addDays(-10));
           system.debug('>>>>inqObjNew.Last_Activity_Date__c>>>>'+inqObjNew.Last_Activity_Date__c);
        list<Inquiry__c> inqlIST = [Select id , name,Mobile_Phone_Encrypt__c,Email__c,Last_Activity_Date__c from Inquiry__c ];
        system.debug('>>>inqlIST>>>>'+inqlIST);
        system.debug('>>>>inqObj.Last_Activity_Date__c>>>>'+inqObjNew.Last_Activity_Date__c);
        system.debug('>>>>inqObj.Last_Activity_Date__c>>>>'+inqObj.Last_Activity_Date__c);
         }
         
         Test.startTest();
         PreinquiryReshuffleBatch preInqBatch = new PreinquiryReshuffleBatch(preInquiryConditionMDT);
        DataBase.executeBatch(preInqBatch);
        Test.stopTest();
        */
    }
     public static testmethod void testMethod2() {
        /*Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        
            User u1 = new User(Alias = 'standt', Email='standardgssfduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Tesdssting', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standssdarduser@testorg.com');
            insert u1;
            User u = new User(Alias = 'standt', Email='standardgsduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Tesdsting', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standsdarduser@testorg.com');
            insert u;
             u.ManagerID = u1.id;
             update u;
            
            TS_Assignment_Settings__c csInstance = new TS_Assignment_Settings__c(
                Name = 'Test',
                Owner_Id__c = u1.id,
                Round_Robin_Id__c = 2,
                Telesales_Executive__c = 'abc',
                Type__c = 'Stands'
            );
            insert csInstance;
        List<TSA_Preinquiry_Reshuffle__mdt> preInquiryConditionMDT = [SELECT id,DeveloperName,Based_In__c,
        Days__c,Queue__c,ReassignmentPass__c,Status__c FROM TSA_Preinquiry_Reshuffle__mdt];
         System.runAs(u){
              Id preInq = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Pre Inquiry').getRecordTypeId();
        Inquiry__c inqObj= new Inquiry__c(RecordTypeId=preInq,
                                          Mobile_CountryCode__c='American Samoa: 001684',
                                          Mobile_Phone__c='1234',Mobile_Phone_Encrypt__c = '123456789',
                                          Email__c='mk@gmail.com',Re_Assignment_Pass_No__c = 1,
                                          First_Name__c='Test',Last_Name__c='Last',
                                          Inquiry_Status__c = 'Unreachable - Not responding',
                                          Last_Activity_Date__c = System.now()
                                          );
        insert inqObj;
         Inquiry__c inqObjNew= new Inquiry__c(RecordTypeId=preInq,
                                          Mobile_CountryCode__c='American Samoa: 001684',
                                          Mobile_Phone__c='1234',Mobile_Phone_Encrypt__c = '123456789123',
                                          Email__c='mk@gmail123.com',Re_Assignment_Pass_No__c = 1,
                                          First_Name__c='Test',Last_Name__c='Last',
                                          Inquiry_Status__c = 'Unreachable - Not responding',
                                          Last_Activity_Date__c = System.now().Date().addDays(-10)
                                          );
        insert inqObjNew;
           Test.setCreatedDate(inqObjNew.Id, System.now().Date().addDays(-10));
          system.debug('>>>System.now()>>>>'+System.now());
          system.debug('>>>System.now().Date().addDays(-10)>>>>'+System.now().Date().addDays(-10));
           system.debug('>>>>inqObjNew.Last_Activity_Date__c>>>>'+inqObjNew.Last_Activity_Date__c);
        list<Inquiry__c> inqlIST = [Select id , name,Mobile_Phone_Encrypt__c,Email__c,Last_Activity_Date__c from Inquiry__c ];
        system.debug('>>>inqlIST>>>>'+inqlIST);
        system.debug('>>>>inqObj.Last_Activity_Date__c>>>>'+inqObjNew.Last_Activity_Date__c);
        system.debug('>>>>inqObj.Last_Activity_Date__c>>>>'+inqObj.Last_Activity_Date__c);
         }
         
         Test.startTest();
         PreinquiryReshuffleBatch preInqBatch = new PreinquiryReshuffleBatch(preInquiryConditionMDT);
        DataBase.executeBatch(preInqBatch);
        Test.stopTest();*/
    }
}