/*
    Date             Author     Description
16th April 2018      Vivian     Updated process to not send LE status after SPA Execution
*/
public without sharing class TaskTriggerHandlerAssignment {

    public void checkTaskAssignedUser(list<Task> listTask) {
        Integer counterVal;
        Integer counterRegVal;
        set<Id> setTaskCaseId = new set<Id>();
        set<Id> setCaseRegUpdate = new set<Id>();
        set<Id> setTaskForCallout1 = new set<Id>();
        set<Id> setTaskForCallout2 = new set<Id>();
        set<Id> setTaskForRegUpdate = new set<Id>();
        set<String> setTaskForUser = new set<String>();
        system.debug('Inside handler*****');
        if(Label.UsersOutsideSalesforce != null) {
            setTaskForUser.addAll(Label.UsersOutsideSalesforce.split(','));
            system.debug('setTaskForUser==='+setTaskForUser);
            counterVal = 1;
            counterRegVal = 1;
            for(Task taskInst: listTask){
                if(taskInst.Process_Name__c != null
                && taskInst.Assigned_User__c != null
                && taskInst.WhatId != null
                && String.valueOf(taskInst.WhatId).startsWith('500')
                && !taskInst.IPMS_Status__c.equalsIgnoreCase('OTHERS')
                && taskInst.Process_Name__c.equalsIgnoreCase('Assignment')) {
                    system.debug('*********CAME INSIDE**********');
                    if(counterRegVal <= 100) {
                        setTaskForRegUpdate.add(taskInst.Id);
                        setCaseRegUpdate.add(taskInst.WhatId);
                    }
                    else {
                        updateRegStatus(setTaskForRegUpdate, setCaseRegUpdate);
                        setCaseRegUpdate = new set<Id>();
                        setTaskForRegUpdate = new set<Id>();
                        setTaskForRegUpdate.add(taskInst.Id);
                        setCaseRegUpdate.add(taskInst.WhatId);
                    }
                    counterRegVal++;
                }
                if(taskInst.Process_Name__c != null
                && taskInst.Assigned_User__c != null
                && taskInst.WhatId != null
                && taskInst.Process_Name__c.equalsIgnoreCase('Assignment')
                && setTaskForUser.contains(taskInst.Assigned_User__c)
                && String.valueOf(taskInst.WhatId).startsWith('500')){
                    if(counterVal <= 100) {
                        setTaskForCallout1.add(taskInst.Id);
                    }
                    else {
                        setTaskForCallout2.add(taskInst.Id);
                    }
                    setTaskCaseId.add(taskInst.WhatId);
                    counterVal++;
                }
            }
            system.debug('setCaseRegUpdate==='+setCaseRegUpdate);
            system.debug('setTaskForRegUpdate==='+setTaskForRegUpdate);
            system.debug('setTaskCaseId==='+setTaskCaseId);
            system.debug('setTaskForCallout1==='+setTaskForCallout1);
            system.debug('setTaskForCallout2==='+setTaskForCallout2);
            
            if(!setTaskForRegUpdate.isEmpty() && !setCaseRegUpdate.isEmpty()) {
                updateRegStatus(setTaskForRegUpdate, setCaseRegUpdate);
            }
            if(!setTaskForCallout1.isEmpty() && !setTaskCaseId.isEmpty()) {
                createTaskOnIPMS(setTaskForCallout1, setTaskCaseId);
            }
            if(!setTaskForCallout2.isEmpty() && !setTaskCaseId.isEmpty()) {
                createTaskOnIPMS(setTaskForCallout2, setTaskCaseId);
            }
        }
    }

    public void checkTaskCompletion(list<Task> listTask, map<Id, Task> mapOldTask) {
        Integer counterVal;
        set<Id> setCaseIdPOA = new set<Id>();
        set<Id> setCaseIdPRC = new set<Id>();
        set<Id> setCaseIdSPA = new set<Id>();
        set<Id> setTaskCaseId = new set<Id>();
        set<Id> setCaseIdTitleDeed = new set<Id>();
        set<Id> setTaskForIPMSUpdate = new set<Id>();
        set<String> setCasePOA = new set<String>();
        set<String> setCasePRC = new set<String>();
        set<String> setCaseSPA = new set<String>();
        set<String> setTaskForUser = new set<String>();
        map<Id, list<Task>> mapCaseTask = new map<Id, list<Task>>();
        map<String, String> mapTaskDocURL = new map<String, String>();
        list<Task> lstCancelledTask = new list<Task>();

        //get the assigned user for IPMS 
        if(Label.UsersOutsideSalesforce != null) {
            setTaskForUser.addAll(Label.UsersOutsideSalesforce.split(','));
            system.debug('setTaskForUser==='+setTaskForUser);
        }
        counterVal = 1;
        for(Task taskInst: listTask) {
            //check if Update IPMS is true then push task to IPMS
            if(taskInst.Process_Name__c != null
                && taskInst.Assigned_User__c != null
                && taskInst.WhatId != null
                && taskInst.Process_Name__c.contains('Assignment')
                && setTaskForUser.contains(taskInst.Assigned_User__c)
                && String.valueOf(taskInst.WhatId).startsWith('500')
                && ((!taskInst.Pushed_to_IPMS__c && taskInst.Update_IPMS__c 
                && taskInst.Update_IPMS__c != mapOldTask.get(taskInst.Id).Update_IPMS__c)
                || (taskInst.Status != mapOldTask.get(taskInst.Id).Status))){
                if(counterVal <= 100) {
                    setTaskForIPMSUpdate.add(taskInst.Id);
                    setTaskCaseId.add(taskInst.WhatId);
                }
                else {
                    createTaskOnIPMS(setTaskForIPMSUpdate, setTaskCaseId);
                    setTaskCaseId = new set<Id>();
                    setTaskForIPMSUpdate = new set<Id>();
                    setTaskForIPMSUpdate.add(taskInst.Id);
                    setTaskCaseId.add(taskInst.WhatId);
                }
                counterVal++;
                
            }
            if(taskInst.WhatId != null && String.valueOf(taskInst.WhatId).startsWith('500')
                && taskInst.Status.equalsIgnoreCase('Completed') && taskInst.Status != mapOldTask.get(taskInst.Id).Status
                && taskInst.Process_Name__c != null && taskInst.Process_Name__c.equalsIgnoreCase('Assignment') 
                && taskInst.Subject != null) {

                if(taskInst.Subject.equalsIgnoreCase('BUYER POA Verification Pending')
                || taskInst.Subject.equalsIgnoreCase('SELLER POA Verification Pending')) {
                    setCaseIdPOA.add(taskInst.WhatId);
                    setCasePOA.add(taskInst.WhatId+'-'+taskInst.Subject.removeEnd(' Verification Pending'));
                }
                else if(taskInst.Subject.equalsIgnoreCase(Label.Dubai_OffPlan_Task_PRC_Doc)) {
                    setCaseIdPRC.add(taskInst.WhatId);
                    setCasePRC.add(taskInst.WhatId+'-'+Label.PRC_Document_Name);
                    mapTaskDocURL.put(taskInst.WhatId+'-'+Label.PRC_Document_Name, taskInst.Document_URL__c);
                }
                else if(taskInst.Subject.equalsIgnoreCase(Label.Task_SPA_Execution)) {
                    setCaseIdSPA.add(taskInst.WhatId);
                    setCaseSPA.add(taskInst.WhatId+'-'+Label.SPA_Document_Name);
                    mapTaskDocURL.put(taskInst.WhatId+'-'+Label.SPA_Document_Name, taskInst.Document_URL__c);
                }
                else if(taskInst.Subject.equalsIgnoreCase(Label.Abu_Dhabi_Task_Lease_Agreement)) {
                    setCaseIdSPA.add(taskInst.WhatId);
                    setCaseSPA.add(taskInst.WhatId+'-'+Label.Lease_Document_Name);
                    mapTaskDocURL.put(taskInst.WhatId+'-'+Label.Lease_Document_Name, taskInst.Document_URL__c);
                }
                else {
                    if(mapCaseTask.containsKey(taskInst.WhatId)) {
                        mapCaseTask.get(taskInst.WhatId).add(taskInst);
                    }
                    else {
                        mapCaseTask.put(taskInst.WhatId, new list<Task> {taskInst});
                    }
                }
            }
            if(taskInst.WhatId != null && String.valueOf(taskInst.WhatId).startsWith('500')
                && taskInst.Status.equalsIgnoreCase('Closed') && taskInst.Status != mapOldTask.get(taskInst.Id).Status
                && taskInst.Process_Name__c != null && taskInst.Process_Name__c.equalsIgnoreCase('Assignment') 
                && taskInst.Subject != null && taskInst.Subject.equalsIgnoreCase(Label.Dubai_Ready_Task_Upload_Title_Deed)) {
                setCaseIdTitleDeed.add(taskInst.WhatId);
            }
            if(taskInst.WhatId != null && String.valueOf(taskInst.WhatId).startsWith('500')
            && taskInst.Status.equalsIgnoreCase('Cancelled') && taskInst.Status != mapOldTask.get(taskInst.Id).Status
            && taskInst.Process_Name__c != null && taskInst.Process_Name__c.equalsIgnoreCase('Assignment') ){
                lstCancelledTask.add(taskInst);
            }
        }
        system.debug('setCaseIdPOA==='+setCaseIdPOA);
        system.debug('setCasePOA==='+setCasePOA);
        system.debug('setCaseIdPRC==='+setCaseIdPRC);
        system.debug('setCasePRC==='+setCasePRC);
        system.debug('setCaseIdSPA==='+setCaseIdSPA);
        system.debug('setCaseSPA==='+setCaseSPA);
        system.debug('mapTaskDocURL==='+mapTaskDocURL);
        system.debug('mapCaseTask==='+mapCaseTask);
        system.debug('setTaskForIPMSUpdate==='+setTaskForIPMSUpdate);
        system.debug('setTaskCaseId==='+setTaskCaseId);
        
        //push task to IPMS
        if(!setTaskForIPMSUpdate.isEmpty() && !setTaskCaseId.isEmpty()) {
            createTaskOnIPMS(setTaskForIPMSUpdate, setTaskCaseId);
        }
        if(!setCaseIdPOA.isEmpty() && !setCasePOA.isEmpty()) {
            updateCaseAttachment(setCaseIdPOA, setCasePOA, mapTaskDocURL,'POA');
        }
        if(!setCaseIdPRC.isEmpty() && !setCasePRC.isEmpty()) {
            updateCaseAttachment(setCaseIdPRC, setCasePRC, mapTaskDocURL,'PRC');
        }
        if(!setCaseIdSPA.isEmpty() && !setCaseSPA.isEmpty()) {
            updateCaseAttachment(setCaseIdSPA, setCaseSPA, mapTaskDocURL,'SPA');
        }
        if(!mapCaseTask.isEmpty()) {
            updateCase(mapCaseTask);
        }
        if(!setCaseIdTitleDeed.isEmpty()) {
            verifyTitleDeed(setCaseIdTitleDeed);
        }
        if(!lstCancelledTask.isEmpty()){
            checkTaskAssignedUser(lstCancelledTask);
        }
    }

    public void verifyTitleDeed(set<Id> setCaseIdTitleDeed) {
        list<Case> listCaseUpdate = new list<Case>();
        list<Task> listTitleDeedTask = new list<Task>();

        for(Case caseInst: [select Id, CurrencyIsoCode, OwnerId, Status from Case where Id IN: setCaseIdTitleDeed]) {
            Task taskObj = new Task();
            taskObj.ActivityDate = system.today().addDays(1);
            taskObj.OwnerId = caseInst.OwnerId;
            taskObj.WhatId = caseInst.Id;
            taskObj.Subject = Label.Dubai_Ready_Task_Confirmed_Buyer_Details;
            taskObj.Assigned_User__c = 'CDC';
            taskObj.Process_Name__c = 'Assignment';
            taskObj.Priority = 'High';
            taskObj.Status = 'Not Started';
            listTitleDeedTask.add(taskObj);
            
            caseInst.Status = 'Verify New Title Deed';
            listCaseUpdate.add(caseInst);
        }
        if(!listTitleDeedTask.isEmpty()) {
            insert listTitleDeedTask;
            update listCaseUpdate;
        }
    }

    public void updateCase(map<Id, list<Task>> mapCaseTask) {
        list<Case> listUpdateCase = new list<Case>();
        for(Id caseIdInst: mapCaseTask.keySet()) {
            Case caseObj = new Case();
            caseObj.Id = caseIdInst;
            Boolean caseFlag = false;
            for(Task taskInst: mapCaseTask.get(caseIdInst)) {
                if(taskInst.Subject.equalsIgnoreCase(Label.Dubai_Task_Print_NOC) 
                    || taskInst.Subject.equalsIgnoreCase(Label.DIFC_Task_Print_NOC)) {
                    caseObj.NOC_Handovered__c = true;
                    caseFlag = true;
                }
                if(taskInst.Subject.equalsIgnoreCase(Label.DIFC_Task_NOC_Preparation)) {
                    caseObj.Generate_NOC__c = true;
                    caseFlag = true;
                }
                if(taskInst.Subject.equalsIgnoreCase(Label.DIFC_Task_Confirmed_Buyer_Details)
                   || taskInst.Subject.equalsIgnoreCase(Label.Dubai_Ready_Task_Confirmed_Buyer_Details)) {
                    caseObj.New_Buyer_Details_Updated__c = true;
                    caseFlag = true;
                }
                if(taskInst.Subject.equalsIgnoreCase(Label.Dubai_Ready_Task_Document_Upload)) {
                    caseObj.Documents_Uploaded__c = true;
                    caseFlag = true;
                }
                if(taskInst.Subject.equalsIgnoreCase(Label.DIFC_Task_Buyer_SOA)) {
                    caseObj.Buyer_SOA__c = true;
                    caseFlag = true;
                }
                if(taskInst.Subject.equalsIgnoreCase(Label.Dubai_Ready_Task_Transfer_Account)) {
                    caseObj.Transfer_Account__c = true;
                    caseFlag = true;
                }
                if(taskInst.Subject.equalsIgnoreCase(Label.Task_Originals_Submitted)) {
                    caseObj.Originals_submitted_to_contracts__c = true;
                    caseFlag = true;
                }
                if(taskInst.Subject.equalsIgnoreCase(Label.Task_Funds_to_New_Account)) {
                    caseObj.Allocate_funds__c = true;
                    caseFlag = true;
                }
                if(taskInst.Subject.equalsIgnoreCase(Label.Q_INT_Task_SPA_Archival)
                    || taskInst.Subject.equalsIgnoreCase(Label.Abu_Dhabi_Task_Lease_Archival)) {
                    caseObj.SPA_Archival_Audit__c = true;
                    caseFlag = true;
                }
                if(taskInst.Subject.equalsIgnoreCase(Label.Dubai_Off_Plan_Task_Document_Archival)) {
                    caseObj.Document_Archival__c = true;
                    caseFlag = true;
                }
            }
            if(caseFlag) {
                listUpdateCase.add(caseObj);
            }
        }
        if(!listUpdateCase.isEmpty()) {
            update listUpdateCase;
        }
        system.debug('listUpdateCase=='+listUpdateCase);
    }

    public void updateCaseAttachment(set<Id> setCaseId, set<String> setCaseProcess, map<String, String> mapTaskDocURL, String processName) {
        list<SR_Attachments__c> listUpdateCaseAttach = new list<SR_Attachments__c>(); 
        for(SR_Attachments__c caseAttachInst: [select Id, Name, Case__c, isValid__c, Attachment_URL__c 
                                                from SR_Attachments__c 
                                                where Case__c IN: setCaseId]) {
            if(setCaseProcess.contains(caseAttachInst.Case__c+'-'+caseAttachInst.Name)) {
                if(processName.equalsIgnoreCase('POA')) {
                    caseAttachInst.isValid__c = true;
                    listUpdateCaseAttach.add(caseAttachInst);
                }
                else if(processName.equalsIgnoreCase('PRC') 
                    && mapTaskDocURL.containsKey(caseAttachInst.Case__c+'-'+caseAttachInst.Name)) {
                    caseAttachInst.isValid__c = true;
                    if(mapTaskDocURL.get(caseAttachInst.Case__c+'-'+caseAttachInst.Name) != null) {
                       caseAttachInst.Attachment_URL__c = mapTaskDocURL.get(caseAttachInst.Case__c+'-'+caseAttachInst.Name);
                    }
                    listUpdateCaseAttach.add(caseAttachInst);
                }
                else if(processName.equalsIgnoreCase('SPA') 
                    && mapTaskDocURL.containsKey(caseAttachInst.Case__c+'-'+caseAttachInst.Name)){
                    caseAttachInst.isValid__c = true;
                    caseAttachInst.Signed_by_DAMAC__c = true;
                    if(mapTaskDocURL.get(caseAttachInst.Case__c+'-'+caseAttachInst.Name) != null) {
                        caseAttachInst.Attachment_URL__c = mapTaskDocURL.get(caseAttachInst.Case__c+'-'+caseAttachInst.Name);
                    }
                    listUpdateCaseAttach.add(caseAttachInst);
                }
            }
        }
        if(!listUpdateCaseAttach.isEmpty()) {
            update listUpdateCaseAttach;
        }
        system.debug('listUpdateCaseAttach==='+listUpdateCaseAttach);
    }

    /*
    public map<Id, Case> getCaseDetails(set<Id> setTaskCaseId) {
        map<Id, Case> mapCaseDetails = new map<Id, Case>([select Id, Status, Buyer__r.Party_ID__c,
                            Booking_Unit__r.Registration_ID__c, CaseNumber, Relationship_with_Seller__c,
                            Case_Type__c, Buyer_Type__c, Booking_Unit__r.Unit_Details__c,
                            Booking_Unit__r.Inventory__r.Property_City__c, Buyer__r.First_Name__c,
                            Booking_Unit__r.Inventory__r.Property_Status__c, Buyer__r.Last_Name__c,
                            Booking_Unit__r.Inventory__r.Property_Country__c, Buyer__r.Nationality__c,
                            Booking_Unit__r.Inventory__r.Property__r.DIFC__c, Buyer__r.Passport_Number__c,
                            Buyer__r.Passport_Expiry_Date__c, Buyer__r.IPMS_Buyer_ID__c, Seller__r.Party_ID__c,
                            Seller__r.FirstName, Seller__r.LastName, Seller__r.Nationality__pc
                            from Case where Id IN: setTaskCaseId]);
        return mapCaseDetails;
    }
    */

    @future(callout=true)
    public static void updateRegStatus(set<Id> setTaskForRegUpdate, set<Id> setCaseRegUpdate) {
        list<Error_Log__c> listErrorLog = new list<Error_Log__c>();
        map<Id, String> mapCaseRegId = new map<Id, String>();
        map<String, Object> mapDeserializeRegStatus = new map<String, Object>();
        // Removed and "SPA_Execution__c = false" filter from query on 16th April 2018
        for(Case caseInst: [select Id, SPA_Execution__c, Booking_Unit__r.Registration_ID__c from Case 
                 where Id IN: setCaseRegUpdate and Booking_Unit__c != null 
                 and Booking_Unit__r.Registration_ID__c != null]) {
            mapCaseRegId.put(caseInst.Id, caseInst.Booking_Unit__r.Registration_ID__c);
        }
        system.debug('mapCaseRegId==='+mapCaseRegId);

        for(Task taskInst: [select Id, WhatId, IPMS_Status__c, Subject from Task where Id IN: setTaskForRegUpdate]) {
            if(mapCaseRegId.containsKey(taskInst.WhatId)) {
                AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint objClass = new AssignmentProcessWSDL.AssignmentHttpSoap11Endpoint();
                objClass.timeout_x = 120000;
                String strRegStatusResponse = objClass.UpdateRegistrationStatus('2-'+String.valueOf(System.currentTimeMillis()), 
                       'UPDATE_REG_STATUS', 'SFDC', mapCaseRegId.get(taskInst.WhatId), taskInst.IPMS_Status__c);
                system.debug('strRegStatusResponse==='+strRegStatusResponse);
                if(String.isNotBlank(strRegStatusResponse)) {
                    mapDeserializeRegStatus = (map<String, Object>)JSON.deserializeUntyped(strRegStatusResponse);
                    if(mapDeserializeRegStatus.get('Status') != 'S') {
                        Error_Log__c objErr = createErrorLogRecord(null,mapCaseRegId.get(taskInst.WhatId),taskInst.WhatId);
                        objErr.Error_Details__c = taskInst.Subject+' Failed with Error from IPMS for Status Update '+taskInst.IPMS_Status__c;
                        listErrorLog.add(objErr);
                    }
                }
                else {
                    Error_Log__c objErr = createErrorLogRecord(null,mapCaseRegId.get(taskInst.WhatId),taskInst.WhatId);
                    objErr.Error_Details__c = taskInst.Subject+' Failed with Error : No Response from IPMS for Status Update '+taskInst.IPMS_Status__c;
                    listErrorLog.add(objErr);
                }
            }
        }
        if(!listErrorLog.isEmpty()) {
            insertErrorLog(listErrorLog);
        }
    }

    @future(callout=true)
    public static void createTaskOnIPMS(set<Id> setTaskForCallout, set<Id> setTaskCaseId) {
        list<Error_Log__c> listErrorLog = new list<Error_Log__c>();
        map<String, Object> mapDeserializeTask = new map<String, Object>(); 

        system.debug('Task cfreation called********************');
        map<Id, Case> mapCaseDetails = new map<Id, Case>([select Id, Status, Buyer__r.Party_ID__c,
                            Booking_Unit__r.Registration_ID__c, CaseNumber, Relationship_with_Seller__c,
                            Case_Type__c, Buyer_Type__c, Booking_Unit__r.Unit_Details__c,
                            Booking_Unit__r.Rental_Pool__c, Purpose_of_POA__c, Purpose_of_POA_Seller__c,
                            Booking_Unit__r.Inventory__r.Property_City__c, Buyer__r.First_Name__c,
                            Booking_Unit__r.Inventory__r.Property_Status__c, Buyer__r.Last_Name__c,
                            Booking_Unit__r.Inventory__r.Property_Country__c, Buyer__r.Nationality__c,
                            Booking_Unit__r.Inventory__r.Property__r.DIFC__c, Buyer__r.Passport_Number__c,
                            Buyer__r.Passport_Expiry_Date__c, Buyer__r.IPMS_Buyer_ID__c, Seller__r.Party_ID__c,
                            Seller__r.FirstName, Seller__r.LastName, Seller__r.Nationality__pc,
                            AccountId, Account.Party_ID__c, Booking_Unit__c, Seller__c,
                            Recordtype.DeveloperName
                            from Case where Id IN: setTaskCaseId]);
        //return mapCaseDetails;
        
        list<Task> lsTaskToUpdate = new list<Task>();
        for(Task objTask : [Select t.WhoId
                                 , t.WhatId
                                 , t.Type
                                 , t.Status
                                 , t.OwnerId
                                 , t.Id
                                 , t.IPMS_Role__c
                                 , t.Subject
                                 , t.CreatedDate
                                 , t.Description
                                 , t.Assigned_User__c
                                 , t.ActivityDate
                                 , t.Owner.Name
                                 , t.Task_Error_Details__c
                                 , t.Pushed_to_IPMS__c
                                 , t.Document_URL__c
                                 From Task t
                                 where t.Id IN : setTaskForCallout]){

            list<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5> listObjBeans = 
            new list<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5>();
            
            TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objHeaderBean = 
            new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
            String reqNo = '2-'+string.valueOf(System.currentTimeMillis());
            objHeaderBean.PARAM_ID = '2-'+mapCaseDetails.get(objTask.WhatId).CaseNumber;
            system.debug('param id*****'+objHeaderBean.PARAM_ID);
            objHeaderBean.ATTRIBUTE1 = 'HEADER';
            if(mapCaseDetails.get(objTask.WhatId).Recordtype.DeveloperName.equalsIgnoreCase('Rental_Pool_Assignment')){
                objHeaderBean.ATTRIBUTE2 = 'Rental Pool Assignment';
            }else{
                objHeaderBean.ATTRIBUTE2 = 'Assignment';
            }
            /*
            if(mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Rental_Pool__c){
                objHeaderBean.ATTRIBUTE2 = 'Rental Pool Assignment';
            }else{
                objHeaderBean.ATTRIBUTE2 = 'Assignment';
            }
            */
            if(mapCaseDetails.get(objTask.WhatId).Status.equalsIgnoreCase('Cancelled')){
                objHeaderBean.ATTRIBUTE3 = 'Cancel';
            }else if(mapCaseDetails.get(objTask.WhatId).Status.contains('Reject')
            || objTask.Status.equalsIgnoreCase('Aborted')){
                objHeaderBean.ATTRIBUTE3 = 'Reject';
            }else if(mapCaseDetails.get(objTask.WhatId).Status.equalsIgnoreCase('Completed') 
                || objTask.Status.equalsIgnoreCase('Closed')){
                objHeaderBean.ATTRIBUTE3 = 'Closed';
            }else{
                objHeaderBean.ATTRIBUTE3 = 'Open';
            }
            system.debug('objTask.Status*****'+objTask.Status);
            objHeaderBean.ATTRIBUTE4 = objTask.Owner.Name;
            objHeaderBean.ATTRIBUTE5 = mapCaseDetails.get(objTask.WhatId).Seller__r.Party_ID__c;
            system.debug('party*****'+mapCaseDetails.get(objTask.WhatId).Seller__r.Party_ID__c);
            objHeaderBean.ATTRIBUTE6 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c;
            system.debug('regid*****'+mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c);
            objHeaderBean.ATTRIBUTE7 = String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()); // format this as DD-MON- YYYY
            system.debug('date*****'+String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()));
            objHeaderBean.ATTRIBUTE8 = '';
            objHeaderBean.ATTRIBUTE9 = '';
            objHeaderBean.ATTRIBUTE10 = String.isNotBlank(mapCaseDetails.get(objTask.WhatId).Account.Party_ID__c) == true ? mapCaseDetails.get(objTask.WhatId).Account.Party_ID__c : '';
            objHeaderBean.ATTRIBUTE11 = objTask.WhatId;
            objHeaderBean.ATTRIBUTE12 = '';
            objHeaderBean.ATTRIBUTE13 = '';
            objHeaderBean.ATTRIBUTE14 = '';
            objHeaderBean.ATTRIBUTE15 = '';
            objHeaderBean.ATTRIBUTE16 = '';
            objHeaderBean.ATTRIBUTE17 = '';
            objHeaderBean.ATTRIBUTE18 = '';
            objHeaderBean.ATTRIBUTE19 = '';
            objHeaderBean.ATTRIBUTE20 = '';
            objHeaderBean.ATTRIBUTE21 = '';
            objHeaderBean.ATTRIBUTE22 = '';
            objHeaderBean.ATTRIBUTE23 = '';
            objHeaderBean.ATTRIBUTE24 = '';
            objHeaderBean.ATTRIBUTE25 = '';
            objHeaderBean.ATTRIBUTE26 = '';
            objHeaderBean.ATTRIBUTE27 = '';
            objHeaderBean.ATTRIBUTE28 = '';
            objHeaderBean.ATTRIBUTE29 = '';
            objHeaderBean.ATTRIBUTE30 = '';
            objHeaderBean.ATTRIBUTE31 = '';
            objHeaderBean.ATTRIBUTE32 = '';
            objHeaderBean.ATTRIBUTE33 = '';
            objHeaderBean.ATTRIBUTE34 = '';
            objHeaderBean.ATTRIBUTE35 = '';
            objHeaderBean.ATTRIBUTE36 = '';
            objHeaderBean.ATTRIBUTE37 = '';
            objHeaderBean.ATTRIBUTE38 = '';
            objHeaderBean.ATTRIBUTE39 = '';
            //objHeaderBean.ATTRIBUTE40 = '';
            objHeaderBean.ATTRIBUTE41 = '';
            objHeaderBean.ATTRIBUTE42 = '';
            objHeaderBean.ATTRIBUTE43 = '';
            objHeaderBean.ATTRIBUTE44 = '';
            objHeaderBean.ATTRIBUTE45 = '';
            objHeaderBean.ATTRIBUTE46 = '';
            objHeaderBean.ATTRIBUTE47 = '';
            objHeaderBean.ATTRIBUTE48 = '';
            objHeaderBean.ATTRIBUTE49 = '';
            objHeaderBean.ATTRIBUTE50 = '';
            listObjBeans.add(objHeaderBean);
            
            // create TASK bean
            TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objTaskBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
            objTaskBean.PARAM_ID = '2-'+mapCaseDetails.get(objTask.WhatId).CaseNumber;
            objTaskBean.ATTRIBUTE1 = 'TASK';
            objTaskBean.ATTRIBUTE2 = objTask.Subject;
            system.debug('subject*****'+objTask.Subject);
            if(objTask.Status.equalsIgnoreCase('Cancelled')){
                objTaskBean.ATTRIBUTE3 = 'Cancel';
            }else if(objTask.Status.contains('Reject')
            || objTask.Status.equalsIgnoreCase('Aborted')){
                objTaskBean.ATTRIBUTE3 = 'Reject';
            }else if(objTask.Status.equalsIgnoreCase('Completed')
                || objTask.Status.equalsIgnoreCase('Closed')){
                objTaskBean.ATTRIBUTE3 = 'Closed';
            }else{
                objTaskBean.ATTRIBUTE3 = 'Open';
            }
            //objTaskBean.ATTRIBUTE3 = objTask.Status;
            system.debug('status*****'+objTask.Status);
            objTaskBean.ATTRIBUTE4 = objTask.IPMS_Role__c;
            objTaskBean.ATTRIBUTE5 = '';
            objTaskBean.ATTRIBUTE6 = '';
            objTaskBean.ATTRIBUTE7 = String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()); // format this as DD-MON- YYYY
            system.debug('created date*****'+String.valueOf(objTask.CreatedDate.format('dd-MMM-yyyy').toUpperCase()));
            objTaskBean.ATTRIBUTE8 = objTask.Id;
            system.debug('task id*****'+objTask.Id);
            Datetime dt = objTask.ActivityDate;
            objTaskBean.ATTRIBUTE9 = String.valueOf(dt.format('dd-MMM-yyyy').toUpperCase());
            system.debug('due date*****'+String.valueOf(dt.format('dd-MMM-yyyy').toUpperCase()));
            objTaskBean.ATTRIBUTE10 = '';
            objTaskBean.ATTRIBUTE11 = objTask.WhatId;
            objTaskBean.ATTRIBUTE12 = objTask.Subject;
            objTaskBean.ATTRIBUTE13 = objTask.Status;
            objTaskBean.ATTRIBUTE14 = objTask.Type;
            objTaskBean.ATTRIBUTE15 = objTask.Description;
            objTaskBean.ATTRIBUTE16 = objTask.Assigned_User__c;
            objTaskBean.ATTRIBUTE17 = mapCaseDetails.get(objTask.WhatId).Relationship_with_Seller__c;
            objTaskBean.ATTRIBUTE18 = mapCaseDetails.get(objTask.WhatId).Case_Type__c;
            objTaskBean.ATTRIBUTE19 = mapCaseDetails.get(objTask.WhatId).Buyer_Type__c;
            objTaskBean.ATTRIBUTE20 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Unit_Details__c;
            objTaskBean.ATTRIBUTE21 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Inventory__r.Property_City__c;
            objTaskBean.ATTRIBUTE22 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Inventory__r.Property_Status__c;
            objTaskBean.ATTRIBUTE23 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Inventory__r.Property_Country__c;
            objTaskBean.ATTRIBUTE24 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Inventory__r.Property__r.DIFC__c == true ? 'Yes' : 'No';
            objTaskBean.ATTRIBUTE25 = mapCaseDetails.get(objTask.WhatId).Buyer__r.First_Name__c;
            objTaskBean.ATTRIBUTE26 = mapCaseDetails.get(objTask.WhatId).Buyer__r.Last_Name__c;
            objTaskBean.ATTRIBUTE27 = mapCaseDetails.get(objTask.WhatId).Buyer__r.Nationality__c;
            objTaskBean.ATTRIBUTE28 = mapCaseDetails.get(objTask.WhatId).Buyer__r.Passport_Number__c;
            objTaskBean.ATTRIBUTE29 = mapCaseDetails.get(objTask.WhatId).Buyer__r.Passport_Expiry_Date__c;
            objTaskBean.ATTRIBUTE30 = mapCaseDetails.get(objTask.WhatId).Buyer__r.IPMS_Buyer_ID__c;
            objTaskBean.ATTRIBUTE31 = mapCaseDetails.get(objTask.WhatId).Seller__r.FirstName;
            objTaskBean.ATTRIBUTE32 = mapCaseDetails.get(objTask.WhatId).Seller__r.LastName;
            objTaskBean.ATTRIBUTE33 = mapCaseDetails.get(objTask.WhatId).Seller__r.Nationality__pc;
            objTaskBean.ATTRIBUTE34 = mapCaseDetails.get(objTask.WhatId).Seller__r.Party_ID__c;
            if(objTask.Subject.contains('SELLER POA')){
                objTaskBean.ATTRIBUTE35 = objTask.Document_URL__c;
                objTaskBean.ATTRIBUTE36 = mapCaseDetails.get(objTask.WhatId).Purpose_of_POA_Seller__c;
            }
            else if(objTask.Subject.contains('BUYER POA')){
                objTaskBean.ATTRIBUTE35 = objTask.Document_URL__c;
                objTaskBean.ATTRIBUTE36 = mapCaseDetails.get(objTask.WhatId).Purpose_of_POA__c;
            }else{
                objTaskBean.ATTRIBUTE35 = '';
                objTaskBean.ATTRIBUTE36 = '';
            }
            objTaskBean.ATTRIBUTE37 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Rental_Pool__c ? 'Yes' : 'No';
            objTaskBean.ATTRIBUTE38 = '';
            objTaskBean.ATTRIBUTE39 = '';
            objTaskBean.ATTRIBUTE41 = '';
            objTaskBean.ATTRIBUTE42 = '';
            objTaskBean.ATTRIBUTE43 = '';
            objTaskBean.ATTRIBUTE44 = '';
            objTaskBean.ATTRIBUTE45 = '';
            objTaskBean.ATTRIBUTE46 = '';
            objTaskBean.ATTRIBUTE47 = '';
            objTaskBean.ATTRIBUTE48 = '';
            objTaskBean.ATTRIBUTE49 = '';
            objTaskBean.ATTRIBUTE50 = '';
            listObjBeans.add(objTaskBean);
            
            // create UNIT bean
            TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objUnitBean = new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
            objUnitBean.PARAM_ID = '2-'+mapCaseDetails.get(objTask.WhatId).CaseNumber;
            objUnitBean.ATTRIBUTE1 = 'UNITS';
            if(mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Rental_Pool__c){
                objUnitBean.ATTRIBUTE2 = 'Rental Pool Assignment';
            }else{
                objUnitBean.ATTRIBUTE2 = 'Assignment';
            }
            objUnitBean.ATTRIBUTE3 = '';
            objUnitBean.ATTRIBUTE4 = '';
            objUnitBean.ATTRIBUTE5 = '';
            objUnitBean.ATTRIBUTE6 = mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c;
            system.debug('reg*****'+mapCaseDetails.get(objTask.WhatId).Booking_Unit__r.Registration_ID__c);
            objUnitBean.ATTRIBUTE7 = '';
            objUnitBean.ATTRIBUTE8 = '';
            objUnitBean.ATTRIBUTE9 = '';
            objUnitBean.ATTRIBUTE10 = '';
            objUnitBean.ATTRIBUTE11 = '';
            objUnitBean.ATTRIBUTE12 = '';
            objUnitBean.ATTRIBUTE13 = '';
            objUnitBean.ATTRIBUTE14 = '';
            objUnitBean.ATTRIBUTE15 = '';
            objUnitBean.ATTRIBUTE16 = '';
            objUnitBean.ATTRIBUTE17 = '';
            objUnitBean.ATTRIBUTE18 = '';
            objUnitBean.ATTRIBUTE19 = '';
            objUnitBean.ATTRIBUTE20 = '';
            objUnitBean.ATTRIBUTE21 = '';
            objUnitBean.ATTRIBUTE22 = '';
            objUnitBean.ATTRIBUTE23 = '';
            objUnitBean.ATTRIBUTE24 = '';
            objUnitBean.ATTRIBUTE25 = '';
            objUnitBean.ATTRIBUTE26 = '';
            objUnitBean.ATTRIBUTE27 = '';
            objUnitBean.ATTRIBUTE28 = '';
            objUnitBean.ATTRIBUTE29 = '';
            objUnitBean.ATTRIBUTE30 = '';
            objUnitBean.ATTRIBUTE31 = '';
            objUnitBean.ATTRIBUTE32 = '';
            objUnitBean.ATTRIBUTE33 = '';
            objUnitBean.ATTRIBUTE34 = '';
            objUnitBean.ATTRIBUTE35 = '';
            objUnitBean.ATTRIBUTE36 = '';
            objUnitBean.ATTRIBUTE37 = '';
            objUnitBean.ATTRIBUTE38 = '';
            objUnitBean.ATTRIBUTE39 = '';
            objUnitBean.ATTRIBUTE41 = '';
            objUnitBean.ATTRIBUTE42 = '';
            objUnitBean.ATTRIBUTE43 = '';
            objUnitBean.ATTRIBUTE44 = '';
            objUnitBean.ATTRIBUTE45 = '';
            objUnitBean.ATTRIBUTE46 = '';
            objUnitBean.ATTRIBUTE47 = '';
            objUnitBean.ATTRIBUTE48 = '';
            objUnitBean.ATTRIBUTE49 = '';
            objUnitBean.ATTRIBUTE50 = '';
            listObjBeans.add(objUnitBean);
            
            TaskCreationWSDL.TaskHttpSoap11Endpoint objClass = new TaskCreationWSDL.TaskHttpSoap11Endpoint();
            objClass.timeout_x = 120000;
            String response = objClass.SRDataToIPMSMultiple(reqNo, 'CREATE_SR', 'SFDC', listObjBeans);
            system.debug('resp*****'+response);
            if(String.isNotBlank(response)) {
                innerClass IC = (innerClass)JSON.deserialize(response, innerClass.class);
                system.debug('IC*****'+IC);
                if(IC.status.EqualsIgnoreCase('S')){
                    objTask.Pushed_to_IPMS__c = true;
                }else{
                    objTask.Pushed_to_IPMS__c = false;
                    objTask.Update_IPMS__c = false;
                    objTask.Task_Error_Details__c = IC.status;
                    system.debug('objTask=='+objTask);
                    Error_Log__c objErr = createErrorLogRecord(mapCaseDetails.get(objTask.WhatId).Seller__c, mapCaseDetails.get(objTask.WhatId).Booking_Unit__c, objTask.WhatId);
                    objErr.Error_Details__c = objTask.Subject+' Failed with '+IC.message;
                    listErrorLog.add(objErr);
                }
                
            }
            else {
                Error_Log__c objErr = createErrorLogRecord(mapCaseDetails.get(objTask.WhatId).Seller__c, mapCaseDetails.get(objTask.WhatId).Booking_Unit__c, objTask.WhatId);
                objErr.Error_Details__c = objTask.Subject+' Failed with Error : No Response from IPMS for Task Creation';
                listErrorLog.add(objErr);
            }
            /*if(String.isNotBlank(response)) {
                mapDeserializeTask = (map<String, Object>)JSON.deserializeUntyped(response);
                system.debug('mapDeserializeTask*****'+mapDeserializeTask);
                if(mapDeserializeTask.get('status') == 'E') {
                    objTask.Pushed_to_IPMS__c = false;
                    objTask.Task_Error_Details__c = 'E';

                    Error_Log__c objErr = createErrorLogRecord(mapCaseDetails.get(objTask.WhatId).Seller__c, mapCaseDetails.get(objTask.WhatId).Booking_Unit__c, objTask.WhatId);
                    objErr.Error_Details__c = (String)mapDeserializeTask.get('message');
                    listErrorLog.add(objErr);
                }
                else {
                    objTask.Pushed_to_IPMS__c = true;
                }
            }
            else {
                Error_Log__c objErr = createErrorLogRecord(mapCaseDetails.get(objTask.WhatId).Seller__c, mapCaseDetails.get(objTask.WhatId).Booking_Unit__c, objTask.WhatId);
                objErr.Error_Details__c = 'Error : No Response from IPMS for Task Creation';
                listErrorLog.add(objErr);
            }*/

            /*innerClass IC = (innerClass)JSON.deserialize(response, innerClass.class);
            system.debug('IC*****'+IC);
            if(IC.status.EqualsIgnoreCase('E')){
                objTask.Pushed_to_IPMS__c = false;
                objTask.Task_Error_Details__c = IC.status;
            }else{
                objTask.Pushed_to_IPMS__c = true;
            }*/
            
            lsTaskToUpdate.add(objTask);
        } // end of for loop
        if(!lsTaskToUpdate.isEmpty()){
            update lsTaskToUpdate;
        }
        if(!listErrorLog.isEmpty()) {
            insertErrorLog(listErrorLog);
        }
    } // end of method
    
    public class innerClass{
        public string message;
        public string status;
        
        public innerClass(){
            
        }
    }

    public static void insertErrorLog(list<Error_Log__c> listObjErr){
        try{
            insert listObjErr;
        }catch(Exception ex){
            //errorMessage = 'Error : '+ ex.getMessage();
            system.debug('Error Log ex*****'+ex);
        }
    }

    public static Error_Log__c createErrorLogRecord(Id accId, Id bookingUnitId, Id caseId){
        Error_Log__c objErr = new Error_Log__c();
        objErr.Account__c = accId;
        objErr.Booking_Unit__c = bookingUnitId;
        objErr.Case__c = caseId;
        return objErr;
    }
} // end of class