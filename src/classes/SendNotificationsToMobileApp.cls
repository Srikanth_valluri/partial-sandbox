@RestResource(urlMapping='/SendNotificationsToMobileApp/*')
 Global class SendNotificationsToMobileApp
 {
     @HtTPPost
    Global static list<Notifications__c> SendNotificationsToMobileApp(String accountId)
    {
       list<Notifications__c> nList=[select id,Account__c,name,Case__c,Notification_Text__c,Read__c,createdDate from Notifications__c where Read__c=false and createdbyid=:accountId ORDER BY createddate DESC];
       return nList;
    
    }
 }