/*-------------------------------------------------------------------------------------------------
Description: Test class for FM_AmenityFMUserNotifications
============================================================================================================================
Version | Date(DD-MM-YYYY) | Last Modified By | Comments
----------------------------------------------------------------------------------------------------------------------------
1.0     | 22-11-2017       | Lochana Rajput   | 1. Draft
=============================================================================================================================
*/
@isTest
private class FM_AmenityFMUserNotificationsTest {
	@testSetup
    static void createCommonTestData() {
		Account objAcc = TestDataFactoryFM.createAccount();
        insert objAcc;

        Id personAccountId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acctInsOwner=new Account(RecordTypeID = personAccountId,PersonEmail='a@g.com',FirstName = 'Test FName Owner',
                                    LastName = 'Test LName',Passport_Number__c='12345678',Mobile__c='1212121212',Address_Line_1__c='test');
        insert acctInsOwner;
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        objSR = TestDataFactoryFM.createServiceRequest(objAcc);
        insert objSR;
        Booking__c objBooking = TestDataFactoryFM.createBooking(acctInsOwner, objSR);
        insert objBooking;
        Booking_unit__c objBu = TestDataFactoryFM.createBookingUnit(objAcc, objBooking);
        objBu.Unit_Name__c='JNU/SD168/XH2910B';
        insert objBu;
		Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
		Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;
        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;
		Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                            get('Amenity Booking').getRecordTypeId();
        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Booking_Date__c = Date.Today();
        objFMCase.Resource_Booking_Type__c = 'FM Amenity';
        objFMCase.RecordTypeId =devRecordTypeId;
        objFMCase.Booking_Start_Time_p__c = '10:00';
        objFMCase.Booking_End_Time_p__c = '10:30';
		objFMCase.Booking_Unit__c = objBu.Id;
        objFMCase.Amenity_Booking_Status__c = 'Booking Confirmed';
        insert objFMCase;
	}
	@isTest static void test_BookingConfirmed() {
		FM_Case__c objFMCase = [SELECT Id, Amenity_Booking_Status__c,Unit_Name__c from FM_Case__c LIMIT 1];
		Test.startTest();
		FM_AmenityFMUserNotifications.sendNotificationEmails(new List<FM_Case__c>{objFMCase});
		Test.stopTest();
	}
	@isTest static void test_BookingRejected() {
		FM_Case__c objFMCase = [SELECT Id, Amenity_Booking_Status__c,Unit_Name__c from FM_Case__c LIMIT 1];
		objFMCase.Amenity_Booking_Status__c = 'Rejected';
		update objFMCase;
		Test.startTest();
		FM_AmenityFMUserNotifications.sendNotificationEmails(new List<FM_Case__c>{objFMCase});
		Test.stopTest();
	}
	@isTest static void test_BookingCancelled() {
		FM_Case__c objFMCase = [SELECT Id, Amenity_Booking_Status__c,Unit_Name__c from FM_Case__c LIMIT 1];
		objFMCase.Amenity_Booking_Status__c = 'Cancelled';
		update objFMCase;
		Test.startTest();
		FM_AmenityFMUserNotifications.sendNotificationEmails(new List<FM_Case__c>{objFMCase});
		Test.stopTest();
	}
}