public class CustomerNotificationSendSMSBatchSched  implements Schedulable{
    
    public void execute(SchedulableContext SC) {
        List<SMS_History__c> lstSMSHistoryIds = new list<SMS_History__c>();
        for( SMS_History__c objSms : [Select Id ,Name, SMS_Id__c ,Message__c 
                                        From 
                                            SMS_History__c 
                                        where 
                                            Delivery_Status__c = '' AND SMS_Id__c = ''
                                        AND 
                                            CreatedDate = LAST_N_DAYS:30
                                        AND 
                                            Phone_Number__c != '' 
                                        AND
                                            Is_SMS_Sent__c = false] ) {
            if( String.IsNotBlank(objSms.Message__c))
                lstSMSHistoryIds.add( objSms);
        }
        database.executebatch(new CustomerNotificationSendSMSBatch(lstSMSHistoryIds),1);
    }
     
}