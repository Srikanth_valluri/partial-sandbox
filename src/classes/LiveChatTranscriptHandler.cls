/*
 * Description: LiveChatTranscript Trigger Handler to send content of chat through email.
                Changes on :Jan 20 2019 - Added recordtype exclude logic in the Query so that emails for 
                Agent Portal Chats will not be sent to the Agnet portal users
 */

public class LiveChatTranscriptHandler {

    /**
     * Method to send content of chat through email
     */  
    public void sendEmail(List<LiveChatTranscript> lstTranscripts) {
        Map<LiveChatTranscript, String> mapTranscriptAccountId = new Map<LiveChatTranscript, String>();
        Map<LiveChatTranscript, String> mapTranscriptContactId = new Map<LiveChatTranscript, String>();
        List<Customer> lstCustomer = new List<Customer>();

        for (LiveChatTranscript objTranscript : lstTranscripts) {
            if (String.isNotBlank(objTranscript.Body)) {
                if (objTranscript.AccountId != null) {
                     mapTranscriptAccountId.put(objTranscript, objTranscript.AccountId);
                } else if (objTranscript.ContactId != null) {
                     mapTranscriptContactId.put(objTranscript, objTranscript.ContactId);
                }
            }
        }

    
        // Associate Transcript with Account    
        if (!mapTranscriptAccountId.isEmpty()) {
            Map<Id, Account> mapAccounts = new Map<Id, Account>([SELECT Id, Name, Email__pc, Email__c, IsPersonAccount FROM Account 
                Where Id In: mapTranscriptAccountId.values()
                AND Recordtype.name!='Corporate Agency']); // Exclude the recordtype 

            for (LiveChatTranscript objTranscript : mapTranscriptAccountId.keySet()) {
                if (mapAccounts.containsKey(objTranscript.AccountId)) {
                    Account objAccount = mapAccounts.get(objTranscript.AccountId);
                    Customer objCustomer = new Customer();
                    objCustomer.transcript = objTranscript;
                    objCustomer.name = objAccount.Name;
                    objCustomer.emailId = objAccount.Email__c;
                    objCustomer.parentId = objAccount.Id;
                    lstCustomer.add(objCustomer);
                }
            }
        }
    
        // Associate Transcript with Contact    
        if (!mapTranscriptContactId.isEmpty()) {
            Map<Id, Contact> mapContacts = new Map<Id, Contact>([SELECT Id, Name, Email, AccountId FROM Contact 
            Where Id In: mapTranscriptContactId.values()
            AND Account_RecordType__c!='Corporate Agency']);// Exclude the recordtype 

            for (LiveChatTranscript objTranscript : mapTranscriptContactId.keySet()) {
                if (mapContacts.containsKey(objTranscript.ContactId)) {
                    Contact objContact = mapContacts.get(objTranscript.ContactId);
                    Customer objCustomer = new Customer();
                    objCustomer.transcript = objTranscript;
                    objCustomer.name = objContact.Name;
                    objCustomer.emailId = objContact.Email;
                    if (objContact.AccountId != null) {
                        objCustomer.parentId = objContact.AccountId;
                    } else {
                        objCustomer.parentId = objContact.Id;
                    }
                    lstCustomer.add(objCustomer);
                }
            }
        }
        
        if (!lstCustomer.isEmpty()) {

            List<Messaging.SingleEmailMessage> messages =   new List<Messaging.SingleEmailMessage>();
            EmailTemplate reqEmailTemplate =  [SELECT ID, Subject, Body, BrandTemplateId, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'Live_Chat_Transcript_Creation' limit 1];
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'no-replysf@damacgroup.com'];

            for (Customer objCustomer : lstCustomer) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                if (String.isNotBlank(objCustomer.parentId)) {
                    mail.setWhatId(objCustomer.parentId);
                    mail.setSaveAsActivity(true);
                }
                List<String> sendTo = new List<String>();
                String body, sub;
                if (reqEmailTemplate != null) {
                    body = reqEmailTemplate.HtmlValue;
                    sub = reqEmailTemplate.Subject;
                    body = body.replace('{!LiveChatTranscript.Body}', objCustomer.transcript.Body);
                    body = body.replace('{!Customer}', objCustomer.name);
                    body = body.replace(']]>', '');
                    mail.setHtmlBody(body);
                    mail.setSubject(sub);
                    if (String.isNotBlank(objCustomer.emailId)) {
                        sendTo.add(objCustomer.emailId);
                    }
                    if (!sendTo.isEmpty()) {
                        mail.setToAddresses(sendTo);    
            
                        if (String.isNotBlank(body)) {
                            messages.add(mail);
                        }

                        if (owea.size() > 0 ) {
                            mail.setOrgWideEmailAddressId(owea.get(0).Id);
                        }   
                    }
                }
            }
            
            if (!messages.isEmpty()) {
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            }
        }
    }
    
    public Class Customer {
        public LiveChatTranscript transcript;
        public String name;
        public String emailId;
        public String parentId;
    }
}