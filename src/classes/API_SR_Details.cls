/**********************************************************************************************************************
Class Name : API_SR_Details 
Description: To get the SR details and collections, commissions total of an agent.
Test Class : API_SR_Details_Test
========================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.1     |   15-10-2020      | Subin C Antony	  | Added HTTP callout to IPMS to fetch payment/payment-qualification statuses for BU. Added the additional info to API response.
1.1     |   28-10-2020      | Subin C Antony	  | Changes in remote URL for fetching invoice details from IPMS.
***********************************************************************************************************************/

@RestResource(urlMapping='/API_SRDetails')
global without sharing class API_SR_Details {
    public static Boolean IsSandbox;
    static {
        IsSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }

    @HttpGET
    global static void doGET() {
        // inner class to hold the json data
        AgentSummary obj = new AgentSummary ();
        obj.totalBookingAmount = 0;
        obj.totalCollectionAmount = 0;
        obj.totalCommissionAmount = 0;

        Set <ID> srIds = new Set <ID> ();
        //Set <ID> buIds = new Set <ID> ();
        User u = [SELECT ContactId, profile.Name, AccountId FROM User WHERE ID =: UserInfo.getUserId ()];
        
        Set <ID> agentIds = new Set <ID> ();
        agentIds.add (UserInfo.getUserId());
        
        Set <ID> contactIds = new Set <ID> ();
        contactIds.add (u.contactId);
        
        // To fetch the account related contact and contact realted user's SR details when the API request coming from Super User Profile
        if (u.profile.Name == 'Customer Community - Super User' || Test.isRunningTest()) {
            Set <ID> conIds = new Set <ID> ();
            for (Contact con : [SELECT ID FROM Contact WHERE accountId =: u.accountId]) {
                conIds.add (con.ID);
            }
            for (User usr : [SELECT ContactId, AccountID FROM User WHERE ContactId IN :conIds AND IsActive = TRUE]) {
                contactIds.add (usr.contactId);
                agentIds.add (usr.id);
            }
        }
        
         
                
        // to get the total collections, booking amount from SR
        
        if (u.profile.Name == 'Customer Community - Super User' || Test.isRunningTest()) {
            for (NSIBPM__Service_Request__c sr : [SELECT NSIBPM__Internal_Status_Name__c, Total_Booking_Amount__c, Token_Amount_AED__c, List_of_Units__c 
                                                    FROM NSIBPM__Service_Request__c 
                                                    WHERE 
                                                    NSIBPM__External_Status_Code__c != 'Draft'
                                                    AND 
                                                    Agency__r.Id=:u.accountId]) //OR NSIBPM__Contact__c IN: contactIds
                                                   
            {
                srIds.add (sr.Id);
                if (sr.Total_Booking_Amount__c != NULL){
                    //obj.totalBookingAmount += sr.Total_Booking_Amount__c;
                }
                
            }
            System.debug('SR_IDs .............................: ' + srIds);
        }
        if (u.profile.Name != 'Customer Community - Super User' || Test.isRunningTest()) {
                for (NSIBPM__Service_Request__c sr : [SELECT NSIBPM__Internal_Status_Name__c, Total_Booking_Amount__c, Token_Amount_AED__c, List_of_Units__c 
                                                    FROM NSIBPM__Service_Request__c 
                                                    WHERE 
                                                    NSIBPM__External_Status_Code__c != 'Draft'
                                                    AND 
                                                    (Agent_Name__c IN: agentIds)]) //OR NSIBPM__Contact__c IN: contactIds
                                                   
                {
                    srIds.add (sr.Id);
                    if (sr.Total_Booking_Amount__c != NULL){
                        //obj.totalBookingAmount += sr.Total_Booking_Amount__c;
                    }
                    
                }
            
            System.debug('SR_IDs .............................: ' + srIds);
        }
        
        
        //obj.noofsrs = string.valueof(srIds.size());
        
        for (Booking_Unit__c bUnit : [SELECT Booking__c, total_Collections__c,Requested_Price__c, Booking__r.Deal_Sr__r.NSIBPM__External_Status_Code__c
                                    FROM Booking_Unit__c
                                    WHERE 
                                    Booking__r.Deal_Sr__c IN: srIds
                                    AND Booking__r.Deal_Sr__c != null])
        {
            //buIds.add(bUnit.id);
            if (bUnit.total_Collections__c != NULL){
                obj.totalCollectionAmount += bUnit.total_Collections__c;
            }
            
            if(bUnit.Requested_Price__c != NULL && bUnit.Booking__r.Deal_Sr__r.NSIBPM__External_Status_Code__c == 'SPA_Executed'){
                obj.totalBookingAmount += bUnit.Requested_Price__c;
            }
                                                
        }
        
        
        //obj.noofbus = string.valueof(buIds.size()); 
        
        // To get the commission amount for the agent
        for (Agent_Commission__c commission : [SELECT Amount__c, Booking_Unit__c 
                                                FROM Agent_Commission__c 
                                                WHERE Booking__r.Deal_SR__c IN :srIds
                                                AND Amount__c != null])
        {
            obj.totalCommissionAmount += commission.Amount__c;
        }
        
        List <SRDetails> srDetails = new List <SRDetails> ();
        Set <String> bookingIDSet = new Set<String>();
        // regIdSet.add('108358'); // COMMENT LATER ################################################
        for (Buyer__c buyer : [SELECT id, Booking__c, Booking__r.Deal_Sr__c, Damac_Agent_Action_Type__c, inquiry__c, CreatedById,
                                Booking__r.Deal_Sr__r.App_Booking_Status__c, Booking_Unit__c, Booking_Unit__r.Registration_ID__c,
                                Booking__r.Deal_Sr__r.List_of_Units__c,
                                Booking__r.Deal_Sr__r.Name, Booking__r.Deal_Sr__r.IPMS_Registration_ID__c,
                                Title__c, First_Name__c, Last_Name__c,
                                Booking__r.Deal_Sr__r.NSIBPM__Internal_SR_Status__r.Name 
                                FROM Buyer__c 
                                WHERE Primary_Buyer__c = true 
                                AND 
                                Booking__r.Deal_Sr__c IN :srIds ORDER BY Booking__r.Deal_Sr__r.Registration_DateTime__c DESC NULLS LAST])
        {
            String buyerName = '';
            if (buyer.Title__c != NULL)
                buyerName = buyer.Title__c+' ';
            
            if (buyer.First_Name__c != NULL)
                buyerName += buyer.First_Name__c+' ';
                
            if (buyer.Last_Name__c != NULL)
                buyerName += buyer.Last_Name__c;
           
                
            SRDetails srDetail = new SRDetails ();
            srDetail.buyerId = buyer.id;
            srDetail.buyerName = buyerName;
            srDetail.srId = buyer.Booking__r.Deal_Sr__c;
            srDetail.srStatus = buyer.Booking__r.Deal_Sr__r.App_Booking_Status__c;
            srDetail.srNumber = buyer.Booking__r.Deal_Sr__r.Name;
            srDetail.unitName = buyer.Booking__r.Deal_Sr__r.List_of_Units__c;
            srDetail.internalSrStatus = buyer.Booking__r.Deal_Sr__r.NSIBPM__Internal_SR_Status__r.Name;
            srDetail.bookingId = buyer.Booking__c;
            srDetail.buyerStatus = buyer.Damac_Agent_Action_Type__c;
            srDetail.inquiryId = buyer.inquiry__c;
            srDetail.bookingUnitId = buyer.Booking_Unit__c; 
            // srDetail.registrationId = buyer.Booking_Unit__r.Registration_ID__c;
            srDetail.createdUserId = buyer.CreatedById;         
            srDetails.add (srDetail);
            bookingIDSet.add(buyer.Booking__c);
        }
        
        /* Section Modified on 15 Oct 2020 : to fetch RegID for BU and get payment Status from IPMS ......................BEGIN */
        Set <String> regIdSet = new Set<String>();  /* Added on 15 Oct 2020 */
        Map <String, List<String>> buRegIdListByBoookingMap = new Map<String, List<String>>(); /* Added on 15 Oct 2020 */
        List <Booking_Unit__c> agent_BU_List = [SELECT id, name, Unit_Name__c, Registration_ID__c, Booking__c FROM Booking_Unit__c 
                                               WHERE Booking__c IN :bookingIDSet];
        for(Booking_Unit__c obj_BU : agent_BU_List) {
            if(String.isNotBlank(obj_BU.Registration_ID__c)) {
                regIdSet.add(obj_BU.Registration_ID__c); /* adding to Set of RegID to Query from IPMS by callout */
            }
            List <String> buRegIdList;
            if(buRegIdListByBoookingMap.containsKey(obj_BU.Booking__c)) {
                buRegIdList = buRegIdListByBoookingMap.get(obj_BU.Booking__c);
                if(NULL == buRegIdList) {
                    buRegIdList = new List<String>();
                }
                buRegIdList.add(obj_BU.Registration_ID__c);
                buRegIdListByBoookingMap.put(obj_BU.Booking__c, buRegIdList);
            }
            else {
                buRegIdList = new List<String>();
                buRegIdList.add(obj_BU.Registration_ID__c);
                buRegIdListByBoookingMap.put(obj_BU.Booking__c, buRegIdList);
            }
        } /* for(Booking_Unit__c obj_BU : agent_BU_List) */
        
        IPMS_ResponseWraper remotePaymentStatusResponse = getPaymentStatusFromIPMS(regIdSet);
        Map <String, IPMS_ResponseDataItem> paymentStatusMapByRegId = new Map<String, IPMS_ResponseDataItem>();
        if(remotePaymentStatusResponse.meta_data.status_code == 1 && NULL != remotePaymentStatusResponse.data && remotePaymentStatusResponse.data.size() > 0) {
            for(IPMS_ResponseDataItem item : remotePaymentStatusResponse.data){
                paymentStatusMapByRegId.put(item.registration_id, item);
            }
        }
        System.debug('IPMS_DATA_MAP  : ............................................ ' + paymentStatusMapByRegId);
        
        for(SRDetails srDetail : srDetails) {
            List<String> sr_regIds = new List<String>();
            if(buRegIdListByBoookingMap.containsKey(srDetail.bookingId)) {
                sr_regIds = buRegIdListByBoookingMap.get(srDetail.bookingId);
                srDetail.invoice_types = new Set<String>();  // init
                srDetail.policy_run_qualification = '';  // init
                srDetail.payment_status = '';  // init
                for(String bu_RegID : sr_regIds) {
                    String seperator = '';
                    if(paymentStatusMapByRegId.containsKey(bu_RegID)) {
                        IPMS_ResponseDataItem remotePaymentStatusData = paymentStatusMapByRegId.get(bu_RegID);
                        if(NULL != remotePaymentStatusData && NULL != remotePaymentStatusData.invoice_types && remotePaymentStatusData.invoice_types.size() > 0) {
                            srDetail.invoice_types.addAll(remotePaymentStatusData.invoice_types);
                        }
                        if(NULL != remotePaymentStatusData) {
                            String qualifyStat =  String.isNotBlank(remotePaymentStatusData.policy_run_qualification) ? 
                                remotePaymentStatusData.policy_run_qualification : '';
                            String paymentStat =  String.isNotBlank(remotePaymentStatusData.payment_status) ? 
                                remotePaymentStatusData.payment_status : '';
                            
                            srDetail.policy_run_qualification += (seperator + qualifyStat);
                            srDetail.payment_status += (seperator + paymentStat);
                            seperator = ' , ';
                        }
                    }
                }
                
                /* COMMENT LATER ####################################################################### */
                for(String invTyp : srDetail.invoice_types) {
                    if(invTyp.equalsIgnoreCase('invoice')) {
                        srDetail.invoice_types.remove(invTyp);
                        srDetail.invoice_types.add('Tax_Invoice');
                    }
                } /* COMMENT LATER ##################################################################### */
            }
        }
        /* Section Modified on 15 Oct 2020 : to fetch RegID for BU and get payment Status from IPMS ......................END */
        
        obj.srDetails = srDetails;
        
        // To send response back to API
        RestResponse response = RestContext.response;
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(JSON.serialize(obj));
    }
    
    /*
     * Method to perform HTTP callout to IPMS for payment status */  /* Added on 15 Oct 2020 */
    private static IPMS_ResponseWraper getPaymentStatusFromIPMS(Set<String> regIds) {
        IPMS_ResponseWraper responseWraper = new IPMS_ResponseWraper();
        IPMS_ResponseMetaData metaData = new IPMS_ResponseMetaData();
        List <IPMS_ResponseDataItem> responseDataList = new List<IPMS_ResponseDataItem>();
        
        if(regIds.size() < 1) {
            responseWraper.meta_data = getErrorMetaDataForResponse(3,'Bad Request', 'Empty list of Arguments', NULL);
        }
        else {
            String regIdStringQuery = '';
            String seperator = '';
            for(String regId : regIds){
                regIdStringQuery += (seperator + regId);
                seperator = ',';
            }
            System.debug('REG_ID_QUERY_String : ............................................ ' + regIdStringQuery);
            
            HttpRequest paymentStatusRequest = new HttpRequest();
            String baseUrl = IsSandbox ? 'https://ptctest.damacgroup.com/agent-app/api/v1/' : 'https://ptc.damacgroup.com/agentapp-prod/api/v1/';
            paymentStatusRequest.setEndpoint(baseUrl + 'customers/registrations?reg-id=' + regIdStringQuery);
            paymentStatusRequest.setMethod('GET');
            paymentStatusRequest.setTimeout(120000);
            
            HttpResponse paymentStatusResponse;
            String exceptionMsg;
            try{
                if(!Test.isRunningTest()) {
                    paymentStatusResponse = new Http().send(paymentStatusRequest);
                }
                else {
                    // for test excecution context
                    paymentStatusResponse  = new HttpResponse();
                    paymentStatusResponse.setStatus('OK');
                    paymentStatusResponse.setStatusCode(200);
                    paymentStatusResponse.setBody('{"data":[{"invoice_types":["Invoice"],"policy_run_qualification":"Audit Completed,Doc Ok,Deposit Received,Collection Received","payment_status":null,"registration_id":1234,"agent_id":null}],"meta_data":{"status_code":1,"message":"Success","title":"Success","developer_message":null}}');
                }
                
                system.debug('IPMS_CALLOUT_Response : ............................................ ' + paymentStatusResponse);
                system.debug('IPMS_CALLOUT_ResponseStatus : ............................................ ' + paymentStatusResponse.getStatusCode());
                system.debug('IPMS_CALLOUT_ResponseBody : ............................................ ' + paymentStatusResponse.getBody());
                if(paymentStatusResponse.getStatusCode() == 200 && NULL != paymentStatusResponse.getBody() && 
                   !(paymentStatusResponse.getBody().equals(''))){
                    responseWraper = (IPMS_ResponseWraper)JSON.deserialize(paymentStatusResponse.getBody(), IPMS_ResponseWraper.class);
                }
            }
            catch(Exception ex){
                system.debug(ex.getMessage());
                system.debug(ex.getStackTraceString());
                responseWraper = NULL;
                exceptionMsg = ex.getMessage() + ex.getStackTraceString();
            }
            
            if(NULL == responseWraper || NULL == responseWraper.data || responseWraper.data.size() < 1) {
                responseWraper = new IPMS_ResponseWraper();
                responseWraper.meta_data = getErrorMetaDataForResponse(2,'Failure', 'Error while fetching data', exceptionMsg);
            }
        }
        
        System.debug('IPMS_CALLOUT_RESPONSE_WRAP : ................................... ' + responseWraper);
        return responseWraper;
    }
    
    private static IPMS_ResponseMetaData getErrorMetaDataForResponse(Integer code, String title, String message, String devMsg) {
        IPMS_ResponseMetaData metaData = new IPMS_ResponseMetaData();
        metaData.status_code = code;
        metaData.title = title;
        metaData.message = message;
        metaData.developer_message = devMsg;
        return metaData;
    }
    
    public class IPMS_ResponseWraper {
        public IPMS_ResponseMetaData meta_data;
        public List <IPMS_ResponseDataItem> data;
    }
    
    public class IPMS_ResponseMetaData {
        public Integer status_code;
        public String message;
        public String title;
        public String developer_message;
    }
    
    public class IPMS_ResponseDataItem {
        public String registration_id;
        public String agent_id;
        public List<String> invoice_types;
        public String policy_run_qualification;
        public String payment_status;
    }
    
    // To prepare the summary JSON header as response
    public class AgentSummary {
        public decimal totalBookingAmount;
        public decimal totalCollectionAmount;
        public decimal totalCommissionAmount;
        public List <SRDetails> srDetails;  
        //public string noofsrs;      
        //public string noofbus;      
    }
    
    // To prepare the SR details JSON as response
    public class SRDetails {
        public String buyerId;
        public String srId;
        public String inquiryId;
        public String bookingUnitId;
        // public String registrationId;
        public String srStatus;
        public String srNumber;
        public String buyerName;        
        public String unitName; 
        public String internalSrStatus;
        public String bookingId;
        public String buyerStatus;
        public String createdUserId;
        public Set<String> invoice_types; /* Added on 15 Oct 2020 */
        public String policy_run_qualification; /* Added on 15 Oct 2020 */
        public String payment_status; /* Added on 15 Oct 2020 */
    }
}