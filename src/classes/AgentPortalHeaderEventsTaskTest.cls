/*--------------------------------------------------------------------------------------
* Name               : AgentPortalHeaderEventsTaskTest
* Description        : Test class for AgentPortalHeaderEventsTask
* Created Date       : 06/09/2017                                                                   
* Created By         : Naresh Kaneriya                                                              
* Last Modified Date :                                                                            
* Last Modified By   :                                                                            
* ------------------------------------------------------------------------------------ 
* VERSION     AUTHOR                    DATE                                                                    
* 1.0         Naresh                 06-09-2017 
----------------------------------------------------------------------------------------*/
@isTest 
public class AgentPortalHeaderEventsTaskTest{

public static testMethod void AgentPortalHeaderEventsTest(){

AgentPortalHeaderEventsTask  obj =  new AgentPortalHeaderEventsTask();

obj.NotificationCount = 20.32;
obj.AnnouncementCount= 20.32;
obj.TaskCount= 20.32;

obj.getNotification();
obj.getAnnouncement();
obj.getTask();


}

}