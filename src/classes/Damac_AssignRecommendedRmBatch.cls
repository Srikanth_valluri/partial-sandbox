global without sharing class Damac_AssignRecommendedRmBatch implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext BC){
        String inqRecordType = 'Pre Inquiry';
        String query = 'SELECT Sales_Office__c , Comments__c, Recommended_RM__c,Inquiry__r.Sales_Office__c,Inquiry__r.RecordType.Name FROM Office_Meeting__c '
            +' WHERE Recommended_RM__c = NULL '
            +' AND CreatedDate = TODAY '
            +' AND Inquiry__r.RecordType.Name =:inqRecordType Order By CreatedDate ASC';
            
        System.debug(query);
        return DataBase.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List <Office_Meeting__c> scope){
        List <Office_Meeting__c> meetingsToupdate = new List <Office_Meeting__c> ();
        Map <String, Sales_Office_Locations__c> salesOfficeCampaings = new Map <String, Sales_Office_Locations__c> ();
        Set <String> salesOffices = new Set <String> ();
        for (Office_Meeting__c meeting :scope) 
        {
            if (meeting.Inquiry__r.Sales_Office__c != NULL) {
                salesOffices.add (meeting.Inquiry__r.Sales_Office__c);
            }
        }
        if (salesOffices.size () > 0) {
            for (Sales_Office_Locations__c campaign: [SELECT Name, Assign_To__c, Campaign_ID__c FROM Sales_Office_Locations__c 
                                                        WHERE Name IN : salesOffices]) 
            {
                salesOfficeCampaings.put (campaign.Name, campaign);
            }
        }

        Set <ID> officemeetingId = new Set <ID> ();
        for (Office_Meeting__c meeting :scope) 
        {
            officemeetingId.add (meeting.id);
        }
        
        for (Office_Meeting__c meeting :[SELECT Sales_Office__c , Comments__c, Recommended_RM__c,Inquiry__r.Sales_Office__c,Inquiry__r.RecordType.Name 
                                        FROM Office_Meeting__c WHERE ID IN :officemeetingId]) 
        {
            if (meeting.Recommended_RM__c == NULL) {
                String campaignId = '';
                String assignId = '';
                if (meeting.Inquiry__r.Sales_Office__c != NULL) {
                    if (salesOfficeCampaings.containsKey (meeting.Inquiry__r.Sales_Office__c)) {
                        campaignId = salesOfficeCampaings.get (meeting.Inquiry__r.Sales_Office__c).Campaign_ID__c;
                        assignId = salesOfficeCampaings.get (meeting.Inquiry__r.Sales_Office__c).Assign_To__c;
                    }
                }
                if (assignId != '' && assignId != NULL) {
                    // if Salesoffice has assign Id, then changing Office meeting owner to Sales offcie 
                    meeting.OwnerId = assignId;
                    
                }
                else {
                    Damac_CheckRMController obj = new Damac_CheckRMController ();
                    if (campaignId != '' && campaignId != NULL) {
                        obj.checkForCampaignRM (meeting, campaignId);
                    } else {
                        obj.checkForRM (meeting);
                    }
                    for (Damac_CheckRMController.rmInnerClass availableRms :obj.rmsList) {
                        meeting.comments__c = Label.Office_Meeting_Default_Comment;
                        meeting.Recommended_RM__c = availableRms.u.Id;
                        break;
                    }
                    if (Test.isRunningTest()){
                        meeting.comments__c = Label.Office_Meeting_Default_Comment;
                        meeting.Recommended_RM__c = UserInfo.getUserID();
                    }
                }
                meetingsToupdate.add (meeting);
            }
        }
        List <Inquiry_Assignment_Algorithm__c> algToUpdate = new List <Inquiry_Assignment_Algorithm__c> ();
        for (Office_Meeting__c meeting :meetingsToupdate) 
        {
            if (meeting.Recommended_RM__c != NULL) {
                Inquiry_Assignment_Algorithm__c alg = new Inquiry_Assignment_Algorithm__c ();
                alg.RM_User_ID__c = meeting.Recommended_RM__c;
                alg.RM_User__c = meeting.Recommended_RM__c;
                
                alg.RM_Assigned__c = true;
                if (alg.RM_User_ID__c != NULL)
                    algToUpdate.add (alg);
            }
        }
        if (algToUpdate.size () > 0)
            Upsert algToUpdate RM_User_ID__c;
        if (meetingsToupdate.size () > 0)
            Update meetingsToupdate;
        
    }
    global void finish(Database.BatchableContext BC){

    }
}