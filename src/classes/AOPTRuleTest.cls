/*
* Description - Test class developed for 'AOPTMQService'
*
* Version            Date            Author            Description
* 1.0                21/11/17        Hardik            Initial Draft
*/
@isTest
public class AOPTRuleTest {
    static testMethod void testMethod1() {
    	test.StartTest();

        SOAPCalloutServiceMock.returnToMe = new Map<String,AOPTRule.AoptDetailsResponse_element>();
        AOPTRule.AoptDetailsResponse_element response_x =  new AOPTRule.AoptDetailsResponse_element();
        response_x.return_x = '{"allowed":"Yes","message":null,"recommendingAuthorityOne":"One","recommendingAuthorityTwo":"Two","recommendingAuthorityThree":"Three","recommendingAuthorityFour":"Four","approvingAuthorityOne":"One","approvingAuthorityTwo":"Two","approvingAuthorityThree":"Three","percToBeRefundedTransferred":null,"deductionFeePsf":null,"deductionFeeFlat":null,"deductionFeePercentage":null}';
           
        SOAPCalloutServiceMock.returnToMe.put('response_x',response_x);
        
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMock());
        AOPTRule.AOPTRuleHttpSoap11Endpoint obj = new AOPTRule.AOPTRuleHttpSoap11Endpoint();
        String res = obj.AoptDetails('','','','','','','','','','','','','','','','','','','','','','','','','','','','','');
        test.StopTest();
        
    }
}