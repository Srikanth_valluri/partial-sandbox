public class Damac_ShareProjectDetails implements Queueable {
    Id offerRecId;
    String inqRecId;
    Id shareProjDetails;
    public Damac_ShareProjectDetails (Id offerId, id InqId, Id shareProjectDetails) {
        offerRecId = offerId;
        inqRecId = inqId;
        shareProjDetails= shareProjectDetails;
        
    }
    
    public void execute(QueueableContext context) {
        Email_Metrics__c emailMetrics = new Email_Metrics__c ();
        
        if (inqRecId.startsWith ('001')) {
            emailMetrics.Account__c = inqRecId ;
            emailMetrics.Email_Request__c = [SELECT Template_For__c FROM Email_Request__c WHERE Template_For__c = 'Sales Offer Account'].id;    
        }
        else {
            emailMetrics.Inquiry__c = inqRecId ;
            emailMetrics.Email_Request__c = [SELECT Template_For__c FROM Email_Request__c WHERE Template_For__c = 'Sales Offer Inquiry'].id;    
        }
        emailMetrics.Share_Project_Details__c = shareProjDetails;
        emailMetrics.SalesOffer_Record__c = offerrecId;
        emailmetrics.SObject_id__c = inqRecId;
        if (!Test.isRunningTest ())
            insert emailMetrics;        
    }
}