@isTest
public class Invocable_PromoLetter_Test {
    
    @testSetup 
    static void setupData() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Id DealRT = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c();
        sr.NSIBPM__Customer__c = acc.Id;
        sr.RecordTypeId = DealRT;
        sr.Agency__c = acc.id;
        sr.Agency_Type__c = 'Corporate';
        insert sr;
        
        New_Step__c nstp = new New_Step__c();
        nstp.Service_Request__c = sr.id;
        nstp.Step_No__c = 2;
        nstp.Step_Status__c = 'Docs OK';
        nstp.Step_Type__c = 'Document Verification';
        insert nstp;
        
        NSIBPM__Document_Master__c dm = new NSIBPM__Document_Master__c();
        dm.Name = 'prom1';
        dm.NSIBPM__Code__c = 'Bank Statement';
        dm.NSIBPM__Document_Type_Code__c = 'Doc Type';
        dm.Promotion_Letter__c = True;
        insert Dm;
        
        Id RecTypeid = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Standalone').getRecordTypeId();
        Campaign__c c = new Campaign__c();
        c.Campaign_Name__c = 'Standalone';
        C.End_Date__c = system.today();
        C.Marketing_End_Date__c = system.today();
        C.Marketing_Start_Date__c = system.today();
        C.Start_Date__c = system.today();
        insert c;
        
        Promotion__c pro = new Promotion__c();
        pro.Start_Date__c= system.today().adddays(-10);
        pro.end_date__c = system.today().adddays(10);
        pro.Promotion_Title__c = 'prom1';
        pro.campaign__c = c.id;
        insert pro;
        
        Booking__c booking = new Booking__c(Account__c = acc.Id, Deal_SR__c = sr.Id);
        insert booking;
        
        Booking_Unit__c bookingUnit = new Booking_Unit__c(Booking__c = booking.Id, Unit_Name__c = 'Test name 1',
                                                          Registration_ID__c = '1600', Registration_Status__c = 'Active Status',
                                                          Unit_Selling_Price_AED__c = 100, Related_Promotion__c = pro.Id);
        insert bookingUnit;
    }
    
    @isTest 
    static void test_Invocable_PromoLetter() {
        NSIBPM__Service_Request__c sr = [select id from NSIBPM__Service_Request__c limit 1];
        New_Step__c stp = [select id From New_Step__c where Service_Request__c =: sr.id Limit 1];
        Invocable_PromoLetter.UpdateBuyer(new List<Id>{stp.Id});
    }
}