public class Damac_SendSMSResponse {
    public String Status;
    public string ErrorDescription;
    public List<Data> Data;
    public class Data {
        public String mobileNo;
        public String status;
        public String details;
        public String creditsUsed;
        public string msgId;
    }
    public static Damac_SendSMSResponse parse(String json) {
        return (Damac_SendSMSResponse) System.JSON.deserialize(json, Damac_SendSMSResponse.class);
    }
}