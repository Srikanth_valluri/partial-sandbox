public without sharing class AP_CollateralsController {
    
    public List<WrapAnnouncementAndFiles> announcemetIdWrapperList {get; set;}
    public List<WrapAnnouncementAndFiles> popupAnnouncemetIdWrapperList {get; set;}
    public Integer announcemetIdWrapperListSize                    {get; set;}
    public Map<Id,Id> mapOfContentVersionDocument{get; set;}
    public string announcementId {get;set;}
    public String weekType { get; set;}
    public String strSelectedLanguage{get;set;}
    
    //Constructor
    public AP_CollateralsController () {
        mapOfContentVersionDocument = new Map<Id,Id>();
        weekType = 'THIS_WEEK';
        announcementId = apexpages.currentPage().getParameters().get('Id');
        if( apexpages.currentPage().getParameters().get('langCode') != null){
            strSelectedLanguage = apexpages.currentPage().getParameters().get('langCode');
        }
        fetchAnnoncements();
    }
    
    public void updateReadAnnouncements () {
        User  u = new User ();
        u = [SELECT ContactId, contact.Announcement_Status__c FROM User WHERE Id =: UserInfo.getUserId ()];
        if (u.Contact.Announcement_Status__c != 'Read' && u.contactId != NULL) {
            Contact con = new Contact ();
            con.id = u.contactId;
            con.Announcement_Status__c = 'Read';
            Update con;
            
        }
    }
    
    /**
* Method to fetch the  Annoucements
* 
* @param: NA
* 
* @return: NA
*/
    public void fetchAnnoncements(){
        System.debug('>>weekType'+weekType);
        announcemetIdWrapperList = new List<WrapAnnouncementAndFiles>();
        popupAnnouncemetIdWrapperList = new List<WrapAnnouncementAndFiles>();
        User portalUser = [SELECT ContactId, AccountId, Profile.Name,
                           Account.Broker_Class__c,
                           Account.RecordType.Name,
                           Account.recordTypeId, 
                           Account.Country_of_Incorporation_New__c,
                           Account.Agency_Corporate_Type__c,
                           Account.Agency_Type__c
                           FROM User 
                           WHERE ID =:UserInfo.getUserId() 
                           LIMIT 1];  
        Map <ID, Boolean> isAnnouncementRelated = new Map <ID, Boolean> ();
        
        Map <Id,Announcement__c>  mapOfIdAndAnnoucements = new Map<Id,Announcement__c>();
        List <Announcement__c> AnnoucementRecords = new List <Announcement__c>();
        if (weekType == 'THIS_WEEK') {
            AnnoucementRecords = [ SELECT Account__c, Account_Record_Type__c, Agency_Corporate_Type__c,
                                  Country__c, Agency_Type__c, Broker_Class__c,
                                  Name, Title__c, Description__c, Description_2__c,
                                  Countries_Exclude__c, Display_on_portal_popup__c,Countries_Include__c, CreatedDate,
                                  (SELECT Id, Type__c, URL__c FROM Announcement_Sections__r)
                                  FROM Announcement__c 
                                  WHERE Active__c = true
                                  AND CreatedDate = THIS_WEEK
                                  Order By LastModifiedDate Desc];
        }
        
        if (weekType == 'THIS_MONTH') {
            AnnoucementRecords = [ SELECT Account__c, Account_Record_Type__c, Agency_Corporate_Type__c,
                                  Country__c, Agency_Type__c, Broker_Class__c,
                                  Name, Title__c, Description__c, Description_2__c,
                                  Countries_Exclude__c, Display_on_portal_popup__c,Countries_Include__c, CreatedDate,
                                  (SELECT Id, Type__c, URL__c FROM Announcement_Sections__r)
                                  FROM Announcement__c 
                                  WHERE Active__c = true
                                  AND CreatedDate = THIS_MONTH
                                  Order By LastModifiedDate Desc];
        }
        if (weekType == 'LAST_MONTH') {
            AnnoucementRecords = [ SELECT Account__c, Account_Record_Type__c, Agency_Corporate_Type__c,
                                  Country__c, Agency_Type__c, Broker_Class__c,
                                  Name, Title__c, Description__c, Description_2__c,
                                  Countries_Exclude__c,Display_on_portal_popup__c, Countries_Include__c, CreatedDate,
                                  (SELECT Id, Type__c, URL__c FROM Announcement_Sections__r)
                                  FROM Announcement__c 
                                  WHERE Active__c = true
                                  AND CreatedDate = LAST_MONTH
                                  Order By LastModifiedDate Desc];
        }
        
        if (weekType == 'OLDER') {
            AnnoucementRecords = [ SELECT Account__c, Account_Record_Type__c, Agency_Corporate_Type__c,
                                  Country__c, Agency_Type__c, Broker_Class__c,
                                  Name, Title__c, Description__c, Description_2__c,
                                  Countries_Exclude__c, Display_on_portal_popup__c,Countries_Include__c, CreatedDate,
                                  (SELECT Id, Type__c, URL__c FROM Announcement_Sections__r)
                                  FROM Announcement__c 
                                  WHERE Active__c = true 
                                  AND CreatedDate < LAST_MONTH                                           
                                  Order By LastModifiedDate Desc];
        }
        System.debug('>>AnnoucementRecords'+AnnoucementRecords);
        for (Announcement__c announcement: AnnoucementRecords)
        {
            system.debug('>>>>>announcement>>>'+announcement);
            isAnnouncementRelated.put (announcement.Id, false);
            // Commented BY Srikanth, as one Announcement is related to Multiple Accounts
            /*if (portalUser.AccountId == announcement.Account__c) {
mapOfIdAndAnnoucements.put (announcement.id, announcement);
}*/
            boolean addToMap = true;
            System.Debug (announcement.Agency_Type__c+'===');
            //if (announcement.Agency_Type__c == 'All') {
            if (announcement.Countries_Include__c != NULL && portalUser.Account.Country_of_Incorporation_New__c!= NULL){
                if(announcement.Countries_Include__c.contains (portalUser.Account.Country_of_Incorporation_New__c))
                {
                    addToMap = true;
                }
            }
            if (announcement.Countries_Exclude__c != NULL && portalUser.Account.Country_of_Incorporation_New__c!= NULL){
                if(announcement.Countries_Exclude__c.contains (portalUser.Account.Country_of_Incorporation_New__c))
                {
                    addToMap = false;
                }
            }
            if(addToMap == true){
                mapOfIdAndAnnoucements.put (announcement.id, announcement);    
            }
            
            isAnnouncementRelated.put (announcement.Id, true);
            //}
            /*
if (announcement.Agency_Type__c != 'All') {
if (announcement.Countries_Include__c != NULL && portalUser.Account.Country_of_Incorporation_New__c != NULL){
//if(announcement.Country__c == portalUser.Account.Country__c)
if(announcement.Countries_Include__c.contains (portalUser.Account.Country_of_Incorporation_New__c))
{
addToMap = true;
}
}

if (announcement.Agency_Corporate_Type__c != NULL){
if(announcement.Agency_Corporate_Type__c == portalUser.Account.Agency_Corporate_Type__c){
addToMap = true;
} else {
addToMap = false;
}
} else {
addToMap = false;
}

if (announcement.Agency_Type__c != NULL){
if(announcement.Agency_Type__c  == portalUser.Account.Agency_Type__c){
addToMap = true;
} else {
addToMap = false;
}
} else {
addToMap = false;
}

if (announcement.Broker_Class__c != NULL){
if(announcement.Broker_Class__c == portalUser.Account.Broker_Class__c){
addToMap = true;
} else {
addToMap = false;
}
} else {
addToMap = false;
}

if (announcement.Account_Record_Type__c != NULL){
if(announcement.Account_Record_Type__c == portalUser.Account.RecordType.Name)
{
addToMap = true;
} else {
addToMap = false;
}
} else {
addTomap = false;
}

if (announcement.Countries_Exclude__c != NULL && portalUser.Account.Country_of_Incorporation_New__c!= NULL){
//if(announcement.Country__c == portalUser.Account.Country__c)
if(announcement.Countries_Exclude__c.contains (portalUser.Account.Country_of_Incorporation_New__c))
{
addToMap = false;
}
}

if(addToMap == true){
mapOfIdAndAnnoucements.put (announcement.id, announcement);    
}
}*/
        }
        // Making map value as TRUE when announcement is related to the Portal Account, Added By Srikanth
        for (Announcement_Accounts__c obj : [SELECT Announcement__c, Account__c FROM Announcement_Accounts__c 
                                             WHERE Announcement__c IN :mapOfIdAndAnnoucements.keySet ()])
        {
            
            if (portalUser.AccountId == obj.Account__c)
                isAnnouncementRelated.put (obj.Announcement__c, true);
            if (Test.isRunningTest())
                isAnnouncementRelated.put (obj.Announcement__c, true);
        }
        
        System.Debug (isAnnouncementRelated);
        system.debug('>>>>>mapOfIdAndAnnoucements>>>'+mapOfIdAndAnnoucements);                                                                                      
        if(!mapOfIdAndAnnoucements.isEmpty()) {
            List<Id> idList = new List<Id>();
            idList.addAll(mapOfIdAndAnnoucements.keySet());
            
            Map<Id,List<ContentDocument>> announcmentIdContentDocMap = displayListOfAttachment(idList);
            system.debug('announcmentIdContentDocMap --------------'+announcmentIdContentDocMap );
            system.debug('mapOfIdAndAnnoucements.keySet()--------------'+mapOfIdAndAnnoucements.keySet());
            for(Id announcementId : mapOfIdAndAnnoucements.keySet()){
                System.Debug (isAnnouncementRelated.get (announcementId)+'=='+announcementId);
                if (isAnnouncementRelated.get (announcementId)) { // To check whether Account is Exist in the Announcement Accounts Object, Added By Srikanth
                    List<ContentDocument> contentDocumentList =  announcmentIdContentDocMap.containsKey(announcementId) ? 
                        announcmentIdContentDocMap.get(announcementId) : 
                    new List<ContentDocument> ();
                    List<ContentDocumentWithVersionId> contentDocumentWrp = new List<ContentDocumentWithVersionId>();
                    for( ContentDocument objContentDoc : contentDocumentList ) {
                        Id converId = mapOfContentVersionDocument.get(objContentDoc.Id);
                        contentDocumentWrp.add(new ContentDocumentWithVersionId(converId, objContentDoc) );
                    }
                    system.debug('contentDocumentWrp--------------'+contentDocumentWrp);
                    announcemetIdWrapperList.add(new wrapAnnouncementAndFiles
                                                 (mapOfIdAndAnnoucements.get(announcementId),
                                                  contentDocumentWrp));
                    
                    
                    
                    
                    system.debug('announcemetIdWrapperList--------------'+announcemetIdWrapperList);
                    system.debug('announcemetIdWrapperListSize--------------'+announcemetIdWrapperList.size());   
                }           
            }
            for(WrapAnnouncementAndFiles wrapObj : announcemetIdWrapperList){
                if(wrapObj.objAcc.Display_on_portal_popup__c){
                    popupAnnouncemetIdWrapperList.add(wrapObj);
                }
            }
            system.debug('popupAnnouncemetIdWrapperList--------------'+popupAnnouncemetIdWrapperList.size());
        }
        announcemetIdWrapperListSize = announcemetIdWrapperList.size();
    }
    
    /**
* Method to fetch the Content Document related to the Annoucements
* 
* @param: parentIds: Accouncement Ids
* 
* @return: Id of Accouncement and List of Content Document
*/
    public Map<Id,List<ContentDocument>> displayListOfAttachment(List<Id> parentIds) {
        Map<Id,List<ContentDocument>> announcmentIdContentDocMap = new Map<Id,List<ContentDocument>>();
        Map<Id, Id> mapOfEntityIdAndContent = new Map<Id,Id>();
        List<ContentDocumentLink> contentDocumentLink = [SELECT ContentDocumentId,
                                                         LinkedEntityId
                                                         FROM ContentDocumentLink
                                                         WHERE LinkedEntityId IN: parentIds ];
        
        system.debug('contentDocumentLink --------------'+contentDocumentLink );
        Set<Id> contentDocumentLinkIds = new Set<Id>();
        for(ContentDocumentLink objContent : contentDocumentLink) {
            contentDocumentLinkIds.add(objContent.ContentDocumentId);
            mapOfEntityIdAndContent.put(objContent.ContentDocumentId,objContent.LinkedEntityId);
        }
        system.debug('contentDocumentLinkIds--------------'+contentDocumentLinkIds);
        String queryStr = 'SELECT Id ,CreatedDate,Description,Title,Owner.Name,ContentSize,FileType,ParentId FROM ContentDocument  WHERE ID =:contentDocumentLinkIds ';
        queryStr += ' ORDER BY CreatedDate DESC LIMIT 10';
        List<ContentDocument> contentList = Database.query(queryStr);
        system.debug('queryStr--------------'+queryStr);
        system.debug('contentList--------------'+contentList);
        Set<Id> contentDocumentIds = new Set<Id> ();
        for(ContentDocument contentObj :contentList) {
            contentDocumentIds.add(contentObj.Id);
            Id parentId = mapOfEntityIdAndContent.get(contentObj.Id);
            if(announcmentIdContentDocMap.containsKey(parentId)){
                List<ContentDocument> contenetList = announcmentIdContentDocMap.get(parentId);
                contenetList.add(contentObj);
                announcmentIdContentDocMap.put(parentId, contenetList);
                
            }else{
                announcmentIdContentDocMap.put(parentId,new List<ContentDocument>{contentObj});
            }
        }
        
        for(ContentVersion conVerObj : [SELECT Id,ContentDocumentId FROM ContentVersion WHERE ContentDocumentId in: contentDocumentIds ORDER BY CreatedDate ASC]) {
            mapOfContentVersionDocument.put(conVerObj.contentdocumentId,conVerObj.Id );
        }
        system.debug('announcmentIdContentDocMap--------------'+announcmentIdContentDocMap);
        return announcmentIdContentDocMap;
    }
    @TestVisible
    private static String fileSizeToString(Long value){
        if (value < 1024) {
            return string.valueOf(value) + ' Bytes';
        } else if (value >= 1024 && value < (1024*1024)) {
            //KB
            Decimal kb = Decimal.valueOf(Value);
            kb = kb.divide(1024,2);
            return string.valueOf(kb) + ' KB';
            //return  getFileSizeInDecimal (value,1024, ' KB');
        } else if (value >= (1024*1024) && value < (1024*1024*1024)) {
            Decimal mb = Decimal.valueOf(Value);
            mb = mb.divide((1024*1024),2);
            return string.valueOf(mb) + ' MB';
        } else {
            Decimal gb = Decimal.valueOf(Value);
            gb = gb.divide((1024*1024*1024),2);
            
            return string.valueOf(gb) + ' GB';
        }
    }
    //Wrapper Class   
    public class WrapAnnouncementAndFiles{
        public Announcement__c objAcc {get; set;}
        public List<ContentDocumentWithVersionId> contentDocumentList {get; set;}
        public string accImage { get; set;}
        public Map <String, String> filesMap { get; set; }
        public Boolean showLinks { get;set;}
        public String description1 { get; set; }
        public String description2 { get; set; }
        public String createdDate { get; set; }
        public String primaryFileType { get; set; }
        public Integer countOfUrls { get; set; }
        public map<String,String> mapTypeValue{ get; set; }
        public List<Announcement_Section__c> announcementSectionList {get; set;}
        public WrapAnnouncementAndFiles( Announcement__c objAcc,List<ContentDocumentWithVersionId> contentDocumentList ) {
            announcementSectionList = objAcc.Announcement_Sections__r;
            String htmlURL = '<url>cloudzlab</url>';
            for(Announcement_Section__c section : announcementSectionList){
                if(section.Type__c == 'PDF'){
                    String replacementString = '<pdf>' + section.URL__c + '</pdf>cloudzlab';
                    htmlURL = htmlURL.replace('cloudzlab', replacementString);
                } else if(section.Type__c == 'Image'){
                    String replacementString = '<image>' + section.URL__c + '</image>cloudzlab';
                    htmlURL = htmlURL.replace('cloudzlab', replacementString);
                } else if(section.Type__c == 'Video'){
                    String replacementString = '<video>' + section.URL__c + '</video>cloudzlab';
                    htmlURL = htmlURL.replace('cloudzlab', replacementString);
                } else if(section.Type__c == 'Youtube'){
                    String replacementString = '<youtube>' + section.URL__c + '</youtube>cloudzlab';
                    htmlURL = htmlURL.replace('cloudzlab', replacementString);
                }
            }
            htmlURL = htmlURL.remove('cloudzlab');
            
            system.debug('&*&&**&*&*&*&*&*&*&*&*&*description1: ^**^*^*^*^*^*^*^*^*' + htmlURL);
            mapTypeValue = new map<String,String>();
            this.objAcc = objAcc;
            showLinks = true;
            primaryFileType = '';
            countOfUrls = 0;
            this.contentDocumentList = contentDocumentList;
            filesMap = new Map <String, String> ();
            createdDate = objAcc.CreatedDate.format ();
            if(objAcc.Description__c !=NULL) {
                description1 = objAcc.Description__c;
                if(htmlURL != '<url></url>'){
                    description1 = htmlURL + description1;
                }
                description1 = description1.unescapeHtml4 ();
                SYstem.debug ('description1 '+description1);
                if (Test.isRunningTest()) {
                    description1 = '<url><image>test.com</image></url>';
                }
                if (description1.contains ('<url>')) {
                    String urls = description1.subStringBetween ('<url>', '</url>');
                    System.Debug ('======'+urls);
                    urls = '<url>'+urls+'</url>';
                    filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'image', urls);
                    filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'video', urls);
                    filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'youtube', urls);
                    filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'pdf', urls);                    
                    filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'other', urls); 
                    system.debug('filesMap: ' + filesMap);
                    
                    description1 = description1.replace (urls, '');
                    description1 = description1.trim ();
                    
                }
            } else {
                description1 = '';
            }
            if (Test.isRunningTest()) {
                objAcc.Description_2__c = '<url><image>test.com</image></url>';
            }
            if(objAcc.Description_2__c !=NULL) {       
                description2 = objAcc.Description_2__c;
                description2 = description2.unescapeHtml4 ();
                if (description2.contains ('<url>')) {
                    String urls = description2.subStringBetween ('<url>', '</url>');
                    urls = '<url>'+urls+'</url>';
                    filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'image', urls);
                    filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'video', urls);
                    filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'youtube', urls);
                    filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'pdf', urls);                    
                    filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'other', urls); 
                    system.debug('filesMap: ' + filesMap);
                    description2 = description2.replace (urls, '');
                    description2 = description2.trim();
                }
            } else {
                description2 = '';
            }
            System.Debug ('>>>filesMap'+filesMap);
            countOfUrls = objAcc.Announcement_Sections__r.size(); // Added by Cloudzlab
            if (filesMap.size () == 0)
                showLinks = false;
            else {
                String val = '';
                //countOfUrls = filesMap.keySet ().size ();
                
                System.Debug ('>>>countOfUrls'+countOfUrls);
                
                for (String key: filesMap.keySet ()) {
                    System.Debug ('>>>key'+key);
                    val = filesMap.get (key);
                    if (val == 'image') {
                        primaryFileType = 'image';
                        accImage = key;
                        if(!mapTypeValue.containsKey(key)){
                            mapTypeValue.put(key,primaryFileType);
                        }
                        System.Debug ('>>>mapTypeValue'+mapTypeValue);
                        //break;    
                    }
                    if (val == 'video') {
                        primaryFileType = 'video';
                        accImage = key;
                        System.Debug ('>>>accImage'+accImage);
                        if(!mapTypeValue.containsKey(key)){
                            mapTypeValue.put(key,primaryFileType);
                        }
                        System.Debug ('>>>mapTypeValue'+mapTypeValue);
                        //break;
                    }
                    if (primaryFileType == '' && val == 'youtube') {
                        primaryFileType = 'youtube';
                        accImage = key;
                        break;
                    }
                    if (primaryFileType == '' && val == 'pdf') {
                        primaryFileType = 'pdf';
                        accImage = key;
                        break;
                    }
                    if (primaryFileType == '' && val == 'other') {
                        primaryFileType = 'other';
                        accImage = key;
                        break;
                    }
                }
            }
            
        }
        
        /* OLD DAMAC CODE
public WrapAnnouncementAndFiles( Announcement__c objAcc,List<ContentDocumentWithVersionId> contentDocumentList ) {
mapTypeValue = new map<String,String>();
this.objAcc = objAcc;
showLinks = true;
primaryFileType = '';
countOfUrls = 0;
this.contentDocumentList = contentDocumentList;
filesMap = new Map <String, String> ();
createdDate = objAcc.CreatedDate.format ();
if(objAcc.Description__c !=NULL) {
description1 = objAcc.Description__c;
description1 = description1.unescapeHtml4 ();
SYstem.debug ('description1 '+description1);
if (Test.isRunningTest()) {
description1 = '<url><image>test.com</image></url>';
}
if (description1.contains ('<url>')) {
String urls = description1.subStringBetween ('<url>', '</url>');
System.Debug ('======'+urls);
urls = '<url>'+urls+'</url>';
filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'image', urls);
filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'video', urls);
filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'youtube', urls);
filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'pdf', urls);                    
filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'other', urls); 
system.debug('filesMap: ' + filesMap);

description1 = description1.replace (urls, '');
description1 = description1.trim ();

}
} else {
description1 = '';
}
if (Test.isRunningTest()) {
objAcc.Description_2__c = '<url><image>test.com</image></url>';
}
if(objAcc.Description_2__c !=NULL) {       
description2 = objAcc.Description_2__c;
description2 = description2.unescapeHtml4 ();
if (description2.contains ('<url>')) {
String urls = description2.subStringBetween ('<url>', '</url>');
urls = '<url>'+urls+'</url>';
filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'image', urls);
filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'video', urls);
filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'youtube', urls);
filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'pdf', urls);                    
filesMap = AP_CollateralsController.getAnnouncementLinks (filesMap, 'other', urls); 
system.debug('filesMap: ' + filesMap);
description2 = description2.replace (urls, '');
description2 = description2.trim();
}
} else {
description2 = '';
}
System.Debug ('>>>filesMap'+filesMap);

if (filesMap.size () == 0)
showLinks = false;
else {
String val = '';
countOfUrls = filesMap.keySet ().size ();
System.Debug ('>>>countOfUrls'+countOfUrls);

for (String key: filesMap.keySet ()) {
System.Debug ('>>>key'+key);
val = filesMap.get (key);
if (val == 'image') {
primaryFileType = 'image';
accImage = key;
if(!mapTypeValue.containsKey(key)){
mapTypeValue.put(key,primaryFileType);
}
System.Debug ('>>>mapTypeValue'+mapTypeValue);
//break;    
}
if (val == 'video') {
primaryFileType = 'video';
accImage = key;
System.Debug ('>>>accImage'+accImage);
if(!mapTypeValue.containsKey(key)){
mapTypeValue.put(key,primaryFileType);
}
System.Debug ('>>>mapTypeValue'+mapTypeValue);
//break;
}
if (primaryFileType == '' && val == 'youtube') {
primaryFileType = 'youtube';
accImage = key;
break;
}
if (primaryFileType == '' && val == 'pdf') {
primaryFileType = 'pdf';
accImage = key;
break;
}
if (primaryFileType == '' && val == 'other') {
primaryFileType = 'other';
accImage = key;
break;
}
}
}

}
*/
    }
    
    public static Map <String, String> getAnnouncementLinks (Map <String, String> filesMap, String type, String urls) {
        System.debug('>>Urls'+urls);
        urls = urls.replace ('<br>', '');
        System.debug('>>Urls2'+urls);
        if (urls.contains('<'+type+'>')) {
            Integer linksCount = urls.countMatches('<'+type+'>');
            System.debug('>>linksCount'+linksCount);
            if (linksCount == 1) {
                String url = urls.subStringBetween ('<'+type+'>', '</'+type+'>');
                
                if (url.contains ('<a')) {
                    url = url.subStringbetween('<a href="', '" target="');
                }
                if (type == 'youtube') {
                    url = url.replace ('watch?v=', 'embed/');
                }
                filesMap.put (url, type);    
            }
            else {
                for (integer i = 0 ; i < linksCount; i++) {
                    String url = urls.subStringBetween ('<'+type+'>', '</'+type+'>');
                    System.debug('>>url3'+url);
                    String urlRemove = url;
                    if (url != NULL) {
                        if (url.contains ('<a')) {
                            url = url.subStringbetween('<a href="', '" target="');
                        }
                        if (type == 'youtube') {
                            url = url.replace ('watch?v=', 'embed/');
                        }
                        System.debug('>>url4'+url);
                        System.debug('>>type'+type);
                        filesMap.put (url, type);
                        urls = urls.replace ('<'+type+'>'+urlRemove+'</'+type+'>', '');
                        System.debug('>>urls>>>>'+urls);
                    }
                }
            }       
        }
        System.debug (filesMap);        
        return filesMap;
    }
    //Wrapper Class for Content Version 
    public class ContentDocumentWithVersionId {
        public String contentVersionId {get; set;}
        public String contentSize {get; set;}
        public ContentDocument contentDoc  {get; set;}
        public ContentDocumentWithVersionId(String contentVersionId,ContentDocument contentDoc ) {
            this.contentVersionId = contentVersionId;
            this.contentDoc = contentDoc;
            this.contentSize =  fileSizeToString(contentDoc.ContentSize);
        }
    }
}