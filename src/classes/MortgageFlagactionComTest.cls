/***********************************************************************
* Description - Test class developed for MortgageFlagactionCom
*
* Version            Date            Author        Description
* 1.0                01/03/18        Arjun         Initial Draft
**********************************************************************/
@isTest
private class MortgageFlagactionComTest
{
  @isTest static void testUpdateMortgageFlag() {
   
    
Account accObj = TestDataFactory_CRM.createPersonAccount();
    insert accObj;

        NSIBPM__Service_Request__c dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( accObj.Id, dealSR.Id, 1 );
        insert lstBookings;

        List<Booking_Unit__c> bookingUnitsObj = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        insert bookingUnitsObj;
        
        List<Case> caseLst= createCase('Mortgage',1,accObj.Id );
            insert caseLst;
     Test.startTest();
     MortgageFlagactionCom.ProcessHttpSoap11Endpoint calloutObj = new MortgageFlagactionCom.ProcessHttpSoap11Endpoint();
    
    Test.setMock( WebServiceMock.class, new MortgageWebServiceCalloutMock());

    List<MortgageFlagService.APPSXXDC_PROCESS_SERX1794747X1X5> regTerms = new List<MortgageFlagService.APPSXXDC_PROCESS_SERX1794747X1X5>();
    MortgageFlagService.APPSXXDC_PROCESS_SERX1794747X1X5 objTerms = new MortgageFlagService.APPSXXDC_PROCESS_SERX1794747X1X5();
       objTerms.ATTRIBUTE1     = '';   
  objTerms.ATTRIBUTE10 = '';
            
            //Datetime dt = caseLst[0].CreatedDate;
            //objTerms.ATTRIBUTE11 = String.valueOf(dt.format('dd-MMM-yyyy').toUpperCase());
            objTerms.ATTRIBUTE11 = '';
            
            objTerms.ATTRIBUTE12 = '';
            objTerms.ATTRIBUTE13 = '';
            objTerms.ATTRIBUTE14 = '';
            objTerms.ATTRIBUTE15 = '';
            objTerms.ATTRIBUTE16 = '';
            objTerms.ATTRIBUTE17 = '';
            objTerms.ATTRIBUTE18 = '';
            //objTerms.ATTRIBUTE19 = caseLst[0].OwnerId.Manager.Name;
            //Booking_Unit__r.Property_Name__c;
            objTerms.ATTRIBUTE2     = '';
            objTerms.ATTRIBUTE20 = '';
            objTerms.ATTRIBUTE21 = '';
            objTerms.ATTRIBUTE22 = '';
            objTerms.ATTRIBUTE23 = '';
            objTerms.ATTRIBUTE24 = '';
            objTerms.ATTRIBUTE25 = '';
            objTerms.ATTRIBUTE26 = '';
            objTerms.ATTRIBUTE27 = '';
            objTerms.ATTRIBUTE28 = '';
            objTerms.ATTRIBUTE29 = '';
            objTerms.ATTRIBUTE3     = '';
            objTerms.ATTRIBUTE30 = '';
            objTerms.ATTRIBUTE31 = '';
            objTerms.ATTRIBUTE32 = '';
            objTerms.ATTRIBUTE33 = '';
            objTerms.ATTRIBUTE34 = '';
            objTerms.ATTRIBUTE35 = '';
            objTerms.ATTRIBUTE36 = '';
            objTerms.ATTRIBUTE37 = '';
            objTerms.ATTRIBUTE38 = '';
            objTerms.ATTRIBUTE39 = '';
            objTerms.ATTRIBUTE4     = '';
            objTerms.ATTRIBUTE41 = '';
            objTerms.ATTRIBUTE42 = '';
            objTerms.ATTRIBUTE43 = '';
            objTerms.ATTRIBUTE44 = '';
            objTerms.ATTRIBUTE45 = '';
            objTerms.ATTRIBUTE46 = '';
            objTerms.ATTRIBUTE47 = '';
            objTerms.ATTRIBUTE48 = '';
            objTerms.ATTRIBUTE49 = '';
            objTerms.ATTRIBUTE5     = caseLst[0].Mortgage_Bank_Name__c;
            objTerms.ATTRIBUTE50 = '';
            objTerms.ATTRIBUTE6     = '';
            objTerms.ATTRIBUTE7     = String.valueOf( caseLst[0].Mortgage_Amount__c);
            objTerms.ATTRIBUTE8     = '';
            objTerms.ATTRIBUTE9     = '';
            objTerms.PARAM_ID   = bookingUnitsObj[0].Registration_ID__c;
            regTerms.add(objTerms);
      system.debug( 'regTerms' + regTerms);
      
      String response = calloutObj.updateMortgage( '2-'+String.valueOf( Datetime.now().getTime())
                                  , 'UPDATE_MORTGAGE' ,'SFDC' ,regTerms);
    
    Test.stopTest();
  }
  
public static List<Case> createCase( String recordTypeDevName, Integer counter ,Id accId  ) {

        Id recodTypeIdCase = [
            SELECT Id
            FROM RecordType
            WHERE SObjectType=:'Case'
            AND DeveloperName=:recordTypeDevName
            AND IsActive = TRUE LIMIT 1
        ].Id;

        List<Case> lstCase = new List<Case>();
        for( Integer i=0; i<counter; i++ ) {
            lstCase.add(new Case( Mortgage_Bank_Name__c ='test bank',Mortgage_Amount__c = 10, AccountId = accId, Credit_Note_Amount__c=5000, RecordTypeId = recodTypeIdCase ) );
        }
        return lstCase;
    }  
}