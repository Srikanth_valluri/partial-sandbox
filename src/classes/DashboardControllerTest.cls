@isTest
public class DashboardControllerTest{

    @testSetup static void setup() {

      insert new IpmsRestServices__c(
                   SetupOwnerId = UserInfo.getOrganizationId(),
                   BaseUrl__c = 'http://83.111.194.181:8045/webservices/rest',
                   Username__c = 'oracle_user',
                   Password__c = 'crp1user',
                   Client_Id__c = '8MABLQM-KJ8I8-1XA58-WWCM1S1',
                   BearerToken__c = 'eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1MzAwtTMwsDIx2l1IoCqIC5oRFIoLQ4tSgvMTcVqM_C19HJJ9BX19vLwtNC1zDC0dRCNzzc2dcw2FCpFgBXRb-1XQAAAA.6Ym224Vwr9AniBeq6gL8OM9u4vGnUB_vbEUVjWojg14',
                   Timeout__c = 120000
                   );
      /*
       Id p = [select id from profile where name='Customer Community Login User(Use this)'].id;
       Account ac = new Account(name ='Grazitti') ;
       insert ac;

       Contact con = new Contact(LastName ='testCon',AccountId = ac.Id);
       insert con;

        Case caseInstance = new Case(Status='new',pending_Amount__c=500.00,AccountId=ac.id,ClosedDate=System.Today());
        insert caseInstance;

        User u = new User();
        u.alias = 'test123';
        u.email='test123@noemail.com';
        u.emailencodingkey='UTF-8';
        u.lastname='Testing';
        u.languagelocalekey='en_US';
        u.localesidkey='en_US';
        u.profileid = p;
        u.country='United States';
        u.IsActive =true;
        u.ContactId = con.Id;
        u.timezonesidkey='America/Los_Angeles';
        u.username='tester@noemail.com';


       insert u;
       */

    }
    @isTest
    public static void testpopulateCreateSRBoolean(){
       //User u = [Select id from user where UserName='tester@noemail.com'];
       User communityUser = CommunityTestDataFactory.CreatePortalUser();
       User objUser = [select id,contact.accountId,contactID from user where id =: communityUser.id ];
       String RecTypeId= [select Id from RecordType where (Name='Email') and (SobjectType='Case')].Id;

       PageReference pageRef = Page.Customer; // Add your VF page Name here
       //pageRef.getParameters().put('view', 'AdditionalParking');
       Test.setCurrentPage(pageRef);
       NSIBPM__Service_Request__c ServiceRequest = TestDataFactory_CRM.createServiceRequest();
       insert ServiceRequest;

       List<Booking__c> lstBookings  =  TestDataFactory_CRM.createBookingForAccount(objUser.contact.accountId,
                                                                                      ServiceRequest.id,
                                                                                      2);
         insert lstBookings;
         List<Booking_Unit__c> lstBookingUnit = TestDataFactory_CRM.createBookingUnits(lstBookings,2);
         lstBookingUnit[0].Registration_Status__c = 'Agreement executed by DAMAC';
         lstBookingUnit[0].Requested_Price__c = 15556366;
         lstBookingUnit[1].Registration_Status__c = 'Agreement executed by DAMAC';
         lstBookingUnit[1].Requested_Price__c = 15556366;
         insert lstBookingUnit;

       system.runAs(communityUser){
         Case objCase = TestDataFactory_CRM.createCase(objUser.contact.accountId,RecTypeId);
         objCase.Pending_Amount__c = 1000;
         objCase.status = 'Rejected';
         objCase.contactID = objUser.contactId;
         insert objCase;
         system.debug('objUser.contact.accountId---'+objUser.contact.accountId);
         system.debug('ServiceRequest.id----'+ServiceRequest.id);
          Test.setCurrentPage(Page.Customer);
          ApexPages.currentPage().getParameters().put('view', 'home');
         DashboardController controller = new DashboardController();
         system.debug('-------------'+lstBookingUnit);
         Boolean varBool =  DashboardController.checkCurrentValueStatus('Agreement executed by DAMAC');
         List<Selectoption> lstoptions = controller.getselectedBookingUnitfields();
         Boolean varBool2 =  DashboardController.checkTotalInvestmentStatus('Agreement executed by DAMAC');
         string str = DashboardController.blank('str');
         DashboardController.Unitdetailwrapper objwrapper = new DashboardController.Unitdetailwrapper();
         objwrapper = DashboardController.fetchPaymentDetails_SFnIPMS(lstBookingUnit[0].id);

         
       }
   }

   @isTest
   Public static void testGenerateSOA() {

      NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
      insert sr;

      Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id);
      insert booking;

      Booking_Unit__c bu = new Booking_Unit__c();
      bu.Registration_ID__c = '12345';
      bu.Booking__c = booking.Id;
      insert bu;

      Booking_Unit__c bkUnit = [SELECT id
                                      ,Registration_ID__c
                                FROM Booking_Unit__c
                                WHERE id =: bu.id];
      //for GenarateSOA() method
         FmHttpCalloutMock.Response getAllUnitSoaInLangUrlResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ');

         String UNIT_SOA_LANG_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.UNIT_SOA_LANG, new List<String> {bkUnit.Registration_ID__c});

        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
             UNIT_SOA_LANG_str => getAllUnitSoaInLangUrlResponse
        };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));

        Test.startTest();
        DashboardController.GenarateSOA(bkUnit.Id);
        Test.stopTest();


   }

}