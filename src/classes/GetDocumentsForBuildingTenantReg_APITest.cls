@isTest
public class GetDocumentsForBuildingTenantReg_APITest {
    @isTest
    static void testDocuments(){
        Location__c lstLocations = new Location__c();
        lstLocations.Building_Name__c = 'test';
        lstLocations.Location_Type__c = 'Building';
        lstLocations.Location_ID__c = '12345';
        insert lstLocations;
        
        FM_Process_Hello_Damac_App__mdt objFMProcess = new FM_Process_Hello_Damac_App__mdt();
        objFMProcess.DeveloperName = 'Tenant_Registration';
        
        FM_Documents_Hello_Damac_App__mdt lstCommonFmDocs = new FM_Documents_Hello_Damac_App__mdt();
        lstCommonFmDocs.Is_Active__c = True;
        lstCommonFmDocs.DeveloperName = 'Tenant_Registration';
        lstCommonFmDocs.IsCommonDocument__c = True;
        lstCommonFmDocs.Process__c = objFMProcess.Id;
        
        lstCommonFmDocs.Mandatory__c = True;
        lstCommonFmDocs.Unique_Id__c = '3';
        lstCommonFmDocs.Document_Name__c = 'Passport';
      
        District_Cooling_Projects_Hello_Damac_Ap__mdt objDistrict = new District_Cooling_Projects_Hello_Damac_Ap__mdt();
       	objDistrict.DeveloperName = 'Tenant_Registration';
        objDistrict.Building_Name__c = 'Tenant_Registration';
        objDistrict.Building_Record_Id__c = lstLocations.Id;
        objDistrict.FM_Documents_Hello_Damac_App__c = lstCommonFmDocs.Id;
        
        LPG_Connected_Projects_Hello_Damac_App__mdt objLPG = new LPG_Connected_Projects_Hello_Damac_App__mdt();
        objDistrict.Building_Record_Id__c = lstLocations.Id;
        objLPG.FM_Documents_Hello_Damac_App__c = lstCommonFmDocs.Id;
        
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getUnitsFromBuilding';  
        req.addParameter('buildingId', lstLocations.Id);
        req.addParameter('processName', 'Tenant Registration');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetDocumentsForBuildingTenantReg_API.getDocuments();
    }
    
     @isTest
    static void testDocumentsElse(){
        Location__c lstLocations = new Location__c();
        lstLocations.Building_Name__c = 'test';
        lstLocations.Location_Type__c = 'Building';
        lstLocations.Location_ID__c = '12345';
        insert lstLocations;
        
        FM_Documents_Hello_Damac_App__mdt lstCommonFmDocs = new FM_Documents_Hello_Damac_App__mdt();
        lstCommonFmDocs.Is_Active__c = True;
        lstCommonFmDocs.DeveloperName = 'Tenant_Registration';
        lstCommonFmDocs.IsCommonDocument__c = True;
      
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getUnitsFromBuilding';  
        req.addParameter('buildingId', lstLocations.Id);
        //req.addParameter('processName', 'Tenant Registration');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetDocumentsForBuildingTenantReg_API.getDocuments();
    }
    
    @isTest
    static void testDocumentsElseIf(){
        Location__c lstLocations = new Location__c();
        lstLocations.Building_Name__c = 'test';
        lstLocations.Location_Type__c = 'Building';
        lstLocations.Location_ID__c = '12345';
        insert lstLocations;
        
        FM_Documents_Hello_Damac_App__mdt lstCommonFmDocs = new FM_Documents_Hello_Damac_App__mdt();
        lstCommonFmDocs.Is_Active__c = True;
        lstCommonFmDocs.DeveloperName = 'Tenant_Registration';
        lstCommonFmDocs.IsCommonDocument__c = True;
      
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getUnitsFromBuilding';  
        //req.addParameter('buildingId', lstLocations.Id);
        req.addParameter('processName', 'Tenant Registration');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetDocumentsForBuildingTenantReg_API.getDocuments();
    }
    
    @isTest
    static void testDocumentsElseIf1(){
        Location__c lstLocations = new Location__c();
        lstLocations.Building_Name__c = 'test';
        lstLocations.Location_Type__c = 'Building';
        lstLocations.Location_ID__c = '12345';
        insert lstLocations;
        
        FM_Documents_Hello_Damac_App__mdt lstCommonFmDocs = new FM_Documents_Hello_Damac_App__mdt();
        lstCommonFmDocs.Is_Active__c = True;
        lstCommonFmDocs.DeveloperName = 'Tenant_Registration';
        lstCommonFmDocs.IsCommonDocument__c = True;
      
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getUnitsFromBuilding';  
        req.addParameter('buildingId', '');
        req.addParameter('processName', 'Tenant Registration');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetDocumentsForBuildingTenantReg_API.getDocuments();
    }
    
     @isTest
    static void testDocumentsElseIf2(){
        Location__c lstLocations = new Location__c();
        lstLocations.Building_Name__c = 'test';
        lstLocations.Location_Type__c = 'Building';
        lstLocations.Location_ID__c = '12345';
        insert lstLocations;
        
        FM_Documents_Hello_Damac_App__mdt lstCommonFmDocs = new FM_Documents_Hello_Damac_App__mdt();
        lstCommonFmDocs.Is_Active__c = True;
        lstCommonFmDocs.DeveloperName = 'Tenant_Registration';
        lstCommonFmDocs.IsCommonDocument__c = True;
      
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getUnitsFromBuilding';  
        req.addParameter('buildingId', '');
        req.addParameter('processName', '');
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        GetDocumentsForBuildingTenantReg_API.getDocuments();
    }
}