@isTest
private class AutoRejectAmenityBookingBatchTest {

	@isTest static void test_method_one() {
        Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().
                            get('Amenity Booking').getRecordTypeId();
        FM_Case__c objFMCase = new FM_Case__c();
        objFMCase.Booking_Date__c = Date.Today().addDays(2);
        objFMCase.Resource_Booking_Type__c = 'FM Amenity';
        objFMCase.RecordTypeId =devRecordTypeId;
        // objFMCase.Resource_Booking_Deposit__c = 20;
        objFMCase.Amenity_Booking_Status__c = 'On Hold';
        objFMCase.Resource_Booking_Due_Date__c = Date.today().addDays(-2);
        insert objFMCase;
        Test.startTest();
        new AutoRejectAmenityBookingBatchScheduler().scheduleMyJob();
        Test.stopTest();
	}

}