/***************************************************************************************************
 * @Name              : DAMAC_Inventory_External_SearchAPI 
 * @Test Class Name   : DAMAC_Inventory_External_SearchAPI_Test
 * @Description       : API to Search for Projects and Units based on a keyword
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         29/06/2020       Created
****************************************************************************************************/
@RestResource(urlMapping='/queryInventorySearchAPIPagination/*')
global class DAMAC_Inventory_External_SearchAPI {
    
    @HttpGET
    global static void getResults(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
       // String reqBody = req.requestBody.toString();
        //System.debug (reqBody);             
        // Parameters for Search API and List of Available Units
        String type = req.params.get('type');            
        String searchTerm = req.params.get('searchTerm');
        if(searchTerm !=NULL && searchTerm != ''){
            searchTerm = '%'+searchTerm +'%'; //to search for the keyword even if it is appended with anything
        }
        List<inventory__c> inventoryList = new List<inventory__c >();
        if(req.requestURI == '/queryInventorySearchAPIPagination'){
            system.debug('POST with no body');            
            
            Set <Id> excludeInvIds = new Set <Id> ();
        
            List <Inventory_User__c> otherUserinvUnit = new List <Inventory_User__c>();
            List <Inventory_User__c> thisUserInvUnit = new List <Inventory_User__c>();
    
            thisUserInvUnit = [SELECT inventory__c FROM inventory_user__c 
                                WHERE user__c =: userinfo.getuserid() 
                                AND inventory__r.Tagged_To_Unit_Assignment__c = false 
                                AND inventory__r.status__c='Released'];
                                
            otherUserinvUnit = [SELECT inventory__c FROM inventory_user__c 
                                WHERE User__c !=: userinfo.getuserid() 
                                AND inventory__r.Tagged_To_Unit_Assignment__c = false 
                                AND inventory__r.status__c='Released' ];
    
            Set <Id> thisPcinvIdsUnit = new Set <Id>();
            if (thisUserInvUnit.size() > 0) {
                for (inventory_user__c thisPcInv:thisUserInvUnit){
                    thisPcinvIdsUnit.add(thisPcInv.inventory__c);
                }
            }
            Set <Id> otherInventoryIdsUnit = new set<Id>();
            for (inventory_user__c invUs:otherUserinvUnit){
                if (thisPcinvIdsUnit.size()>0){
                    if (!thisPcinvIdsUnit.contains(invUs.inventory__c)){
                        excludeInvIds.add(invUs.inventory__c);
                    }
                }else{
                    excludeInvIds.add(invUs.inventory__c);
                }
            }
            
            Map<Id,inventory__c> projectResultMap = new Map<Id,inventory__c>();
            Map<Id,inventory__c> unitResultMap = new Map<Id,inventory__c>();    
            Map<Id,inventory__c> resultMap = new Map<Id,inventory__c>();    
            
            Map<string,string> projectMap = new Map<string,string>();
            Map<string,string> unitMap = new Map<string,string>(); 
                 
            List<Location__c> invReleasedUnitFilter = new List<Location__c>();
            List<Marketing_Documents__c > invReleasedProjectFilter = new List<Marketing_Documents__c >();
            
            String jsonBody = '';
            String jsonProjectBody = '';
            String jsonUnitBody = '';                                           
                            
            if(searchTerm != NULL && searchTerm != ''){
            
                
                invReleasedProjectFilter = [SELECT Id, Name, Marketing_Plan__c,
                                            (SELECT Id, Property_City__c, District__c  
                                              FROM Inventories__r  
                                              WHERE Property_City__c != NULL AND District__c != NULL LIMIT 1) 
                                            FROM Marketing_Documents__c 
                                            WHERE Name Like : searchTerm AND Id IN (SELECT Marketing_Name_Doc__c  
                                                                                      FROM Inventory__c WHERE Property_City__c != NULL AND 
                                                                                      District__c != NULL AND Marketing_Name_Doc__c != NULL)];
                                                           
                invReleasedUnitFilter =    [SELECT Id, Name, Image_Of_Location__c,  
                                             (SELECT Id, Marketing_Name__c, Marketing_Name_Doc__c, Unit_Location__c,
                                               Marketing_Name_Doc__r.Name, Marketing_Name_Doc__r.Marketing_Plan__c 
                                               FROM Unit_Inventories__r                                                
                                               WHERE Marketing_Name_Doc__c != NULL LIMIT 1) 
                                            FROM Location__c
                                            WHERE Name Like : searchTerm AND Id IN (SELECT Unit_Location__c FROM Inventory__c                                                
                                                                                       WHERE Marketing_Name_Doc__c != NULL)];
            
            }   
            else{
                invReleasedProjectFilter = [SELECT Id, Name, Marketing_Plan__c,
                                            (SELECT Id, Property_City__c, District__c  
                                              FROM Inventories__r  
                                              WHERE Property_City__c != NULL AND District__c != NULL LIMIT 1) 
                                            FROM Marketing_Documents__c LIMIT 2000];
                                                           
                invReleasedUnitFilter =    [SELECT Id, Name, Image_Of_Location__c,  
                                             (SELECT Id, Marketing_Name__c, Marketing_Name_Doc__c, 
                                               Marketing_Name_Doc__r.Name, Marketing_Name_Doc__r.Marketing_Plan__c 
                                               FROM Unit_Inventories__r 
                                               WHERE Marketing_Name_Doc__c != NULL LIMIT 1) 
                                            FROM Location__c LIMIT 2000];
            
            }            
            
            // Projects
            for(Marketing_Documents__c ProjectFilter : invReleasedProjectFilter ){                    
                                    
                 String body = '{';                              
                 body  +=    '"project_Id": "' + ProjectFilter.Id + '",'
                                 + '"project_Name": "' + ProjectFilter.Name + '"';                                                                                                                                               
                                 //checking if there are inventories correlated
                                 if(ProjectFilter.Inventories__r.size() > 0){
                                     if(ProjectFilter.Inventories__r[0].District__c != NULL) {    
                                          body +=  ', '+ '"location": "' + ProjectFilter.Inventories__r[0].District__c +'"';
                                          }
                                     else{
                                          body +=  ', '+ '"location": ' + null;
                                     } 
                                     //Property City         
                                     if(ProjectFilter.Inventories__r[0].Property_City__c!= NULL) {    
                                          body +=  ', '+ '"property_City": "' + ProjectFilter.Inventories__r[0].Property_City__c +'"';
                                          }
                                     else{
                                          body +=  ', '+ '"property_City": ' + null;
                                     }
                                 } 
                                 else{
                                     body +=  ', '+ '"location": ' + null
                                              + ', '+ '"property_City": ' + null;                                     
                                 } 
                                 //Logo        
                                 if(ProjectFilter.Marketing_Plan__c!= NULL) {    
                                      body +=  ', '+ '"logo_Image": "' + ProjectFilter.Marketing_Plan__c +'"';
                                      }
                                 else{
                                      body +=  ', '+ '"logo_Image": ' + null;
                                 }
                                 
                 body += '}'; 
                                 
                 projectMap.put(ProjectFilter.Id,body);
                                 
                                            
            }
            if(!projectMap.isEmpty()){
                
                for(String body: projectMap.values()){
                
                    if(jsonProjectBody != ''){
                        jsonProjectBody +=  ',' + body ;
                    }
                    else{
                        jsonProjectBody = '[' + body ;
                    }
                    
                } 
                jsonProjectBody += ']';   
            
            }
            else{
                jsonProjectBody = '[]';
            
            }
            
            // Units            
            for(Location__c UnitFilter : invReleasedUnitFilter ){

                String body = '{';
                body +=   '"unit_Id": "' + UnitFilter.Id + '",'
                                 + '"unit_Name": "' + UnitFilter.Name + '"';  
                
                                //+ '"project_Name": "' + UnitFilter.Unit_Inventories__r[0].Marketing_Name__c + '"';
                                    
                               
                                if(UnitFilter.Unit_Inventories__r.size() > 0 ){ 
                                
                                    if(UnitFilter.Unit_Inventories__r[0].Marketing_Name__c  != NULL) {                                            
                                        body +=  ', '+ '"project_Name": "' + UnitFilter.Unit_Inventories__r[0].Marketing_Name__c +'"';
                                    }
                                    else{
                                          body +=  ', '+ '"project_Name": ' + null;
                                    }                                                                            
                                    
                                    if(UnitFilter.Unit_Inventories__r[0].Marketing_Name_Doc__c != NULL) {                                            
                                        body +=  ', '+ '"marketing_Doc_Name": "' + UnitFilter.Unit_Inventories__r[0].Marketing_Name_Doc__r.Name +'"';
                                    }
                                     else{
                                          body +=  ', '+ '"marketing_Doc_Name": ' + null;
                                     }
                                     if(UnitFilter.Unit_Inventories__r[0].Marketing_Name_Doc__c != NULL) {    
                                      body +=  ', '+ '"unit_Logo_Image": "' + UnitFilter.Unit_Inventories__r[0].Marketing_Name_Doc__r.Marketing_Plan__c +'"';
                                      }
                                     else{
                                          body +=  ', '+ '"unit_Logo_Image": ' + null;
                                     }  
                                 } 
                                 else{
                                     body +=  ', '+ '"project_Name": ' + null
                                              + ', '+ '"marketing_Doc_Name": ' + null
                                              + ', '+ '"unit_Logo_Image": ' + null;              
                                 
                                 }
                                 
                                 /*if(UnitFilter.Unit_Inventories__r[0] != NULL) { 
                                     if(UnitFilter.Unit_Inventories__r[0].Marketing_Name_Doc__c != NULL) {    
                                      body +=  ', '+ '"unit_Logo_Image": "' + UnitFilter.Unit_Inventories__r[0].Marketing_Name_Doc__r.Marketing_Plan__c +'"';
                                      }
                                     else{
                                          body +=  ', '+ '"unit_Logo_Image": ' + null;
                                     }  
                                 }*/
                
               body += '}'; 
               unitMap.put(unitFilter.Id,body);
            }
            
            if(!unitMap.isEmpty()){
                for(String body: unitMap.values()){
                
                    if(jsonUnitBody != ''){
                        jsonUnitBody +=  ',' + body ;
                    }
                    else{
                        jsonUnitBody = '[' + body ;
                    }
                    
                } 
                jsonUnitBody += ']';   
            
            }
            else{
                jsonUnitBody = '[]';                
            }
              
            jsonBody = '{'  +  '"Data" :{' +  '"Project" :' + jsonProjectBody + ',' +  '"Unit" :' + jsonUnitBody + '}}';                                 
            RestContext.response.addHeader('Content-Type', 'application/json');           
            RestContext.response.responseBody = Blob.valueOf(jsonBody );
        
        }
        else {
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf('{"Error" : "URL Not found"}');    
        }
    } //End of GET method
}