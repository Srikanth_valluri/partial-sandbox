/*
 * Description - Class used for instantiating a task object
 *
 * Version        Date            Author            Description
 * 1.0            10/10/17        Vivek Shinde      Initial Draft 
 * 1.1            20/02/18        Vivek Shinde      Changes for assining ownership to admin user if case assigned to queue
 */
public with sharing class TaskUtility {
    
    public static Task getTask(sObject objGeneric, String strSubject, String strAssignedUser, String strProcessName,
        Date dtDueDate) {
        Task objTask = new Task();
        system.debug('>>>>objGeneric'+objGeneric.get('OwnerId'));
        objTask.OwnerId = String.valueOf(objGeneric.get('OwnerId')).startsWith('00G') ? Label.DefaultCaseOwnerId : (Id)objGeneric.get('OwnerId');
        system.debug('>>>>objTask.OwnerId'+objTask.OwnerId);
        objTask.WhatId = objGeneric.Id;
        objTask.Subject = strSubject;
        objTask.ActivityDate = dtDueDate;
        objTask.Assigned_User__c = strAssignedUser;
        objTask.Process_Name__c = strProcessName;
        objTask.Priority = 'High';
        objTask.Status = 'Not Started';
        return objTask;
    }
}