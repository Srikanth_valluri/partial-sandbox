/******************************************************************************************************************************************
Description: Assigning tasks to owner in round robin fashion
===========================================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By  | Comments
--------------------------------------------------------------------------------------------------------------------------------------------
1.1     | 30-12-2020        | Aishwarya Todkar  | 1. Added FM_Pre_Legal_From label

1.2     | 07-01-2021        | Aishwarya Todkar  | 1. Added property check for UMA-LTL
******************************************************************************************************************************************/
global class FM_PreLegalNoticeEmailBatch implements Database.Batchable<sObject>
                                        , Database.Stateful
                                        , Database.AllowsCallouts {
    
    
    List<SMS_History__c> smsLst  = new List<SMS_History__c>();
    Map<id,Booking_Unit__c> mapIdBU;
    Map<id, Attachment> mapBUIdAttach;
    EmailTemplate FM_PreLegalNoticeTemplate;
    List<EmailMessage> lstEmails;
    Id personRecTypeId, businessRecTypeId;
    List<Booking_Unit__c> lstBUTOUpdate;
    public FM_PreLegalNoticeEmailBatch() {
        FM_PreLegalNoticeTemplate = [SELECT 
                                ID, Subject, Body, HtmlValue, TemplateType
                            FROM 
                                EmailTemplate 
                            WHERE 
                                DeveloperName = 'Pre_Legal_Notice_Service_Charges' limit 1];
        personRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        businessRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();                        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        System.debug('***FM_PreLegalNoticeBatch  start****');

        /** 
         * Added by Aishwarya T on 07-01-2020
        */
        Set<String> SetProperties = new Set<String>();
        if( Label.UMA_LTL_Properties.startsWith( 'ON' ) ) {
            String properties = Label.UMA_LTL_Properties.subStringAfter( '-' );
            if( properties.contains( ',' ) ) {
                SetProperties.addAll( properties.split( ',' ) );
            }
            else {
                SetProperties.add( properties );
            }
        }
        String strQuery = 'SELECT Id, FM_Pre_Legal_Notice_sent_date__c, Send_FM_Pre_Legal_Notice__c, ' +
                        ' Booking__r.Account__r.Email__c,Building_Name__c,Unit_Name__c, Account_Id__c, ' +
                        ' Booking__r.Account__r.Name, Booking__r.Account__r.Email__pc, Booking__r.Account__r.IsPersonAccount, ' +
                        ' Booking__r.Account__r.RecordTypeId, Inventory__r.Building_Name__c FROM Booking_Unit__c '+
                        ' WHERE Send_FM_Pre_Legal_Notice__c = true AND FM_Pre_Legal_Notice_sent_date__c = NULL';
                        
                        if( !SetProperties.isEmpty() ) {
                            strQuery += ' AND Property_Name__c Not In:SetProperties AND Inventory__r.Property_Name__c NOT IN : SetProperties';
                        }
                        /*'Unit_Active__c = \'Active\' AND Registration_ID__c != Null' +
                        ' AND Booking__r.Account__c != NULL AND ( Booking__r.Account__r.Email__pc != NULL OR Booking__r.Account__r.Email__c != NULL) ' +
                        ' AND Unit_Name__c != NULL AND Building_Name__c != NULL' +
                        ' AND ';*/
        
        System.debug('strQuery :: ' + strQuery);
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<Booking_Unit__c> listBUs){ 
        System.debug('***FM_PreLegalNoticeBatch execute****');
        lstBUTOUpdate = new List<Booking_Unit__c>();
        lstEmails = new List<EmailMessage>();
        
        mapIdBU =new  Map<id,Booking_Unit__c>([SELECT Id, FM_Pre_Legal_Notice_sent_date__c, Send_FM_Pre_Legal_Notice__c, 
                        Booking__r.Account__r.Email__c,Inventory__r.Building_Name__c,Unit_Name__c, Booking__r.Account__r.Name, 
                        Booking__r.Account__r.Email__pc, Booking__r.Account__r.IsPersonAccount 
                        FROM Booking_Unit__c where Id In : listBUs]);
                        
        mapBUIdAttach =  new map<Id, Attachment>();
        List<Attachment> attachs = [SELECT id,ParentId,body,name from Attachment where parentId in : mapIdBU.keySet() order by createddate ];
        for(Attachment objAttach : attachs) {
            mapBUIdAttach.put(objAttach.parentId,objAttach);
        }
        system.debug('mapBUIdAttach==' + mapBUIdAttach);
        
        for(Booking_Unit__c objBU : listBUs) {
            if(mapBUIdAttach.containsKey(objBU.id)) {
                sendEmail(objBU, new List<Attachment> {mapBUIdAttach.get(objBU.id)});
            }
            else {
                sendEmail(objBU, new List<Attachment> {});
            }
        }
        
        System.debug('lstEmails=== ' + lstEmails);
        if(lstEmails.size() > 0) {
            if(!Test.isRunningTest()) {
                insert lstEmails;
            }
            
            //Map<Id,EmailMessage> mapBuIdToEmailMsg = new Map<Id,EmailMessage>();
            Attachment[] attachments=new Attachment[]{};
            for(EmailMessage objEmailmsg : lstEmails) {
                //mapBuIdToEmailMsg.put(objEmailmsg.Booking_Unit__c,objEmailmsg);
                if(mapBUIdAttach.containsKey(objEmailmsg.Booking_Unit__c)){
                    Attachment attachment = new Attachment();
                    attachment.Name = mapBUIdAttach.get(objEmailmsg.Booking_Unit__c).Name;
                    attachment.Body = mapBUIdAttach.get(objEmailmsg.Booking_Unit__c).Body;
                    attachment.ParentId = objEmailmsg.Id; 
                    attachments.add(attachment);
                }
            }
             if(attachments.size() > 0) {
                insert attachments;
             }
        }
        
        if(lstBUTOUpdate.size() > 0) {
            update lstBUTOUpdate;
        }
    }

    global void finish(Database.BatchableContext BC){
        System.debug('***FM_PreLegalNoticeBatch finish****');
    }
    
     void sendEmail(Booking_Unit__c objBU, List<Attachment> lstAttach) {
        
        String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue,contentBody = '';
        String subject, FM_PreLegalNoticeTemplateId = '', strAccountId, strBUId, bccAddress ='';
        
        strAccountId = objBU.Account_Id__c != null ? String.valueOf(objBU.Account_Id__c) : objBU.Booking__r.Account__c;
        strBUId = String.valueOf(objBU.id);
        bccAddress = Label.SF_Copy_EMail_Address;
        if( Label.FM_Pre_Legal_From.equalsIgnoreCase( 'CRM' ) ) {
            fromAddress = 'atyourservice@damacproperties.com';
        }
        else {
            fromAddress = 'servicecharges@loams.ae';//Label.Service_Charges_Email;
        }
        
        // If Person Acount 
        if(objBU.Booking__r.Account__r.recordTypeId == personRecTypeId
        && objBU.Booking__r.Account__r.Email__pc != null) {
            toAddress = objBU.Booking__r.Account__r.Email__pc;
        }
        //If business account
        else if(objBU.Booking__r.Account__r.recordTypeId == businessRecTypeId
        && objBU.Booking__r.Account__r.Email__c != null){
            toAddress = objBU.Booking__r.Account__r.Email__c;
        }
        subject = FM_PreLegalNoticeTemplate.subject;
        System.debug('is Subject contains Tower ??? == ' + subject.contains('{Tower name}'));
        if(objBU.Inventory__r.Building_Name__c != null) {
            System.debug('in Tower contains if');
            subject = subject.replace('{Tower name}', objBU.Inventory__r.Building_Name__c);
        }
        else{
            System.debug('in Tower contains if');
            subject = subject.replace('{Tower name}','');
        }
        
        
        
        if(objBU.Unit_Name__c != null)
            subject = subject.replace('{Unit number}',objBU.Unit_Name__c);
        else{
            subject = subject.replace('{Unit number}','');
        }
        
        System.debug('subject==-=' + subject);
        
        contentType = 'text/html';
        
        replyToAddress = Label.Service_Charges_replyTo_Address;
        
        if(FM_PreLegalNoticeTemplate.htmlValue != null) {
            contentValue = FM_PreLegalNoticeTemplate.htmlValue;
            contentBody = FM_PreLegalNoticeTemplate.body.replaceAll('\\<.*?\\>', '');
            String strName = getCamelCase(objBU.Booking__r.Account__r.Name);
            
            contentValue = contentValue.replace('{Customer}', strName);
            
            System.debug('is contentValue contains Tower ??? == ' + contentValue.contains('{Tower name}'));
            if(objBU.Inventory__r.Building_Name__c != null) {
                System.debug('in Tower contains if');
                contentValue = contentValue.replace('{Tower name}', objBU.Inventory__r.Building_Name__c);
            }
            else{
                System.debug('in Tower else');
                contentValue = contentValue.replace('{Tower name}', '');
            }
            if(objBU.Unit_Name__c != null){
                contentValue = contentValue.replace('{Unit number}', objBU.Unit_Name__c);
            }
            else{
                contentValue = contentValue.replace('{Unit number}', '');
            }
        }

        String strCCAddress = '';// = getCCAddress(objBU,toAddress);
        //Call method to send an email
        if(toAddress != '') {       
            SendGridEmailService.SendGridResponse objSendGridResponse = 
                SendGridEmailService.sendEmailService(toAddress,'', strCCAddress, '', bccAddress, ''
                                            , subject, '', fromAddress,
                                            '', replyToAddress, '', contentType, contentValue, '',
                                            lstAttach);
            system.debug('toAddress === ' + toAddress);
            system.debug('fromAddress === ' + fromAddress);
            system.debug('subject === ' + subject);
            system.debug('contentValue === ' + contentValue);
            system.debug('contentType === ' + contentType);
            system.debug('objSendGridResponse === ' + objSendGridResponse);
            String sgResponse = Test.isRunningTest() ? 'Accepted' :objSendGridResponse.ResponseStatus;
            if (sgResponse == 'Accepted') {
                EmailMessage mail = new EmailMessage();
                mail.Subject = subject;
                mail.MessageDate = System.Today();
                mail.Status = '3';//'Sent';
                mail.relatedToId= strAccountId;
                mail.ToAddress = toAddress;
                mail.FromAddress = fromAddress;
                mail.TextBody = contentValue;//contentValue.replaceAll('\\<.*?\\>', '');
                mail.Sent_By_Sendgrid__c = true;
                mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                mail.Booking_Unit__c = strBUId;
                mail.CcAddress = strCCAddress;
                mail.BccAddress = bccAddress;
                lstEmails.add(mail);
                system.debug('Mail obj == ' + mail);
                
                objBU.FM_Pre_Legal_Notice_sent_date__c = system.today();
                lstBUTOUpdate.add(objBU);
            }
        }
    }

/*********************************************************************************
 * Method Name : getCamelCase
 * Description : Convert case of Customer name into Camel case.
 * Return Type : String
 * Parameter(s): Customer Name
**********************************************************************************/
    String getCamelCase(String strName) {
        
        strName = strName.toLowerCase();    
        List<String> names = strName.split(' ');
        for (Integer i = 0; i < names.size(); i++)
            names[i] = names[i].capitalize();
        strName = String.join(names, ' ');

        System.debug('getCamelCase strName == ' + strName);
        return strName;
    }
}