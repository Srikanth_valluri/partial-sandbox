public without sharing class FM_CreateTenantPortalUser {

    private static User objTenantUser ;

    @InvocableMethod
    public static void createPortalUser( list<FM_Case__c> lstCases ) {
        FM_Case__c objCase = lstCases[0];

        if( String.isNotBlank( objCase.Tenant__c ) ) {
            Account objAcc = [ SELECT Id
                                    , PersonContactId
                                 FROM Account
                                WHERE Id = :objCase.Tenant__c ];
            if( objAcc != NULL && String.isNotBlank( objAcc.PersonContactId ) &&
                !isPortalUser( objAcc.PersonContactId ) ) {
                createPortalUser( objCase, objAcc.PersonContactId ) ;
            }
            else if( !isPortalUserActive() ) {
                activateTenantUser( objCase );
            }
        }
    }


    public static boolean isPortalUser( Id contactId ) {
        if( String.isNotBlank( contactId ) ) {
            list<User> lstUser = [ SELECT Id
                                        , IsActive
                                     FROM User
                                    WHERE ContactId = :contactId ];
            if( lstUser != NULL && lstUser.size() > 0 ) {
                objTenantUser = lstUser[0] ;
                return true ;
            }
        }
        return false ;
    }

    private static String getUniqueUserName( String strUserName ) {
    	system.debug('== strUserName =='+strUserName);
    	if( String.isNotBlank( strUserName ) ) {
    		list<User> lstUser = [ SELECT Id
                                        , IsActive
                                        , UserName
                                     FROM User
                                    WHERE UserName = :strUserName
                                 ORDER BY UserName DESC
                                 	LIMIT 1 ];
            system.debug('== lstUser =='+lstUser);
            if( lstUser != NULL && !lstUser.isEmpty() ) {
            	String strName = lstUser[0].UserName ;
            	system.debug('== strName =='+strName);
        		String strPostfix = strName.substringAfterLast('.');
        		system.debug('== strPostfix =='+strPostfix);
            	if( String.isNotBlank( strPostfix ) && strPostfix.trim().isNumeric() ) {
            		Integer intPostfix = Integer.valueOf( strPostfix );
            		intPostfix++ ;
            		String strNewName = strName.substringBeforeLast('.') + intPostfix ;
            		system.debug('== strNewName =='+strNewName);
            		return strNewName ;
            	}
            	else if( String.isNotBlank( strPostfix ) && !strPostfix.trim().isNumeric() ) {
            		String strNewName = strName + '.01' ;
            		system.debug('== strNewName =='+strNewName);
            		return strNewName ;
            	}
            }
            else  {
            	return strUserName ;
            }
    	}
    	return '' ;
    }

    @testVisible
    private static boolean isPortalUserActive() {
        if( objTenantUser != NULL && objTenantUser.IsActive ) {
            return true ;
        }
        return false ;
    }

    public static void createPortalUser( FM_Case__c objCase, Id contactId ) {

        List<Profile> lstProfile;
        LoamsCommunitySettings__c loamsCommunitySettings = LoamsCommunitySettings__c.getInstance();
        if (String.isNotBlank(loamsCommunitySettings.TenantProfileName__c)) {
            lstProfile = [SELECT  Id, Name FROM Profile WHERE Name = :loamsCommunitySettings.TenantProfileName__c];
        }
        system.debug('==lstProfile==='+lstProfile);
        User u = new User();
        u.Username = getUniqueUserName( objCase.Tenant_Email__c );
        system.debug('==Username==='+u.Username);
        u.Email = objCase.Tenant_Email__c;
        u.FirstName = objCase.First_Name__c;
        u.LastName = objCase.Last_Name__c;
        u.ContactId = contactId;

        u.CommunityNickname = String.isNotBlank( objCase.First_Name__c ) ?
        					  objCase.First_Name__c.subString(0,1) : '' ;

      	u.CommunityNickname += String.isNotBlank( objCase.Last_Name__c ) ?
        					   objCase.Last_Name__c.subString(0,1) : '' ;

        u.CommunityNickname += system.now().time();

        System.debug('u.CommunityNickname = ' + u.CommunityNickname);

        if( lstProfile!= NULL && !lstProfile.isEmpty() ) {
            u.ProfileId = lstProfile[0].id;
        }

        u.Phone = objCase.Mobile_no__c;
        u.MobilePhone = objCase.Mobile_no__c;
        u.LanguageLocaleKey='en_US';
        u.LocaleSidKey='en_GB';
        u.TimeZoneSidKey='America/Los_Angeles';
        u.Alias = objCase.First_Name__c.substring(0,1) + objCase.Last_Name__c.substring(0,1) + system.now().minute() + system.now().second();
        u.EmailEncodingKey = 'UTF-8';
        Database.DMLOptions dlo = new Database.DMLOptions();
        dlo.EmailHeader.triggerUserEmail= true;
        Database.saveresult sr = Database.insert(u,dlo);
        system.debug('==Database.saveresult for inserting a community user==='+sr);

        if ( sr.isSuccess() ) {
            // Operation was successful, so get the ID of the record that was processed
            Error_Log__c objLog = FM_Utility.createErrorLog( objCase.Account__c,
                                        objCase.Booking_Unit__c,
                                        objCase.Id ,
                                        'Tenant Registration',
                                        'Portal User Enabled Successfully' );
            insert objLog ;
        }
        else {
            // Operation failed, so get all errors
            String strErrorMessage = 'Error Occured while enabling the customer as Portal User : ';
            for( Database.Error err : sr.getErrors() ) {
                strErrorMessage += err.getMessage() + '\n' ;
            }
            Error_Log__c objLog = FM_Utility.createErrorLog(  objCase.Account__c,
                                        objCase.Booking_Unit__c,
                                        objCase.Id ,
                                        'Tenant Registration',
                                        strErrorMessage );
            insert objLog ;
        }

    }

    @testVisible
    private static void activateTenantUser( FM_Case__c objCase ) {
        try {
            objTenantUser.IsActive = true ;
            update objTenantUser ;
            Error_Log__c objLog = FM_Utility.createErrorLog( objCase.Account__c,
                                                             objCase.Booking_Unit__c,
                                                             objCase.Id ,
                                                             'Tenant Registration',
                                                             'Portal User Activated Successfully' );
            insert objLog ;
        }
        catch( Exception e ) {
            Error_Log__c objLog = FM_Utility.createErrorLog( objCase.Account__c,
                                                             objCase.Booking_Unit__c,
                                                             objCase.Id ,
                                                             'Tenant Registration',
                                                             'Portal User Activation Failed : ' + e.getMessage() );
            insert objLog ;
        }
    }
}