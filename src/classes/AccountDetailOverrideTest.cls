@isTest
private class AccountDetailOverrideTest{
    static Account objAcc;
    static NSIBPM__Service_Request__c objDealSR;
    static list<Booking__c> listCreateBookingForAccount;
    static list<Booking_Unit__c> listCreateBookingUnit;
    static Location__c objL;
    
    static void init(){
        objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        
        objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        listCreateBookingForAccount = TestDataFactory_CRM.createBookingForAccount(objAcc.Id, objDealSR.Id, 1);
        insert listCreateBookingForAccount;
        
        objL = new Location__c();
        objL.Name = 'DFA';
        objL.Location_Id__c = 'DFA';
        insert objL;
        
        listCreateBookingUnit = TestDataFactory_CRM.createBookingUnits(listCreateBookingForAccount, 1);
        listCreateBookingUnit[0].Unit_Name__c = 'DFA/ABC/123';
        listCreateBookingUnit[0].Building_Name__c = 'DFA';
        insert listCreateBookingUnit;
    }
    
    private static testMethod void positiveTest(){
        init();
        FM_User__c objUser = new FM_User__c();
        objUser.Building__c = objL.Id;
        objUser.FM_User__c = UserInfo.getUserId();
        objUser.FM_Role__c = 'FM Admin';
        insert objUser;
        
        Test.setCurrentPage(Page.Account_Detail_Override);
        ApexPages.StandardController sc = new ApexPages.StandardController(objAcc);
        AccountDetailOverride objCls = new AccountDetailOverride(sc);
        objCls.redirectToDetail();
    }
    
    private static testMethod void negativeTest1(){
        init();
        FM_User__c objUser = new FM_User__c();
        objUser.Building__c = objL.Id;
        objUser.FM_User__c = UserInfo.getUserId();
        objUser.FM_Role__c = 'FM Admin';
        insert objUser;
        
        Account objA = TestDataFactory_CRM.createPersonAccount();
        objA.Party_Id__c = '12081989';
        objA.Party_Id__pc = '12081989';
        insert objA;
        
        Test.setCurrentPage(Page.Account_Detail_Override);
        ApexPages.StandardController sc = new ApexPages.StandardController(objA);
        AccountDetailOverride objCls = new AccountDetailOverride(sc);
        objCls.redirectToDetail();
    }
    
    private static testMethod void negativeTest2(){
        init();
        
        Test.setCurrentPage(Page.Account_Detail_Override);
        ApexPages.StandardController sc = new ApexPages.StandardController(objAcc);
        AccountDetailOverride objCls = new AccountDetailOverride(sc);
        objCls.redirectToDetail();
    }
}