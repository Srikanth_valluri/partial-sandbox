@isTest
public class EnrichmentServiceTest {
    @isTest
    public static void testUpdateTDService(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        //acc.PersonEmail = 'abc@xyz.com';
        insert acc;
        
        Credentials_Details__c objCredDetail = new Credentials_Details__c();
        objCredDetail.Name = 'Enrichment';
        objCredDetail.Endpoint__c = 'https://damaccustenrich.servicebus.windows.net/customer-enrichment-hub-prod/messages';
        objCredDetail.Password__c = 'test';
        objCredDetail.User_Name__c = 'test';
        insert objCredDetail;
        
        Test.startTest();
            EnrichmentService.sendHttpRequest(acc.Id, 'Hello@xyz.com');
        Test.stopTest();
    }
}