Public class InquiryRMAssignment {
    static Map <ID, Inquiry_User_Assignment_Rules__c> userRulesMap;
    static Map <String, Boolean> existingUsersInQueueIds;
    Static Map <ID, User> userDetailsMap;
    public static void checkAlreadyAssignedRMs () {
        for (Inquiry_Assignment_Algorithm__c inq : [SELECT RM_User__c, RM_Assigned__c, RM_User_ID__c
                                                    FROM Inquiry_Assignment_Algorithm__c 
                                                    WHERE RM_User_ID__c != NULL]) 
        {
            existingUsersInQueueIds.put (inq.RM_User_ID__c, inq.RM_Assigned__c);
        }
    }
    public static List <Inquiry__c> checkForRM (List <Inquiry__c> inqList) {
        userRulesMap = new Map <ID, Inquiry_User_Assignment_Rules__c> ();
        userDetailsMap = new Map <ID, User> ();
        existingUsersInQueueIds = new Map <String, Boolean> ();
        for (Inquiry_User_Assignment_Rules__c rule :[SELECT 
                                SetupOwnerId,  
                                Net_Direct_Sales_Rank__c
                            FROM 
                                Inquiry_User_Assignment_Rules__c
                            WHERE User_Profile__c = 'Property Consultant' AND setupOwnerId != NULL 
                            Order By Net_Direct_Sales_Rank__c ]) {
            userRulesMap.put (rule.setupOwnerId, rule);    
            existingUsersInQueueIds.put (rule.setupOwnerId, false);       
        }   
        checkAlreadyAssignedRMs ();
        System.Debug (userRulesMap.keySet ());
        userDetailsMap = new Map <ID, User> ([SELECT Name FROM USER 
                            WHERE ISActive = TRUE 
                            AND Is_Blacklisted__c = false 
                            AND isUserOnLeave__c = false 
                            AND ID IN: userRulesMap.keySet ()]);
        
        for (Inquiry__c inq :inqList) {
            checkUser (inq, userDetailsMap.values ());
        }
        List <Inquiry_Assignment_Algorithm__c> algToUpdate = new List <Inquiry_Assignment_Algorithm__c> ();
        for (String userId :existingUsersInQueueIds.keySet ()) {
            Inquiry_Assignment_Algorithm__c alg = new Inquiry_Assignment_Algorithm__c ();
            alg.RM_User_ID__c = userId;
            alg.RM_User__c = userId;
            alg.RM_Assigned__c = existingUsersInQueueIds.get (userId);
            algToUpdate.add (alg);
        }
        Upsert algToUpdate RM_User_ID__c;
        return inqList;
    }
    
    public static void checkUser (Inquiry__c inq, List <User> totalUsers) {
        Map <Decimal, Id> powerLineUsersMap = new Map <Decimal, Id> ();
        List <Decimal> sortedUsers = new List <Decimal> ();
        for (User u :totalUsers) {
            if (userRulesMap.containsKey (u.Id)) {
                powerLineUsersMap.put (userRulesMap.get (u.ID).Net_Direct_Sales_Rank__c, u.ID);
                sortedUsers.add (userRulesMap.get (u.ID).Net_Direct_Sales_Rank__c);
            }
        }
        sortedUsers.sort ();
        List <ID> powerLineUsersWithOrder = new List <ID> ();
        for (Decimal val :sortedUsers)
            powerLineUsersWithOrder.add (powerLineUsersMap.get (val));
            
        System.Debug (' POWER LINE USERS ');
        System.Debug (powerLineUsersWithOrder);
        
        if (powerLineUsersWithOrder.size () > 0) {
            List <ID> finalPowerUsers = new List <ID> ();
            for (Id userId :powerLineUsersWithOrder) {
                Boolean flag = existingUsersInQueueIds.get (userId);
                if (!flag) {
                    finalPowerUsers.add (userID);
                }
            }
            System.Debug (finalPowerUsers);
            
            if (finalPowerUsers.size () > 0) {
                ID powerLineUserID = finalPowerUsers[0];
                System.Debug (powerLineUserID);
                inq.Recommended_RM__c = powerLineUserID;
                existingUsersInQueueIds.put (powerLineUserID, true);
                if (finalPowerUsers.size () == 1) {                
                    for (User u : totalUsers) {                        
                        existingUsersInQueueIds.put (u.Id, false);
                    }
                }
            }
            else {
                for (User u : totalUsers) {
                    existingUsersInQueueIds.put (u.Id, false);
                }
                checkUser (inq, totalUsers);
            }
        }
    }
}