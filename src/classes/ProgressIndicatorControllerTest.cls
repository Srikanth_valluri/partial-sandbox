@isTest
public class ProgressIndicatorControllerTest {
    
    private static final Id DEAL_SR_RECORD_TYPE_ID = DamacUtility.getRecordTypeId('NSIBPM__Service_Request__c', 'Deal');
    private static final Id CHANGE_AGENT_SR_RECORD_TYPE_ID = DamacUtility.getRecordTypeId('NSIBPM__Service_Request__c', 'Change Agent');
    
    static testMethod void myUnitTest(){
        
        List<User> createdUsers = TestDataFactory.createTestUserRecords(new List<User>{new User(UserRoleId='00E9E000000MTyT', ManagerId = UserInfo.getUserId(), IPMS_Employee_ID__c = '1000'),
            new User(UserRoleId='00E9E000000MTyO', ManagerId = UserInfo.getUserId(), IPMS_Employee_ID__c = '1001'), 
            new User(UserRoleId='00E9E000000MTxz', ManagerId = UserInfo.getUserId(), IPMS_Employee_ID__c = '1002'), 
            new User(UserRoleId='00E9E000000MTxk', ManagerId = UserInfo.getUserId(), IPMS_Employee_ID__c = '1003'), 
            new User(UserRoleId='00E9E000000MTxf', ManagerId = UserInfo.getUserId(), IPMS_Employee_ID__c = '1004')});
        
        system.runAs(new User(Id=UserInfo.getUserId())){
            List<NSIBPM__SR_Status__c> createdSRStatus = TestDataFactory.createSrStatusRecords(new List<NSIBPM__SR_Status__c>{new NSIBPM__SR_Status__c()});
            
            List<NSIBPM__SR_Template__c> createdSRTemplates = TestDataFactory.createTestTemplateRecords(new List<NSIBPM__SR_Template__c>{
                new NSIBPM__SR_Template__c(Name='Deal',	NSIBPM__SR_RecordType_API_Name__c = 'Deal'),
                    new NSIBPM__SR_Template__c(Name='Change Agent',	NSIBPM__SR_RecordType_API_Name__c = 'Change_Agent')});
            
            List<NSIBPM__SR_Steps__c> createSrSteps = TestDataFactory.createSrStepsRecords(new List<NSIBPM__SR_Steps__c>{
                new NSIBPM__SR_Steps__c(NSIBPM__SR_Template__c = createdSRTemplates[0].Id ),
                new NSIBPM__SR_Steps__c(NSIBPM__SR_Template__c = createdSRTemplates[1].Id )    });
            
            List<NSIBPM__Service_Request__c> createdSRs = TestDataFactory.createTestServiceRequestRecords(new List<NSIBPM__Service_Request__c>{
                new NSIBPM__Service_Request__c(OwnerId = createdUsers[0].Id, recordTypeId = DEAL_SR_RECORD_TYPE_ID, NSIBPM__Internal_SR_Status__c = createdSRStatus[0].Id, 
                                               NSIBPM__SR_Template__c = createdSRTemplates[0].Id, Registration_Date__c = Date.newInstance(2017,07,03),
                                               NSIBPM__Submitted_DateTime__c = system.now(), Closed_DateTime__c = system.today().addDays(2)),
                    new NSIBPM__Service_Request__c(OwnerId = createdUsers[1].Id, recordTypeId = DEAL_SR_RECORD_TYPE_ID, NSIBPM__Internal_SR_Status__c = createdSRStatus[0].Id, 
                                                   NSIBPM__SR_Template__c = createdSRTemplates[0].Id, Registration_Date__c = Date.newInstance(2017,07,03),
                                                   NSIBPM__Submitted_DateTime__c = system.now()),
                    new NSIBPM__Service_Request__c(OwnerId = createdUsers[2].Id, recordTypeId = CHANGE_AGENT_SR_RECORD_TYPE_ID, NSIBPM__Internal_SR_Status__c = createdSRStatus[0].Id, 
                                                   NSIBPM__SR_Template__c = createdSRTemplates[1].Id, Registration_Date__c = Date.newInstance(2017,07,03),
                                                   NSIBPM__Submitted_DateTime__c = system.now())});
            List<NSIBPM__Status__c> createdStepStatus = TestDataFactory.createStatusRecords(new List<NSIBPM__Status__c>{
                new NSIBPM__Status__c(NSIBPM__Code__c='APPROVED', Name='Approved'), new NSIBPM__Status__c(NSIBPM__Type__c='Start')});
            List<NSIBPM__Step__c> createdSteps = TestDataFactory.createTestStepRecords(new List<NSIBPM__Step__c>{
                new NSIBPM__Step__c(NSIBPM__SR__c = createdSRs[0].Id, NSIBPM__Status__c = createdStepStatus[0].Id, NSIBPM__SR_Step__c = createSrSteps[0].Id),
                    new NSIBPM__Step__c(NSIBPM__SR__c = createdSRs[1].Id, NSIBPM__Status__c = createdStepStatus[1].Id, NSIBPM__SR_Step__c = createSrSteps[0].Id)});
            
            
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(createdSRs[0]);
            ApexPages.currentPage().getParameters().put('Id',createdSRs[0].Id);
            ProgressIndicatorController objProgressIndicator = new ProgressIndicatorController(sc);
            
            ProgressIndicatorController.StepGroupChildData st1 = new ProgressIndicatorController.StepGroupChildData(system.today().addDays(1));
            ProgressIndicatorController.StepGroupChildData st2 = new ProgressIndicatorController.StepGroupChildData(system.today().addDays(2));
            st1.compareTo((Object)st2);
            
            ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(createdSRs[1]);
            ApexPages.currentPage().getParameters().put('Id',createdSRs[1].Id);
            ProgressIndicatorController objProgressIndicator1 = new ProgressIndicatorController(sc1);
            
            ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(createdSRs[2]);
            ApexPages.currentPage().getParameters().put('Id',createdSRs[2].Id);
            ProgressIndicatorController objProgressIndicator2 = new ProgressIndicatorController(sc2);
        }
    }
}