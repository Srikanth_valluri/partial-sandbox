/************************************************************************************************
 * @Name              : VirtualHandover_MSTeamsIntegration
 * @Test Class Name   : VirtualHandover_MSTeamsIntegrationTest
 * @Description       : Class for accessing Team Meetings/Calendar Events from Microsoft Graph API 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         Rohit         10/08/2020       Created
***********************************************************************************************/

global class VirtualHandover_MSTeamsIntegration {
    public static API_Integration_Value__c apiValue = [SELECT Id, Refresh_Token__c, Client_Id__c, Client_Secret__c,
                                                            Endpoint_URL__c, Redirect_URI__c, Scope__c, Event_Creation_URL__c,
                                                            Tenant_Id__c, Username__c, Password__c, Meeting_EndPoint_URL__c
                                                        FROM API_Integration_Value__c WHERE name = 'Virtual Handover Graph API'
                                                        AND  Username__c = 'virtual.handover@damacproperties.com' LIMIT 1];

    public static API_Integration_Value__c dldApiValue = [SELECT Id, Refresh_Token__c, Client_Id__c, Client_Secret__c,
                                                            Endpoint_URL__c, Redirect_URI__c, Scope__c, Event_Creation_URL__c,
                                                            Tenant_Id__c, Username__c, Password__c, Meeting_EndPoint_URL__c
                                                        FROM API_Integration_Value__c WHERE name = 'Virtual Handover Graph API'
                                                        AND  Username__c = 'DLDTrustee.Meeting@damacproperties.com' LIMIT 1];
                                                        

    
    /************************************************************************************************
    * @Description : Fetches AccessToken using RefreshToken
    * @Params      : None
    * @Return      : String token - Returns Access Token from Microsoft Graph API
    ************************************************************************************************/
    public static String fetchAccessToken(){
        String accesToken = '';
        Http http = new Http();
        if(apiValue != null){
            String refresh_token = apiValue.Refresh_Token__c;
        HttpRequest tokenReq = new HttpRequest();
        String endPoint = apiValue.Endpoint_URL__c + '/' + apiValue.Tenant_Id__c + '/oauth2/v2.0/token';
        String body = 'client_id=' + EncodingUtil.urlEncode(apiValue.Client_Id__c, 'UTF-8')
                    + '&client_secret=' + EncodingUtil.urlEncode(apiValue.Client_Secret__c, 'UTF-8')
                    + '&scope=' + apiValue.Scope__c
                    + '&refresh_token=' + EncodingUtil.urlEncode(apiValue.Refresh_Token__c, 'UTF-8')
                  //  + '&redirect_uri=' + EncodingUtil.urlEncode(apiValue.Redirect_URI__c, 'UTF-8')
                   // + '&username=' + EncodingUtil.urlEncode(apiValue.Refresh_Token__c, 'UTF-8')
                   // + '&password=' + EncodingUtil.urlEncode(apiValue.Refresh_Token__c, 'UTF-8')
                   // + '&grant_type=client_credentials';
                    + '&grant_type=refresh_token';
        tokenReq.setEndpoint(endPoint);
        tokenReq.setBody(body);
        tokenReq.setMethod('POST');
        tokenReq.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        HttpResponse tokenRes = new HttpResponse();
        try{
            String responseBody = '';
            if(!Test.isRunningTest()){
                tokenRes = http.send(tokenReq);
                responseBody = tokenRes.getBody();
            } else{
                responseBody = '{"token_type": "Bearer","scope": "Calendars.Read","expires_in": 3599,"ext_expires_in": 3599,'
                                + '"access_token": " accesstoken","refresh_token": "refreshtoken"}';
            }
            VirtualHandover_MSTeams_Token_JSON token = VirtualHandover_MSTeams_Token_JSON.parse(responseBody);
            return token.access_token;
        } catch(exception e){
            System.debug('Exception: ' + e);
            return '';
        }
        }
        return null;
    }

    /************************************************************************************************
    * @Description : Fetches AccessToken using RefreshToken
    * @Params      : None
    * @Return      : String token - Returns Access Token from Microsoft Graph API
    ************************************************************************************************/
    public static String fetchDLDAccessToken(){
        String accesToken = '';
        Http http = new Http();
        if(dldApiValue != null){
            String refresh_token = dldApiValue.Refresh_Token__c;
        HttpRequest tokenReq = new HttpRequest();
        String endPoint = dldApiValue.Endpoint_URL__c + '/' + dldApiValue.Tenant_Id__c + '/oauth2/v2.0/token';
        String body = 'client_id=' + EncodingUtil.urlEncode(dldApiValue.Client_Id__c, 'UTF-8')
                    + '&client_secret=' + EncodingUtil.urlEncode(dldApiValue.Client_Secret__c, 'UTF-8')
                    + '&scope=' + dldApiValue.Scope__c
                    + '&refresh_token=' + EncodingUtil.urlEncode(dldApiValue.Refresh_Token__c, 'UTF-8')
                    + '&grant_type=refresh_token';
        tokenReq.setEndpoint(endPoint);
        tokenReq.setBody(body);
        tokenReq.setMethod('POST');
        tokenReq.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        HttpResponse tokenRes = new HttpResponse();
        System.debug('body-->' +body);
        try{
            String responseBody = '';
            if(!Test.isRunningTest()){
                tokenRes = http.send(tokenReq);
                System.debug('tokenRes-->' +tokenRes);
                responseBody = tokenRes.getBody();
                System.debug('responseBody-->' +responseBody);
            } else{
                responseBody = '{"token_type": "Bearer","scope": "Calendars.Read","expires_in": 3599,"ext_expires_in": 3599,'
                                + '"access_token": " accesstoken","refresh_token": "refreshtoken"}';
            }
            VirtualHandover_MSTeams_Token_JSON token = VirtualHandover_MSTeams_Token_JSON.parse(responseBody);
            return token.access_token;
        } catch(exception e){
            System.debug('Exception: ' + e);
            return '';
        }
        }
        return null;
    }

    /************************************************************************************************
    * @Description : Creates OnlineMeeting (Teams Meeting) using Graph API
    * @Params      : 1. String token - AccessToken to access the Graph API Endpoints
                     2. String startTime - Start time of Teams Meeting
                     3. string EndTime - end time of Teams Meeting
    * @Return      : VirtualHandover_TeamsMeeting_JSON - returns the JSON record of response for Create Meeting
    *                                                from Graph API
    ************************************************************************************************/
    public static VirtualHandover_TeamsMeeting_JSON createMeeting(String token, String subject, String startTime, string EndTime){
        if(token != ''){
            Http http = new Http();
            HttpRequest meetingReq = new HttpRequest();
            meetingReq.setHeader('Authorization', 'Bearer ' + token);
           
            string body = '{"startDateTime":"' + startTime + '",'
                      + '"endDateTime":"' + endTime + '",'
                      + '"subject":"' + subject + '"}';
            meetingReq.setEndpoint(apiValue.Meeting_EndPoint_URL__c);
            meetingReq.setMethod('POST');
            system.debug('Meeting Request Body: ' + body);
            meetingReq.setBody(body);
            meetingReq.setHeader('Accept', 'application/json');
            meetingReq.setHeader('Content-Type', 'application/json');
            HttpResponse meetingRes = new HttpResponse();
            String responseBody = '';
            try{
                if(!Test.isRunningTest()){
                    meetingRes = http.send(meetingReq);
                    responseBody = meetingRes.getBody();
                } else{
                    responseBody = '{"id":"testid","startDateTime":"2020-03-27T13:21:03Z","endDateTime":"2020-03-27T13:51:03Z","joinUrl":"joinUrl"}';
                }
                system.debug('create meeting response body: ' + responseBody);
                VirtualHandover_TeamsMeeting_JSON meetingDetails = VirtualHandover_TeamsMeeting_JSON.parse(responseBody);
                return meetingDetails;
            } catch(exception e){
                System.debug('Exception: ' + e);
            }
        }
        return null;
    }

    
    /************************************************************************************************
    * @Description : Invokes createMeeting() and creates Calendar Event using Graph API
    * @Params      : 1. DateTime startTime - startTime of Calendar Event
                     2. DateTime endTime - endTime of Calendar Event
                     3. String emailList - List of Calendar Event Attendess
                     4. String subject - subject of Calendar Event
                     5. String messageBody - Message content for Calendar Event Invite
                     6. CallingList__c  - Calling List record
    * @Return      : 
    ************************************************************************************************/
    public static String createTeamsMeeting(DateTime startTime, DateTime endTime, String emailList, String subject, 
                                String messageBody, Calling_List__c callinglist){
        String returnValue;
        String token = fetchAccessToken();
        String msg = '';
        String stTime = startTime.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        String enTime = endTime.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        VirtualHandover_TeamsMeeting_JSON meetingDetails = createMeeting(token, subject , stTime, enTime);
        system.debug('meetingDetails: ' + meetingDetails);
        if(meetingDetails != null && meetingDetails.joinURL != null && meetingDetails.joinURL != ''){
            returnValue = meetingDetails.joinURL;
            
            String name = '';
            String attendees = '';
            
            for(String emailAdr: emailList.split(',')){
                if(attendees != ''){
                    attendees += ' , ';
                }
                attendees += '{"emailAddress": {"address":"' + emailAdr.trim() + '","name": ""},"type": "required"}';
            }
            if(!String.isBlank(callinglist.Account__r.Person_Business_Email__c)){
                attendees += ' , ';
                attendees += '{"emailAddress": {"address":"' + callinglist.Account__r.Person_Business_Email__c.trim() + '","name": ""},"type": "required"}';
            }
            
            String body = '{"subject": "' + subject + '",'
                          + '"body": { '
                          + '"contentType": "TEXT",'
                          + '"content": "' + messageBody + '\n\n'
                          + 'Meeting Link: ' + meetingDetails.joinURL + '"'
                          + '  },'
                          //+ '"onlineMeetingUrl": "' + meetingDetails.joinURL + '",'
                          + '"start": {"dateTime": "' + stTime + '","timeZone": "Greenwich Standard Time"},'
                          + '"end": {"dateTime": "' + enTime + '","timeZone": "Greenwich Standard Time"},'
                          + '"attendees": [' + attendees + ']}';  
            Http http = new Http();
            HttpRequest eventReq = new HttpRequest();
            eventReq.setHeader('Authorization', 'Bearer ' + token);
            eventReq.setMethod('POST');
            String endPoint = apiValue.Event_Creation_URL__c;
            eventReq.setEndpoint(endPoint);
            system.debug('Meeting Request Body: ' + body);
            eventReq.setBody(body);
            eventReq.setHeader('Accept', 'application/json');
            eventReq.setHeader('Content-Type', 'application/json');
            HttpResponse eventRes = new HttpResponse();
            VirtualHandover_OutlookEvents_JSON eventDetails = new VirtualHandover_OutlookEvents_JSON();
            try{
                String responseBody = '';
                if(!Test.isRunningTest()){
                    eventRes = http.send(eventReq);
                    responseBody = eventRes.getBody();
                }else{
                    responseBody = '{"id": "testid","subject": "DAMAC: Meeting with Property Advisor","bodyPreview": "testbody"}';    
                }
                system.debug('create meeting event response body: ' + responseBody);
                if(eventRes.getStatusCode() == 201 || Test.isRunningTest()){
                    // String jsonBody = responseBody.replace('"end":', '"end_x":');
                    // jsonBody = jsonBody.replace('"dateTime":', '"dateTime_x":');
                    // jsonBody = jsonBody.replace('"name":', '"name_x":');
                    // jsonBody = jsonBody.replace('"time":', '"time_x":');
                    eventDetails = VirtualHandover_OutlookEvents_JSON.parse(responseBody);
                    system.debug('eventDetails: ' + eventDetails);
                    msg = 'Event Created Successfully';
                    callingList.Remarks_Pratik__c = eventDetails.Id;
                    update callingList;
                } else{
                    msg = 'Event Creation Failed';
                }
            } catch(exception e){
                System.debug('Exception: ' + e);
            }
            
            return returnValue;
        }
        return 'Failure';
    }

    /************************************************************************************************
    * @Description : Invokes createMeeting() and creates Calendar Event using Graph API
    * @Params      : 1. DateTime startTime - startTime of Calendar Event
                     2. DateTime endTime - endTime of Calendar Event
                     3. String emailList - List of Calendar Event Attendess
                     4. String subject - subject of Calendar Event
                     5. String messageBody - Message content for Calendar Event Invite
                     6. CallingList__c  - Calling List record
    * @Return      : 
    ************************************************************************************************/
    public static void createDLDTeamsMeeting(DateTime startTime, DateTime endTime, String emailList, String subject, 
                                String messageBody, Calling_List__c callinglist){
        String returnValue;
        String token = fetchDLDAccessToken();
        String msg = '';
        String stTime = startTime.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        String enTime = endTime.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
        VirtualHandover_TeamsMeeting_JSON meetingDetails = createMeeting(token, subject , stTime, enTime);
        system.debug('meetingDetails: ' + meetingDetails);
        if(meetingDetails != null && meetingDetails.joinURL != null && meetingDetails.joinURL != ''){
            returnValue = meetingDetails.joinURL;
            
            String name = '';
            String attendees = '';
            
            for(String emailAdr: emailList.split(',')){
                if(attendees != ''){
                    attendees += ' , ';
                }
                attendees += '{"emailAddress": {"address":"' + emailAdr.trim() + '","name": ""},"type": "required"}';
            }
            
            String body = '{"subject": "' + subject + '",'
                          + '"body": { '
                          + '"contentType": "TEXT",'
                          + '"content": "' + messageBody + '\n'
                          + 'Meeting Link: ' + meetingDetails.joinURL + '"'
                          + '  },'
                          + '"start": {"dateTime": "' + stTime + '","timeZone": "Greenwich Standard Time"},'
                          + '"end": {"dateTime": "' + enTime + '","timeZone": "Greenwich Standard Time"},'
                          + '"attendees": [' + attendees + ']}';  
            Http http = new Http();
            HttpRequest eventReq = new HttpRequest();
            eventReq.setHeader('Authorization', 'Bearer ' + token);
            eventReq.setMethod('POST');
            String endPoint = dldApiValue.Event_Creation_URL__c;
            eventReq.setEndpoint(endPoint);
            system.debug('Meeting Request Body: ' + body);
            eventReq.setBody(body);
            eventReq.setHeader('Accept', 'application/json');
            eventReq.setHeader('Content-Type', 'application/json');
            HttpResponse eventRes = new HttpResponse();
            VirtualHandover_OutlookEvents_JSON eventDetails = new VirtualHandover_OutlookEvents_JSON();
            try{
                String responseBody = '';
                if(!Test.isRunningTest()){
                    eventRes = http.send(eventReq);
                    responseBody = eventRes.getBody();
                }else{
                    responseBody = '{"id": "testid","subject": "DAMAC: Meeting with Property Advisor","bodyPreview": "testbody"}';    
                }
                system.debug('create meeting event response body: ' + responseBody);
                if(eventRes.getStatusCode() == 201 || Test.isRunningTest()){
                    // String jsonBody = responseBody.replace('"end":', '"end_x":');
                    // jsonBody = jsonBody.replace('"dateTime":', '"dateTime_x":');
                    // jsonBody = jsonBody.replace('"name":', '"name_x":');
                    // jsonBody = jsonBody.replace('"time":', '"time_x":');
                    eventDetails = VirtualHandover_OutlookEvents_JSON.parse(responseBody);
                    system.debug('eventDetails: ' + eventDetails);
                    msg = 'Event Created Successfully';
                    callingList.Remarks_Pratik__c = eventDetails.Id;
                    callingList.Virtual_Meeting_Link__c = meetingDetails.joinURL;
                    update callingList;
                } else{
                    msg = 'Event Creation Failed';
                }
            } catch(exception e){
                System.debug('Exception: ' + e);
            }
            
            //return returnValue;
        }
        //return 'Failure';
    }


    /************************************************************************************************
    * @Description : Deletes the Calendar Event from Calendar using Graph API
    * @Params      : String eventId - Microsoft Event Id of the Calendar Event to be deleted 
    * @Return      : None
    ************************************************************************************************/
    public static void deleteEvent(String eventId){
        String token = fetchAccessToken();
        Http http = new Http();
        String msg = '';
        HttpRequest eventReq = new HttpRequest();
        eventReq.setHeader('Authorization', 'Bearer ' + token);
        String endPoint = apiValue.Event_Creation_URL__c;
        endPoint += '/' + eventId;
        eventReq.setEndpoint(endPoint);
        eventReq.setMethod('DELETE');
        eventReq.setHeader('Accept', 'application/json');
        HttpResponse eventRes = new HttpResponse();
        VirtualHandover_OutlookEvents_JSON eventDetails = new VirtualHandover_OutlookEvents_JSON();
        try{
            String responseBody = '';
            if(!Test.isRunningTest()){
                eventRes = http.send(eventReq);
                responseBody = eventRes.getBody();
            } else{
                responseBody = '';
            }
            system.debug('create meeting response body: ' + responseBody);
            if(eventRes.getStatusCode() == 204){
                msg = 'Event Deleted Successfully';
            } else if(eventRes.getStatusCode() == 404){
                msg = 'Event Deletion Failed: Item not found';
            } else{
                msg = 'Event Deletion Failed';
            }
            system.debug('msg: ' + msg);
        } catch(exception e){
            System.debug('Exception: ' + e);
        }
    }

    /************************************************************************************************
    * @Description : Fetches the Calendar Event details using Graph API
    * @Params      : String eventId - Microsoft Event Id of the Calendar event to be fetched
    * @Return      : None
    ************************************************************************************************/
    public static void getEventDetails(String eventId){
        String token = fetchAccessToken();
        String msg = '';
        Http http = new Http();
        HttpRequest eventReq = new HttpRequest();
        eventReq.setHeader('Authorization', 'Bearer ' + token);
        String endPoint = apiValue.Event_Creation_URL__c;
        eventReq.setEndpoint(endPoint + '/' + eventId);
        eventReq.setMethod('GET');
        eventReq.setHeader('Accept', 'application/json');
        HttpResponse eventRes = new HttpResponse();
        try{
            String responseBody = '';
            if(!Test.isRunningTest()){
                eventRes = http.send(eventReq);
                responseBody = eventRes.getBody();
            } else{
                responseBody = '';
            }
            if(eventRes.getStatusCode() == 204 || Test.isRunningTest()){
                VirtualHandover_OutlookEvents_JSON eventDetails = VirtualHandover_OutlookEvents_JSON.parse(responseBody);
                system.debug('eventDetails: ' + eventDetails);
                msg = 'Event Fetched Successfully';    
            } else{
                msg = 'Event Fetch Failed';
            }
            system.debug('msg: ' + msg);
            system.debug('create meeting response body: ' + responseBody);
            
        } catch(exception e){
            System.debug('Exception: ' + e);
        }  
    }

    
}