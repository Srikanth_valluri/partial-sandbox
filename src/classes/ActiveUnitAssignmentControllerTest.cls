@isTest
public class ActiveUnitAssignmentControllerTest {
     @isTest static void activeUnitAssignment(){
         Unit_Assignment__c uAObj = new Unit_Assignment__c();
         uAObj.Start_Date__c = Date.parse('11/12/17');
         uAObj.End_Date__c =  Date.parse('11/12/18');
         uAObj.Unit_Assignment_Name__c = 'Test US1';
         uAObj.Reason_For_Unit_Assignment__c = 'Test reason';
         uAObj.Active__c = false;
         insert uAObj;

         List<Inventory__c> inventoriesList = new  List<Inventory__c>();
         for(Integer i=0;i<=1;i++){
             Inventory__c inventory = InitialiseTestData.getInventoryDetails('345'+i,'123'+i,'234'+i,908+i,765+i); 
             inventory.Unit_Assignment__c = uAObj.id;
             inventoriesList.add(inventory);
         }
         insert inventoriesList ;
         
         Inventory_User__c invUser = new Inventory_User__c();
         invUser.User__c = userinfo.getuserId();
         invUser.Inventory__c = inventoriesList[0].id;
         invUser.Unit_Assignment__c = uAObj.id;
         insert invUser;
         
         Test.startTest ();
             ApexPages.StandardController sc = new ApexPages.standardController(uAObj);
             ActiveUnitAssignmentController activeUAObj = new ActiveUnitAssignmentController(sc);
             activeUAObj.buttonVal = 'deactivate';
             activeUAObj.isActive ='1';
             activeUAObj.checkOnLoad();
         Test.stopTest ();

    }
}