@istest
public class AssignedPcTriggerHandlerTest {
	private static List<User> userList = new List<User>();	
    static testmethod void TestMethods(){
        system.runAs(new User(Id = UserInfo.getUserId())){
        	userList = TestDataFactory.createTestUserRecords(new List<User>{new User()});	
        }
        
        Id RSRecordTypeId = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows').getRecordTypeId();
        Campaign__c camp = new Campaign__c();
        camp.RecordTypeId = RSRecordTypeId;
        camp.Campaign_Name__c='Test Campaign';
        camp.start_date__c = System.today();
        camp.end_date__c = System.Today().addDays(30);
        camp.Marketing_start_date__c = System.today();
        camp.Marketing_end_date__c = System.Today().addDays(30);
        camp.Language__c = 'English';
        insert camp;
        
        List<Inventory__c> inventoryList = new List<Inventory__c>{new Inventory__c(), new Inventory__c()};        
        insert inventoryList;
        
        Assigned_Agent__c aa = new Assigned_Agent__c();
        aa.Campaign__c = camp.id;
        insert aa;
        
        List<Campaign_Inventory__c> campaignInventoryList = 
        	new List<Campaign_Inventory__c>{
        		new Campaign_Inventory__c(campaign__c = camp.Id, Inventory__c = inventoryList[0].Id), 
        		new Campaign_Inventory__c(campaign__c = camp.Id, Inventory__c = inventoryList[1].Id)};
        insert campaignInventoryList;
        
        List<Assigned_PC__c> assignedPcs = new List<Assigned_PC__c>();
        Assigned_PC__c assObj = new Assigned_PC__c();
        assObj.Campaign__c = camp.id;
        assObj.User__c = userList[0].Id;
        assObj.End_Date__c=date.parse('13/11/16');
        assObj.Start_Date__c=date.parse('13/11/18');
        assignedPcs.add(assObj);
        insert assignedPcs;
        AssignedPcTriggerHandler cithObject = new AssignedPcTriggerHandler();
        AssignedPcTriggerHandler.isRecursion=false;
        cithObject.deleteInventoryUsers(assignedPcs);
        Map<Id, Assigned_PC__c> deletedRecords= new Map<Id, Assigned_PC__c>();
        for( Assigned_PC__c assignedObj :[SELECT id, Campaign__c,User__c 
                                          FROM  Assigned_PC__c])
        {
            deletedRecords.put(assignedObj.Id,assignedObj);
        }
        cithObject.revokeAccess(deletedRecords); 
        cithObject.executeBeforeUpdateTrigger(deletedRecords, deletedRecords);
        delete assignedPcs;
        delete campaignInventoryList; 
        cithObject.executeBeforeInsertUpdateTrigger(new List<sObject>(), new Map<Id,sObject>());
        cithObject.executeAfterInsertUpdateTrigger(new Map<Id,sObject>(), new Map<Id,sObject>());        
    }
}