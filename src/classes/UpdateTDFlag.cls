/****************************************************************************************
Description: Class to update TD flag when case is closed or TD flag is marked on 
             Booking Unit.
----------------------------------------------------------------------------------------
Version     | Date(DD-MM-YYYY)  | Last Modified By   |   Comments     
----------------------------------------------------------------------------------------                           
1.0         |   17/9/2020       |  Ruchika Choudhary |  Initial Draft
*****************************************************************************************/

global with sharing class UpdateTDFlag{
    
    static string app_jsonString = 'application/json';
    
    /*****************************************************************************************************************
     * Description  : Method to call service method 
     * Parameter(s) : String
     * Return       : void
     *****************************************************************************************************************/
    global static void updateTD(String regId){
        String attr = 'Y';
        if (!String.isBlank(regId)) {
             sendHttpRequest(regId, attr);
        }
    }
    
    
    /*****************************************************************************************************************
     * Description  : Method called from Process Builder. 
     * Parameter(s) : List<Id>
     * Return       : void
     **************************************************************************************************************** */ 

    @InvocableMethod
    public static void sendDetailsFromPB(List<Id> listCaseIDs){
        if(listCaseIDs != null && !listCaseIDs.isEmpty() ) {
            List<Case> listParentCase = [ 
                                        SELECT 
                                             Id
                                            ,Subvention_Amount__c
                                            ,Registration_Id__c
                                        FROM 
                                            Case
                                        WHERE 
                                            ID IN :listCaseIDs
                                    ];
            if (listParentCase != null && !listParentCase.isEmpty()) {
                if (listParentCase[0].Registration_Id__c != null) {
                    updateTD(listParentCase[0].Registration_Id__c);
                }
            }
        }
    }
    
    /*****************************************************************************************************************
     * Description  : Method to send Htp Request to IPMS 
     * Parameter(s) : String , String 
     * Return       : void
     **************************************************************************************************************** */ 
    @future (Callout = true)
    global static void sendHttpRequest(String paramId, String attribute1 ){
        Credentials_Details__c creds = getCredentials();
        String headerValue;
        if (creds != null ) {
            if(creds.User_Name__c != null && creds.Password__c != null) {
                String userName = creds.User_Name__c;
                String password = creds.Password__c; 
                headerValue = 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(userName + ':' + password));
            }
            if (creds.Endpoint__c != null && headerValue != null && !String.isEmpty(headerValue)) {
             try {
                Http http = new Http();
                HttpResponse response = new HttpResponse();
                HttpRequest request = new HttpRequest();
                request.setEndpoint(creds.Endpoint__c);
                request.setMethod('POST');
                request.setHeader('Accept', app_jsonString );
                request.setHeader('Content-Type', app_jsonString );
                request.setHeader('Accept-Language', 'en-US');
                request.setHeader('Authorization', headerValue);
                request.setBody('{' +
                          '"PROCESS_Input": {' +
                            '"RESTHeader": {' +
                              '"Responsibility": "ONT_ICP_SUPER_USER",' +
                              '"RespApplication": "ONT",' +
                              '"SecurityGroup": "STANDARD",' +
                              '"NLSLanguage": "AMERICAN"' +
                            '},' +
                            '"InputParameters": {' +
                              '"P_REQUEST_NUMBER": "'+paramId+'",' +
                              '"P_SOURCE_SYSTEM": "SFDC",' +
                              '"P_REQUEST_NAME": "UPDATE_TD_FLAG",' +
                              '"P_REQUEST_MESSAGE": {'+
                                '"P_REQUEST_MESSAGE_ITEM": ['+
                                  '{' +
                                    '"PARAM_ID": "'+ paramId +'",'+
                                    '"ATTRIBUTE1": "'+attribute1+'"'+
                                  '}' +
                                ']' +
                              '}' +
                            '}'+
                          '}'+
                        '}'
               );
                response = http.send(request);
                System.debug('Req Body -----' + request.getBody()); //important
                System.debug('Response -----' + response.getBody()); //important
                System.debug('Response -----' + response.getStatusCode()); //important
            } catch (Exception e) {
                System.debug('Exception --- ' + e); //important
            }
        }
        }
    }
    
    /*****************************************************************************************************************
     * Description  : Method to fetch IPMS creds and endpoint from
     *                custom setting 
     * Parameter(s) : void
     * Return       : void
     **************************************************************************************************************** */ 
     
    Public Static Credentials_Details__c getCredentials() {
    
       List<Credentials_Details__c> lstCreds = [ SELECT
                                                        Id
                                                        , Name
                                                        , User_Name__c
                                                        , Password__c
                                                        , Endpoint__c
                                                    FROM
                                                        Credentials_Details__c
                                                    WHERE
                                                        Name = :System.Label.Title_Deed_Label ];
        if( lstCreds != null && lstCreds.size() > 0 ) {
            return lstCreds[0];
        } else {
            return null;
        }
    }
}