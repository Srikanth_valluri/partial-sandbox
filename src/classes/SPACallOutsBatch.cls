/**************************************************************************************************
* Name               : SPACallOutsBatch                                                                *
* Description        : Batch class to make SPA Callouts asynchronously                            *
* Created Date       : June/2017                                                                  *
* Created By         : PwC ME                                                                     *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE            COMMENTS                                          *
* 1.0         Venkata Subhash  June/2017       Initial Draft.                                    *
**************************************************************************************************/


global class SPACallOutsBatch implements Database.Batchable<sObject>{

   global final String srId;
   global final String Query;

   global SPACallOutsBatch(String srid){   
       srId = String.valueOf(srId).substring(0, 15);    
       Query =  'select id from Booking_unit__c where SR_Id__c=\'' + srid + '\'';
       system.debug(Query+'>>>>>>>>');          
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC,List<sObject> scope){
      for(Sobject s : scope){
          list<id> buid = new list<id>();
          buid.add(s.id);
          if(Limits.getQueueableJobs() < Limits.getLimitQueueableJobs()){          
              system.enqueueJob(new AsyncReceiptWebservice (buid,'SPA'));          
          }
      }    
      
   }

   global void finish(Database.BatchableContext BC){

   }

}