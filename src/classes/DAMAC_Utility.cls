/************************************************************************************************
 * @Name              : DAMAC_Utility
 * @Test Class Name   : 
 * @Description       : Snippets for DAMAC
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         Charan Vuyyuru 14/07/2020       Created
 ***********************************************************************************************/

global class DAMAC_Utility{
    
    //Trim the parameters in webservice request
    public static string trimData(string data){
        return data.replaceAll('"','').trim();
    }
    /*
    //Query Constructor
    public static string queryConstructor(map<string,string> fieldApiValue,list<string> excludeKeys){
        string mainQuery = '';
        for(string fAPi : fieldApiValue.keySet()){
            if(!excludeKeys.contains(fAPi))
                mainQuery = mainQuery+ ' and '+fAPi + ' = \'' + fieldApiValue.get(fAPi)+ '\'';
        }
        return mainQuery;
    }
    */
    
    //readFieldSet
    public static list<string> readFieldSet(String fieldSetName, String ObjectName){
        list<string> FieldApiNames = new list<string>();
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
       
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        for(Schema.FieldSetMember f : fieldSetObj.getFields()) {
            FieldApiNames.add(f.getFieldPath());
        }
        return FieldApiNames;
    } 
    
    /*
    //Get PickListValues
    public static List<String> getPickListValues(string objectName,string fldAPIName){
        List<String> entries = new List<String>();
        Schema.DescribeFieldResult fieldResult = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap().get(fldAPIName).getDescribe();
        List<Schema.PicklistEntry> picklistEntries = fieldResult.getPicklistValues();
        for (Integer i = 0; i < picklistEntries.size(); i++){           
            Schema.PicklistEntry entry = picklistEntries[i];
            entries.add(entry.value);
        }  
        return entries;
    }
    */

    public static HTTPResponse makeCallout(string endpoint, string method, string body){
        HTTPRequest req = new HTTPRequest();
        req.setEndpoint(endpoint);
        req.setMethod(method);
        req.setBody(body);
        HTTP http = new HTTP();
        HTTPResponse res = new HTTPResponse();
        if(!test.isRunningTest())
            res = http.send(req);
        return res;
    }
}