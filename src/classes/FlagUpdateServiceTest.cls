/***********************************************************************
* Description - Test class developed for FlagUpdateService
*
* Version            Date            Author        Description
* 1.0                30/11/17        Arjun         Initial Draft
**********************************************************************/
@isTest
private class FlagUpdateServiceTest
{
  @isTest static void testUpdateAddressVerifiedFlag() {
    Test.startTest();
    
    FlagUpdateService.CallingListFlagHttpSoap11Endpoint calloutObj = new FlagUpdateService.CallingListFlagHttpSoap11Endpoint ();
    
    Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockCallingList());

    List<FlagUpProcessService.APPSXXDC_PROCESS_SERX1794747X1X5> regTerms = new List<FlagUpProcessService.APPSXXDC_PROCESS_SERX1794747X1X5>();
    FlagUpProcessService.APPSXXDC_PROCESS_SERX1794747X1X5 objTerms = new FlagUpProcessService.APPSXXDC_PROCESS_SERX1794747X1X5();
      objTerms.ATTRIBUTE1   = 'Y';    
      objTerms.ATTRIBUTE10 = '';
      objTerms.ATTRIBUTE11  = '';
      objTerms.ATTRIBUTE12 = '';
      objTerms.ATTRIBUTE13 = '';
      objTerms.ATTRIBUTE14 = '';
      objTerms.ATTRIBUTE15 = '';
      objTerms.ATTRIBUTE16 = '';
      objTerms.ATTRIBUTE17 = '';
      objTerms.ATTRIBUTE18 = '';
      objTerms.ATTRIBUTE19 = '';
      objTerms.ATTRIBUTE2   = '';
      objTerms.ATTRIBUTE20 = '';
      objTerms.ATTRIBUTE21 = '';
      objTerms.ATTRIBUTE22 = '';
      objTerms.ATTRIBUTE23 = '';
      objTerms.ATTRIBUTE24 = '';
      objTerms.ATTRIBUTE25 = '';
      objTerms.ATTRIBUTE26 = '';
      objTerms.ATTRIBUTE27 = '';
      objTerms.ATTRIBUTE28 = '';
      objTerms.ATTRIBUTE29 = '';
      objTerms.ATTRIBUTE3   = '';
      objTerms.ATTRIBUTE30 = '';
      objTerms.ATTRIBUTE31 = '';
      objTerms.ATTRIBUTE32 = '';
      objTerms.ATTRIBUTE33 = '';
      objTerms.ATTRIBUTE34 = '';
      objTerms.ATTRIBUTE35 = '';
      objTerms.ATTRIBUTE36 = '';
      objTerms.ATTRIBUTE37 = '';
      objTerms.ATTRIBUTE38 = '';
      objTerms.ATTRIBUTE39 = '';
      objTerms.ATTRIBUTE4   = '';
      objTerms.ATTRIBUTE41 = '';
      objTerms.ATTRIBUTE42 = '';
      objTerms.ATTRIBUTE43 = '';
      objTerms.ATTRIBUTE44 = '';
      objTerms.ATTRIBUTE45 = '';
      objTerms.ATTRIBUTE46 = '';
      objTerms.ATTRIBUTE47 = '';
      objTerms.ATTRIBUTE48 = '';
      objTerms.ATTRIBUTE49 = '';
      objTerms.ATTRIBUTE5   = '';
      objTerms.ATTRIBUTE50 = '';
      objTerms.ATTRIBUTE6   = '';
      objTerms.ATTRIBUTE7   = '';
      objTerms.ATTRIBUTE8   = '';
      objTerms.ATTRIBUTE9   = '';
      objTerms.PARAM_ID   = '12851';
      regTerms.add(objTerms);
      system.debug( 'regTerms' + regTerms);
      
      String response = calloutObj.UpdateAddressVerifiedFlag( '2-'+String.valueOf( Datetime.now().getTime())
                                  , 'UPDATE_ADDRESS_VERIFIED' ,'SFDC' ,regTerms);
    
    Test.stopTest();
  }
  @isTest static void testUpdateRecoveryFlag() {
    Test.startTest();
    
    FlagUpdateService.CallingListFlagHttpSoap11Endpoint calloutObj = new FlagUpdateService.CallingListFlagHttpSoap11Endpoint ();
    
    Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMockCallingList());

    List<FlagUpProcessService.APPSXXDC_PROCESS_SERX1794747X1X5> regTerms = new List<FlagUpProcessService.APPSXXDC_PROCESS_SERX1794747X1X5>();
    FlagUpProcessService.APPSXXDC_PROCESS_SERX1794747X1X5 objTerms = new FlagUpProcessService.APPSXXDC_PROCESS_SERX1794747X1X5();
      objTerms.ATTRIBUTE1   = 'Y';    
      objTerms.ATTRIBUTE10 = '';
      objTerms.ATTRIBUTE11  = '';
      objTerms.ATTRIBUTE12 = '';
      objTerms.ATTRIBUTE13 = '';
      objTerms.ATTRIBUTE14 = '';
      objTerms.ATTRIBUTE15 = '';
      objTerms.ATTRIBUTE16 = '';
      objTerms.ATTRIBUTE17 = '';
      objTerms.ATTRIBUTE18 = '';
      objTerms.ATTRIBUTE19 = '';
      objTerms.ATTRIBUTE2   = '';
      objTerms.ATTRIBUTE20 = '';
      objTerms.ATTRIBUTE21 = '';
      objTerms.ATTRIBUTE22 = '';
      objTerms.ATTRIBUTE23 = '';
      objTerms.ATTRIBUTE24 = '';
      objTerms.ATTRIBUTE25 = '';
      objTerms.ATTRIBUTE26 = '';
      objTerms.ATTRIBUTE27 = '';
      objTerms.ATTRIBUTE28 = '';
      objTerms.ATTRIBUTE29 = '';
      objTerms.ATTRIBUTE3   = '';
      objTerms.ATTRIBUTE30 = '';
      objTerms.ATTRIBUTE31 = '';
      objTerms.ATTRIBUTE32 = '';
      objTerms.ATTRIBUTE33 = '';
      objTerms.ATTRIBUTE34 = '';
      objTerms.ATTRIBUTE35 = '';
      objTerms.ATTRIBUTE36 = '';
      objTerms.ATTRIBUTE37 = '';
      objTerms.ATTRIBUTE38 = '';
      objTerms.ATTRIBUTE39 = '';
      objTerms.ATTRIBUTE4   = '';
      objTerms.ATTRIBUTE41 = '';
      objTerms.ATTRIBUTE42 = '';
      objTerms.ATTRIBUTE43 = '';
      objTerms.ATTRIBUTE44 = '';
      objTerms.ATTRIBUTE45 = '';
      objTerms.ATTRIBUTE46 = '';
      objTerms.ATTRIBUTE47 = '';
      objTerms.ATTRIBUTE48 = '';
      objTerms.ATTRIBUTE49 = '';
      objTerms.ATTRIBUTE5   = '';
      objTerms.ATTRIBUTE50 = '';
      objTerms.ATTRIBUTE6   = '';
      objTerms.ATTRIBUTE7   = '';
      objTerms.ATTRIBUTE8   = '';
      objTerms.ATTRIBUTE9   = '';
      objTerms.PARAM_ID   = '12851';
      regTerms.add(objTerms);
      system.debug( 'regTerms' + regTerms);
      
      String response = calloutObj.UpdateRecoveryFlag( '2-'+String.valueOf( Datetime.now().getTime())
                                  , 'UPDATE_RECOVERY_FLAG' ,'SFDC' ,regTerms);
    
    Test.stopTest();
  }  
}