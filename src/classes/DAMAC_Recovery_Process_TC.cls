@isTest
public class DAMAC_Recovery_Process_TC {
    static testmethod void myUnitTest() {
        ID recordTypeId = Schema.SObjectType.inventory__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        ID AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        ID InquiryRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry Decrypted').getRecordTypeId();
        NSIBPM__Service_Request__c serviceReq = New NSIBPM__Service_Request__c ();
        serviceReq.Generate_Credit_Letter__c = true;
        serviceReq.Generate_side_Letter__c = true;
        serviceReq.dp_ok__c = true;
        serviceReq.doc_ok__c = true;
        insert serviceReq;
        Booking__c booking = New Booking__c ();
        booking.Deal_SR__c = serviceReq.id;
        insert booking;
        
        List <Id> bookingId = New List <Id> ();
        bookingId.add(booking.id);
        Location__c loc=new Location__c();
        loc.Location_ID__c='123';
        loc.Name = 'test';
        insert loc;
        Inventory__c inv2 = new Inventory__c();
        inv2.Unit_Location__c=loc.id;
        insert inv2;
        Booking_unit__c bu = new Booking_unit__c ();
        bu.Inventory__c = inv2.id;
        bu.Booking__c = booking.id;
        insert bu;
        
        inventory__c inv1 = new inventory__c ();
        inv1.RecordtypeId = recordTypeId;
        inv1.Status__c = 'Booked';
        //inv1.unit_name__c = 'test';
        inv1.CurrencyIsoCode = 'EUR';
        insert inv1;
        List<booking_unit__c> bookingUnitList = New List <booking_unit__c> (); 
        Booking_Unit__c bookngUnit = new Booking_Unit__c ();
        bookngUnit.Booking__c = booking.id;            
        bookngUnit.Inventory__c = inv1.ID;
        bookngUnit.Total_Collections_NEW__c = 455;
        bookngUnit.Total_Collections_NEW__c = 455;
        bookngUnit.requested_price_aed__c = 23;
        bookngUnit.Allocated_Amount__c = 1123;
        bookngUnit.Amount_to_Transfer__c = String.ValueOf(133);
        bookngUnit.Recovery_type__c = 'Entry';
        bookngUnit.Registration_Status_Code__c = 'AC';
        bookngUnit.Credit_Amount__c = 43543;
        bookngUnit.Recovery_Flag__c ='R';
        bookngUnit.New_Booking__c = false;
        bookngUnit.Recovery_Service_Request__c = serviceReq.id;
        
        insert bookngUnit;
        
        Booking_Unit__c bookngUnit1 = new Booking_Unit__c ();
        bookngUnit1.Booking__c = booking.id; 
        bookngUnit1.Inventory__c = inv1.ID;
        bookngUnit1.Total_Collections_NEW__c = 424;
        bookngUnit1.requested_price_aed__c = 13;
        bookngUnit1.Allocated_Amount__c = 113;
        bookngUnit1.Credit_Amount__c = 43543;
        bookngUnit1.Amount_to_Transfer__c = String.valueOf(133);
        bookngUnit1.Recovery_type__c= 'Exit';
        bookngUnit1.Registration_Status_Code__c = 'AC';
        bookngUnit1.Recovery_Flag__c ='R';
        bookngUnit1.Recovery_Service_Request__c = serviceReq.id;
        
        insert bookngUnit1;
        bookingUnitList.add(bookngUnit);
        bookingUnitList.add(bookngUnit1);
        
        
        system.debug('BookingUnit----'+bookngUnit);
        system.debug('BookingUnit1----'+bookngUnit1);
        
        //added by Mariam CloudzLab
        Inquiry__c inquiryRecord = new Inquiry__c();
        inquiryRecord.By_Pass_Validation__c = true;
        inquiryRecord.Party_ID__c = '12345';
        inquiryRecord.Title__c = 'MR.';
        inquiryRecord.Title_Arabic__c = 'MR.';
        inquiryRecord.First_Name__c = 'Test';
        inquiryRecord.First_Name_Arabic__c = 'Test';
        inquiryRecord.Last_Name__c = 'Test';
        inquiryRecord.Last_Name_Arabic__c = 'Test';
        insert inquiryRecord;
        
        Buyer__c buyr = New Buyer__c();
        buyr.Inquiry__c = inquiryRecord.Id;
        buyr.Booking__c = booking.id;
        buyr.Primary_Buyer__c = true;
        buyr.passport_expiry__c = system.today().addDays(29);
        buyr.DOB__c = system.today();
        buyr.Address_Line_1__c = 'acd1235';
        buyr.city__c ='';
        buyr.Phone__c ='';
        
        insert buyr;
        Buyer__c buyr1 = New Buyer__c();
        buyr1.Inquiry__c = inquiryRecord.Id;
        buyr1.Booking__c = booking.id;
        buyr1.Primary_Buyer__c = false;
        buyr1.passport_expiry__c = system.today().addDays(29);
        buyr1.DOB__c = system.today();
        buyr1.Address_Line_1__c = 'acd1235';
        buyr1.city__c ='';
        buyr1.Phone__c ='';
        
        insert buyr1;
        Inquiry__c inq = New Inquiry__c ();
        inq.First_Name__c = 'Test';
        inq.Last_Name__c = 'Test';
        inq.Inquiry_Status__c = 'Meeting Scheduled';
        inq.RecordTypeId = InquiryRecordTypeId;
        inq.Preferred_Language__c = 'English';
        inq.Inquiry_Source__c = 'Chat';
        inq.Primary_Contacts__c = 'Mobile Phone';
        inq.Mobile_CountryCode__c = 'India: 0091';
        inq.Mobile_Phone_Encrypt__c = '100110101';
        inq.Mobile_Phone__c = '9876543210';
        inq.Meeting_Type__c = 'Meeting On Stand';
        inq.Assigned_PC__c = userinfo.getuserid();
        inq.Agent_Email__c = 'test@gmail.com';
        //insert inq; 
        
        Set <String> bookingUnitIds = New Set<String> ();
        bookingUnitIds.add (bookngUnit.id);
        bookingUnitIds.add (bookngUnit1.id);
        List<Id> bookingIdList = new List<Id> ();
        bookingIdList.add(booking.id);
        Account acc = New Account ();
        acc.LastName = 'Test';
        acc.FirstName = 'Test';
        acc.RecordTypeId = AccountRecordTypeId;
        insert acc;
        inventory__c inv = new inventory__c ();
        inv.RecordtypeId = recordTypeId;
        // inv.unit_name__c = 'test';
        inv.CurrencyIsoCode = 'EUR';
        inv.status__c = 'Released';
        insert inv;
        Payment_Plan__c payPlan = New Payment_Plan__c ();
        payPlan.Effective_From__c =system.today().addDays(5);
        payPlan.booking_unit__c = bookngUnit.id;
        payPlan.Effective_To__c = system.today();
        // payPlan.parent_payment_plan__c = '';
        //payPlan.Effective_To_calculated__c = system.today();
        insert payPlan;
        bookngUnit.Payment_plan_ID__c = payPlan.Id;
        update bookngUnit;
        Payment_Terms__c payTerms = New Payment_Terms__c();
        payTerms.CurrencyIsoCode = 'EUR';
        payTerms.Payment_Plan__c = payPlan.id;
        payTerms.Percent_Value__c = '30';
        payTerms.Booking_Unit__c =  bookngUnit.id;
        payTerms.Installment__c = 'Installment1';
        
        insert payTerms;
        
        Set <String> invIds = new Set<String> ();
        invIds.add(inv.id);
        invIds.add(inv1.id);
        
        List<inventory__c> invList = New List<inventory__c> ();
        invList.add(inv);
        invList.add(inv1);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(serviceReq);
        ApexPages.currentPage().getParameters().put('id', serviceReq.id);
        ApexPages.currentPage().getParameters().put('buyerType', 'Primary');
        ApexPages.currentPage().getParameters().put('selectedBookingID', 'All');
        
        Test.startTest ();
        DAMAC_Recovery_Process CancelAndTransfer = new DAMAC_Recovery_Process(sc);
        
        CancelAndTransfer.multipleExitUnits = true;
        CancelAndTransfer.exitUnitString = bookngUnit.id+','+bookngUnit1.id;
        CancelAndTransfer.entryUnitString = inv.id+','+inv1.id;
        DAMAC_Recovery_Process  cancel = new DAMAC_Recovery_Process ();
        CancelAndTransfer.exitSearchkey = 'test';
        CancelAndTransfer.exitBookingUnits = new List <booking_unit__c> {bookingUnitList[0]};
            CancelAndTransfer.tempPaymentTerms = new List<Payment_Terms__c> {payTerms};
                CancelAndTransfer.loadDataSheet();
        CancelAndTransfer.searchExitUnits();
        CancelAndTransfer.entrySearchkey = 'test';
        CancelAndTransfer.searchEntryUnits();
        CancelAndTransfer.selectedTypeofCandT = 'Upgrade';
        CancelAndTransfer.validateToProceed();
        CancelAndTransfer.selectedexitUnits= bookingUnitList;
        CancelAndTransfer.queryExitUnits(bookingUnitIds);       
        CancelAndTransfer.holdSelectedunits();
        CancelAndTransfer.queryEntryUnits(invIds);
        CancelAndTransfer.prepareStructure(bookingUnitList,invList);
        CancelAndTransfer.saveDataSheet();
        CancelAndTransfer.getTypesofCandT();
        CancelAndTransfer.selectedPrimaryBuyer = buyr.id;
        
        DAMAC_Recovery_Process.getAccountDetails('Test','Individual');
        DAMAC_Recovery_Process.checkForAccount('Indian','ae66606');
        DAMAC_Recovery_Process.getInquiryDetails('Test');
        DAMAC_Recovery_Process.mapInquiryDetailsToBuyer(inq.id,'Account__c');
        DAMAC_Recovery_Process.validateDateOfBirth(system.today());
        DAMAC_Recovery_Process.getBuyerDetails (buyr.id);
        DAMAC_Recovery_Process.getCityValues('India');
        CancelAndTransfer.checkForBuyers();
        // CancelAndTransfer.saveDataSheet();
        
        CancelAndTransfer.getPaymentPlan(payPlan.id);
        //CancelAndTransfer.saveDataSheet();
        CancelAndTransfer.addNewPaymentTerm ();
        CancelAndTransfer.PaymentPlanID = payPlan.id;
        CancelAndTransfer.updatePaymentTerms();
        ApexPages.currentPage().getParameters().put('selectedId', payPlan.id);
        CancelAndTransfer.getPaymentTerms();
        
        CancelAndTransfer.loadPaymentPlansForUnits();
        CancelAndTransfer.MarkBuyers();
        CancelAndTransfer.validateBuyerInformation(buyr,true);
        CancelAndTransfer.clearPageMessages();
        ApexPages.currentPage().getParameters().put('selectedBookingID', booking.id);
        CancelAndTransfer.SavePrimaryBuyer();
        CancelAndTransfer.jointBuyerOftheBooking();
        CancelAndTransfer.getDLDpaymentOptions();
        CancelAndTransfer.getPrimaryBuyers();
        CancelAndTransfer.checkLength ('test');
        CancelAndTransfer.formatDate (Date.Today ());
        CancelAndTransfer.primaryBuyersUnderBookings (bookingId);
        cancelAndTransfer.createDealTeam (serviceReq, UserInfo.getUserID ());
        DAMAC_Recovery_Process.getAllFields('Account');
        
        
        Test.stopTest ();
    }
    
    static testmethod void myUnitTest1() {
        ID AccountRecordTypeId1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        ID InquiryRecordTypeId1 = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry Decrypted').getRecordTypeId();
        ID recordTypeId = Schema.SObjectType.inventory__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();      
        ID InquiryRecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry Decrypted').getRecordTypeId();
        NSIBPM__Service_Request__c serviceReq = New NSIBPM__Service_Request__c ();
        serviceReq.Generate_Credit_Letter__c = true;
        serviceReq.Generate_side_Letter__c = true;
        serviceReq.dp_ok__c = true;
        serviceReq.doc_ok__c = true;
        insert serviceReq;
        Booking__c booking = New Booking__c ();
        booking.Deal_SR__c = serviceReq.id;
        insert booking;
        
        List <Id> bookingId = New List <Id> ();
        bookingId.add(booking.id);
        Location__c loc=new Location__c();
        loc.Location_ID__c='123';
        loc.Name = 'test';
        insert loc;
        Inventory__c inv2 = new Inventory__c();
        inv2.Unit_Location__c=loc.id;
        insert inv2;
        Booking_unit__c bu = new Booking_unit__c ();
        bu.Inventory__c = inv2.id;
        bu.Booking__c = booking.id;
        insert bu;
        
        inventory__c inv1 = new inventory__c ();
        inv1.RecordtypeId = recordTypeId;
        inv1.Status__c = 'Booked';
        //inv1.unit_name__c = 'test';
        inv1.CurrencyIsoCode = 'EUR';
        insert inv1;
        List<booking_unit__c> bookingUnitList = New List <booking_unit__c> (); 
        Booking_Unit__c bookngUnit = new Booking_Unit__c ();
        bookngUnit.Booking__c = booking.id;            
        bookngUnit.Inventory__c = inv1.ID;
        bookngUnit.Total_Collections_NEW__c = 4552;
        bookngUnit.requested_price_aed__c = 23;
        bookngUnit.Allocated_Amount__c = 1123;
        bookngUnit.Amount_to_Transfer__c = String.ValueOf(133);
        bookngUnit.Recovery_type__c = 'Entry';
        bookngUnit.Registration_Status_Code__c = 'AC';
        bookngUnit.Credit_Amount__c = 43543;
        bookngUnit.Recovery_Flag__c ='R';
        bookngUnit.New_Booking__c = true;
        bookngUnit.Recovery_Service_Request__c = serviceReq.id;
        
        insert bookngUnit;
        
        Booking_Unit__c bookngUnit1 = new Booking_Unit__c ();
        bookngUnit1.Booking__c = booking.id; 
        bookngUnit1.Inventory__c = inv1.ID;
        bookngUnit1.Total_Collections_NEW__c = 754;
        bookngUnit1.requested_price_aed__c = 13;
        bookngUnit1.Allocated_Amount__c = 1123;
        bookngUnit1.Credit_Amount__c = 43543;
        bookngUnit1.Amount_to_Transfer__c = String.valueOf(133);
        bookngUnit1.Recovery_type__c= 'Exit';
        bookngUnit1.Registration_Status_Code__c = 'AC';
        bookngUnit1.Recovery_Flag__c ='R';
        bookngUnit1.Recovery_Service_Request__c = serviceReq.id;
        
        insert bookngUnit1;
        bookingUnitList.add(bookngUnit);
        bookingUnitList.add(bookngUnit1);
        
        
        system.debug('BookingUnit----'+bookngUnit);
        system.debug('BookingUnit1----'+bookngUnit1);
        
        //added by Mariam CloudzLab
        Inquiry__c inquiryRecord = new Inquiry__c();
        inquiryRecord.By_Pass_Validation__c = true;
        inquiryRecord.Party_ID__c = '12345';
        inquiryRecord.Title__c = 'MR.';
        inquiryRecord.Title_Arabic__c = 'MR.';
        inquiryRecord.First_Name__c = 'Test';
        inquiryRecord.First_Name_Arabic__c = 'Test';
        inquiryRecord.Last_Name__c = 'Test';
        inquiryRecord.Last_Name_Arabic__c = 'Test';
        insert inquiryRecord;
        
        
        Buyer__c buyr = New Buyer__c();
        buyr.Inquiry__c = inquiryRecord.Id;
        buyr.Booking__c = booking.id;
        buyr.Primary_Buyer__c = false;
        buyr.passport_expiry__c = system.today().addDays(29);
        buyr.DOB__c = system.today();
        buyr.Address_Line_1__c = 'acd1235';
        buyr.city__c ='';
        buyr.Phone__c ='';
        
        insert buyr;
        Buyer__c buyr1 = New Buyer__c();
        buyr1.Inquiry__c = inquiryRecord.Id;
        buyr1.Booking__c = booking.id;
        buyr1.Primary_Buyer__c = true;
        buyr1.passport_expiry__c = system.today().addDays(29);
        buyr1.DOB__c = system.today();
        buyr1.Address_Line_1__c = 'acd1235';
        buyr1.city__c ='';
        buyr1.Phone__c ='';
        
        insert buyr1;
        Inquiry__c inq = New Inquiry__c ();
        inq.First_Name__c = 'Test';
        inq.Last_Name__c = 'Test';
        inq.Inquiry_Status__c = 'Meeting Scheduled';
        inq.RecordTypeId = InquiryRecordTypeId1;
        inq.Preferred_Language__c = 'English';
        inq.Inquiry_Source__c = 'Chat';
        inq.Primary_Contacts__c = 'Mobile Phone';
        inq.Mobile_CountryCode__c = 'India: 0091';
        inq.Mobile_Phone_Encrypt__c = '100110101';
        inq.Mobile_Phone__c = '9876543210';
        inq.Meeting_Type__c = 'Meeting On Stand';
        inq.Assigned_PC__c = userinfo.getuserid();
        inq.Agent_Email__c = 'test@gmail.com';
        //insert inq; 
        
        Set <String> bookingUnitIds = New Set<String> ();
        bookingUnitIds.add (bookngUnit.id);
        bookingUnitIds.add (bookngUnit1.id);
        List<Id> bookingIdList = new List<Id> ();
        bookingIdList.add(booking.id);
        Account acc = New Account ();
        //acc.LastName = 'Test';
        acc.Name = 'Test';
        acc.RecordTypeId = AccountRecordTypeId1;
        insert acc;
        inventory__c inv = new inventory__c ();
        inv.RecordtypeId = recordTypeId;
        // inv.unit_name__c = 'test';
        inv.CurrencyIsoCode = 'EUR';
        inv.status__c = 'Released';
        insert inv;
        Payment_Plan__c payPlan = New Payment_Plan__c ();
        payPlan.Effective_From__c =system.today().addDays(5);
        payPlan.booking_unit__c = bookngUnit.id;
        payPlan.Effective_To__c = system.today();
        // payPlan.parent_payment_plan__c = '';
        //payPlan.Effective_To_calculated__c = system.today();
        insert payPlan;
        bookngUnit.Payment_plan_ID__c = payPlan.Id;
        update bookngUnit;
        Payment_Terms__c payTerms = New Payment_Terms__c();
        payTerms.CurrencyIsoCode = 'EUR';
        payTerms.Payment_Plan__c = payPlan.id;
        payTerms.Percent_Value__c = '30';
        payTerms.Booking_Unit__c =  bookngUnit.id;
        payTerms.Installment__c = 'Installment1';
        
        insert payTerms;
        
        Set <String> invIds = new Set<String> ();
        invIds.add(inv.id);
        invIds.add(inv1.id);
        
        List<inventory__c> invList = New List<inventory__c> ();
        invList.add(inv);
        invList.add(inv1);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(serviceReq);
        ApexPages.currentPage().getParameters().put('id', serviceReq.id);
        ApexPages.currentPage().getParameters().put('buyerType', 'Secondary');
        ApexPages.currentPage().getParameters().put('selectedBookingID', 'All');
        
        Test.startTest ();
        
        DAMAC_Recovery_Process CancelAndTransfer = new DAMAC_Recovery_Process(sc);
        
        CancelAndTransfer.multipleExitUnits = true;
        CancelAndTransfer.exitUnitString = bookngUnit.id+','+bookngUnit1.id;
        CancelAndTransfer.entryUnitString = inv.id+','+inv1.id;
        DAMAC_Recovery_Process  cancel = new DAMAC_Recovery_Process ();
        CancelAndTransfer.exitSearchkey = 'test';
        CancelAndTransfer.exitBookingUnits = new List <booking_unit__c> {bookingUnitList[0]};
            
            CancelAndTransfer.loadDataSheet();
        CancelAndTransfer.searchExitUnits();
        CancelAndTransfer.entrySearchkey = 'test';
        CancelAndTransfer.searchEntryUnits();
        CancelAndTransfer.selectedTypeofCandT = 'Upgrade';
        CancelAndTransfer.validateToProceed();
        CancelAndTransfer.selectedexitUnits= bookingUnitList;
        CancelAndTransfer.queryExitUnits(bookingUnitIds);       
        CancelAndTransfer.holdSelectedunits();
        CancelAndTransfer.queryEntryUnits(invIds);
        CancelAndTransfer.prepareStructure(bookingUnitList,invList);
        CancelAndTransfer.saveDataSheet();
        CancelAndTransfer.getTypesofCandT();
        CancelAndTransfer.selectedPrimaryBuyer = buyr.id;
        
        
        DAMAC_Recovery_Process.getAccountDetails('Test','Corporate');
        DAMAC_Recovery_Process.checkForAccount('Indian','ae66606');
        DAMAC_Recovery_Process.getInquiryDetails('Test');
        DAMAC_Recovery_Process.mapInquiryDetailsToBuyer(inq.id,'Inquiry__c');
        DAMAC_Recovery_Process.validateDateOfBirth(system.today());
        DAMAC_Recovery_Process.getBuyerDetails (buyr.id);
        DAMAC_Recovery_Process.getCityValues('India');
        CancelAndTransfer.checkForBuyers();
        //  CancelAndTransfer.saveDataSheet();
        CancelAndTransfer.getPaymentPlan(payPlan.id);
        
        CancelAndTransfer.addNewPaymentTerm ();
        CancelAndTransfer.PaymentPlanID = payPlan.id;
        CancelAndTransfer.tempPaymentTerms = new List<Payment_Terms__c> {payTerms};
            CancelAndTransfer.updatePaymentTerms();
        ApexPages.currentPage().getParameters().put('selectedId', payPlan.id);
        
        CancelAndTransfer.getPaymentTerms();
        
        CancelAndTransfer.loadPaymentPlansForUnits();
        CancelAndTransfer.MarkBuyers();
        CancelAndTransfer.validateBuyerInformation(buyr,true);
        CancelAndTransfer.clearPageMessages();
        ApexPages.currentPage().getParameters().put('SelectedBookingID', 'All');
        CancelAndTransfer.SavePrimaryBuyer();
        CancelAndTransfer.jointBuyerOftheBooking();
        CancelAndTransfer.getDLDpaymentOptions();
        CancelAndTransfer.getPrimaryBuyers();
        CancelAndTransfer.checkLength ('test');
        CancelAndTransfer.formatDate (Date.Today ());
        CancelAndTransfer.primaryBuyersUnderBookings (bookingId);
        cancelAndTransfer.createDealTeam (serviceReq, UserInfo.getUserID ());
        DAMAC_Recovery_Process.getAllFields('Account');
        
        
        Test.stopTest ();
        
    }
}