/*
* Description - Test class developed for 'NameAndNationalityService'
*
* Version            Date            Author            Description
* 1.0                17/11/17        Snehil            Initial Draft
*/

@isTest 
private class NameAndNationalityServiceTest { 
	
    static testMethod void testMethod1() {
        String resp ;   
        String P_REQUEST_NUMBER = 'Test';
		String P_REQUEST_NAME = 'Test';
		String P_SOURCE_SYSTEM = 'Test';
		String RegistrationId = 'Test';
		String Title = 'Test';
		String FirstName = 'Test';
		String MiddleName = 'Test';
		String LastName = 'Test';
		String Nationality = 'Test';
		String PassportNumber = 'Test';
		String PassportIssuePlace = 'Test';
		String PassportIssueDate = 'Test';
		String TitleAR = 'Test';
		String FirstNameAR = 'Test';
		String MiddleNameAR = 'Test';
		String LastNameAR = 'Test';
		String NationalityAR  = 'Test';  
    	
    	test.startTest();
    	SOAPCalloutServiceMock.returnToMe = new Map<String, NameAndNationalityService.NameAndNationalityProcessResponse_element>();
    	NameAndNationalityService.NameAndNationalityProcessResponse_element response = new NameAndNationalityService.NameAndNationalityProcessResponse_element();
		response.return_x = 'S~Account Succesfully upated.';
		SOAPCalloutServiceMock.returnToMe.put('response_x', response);
		//Test.setMock( SOAPCalloutServiceMock.class, new SOAPCalloutServiceMock() );
        Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
		NameAndNationalityService objNameAndNationalityService = new NameAndNationalityService();
    	NameAndNationalityService.NameAndNationalityHttpSoap11Endpoint obj1 = new NameAndNationalityService.NameAndNationalityHttpSoap11Endpoint();

		resp = obj1.NameAndNationalityProcess(P_REQUEST_NUMBER,
											  P_REQUEST_NAME,
											  P_SOURCE_SYSTEM,
											  RegistrationId,
											  Title,
											  FirstName, 
											  MiddleName,
											  LastName,
											  Nationality,
											  PassportNumber,
											  PassportIssuePlace,
											  PassportIssueDate, 
											  TitleAR,
											  FirstNameAR,
											  MiddleNameAR,
											  LastNameAR,
											  NationalityAR);
		test.stopTest();
    }   
}