/*
* Description - Test class developed for GenerateStatementForCase
*
* Version            Date            Author            Description
* 1.0              21/11/2017      Ashish Agarwal     Initial Draft
*/


@isTest
private class GenerateStatementForCaseTest {

    static testMethod void testSOAGeneration() {
         // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        //Insert Cases for the units
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id; 
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        list<SR_Attachments__c> lstAttachment = new list<SR_Attachments__c>();
        for( Case objCase : lstCases ) {
            lstAttachment.add( TestDataFactory_CRM.createCaseDocument( objCase.Id, PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF ) );
        }
        insert lstAttachment ;
        
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateSOAService.GenCustomerStatementResponse_element>();
            GenerateSOAService.GenCustomerStatementResponse_element response = new GenerateSOAService.GenCustomerStatementResponse_element();
            response.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338397 and Request Id :40187735 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"92061","REQUEST_ID":"40187735","STAGE_ID":"338397","URL":"https://sftest.deeprootsurface.com/docs/e/40187735_92061_SOA.pdf"}';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	GenerateStatementForCase.generateStatementOfAccountforCase(lstBookingUnits[0].Registration_Id__c);
        test.stopTest();
    }
    
    static testMethod void testSOPGeneration() {
         // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        //Insert Cases for the units
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id; 
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        list<SR_Attachments__c> lstAttachment = new list<SR_Attachments__c>();
        for( Case objCase : lstCases ) {
            lstAttachment.add( TestDataFactory_CRM.createCaseDocument( objCase.Id, PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF ) );
        }
        insert lstAttachment ;
        
        test.startTest();
        	SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateSOPService.GeneratePenaltyStatmentResponse_element>();
            GenerateSOPService.GeneratePenaltyStatmentResponse_element response1 = new GenerateSOPService.GeneratePenaltyStatmentResponse_element();
            response1.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338396 and Request Id :40185767 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"78152","REQUEST_ID":"40185767","STAGE_ID":"338396","URL":"https://sftest.deeprootsurface.com/docs/e/40185767_78152_PENALTY.pdf"}';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
        	GenerateStatementForCase.generateStatementOfPenaltytforCase(lstBookingUnits[0].Registration_Id__c);
        test.stopTest();
        
    }
    
    static testMethod void testCaseUpdate() {
         // Insert Accont
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        //Insert Options
         List<Option__c> lstOptions = TestDataFactory_CRM.createOptions( lstBookingUnits );
         insert lstOptions ;
        
        //Insert Customer Setting Records
        List<Booking_Unit_Active_Status__c> lstActiveStatus = TestDataFactory_CRM.createActiveUnitCustomSetting( new list<String>{ 'Agreement executed by DAMAC' } );
        insert lstActiveStatus ;
        
        //Insert Cases for the units
        list<Case> lstCases = new list<Case>();
        Id recordTypeId = PenaltyWaiverUtility.getRecordTypeId( PenaltyWaiverUtility.PENALTY_WAIVER_RECTYPE, PenaltyWaiverUtility.CASE_SOBJ );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
            Case objCase = TestDataFactory_CRM.createCase( objAcc.Id, recordTypeId );
            objCase.Booking_Unit__c = objUnit.Id; 
            lstCases.add( objCase );
        }
        insert lstCases ;
        
        list<SR_Attachments__c> lstAttachment = new list<SR_Attachments__c>();
        for( Case objCase : lstCases ) {
            lstAttachment.add( TestDataFactory_CRM.createCaseDocument( objCase.Id, PenaltyWaiverUtility.ATTACH_TYPE_FULL_CRF ) );
        }
        insert lstAttachment ;
        
        test.startTest();
        	GenerateStatementForCase.cancelWaiverRequest(lstCases[0].Id);
        test.stopTest();
        
    }
}