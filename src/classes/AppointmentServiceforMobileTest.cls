/****************************************************************************************************
* Name          : AppointmentServiceforMobileTest                                                   *
* Description   : Test Class for AppointmentServiceforMobile                                        *
* Created Date  : 31-03-2019                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE        COMMENTS                                                    *
* 1.0                                 Initial Draft.                                                *
****************************************************************************************************/
@isTest
private class AppointmentServiceforMobileTest {
	static Calling_List__c objCalling;
	static Appointment__c objApp;
	@isTest
	static void itShould() {
		AppointmentServiceforMobile nation=new AppointmentServiceforMobile();
        AppointmentServiceforMobile.AppointmentWrapper appWrapObj =new AppointmentServiceforMobile.AppointmentWrapper();
		AppointmentServiceforMobile.selectionWrapper  selWrapObj =new AppointmentServiceforMobile.selectionWrapper(); 
        Account a=new Account();
        a.Name='test';     
        insert a;
		set<string> setAccountIds = new set<string>();
		setAccountIds.add(a.id);

        NSIBPM__Service_Request__c cs=new NSIBPM__Service_Request__c();
        insert cs;
         Booking__c bo=new Booking__c();
        bo.Account__c=a.id;
        bo.Deal_SR__c=cs.id;
        insert bo;
        
        Booking_Unit__c b=new Booking_Unit__c();
        b.Booking__c=bo.id;
        b.Unit_Name__c='BSB/14/1403';
        insert b;
        Location__c l=new Location__c ();
        l.Name='BSB';
        l.Location_ID__c='BSB';
        insert l;

		TriggerOnOffCustomSetting__c objSetting = new TriggerOnOffCustomSetting__c();
        objSetting.Name = 'CallingListTrigger';
        objSetting.OnOffCheck__c = false ;
        insert objSetting ;

		ID RecordTypeID = 
        Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Appointment Scheduling').getRecordTypeId();
                            
        objCalling = new Calling_list__c();                    
        objCalling.Appointment_Date__c =  System.today();
        objCalling.Account__c =  a.id; 
        objCalling.RecordTypeID  =  RecordTypeID ; 
        objCalling.Customer_Name__c = a.Name;               
        objCalling.Appointment_Status__c = 'Requested';
        objCalling.CRE_Email__c = 'abc@new.com';
        objCalling.Sub_Purpose__c = 'Key Handover';
        insert objCalling;

		objApp = new Appointment__c();
        objApp.Appointment_Date__c = date.newinstance( 2018, 12, 12 );
        objApp.Appointment_End_Date__c = date.newinstance( 2018, 12, 21 );
        objApp.Building__c = l.id;
        objApp.Processes__c = 'Handover';
        objApp.Sub_Processes__c = 'Documentation';
        objApp.Group_Name__c = 'Handover Queue';
        objApp.Slots__c = '10:00 - 11:00';
        //insert objApp;

        Appointment__c objApp1 = new Appointment__c();
        objApp1.Appointment_Date__c = date.newinstance( 2018, 12, 12 );
        objApp1.Appointment_End_Date__c = date.newinstance( 2018, 12, 21 );
        objApp1.Building__c = l.id;
        objApp1.Processes__c = 'Handover';
        objApp1.Sub_Processes__c = 'Unit Viewing';
        objApp1.Group_Name__c = 'Handover Queue';
        objApp1.Slots__c = '10:00 - 11:00';
        //insert objApp1;

		list<Appointment__c> lstApp = new list<Appointment__c>();
		lstApp.add(objApp);
		lstApp.add(objApp1);
		insert lstApp;

		appWrapObj.objApp = lstApp[0];
		appWrapObj.objCL = objCalling;
		appWrapObj.isSelected = true;
		appWrapObj.errorMessage = 'test';

		list<AppointmentServiceforMobile.AppointmentWrapper> lstAppWrap = new list<AppointmentServiceforMobile.AppointmentWrapper>();
		lstAppWrap.add(appWrapObj);

        selWrapObj.AccountID=a.id;
        selWrapObj.processName='Appointment Scheduling';
        selWrapObj.unitName='BSB/14/1403';
        selWrapObj.subProcessName='Documentation';
        selWrapObj.selectedDate='2018-06-26';
		selWrapObj.lstWrap = lstAppWrap;
        
		list<AppointmentServiceforMobile.selectionWrapper> lstSelWrap = new list<AppointmentServiceforMobile.selectionWrapper>();
		lstSelWrap.add(selWrapObj);
		
		AppointmentServiceforMobile.correctDateFormat('2018-06-26');
		AppointmentServiceforMobile.getAccounts(setAccountIds);

		String strJSON = JSON.serialize(lstSelWrap);      
		system.debug('strJSON'+strJSON);  
        RestRequest request = new RestRequest();
		
        request.requestUri = URL.getSalesforceBaseUrl()+'/services/apexrest/appointmentMobile';
		request.params.put('accountId', a.id);
		request.params.put('unitName', b.id);
		request.params.put('processName', 'Appointment Scheduling');
		request.params.put('subProcessName', 'Documentation');
		request.params.put('selectedDate', '2018-06-26');
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(strJSON);        
        RestContext.request = request;
        String response = AppointmentServiceforMobile.doPost();		
        AppointmentServiceforMobile.doGet();		
	}
}