@istest
public class DealExceptionRequest_Search_Test{

    static testmethod void callMethods(){
        Sendgrid_Credentials__c credentials = new Sendgrid_Credentials__c ();
        credentials.Endpoint_URL__c = 'test.com';
        credentials.API_Key__c = 'testcom';
        credentials.From_name__c = 'test';
        credentials.From_Email__c = 'test@test.com';
        insert credentials;
        
        List<To_Currency_Values__c> currencylst = new List<To_Currency_Values__c>();
        To_Currency_Values__c obj1 = new To_Currency_Values__c();
        obj1.Label__c='INR';
        obj1.Name='INR';
        currencylst.add(obj1);
        To_Currency_Values__c obj2 = new To_Currency_Values__c();
        obj2.Label__c='AED';
        obj2.Name='AED';
        currencylst.add(obj2);
        To_Currency_Values__c obj3 = new To_Currency_Values__c();
        obj3.Label__c='BAR';
        obj3.Name='BAR';
        currencylst.add(obj3);
        insert currencylst;
        
        List<Currency_Rate__c> currencyconversionlst = new List<Currency_Rate__c>();
        
        Currency_Rate__c currency1 = new Currency_Rate__c();
        currency1.Conversion_Date__c = System.today();
        currency1.Conversion_Rate__c = 0.12;
        currency1.From_Currency__c = 'AED';
        currency1.To_Currency__c = 'INR';
        currency1.IPMS_Row_ID__c= '12345';
        currencyconversionlst .add(currency1);
        
        Currency_Rate__c currency3 = new Currency_Rate__c();
        currency3.Conversion_Date__c = System.today();
        currency3.Conversion_Rate__c = 0.12;
        currency3.From_Currency__c = 'AED';
        currency3.To_Currency__c = 'EUR';
        currency3.IPMS_Row_ID__c= '12348';
        currencyconversionlst .add(currency3);
        
        Currency_Rate__c currency2 = new Currency_Rate__c();
        currency2.Conversion_Date__c = System.today();
        currency2.Conversion_Rate__c = 0.12;
        currency2.From_Currency__c = 'EUR';
        currency2.To_Currency__c = 'AED';
        currency2.IPMS_Row_ID__c= '12346';
        currencyconversionlst .add(currency2);
        
        insert currencyconversionlst ;
         
        user u = [select id from user where id!=:userInfo.getUserId() LIMIT 1];

        //Create Marketing Doc
        Marketing_Documents__c mDoc = new Marketing_Documents__c();
        mDoc.Marketing_Name__c = 'Test MarketingDoc';
        insert mDoc;
        
        Location__c loc = new Location__c();
        loc.Location_Id__c = '70274';
        loc.Name = 'CLR/SD88/A088B';
        insert loc;
        
        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Building_ID__c = '70274';
        pp.Building_Location__c = loc.id;
        pp.Effective_From__c = system.today();
        insert pp;
        
        Payment_Terms__c pT = new Payment_Terms__c();
        pT.Payment_Plan__c = pp.id;
        insert pT;
        
        ID recordTypeId = Schema.SObjectType.inventory__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        
        List<Inventory__c> invList=TestDataFactory.CreateInventoryRecords(new List<Inventory__c>{new Inventory__c()});
        List<Inventory__c> invList1=TestDataFactory.CreateInventoryRecords(new List<Inventory__c>{new Inventory__c()});
        
        invList1[0].status__c = 'Released';
        invList1[0].property_city__c = 'Dubai';
        invList1[0].Property_Status__c = 'Ready';
        invList1[0].Unit_Type__c = 'Hotel';
        invList1[0].Unit__c = 'DH/14/1407';
        invList1[0].Bedroom_Type__c = 'Retail';
        invList1[0].property_name__c = 'DAMAC TOWER';
        invList1[0].View_Type__c ='Back';
        invList1[0].Tagged_To_Unit_Assignment__c = false;
        invList1[0].Marketing_Name__c = 'Test MarketingDoc';
        invList1[0].Marketing_Name_Doc__c = mDoc.id;
        invList1[0].Building_ID__c  = '70274';
        invList1[0].RecordtypeId = recordTypeId;
        invList1[0].CurrencyIsoCode = 'EUR';
        invList1[0].Currency_of_Sale__c = 'EUR';
        invList1[0].Special_Price_Tax_Amount__c = 1000000;
        invList1[0].Special_Price__c = 100;       
        update invList1[0];
        
        invList[0].status__c = 'Released';
        invList[0].property_city__c = 'Dubai';
        invList[0].Property_Status__c = 'Ready';
        invList[0].Unit_Type__c = 'Hotel';
        invList[0].Unit__c = 'DH/14/1407';
        //invList[0].Anticipated_Completion_Date__c = '2013 Q2';
        invList[0].Bedroom_Type__c = 'Retail';
        invList[0].property_name__c = 'DAMAC TOWER';
        //invList[0].Floor_Package_Name__c
        invList[0].View_Type__c ='Back';
        invList[0].Tagged_To_Unit_Assignment__c = false;
        invList[0].Marketing_Name__c = 'Test MarketingDoc';
        invList[0].Marketing_Name_Doc__c = mDoc.id;
        invList[0].Building_ID__c   = '70274';
        invList[0].Floor_Package_Type__c = 'Floor';
        invList[0].CurrencyIsoCode = 'EUR';
        invList[0].Special_Price_Tax_Amount__c = 1000000;
        invList[0].Special_Price__c = 100;
        update invList[0];
        
        
        Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        ID recordTypeIdServiceReq = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();  
        
        Account a = new Account();
        a.LastName= 'Test Account';
        a.RecordTypeId = accountRTId;
        a.Agency_Short_Name__c = 'testShrName';
        a.Party_ID__c = '789456';
        a.Email__c = 'test@gmail.com';
        a.Mobile__c = '7894561230';
        a.Title__c = 'Ms.';
        a.Nationality__c = 'Australian';
        a.Passport_Number__c = '123654';
        insert a;
        
        NSIBPM__Service_Request__c servceReq = new NSIBPM__Service_Request__c ();
        servceReq.Agency__c = a.id;
        servceReq.RecordtypeId = recordTypeIdServiceReq;  
        insert servceReq;
        
        Booking__c bookng = new Booking__c ();
        bookng.Deal_SR__c = servceReq.id;
        insert bookng;
        
        set<id> bookingIds = new Set <ID> ();
        bookingIds.add (bookng.Id);
        
        Booking_Unit__c bookngUnit = new Booking_Unit__c ();
        bookngUnit.Booking__c = bookng.id;
        insert bookngUnit;
        
        Campaign__c camp = new Campaign__c();
        camp.Campaign_Name__c='Test Campaign';
        camp.start_date__c = System.today();
        camp.end_date__c = System.Today().addDays(30);
        camp.Marketing_start_date__c = System.today();
        camp.Marketing_end_date__c = System.Today().addDays(30);
        camp.Language__c = 'English';
        camp.Marketing_Active__c = true;
        camp.Credit_Control_Active__c  = true;
        camp.Sales_Admin_Active__c  = true;
        insert camp;

        
        Inventory_User__c invUser=new Inventory_User__c();
        invUser.Inventory__c=invList[0].id;
        invUser.User__c=userInfo.getUserId();
        invUser.Campaign__c = camp.id;
        insert invUser;

        Inventory_User__c invUser1=new Inventory_User__c();
        invUser1.Inventory__c=invList1[0].id;
        invUser1.User__c=u.id;
        insert invUser1;
        
        Inquiry__c newInquiry = new Inquiry__c();
        newInquiry.Campaign__c = camp.Id;
        newInquiry.First_Name__c = 'Testsearch';
        newInquiry.Last_Name__c = 'Inquiry';
        newInquiry.Mobile_Phone_Encrypt__c = '05789088';
        newInquiry.Mobile_Phone__c = '05789088';
        newInquiry.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        newInquiry.Email__c = 'test.lastname@eternussolutions.com';
        newInquiry.Preferred_Language__c = 'English';
        newInquiry.Inquiry_Source__c = 'Social' ;
        newInquiry.Inquiry_Status__c = 'New' ;
        insert newInquiry;
        
        SalesOffer_Record__c salesObj = new SalesOffer_Record__c();
        salesObj.Selected_Inventory_Ids__c = String.valueOf(invList1[0].id);
        salesObj.Type__c = 'DownLoad Availaibility';
        //salesObj.Trasaction_User__c = UserInfo.getUserId() ;
        insert salesObj;

        string ids = invList[0].id+','+invList1[0].id+',';
        
        DealExceptionRequest_SearchController con = new DealExceptionRequest_SearchController();
        ApexPages.currentPage().getParameters().put('limitVal','10');
        ApexPages.currentPage().getParameters().put('selectedInventories',ids);
        

        List<String> mDocList = new List<String>();
        mDocList.add(mDoc.id);
        
        Share_Project_Details__c obj = new Share_Project_Details__c();
        obj.Account__c = a.id;
        obj.Sent_To_Email__c ='test@test.com';
        insert obj;
        
        SalesOffer_Record__c salesofferrec = new SalesOffer_Record__c();
        salesofferrec.Inventory__c = invList[0].id;
        salesofferrec.Selected_Inventory_Ids__c = ids;
        insert salesofferrec;
        
        
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('salesofferrecordid',salesofferrec.Id);
        ApexPages.currentPage().getParameters().put('entityId', newInquiry.Id);
        ApexPages.currentPage().getParameters().put('shareprojectdetailid',obj.Id);
        Email_Request__c req = new Email_Request__c ();
        req.Template_for__c = 'Sales Offer Inquiry';
        req.Email_template__c = 'b70c2c19-b7d8-4eb4-acd1-e3f0883f438b';
        req.Substitution_tags__c = '-Name-';
        insert req;
        
        DAMAC_Project_Avaliability_Clone.getInquiries('Testsearch');
        DAMAC_Project_Avaliability_Clone.getAccounts('Test Account');
        DAMAC_Project_Avaliability_Clone.searchUnits('DH');
        con.getItems();
        con.getPropertyNames();
        con.getBedRooms();
        con.pageload();
        list<inventory_user__c> invo = new list<inventory_user__c>();
        invo.add(invuser1);
        
        con.otherUserinventory = invo;
        con.filterdata();
        
        con.nextbtn();
        con.prvbtn();
        
        con.sortColVal = 'Product';
        con.sortOrderVal = 'asc';
        con.nextbtn();
        
        con.sortColVal = 'Project';
        con.sortOrderVal = 'asc';
        con.nextbtn();
        
        
        con.sortColVal = 'Units';
        con.sortOrderVal = 'asc';
        con.nextbtn();
        
        con.sortColVal = 'Bedroom_Type';
        con.sortOrderVal = 'asc';
        con.nextbtn();
        
        con.sortColVal = 'Bedrooms';
        con.sortOrderVal = 'asc';
        con.nextbtn();
        
        con.changeRecCountFunMethod();
        con.changeSortOrder();
        con.searchRecs();
        con.getprv();
        con.getnxt();
        con.getUnits();
        
        Apexpages.currentPage().getParameters().put('currencyset','INR');
        con.filterData();
        Test.stopTest();
    }


     static testmethod void callMethods1(){
        Sendgrid_Credentials__c credentials = new Sendgrid_Credentials__c ();
        credentials.Endpoint_URL__c = 'test.com';
        credentials.API_Key__c = 'testcom';
        credentials.From_name__c = 'test';
        credentials.From_Email__c = 'test@test.com';
        insert credentials;
        
        List<To_Currency_Values__c> currencylst = new List<To_Currency_Values__c>();
        To_Currency_Values__c obj1 = new To_Currency_Values__c();
        obj1.Label__c='INR';
        obj1.Name='INR';
        currencylst.add(obj1);
        To_Currency_Values__c obj2 = new To_Currency_Values__c();
        obj2.Label__c='AED';
        obj2.Name='AED';
        currencylst.add(obj2);
        To_Currency_Values__c obj3 = new To_Currency_Values__c();
        obj3.Label__c='BAR';
        obj3.Name='BAR';
        currencylst.add(obj3);
        insert currencylst;
        
        List<Currency_Rate__c> currencyconversionlst = new List<Currency_Rate__c>();
        
        Currency_Rate__c currency1 = new Currency_Rate__c();
        currency1.Conversion_Date__c = System.today();
        currency1.Conversion_Rate__c = 0.12;
        currency1.From_Currency__c = 'AED';
        currency1.To_Currency__c = 'INR';
        currency1.IPMS_Row_ID__c= '12345';
        currencyconversionlst .add(currency1);
        
        Currency_Rate__c currency3 = new Currency_Rate__c();
        currency3.Conversion_Date__c = System.today();
        currency3.Conversion_Rate__c = 0.12;
        currency3.From_Currency__c = 'AED';
        currency3.To_Currency__c = 'EUR';
        currency3.IPMS_Row_ID__c= '12348';
        currencyconversionlst .add(currency3);
        
        Currency_Rate__c currency2 = new Currency_Rate__c();
        currency2.Conversion_Date__c = System.today();
        currency2.Conversion_Rate__c = 0.12;
        currency2.From_Currency__c = 'EUR';
        currency2.To_Currency__c = 'AED';
        currency2.IPMS_Row_ID__c= '12346';
        currencyconversionlst .add(currency2);
        
        insert currencyconversionlst ;
         
        user u = [select id from user where id!=:userInfo.getUserId() LIMIT 1];

        //Create Marketing Doc
        Marketing_Documents__c mDoc = new Marketing_Documents__c();
        mDoc.Marketing_Name__c = 'Test MarketingDoc';
        insert mDoc;
        
        Location__c loc = new Location__c();
        loc.Location_Id__c = '70274';
        loc.Name = 'CLR/SD88/A088B';
        insert loc;
        
        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Building_ID__c = '70274';
        pp.Building_Location__c = loc.id;
        pp.Effective_From__c = system.today();
        insert pp;
        
        Payment_Terms__c pT = new Payment_Terms__c();
        pT.Payment_Plan__c = pp.id;
        insert pT;
        
        Campaign__c camp = new Campaign__c();
        camp.Campaign_Name__c='Test Campaign';
        camp.start_date__c = System.today();
        camp.end_date__c = System.Today().addDays(30);
        camp.Marketing_start_date__c = System.today();
        camp.Marketing_end_date__c = System.Today().addDays(30);
        camp.Language__c = 'English';
        camp.Marketing_Active__c = true;
        camp.Credit_Control_Active__c  = true;
        camp.Sales_Admin_Active__c  = true;
        insert camp;

        Inquiry__c newInquiry = new Inquiry__c();
        newInquiry.Campaign__c = camp.Id;
        newInquiry.First_Name__c = 'Testsearch';
        newInquiry.Last_Name__c = 'Inquiry';
        newInquiry.Mobile_Phone_Encrypt__c = '05789088';
        newInquiry.Mobile_Phone__c = '05789088';
        newInquiry.Mobile_CountryCode__c = 'United Arab Emirates: 00971';
        newInquiry.Email__c = 'test.lastname@eternussolutions.com';
        newInquiry.Preferred_Language__c = 'English';
        newInquiry.Inquiry_Source__c = 'Social' ;
        newInquiry.Inquiry_Status__c = 'New' ;
        insert newInquiry;
        
        EOI_Process__c inserteoi = new EOI_Process__c();
        inserteoi.Inquiry__c = newInquiry.id; 
        inserteoi.Mode_of_Token_Payment__c = 'Cash';     
        inserteoi.No_of_Units__c = '1 Unit';
        insert inserteoi;

        ID recordTypeId = Schema.SObjectType.inventory__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
        
        List<Inventory__c> invList=TestDataFactory.CreateInventoryRecords(new List<Inventory__c>{new Inventory__c()});
        List<Inventory__c> invList1=TestDataFactory.CreateInventoryRecords(new List<Inventory__c>{new Inventory__c()});
        
        invList1[0].status__c = 'Released';
        invList1[0].property_city__c = 'Dubai';
        invList1[0].Property_Status__c = 'Ready';
        invList1[0].Unit_Type__c = 'Hotel';
        invList1[0].Unit__c = 'DH/14/1407';
        invList1[0].EOI__c = inserteoi.id;
        invList1[0].property_name__c = 'DAMAC TOWER';
        invList1[0].View_Type__c ='Back';
        invList1[0].Tagged_To_Unit_Assignment__c = false;
        invList1[0].Marketing_Name__c = 'Test MarketingDoc';
        invList1[0].Marketing_Name_Doc__c = mDoc.id;
        invList1[0].Building_ID__c  = '70274';
        invList1[0].RecordtypeId = recordTypeId;
        invList1[0].CurrencyIsoCode = 'EUR';
        invList1[0].Currency_of_Sale__c = 'EUR';
        invList1[0].Special_Price_Tax_Amount__c = 1000000;
        invList1[0].Special_Price__c = 100;
        update invList1[0];
        
        invList[0].status__c = 'Released';
        invList[0].property_city__c = 'Dubai';
        invList[0].Property_Status__c = 'Ready';
        invList[0].Unit_Type__c = 'Hotel';
        invList[0].Unit__c = 'DH/14/1407';
        invList[0].Bedroom_Type__c = 'Retail';
        invList[0].property_name__c = 'DAMAC TOWER';
        invList[0].View_Type__c ='Back';
        invList[0].Tagged_To_Unit_Assignment__c = false;
        invList[0].Marketing_Name__c = 'Test MarketingDoc';
        invList[0].Marketing_Name_Doc__c = mDoc.id;
        invList[0].Building_ID__c   = '70274';
        invList[0].Floor_Package_Type__c = 'Floor';
        invList[0].CurrencyIsoCode = 'EUR';
        invList[0].Special_Price_Tax_Amount__c = 1000000;
        invList[0].Special_Price__c = 100;
        update invList[0];
        
        
        Id accountRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        ID recordTypeIdServiceReq = Schema.SObjectType.NSIBPM__Service_Request__c.getRecordTypeInfosByName().get('Deal').getRecordTypeId();  
        
        Account a = new Account();
        a.LastName= 'Test Account';
        a.RecordTypeId = accountRTId;
        a.Agency_Short_Name__c = 'testShrName';
        a.Party_ID__c = '789456';
        a.Email__c = 'test@gmail.com';
        a.Mobile__c = '7894561230';
        a.Title__c = 'Ms.';
        a.Nationality__c = 'Australian';
        a.Passport_Number__c = '123654';
        insert a;
        
        NSIBPM__Service_Request__c servceReq = new NSIBPM__Service_Request__c ();
        servceReq.Agency__c = a.id;
        servceReq.RecordtypeId = recordTypeIdServiceReq;  
        insert servceReq;
        
        Booking__c bookng = new Booking__c ();
        bookng.Deal_SR__c = servceReq.id;
        insert bookng;
        
        set<id> bookingIds = new Set <ID> ();
        bookingIds.add (bookng.Id);
        
        Booking_Unit__c bookngUnit = new Booking_Unit__c ();
        bookngUnit.Booking__c = bookng.id;
        insert bookngUnit;
        
        Inventory_User__c invUser=new Inventory_User__c();
        invUser.Inventory__c=invList[0].id;
        invUser.User__c=userInfo.getUserId();
        invUser.Campaign__c = camp.id;
        insert invUser;

        Inventory_User__c invUser1=new Inventory_User__c();
        invUser1.Inventory__c=invList1[0].id;
        invUser1.User__c=u.id;
        insert invUser1;

        SalesOffer_Record__c salesObj = new SalesOffer_Record__c();
        salesObj.Selected_Inventory_Ids__c = String.valueOf(invList1[0].id);
        salesObj.Type__c = 'DownLoad Availaibility';
        salesObj.Trasaction_User__c = UserInfo.getUserId() ;
        insert salesObj;

       string ids = invList[0].id+','+invList1[0].id+',';

        DealExceptionRequest_SearchController con = new DealExceptionRequest_SearchController();
        ApexPages.currentPage().getParameters().put('limitVal','10');
        ApexPages.currentPage().getParameters().put('selectedInventories',ids);
        
        
        
        List<String> statusList = new List<String>();
        statusList.add('Ready');
        DealExceptionRequest_SearchController.checkSelectedUnits(String.valueOf(invList[0].id),String.valueOf(inserteoi.id));
        List<String> mDocList = new List<String>();
        mDocList.add(mDoc.id);
        con.idsselected = statusList;
        
        Share_Project_Details__c obj = new Share_Project_Details__c();
        obj.Account__c = a.id;
        obj.Sent_To_Email__c ='test@test.com';
        insert obj;
        
        SalesOffer_Record__c salesofferrec = new SalesOffer_Record__c();
        salesofferrec.Inventory__c = invList[0].id;
        salesofferrec.Selected_Inventory_Ids__c = ids;
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('salesofferrecordid',salesofferrec.Id);
        ApexPages.currentPage().getParameters().put('entityId', newInquiry.Id);
        ApexPages.currentPage().getParameters().put('shareprojectdetailid',obj.Id);
        Email_Request__c req = new Email_Request__c ();
        req.Template_for__c = 'Sales Offer Inquiry';
        req.Email_template__c = 'b70c2c19-b7d8-4eb4-acd1-e3f0883f438b';
        req.Substitution_tags__c = '-Name-';
        insert req;
        
        DealExceptionRequest_SearchController.getInquiries('Testsearch');
        DealExceptionRequest_SearchController.getAccounts('Test Account');
        DealExceptionRequest_SearchController.searchUnits('DH');
        con.getItems();
        con.getPropertyNames();
        con.getBedRooms();
        con.pageload();
        list<inventory_user__c> invo = new list<inventory_user__c>();
        invo.add(invuser1);
        
        con.otherUserinventory = invo;
        con.filterdata();
        
        con.nextbtn();
        con.prvbtn();
        
        con.sortColVal = 'Product';
        con.sortOrderVal = 'asc';
        con.nextbtn();
        
        con.sortColVal = 'Project';
        con.sortOrderVal = 'asc';
        con.nextbtn();
        
        
        con.sortColVal = 'Units';
        con.sortOrderVal = 'asc';
        con.nextbtn();
        
        con.sortColVal = 'Bedroom_Type';
        con.sortOrderVal = 'asc';
        con.nextbtn();
        
        con.sortColVal = 'Bedrooms';
        con.sortOrderVal = 'asc';
        con.nextbtn();
                
        con.changeRecCountFunMethod();
        con.changeSortOrder();
        con.searchRecs();
        con.getprv();
        con.getnxt();
        con.getUnits();
        
        Apexpages.currentPage().getParameters().put('currencyset','INR');
        con.filterData();
        Test.stopTest();
    }
}