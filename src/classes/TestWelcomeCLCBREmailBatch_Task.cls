/**
 * @File Name          : TestWelcomeCLCBREmailBatch_Task.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 10/28/2019, 3:27:02 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    10/22/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
private class TestWelcomeCLCBREmailBatch_Task{

    @testSetup static void setup(){

    }

    @isTest static void testMethod1() {
        // code_block   
        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        insert objAccount;
        System.debug('-->> objAccount ## : ' + objAccount );

        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = true);

        settingLst2.add(newSetting1);
        insert settingLst2;

        Id recordTypeIdWlc = [SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Welcome_Calling_List'
            AND IsActive = TRUE LIMIT 1].Id;

        Calling_List__c objCalling_List = new Calling_List__c(
            RecordTypeId = recordTypeIdWlc,
            Calling_List_Type__c='DP Calling',
            CBR_Assignment_Date__c = System.Today() - 1,
            Unit_Name__c = 'Test Unit',
            Notification_Time_for_outcome__c = System.Now(),
            IsHideFromUI__c = false,
            Customer_Name__c = 'Test Customer',
            Registration_ID__c = 'test',
            Calling_List_Status__c = 'New',
            Account__c = objAccount.Id
        );
        insert objCalling_List;

        Task objTask = new Task(
            WhatId = objCalling_List.Id ,
            Status = 'Not Started',
            Subject = 'Incorrect Contact Details',
            Assigned_User__c = 'RM'
        );
        insert objTask;
        System.debug('-->> objTask : ' + objTask );
        Datetime yesterday = Datetime.now().addDays(-1);
        Test.setCreatedDate(objTask.Id, yesterday);
        System.debug('-->> yesterday : ' + yesterday );
        System.debug('-->> After objTask: ' + objTask );

        Test.startTest();
            FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            WelcomeCLCBREmailBatch_Task objWelcomeCLCBREmailBatch_Task = new WelcomeCLCBREmailBatch_Task();
            Database.executeBatch(objWelcomeCLCBREmailBatch_Task, 1);
        Test.stopTest();

    }

    @isTest static void testMethod2() {
        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        insert objAccount;
        System.debug('-->> objAccount ## : ' + objAccount );

        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = true);

        settingLst2.add(newSetting1);
        insert settingLst2;

        Id recordTypeIdWlc = [SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Welcome_Calling_List'
            AND IsActive = TRUE LIMIT 1].Id;

        Calling_List__c objCalling_List = new Calling_List__c(
            RecordTypeId = recordTypeIdWlc,
            CBR_Assignment_Date__c = System.Today() - 1,
            Unit_Name__c = 'Test Unit',
            Calling_List_Type__c='DP Calling',
            Notification_Time_for_outcome__c = System.Now(),
            IsHideFromUI__c = false,
            Customer_Name__c = 'Test Customer',
            Registration_ID__c = 'test',
            Calling_List_Status__c = 'New',
            Account__c = objAccount.Id
        );
        insert objCalling_List;

        Task objTask1 = new Task(
            WhatId = objCalling_List.Id ,
            Status = 'Not Started',
            Subject = 'Incorrect Contact Details',
            Assigned_User__c = 'RM'
        );
        insert objTask1;
        System.debug('-->> objTask : ' + objTask1 );
        Datetime yesterday1 = Datetime.now().addDays(-2);
        Test.setCreatedDate(objTask1.Id, yesterday1);
        System.debug('-->> yesterday1 : ' + yesterday1 );
        System.debug('-->> After objTask1: ' + objTask1 );

        // code_block   
        Test.startTest();
            FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            WelcomeCLCBREmailBatch_Task objWelcomeCLCBREmailBatch_Task = new WelcomeCLCBREmailBatch_Task();
            Database.executeBatch(objWelcomeCLCBREmailBatch_Task, 1);
        Test.stopTest();
    }

    @isTest static void testMethod3() {
        Account objAccount = TestDataFactoryFM.createAccount();
        objAccount.Email__c = 'test@test.com'; 
        insert objAccount;
        System.debug('-->> objAccount ## : ' + objAccount );

        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
        OnOffCheck__c = true);

        settingLst2.add(newSetting1);
        insert settingLst2;

        Id recordTypeIdWlc = [SELECT Id
            FROM RecordType
            WHERE SObjectType='Calling_List__c' 
            AND DeveloperName='Welcome_Calling_List'
            AND IsActive = TRUE LIMIT 1].Id;

        Calling_List__c objCalling_List = new Calling_List__c(
            RecordTypeId = recordTypeIdWlc,
            Calling_List_Type__c='DP Calling',
            CBR_Assignment_Date__c = System.Today() - 1,
            Unit_Name__c = 'Test Unit',
            Notification_Time_for_outcome__c = System.Now(),
            IsHideFromUI__c = false,
            Customer_Name__c = 'Test Customer',
            Registration_ID__c = 'test',
            Calling_List_Status__c = 'New',
            Account__c = objAccount.Id
        );
        insert objCalling_List;

        Task objTask1 = new Task(
            WhatId = objCalling_List.Id ,
            Status = 'Not Started',
            Subject = 'Incorrect Contact Details',
            Assigned_User__c = 'RM'
        );
        insert objTask1;
        System.debug('-->> objTask : ' + objTask1 );
        Datetime yesterday1 = Datetime.now().addDays(-2);
        Test.setCreatedDate(objTask1.Id, yesterday1);
        System.debug('-->> yesterday1 : ' + yesterday1 );
        System.debug('-->> After objTask1: ' + objTask1 );

        // code_block   
        Test.startTest();
            FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            WelcomeCLCBREmailBatchTaskSchedule objWelcomeCLCBREmailBatchTaskSchedule = new WelcomeCLCBREmailBatchTaskSchedule();
            String sch = '0 0 23 * * ?'; 
            system.schedule('Test Account Sales Calculator Check', sch, objWelcomeCLCBREmailBatchTaskSchedule);
        Test.stopTest();
    }
}