/*
    Class Name      : Damac_ReservationFormHelper
    Test Class Name : Damac_ReservationFormHelper_Test
    Description     : To gather the information for Reservation form 
                        and gathering the sign information from Inquiry or Account
    Created By      : Srikanth V
*/

global class Damac_ReservationFormHelper {
    public String message { get; set; }
    public String signNowURL { get; set; }
    public Form_Request__c formRequest { get; set; }
    public List <Form_request_Inventories__c> inventories { get; set; }
    public User userDetails { get; set; }
    public boolean hasAlaCarte{get; set;}
    public Map<Id,Integer> invIdsWithPlansSize { get; set; }
    public Map<Id,Integer> invIdWithPromotionsSize { get; set; }
    public Map<Id,list<selectOption>> invIdWithPlans { get; set; }
    public list<selectOption> promotions { get; set; }
    public Map<Id,list<selectOption>> invIdWithPromotions { get; set; }
    public boolean hasAlacarteUnit{get; set;}

    //AlacarteOptions
    public list<SelectOption> interiorOptions{get; set;}
    public list<SelectOption> exteriorOptions{get; set;}
    public list<SelectOption> poolOptions{get; set;}
    public list<SelectOption> furnitureOptions{get; set;}
    
    public Damac_ReservationFormHelper (ApexPages.StandardController stdController) {
        ID formReqId = stdController.getId();
        message = '';
        userDetails = new User ();
        userDetails = [SELECT isPortalEnabled FROM User WHERE ID =: Userinfo.getUserId ()];
        hasAlacarteUnit = false;
        invIdWithPromotions = new Map<Id,list<selectOption>>();
        invIdWithPromotionsSize = new Map<Id,Integer>();
        formRequest = new Form_Request__c ();
        formRequest = Database.query ('SELECT RecordType.Name, inquiry__r.First_Name__c, Inquiry__r.Last_name__c, inquiry__r.Name, Corporate__r.name, '
                                    +getAllFields('Form_request__c')+' FROM Form_request__c WHERE Id =: formReqId');
        promotions = new list<selectOption>();
        if (formRequest.Buyer_Type__c == null)
            formRequest.Buyer_Type__c = 'Individual';
        formRequest.Document_Name__c = 'Reservation Form';
        

        interiorOptions = new list<SelectOption>();
        interiorOptions.add(new SelectOPtion('','--None--'));
        for(string s : system.label.Interior_options.split(','))
            interiorOptions.add(new SelectOption(s,s));
        
        exteriorOptions = new list<SelectOption>();
        exteriorOptions.add(new SelectOPtion('','--None--'));
        for(string s : system.label.Exterior_options.split(','))
            exteriorOptions.add(new SelectOption(s,s));
        
        poolOptions = new list<SelectOption>();
        poolOptions.add(new SelectOPtion('','--None--'));
        for(string s : system.label.Pool_options.split(','))
            poolOptions.add(new SelectOption(s,s));
        
        furnitureOptions = new list<SelectOption>();
        furnitureOptions.add(new SelectOPtion('','--None--'));
        for(string s : system.label.Furniture_options.split(','))
            furnitureOptions.add(new SelectOption(s,s));

        inventories = new List <Form_request_Inventories__c> ();
        Set<String> invIds = new Set<String>();
        Map<Id,String>  invIdsWithBuildingId = new Map<Id,String>();
        invIdsWithPlansSize = new Map<Id,Integer>();
        Id formRecordTypeId = Schema.SObjectType.Form_Request__c.getRecordTypeInfosByName().get('UK Form').getRecordTypeId();
        String unitCodes = '';
        for(Available_Options__c  availablePoint : [SELECT Promotion_Option__c,TemplateIdPN__c FROM Available_Options__c WHERE Form_Request__c =: formRequest.Id]){
            promotions.add(new SelectOption(availablePoint.TemplateIdPN__c, availablePoint.Promotion_Option__c));
        }

        list<Form_request_Inventories__c> formReqinventories = [SELECT Upgrade_Type__c,Form_request__r.RecordTypeId, Inventory__r.Property__r.ALACARTE_Enabled__c,Inventory__r.Property_Name__c, Inventory__r.Marketing_Name__c,
                            Inventory__r.Area_Sqft_2__c, Inventory__c, Inventory__r.Special_Price__c, Inventory__r.Total_Price_inc_VAT__c, inventory__r.Unit_Categorization__c, inventory__r.Bedroom_type__c,
                            Inventory__r.Unit_name__c, Inventory__r.is_mixed_use__c, Inventory__r.Bedroom_Type_Code__c,
                            inventory__r.ACD_Date__c, Inventory__r.Currency_of_Sale__c, Inventory__r.Area__c,
                            Form_request__c, Gross_price__c, Permitted_Usage__c,Additional_requests__c,Anticipated_completion_date__c,
                            Apartment_area__c, Apartment_No__c, Apartment_price__c, Apartment_Type__c, 
                            Introduced_by__c, Reservation_expiry_date__c, Interior_Option__c , Exterior_Option__c, Furniture_Option__c, Pool_Option__c,  
                            Type_of_Buyer__c ,Inventory__r.Building_ID__c,Payment_Plan_Name__c,
                            Selected_payment_Plan__c,
                            Promotions_Offered__c,Promotion_Name__c
                            FROM Form_request_Inventories__c
                            WHERE Form_request__c =: formRequest.ID];

        set<Id> inventoryIds = new set<Id>();
        Map <ID, ID> paymentPlanWithInvs = new Map <ID, ID> ();
        for (Form_request_Inventories__c inv : formReqinventories ){
            inventoryIds.add(inv.Inventory__c);
            if (inv.Selected_payment_Plan__c != null)
                paymentPlanWithInvs.put (inv.Inventory__c, inv.Selected_payment_Plan__c);
            if(inv.Inventory__r.Property__r.alacarte_Enabled__c)
                hasAlacarteUnit = true;
        }
        
        Map <String, Decimal> sellingPrices = new Map <String, Decimal> ();
        for (Payment_Plan_Association__c ppa : [SELECT Payment_Plan__c, Inventory__c, Selling_Price__c FROM Payment_Plan_Association__c 
                                                WHERE Inventory__c IN :paymentPlanWithInvs.keySet () 
                                                AND Payment_Plan__c IN :paymentPlanWithInvs.values()
                                                AND Selling_Price__c != NULL])
                                                
        {
            if (ppa.Selling_Price__c != null)
                sellingPrices.put (ppa.Inventory__c+'##'+ppa.Payment_Plan__c, ppa.Selling_Price__c);
        }

        map<id, list<Inventory_AddOn__c>> mpExistingAddOns = new map<id, list<Inventory_AddOn__c>>();
        if(!inventoryIds.isEmpty()){
            for(Inventory_AddOn__c addOn : [Select id, Inventory__c, Name, Option__c, Costing__c from Inventory_AddOn__c where Inventory__c in: inventoryIds]){
                if(!mpExistingAddOns.containsKey(addOn.Inventory__c))
                    mpExistingAddOns.put(addOn.Inventory__c, new list<Inventory_AddOn__c>{addOn});
                else{
                    list<Inventory_AddOn__c> existing = mpExistingAddOns.get(addOn.Inventory__c);
                    existing.add(addOn);
                    mpExistingAddOns.put(addOn.Inventory__c, existing);
                }
            }    
        }
        Set<Id> frInvIds = new Set<Id>();
        for (Form_request_Inventories__c inv : formReqinventories ) 
        {
            if (inv.Inventory__r.is_mixed_use__c != null)
                inv.Permitted_Usage__c = 'Residential';
            
            if (inv.Gross_price__c == NULL)
                inv.Gross_price__c = inv.Inventory__r.Total_Price_inc_VAT__c;    
            
            if(inv.Inventory__r.Property__r.ALACARTE_Enabled__c)
                hasAlaCarte = true;
                
            if (inv.Selected_payment_Plan__c != null && sellingPrices.containsKey (inv.Inventory__c+'##'+inv.Selected_payment_Plan__c)) {
                    inv.Gross_price__c = sellingPrices.get (inv.Inventory__c+'##'+inv.Selected_payment_Plan__c);

            }
            
            if (formRecordTypeId  == inv.Form_request__r.RecordTypeId) {
                invIds.add(inv.Inventory__r.Building_ID__c); 
                if(!invIdsWithBuildingId.containsKey(inv.Inventory__c)){
                    invIdsWithBuildingId.put(inv.Inventory__c,inv.Inventory__r.Building_ID__c);
                } 
                invIdsWithPlansSize.put(inv.Inventory__c,0); 
                invIdWithPromotionsSize.put(inv.Id,0); 
                
                if (inv.Inventory__r.ACD_Date__c != null)
                    inv.Anticipated_completion_date__c = Date.valueOf (inv.Inventory__r.ACD_Date__c);
                inv.Apartment_No__c = inv.inventory__r.Unit_name__c;
                inv.Apartment_Type__c = inv.Inventory__r.Bedroom_Type_Code__c;
                inv.currencyIsoCode = 'GBP';
                inv.Apartment_area__c = inv.Inventory__r.Area__c ;
                inv.Apartment_price__c = inv.Inventory__r.Total_Price_inc_VAT__c;
                
                inv.Gross_price__c = inv.Apartment_price__c;  
                
                unitCodes += inv.inventory__r.Unit_name__c+',';
                frInvIds.add(inv.Id);
                 
            }  
            if(mpExistingAddOns.containsKey(inv.Inventory__c)){
                for(Inventory_AddOn__c io : mpExistingAddOns.get(inv.Inventory__c)){
                    if(io.Name == 'Pool')
                        inv.Pool_option__c = io.Option__c;
                    if(io.Name == 'Furniture')
                        inv.Furniture_option__c = io.Option__c;
                    if(io.Name == 'Interior')
                        inv.Interior_option__c = io.Option__c;
                    if(io.Name == 'Exterior')
                        inv.Exterior_option__c = io.Option__c;
                }
            }      
            inventories.add (inv);
        }
        
        if (formRecordTypeId == formRequest.RecordTypeId && formRequest.Status__c == null) {
            Set<String> locIds = new Set<String>();
            for(Location__c loc: [Select id, location_Id__c, Construction_status__c, status__c, Building_name__c from Location__c where Location_Id__c in: invIds]){
               locIds.add(loc.location_Id__c);
            }
            invIdWithPlans = new Map<Id,list<selectOption>>();
            date currDate = system.today();
            
            for(Payment_Plan__c pp: [Select id,Building_ID__c,Name from Payment_Plan__c where Building_ID__c in: locIds and Effective_From__c<=:currDate and Effective_To_calculated__c>=:currDate]){
                for(Id invid : invIdsWithBuildingId.keySet()){
                    if(pp.Building_ID__c == invIdsWithBuildingId.get(invid)){
                        if(invIdWithPlans.containsKey(invid)){
                            invIdWithPlans.get(invid).add(new SelectOption(pp.Id, pp.Name));
                        }else{
                            invIdWithPlans.put(invid ,new List<selectOption>{new SelectOption(pp.Id, pp.Name)});
                            invIdsWithPlansSize.put(invid,1);
                        }
                    }
                }
            }
        
            formRequest.Seller_s_Solicitor_Details__c = Label.Seller_Solicitor;
            formRequest.Buyer_s_Solicitors_Details__c = Label.Buyer_Solicitor;
            
            for(Available_Options__c  availablePoint : [SELECT Promotion_Option__c,TemplateIdPN__c,Form_Request_Inventory__c 
                                                             FROM Available_Options__c WHERE Form_Request_Inventory__c IN : frInvIds]){
                if(invIdWithPromotions.containsKey(availablePoint.Form_Request_Inventory__c)){
                    invIdWithPromotions.get(availablePoint.Form_Request_Inventory__c).add(new SelectOption(availablePoint.TemplateIdPN__c, availablePoint.Promotion_Option__c));
                }else{
                    invIdWithPromotions.put(availablePoint.Form_Request_Inventory__c,new List<selectOption>{new SelectOption(availablePoint.TemplateIdPN__c, availablePoint.Promotion_Option__c)});
                    invIdWithPromotionsSize.put(availablePoint.Form_Request_Inventory__c,1);
                }
            }
            
        }
    }
    
    // Method to populate inquiry details on Reservation form when inquiry is selected from serach results
    public void populateInquiryOnFormRequest() {
        String inquiryId = Apexpages.currentpage().getparameters().get('inquiryId');
        Inquiry__c inq = [SELECT Nationality__c, Passport_number__c, Address_line_1__c, country__c, City__c,
                          Organisation_Name__c, Email__c, CR_Number__c, CR_Registration_Place__c,
                          CR_Registration_Expiry_Date__c, First_Name__c, Last_Name__c, Mobile_CountryCode__c, Country_of_Permanent_Residence__c
                          FROM Inquiry__c WHERE Id =: inquiryId];
        Id formRecordTypeId = Schema.SObjectType.Form_Request__c.getRecordTypeInfosByName().get('UK Form').getRecordTypeId();
        formRequest.Nationality__c = inq.Nationality__c;
        formRequest.Mobile_Country_Code__c = inq.Mobile_CountryCode__c;
        formRequest.Address__c = inq.Address_line_1__c;
        formRequest.Email__c = inq.Email__c;
        formRequest.Registration_Number__c = inq.CR_Number__c;
        formRequest.Passport_number__c = inq.Passport_number__c;
        formRequest.Purchaser_Name__c = inq.First_Name__c+'-'+inq.Last_Name__c;
        
        if (formRequest.RecordTypeId != formRecordTypeId) {
            formRequest.Country__c = inq.Country__c;
            formRequest.City__c = inq.City__c;
            formRequest.Corporation_Name__c = inq.Organisation_Name__c;
            formRequest.Registered_In__c = inq.CR_Registration_Place__c;
            formRequest.Country_of_Permanent_Residence__c = inq.Country_of_Permanent_Residence__c;
            
        }else{
            formRequest.corporate_Address__c = inq.Address_line_1__c;
            formRequest.Corporate_Nationality__c = inq.Nationality__c;
            formRequest.Corporate_Email__c = inq.Email__c;
        }
        formRequest.Inquiry__c = inq.Id;


    }
    
    // Method to populate Account details on Reservation form when Account is selected from serach results
    public void populateAccountOnFormRequest() {
        String accountId = Apexpages.currentpage().getparameters().get('accountId');
        Account acc = [SELECT Nationality__c, Passport_number__c, Address_line_1__c, country__c, City__c,
                          Organisation_Name__c, CR_Number__c, CR_Registration_Place__c,
                          CR_Registration_Expiry_Date__c, Agency_Email__c, Name
                          FROM Account WHERE Id =: accountId];
        formRequest.Passport_number__c = acc.Passport_number__c;
        formRequest.Address__c = acc.Address_line_1__c;
        formRequest.Country__c = acc.Country__c;
        formRequest.City__c = acc.City__c;
        formRequest.Email__c = acc.Agency_Email__c;
        formRequest.Corporation_Name__c = acc.Organisation_Name__c;
        formRequest.Registration_Number__c = acc.CR_Number__c;
        formRequest.Registered_In__c = acc.CR_Registration_Place__c;
        formRequest.Nationality__c = acc.Nationality__c;
        formRequest.Corporate__c = acc.Id;
        formRequest.Purchaser_Name__c = acc.Name;
    }
    
    // To save the details entered as a Form request record
    public void saveFormRequest () {
        message = '';
        if (formRequest.Inquiry__c == null && formRequest.Corporate__c == null) {
            message = 'Please select either Inquiry or Account';
        } 
        try {
        
            if (String.valueOf(formRequest.Inquiry__c).length() == 0 && String.valueOf (formRequest.Corporate__c).length() == 0) {
                message = 'Pleae select either Inquiry or Account';
            }
        } catch (Exception e) {
            message = 'Please select either Inquiry or Account';
        }
        
        for(Form_request_Inventories__c i : inventories){
            if(i.Inventory__r.Marketing_Name__c.contains('A LA CARTE')){
                if(i.Interior_Option__c == '' || i.Exterior_Option__c == ''|| i.Furniture_OPtion__c == '' || i.Pool_OPtion__c == '' || i.Pool_option__c == null || i.Furniture_option__c == null || i.Interior_option__c == null || i.Exterior_option__c == null){
                    message = 'Please provide A LA CARTE Options';
                }
            }
        }
        if (formRequest.Date_of_Payment_of_Reservation_Fee__c != null && message == '') {
            if (formRequest.Date_of_Payment_of_Reservation_Fee__c > Date.Today()) {
                message = 'Reservation fee payment date should not be greater than Today.';
            }    
        }
        system.debug (message+'===');
        if (message == '') {
            try {
                if (formRequest.Status__c != 'Signed') {
                    String signType = apexpages.currentpage().getparameters().get('signType');
                    formRequest.Status__c = signType;
                    Id formRecordTypeId = Schema.SObjectType.Form_Request__c.getRecordTypeInfosByName().get('UK Form').getRecordTypeId();
                    if (formRequest.Buyer_type__c != 'Corporate' && formRequest.RecordTypeId != formRecordTypeId) {
                        
                        formRequest.Corporation_Name__c = NULL;
                        formRequest.Registration_Number__c = NULL;
                        formRequest.Corporate_Email__c = NULL;
                        formRequest.Registered_In__c = null;
                        formRequest.Date_of_Registration__c = NULL;
                    }
                    if (formRequest.Buyer_Type__c == 'Corporate' && formRequest.RecordTypeId == formRecordTypeId) {
                        formRequest.Passport_Number__c = null;
                        formRequest.Nationality__c = null;
                        formRequest.Telephone_Number__c = null;
                        formRequest.Email__c = null;
                        formRequest.Address__c = null;
                        formRequest.Other_Individual_Name__c = null;
                        formRequest.X2nd_Application_Passport_Number__c = null;
                        formRequest.X2nd_Applicant_nationality__c = null;
                        formRequest.Other_Individual_Telephone__c = null;
                        formRequest.Other_Buyer_Individual_Email__c = null;
                        formRequest.Other_Individual_Address__c = null;
                    }
                    
                    if (formRequest.Buyer_Type__c == 'Individual' && formRequest.RecordTypeId == formRecordTypeId) {
                        formRequest.Corporation_Name__c = null;
                        formRequest.Registration_Number__c = null;
                        formRequest.Corporate_Nationality__c = null;
                        formRequest.Mobile_Number__c = null;
                        formRequest.Corporate_Email__c = null;
                        formRequest.Corporate_Address__c = null;
                        formRequest.Other_Corporate_Name__c = null;
                        formRequest.Other_Corporate_Registration_number__c = null;
                        formRequest.Other_Corporate_Nationality__c = null;
                        formRequest.Other_Corporate_Telephone__c = null;
                        formRequest.Other_Buyer_Corporate_Email__c = null;
                        formRequest.Other_Corporate_Address__c = null;
                    }
                    
                    if (formRequest.Date_of_Registration__c != null) {
                        if (formRequest.Date_of_Registration__c > Date.Today()) {
                            message = 'Registration date should not be greater than Today.';
                        }    
                    }
                    
                    set<string> alacarteKeys = new set<string>();
                    for(Form_request_Inventories__c i:inventories){
                        alacarteKeys.add(i.inventory__r.Unit_Categorization__c+'###'+i.inventory__r.Bedroom_Type__c);
                    }
                    System.debug('>>>>>>>alacarteKeys>>>>>>>>>>>'+alacarteKeys);
                    map<string, Alacarte_Addon__c> mpAlacarteAddOns = new map<string, Alacarte_AddOn__c>();
                    for(Alacarte_Addon__c aa : [Select id, Unique_Key__c, Furnishing_Price__c, Pool_price__c, Terrace_price__c from Alacarte_Addon__c where Unique_Key__c in: alacarteKeys]){
                        mpAlacarteAddOns.put(aa.Unique_Key__c, aa);
                    }

                    System.debug('>>>>>>>>mpAlacarteAddOns>>>>>>>>>'+mpAlacarteAddOns);
                    
                    list<Inventory_Addon__c> addOns = new list<Inventory_Addon__c>();
                    for(Form_request_Inventories__c i : inventories){
                        string key = i.inventory__r.Unit_Categorization__c+'###'+i.inventory__r.Bedroom_Type__c;
                        if(i.Inventory__r.Property__r.ALACARTE_Enabled__c){
                            addOns.add(new Inventory_Addon__c(Name = 'Interior', Option__c = i.Interior_option__c, Form_Request_Inventory__c = i.Id));
                            addOns.add(new Inventory_Addon__c(Name = 'Exterior', Option__c = i.Exterior_option__c, Form_Request_Inventory__c = i.Id));
                            if(mpAlacarteAddOns.containsKey(key)){
                                if(i.Pool_Option__c == 'Splash Pool')
                                    addOns.add(new Inventory_Addon__c(Name = 'Pool', Option__c = i.Pool_option__c, Form_Request_Inventory__c = i.Id, 
                                                                                Costing__c = mpAlacarteAddOns.get(key).Pool_price__c));
                                if(i.Furniture_Option__c != 'Without Furniture' && i.Furniture_Option__c != '' )
                                    addOns.add(new Inventory_Addon__c(Name = 'Furniture', Option__c = i.Furniture_option__c, Form_Request_Inventory__c = i.Id,
                                                                                Costing__c = mpAlacarteAddOns.get(key).Furnishing_Price__c));
                            }
                        }
                    }

                    if(!addOns.isEmpty())
                        insert addOns;

                    if (message == '') {
                        update formRequest;
                        update inventories;
                        message = 'Success';
                    }
                    system.debug (message);
                } else {
                    message = 'This form is already signed.';
                }
            } catch (Exception e) {
                message = e.getMessage()+' '+e.getLineNumber()+' '+e.getStackTraceString();
            }
        }
    }
    
    
    
    // Method to initiate the signing process.
    public void reservationFormSigning () {
        message = '';
        system.debug ('Reservation Signing');
       // try {
            String noSignUpUrl = '';
            String firstName = '';
            String lastName = '';
            String email = '';
            ID formReqId = formRequest.Id;
            formRequest = Database.query ('SELECT Corporate__r.Name, Corporate__r.Agency_Email__c, '
                           +'Inquiry__r.Email__c, Inquiry__r.First_Name__c, Inquiry__r.Last_Name__c,RecordType.Name,'
                            +getAllFields('Form_request__c')+' FROM Form_request__c WHERE Id = \''+ formReqId+'\'');
            system.debug (formRequest.status__c);
            
            Id formRecordTypeId = Schema.SObjectType.Form_Request__c.getRecordTypeInfosByName().get('UK Form').getRecordTypeId();
            PageReference pdf;
            if(formRequest.RecordTypeId == formRecordTypeId ){
                    pdf = page.Damac_UKURRF_WithSolicitor;
            }else{
                pdf = page.ReservationFormPDFClone;
            }
            pdf.getParameters().put( 'recId', formRequest.Id);
            
            Attachment att = new Attachment ();
            if(Test.isRunningTest()) { 
                att.body = blob.valueOf('Unit.Test');
            } else {
                att.Body = pdf.getContentAsPDF();
            }
            att.Name = 'Reservation Form -'+formRequest.Name+'.pdf';
            
            Signnow_credentials__c credentials = Signnow_credentials__c.getInstance(UserInfo.getUserId());
            if(formRequest.RecordTypeId == formRecordTypeId ){
                Signnow_credentials_UK__c credentialsUK = Signnow_credentials_UK__c.getInstance(UserInfo.getUserId());
                credentials.API_Key__c = credentialsUK.API_Key__c;
                credentials.Callback_URL__c = credentialsUK.Callback_URL__c;
                credentials.Email_Body__c = credentialsUK.Email_Body__c;
                credentials.Endpoint_url__c = credentialsUK.Endpoint_url__c;
                credentials.From_Email__c = credentialsUK.From_Email__c;
                credentials.Password__c = credentialsUK.Password__c;
                credentials.Subject__c = credentialsUK.Subject__c;
                credentials.User_Name__c = credentialsUK.User_Name__c; 
            
            }
            String accessToken = Damac_SignNowAPI.authenticate (credentials);
            String documentId = Damac_SignNowAPI.uploadAttachment(credentials, att, accessToken, null, null);
            
            Integer totalPageCount = Damac_SignNowAPI.getDocumentPageCount (credentials, documentId, accessToken);
            if (Test.isRunningTest()) {
                totalPageCount = 1;
            }

            List <Agent_Sign_Positions__mdt> signerPositions = new List <Agent_Sign_Positions__mdt> ();
            for (Agent_Sign_Positions__mdt rec : [SELECT MasterLabel, Document_Name__c, Page_Number__c,Signer__c, Type__c,
                                                  X_Position__c, Y_Position__c, Width__c, Height__c,Required__c
                                                  FROM Agent_Sign_Positions__mdt
                                                  WHERE Signer__c = '1' 
                                                  AND Document_Name__c = 'ReservationForm'
                                                  ORDER BY Page_Number__c])
            {
                Agent_sign_positions__mdt obj = new Agent_sign_positions__mdt ();
                obj = rec;
                obj.page_Number__c = totalPageCount - 1;
                signerPositions.add (obj);
            }
            if (signerPositions.size () > 0) {
                Damac_signNowAPI.addSignatureFieldMultiple (credentials, documentId, signerPositions, accessToken);
            }
            
            List <String> documentIds = new List <String> ();
            documentIds.add (documentId);
            
            if(formRequest.Corporate__c != null) {
                firstName = formRequest.Corporate__r.Name;
                lastName = formRequest.Corporate__r.Name;
                email = formRequest.Email__c;
            }
            
            if(formRequest.Inquiry__c != null) {
                firstName = formRequest.Inquiry__r.First_Name__c;
                lastName = formRequest.Inquiry__r.Last_Name__c;
                email = formRequest.Email__c;
            }
            
            if (formRequest.Buyer_Type__c == 'Corporate' && formRequest.RecordTypeId == formRecordTypeId) {
                firstName = formRequest.Corporation_Name__c;
                lastName = '';
                email = formRequest.Corporate_Email__c;
                
            }
            
            Map <String, HttpResponse> res = new Map <String, HttpResponse> ();
            if (formRequest.Status__c == 'Sign via Email')
            {
                res = Damac_SignNowAPI.sendInvitieToSign (credentials, documentIds, accessToken, 
                                                          email, '', '', att.Name);
            }
            
            else {
                noSignUpUrl = Damac_SignNowAPI.getSignUrlWithOutSignup(credentials, documentId, firstName, LastName); 
            }
            System.debug('::::noSignUpUrl::::'+noSignUpUrl);
            System.debug('::::resresres::::'+res);
            
            ESign_Details__c sDoc = new ESign_Details__c ();
            sDoc.Document_Id__c = documentId;
            sDoc.To_Email__c = email;
            sDoc.Form_Request__c = formRequest.Id;
            sDoc.Status__c = 'Email Sent';
            if (noSignUpUrl != '') {
                sDoc.Status__c = 'Sign Now';
            }
            insert sDoc;
            
            if(formRequest.Status__c == 'Sign Now' && noSignUpUrl != '') {
                message = noSignUpUrl;
            } else if (formRequest.Status__c == 'Sign Via Email' && res.get (documentId).getStatusCode() == 200) {
                message = 'Reservation form sent for signing';
            } else {
                message = 'Failed to generate Reservation Form. Please try again.';
            }
       // } catch (Exception e) {
        //    message = e.getMessage()+' '+e.getLineNumber();
       // }
    }
    
    // To get the inquiries based on the search keyword
    @RemoteAction 
    global static List<Inquiry__c> inquiryDetails(String searchKey){
        String key = '%'+searchKey+'%';
        User userObj = [SELECT IsPortalEnabled  FROM user WHERE id=:userinfo.getuserid() LIMIT 1];
        
        if(userObj.IsPortalEnabled) {
            Contact loginContact = AgentPortalUtilityQueryManager.getContactInformation();
            system.debug ('-- Portal Enabled --');
            return [select 
                            id,Name,First_name__c,Last_Name__c,Passport_Expiry_Date__c,Email__c,Phone_CountryCode__c,
                            Nationality__c,Phone__c,Date_of_Birth__c,Title__c,Mobile_Phone__c,Passport_Number__c from inquiry__c 
                            where ( First_Name__c LIKE:key or Last_Name__c LIKE:key or Name LIKE:key ) 
                            and  ( Inquiry_Status__c='Active' OR Inquiry_Status__c='New' OR Inquiry_Status__c='Qualified' 
                            OR Inquiry_Status__c ='Meeting Scheduled' )and (recordtype.Name='CIL' OR recordtype.Name='Inquiry') 
                            and Agent_Name__c =:loginContact.id LIMIT 10]; 
        } else {
    
        
            return [SELECT Name, First_name__c, Last_Name__c, Email__c, Mobile_Phone__c, Passport_Number__c 
                       FROM Inquiry__c 
                       WHERE ( First_Name__c LIKE:key OR Last_Name__c LIKE:key OR Name LIKE:key ) 
                       AND  ( Inquiry_Status__c='Active' 
                             OR Inquiry_Status__c='New' 
                             OR Inquiry_Status__c='Qualified' 
                             OR Inquiry_Status__c ='Meeting Scheduled' OR Inquiry_Status__c ='Negotiation' 
                             OR Inquiry_Status__c ='Negotiation In-Progress' 
                             OR Inquiry_Status__c='Contact Made')
                       AND (recordtype.Name='Inquiry' OR recordtype.Name='Inquiry Decrypted') 
                       AND ownerid=:userinfo.getuserid() 
                       LIMIT 10];  
        }
    }
    
    // To get the accounts based on the keywords searched and the type selected
    @RemoteAction 
    global static List<Account> accountDetails (String searchKey, String btype){
        List<Account> AcconuntList = new List<Account>();
        if(btype == 'Individual'){        
            AcconuntList = [SELECT Name, FirstName, LastName FROM Account 
                            WHERE Ownerid=:userinfo.getuserid() 
                                AND (firstname LIKE:searchKey OR lastName LIKE:searchKey OR name LIKE:'%'+searchkey+'%' ) 
                                AND recordtype.Name='Person Account' LIMIT 10];
        }
        if(btype == 'Corporate'){        
            AcconuntList = [SELECT Name, FirstName, LastName FROM Account 
                            WHERE ownerid=:userinfo.getuserid() 
                                AND (firstname LIKE:searchKey OR lastName LIKE:searchKey OR name LIKE:'%'+searchkey+'%' ) 
                                AND recordtype.Name='Business Account' LIMIT 10];            
        }        
        return AcconuntList;
    } 
    /************************************************************************************************
* @Description : Utility Method to get all fields based on object name for query                *
* @Params      : Object API                                                                     *
* @Return      : Object related Field APIs as Comma seperated                                   *
************************************************************************************************/
    public static string getAllFields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        } 
        Map <String, Schema.SObjectField> fieldMap = objectType.getDescribe ().fields.getMap ();
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            fields += sfield.getDescribe().getName () + ', ';
        }
        return fields.removeEnd(', ');
    }
}