@isTest
private class UpdateCallingListHandlerTest {
      static Id collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
      //static Id AgingcallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Aging Calling List').RecordTypeId;
      //static Id DPcallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('DP Calling List').RecordTypeId;
      static Id FMCollectionRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collections').RecordTypeId;
      static Id SPACallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('SPA Calling').RecordTypeId;
      static Id welcomeCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Welcome Calling List').RecordTypeId;
      static Id DroppedCallCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Dropped Calls - Call Back').RecordTypeId;
      static Id mortgageCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Mortgage Calling List').RecordTypeId;
    static Id walkInCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Walk In Calling List').RecordTypeId;
    static Id callBackCallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Call Back Request').RecordTypeId;
   
      static List<Calling_List__c>insertCallingLst;
      static List<User>userList;
      @testSetup()
      private static  void testData(){
       Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
     List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        // Insert Accont
        Account objAcc =  new Account(RecordTypeId = personAccRTId, 
                       FirstName='Test FirstName1', 
                       LastName='Test LastName2', 
                       Type='Person'
                      );
        insert objAcc ;
        insertCallingLst = new List<Calling_List__c>();
        userList = new List<User>();
         
      
        
        Calling_List__c welcomeCallInst = new Calling_List__c(RecordTypeId = welcomeCallingRecordTypeId,
                                                 Call_Outcome__c = 'Recovery list',
                                                 Customer_Flag__c = true,
                                                 Registration_ID__c = '1111',
                                                  Party_ID__c = '1177922');
        Calling_List__c collectionCallInst = new Calling_List__c(RecordTypeId = collectioncallingRecordTypeId,
                                                 Call_Outcome__c = 'Recovery list',
                                                 Registration_ID__c = '12851',
                                                  Party_ID__c = '1177924');
       
        Calling_List__c callbackCallInst = new Calling_List__c(RecordTypeId = collectioncallingRecordTypeId,
                                                 Call_Outcome__c = 'Recovery list',
                                                 Customer_Flag__c = true,
                                                 Call_Back_Date__c =  System.Now(),
                                                 Registration_ID__c = '128961',
                                                 Party_ID__c = '1177926');
        Calling_List__c callbackCallInstMortgage = new Calling_List__c(RecordTypeId = mortgageCallingRecordTypeId,
                                                  Call_Outcome__c = 'Recovery list',
                                                  Customer_Flag__c = true,
                                                  Registration_ID__c = '9978',
                                                  Party_ID__c = '1177927');
        Calling_List__c collectionCallInst2 = new Calling_List__c(RecordTypeId = collectioncallingRecordTypeId,
                                                 Call_Outcome__c = 'Recovery list',
                                                 Customer_Flag__c = true,
                                                 Registration_ID__c = '86466',
                                                 Party_ID__c = '1177928');
        /*Calling_List__c agingCallInst = new Calling_List__c(RecordTypeId = AgingcallingRecordTypeId,
                                                 Call_Outcome__c = 'Recovery list',
                                                 Customer_Flag__c = true,
                                                 Registration_ID__c = '86466',
                                                 Party_ID__c = '1177929');
        Calling_List__c agingCallInst2 = new Calling_List__c(RecordTypeId = AgingcallingRecordTypeId,
                                                 Call_Outcome__c = 'Recovery list',
                                                 Customer_Flag__c = true,
                                                 Registration_ID__c = '11112',
                                                 Party_ID__c = '1177910');
        Calling_List__c dpCallInst = new Calling_List__c(RecordTypeId = DPcallingRecordTypeId,
                                                 Call_Outcome__c = 'Recovery list',
                                                 Customer_Flag__c = true,
                                                 Registration_ID__c = '86466',
                                                 Party_ID__c = '1177911');*/
        Calling_List__c fmCallInst = new Calling_List__c(RecordTypeId = FMCollectionRecordTypeId,
                                                 Call_Outcome__c = 'Recovery list',
                                                 Customer_Flag__c = true,
                                                 Registration_ID__c = '86466',
                                                   Party_ID__c = '1177912'); 
        Calling_List__c fmCallInst2 = new Calling_List__c(RecordTypeId = FMCollectionRecordTypeId,
                                                 Call_Outcome__c = 'Recovery list',
                                                 Customer_Flag__c = true,
                                                 Registration_ID__c = '869216',
                                                 Party_ID__c = '1177913'); 
        /*Calling_List__c dpCallInst2 = new Calling_List__c(RecordTypeId = DPcallingRecordTypeId,
                                                 Call_Outcome__c = 'Recovery list',
                                                 Registration_ID__c = '12851',
                                                 Customer_Flag__c = true,
                                                 Party_ID__c = '1177914');*/
        Calling_List__c spaCallInstManager = new Calling_List__c(RecordTypeId = SPACallingRecordTypeId,
                                                 Manager_Name__c = 'Eternus Manager',
                                                 Customer_Flag__c = true,
                                                 IPMS_Employee_ID__c = '1000004',
                                                 Call_Outcome__c = 'Recovery list',
                                                 Registration_ID__c = '12851',
                                                 Party_ID__c = '1177915');
        Calling_List__c spaCallInstHOS = new Calling_List__c(RecordTypeId = SPACallingRecordTypeId,
                                                 Hos_Name__c = 'Eternus HOS',
                                                 HR_Employee_ID__c = '8975',
                                                 Call_Outcome__c = 'Recovery list',
                                                 Registration_ID__c = '12851',
                                                 Customer_Flag__c = true,
                                                 Party_ID__c = '1177916');
        Calling_List__c spaCallInstPC = new Calling_List__c(RecordTypeId = SPACallingRecordTypeId,
                                                 Property_Consultant__c = 'Eternus PC',
                                                 Manager_IPMS_Employee_ID__c = '589565',
                                                 Call_Outcome__c = 'Recovery list',
                                                 Registration_ID__c = '12851',
                                                 Customer_Flag__c = true,
                                                 Party_ID__c = '1177917');
                   
        
                                             
        Calling_List__c IncorrectiCallInstPC = new Calling_List__c(RecordTypeId = collectioncallingRecordTypeId,
                                                   Call_Outcome__c = 'Recovery list',
                                                   Customer_Flag__c = true,
                                                   Registration_ID__c = '12751',
                                                   Party_ID__c = '1177918');
        
        Calling_List__c callBackInst = new Calling_List__c(RecordTypeId = collectioncallingRecordTypeId,
                                                   Call_Outcome__c = 'Recovery list',
                                                   Customer_Flag__c = true,
                                                  // Call_Back_Date__c = System.now(),
                                                   Registration_ID__c = '12751',
                                                     Party_ID__c = '19618');
                                                     
          Calling_List__c walkinObj = new Calling_List__c(RecordTypeId = walkInCallingRecordTypeId,
                                                     Account__c = objAcc.Id,
                                                     Customer_Flag__c = true,
                                                     Purpose_of_Walk_in__c = 'Collections',
                                                     Calling_List_Status__c = 'New',
                                                     Notification_Time_for_outcome__c = DateTime.newInstance(2017,12,12,8,30,0),
                                                     Service_start__c = DateTime.newInstance(2017,12,12,2,37,0));
    
    
     Calling_List__c walkinObj2 = new Calling_List__c(RecordTypeId = walkInCallingRecordTypeId,
                                                     Account__c = objAcc.Id,
                                                     Customer_Flag__c = true,
                                                     Purpose_of_Walk_in__c = 'Collections',
                                                     Calling_List_Status__c = 'New',
                                                     Notification_Time_for_outcome__c = DateTime.newInstance(2017,12,12,8,30,0),
                                                     Service_start__c = System.now().addMinutes(8));
    //lstCallingLists.add(walkinObj);
    //insert lstCallingLists;
        insertCallingLst.add(walkinObj);
        insertCallingLst.add(walkinObj2);
        insertCallingLst.add(callbackCallInstMortgage);
        insertCallingLst.add(callBackInst);
        insertCallingLst.add(fmCallInst2);
        insertCallingLst.add(welcomeCallInst);
        insertCallingLst.add(collectionCallInst);
        insertCallingLst.add(IncorrectiCallInstPC);
        insertCallingLst.add(callbackCallInst);
        insertCallingLst.add(spaCallInstManager);
        insertCallingLst.add(spaCallInstHOS);
        insertCallingLst.add(spaCallInstPC);
        insertCallingLst.add(fmCallInst);
        insertCallingLst.add(collectionCallInst2);
        /*insertCallingLst.add(agingCallInst);
        insertCallingLst.add(agingCallInst2);
        insertCallingLst.add(dpCallInst);
        insertCallingLst.add(dpCallInst2);*/
        insert insertCallingLst;
        
         List<Calling_List_Setting__c>settingLst = new List<Calling_List_Setting__c>();
         Calling_List_Setting__c newSetting = new Calling_List_Setting__c(Object_Name__c= 'Account',
                                                                          Source_Field__c = 'Party_ID__c',
                                                                          Name= 'Party_ID__c');
          
        settingLst.add(newSetting);
        Calling_List_Setting__c newSetting2 = new Calling_List_Setting__c(Object_Name__c= 'Booking_Unit__c',
                                                                          Source_Field__c = 'Area__c',
                                                                          Name= 'Area__c');
          
        settingLst.add(newSetting2);
        Calling_List_Setting__c newSetting3 = new Calling_List_Setting__c(Object_Name__c= 'Inventory__c',
                                                                          Source_Field__c = 'Status_Flag__c ',
                                                                          Name= 'Status_Flag__c');
          
        settingLst.add(newSetting3);
        Calling_List_Setting__c newSetting4 = new Calling_List_Setting__c(Object_Name__c= 'Property__c',
                                                                          Source_Field__c = 'Property_Id__c',
                                                                          Name= 'Property_Id__c');
        settingLst.add(newSetting4);
        Calling_List_Setting__c newSetting5 = new Calling_List_Setting__c(Object_Name__c= 'Inventory__c',
                                                                          Source_Field__c = 'Area_Sqft__c',
                                                                          Name= 'Area_Sq_ft__c');
          
        settingLst.add(newSetting5);
        insert settingLst;
        
         //List<Emails_Ids_for_Queues__mdt>emailIdsMetadata = [];
         
         //emailIdsMetadata.add(metadata1);
         //insert emailIdsMetadata;
         User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
         Profile someProfile = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
         Profile adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
         Profile stdProfile = [SELECT Id FROM Profile WHERE Name = 'Standard Platform User' LIMIT 1];
         System.debug('^^^thisUser^^'+thisUser  );
         
         System.runAs (thisUser) {
                Group grpInst = new Group(Name='Contact Center Queue', type='Queue');
                insert grpInst;
                QueuesObject queueInst = new QueueSObject(QueueID = grpInst.id, SobjectType = 'Calling_List__c');
              insert queueInst;
               Group grpInst2 = new Group(Name='Collections WalkIn Queue', type='Queue');
                insert grpInst2;
                QueuesObject queueInst2 = new QueueSObject(QueueID = grpInst.id, SobjectType = 'Calling_List__c');
              insert queueInst2;
                UserRole roleInst = [SELECT Id FROM UserRole WHERE Name='Sales Admin'];
                System.debug('roleInst::::'+roleInst);
                User newManager = new User(firstName = 'Eternus',LastName='Manager',
                                            Email = 'puser002@amamama.com',
                                             Username = 'puser100@amamama.com' + System.currentTimeMillis(),
                                             CompanyName = 'TEST',
                                             Title = 'title',
                                             Alias = 'alias',
                                             TimeZoneSidKey = 'America/Los_Angeles',
                                             EmailEncodingKey = 'UTF-8',
                                             LanguageLocaleKey = 'en_US',
                                             LocaleSidKey = 'en_US',
                                             UserRoleId = roleInst.Id ,
                     /**/                    profileId =adminProfile.Id);
                                             
                 User newManager2 = new User(firstName = 'Anantha',LastName='Reddy Velma',
                                            Email = 'puser0022@amamama.com',
                                             Username = 'puser10022@amamama.com' + System.currentTimeMillis(),
                                             CompanyName = 'eternus',
                                             Title = 'title',
                                             Alias = 'alias',
                                             TimeZoneSidKey = 'America/Los_Angeles',
                                             EmailEncodingKey = 'UTF-8',
                                             LanguageLocaleKey = 'en_US',
                                             LocaleSidKey = 'en_US',
                                             UserRoleId = roleInst.Id ,
                                             profileId =adminProfile.Id);
                 /**/                            
                User newHOS = new User(firstName = 'Eternus',LastName='HOS', 
                                        Email = 'puser000@amamama.com',
                                             Username = 'puser200@amamama.com' + System.currentTimeMillis(),
                                             CompanyName = 'TEST',
                                             Title = 'title',
                                             Alias = 'alias',
                                             TimeZoneSidKey = 'America/Los_Angeles',
                                             EmailEncodingKey = 'UTF-8',
                                             LanguageLocaleKey = 'en_US',
                                             LocaleSidKey = 'en_US',
                                             UserRoleId = roleInst.Id,
                                             profileId =someProfile.Id );
                User newPC = new User(firstName = 'Eternus',LastName='PC',
                                        Email = 'puser000@amamama.com',
                                             Username = 'puser001@amamama.com' + System.currentTimeMillis(),
                                             CompanyName = 'TEST2',
                                             Title = 'title',
                                             Alias = 'alias',
                                             TimeZoneSidKey = 'America/Los_Angeles',
                                             EmailEncodingKey = 'UTF-8',
                                             LanguageLocaleKey = 'en_US',
                                             LocaleSidKey = 'en_US',
                                             UserRoleId = roleInst.Id,
                                             profileId =stdProfile.Id   );
                userList.add(newManager);
                userList.add(newManager2);
                userList.add(newHOS);
                userList.add(newPC);
                insert userList;
         }
    } 
     private static testMethod void test_fetchOwnerForWalkInCallingList(){
       Set<Id>queueIdSet = new Set<Id>();
       List<Group> queueLst = [SELECT Id,
                       Name 
                      FROM Group
                      WHERE Name = 'Contact Center Queue'];
     
        queueIdSet.add(queueLst[0].Id);
       
        Calling_List__c walkinObj2 = new Calling_List__c(RecordTypeId = walkInCallingRecordTypeId,
                                                     //Account__c = objAcc.Id,
                                                     Customer_Flag__c = true,
                                                     CE_Comments__c = 'hey this is demo comment',
                                                     //Booking_Unit_Name__c = 'Unit303',
                                                     Remarks__c = 'demo remark',
                                                     Purpose_of_Walk_in__c = 'Collections',
                                                     Calling_List_Status__c = 'New',
                                                     Notification_Time_for_outcome__c = DateTime.newInstance(2017,12,12,8,30,0),
                                                     Service_start__c = System.now().addMinutes(8));
  
        insert walkinObj2;       
      UpdateCallingListHandler.fetchOwnerForWalkInCallingList(queueIdSet,false,new List<Calling_List__c>{walkinObj2});
        UpdateCallingListHandler.fetchOwnerForWalkInCallingList(queueIdSet,true,new List<Calling_List__c>{walkinObj2});
      
    
    }
   private static testMethod void test_onBeforeInsertForCallback(){
      Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
          Account objAcc = new Account(RecordTypeId = personAccRTId, 
                       FirstName='Test FirstName1', 
                       LastName='Test LastName2', 
                       Type='Person', 
                       party_ID__C='12345'
                       );
        insert objAcc ;
     List<Calling_List__c>updateCallingList = new List<Calling_List__c>();
           Calling_List__c callbackCallInst = new Calling_List__c(RecordTypeId = callBackCallingRecordTypeId,
                                                 Call_Outcome__c = 'Recovery list',
                                                 Account__c = objAcc.Id,
                                                 Customer_Flag__c = true,
                                                 Registration_ID__c = '1000',
                                                 Party_ID__c = '1177923'  );
        insert callbackCallInst;
   }
    
    private static testMethod void updateCallingLstForOutcomesTest(){
      List<Calling_List__c>updateCallingList = new List<Calling_List__c>();
      Calling_List__c dealingCallobj = new Calling_List__c(RecordTypeId = mortgageCallingRecordTypeId,
                                                 Call_Outcome__c = 'Management dealing',
                                                 //Account__c = objAcc.Id,
                                                 Customer_Flag__c = true,
                                                 Registration_ID__c = '1090',
                                                 Party_ID__c = '117413'  );
        insert dealingCallobj;
        dealingCallobj.Call_Outcome__c = 'AoPT under process' ;
        updateCallingList.add(dealingCallobj);
        update updateCallingList;
        
        Calling_List__c RefusingCallobj = new Calling_List__c(RecordTypeId = mortgageCallingRecordTypeId,
                                                 Call_Outcome__c = 'Management dealing',
                                                 //Account__c = objAcc.Id,
                                                 Customer_Flag__c = true,
                                                 Registration_ID__c = '10740',
                                                 Party_ID__c = '1413'  );
        insert RefusingCallobj;
        RefusingCallobj.Call_Outcome__c = 'Refusing to Pay' ;
        updateCallingList.add(RefusingCallobj);
        update updateCallingList;
        
        
        
      
         Calling_List__c collectionCallInstManagement = new Calling_List__c(RecordTypeId = collectioncallingRecordTypeId,
                                                 Call_Outcome__c = 'Recovery list',
                                                 Customer_Flag__c = true,
                                                 Registration_ID__c = '1000',
                                                 Party_ID__c = '1177923'  );
        insert collectionCallInstManagement;
        collectionCallInstManagement.Call_Outcome__c = 'Management dealing' ;
        collectionCallInstManagement.Directors_list__c = 'Anantha Reddy Velma';
        updateCallingList.add(collectionCallInstManagement);
        update updateCallingList;
      
    }
    private static testMethod void onBeforeInsert(){
      List<Calling_List__c>callingLst = [SELECT Id,Registration_ID__c 
                                            FROM Calling_List__c
                                            WHERE Registration_ID__c != null];
    }
 
    private static testMethod void populateDataOnBeforeUpdateTest(){
        List<Calling_List__c>callingLst = [SELECT Id,Registration_ID__c 
                                            FROM Calling_List__c
                                            WHERE Registration_ID__c != null LIMIT 1];
        List<Calling_List__c>updateCallingList = new List<Calling_List__c>();
        for(Calling_List__c callInst : callingLst){
            updateCallingList.add(callInst);
        }
         update updateCallingList;
    }
    private static testMethod void testWelcomeCallingLst(){
       Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
          Account objAcc = new Account(RecordTypeId = personAccRTId, 
                               FirstName='Test FirstName1', 
                               LastName='Test LastName2', 
                               Type='Person', 
                               party_ID__C='12345'
                               );
        insert objAcc ;
        Calling_List__c welcomeCallInstforParty = new Calling_List__c(RecordTypeId = welcomeCallingRecordTypeId,
                                                 Customer_Flag__c = true,
                                                 Call_Outcome__c = 'Recovery list',
                                                 Party_ID__c = '12345'); 
         insert welcomeCallInstforParty;
        
       
    }
   
    //after update COD pending
     private static testMethod void testCODPendingCallingLst(){
       Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

        User u1 = [SELECT Id,
                  Name,
                  CompanyName,
                  Email
               FROM User
               WHERE CompanyName = 'eternus'];
        // Insert Accont
        //Primary_CRE__c
        Account objAcc = new Account(RecordTypeId = personAccRTId, 
                       FirstName='Test FirstName1', 
                       LastName='Test LastName2', 
                       Type='Person', 
                       party_ID__C='12345'
                       );
        insert objAcc ;
       /* objAcc.Primary_CRE__c = u1.Id;
    update objAcc;*/
    //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;

        //create property
        Property__c propInst = TestDataFactory_CRM.createProperty();
        //create inventory
        Inventory__c invInst =  TestDataFactory_CRM.createInventory(propInst.Id);
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1 );
        insert lstBookings ;

        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 1 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC';
           objUnit.Inventory__c = invInst.Id;
        }
    insert lstBookingUnits;
    
    
        List<Calling_List__c> lstCallingLists = createCallingList( welcomeCallingRecordTypeId , 1 ,lstBookingUnits );
        insert lstCallingLists;
    
    List<Calling_List__c> lstCallingListUpdate = [Select Id , 
                               Call_Outcome__c 
                                From Calling_List__c 
                             Where Id =:lstCallingLists LIMIT 1];
    
    lstCallingListUpdate[0].Call_Outcome__c = 'Incorrect Address - COD Pending';
    lstCallingListUpdate[0].Customer_Flag__c = true;
    update lstCallingListUpdate;
          
    }
  /*
     @ Description : To create calling list reocord
     @ Return      : list of calling list to be created
    */
    public static List<Calling_List__c> createCallingList( Id RecordTypeIdCollection, Integer counter , List<Booking_Unit__c> lstBookingUnits ) {
        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        for( Integer i=0; i<counter; i++ ) {
            lstCallingLists.add(new Calling_List__c( Registration_ID__c = lstBookingUnits[i].Registration_ID__c ,
                                 RecordTypeId = RecordTypeIdCollection ) );
        }
        return lstCallingLists;
    }
    
    private static testMethod void testWelcomeCallingLst2(){
       Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account objAcc = new Account(RecordTypeId = personAccRTId, 
                       FirstName='Test FirstName1', 
                       LastName='Test LastName2', 
                       Type='Person', 
                       party_ID__C='123456'
                       //Primary_CRE__c = '',
                       //Tertiary_CRE__c = null,
                      // Secondary_CRE__c = null 
                      );
        insert objAcc ;
        Calling_List__c welcomeCallInstforParty = new Calling_List__c(Account__c = objAcc.Id,
                             RecordTypeId = welcomeCallingRecordTypeId,
                                                 Call_Outcome__c = '',
                                                Customer_Flag__c = true,
                                                 Registration_ID__c = '99830',
                                                 Party_ID__c = '123456'); 
        insert welcomeCallInstforParty;
        Calling_List__c  callInst = [SELECT Id,
                            Account__c,
                            RecordTypeId,
                            Call_Outcome__c,
                            Party_ID__c,
                            Registration_ID__c
                         FROM Calling_List__c
                        WHERE Registration_ID__c = '99830'];
          callInst.Call_Outcome__c = 'Incorrect Contact Details';
           update callInst;
    
    }
   
    private static testMethod void onAfterUpdateTest(){
        List<Calling_List__c>updateCallingList = new List<Calling_List__c>();
    
         
        
       
        List<Calling_List__c>callingLstRec = [SELECT Id,Registration_ID__c, 
                                                  Call_Outcome__c
                                            FROM Calling_List__c
                                            WHERE RecordTypeId = :collectioncallingRecordTypeId
                                            AND  Registration_ID__c = '12851' LIMIT 1];
        for(Calling_List__c callInst : callingLstRec){
            callInst.Call_Outcome__c = 'Legal/Dispute cases/DLD/RERA Related' ;
            updateCallingList.add(callInst);
        }
        List<Calling_List__c>callingLstRecMortgage = [SELECT Id,Registration_ID__c, 
                                                  Call_Outcome__c
                                            FROM Calling_List__c
                                            WHERE RecordTypeId = :mortgageCallingRecordTypeId
                                            AND  Registration_ID__c = '9978' LIMIT 1];
        for(Calling_List__c callInst : callingLstRecMortgage){
            callInst.Call_Outcome__c = 'Customer interested, SR not initiated' ;
            updateCallingList.add(callInst);
        }
        
        List<Calling_List__c>callingLstRec2 = [SELECT Id,Registration_ID__c, 
                                                  Call_Outcome__c
                                            FROM Calling_List__c
                                            WHERE RecordTypeId = :FMCollectionRecordTypeId
                                            AND  Registration_ID__c = '869216' LIMIT 1];
        for(Calling_List__c callInst : callingLstRec2){
            callInst.Call_Outcome__c = 'Legal/Dispute cases/DLD/RERA Related' ;
            updateCallingList.add(callInst);
        }
       
        List<Calling_List__c>callingLstRecrds = [SELECT Id,Registration_ID__c, 
                                                  Call_Outcome__c
                                            FROM Calling_List__c
                                            WHERE RecordTypeId = :collectioncallingRecordTypeId
                                            AND Registration_ID__c = '86466' LIMIT 1];
        for(Calling_List__c callInst : callingLstRecrds){
            callInst.Call_Outcome__c = 'Mortgage list' ;
            updateCallingList.add(callInst);
        }
        
        List<Calling_List__c>callingLst1 = [SELECT Id,Registration_ID__c, 
                                                  Call_Outcome__c
                                            FROM Calling_List__c
                                            WHERE RecordTypeId = :FMCollectionRecordTypeId
                                            AND Registration_ID__c = '86466' LIMIT 1];
        for(Calling_List__c callInst : callingLst1){
            callInst.Call_Outcome__c = 'Legal/Dispute cases/DLD/RERA Related' ;
            updateCallingList.add(callInst);
        }
                
        List<Calling_List__c>callingLst2 = [SELECT Id,Registration_ID__c, 
                                                  Call_Outcome__c
                                            FROM Calling_List__c
                                            WHERE RecordTypeId = :SPACallingRecordTypeId
                                            AND Registration_ID__c = '12851' LIMIT 1];
        for(Calling_List__c callInst : callingLst2){
            callInst.Call_Outcome__c = 'Incorrect Contact Details' ;
            updateCallingList.add(callInst);
        }
       
        Calling_List__c callBackInst = new Calling_List__c(RecordTypeId = FMCollectionRecordTypeId,
                                                   Call_Outcome__c = 'Recovery list',
                                                   Customer_Flag__c = true,
                                                  // Call_Back_Date__c = System.now(),
                                                   Registration_ID__c = '125751',
                                                     Party_ID__c = '1961658');
        insert callBackInst;
        callBackInst.Call_Outcome__c = 'Call Back' ;
        callBackInst.Call_Back_Date__c = System.Now();
        
        /**/
        Calling_List__c callBackInst2 = new Calling_List__c(RecordTypeId = SPACallingRecordTypeId,
                                                   Call_Outcome__c = 'Recovery list',
                                                   Customer_Flag__c = true,
                                                  // Call_Back_Date__c = System.now(),
                                                   Registration_ID__c = '5751',
                                                     Party_ID__c = '1658');
        insert callBackInst2;
        callBackInst2.Call_Outcome__c = 'Call Back' ;
        callBackInst2.Call_Back_Date__c = System.Now();
        updateCallingList.add(callBackInst2);
        
         /**/
        Calling_List__c callBackInst3 = new Calling_List__c(RecordTypeId = welcomeCallingRecordTypeId,
                                                   Call_Outcome__c = 'Recovery list',
                                                   Customer_Flag__c = true,
                                                  // Call_Back_Date__c = System.now(),
                                                   Registration_ID__c = '751',
                                                     Party_ID__c = '168');
        insert callBackInst3;
        callBackInst3.Call_Outcome__c = 'Call Back' ;
        callBackInst3.Call_Back_Date__c = System.Now();
        updateCallingList.add(callBackInst3);
        /**/
         Calling_List__c callBackInst4 = new Calling_List__c(RecordTypeId = DroppedCallCallingRecordTypeId,
                                                   Call_Outcome__c = 'Recovery list',
                                                   Customer_Flag__c = true,
                                                  // Call_Back_Date__c = System.now(),
                                                   Registration_ID__c = '755461',
                                                     Party_ID__c = '546');
        insert callBackInst4;
        callBackInst4.Call_Outcome__c = 'Call Back' ;
        callBackInst4.Call_Back_Date__c = System.Now();
        updateCallingList.add(callBackInst4);
        
        /**/
         Calling_List__c callBackInst5 = new Calling_List__c(RecordTypeId = mortgageCallingRecordTypeId,
                                                   Call_Outcome__c = 'Recovery list',
                                                   Customer_Flag__c = true,
                                                  // Call_Back_Date__c = System.now(),
                                                   Registration_ID__c = '7475451',
                                                     Party_ID__c = '1645548');
        insert callBackInst5;
        callBackInst5.Call_Outcome__c = 'Call Back' ;
        callBackInst5.Call_Back_Date__c = System.Now();
        updateCallingList.add(callBackInst5);
       
       
        
        List<Calling_List__c>callingLst4 = [SELECT Id,Registration_ID__c, 
                                                  Call_Outcome__c,
                                                  ownerId,
                                                  Call_Back_Date__c
                                            FROM Calling_List__c
                                            WHERE RecordTypeId = :walkInCallingRecordTypeId
                                           LIMIT 1];
        for(Calling_List__c callInst : callingLst4){
          system.debug('callInst owner in test class::'+callInst.ownerId);
            updateCallingList.add(callInst);
        }
        
        update updateCallingList;
    }
    
}