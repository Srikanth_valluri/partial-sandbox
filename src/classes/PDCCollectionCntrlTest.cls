@isTest
public class PDCCollectionCntrlTest {
    @isTest
    static void testCLDetails(){
        Account objAccount = new Account();
        objAccount.Name = 'test';
        insert objAccount;
        
        Case objCase = new Case();
        objCase.AccountId = objAccount.Id;
        insert objCase;
        
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        objBooking.Account__c = objAccount.Id;
        insert objBooking;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.Unit_Name__c = 'test';
        objBU.Registration_ID__c = 'test';
        objBU.Booking__c = objBooking.Id;
        insert objBU;
        
        TriggerOnOffCustomSetting__c objTrigger = new TriggerOnOffCustomSetting__c();
        objTrigger.Name = 'CallingListTrigger';
        objTrigger.OnOffCheck__c = true;
        insert objTrigger;
        
        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Unit_Name__c = 'test';       
        objCallingList.Account__c = objAccount.Id;
        objCallingList.Booking_Unit__c  = objBU.Id;
        //objCallingList.Case__c = objCase.Id;
        insert objCallingList;
        
        Case_Post_Dated_Cheque__c objCasePost = new Case_Post_Dated_Cheque__c();
        objCasePost.Receipt_No__c =  '12345';
        objCasePost.Receipt_Date__c = System.today();
        objCasePost.New_maturity_date__c = System.today(); 
        objCasePost.New_Amount__c = 100;
        objCasePost.CurrencyIsoCode =  'AED';
        objCasePost.Reason__c = 'testReason';
        objCasePost.IPMS_Cash_Receipt_Id__c = 'test7855';
        objCasePost.Receipt_Classification__c = 'Early Handover';
        objCasePost.Mode_of_Payment__c = 'Cash';
        objCasePost.New_Drawer__c = ' testDrawer';
        objCasePost.New_Bank__c = 'testBank';
        objCasePost.Case__c = objCase.Id;
        objCasePost.Account__c = objAccount.Id;
        objCasePost.Calling_List__c = objCallingList.Id;
        objCasePost.Booking_Unit__c = objBU.Id;
        insert objCasePost;  
        
        PDCCollectionCntrl.ParentDetails objParentDetails = new PDCCollectionCntrl.ParentDetails();
   
        Test.startTest();
            PDCCollectionCntrl.getParentDetails(objCallingList.Id,objCase.Id);
            PDCCollectionCntrl.getPDCDetails(objCallingList.Id,'');
            PDCCollectionCntrl.getPDCDetails('',objCase.Id);
            PDCCollectionCntrl.callPDCReceiptService(objCallingList.Id,'');
            PDCCollectionCntrl.callPDCReceiptService('',objCase.Id);
            PDCCollectionCntrl.futureGeneratePDC( objCallingList.Id,objCase.Id );
        Test.stopTest();
    }
    
    @isTest
    static void testCreateCase(){
        
        Case objCase = new Case();
        insert objCase;
                
        TriggerOnOffCustomSetting__c objTrigger = new TriggerOnOffCustomSetting__c();
        objTrigger.Name = 'CallingListTrigger';
        objTrigger.OnOffCheck__c = true;
        insert objTrigger;
        
        Calling_List__c objCallingList = new Calling_List__c();
        objCallingList.Unit_Name__c = 'test';       
        objCallingList.Case__c = objCase.Id;
        insert objCallingList;
        
        objCallingList.Unit_Name__c = 'test1';
        Update objCallingList;
              
        Task objTask = new Task();
        objTask.Subject = 'Finance Verification';
        objTask.Parent_Task_Id__c = objCallingList.Id;
        objTask.WhatId = objCase.Id;
        objTask.assigned_user__c = 'Finance';
        Insert objTask;
        
        Test.startTest();
        	PDCCollectionCntrl.createCase(objCallingList.Id);
        Test.stopTest();
    }
    
    
    
}