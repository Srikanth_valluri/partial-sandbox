@istest
private class DAMAC_AssignApprovers_Test {
    @testSetup static void setup() 
    {
        Id devRecordTypeId = Schema.SObjectType.Inquiry_Assignment_Rules__c.getRecordTypeInfosByName().get('Approval Rules').getRecordTypeId();
        
        Inquiry_Assignment_Rules__c ua = new Inquiry_Assignment_Rules__c ();
            ua.Active__C = true;
            ua.recordTypeId = devRecordTypeId;
            ua.Fields_to_Apply_Rule__c = 'Token_Amount_AED__c; NSIBPM__Record_Type_Name__c';
            ua.Token_Amount_AED__c = '<20000';
            ua.Priority__c = 1;
            ua.Inquiry_Record_Type__c = 'Deal';
        insert ua;
        
        Inquiry_Assignment_Rules__c ua1 = new Inquiry_Assignment_Rules__c ();
            ua1.Active__C = true;
            ua1.Priority__c = 2;
            ua1.recordTypeId = devRecordTypeId;
            ua1.Fields_to_Apply_Rule__c = 'Token_Amount_AED__c; NSIBPM__Record_Type_Name__c';
            ua1.Token_Amount_AED__c = '<=20000';
            ua1.Inquiry_Record_Type__c = 'Deal';
        insert ua1;
        
        Inquiry_Assignment_Rules__c ua2 = new Inquiry_Assignment_Rules__c ();
            ua2.Active__C = true;
            ua2.Priority__c = 3;
            ua2.recordTypeId = devRecordTypeId;
            ua2.Fields_to_Apply_Rule__c = 'Token_Amount_AED__c; NSIBPM__Record_Type_Name__c';
            ua2.Token_Amount_AED__c = '>=20000';
            ua2.Inquiry_Record_Type__c = 'Deal';
        insert ua2;
        
        Inquiry_Assignment_Rules__c ua3 = new Inquiry_Assignment_Rules__c ();
            ua3.Active__C = true;
            ua3.Priority__c = 4;
            ua3.recordTypeId = devRecordTypeId;
            ua3.Fields_to_Apply_Rule__c = 'Token_Amount_AED__c; NSIBPM__Record_Type_Name__c';
            ua3.Token_Amount_AED__c = '>20000';
            ua3.Inquiry_Record_Type__c = 'Deal';
        insert ua3;
        
        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Deal');
        insert srTemplate;

        NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Deal', false, null);
        sr.ID_Type__c = null;
        sr.Agency_Type__c = 'Corporate';
        sr.agency__c = null;
        
        sr.Token_Amount_AED__c = 10000;
        sr.NSIBPM__SR_Template__c = srTemplate.id;
        insert sr;
        
        NSIBPM__Service_Request__c SR2 = InitializeSRDataTest.getSerReq('Deal', false, null);
        sr2.ID_Type__c = null;
        sr2.Agency_Type__c = 'Corporate';
        sr2.agency__c = null;
        
        sr2.Token_Amount_AED__c = 30000;
        sr2.NSIBPM__SR_Template__c = srTemplate.id;
        insert sr2;
    
    }
    @isTest static void testVillaBooking () {
        NSIBPM__Service_Request__c dealSR = [SELECT createdById, CreatedBy.Name, CreatedBy.ManagerId, 
                                                createdby.manager.managerId, 
                                                createdby.manager.manager.managerId,
                                                createdBy.manager.manager.manager.managerId,
                                                RM_Email__c, Token_Amount_AED__c,
                                                NSIBPM__Record_Type_Name__c
                                            FROM NSIBPM__Service_Request__c WHERE Token_Amount_AED__c = 10000 LIMIT 1];
                                            
        DAMAC_AssignApprovers.getEligibleRule (new List <NSIBPM__Service_Request__c>{dealSR});
    }
    
    @isTest static void testVillaBooking2 () {
        NSIBPM__Service_Request__c dealSR2 = [SELECT createdById, CreatedBy.Name, CreatedBy.ManagerId, 
                                                createdby.manager.managerId, 
                                                createdby.manager.manager.managerId,
                                                createdBy.manager.manager.manager.managerId,
                                                RM_Email__c, Token_Amount_AED__c,
                                                NSIBPM__Record_Type_Name__c
                                            FROM NSIBPM__Service_Request__c WHERE Token_Amount_AED__c = 30000  LIMIT 1];
                                            
        DAMAC_AssignApprovers.getEligibleRule (new List <NSIBPM__Service_Request__c>{dealSR2});
    }
}