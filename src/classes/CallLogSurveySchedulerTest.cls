@isTest
private class CallLogSurveySchedulerTest {

    static testMethod void testScheduler() {
      Test.startTest();
        String jobName = 'demo job';
        CallLogSurveyScheduler ascsObject = new CallLogSurveyScheduler ();
      String sch = '0 0 23 * * ?'; 
      system.schedule(jobName+System.Now(), sch, ascsObject); 
    Test.stopTest();
    }
}