@isTest(seeAlldata = true)
public class DMLBatchHandler_Test {
    
    static testmethod void m1(){
        Test.startTest();
        DMLBatchHandler obj1 = new DMLBatchHandler('Select id from Inquiry__c limit 1','delete',true,'','','');
        database.executeBatch(obj1);
        test.StopTest();
    } 
    
    static testmethod void m2(){   
        Test.startTest();
        DMLBatchHandler obj2 = new DMLBatchHandler('Select id from Inquiry__c limit 1','update',false,'First_Name__c','test','');
        database.executeBatch(obj2);
        Test.StopTest();
    }
    
    static testmethod void m3(){
        Test.startTest();
        DMLBatchHandler obj3 = new DMLBatchHandler('Select id, First_Name__c from Inquiry__c limit 1','update',false,'First_Name__c','','_1');
        database.executeBatch(obj3);
        Test.StopTest();
    }
    
}