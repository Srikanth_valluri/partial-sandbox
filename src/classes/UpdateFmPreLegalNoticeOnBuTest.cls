@isTest
public class UpdateFmPreLegalNoticeOnBuTest {
    private static testMethod void testOne(){
        Admin_Login_for_Drawloop__c objInst = new Admin_Login_for_Drawloop__c();
        objInst.Username__c = 'none';
        objInst.Password__c = 'none';
        objInst.Domain__c = 'none';
        insert objInst;
        
        List<Booking__c> bookingList = new List<Booking__c>();
        List<Booking_Unit__c> bookingUnitList = new List<Booking_Unit__c>();
        
        // Creation of Account
        Account objAccount = TestDataFactory_CRM.createPersonAccount();
        insert objAccount;

        //create Deal SR record
        NSIBPM__Service_Request__c objDealSR = TestDataFactory_CRM.createServiceRequest();
        insert objDealSR;

        //create Booking record for above created Deal and Account
        bookingList = TestDataFactory_CRM.createBookingForAccount(objAccount.Id,objDealSR.Id,1);
        insert bookingList;

        //create Booking Units record for above created Bookings
        bookingUnitList = TestDataFactory_CRM.createBookingUnits(bookingList,1);
        for(Booking_Unit__c objBookingUnit : bookingUnitList) {
          objBookingUnit.Send_FM_Pre_Legal_Notice__c = true;
          objBookingUnit.Registration_Status_Code__c = 'MI';
          objBookingUnit.Building_Name__c = 'Test building';
        }
		
		/*bookingUnitList[0].Id = 'a0x0Y000001hiZW';
		upsert bookingUnitList;*/
		
		
        insert bookingUnitList;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestServiceMock());
        UpdateFmPreLegalNoticeOnBu objClass = new UpdateFmPreLegalNoticeOnBu();
        Database.Executebatch(objClass);
        Test.stopTest();
    }
}