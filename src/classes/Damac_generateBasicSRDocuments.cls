Global class Damac_generateBasicSRDocuments {

    webservice static void generateDocuments (Id srId) {
        NSIBPM__Service_Request__c objFinSR = new NSIBPM__Service_Request__c ();

        String strQuery = '';
        strQuery += UtilityQueryManager.getAllFields(NSIBPM__Service_Request__c.getsObjecttype().getDescribe()) ;
        strQuery += '  WHERE Id =:srId';
        for (NSIBPM__Service_Request__c SR : database.query(strQuery)) {
            objFinSR = SR;
        }
        
        String amdQuery = UtilityQueryManager.getAllFields(Amendment__c.getsObjectType().getDescribe());
        amdQuery += ' WHERE Service_Request__c =: srId ';
        List <Amendment__c> amdList  = new List <Amendment__c> ();
        amdlist = Database.query(amdQuery);
        
       
        
        if (objFinSR.Agent_Registration_Type__c != 'LOI') {
            if(SR_Process__c.getInstance(userinfo.getProfileId()).New_Document_Process__c) {
                SRUtility.createDocuments(amdlist, objFinSR, 'Generate'); 
            }
            else {
                for (Amendment__c amd : amdList)
                    SRUtility.createSRDocuments(amd, objFinSR); 
            }
        }
    }
}