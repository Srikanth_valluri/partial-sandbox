public without sharing class FmcServiceChargeController {

    public List<Booking_Unit__c>    lstUnit     {get; set;}
    public String                   baseUrl     {get; set;}
    public String                   units       {get; set;}
    public String                   projects    {get; set;}
    public Decimal                  amount      {get; set;}


    static  String  FM_PORTAL_SERVICE_CHARGE    = Label.FMPortalServiceCharge;
    static  String  UNIT_QUERY_ERROR            = 'There was an error while fetching your units.'
                                                    + ' Please try again or contact support team.';

    public FmcServiceChargeController() {
        if (FmcUtils.isCurrentView('ServiceCharge')) {
            lstUnit = new List<Booking_Unit__c>();
            baseUrl = Site.getBaseSecureUrl();
            units = '';
            projects = '';
            amount = 0;
            try {
                lstUnit = queryUnitsForAccount(CustomerCommunityUtils.customerAccountId, new Set<String>{'Booking__r.Account__c'}, FmcUtils.UNIT_FIELDS, '');
            } catch(QueryException queryExcp) {
                System.debug(queryExcp.getMessage());
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, UNIT_QUERY_ERROR));
            }
        }
    }

    public static List<Booking_Unit__c> queryUnitsForAccount(
        Id accountId, Set<String> relationToUnit, String fields, String filter
    ) {
        Set<String> setActiveStatuses = Booking_Unit_Active_Status__c.getAll().keySet();

        Set<String> unitFields = new Set<String>();
        for (String field : fields.toLowerCase().split(',')) {
            unitFields.add(field.trim());
        }

        if (!unitFields.contains('id')) {
            fields += ',Id';
        }

        Set<String> invalidHandoverStatus = new Set<String> {NULL, ''};
        invalidHandoverStatus.addAll(Label.FM_Booking_Unit_Handover_Status.split(','));

        String query = ' SELECT ' + fields + ' FROM Booking_Unit__c '
                     + ' WHERE  Handover_Status__c NOT IN :invalidHandoverStatus '
                     + ' AND (Handover_Flag__c = \'Y\' OR Early_Handover__c = true OR Inventory__r.Building_Location__r.Finance_Confirmation_Complete__c = TRUE)'
                        + ' AND Registration_Status__c IN :setActiveStatuses ';

        String strAccountId = ' \'' + accountId + '\' ';

        query += ' AND (';
        for (String relatedField : relationToUnit) {
            query += (' ' + relatedField + ' = ' + strAccountId + ' OR ');
        }
        query = query.removeEnd(' OR ');
        query += ' ) ';

        query += (String.isBlank(filter) ? '' : (' AND ' + filter));
        System.debug('units query = ' + query);
        return Database.query(query);
    }

    @RemoteAction
    public static FmIpmsRestServices.DueInvoicesResult fetchFmDues() {
        String partyId = CustomerCommunityUtils.getPartyId();
        if (String.isBlank(partyId)) {
            throw new LoamsCommunityException('Error: Could not fetch due amount. (Invalid Party Id)');
        }

        FmIpmsRestServices.DueInvoicesResult dues;
        try {
            System.debug('partyId:: ' + partyId);
            dues = FmIpmsRestServices.getDueInvoices('', partyId, '',true);
            System.debug('dues from IPMS : ' + dues);

        } catch(Exception excp) {
            System.debug('excp = ' + excp);
            //throw new LoamsCommunityException('Could not fetch due amount. (Timeout error)');
        }

        return dues;
    }

    @RemoteAction
    public static String generateFmSoa(String registrationId) {
        if (String.isBlank(registrationId)) {
            return registrationId;
        }
        String soaUrl;
        try {
            soaUrl = FmIpmsRestServices.getUnitSoaByRegistrationId(registrationId);
        } catch(Exception e) {
            System.debug(e.getMessage());
            }
        System.debug('soaUrl = ' + soaUrl);
        if (String.isBlank(soaUrl)) {
            throw new LoamsCommunityException('Error : Unit SOA generation failed. Please try again');
        }
        return soaUrl;
    }

    /*public PageReference getPaymentUrl() {
        CCAvenuePaymentGateway gateway = CCAvenuePaymentGateway.getInstance(LoamsCommunityController.GATEWAY_NAME);
        if (gateway == NULL) {
            return NULL;
        }
        return new PageReference(gateway.initiateTransaction(
            units, amount, baseUrl + '/CommunityPaymentStatus?retUrl=' + Site.getPathPrefix(),
            CustomerCommunityUtils.customerAccountId, projects
        ));
    }*/

    @RemoteAction
    public static String getPaymentUrl(
        Decimal paymentAmount, Map<String, List<FmIpmsRestServices.DueInvoice>> unitInvoices
    ) {
        CCAvenuePaymentGateway gateway = CCAvenuePaymentGateway.getInstance(LoamsCommunityController.GATEWAY_NAME);
        if (gateway == NULL) {
            return NULL;
        }

        System.debug('unitInvoices = ' + JSON.serialize(unitInvoices));
        FM_Receipt__c fmReceipt = new FM_Receipt__c(
            Account__c = CustomerCommunityUtils.customerAccountId,
            Payment_Type__c = FM_PORTAL_SERVICE_CHARGE,
            Amount__c = paymentAmount
        );
        insert fmReceipt;

        fmReceipt = [ SELECT Id
                            , Name
                            , Amount__c
                      FROM FM_Receipt__c
                      WHERE Id = :fmReceipt.Id ];

        List<FM_Unit_Invoice_Payment__c> lstUnitInvoicePayment = new List<FM_Unit_Invoice_Payment__c>();

        for (String unitId : unitInvoices.keySet()) {
            List<FmIpmsRestServices.DueInvoice> lstInvoice = unitInvoices.get(unitId);
            for (FmIpmsRestServices.DueInvoice dueInvoice : lstInvoice) {
                lstUnitInvoicePayment.add(new FM_Unit_Invoice_Payment__c(
                    FM_Receipt__c = fmReceipt.Id,
                    Booking_Unit__c = unitId,
                    Name = dueInvoice.invoiceNumber,
                    Call_Type__c = dueInvoice.callType,
                    Creation_Date__c = String.isBlank(dueInvoice.creationDate) ? NULL :
                                CustomerCommunityUtils.parseIpmsDateString(dueInvoice.creationDate),
                    Due_Date__c = String.isBlank(dueInvoice.dueDate) ? NULL :
                                    CustomerCommunityUtils.parseIpmsDateString(dueInvoice.dueDate),
                    Party_Id__c = dueInvoice.partyId,
                    Party_Name__c = dueInvoice.partyName,
                    Payment_Amount__c = dueInvoice.dueRemaining,
                    Project_Name__c = dueInvoice.projectName,
                    Registration_Id__c = dueInvoice.registrationId,
                    TRX_Number__c = dueInvoice.trxNumber,
                    TRX_Type__c = dueInvoice.trxType,
                    //TRX_Id__c = dueInvoice.trxId,
                    Unit_Name__c = dueInvoice.unitName
                ));
            }
        }
        System.debug('lstUnitInvoicePayment = ' + JSON.serialize(lstUnitInvoicePayment));
        insert lstUnitInvoicePayment;

        //Math.round for initiateTransaction in sandbox
        Boolean isSandbox = [SELECT IsSandbox FROM Organization].IsSandbox;
        if (isSandbox) {
            Decimal amount = Math.round(fmReceipt.Amount__c);
            if (amount == NULL || amount <= 0) {
                amount = 1;
            }
        }

        return gateway.initiateTransaction(
            fmReceipt.Name, paymentAmount,
            Site.getBaseSecureUrl() + '/CommunityPaymentStatus?retUrl=' + Site.getPathPrefix(),
            CustomerCommunityUtils.customerAccountId, FM_PORTAL_SERVICE_CHARGE
        );
    }

}