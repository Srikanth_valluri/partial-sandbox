public class notifyCREforCalBack implements Database.Batchable<sObject>,Database.AllowsCallouts{
    
    Map<Id,User> mapOfIdToUser;
    
    public notifyCREforCalBack(){
        
       
        mapOfIdToUser = new Map<Id,User>();
        
        List<User> lstActiveUser = [Select Id,
                                    Name,
                                    isActive,                                   
                                    UserType,Team_Leader_Name__c,
                                    UserRole.Name,
                                    UserRoleID,
                                    FirstName,
                                    LastName,Email,
                                    ManagerId,
                                    Manager.FirstName,
                                    Manager.ManagerId,
                                    Manager.LastName
                                    from User Where IsPortalEnabled = false AND isActive = true];
          
        for(User thisUser:lstActiveUser) {
            mapOfIdToUser.put(thisUser.Id,thisUser);
          
        }
        
        
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
    
    List<String> lstRecordType = new List<String> {'Call Back Request'};
        return Database.getQueryLocator([Select Id,Name,Account__r.Name,
                                         OwnerId,Calling_List_Type__c,Call_Outcome_CSR__c
                                        ,(Select id from call_logs__r)
                                         FROM Calling_List__c
                                         WHERE Call_Outcome_CSR__c = NULL AND Calling_List_Type__c = 'Case Summary' 
                                         AND RecordType.Name IN: lstRecordType 
                                        ]); 
    }
    
    public void execute(Database.BatchableContext BC, List<Calling_List__c> scope){        
       System.debug('mapOfIdToUser'+mapOfIdToUser);
       list<Calling_List__c> lstCL = new list<Calling_List__c>();
        CallLog_Count__c csr = CallLog_Count__c.getInstance('CBR');
        
        for(Calling_List__c thisCalling : scope) {
            system.debug('scope'+scope);
            if(thisCalling.call_logs__r.size() < csr.NoOfCallLog__c){
                sendEmail( mapOfIdToUser.get(thisCalling.OwnerId) ,thisCalling);
            }   
            
        }
        
            
        
    }
    public void finish(Database.BatchableContext BC){
        CallLog_Count__c csr = CallLog_Count__c.getInstance('CBR');
        csr.NoOfCallLog__c = csr.NoOfCallLog__c == 1?csr.NoOfCallLog__c +1:
                            csr.NoOfCallLog__c == 2?csr.NoOfCallLog__c +1:
                            csr.NoOfCallLog__c == 3?1:1;
        update csr;
    }
    
    public  void sendEmail( User ObjUser,Calling_List__c objCase ) {
        List<Attachment> lstAttach = new List<Attachment>();
        List<EmailMessage> lstEmails = new List<EmailMessage>();
        

        EmailTemplate emailTemplateObj = [SELECT Id,Name
                                                , Subject
                                                , Body
                                                , HtmlValue
                                                , TemplateType
                                                , BrandTemplateId
                                                FROM EmailTemplate 
                                                WHERE Name = 'Call back Reminder' LIMIT 1 ];
                                                
        // TODO Added to have attachment related to email template

        Attachment[] attachmentLst = [ SELECT Id,ContentType,Name, Body  From Attachment where parentId = : emailTemplateObj.Id];
        lstAttach.addAll(  attachmentLst );

        String toAddress = '', fromAddress,replyToAddress = '', contentType, contentValue,contentBody = '';
        String subject, strAccountId, strDPIId, bccAddress ='', strCCAddress;
        
        //strAccountId = objCase.AccountId;

        
        toAddress = (ObjUser != NULL && String.isNotBlank(ObjUser.Email))? ObjUser.Email:
                     '';
        
        System.debug('toAddress = ' + toAddress);
        // need to verify 
        fromAddress = 'noreply@damacgroup.com';
        bccAddress = Label.Sf_Copy_Bcc_Mail_Id;
        contentType = 'text/html';
        strCCAddress = '';
        system.debug(' emailTemplateObj : '+ emailTemplateObj );
        if( toAddress != '' && emailTemplateObj != null ) {
            
            if(emailTemplateObj.body != NULL){
                contentBody =  emailTemplateObj.body;
            }
            if(emailTemplateObj.htmlValue != NULL){
                contentValue = emailTemplateObj.htmlValue;
            }
            if(string.isblank(contentValue)){
                contentValue='No HTML Body';
            }
            subject = emailTemplateObj.Subject != NULL ? emailTemplateObj.Subject : 'Social Media Escalation';
           //Message__c,CaseNumber,FirstName LastName
            String userName = (ObjUser != NULL && String.isNotBlank(ObjUser.FirstName) && String.isNotBlank(ObjUser.LastName))?
                                ObjUser.FirstName+' '+ObjUser.LastName:
                                 '';
            contentValue = String.isNotBlank(userName)?contentValue.replace('Team', userName): contentValue.replace('Team', 'Team');
            contentValue = String.isNotBlank(objCase.Account__r.Name)?contentValue.replace('{CxName}', objCase.Account__r.Name):contentValue.replace('{CxName}', '');
            contentValue = String.isNotBlank(objCase.Name)?contentValue.replace('{CLName}', objCase.Name):contentValue.replace('{CLName}', '');
                
               
            contentBody = String.isNotBlank(userName)?contentBody.replace('Team', userName):contentBody.replace('Team', 'Team');
            contentBody = String.isNotBlank(objCase.Account__r.Name)?contentBody.replace('{CxName}', objCase.Account__r.Name):contentBody.replace('{CxName}', '');
            contentBody = String.isNotBlank(objCase.Name)?contentBody.replace('{CLName}', objCase.Name):contentBody.replace('{CLName}', '');
            
                // Callout to sendgrid to send an email
                SendGridEmailService.SendGridResponse objSendGridResponse =
                    SendGridEmailService.sendEmailService(
                        toAddress
                        , ''
                        , strCCAddress
                        , ''
                        , bccAddress
                        , ''
                        , subject
                        , ''
                        , fromAddress
                        , ''
                        , replyToAddress
                        , ''
                        , contentType
                        , contentValue
                        , ''
                        , lstAttach
                    );

                System.debug('objSendGridResponse == ' + objSendGridResponse);
                //Create Email Activity
                String responseStatus = Test.isRunningTest() ? 'Accepted' : objSendGridResponse.ResponseStatus;
                if (responseStatus == 'Accepted') {
                    EmailMessage mail = new EmailMessage();
                    mail.Subject = subject;
                    mail.MessageDate = System.Today();
                    mail.Status = '3';//'Sent';
                    //mail.RelatedToId = strAccountId;
                    //mail.Account__c  = strAccountId;
                    mail.Type__c = 'CSR notification';
                    mail.ToAddress = toAddress;
                    mail.FromAddress = fromAddress;
                    mail.TextBody = contentBody; //contentValue.replaceAll('\\<.*?\\>', '');
                    mail.Sent_By_Sendgrid__c = true;
                    mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                    mail.CcAddress = strCCAddress;
                    mail.BccAddress = bccAddress;
                    lstEmails.add(mail);
                    system.debug('Mail obj == ' + mail);


                }//End Email Activity Creation
                
        }//End Payment details null check if
    }//End emailTemplateObj IF

    
    
}