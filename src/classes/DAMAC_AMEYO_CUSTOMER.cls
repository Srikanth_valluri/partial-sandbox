/*
* Description : Used to get the Customer Id from AMeyo based on Inquiry information and also sending the callback details.
*/

public class DAMAC_AMEYO_CUSTOMER {

    public static Integer attempts = 1;
    // Method to get the session ID from Ameyo
    public static String loginAmeyo () {
        Ameyo_Credentials__c credentials = Ameyo_Credentials__c.getInstance(UserInfo.getUserID());
        String sessionId = '';
        
        HTTPRequest req = new HTTPRequest ();
        req.setEndpoint (credentials.Endpoint__c+'?command=force-login&data='+EncodingUtil.urlEncode('{"userId":"'+credentials.User_Name__c+'","password":"'+credentials.Password__c+'"}', 'UTF-8'));
        req.setHeader ('Content-Type', 'application/json');
        req.setTimeout (120000);
        req.setMethod ('POST');
        
        HTTP http = new HTTP ();
        HTTPResponse res = new HTTPResponse ();
        if (!Test.isRunningTest ())
            res = http.send (req);
        else {
            res.setStatusCode (200);
            res.setBody ('{"sessionId" : "test"}');
        }
        System.Debug ('::: Response ::::'+res.getBody());
        if (res.getStatusCode () == 200) {
            Map <String, Object> response = (Map<String, Object>) JSON.deserializeUntyped (res.getBody());
            sessionId = String.valueOf (response.get ('sessionId'));
        }
        if (sessionId == '' || sessionId == NULL) {
            attempts = attempts+1;
            
            if (attempts <= 3)
                return loginAmeyo ();
            else {
                return sessionId;
            }
            
        } else {
            return sessionId;
        }
    }
    
    
    // Method to check whether the session is active or not, if the session is not active then call the login method to get the new session
    public static String checkSession () {
        string sessionId;
        sessionId = loginAmeyo ();            
        
        return sessionId;

    }

    // Invocable method to get the customer id using process builder
    @InvocableMethod()
    Public static void setCustomerManager (list<id> inquiryIds){
        setCustomerManagerAsync (inquiryIds);
    }
    
    
    
    // Async transaction to get the customer id and sending the callback info
    @Future(Callout = TRUE)
    Public static void setCustomerManagerAsync (list<id> inquiryIds){
           
        Ameyo_Credentials__c credentials = Ameyo_Credentials__c.getInstance(UserInfo.getUserID());
       
        // For Customer Manager API 
        DAMAC_AMEYO_CUSTOMER_JSON obj = new DAMAC_AMEYO_CUSTOMER_JSON();
        obj.campaignId = credentials.Callback_campaign_id__c;
        obj.sessionId = checkSession (); // To get the active session from Ameyo
        obj.numAttempts = String.valueOf (inquiryIds.size ());
        obj.Status = 'NOT_TRIED';
        
        DAMAC_AMEYO_CUSTOMER_JSON.cls_properties prop = new DAMAC_AMEYO_CUSTOMER_JSON.cls_properties ();
        prop.update_customer = true;
        prop.migrate_customer = true;        
        obj.properties = prop;
        
        List <DAMAC_AMEYO_CUSTOMER_JSON.cls_customerRecords> records = new List <DAMAC_AMEYO_CUSTOMER_JSON.cls_customerRecords > ();
        
        // For Callback API
        DAMAC_AMEYO_CALLBACK_JSON callBackObj = new DAMAC_AMEYO_CALLBACK_JSON();
        
        callBackObj.campaignId = credentials.Callback_campaign_id__c != NULL ? Integer.valueOf(credentials.Callback_campaign_id__c) : NULL;
        callBackObj.sessionId = obj.sessionId;
        callBackObj.isSelfCallBack = 'false';
        callBackObj.callBackHandlerType = 'voice.campaign.callback.handler';
        callBackObj.userId = 'apiuser';    
        //callBackObj.queueId = 124;
                
        
        DAMAC_AMEYO_CALLBACK_JSON.cls_callBackProperties callBackProp = new DAMAC_AMEYO_CALLBACK_JSON.cls_callBackProperties();
        
        Set <ID> ownerIds = new Set <ID> ();
        for (Inquiry__c inq :[SELECT OwnerId FROM Inquiry__c WHERE id in :inquiryIds]){
            ownerIds.add (inq.OwnerId);
        }
        Map <ID, User> ownerDetails = new Map <ID, User>([SELECT FederationIdentifier FROM User WHERE ID IN: ownerIds]);
        
        for (Inquiry__c inq:[SELECT Full_Name__c, Owner.Name, Is_Owner_Queue__c,Ameyo_Callback_Id__c, //Owner_Ameyo_lead_id__c 
                            Ameyo_Customer_Id__c, Call_back_Date_time__c,
                            Ameyo_Campaign_Id__c, Encrypted_Mobile__c FROM Inquiry__c WHERE id in :inquiryIds]){
            
            // Customer Manager API
            DAMAC_AMEYO_CUSTOMER_JSON.cls_customerRecords rec = new DAMAC_AMEYO_CUSTOMER_JSON.cls_customerRecords ();
            rec.id = inq.Id;
            rec.phone1 = inq.Encrypted_Mobile__c;
            rec.name = inq.Full_Name__c;
            /*
            obj.leadId = NULL;
            
            if (inq.Is_Owner_Queue__c && inq.Owner.Name == 'Ameyo Dialer Queue') {
                if (inq.Ameyo_Campaign_Id__c != NULL)
                    obj.LeadId = Integer.valueOf(inq.Ameyo_Campaign_Id__c);
            } else {
                if (inq.Owner_Ameyo_lead_id__c != NULL)
                    obj.leadId = Integer.valueOf(inq.Owner_Ameyo_lead_id__c);
            }
            obj.leadId = 1; //119 for testing
            */
            obj.leadId = integer.valueof(credentials.Customer_Create_Update_Lead_Id__c);
            records.add (rec);
            
            // Callback API
            if (ownerDetails.containsKey (inq.OwnerId)) {
                /*if (ownerDetails.get (inq.OwnerId).FederationIdentifier != NULL)
                    callBackObj.userId = ownerDetails.get (inq.OwnerId).FederationIdentifier+'@damacgroup.com';*/
                    //callBackObj.userId = NULL;
            }
            if (inq.Call_back_Date_time__c != NULL){
                //callBackObj.callBackTime = callbackDateTime(inq.Call_back_Date_time__c.addMinutes(5));
                datetime dt = inq.Call_back_Date_time__c.addMinutes(integer.valueof(credentials.callback_in_minutes__c));
                string  st=dt.format('dd-MM-yyyy HH:mm:ss');
                system.debug(st);
                callBackObj.callBackTime = st;
            }
            /*
            else {
                datetime dt = DateTime.Now().addMinutes(integer.valueof(credentials.callback_in_minutes__c));
                string  st=dt.format('dd-MM-yyyy HH:mm:ss');
                system.debug(st);
                callBackObj.callBackTime = st;
            }
            */
            callBackProp.customerId = inq.Id+'_Callback';
            callBackProp.phone = inq.Encrypted_Mobile__c;
        }
        
        callBackObj.callBackProperties = callBackProp;
        system.debug('--->>>obj='+callBackObj);
        obj.customerRecords = records;
        
        String callbackReqBody = JSON.serialize (callBackObj);
        String reqBody = JSON.serialize (obj);
        reqBody = reqBody.replace ('update_customer', 'update.customer');
        reqBody = reqBody.replace ('migrate_customer', 'migrate.customer');
        System.Debug ('::: Customer Insert/Update Body to Send :::'+reqBody);    
        System.Debug('::: Callback :::'+callBackReqBody);
        
        // Http Request to get the Customer id based on the inquiry information
        HTTPRequest req = new HTTPRequest ();
        req.setEndpoint (credentials.Endpoint__c+'?command=uploadContacts');
        req.setHeader ('Content-Type', 'application/x-www-form-urlencoded');
        req.setHeader ('hash-key', credentials.hash_key__c);
        req.setHeader ('Host', credentials.host__c);
        req.setHeader ('policy-name', credentials.policy_name__c);
        req.setHeader ('requesting-host', credentials.requesting_host__c);
        req.setTimeout (120000);
        req.setMethod ('POST');
        req.setBody ('data='+EncodingUtil.urlEncode(reqBody, 'UTF-8'));
                
        HTTP http = new HTTP ();
        HTTPResponse res = new HTTPResponse ();
        if (!Test.isRunningTest ()){
            res = http.send (req);
        }
        
        System.Debug ('Customer Insert/update Response=====>'+res.getStatusCode ()+' :::: '+res.getBody());
        
        String responseBody = '';
        
        if (!Test.isRunningTest ()) {
            responseBody = res.getBody ();
        } else {
            responseBody = '{"beanResponse":[{"inputCustomerRecord":'
                +'{"name":"Amit4","id":"123","phone1":"7838735378"},"inserted":true,'
                +'"customerId":4148793,"resultTypeString":"UPDATED","crmIntegrated":false,'
                +'"crmSuccess":false},{"inputCustomerRecord":{"name":"Ashu2","id":"123",'
                +'"phone1":"9773925451"},"inserted":false,"exception":"Duplicate key 123",'
                +'"resultTypeString":"DUPLICATE_NOT_ADDED","crmIntegrated":false,"crmSuccess":false}]}';
            res.setStatusCode (200);
        }
        if (responseBody != '' && res.getStatusCode () == 200) {
        
            Map <String, String> customerIds = new Map <String, String> ();
            DAMAC_AMEYO_CUSTOMER_RESP_JSON respObj = DAMAC_AMEYO_CUSTOMER_RESP_JSON.parse (responseBody);
            
            for (DAMAC_AMEYO_CUSTOMER_RESP_JSON.cls_beanResponse resp : respObj.beanResponse) {
                if (resp.customerId != NULL) {
                    String customerId = String.valueOf (resp.customerId);
                    String inqId = resp.inputCustomerRecord.id;
                    customerIds.put (inqId, customerId);
                    callbackReqBody = callbackReqBody.replace(inqId+'_Callback', customerId);
                }
            }
            
            
            // HTTP request to send the callback information.
            HTTPRequest callBackReq = new HTTPRequest ();
            callBackReq.setEndpoint (credentials.Endpoint__c+'?command=addCallback');
            callBackReq.setHeader ('Content-Type', 'application/x-www-form-urlencoded');
            callBackReq.setHeader ('hash-key', credentials.hash_key__c);
            callBackReq.setHeader ('Host', credentials.host__c);
            callBackReq.setHeader ('policy-name', credentials.policy_name__c);
            callBackReq.setHeader ('requesting-host', credentials.requesting_host__c);
            callBackReq.setTimeout (120000);
            callBackReq.setMethod ('POST');
            System.debug ('Request Body::::'+callbackReqBody);
            callBackReq.setBody ('data='+EncodingUtil.URLEncode(callbackReqBody, 'UTF-8'));
            
            HTTPResponse callbackRes = new HTTPResponse ();
            if (!Test.isRunningTest ()){
                callbackRes = http.send (callBackReq);
            }else {
                res.setStatusCode (200);
                res.setBody ('{"beanResponse":{"id":"d462-5dbfbbdb-cm-23599","campaignId":12,"callBackTime":"2019-11-27 09:59:51.617","selfCallback":false,"callBackProperties":{"customerId":"8645996","phone":"dcecc9c3a4c0f068e3bacc75b450d45e"},"userId":"apiuser","phone":"dcecc9c3a4c0f068e3bacc75b450d45e","customerId":8645996}}');
            }
            
            System.Debug ('::::: Call Back Response::::'+callbackRes.getStatusCode()+'::::'+callbackRes.getBody());
            if (customerIds.keySet ().size () > 0) {
                List <Inquiry__c> inqToUpdate = new List <Inquiry__c> ();
                for (Inquiry__c inq :[SELECT Ameyo_Customer_Id__c FROM Inquiry__c WHERE ID IN :customerIds.keySet ()]) {
                    inq.Ameyo_Customer_Id__c = customerIds.get (inq.id);
                    if (callBackRes.getStatusCode () == 200){ 
                        DAMAC_AMEYO_CALLBACK_RESPONSE respcls = DAMAC_AMEYO_CALLBACK_RESPONSE.PARSE(callbackRes.getBody());     
                        system.debug(respcls);                  
                        system.debug('Callback Response Id----->'+respcls.BeanResponse.id);
                        inq.Ameyo_Request_Status__c = 'Contact Insert or Uplaod== '+res.getBody()+'--Callback API Resp-'+callbackRes.getBody();
                        inq.Ameyo_Callback_Id__c = respcls.BeanResponse.id;
                        inqToUpdate.add (inq);
                    }
                }
                if (InqToUpdate.size () > 0) {
                    DAMAC_Constants.skip_InquiryTrigger = true;
                    Update inqToUpdate;
                }
            }
        }
        if (obj.sessionId != '') {
            Ameyo_Credentials__c cred = [SELECT Session_Id__c FROM Ameyo_Credentials__c LIMIT 1];
            if (obj.sessionId != cred.Session_Id__c && obj.sessionId != NULL) {
                cred.Session_Id__c = obj.sessionId;
                update cred;
            }
        }
    }
    
    //Delete Callback
    @Future(Callout = TRUE)
    Public static void DeleteCallback(list<id> inquiryIds){
        list<inquiry__c> callbackDeleteInq = new list<inquiry__c>();
        callbackDeleteInq = [select id,Ameyo_Callback_Id__c,Ameyo_Request_Status__c from inquiry__c where id in:inquiryIds and Ameyo_Callback_Id__c!=null];
        if(callbackDeleteInq.size() > 0){
            Ameyo_Credentials__c credentials = Ameyo_Credentials__c.getInstance(UserInfo.getUserID());
            string sessionId = checkSession();
            HTTPRequest req = new HTTPRequest ();
            req.setEndpoint (credentials.Delete_Callback_Endpoint__c+callbackDeleteInq[0].Ameyo_Callback_Id__c);
            req.setHeader ('Content-Type', 'application/json');
            req.setHeader ('sessionId', sessionId );        
            req.setTimeout (120000);
            req.setMethod ('DELETE');
            HTTP http = new HTTP ();
            HTTPResponse res = new HTTPResponse ();
            if (!Test.isRunningTest ()){
                res = http.send (req);
            }
            
            System.Debug ('Callback Removal Response=====>'+res.getStatusCode ()+' :::: '+res.getBody());
            if(res.getStatusCode () == 200 || res.getStatusCode ()==201){
                callbackDeleteInq[0].Ameyo_Request_Status__c = callbackDeleteInq[0].Ameyo_Request_Status__c+'At '+system.now()+' Deleted Callback for Id='+ callbackDeleteInq[0].Ameyo_Callback_Id__c ;
                callbackDeleteInq[0].Ameyo_Callback_Id__c = null;                
                update callbackDeleteInq[0];
            }
        }
    }
   
}