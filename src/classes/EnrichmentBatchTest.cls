@isTest
public class EnrichmentBatchTest {
    @isTest
    static void testEnrichmentBatch(){
    List<Account> accountList = new List<Account>();
    Account objAccount = TestDataFactory_CRM.createPersonAccount();
    objAccount.Nationality__c = 'UAE';
    objAccount.Email__pc = 'abc@z.com';
    objAccount.Party_ID__c = '12345';
    objAccount.Active_Customer__c = 'Active';
    insert objAccount ;
    accountList.add(objAccount);
    
    Credentials_Details__c objCredDetail = new Credentials_Details__c();
    objCredDetail.Name = 'Enrichment';
    objCredDetail.Endpoint__c = 'https://damaccustenrich.servicebus.windows.net/customer-enrichment-hub-prod/messages';
    objCredDetail.Password__c = 'test';
    objCredDetail.User_Name__c = 'test';
    insert objCredDetail;
    
    Test.startTest();
        EnrichmentBatch objEnrichment = new EnrichmentBatch();
        DataBase.executeBatch(objEnrichment,1);
        
        EnrichmentBatchScheduler objEnrichmentScheduler = new EnrichmentBatchScheduler();
        String sch ='0 48 * * * ?'; 
        System.schedule('Schedule to update Account', sch,objEnrichmentScheduler);
    Test.stopTest();

    }
}