/*---------------------------------------------------------------------------------------------------------------------
Description: 
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By      | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     | 08-11-2020        | Aishwarya Todkar      | Updated complaint source, owner and origin
=======================================================================================================================*/
@RestResource(urlMapping='/SaveComplaintFromMobileApp/*')
Global class SaveComplaintFromMobileApp {
      @HtTPPost
    Global static Case SaveComplaint(SaveComplaintWrapper saveComplaintWrapper)
    {
         Case complaintCaseObj = new Case(Type=ComplaintProcessUtility.COMPLAINT_CASE_TYPE
                                  ,RecordTypeId=ComplaintProcessUtility.getRecordTypeId()
                                  ,Status = saveComplaintWrapper.status
                                  ,Origin = saveComplaintWrapper.origin);
		
		Group g =[select Id,name from Group where Type = 'Queue' and name='HappinessTeam'];
		list<Booking_Unit__c> bu;
        if(!String.isBlank(saveComplaintWrapper.BookingUnit))
          bu=[select id,Registration_ID__c,name,Unit_Name__c from Booking_Unit__c where Unit_Name__c=:saveComplaintWrapper.BookingUnit];
        complaintCaseObj.AccountId=saveComplaintWrapper.AccountID;
        complaintCaseObj.Description=saveComplaintWrapper.Description;
        complaintCaseObj.Complaint_Type__c=saveComplaintWrapper.ComplaintType;
        complaintCaseObj.Complaint_Sub_Type__c=saveComplaintWrapper.ComplaintSubType;
        complaintCaseObj.Additional_Doc_File_URL__c  = saveComplaintWrapper.Attachment1Url;
        complaintCaseObj.OD_File_URL__c  = saveComplaintWrapper.Attachment2Url;
        if(bu.size()>0)
        complaintCaseObj.Booking_Unit__c  = bu[0].id;
		
		complaintCaseObj.OwnerId =g.id;
		complaintCaseObj.id=saveComplaintWrapper.salesforceId;
		complaintCaseObj.Complaint_Source__c='DAMAC Living';
		complaintCaseObj.Complaint_Sub_Source__c='DAMAC Living Mob App';
		complaintCaseObj.origin = 'Mobile App';
        upsert complaintCaseObj;
        
      // PushNotificationToMobileApp.PushNotificationSend(complaintCaseObj.CaseNumber,saveComplaintWrapper.fcm);
     /*   Case caseList = [Select id,OwnerId, Status, Origin, Additional_Doc_File_URL__c from Case Where id = :complaintCaseObj.id];
          system.debug('caseList>> '+caseList);
          Task objTask = new Task();
                try{
                objTask = TaskUtility.getTask((SObject)caseList, 'New Complaint Request', 'CRE', 
                               'Complaint', system.today().addDays(1));
              //  objTask.Parent_Task_Id__c=complaintCaseObj.id;
                objTask.OwnerId = Label.DefaultCaseOwnerId;
                objTask.Priority = 'High';
                objTask.Status = 'In Progress';
                objTask.currencyISOcode = caseList.currencyISOcode ;
                system.debug('--objTask--Promotions'+objTask);
                
                  insert objTask;
                  system.debug('>>>>>>>objTask'+objTask);
                }  
                catch(exception e){
                  System.debug('task exception '+ e);
                }  */
                
                
                
                   Case caseList = [Select id,OwnerId, Status, Origin, Additional_Doc_File_URL__c from Case Where id = :complaintCaseObj.id];
         
         Task objTask = new Task();
                try{
					objTask = TaskUtility.getTask((SObject)caseList, 'New Complaint Request', 'CRE', 
								'Complaint', system.today().addDays(1));
				//  objTask.Parent_Task_Id__c=complaintCaseObj.id;
					objTask.OwnerId = Label.DefaultCaseOwnerId;
					objTask.Priority = 'High';
					objTask.Status = 'In Progress';
				// objTask.currencyISOcode = caseList.currencyISOcode ;
					system.debug('--objTask--Promotions'+objTask);
					
					//insert objTask;
					system.debug('>>>>>>>objTask'+objTask);
					
					List<SR_Attachments__c> ListAttach = new List<SR_Attachments__c>();
					if(String.isNotBlank(complaintCaseObj.Additional_Doc_File_URL__c))
					{
						SR_Attachments__c srAttchmentObj = new SR_Attachments__c(Case__c = complaintCaseObj.id,Account__c = complaintCaseObj.AccountId,Type__c = extractType(complaintCaseObj.Additional_Doc_File_URL__c),Name= 'Additional Document '+ System.today(), Attachment_URL__c =  complaintCaseObj.Additional_Doc_File_URL__c);
						ListAttach.add( srAttchmentObj );
					}
					if(String.isNotBlank(complaintCaseObj.OD_File_URL__c))
					{
						SR_Attachments__c srAttchmentObj = new SR_Attachments__c(Case__c = complaintCaseObj.id,Account__c = complaintCaseObj.AccountId,Type__c = extractType(complaintCaseObj.OD_File_URL__c),Name= 'Customer Signed CRF Form '+ System.today(), Attachment_URL__c = complaintCaseObj.OD_File_URL__c);
						ListAttach.add( srAttchmentObj );
					}
					insert ListAttach;
                }  
                catch(exception e){
                  System.debug('task exception '+ e);
                } 
                                                           
        return complaintCaseObj;
    }
    
       public static String extractType( String strName ) {
        strName = strName.substring( strName.lastIndexOf('\\')+1 );
        return strName.substring( strName.lastIndexOf('.')+1 ) ;
    }
    
    global class SaveComplaintWrapper{
        public String userId;
        public String BookingUnit;
        public String AccountID;
        public String Attachment1Url;  
        public String Attachment2Url;
        public String Description;
        public String ComplaintType;
        public String ComplaintSubType;
          public String status;
          public String origin;
          public String fcm;
           public String salesforceId;
      
    }
    
}