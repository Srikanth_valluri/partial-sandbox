/*
Created Date: 29/06/2020
Batch Class: Damac_DocumentTobeUpdatedSchedule
Test Class: Damac_DocumentTobeUpdatedSchedule_Test
Description: To send the email notification to the account owner for the documents to be updated
*/
global class Damac_DocumentTobeUpdatedSchedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        String emails = Label.Document_notification;
        List <String> emailAddress = new List <String> ();
        if (emails.contains (',')) {
            emailAddress = emails.split (',');
        } else {
            emailAddress.add (emails);
        }
        
        Map<String, User> userMap = new Map <String, User> ();
        for (User u : [SELECT Name, Email FROM User WHERE Email IN: emailAddress]) {
            userMap.put (u.Email, u);
        }
        EmailTemplate template = [SELECT Subject, HTMLValue FROM EmailTemplate WHERE developerName = 'Documents_to_be_updated'];
        
        for (String toEmail : emailAddress) {
            String emailBody = template.HTMLValue;
            String subject = template.subject;
            emailBody = emailBody.replace ('{!User.Name}', userMap.get (toEmail).Name);
            sendSMS (toEmail, subject, emailBody);
        }
        
        
    }
    
    global void sendSMS (String emailAddress, String subject, String emailBody) {
        
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        mail.setToAddresses(new List <String> {emailAddress});        
        mail.setSenderDisplayName('DAMAC Properties');
        mail.setSubject (subject);
        mail.setHtmlBody(emailBody);   
        mails.add(mail);
        Messaging.sendEmail(mails);
    }
}