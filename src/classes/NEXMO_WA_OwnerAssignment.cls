/*

    Class Name : NEXMO_WA_OwnerAssignment 
    Description : To assign owner for WA record in round robin based on NEXMO_WA_Owner_Assignment__c (custom settings) order.

*/

public class NEXMO_WA_OwnerAssignment {
    // Assigning owner to the whatsapp record
    public static Id assignOwner () {
        Id ownerId = null;
        Set <ID> userIds = new Set <ID> ();
        for (NEXMO_WA_Owner_Assignment__c rec : [SELECT Owner_Id__c FROM NEXMO_WA_Owner_Assignment__c WHERE Owner_Id__c != NULL]) {
            userIds.add (rec.Owner_Id__c);
        }
        if (userIds.size () > 0) {
            Map <ID, User> users = new Map <Id, User> ([SELECT Name, isActive FROM User WHERE ID IN: userIds AND ISActive = TRUE]);
            List <NEXMO_WA_Owner_Assignment__c> settingsToUpdate = new List <NEXMO_WA_Owner_Assignment__c> ();
            for (NEXMO_WA_Owner_Assignment__c rec : [SELECT Is_Assigned__c, Order__c, Owner_Id__c FROM NEXMO_WA_Owner_Assignment__c 
                                                    WHERE Owner_Id__c != NULL 
                                                    AND Owner_Id__c IN: users.keySet() 
                                                    AND Is_Assigned__c = FALSE
                                                    Order BY Order__c ASC]) 
            {
                ownerId = rec.Owner_Id__c;
                rec.Is_Assigned__c = True;
                settingsToUpdate.add(rec);
                break;
            }
            if (ownerId == null || Test.isRunningTest()) {
                Integer i = 0;
                for (NEXMO_WA_Owner_Assignment__c rec : [SELECT Is_Assigned__c, Order__c, Owner_Id__c FROM NEXMO_WA_Owner_Assignment__c 
                                                    WHERE Owner_Id__c != NULL 
                                                    AND Owner_Id__c IN: users.keySet() 
                                                    Order BY Order__c ASC]) 
                {
                    
                    if (i == 0) {
                        rec.Is_Assigned__c = TRUE;
                        ownerId = rec.Owner_Id__c;
                        i += 1;
                    } else {
                        rec.Is_Assigned__c = FALSE;                        
                    }
                    settingsToUpdate.add (rec);                              
                }
            }
            if(settingsToUpdate.size() > 0){
            	update settingsToUpdate;
            }
            
        }
        System.debug('ownerId'+ownerId);
        return ownerId;
    }
}