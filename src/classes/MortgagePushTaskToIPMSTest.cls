@isTest
private class MortgagePushTaskToIPMSTest
{

    private static Id getRecordTypeId(){
        return Schema.SObjectType.Case.getRecordTypeInfosByName().get('Mortgage').getRecordTypeId();
    }

    @isTest static void TestMethod1() {
        Account accObj = TestDataFactory_CRM.createPersonAccount();
        insert accObj;


        Case caseObj = TestDataFactory_CRM.createCase(accObj.Id, getRecordTypeId());
        caseObj.POA_Expiry_Date__c = System.today();
        insert caseObj;

        List<Task> taskList = new List<Task>();

        Task taskObj1 = new Task();
        taskObj1.WhatId = caseObj.Id;
        taskObj1.Priority = 'High';
        taskObj1.ActivityDate = System.Today();
        taskObj1.Status = 'In Progress';
        taskObj1.Subject = 'Verify POA Documents For Mortgage';
        taskObj1.Assigned_User__c = 'CRE';
        taskObj1.Process_Name__c = 'Mortgage';
        taskObj1.Task_Due_Date__c = System.Today();
        taskList.add(taskObj1);


        Task taskObj2 = new Task();
        taskObj2.WhatId = caseObj.Id;
        taskObj2.Priority = 'High';
        taskObj2.ActivityDate = System.Today();
        taskObj2.Status = 'In Progress';
        taskObj2.Subject = System.label.Mortgage_15;
        taskObj2.Assigned_User__c = 'CRE';
        taskObj2.Process_Name__c = 'Mortgage';
        taskObj2.Task_Due_Date__c = System.Today();
        taskList.add(taskObj2);

        Task taskObj3 = new Task();
        taskObj3.WhatId = caseObj.Id;
        taskObj3.Priority = 'High';
        taskObj3.ActivityDate = System.Today();
        taskObj3.Status = 'In Progress';
        taskObj3.Subject = System.label.Mortgage_16;
        taskObj3.Assigned_User__c = 'CRE';
        taskObj3.Process_Name__c = 'Mortgage';
        taskObj3.Task_Due_Date__c = System.Today();
        taskList.add(taskObj3);

        Task taskObj4 = new Task();
        taskObj4.WhatId = caseObj.Id;
        taskObj4.Priority = 'High';
        taskObj4.ActivityDate = System.Today();
        taskObj4.Status = 'In Progress';
        taskObj4.Subject = System.label.Mortgage_27;
        taskObj4.Assigned_User__c = 'CRE';
        taskObj4.Process_Name__c = 'Mortgage';
        taskObj4.Task_Due_Date__c = System.Today();
        taskList.add(taskObj4);

        Task taskObj5 = new Task();
        taskObj5.WhatId = caseObj.Id;
        taskObj5.Priority = 'High';
        taskObj5.ActivityDate = System.Today();
        taskObj5.Status = 'In Progress';
        taskObj5.Subject = System.label.Mortgage_28;
        taskObj5.Assigned_User__c = 'CRE';
        taskObj5.Process_Name__c = 'Mortgage';
        taskObj5.Task_Due_Date__c = System.Today();
        taskList.add(taskObj5);

        Task taskObj6 = new Task();
        taskObj6.WhatId = caseObj.Id;
        taskObj6.Priority = 'High';
        taskObj6.ActivityDate = System.Today();
        taskObj6.Status = 'In Progress';
        taskObj6.Subject = System.label.Mortgage_29;
        taskObj6.Assigned_User__c = 'CRE';
        taskObj6.Process_Name__c = 'Mortgage';
        taskObj6.Task_Due_Date__c = System.Today();
        taskList.add(taskObj6);

        Task taskObj7 = new Task();
        taskObj7.WhatId = caseObj.Id;
        taskObj7.Priority = 'High';
        taskObj7.ActivityDate = System.Today();
        taskObj7.Status = 'In Progress';
        taskObj7.Subject = System.label.Mortgage_30;
        taskObj7.Assigned_User__c = 'CRE';
        taskObj7.Process_Name__c = 'Mortgage';
        taskObj7.Task_Due_Date__c = System.Today();
        taskList.add(taskObj7);

        Task taskObj8 = new Task();
        taskObj8.WhatId = caseObj.Id;
        taskObj8.Priority = 'High';
        taskObj8.ActivityDate = System.Today();
        taskObj8.Status = 'In Progress';
        taskObj8.Subject = System.label.Mortgage_17;
        taskObj8.Assigned_User__c = 'CRE';
        taskObj8.Process_Name__c = 'Mortgage';
        taskObj8.Task_Due_Date__c = System.Today();
        taskList.add(taskObj8);

        Task taskObj9 = new Task();
        taskObj9.WhatId = caseObj.Id;
        taskObj9.Priority = 'High';
        taskObj9.ActivityDate = System.Today();
        taskObj9.Status = 'In Progress';
        taskObj9.Subject = System.label.Mortgage_22;
        taskObj9.Assigned_User__c = 'CRE';
        taskObj9.Process_Name__c = 'Mortgage';
        taskObj9.Task_Due_Date__c = System.Today();
        taskList.add(taskObj9);     

        
        Task taskObj10 = new Task();
        taskObj10.WhatId = caseObj.Id;
        taskObj10.Priority = 'High';
        taskObj10.ActivityDate = System.Today();
        taskObj10.Status = 'In Progress';
        taskObj10.Subject = System.label.Mortgage_23;
        taskObj10.Assigned_User__c = 'CRE';
        taskObj10.Process_Name__c = 'Mortgage';
        taskObj10.Task_Due_Date__c = System.Today();
        taskList.add(taskObj10);    

        Task taskObj11 = new Task();
        taskObj11.WhatId = caseObj.Id;
        taskObj11.Priority = 'High';
        taskObj11.ActivityDate = System.Today();
        taskObj11.Status = 'In Progress';
        taskObj11.Subject = System.label.Mortgage_31;
        taskObj11.Assigned_User__c = 'CRE';
        taskObj11.Process_Name__c = 'Mortgage';
        taskObj11.Task_Due_Date__c = System.Today();
        taskList.add(taskObj11);        
        
        insert taskList;



    
        
        Test.startTest();
            Test.setMock(WebServiceMock.class, new MortgageWebServiceCalloutMock());
            MortgagePushTaskToIPMS.PushTasktoIPMS(taskList);
        Test.stopTest();

        //Error_Log__c errorLogObj = MortgagePushTaskToIPMS.createErrorLogRecord( accObj.Id, caseObj.Id);
        MortgagePushTaskToIPMS.insertErrorLog(new List<Error_Log__c>{  MortgagePushTaskToIPMS.createErrorLogRecord( accObj.Id, caseObj.Id) } );
    }
}