/*
    Created By              : Srikanth V
    Created Date            : 09/03/2020
    Desription              : 
    
*/

Public class Damac_PromotionsService {

    public static Map <String, List <Promotions>> promotionsRequest (String unitCodes) {
        
        unitCodes = unitCodes.removeEnd (',');
        
        IPMS_Integration_Settings__mdt settings = [SELECT Client_ID__c, Endpoint_URL__c 
                                                    FROM IPMS_Integration_Settings__mdt 
                                                    WHERE DeveloperName = 'Promotions'
                                                    LIMIT 1];
        
        HttpRequest req = new HttpRequest ();
        String token = DAMAC_IPMS_PARTY_CREATION.getIPMSAccessToken();
        
        
        req.setEndpoint (settings.Endpoint_URL__c +unitCodes);
        req.setMethod('GET');
        req.setHeader('Authorization', 'Bearer '+token);
        req.setHeader('Content-Type','application/json');
        req.setTimeout(120000);    
        
        Http http = new HTTP ();
        HTTPResponse res = new HTTPResponse ();
        if (!Test.isRunningTest())
            res = http.send (req);
            
        System.debug (res.getBody ());
        
        Map <String, List <Promotions>> responseMap = new Map <String, List <Promotions>> ();
        if (Test.isRunningTest()) {
            res.setBody ('{"responseLines": [{"promotionId": "124","promotion": "50% Reg Promo","startDate": "02-Feb-2020","endDate": "28-Feb-2020"}]}');        
        }
        
        DAMAC_PromotionsRESP response = DAMAC_PromotionsRESP.parse(res.getBody ());
        if (response.responseLines != null) {
            for (DAMAC_PromotionsRESP.cls_responseLines line :response.responseLines) {
                Promotions obj = new Promotions ();
                obj.promotionId = line.promotionId;
                obj.promotion = line.promotion;
                obj.startDate = line.startDate;
                obj.endDate = line.endDate;
                obj.unitCode = line.unitCode;
                obj.note = line.note;
                
                if (responseMap.containsKey (line.unitCode))
                    responseMap.get (line.unitCode).add (obj);
                else
                    responseMap.put (line.unitCode, new List <Promotions> {obj});
                    
            }
        }
        return responseMap;
        
    }
    
    public class Promotions{
        public String promotionId;
        public String promotion;
        public String startDate;
        public String endDate;
        public String unitCode;
        public String note;
        public Promotions () {
            promotionId = '';
            promotion = '';
            startDate = '';
            endDate = '';
            unitCode = '';
            note = '';
        }
    }
}