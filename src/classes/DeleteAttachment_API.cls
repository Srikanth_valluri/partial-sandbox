@RestResource(urlMapping='/deleteDocFromSF/*')
global class DeleteAttachment_API {

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        //200 => 'OK',
        //400 => 'Bad Request',
        //401 => 'Unauthorized',
        //500 => 'Internal Server Error' 
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };


    @HttpPost
    global static FinalReturnWrapper deleteAttachment() {

        System.debug('inside delete attachment method');

        FinalReturnWrapper retunResponse = new FinalReturnWrapper();
        cls_meta_data objMeta = new cls_meta_data();

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);

        if(!r.params.containsKey('attachmentId')) {

            objMeta.message = 'Missing parameter : attachmentId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            //objMeta.is_success = false;

            retunResponse.meta_data = objMeta;
            return retunResponse;

        }
        else if(r.params.containsKey('attachmentId') && String.isBlank(r.params.get('attachmentId'))) {
            objMeta.message = 'Missing parameter value: attachmentId';
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = null;
            //objMeta.is_success = false;

            retunResponse.meta_data = objMeta;
            return retunResponse;
        }

        if(r.params.containsKey('attachmentId') && String.isNotBlank(r.params.get('attachmentId'))) {

            System.debug('attachmentId:' + r.params.get('attachmentId'));

            List<SR_Attachments__c> lstAttach = [SELECT id
                                                      , Name
                                                      , Attachment_URL__c
                                                 FROM SR_Attachments__c
                                                 WHERE id=:r.params.get('attachmentId')];

            System.debug('lstAttach:: ' + lstAttach);

            if(lstAttach.size() > 0) {
                delete lstAttach;

                objMeta.message = 'Delete Successful';
                objMeta.status_code = 1;
                objMeta.title = mapStatusCode.get(1);
                objMeta.developer_message = null;
                //objMeta.is_success = true;

               
            }
            else {

                objMeta.message = 'No attachment found for given attachmentId';
                objMeta.status_code = 3;
                objMeta.title = mapStatusCode.get(3);
                objMeta.developer_message = null;
                //objMeta.is_success = false;

               

            }
        }

        retunResponse.meta_data = objMeta;
        System.debug('retunResponse: ' + retunResponse);
        return retunResponse;


    }

    
    
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }

    public class cls_data {
        public AttachmentWrapper attachment_details ;
    }

    public class cls_meta_data {
        ////public Boolean is_success;
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }

    public class AttachmentWrapper {
        public String fm_case_id;
        public String fm_case_number;
        public String attachment_id;
        public String attachment_url;
    }

}