@isTest
public class Damac_ReservationFormHelper_Test {
    
    static testMethod void reservationForm(){
        Account acc = insertAccount();
        Inquiry__c inquiry = insertInquiry();
        Form_Request__c formRequest = insertFormRequest(acc.Id, inquiry.Id);
        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c = 1;
        newProperty.Property_Code__c = 'VIR';
        newProperty.Property_Name__c = 'VIRIDIS @ AKOYA OXYGEN';
        newProperty.District__c = 'AL YUFRAH 2';
        newProperty.AR_Transaction_Type__c  = 'INV VIR';
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR';
        newProperty.Brokerage_Distribution_Set__c = '11600';
        newProperty.Sales_Commission_Dist_Set__c = '11601';
        newProperty.Currency_Of_Sale__c = 'AED';
        newProperty.Signature_Col_Customer_Stmt__c = 'Front Line Investment Management Co. LLC';
        newProperty.EOI_Enabled__c = true;
        insert newProperty;

        Location__c loc = InitializeSRDataTest.createLocation('123', 'Building');
        loc.Property_ID__c = '123';
        insert loc;

        Inventory__c invent = new Inventory__c();
        invent.CM_Price_Per_Sqft__c = 20;
        invent.is_mixed_use__c = '0';
        invent.Inventory_ID__c = '1';
        invent.Building_ID__c = '1';
        invent.Floor_ID__c = '1';
        invent.Marketing_Name__c = 'Damac Heights';
        invent.Address_Id__c = '1';
        invent.EOI__C = NULL;
        invent.Tagged_to_EOI__c = false;
        invent.Is_Assigned__c = false;
        invent.Property_ID__c = newProperty.Id;
        invent.Property__c = newProperty.Id;
        invent.Status__c = 'Released';
        invent.special_price__c = 1000000;
        invent.CurrencyISOCode = 'AED';
        invent.Property_ID__c = '1234';
        invent.Floor_Package_ID__c = '';
        invent.building_location__c = loc.Id;
        invent.property_id__c = newProperty.Id;
        insert invent;

        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;

        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.Agency_Name__c = 'Test Account';
        SR.NSIBPM__Customer__c  = a.Id;
        SR.NSIBPM__Email__c = 'test@nsigulf.com';
        SR.NSIBPM__Send_SMS_to_Mobile__c= '12123';
        SR.COUNTRY_OF_SALE__C = 'UAE;Lebanon;Jordan';
        insert SR;

        Form_request_Inventories__c formInv = new Form_request_Inventories__c ();
        formInv.Inventory__c = invent.id;
        formInv.Form_request__c = formRequest.Id;
        
        insert formInv;
        
        formRequest.Service_Request__c = sr.Id;
        update formRequest;
        
        
        
        Unit_Documents__c objUnitDocument = new Unit_Documents__c();
        objUnitDocument.Document_Name__c = 'test';
        objUnitDocument.Document_Description__c = 'test wedcf d';
        objUnitDocument.Document_type__c = 'Registration';
        objUnitDocument.Optional__c = false;
        objUnitDocument.Status__c = 'Pending Upload';
        objUnitDocument.User_Doc__c = false;
        objUnitDocument.Service_Request__c = sr.id;
        insert objUnitDocument;

        ESign_Details__c esign = new ESign_Details__c();
        esign.Form_request__c = formRequest.id;
        esign.Unit_Documents__c = objUnitDocument.Id;
        esign.Document_Id__c= '1234567890';
        esign.Status__c = 'Signed';
        insert esign;
        
        New_Step__c step = new New_Step__c();
        step.Service_Request__c = sr.Id;
        step.is_closed__c = false;
        step.Step_Status__c = 'Awaiting Executed Copy';
        step.Step_No__c = 22;
        step.Step_Type__c = 'Awaiting Executed Copy Upload';
        insert step;

        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(formRequest);
            
            PageReference pageRef = page.Damac_ReservationForm;
            pageRef.getParameters().put('id', String.valueOf(formRequest.Id));
            pageRef.getParameters().put('inquiryId', String.valueOf(inquiry.Id));
            pageRef.getParameters().put('accountId', String.valueOf(acc.Id));
            Test.setCurrentPage(pageRef);
            
            Damac_ReservationFormHelper form = new Damac_ReservationFormHelper(sc);
            form.formRequest = formRequest;
            form.signNowURL = 'test.com';
            form.populateInquiryOnFormRequest();
            form.populateAccountOnFormRequest();
            form.saveFormRequest();
            form.reservationFormSigning();
            formRequest.Status__c = 'Sign via Email';
            update formRequest;
            
            form.formRequest = formRequest;
            form.reservationFormSigning();
            Damac_ReservationFormHelper.inquiryDetails('First');
            Damac_ReservationFormHelper.accountDetails('Testing','Individual');
            Damac_ReservationFormHelper.accountDetails('Testing','Corporate');
            formRequest.Status__c = 'Signed';
            update formRequest;
           
            form.saveFormRequest();
        
            Damac_SignNowAPI.getAttachment(null, 'objContentVersion','');
            Damac_SignNowAPI.userGUID();
            LOI_Signer_Position__mdt position = [SELECT X_Position__c,Y_Position__c,width__c,Height__c,Page_Number__c,Required__c FROM LOI_Signer_Position__mdt LIMIT 1 ];
            
            
            List<Agent_Sign_Positions__mdt> signPositions = [SELECT ID FROM Agent_Sign_Positions__mdt];
            Damac_SignNowAPI.addSignatureField(null, '',position,'basic');
            ContentVersion objContentVersion = new ContentVersion( Title = 'test',
                                                    PathOnClient = 'test',
                                                    VersionData = Blob.valueOf('test'));
            Damac_SignNowAPI.uploadDocument(null, objContentVersion,'basic',null,null);
            Damac_SignNowAPI.getDocumentDetails('1234567890');
                String reqBody = '{"meta":{"timestamp":"1588129888","event":"document.open"},"content":{"document_id":"1234567890","document_name":"test","user_id":"f8a6e77562454658be504edc226849fd0f03a0fd","viewer_user_unique_id":""}}';
                RestRequest req = new RestRequest(); 
                RestResponse res = new RestResponse();
                req.requestURI = 'http://testing.com';  
                req.httpMethod = 'POST';
                req.requestBody = Blob.valueof(reqBody);
                Blob body1 = req.requestBody;
                String bodyString = body1.toString(); 
                RestContext.request = req;
                RestContext.response = res;
                Test.setMock(HttpCalloutMock.class, new MockHttpCalloutClass());
            Damac_Signnow_callback.callback();
        Test.stopTest();

    }
    
    static testMethod void ukreservationForm(){
        Account acc = insertAccount();
        Inquiry__c inquiry = insertInquiry();
        Form_Request__c formRequest = insertukFormRequest(acc.Id, inquiry.Id);
        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c = 1;
        newProperty.Property_Code__c = 'VIR';
        newProperty.Property_Name__c = 'VIRIDIS @ AKOYA OXYGEN';
        newProperty.District__c = 'AL YUFRAH 2';
        newProperty.AR_Transaction_Type__c  = 'INV VIR';
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR';
        newProperty.Brokerage_Distribution_Set__c = '11600';
        newProperty.Sales_Commission_Dist_Set__c = '11601';
        newProperty.Currency_Of_Sale__c = 'AED';
        newProperty.Signature_Col_Customer_Stmt__c = 'Front Line Investment Management Co. LLC';
        newProperty.EOI_Enabled__c = true;
        insert newProperty;

        Location__c loc = InitializeSRDataTest.createLocation('123', 'Building');
        loc.Property_ID__c = '123';
        insert loc;

        Inventory__c invent = new Inventory__c();
        invent.CM_Price_Per_Sqft__c = 20;
        invent.is_mixed_use__c = '0';
        invent.Inventory_ID__c = '1';
        invent.Building_ID__c = '1';
        invent.Floor_ID__c = '1';
        invent.Marketing_Name__c = 'Damac Heights';
        invent.Address_Id__c = '1';
        invent.EOI__C = NULL;
        invent.Tagged_to_EOI__c = false;
        invent.Is_Assigned__c = false;
        invent.Property_ID__c = newProperty.Id;
        invent.Property__c = newProperty.Id;
        invent.Status__c = 'Released';
        invent.special_price__c = 1000000;
        invent.CurrencyISOCode = 'AED';
        invent.Property_ID__c = '1234';
        invent.Floor_Package_ID__c = '';
        invent.building_location__c = loc.Id;
        invent.property_id__c = newProperty.Id;
        insert invent;

        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;

        NSIBPM__Service_Request__c SR = new NSIBPM__Service_Request__c();
        SR.Agency_Name__c = 'Test Account';
        SR.NSIBPM__Customer__c  = a.Id;
        SR.NSIBPM__Email__c = 'test@nsigulf.com';
        SR.NSIBPM__Send_SMS_to_Mobile__c= '12123';
        SR.COUNTRY_OF_SALE__C = 'UAE;Lebanon;Jordan';
        insert SR;

        Form_request_Inventories__c formInv = new Form_request_Inventories__c ();
        formInv.Inventory__c = invent.id;
        formInv.Form_request__c = formRequest.Id;
        
        insert formInv;
        
        formRequest.Service_Request__c = sr.Id;
        update formRequest;
        
        
        Unit_Documents__c objUnitDocument = new Unit_Documents__c();
        objUnitDocument.Document_Name__c = 'test';
        objUnitDocument.Document_Description__c = 'test wedcf d';
        objUnitDocument.Document_type__c = 'Registration';
        objUnitDocument.Optional__c = false;
        objUnitDocument.Status__c = 'Pending Upload';
        objUnitDocument.User_Doc__c = false;
        objUnitDocument.Service_Request__c = sr.id;
        insert objUnitDocument;

        ESign_Details__c esign = new ESign_Details__c();
        esign.Form_request__c = formRequest.id;
        esign.Unit_Documents__c = objUnitDocument.Id;
        esign.Document_Id__c= '1234567890';
        esign.Status__c = 'Signed';
        insert esign;
        
        
        New_Step__c step = new New_Step__c();
        step.Service_Request__c = sr.Id;
        step.is_closed__c = false;
        step.Step_Status__c = 'Awaiting Executed Copy';
        step.Step_No__c = 22;
        step.Step_Type__c = 'Awaiting Executed Copy Upload';
        insert step;

        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(formRequest);
            
            PageReference pageRef = page.Damac_ReservationForm;
            pageRef.getParameters().put('id', String.valueOf(formRequest.Id));
            pageRef.getParameters().put('inquiryId', String.valueOf(inquiry.Id));
            pageRef.getParameters().put('accountId', String.valueOf(acc.Id));
            Test.setCurrentPage(pageRef);
            
            Damac_ReservationFormHelper form = new Damac_ReservationFormHelper(sc);
            form.formRequest = formRequest;
            form.signNowURL = 'test.com';
            form.populateInquiryOnFormRequest();
            form.populateAccountOnFormRequest();
            form.saveFormRequest();
            form.reservationFormSigning();
            formRequest.Status__c = 'Sign via Email';
            update formRequest;
            
            form.formRequest = formRequest;
            form.reservationFormSigning();
            Damac_ReservationFormHelper.inquiryDetails('First');
            Damac_ReservationFormHelper.accountDetails('Testing','Individual');
            Damac_ReservationFormHelper.accountDetails('Testing','Corporate');
            formRequest.Status__c = 'Signed';
            update formRequest;
           
            form.saveFormRequest();
        
            Damac_SignNowAPI.getAttachment(null, 'objContentVersion','');
            Damac_SignNowAPI.userGUID();
            LOI_Signer_Position__mdt position = [SELECT X_Position__c,Y_Position__c,width__c,Height__c,Page_Number__c,Required__c FROM LOI_Signer_Position__mdt LIMIT 1 ];
            
            
            List<Agent_Sign_Positions__mdt> signPositions = [SELECT ID FROM Agent_Sign_Positions__mdt];
            Damac_SignNowAPI.addSignatureField(null, '',position,'basic');
            ContentVersion objContentVersion = new ContentVersion( Title = 'test',
                                                    PathOnClient = 'test',
                                                    VersionData = Blob.valueOf('test'));
            Damac_SignNowAPI.uploadDocument(null, objContentVersion,'basic',null,null);
            Damac_SignNowAPI.getDocumentDetails('1234567890');
                String reqBody = '{"meta":{"timestamp":"1588129888","event":"document.open"},"content":{"document_id":"1234567890","document_name":"test","user_id":"f8a6e77562454658be504edc226849fd0f03a0fd","viewer_user_unique_id":""}}';
                RestRequest req = new RestRequest(); 
                RestResponse res = new RestResponse();
                req.requestURI = 'http://testing.com';  
                req.httpMethod = 'POST';
                req.requestBody = Blob.valueof(reqBody);
                Blob body1 = req.requestBody;
                String bodyString = body1.toString(); 
                RestContext.request = req;
                RestContext.response = res;
                Test.setMock(HttpCalloutMock.class, new MockHttpCalloutClass());
            Damac_Signnow_callback.callback();
        Test.stopTest();

    }

    private static Form_Request__c insertFormRequest(Id accId, Id inquiryId) {
        Form_Request__c formRequest = new Form_Request__c();
        formRequest.Corporate__c = accId;
        formRequest.Inquiry__c = inquiryId;
        formRequest.Status__c = 'Sign Now';
        insert formRequest;
        return formRequest;
    }
    
    private static Form_Request__c insertUKFormRequest(Id accId, Id inquiryId) {
        Id formRecordTypeId = Schema.SObjectType.Form_Request__c.getRecordTypeInfosByName().get('UK Form').getRecordTypeId();
        Form_Request__c formRequest = new Form_Request__c();
        formRequest.Corporate__c = accId;
        formRequest.RecordTypeId = formRecordTypeId;
        formRequest.Inquiry__c = inquiryId;
        formRequest.Other_Buyer_Individual_Email__c  = 'test@test.com';
        formRequest.Other_Individual_Name__c = 'test';
        formRequest.Buyer_Type__c = 'Individual';
        insert formRequest;
        return formRequest;
    }
    
    private static Account insertAccount() {
        Account acc = new Account();
        acc.Name = 'Testing Account';
        acc.Agency_Email__c = 'acc@email.com';
        insert acc;
        return acc;
    }
    
    private static Inquiry__c insertInquiry() {
        Inquiry__c inquiry = new Inquiry__c();
        inquiry.First_Name__c = 'First';
        inquiry.Last_Name__c = 'Last';
        inquiry.Inquiry_Status__c = 'Active';
        inquiry.Email__c = 'test@email.com';
        insert inquiry;
        return inquiry;
    }
}