@isTest
public class PaymentPlanDetailsControllerTest {
    @testSetup 
    public static void InsertData(){
        Payment_Plan__c plan = new Payment_Plan__c();
        plan.Building_ID__c = '123';
        plan.Payment_Term_Description__c = 'Test Desc';
        plan.Effective_From__c = System.today();
        plan.Effective_To__c = System.today()+1;
        insert plan;
        Payment_Terms__c terms= new Payment_Terms__c();
        terms.Installment__c='DP';
        terms.Description__c='Test Desc';
        terms.Milestone_Event__c='Imeediate';
        terms.Percent_Value__c='10.00';
        terms.Payment_Plan__c=plan.Id;
        insert terms;
    }
    
    @isTest
    public static void TestMethod1(){
      Payment_Plan__c plan=[select id,name from Payment_Plan__c limit 1];
      Test.startTest();
      PageReference pageRef = Page.PaymentPlanDetails;
      Test.setCurrentPage(pageRef);
      PaymentPlanDetailsController controller= new PaymentPlanDetailsController();
      apexpages.currentpage().getparameters().put('planName', plan.Name);
      controller.GetPaymentTerms();
      Test.stopTest();
       
    }
        

}