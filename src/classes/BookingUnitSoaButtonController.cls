/**
 * @File Name          : BookingUnitSoaButtonController.cls
 * @Description        :
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 9/2/2019, 3:49:04 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    9/2/2019, 1:40:11 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public class BookingUnitSoaButtonController {

    public Id recordId {get; set;}
    public String responseUrl {get; set;}
    public String accessToken {get; set;}
    public String pdfFile {get; set;}
    public Blob pdfBlob {get; set;}
    public sObject objectType {get; set;}
    public String registrationId;

    public BookingUnitSoaButtonController(ApexPages.StandardController controller) {
        recordId = controller.getId();
        System.debug('Booking Unit Id in consturctor: '+recordId);
        objectType = controller.getRecord();
        System.debug('objectType: ' + objectType);
    }

    public PageReference getUnitSoaUrlCallingList() {

        Calling_List__c callLst = [SELECT id,
                                          Party_ID__c,
                                          Registration_ID__c,
                                          Account__r.Party_ID__c,
                                          Booking_Unit__r.Registration_ID__c
                                    FROM Calling_List__c
                                    WHERE Id =: recordId];

        if(callLst != null) {
            registrationId = callLst.Registration_ID__c;
            if(registrationId == null || registrationId == '') {
                System.debug('In If block of callLst.Booking_Unit__r.Registration_ID__c');
                registrationId = callLst.Booking_Unit__r.Registration_ID__c;
            }
            System.debug('Registration Id is: '+registrationId);
        }
        PageReference pg = getUnitSoaUrl();
        return null;
    }

    public PageReference getUnitSoaUrlBookingUnit() {

        Booking_Unit__c bookingUnit = [SELECT Id, Registration_ID__c
                                       FROM Booking_Unit__c
                                       WHERE Id =: recordId];
        if(bookingUnit != null) {
            
            registrationId = bookingUnit.Registration_ID__c;
            System.debug('Registration Id is: '+registrationId);
        }
        PageReference pg = getUnitSoaUrl();
        return null;
    }

    public PageReference getUnitSoaUrl() {

        //String registrationId;
        /*if(registrationId == NULL || String.isBlank(registrationId)){
            Booking_Unit__c bookingUnit = [SELECT Id, Registration_ID__c
                                           FROM Booking_Unit__c
                                           WHERE Id =: recordId];
            if(bookingUnit != null) {
            registrationId = bookingUnit.Registration_ID__c;
            System.debug('Registration Id is: '+registrationId);
            }
        }*/
        FmIpmsRestCoffeeServices.updateCustomSetting = false;

        responseUrl = FmIpmsRestCoffeeServices.getUnitSoa(registrationId);
        System.debug('responseUrl is: '+responseUrl);

        if(String.isBlank(responseUrl) || responseUrl == null || responseUrl == 'Null') {
            System.debug('In ApexMessage Method');
            ApexPages.Message msg = new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'There was an error processing your request');
            ApexPages.addMessage(msg);
            return null;
            //ApexPages.addMessages(new ApexPages.Messages(ApexPages.SEVERITY.ERROR, 'Invalid RegistrationId'));
        }

        //Http callout

        accessToken = FmIpmsRestCoffeeServices.accessToken;
        System.debug('AccessToken is' + accessToken);

        HttpRequest req = new HttpRequest();
        req.setEndpoint(responseUrl);
        req.setHeader('Accept', 'application/json');
        req.setMethod('GET');
        req.setHeader('Authorization','Bearer' + accessToken);
        req.setTimeout(120000);
        HttpResponse response = new Http().send(req);
        System.debug('Status Code = ' + response.getStatusCode());
        System.debug('Pdf Body = ' + response.getBodyAsBlob());


        if(response.getStatusCode() == 200) {
            pdfBlob =  response.getBodyAsBlob();
            pdfFile = EncodingUtil.base64Encode(pdfBlob);
            
            //TODO  added to create record of soa genrator
            if(String.valueof(recordId).startsWith('a0x'))
            GenericUtility.createSOACreator(responseUrl,UserInfo.getUserId(),system.now(),registrationId,'Generate Unit SOA[New]',Null,recordId,NULL);
            if(String.valueof(recordId).startsWith('a3A'))
            GenericUtility.createSOACreator(responseUrl,UserInfo.getUserId(),system.now(),registrationId,'Generate Unit SOA[New]',Null,Null,recordId);
        }

        FmIpmsRestCoffeeServices.updateCustomSetting = true;
        FmIpmsRestCoffeeServices.updateBearerToken();      //calling updateMethod of Custom Setting after all callouts execution

        return null;
    }





}