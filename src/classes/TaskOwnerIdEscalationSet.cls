/**
 * Ver       Date            Author      		    Modification
 * 1.0    9/19/2019         Arsh Dave              Initial Version 
 * 2.0    10/08/2019        Arsh Dave              Updated the fields to random fields as new fields cannot be deployet on production  
**/
public without sharing class TaskOwnerIdEscalationSet {
    
    public static final String SRT_LEVEL_1 = 'Level 1';
    public static final String SRT_LEVEL_2 = 'Level 2';
    public static final String SRT_LEVEL_3 = 'Level 3';
    public static final String SRT_SLA_LEVEL = 'SLA Level';

    public static List<Customer_Request_SR_Process__mdt> listCustomer_Request_SR_Process;
    public static FM_Case__c objFMCaseTask;

    public static void getmetadata(FM_Case__c objFMCaseTask){

        listCustomer_Request_SR_Process = [
            SELECT MasterLabel
                 , Category__c
                 , Subcategory__c
                 , SR_Type__c
                 , SLA_Timeline__c
                 , SLA_Assigned_User__c
                 , Level_1_Timeline__c
                 , Level_1_User__c
                 , Level_2_Timeline__c
                 , Level_2_User__c
                 , Level_3_Timeline__c
                 , Level_3_User__c
              FROM Customer_Request_SR_Process__mdt
             WHERE Category__c = :objFMCaseTask.Category_GE__c   
        ];
        System.debug('-->> listCustomer_Request_SR_Process  : ' + listCustomer_Request_SR_Process );
    }

    @InvocableMethod
    public static void updateDueDate(List<Task> listTaskId){

        Task getTaskDetails = fetchTaskDetails(listTaskId[0].Id);

        getFmCaseRecord(getTaskDetails);

        getmetadata(objFMCaseTask);

        ReturnWrapper getTaskDetailsWrapper = checkForLevelAssignment(false, getTaskDetails);
        System.debug('-->> getTaskDetailsWrapper in test: ' + getTaskDetailsWrapper);
        update getTaskDetailsWrapper.objTaskDetails;
    }

    public static Task fetchTaskDetails(Id objTaskId){
        Task getTaskDetails = [
            SELECT Id
                 , OwnerId
                 , Status
                 , WhatId
                 , ActivityDate
                 , Age__c
                 , CreatedDate
                 , Subject
              FROM Task
             WHERE Id = :objTaskId  
        ];
        return getTaskDetails;
    }
    
    public static void getFmCaseRecord(Task getTaskDetails){
        objFMCaseTask = [
            SELECT Id
                 , Category_GE__c
                 , Subcategory_GE__c
                 , Booking_Unit__c
                 , Account__c
              FROM FM_Case__c
             WHERE Id = :getTaskDetails.WhatId
        ];
        System.debug('-->> objFMCaseTask fm case  : ' + objFMCaseTask );
    }

    public static ReturnWrapper callFromBatch(Task getTaskDetails){
        
        getFmCaseRecord(getTaskDetails);

        getmetadata(objFMCaseTask);

        return checkForLevelAssignment(true, getTaskDetails);
    }

    public static ReturnWrapper checkForLevelAssignment (Boolean isCalledFromBatch, Task getTaskDetails){
        if(!listCustomer_Request_SR_Process.isEmpty() && objFMCaseTask != NULL){
            for(Customer_Request_SR_Process__mdt objCustomer_Request_SR_Process: listCustomer_Request_SR_Process){
                if(objCustomer_Request_SR_Process.Subcategory__c == NULL && objFMCaseTask.Category_GE__c == objCustomer_Request_SR_Process.Category__c){
                    return allTaskDataUpdation(isCalledFromBatch, getTaskDetails, objCustomer_Request_SR_Process);
                } else if(objCustomer_Request_SR_Process.Subcategory__c != NULL && objFMCaseTask.Subcategory_GE__c == objCustomer_Request_SR_Process.Subcategory__c){
                    System.debug('-->> else if inside checkForLevelAssignment  : ');
                    return allTaskDataUpdation(isCalledFromBatch, getTaskDetails, objCustomer_Request_SR_Process);
                }    
            }
        }
        ReturnWrapper objReturnWrapperVar;
        return objReturnWrapperVar; 
    }

    public static ReturnWrapper allTaskDataUpdation(Boolean isCalledFromBatch, Task getTaskDetails, Customer_Request_SR_Process__mdt objCustomer_Request_SR_Process){
        ReturnWrapper objReturnWrapper;
        if(getTaskDetails.Age__c == SRT_SLA_LEVEL){
            System.debug('-->>  inside SLA Level  : ');
            getTaskDetails = setTheActivityDate(getTaskDetails, objCustomer_Request_SR_Process.SLA_Timeline__c);
            System.debug('-->> getTaskDetails   : ' + getTaskDetails);
            if(isCalledFromBatch) {
                getTaskDetails.Age__c = SRT_LEVEL_1;
                getTaskDetails = setTheActivityDate(getTaskDetails, objCustomer_Request_SR_Process.Level_1_Timeline__c);
            }
            objReturnWrapper = new ReturnWrapper(objCustomer_Request_SR_Process.Level_1_User__c, getTaskDetails);
            System.debug('-->>  objReturnWrapper inside SLA Level before return: '+ objReturnWrapper);
            return objReturnWrapper;    
        } else if(getTaskDetails.Age__c == SRT_LEVEL_1){  
            getTaskDetails = setTheActivityDate(getTaskDetails, objCustomer_Request_SR_Process.Level_1_Timeline__c);
            if(isCalledFromBatch){
                getTaskDetails.Age__c = SRT_LEVEL_2;
                getTaskDetails = setTheActivityDate(getTaskDetails, objCustomer_Request_SR_Process.Level_2_Timeline__c);                
            }
            objReturnWrapper = new ReturnWrapper(objCustomer_Request_SR_Process.Level_2_User__c, getTaskDetails);
            return objReturnWrapper;
        } else if(getTaskDetails.Age__c == SRT_LEVEL_2){
            getTaskDetails = setTheActivityDate(getTaskDetails, objCustomer_Request_SR_Process.Level_2_Timeline__c);
            if(isCalledFromBatch){
                getTaskDetails.Age__c = SRT_LEVEL_3;
                getTaskDetails = setTheActivityDate(getTaskDetails, objCustomer_Request_SR_Process.Level_3_Timeline__c);
            }
            objReturnWrapper = new ReturnWrapper(objCustomer_Request_SR_Process.Level_3_User__c, getTaskDetails);
            return objReturnWrapper;
        } else if(getTaskDetails.Age__c == SRT_LEVEL_3){

            return objReturnWrapper;
        } else {
            getTaskDetails.ActivityDate = CalculateBusinessDays.addBusinessDays(Date.Today(), 2);
            if(isCalledFromBatch){
                getTaskDetails.Age__c = SRT_SLA_LEVEL;
                getTaskDetails = setTheActivityDate(getTaskDetails, objCustomer_Request_SR_Process.SLA_Timeline__c);
            }
            objReturnWrapper = new ReturnWrapper(objCustomer_Request_SR_Process.SLA_Assigned_User__c, getTaskDetails);
            return objReturnWrapper;  
        }
    }

    public static Task setTheActivityDate(Task getTaskDetails, Decimal valueOfDate){
        Integer noOfDays = Integer.valueOf(valueOfDate);
        getTaskDetails.ActivityDate = CalculateBusinessDays.addBusinessDays(Date.Today(), noOfDays);
        return getTaskDetails;
    }
    
    public Class ReturnWrapper {
        public String strOwnerIdEscalation;
        public Task objTaskDetails;

        public ReturnWrapper(String strOwnerIdEscalation, Task objTaskDetails){
            this.strOwnerIdEscalation = strOwnerIdEscalation;
            this.objTaskDetails = objTaskDetails;
        }

    }


}