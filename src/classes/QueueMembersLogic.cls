/**
 * Logic class for QueueMembersWebService
 */
public class QueueMembersLogic {

    /**
     * Method to process request and send the response
     */
    public static void processQueueMemberRequest() {
        QueueMembersResponseBody response = new QueueMembersResponseBody();

        // Get request body
        String requestBody = RestContext.request.requestBody.toString();
        System.debug('=========== requestBody : ' + requestBody);

        try {
            // Parse request body
            QueueMembersRequestBody request = (QueueMembersRequestBody) JSON.deserializeStrict(
                requestBody,
                QueueMembersRequestBody.Class
            );
            System.debug('===== request : ' + request);

            if (request.queueExtension != '') {
                String crePoolName = getCrePoolNameForExtension(request.queueExtension);
                if (String.isNotEmpty(crePoolName)) {
                    List<String> extensions = getCreExtensionsForPoolName(new List<String>{crePoolName});
                    if (extensions.size() > 0) {
                        response.extensions = extensions;
                    } else {
                        response.errorCode = '400';
                        response.errorMessage = 'No users found in pool';
                    }
                } else {
                    response.errorCode = '400';
                    response.errorMessage = 'No pool found for requested extension';
                }
            } else {
                response.errorCode = '400';
                response.errorMessage = 'No extension found';
            }
        } catch (Exception ex) {
            response.errorMessage = ex.getMessage();
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
            Restcontext.response.statusCode = 400;
            System.debug('============= response : ' + JSON.serialize(response));
            return;
        }
        // Send response
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
    }

    /**
     * Methid to get cre pool name
     */
    public static String getCrePoolNameForExtension(String extension) {
        CRE_Pool_Detail__c crePool = CRE_Pool_Detail__c.getInstance(extension);
        if (crePool != null) {
            return crePool.Pool_Name__c;
        } else {
            Collection_Payment_Pool__c collectionPool = Collection_Payment_Pool__c.getInstance(extension);
            if (collectionPool != null) {
                return collectionPool.Pool_Name__c;
            }
        }
        return '';
    }

    /**
     * Method to cre extensions for the given pool name (public group)
     */
    public static List<String> getCreExtensionsForPoolName(List<String> poolNames) {
        List<String> extensions = new List<String>();
        Set<Id> userIds = new Set<Id>();

        List<Group> publicGroups =
            [
                SELECT
                    Id,
                    Name,
                    Type,
                    (
                        SELECT
                            Id,
                            GroupId,
                            UserOrGroupId
                        FROM
                            GroupMembers
                    )
                FROM
                    Group
                WHERE
                    Name IN :poolNames
            ];

        System.debug('======= publicGroups : ' + publicGroups);

        // Fecth all user ids
        for (Group groupRecord : publicGroups) {
            for (GroupMember groupMember : groupRecord.GroupMembers) {
                userIds.add(groupMember.UserOrGroupId);
            }
        }

        // Get all user records
        Map<Id, User> userMap = new Map<Id, User>(
            [
                SELECT
                    Id,
                    Name,
                    Extension
                FROM
                    User
                WHERE
                    Id IN :userIds
            ]
        );

        System.debug('======= userMap : ' + userMap);

        // Create pool detail instances
        for (Group groupRecord : publicGroups) {
            for (GroupMember groupMember : groupRecord.GroupMembers) {
                System.debug('>>>>>>> groupMember : ' + groupMember);
                extensions.add(userMap.get(groupMember.UserOrGroupId).Extension);
            }
        }

        System.debug('======= extensions : ' + extensions);
        return extensions;
    }

    /**
     * Wrapper class for QueueMembersWebService request body
     */
    public class QueueMembersRequestBody {
        public String queueExtension {get; set;}

        public QueueMembersRequestBody() {
            this.queueExtension = '';
        }
    }

    /**
     * Wrapper class for QueueMembersWebService response body
     */
    public class QueueMembersResponseBody {
        public List<String> extensions {get; set;}
        public String status;
        public String errorCode;
        public String errorMessage;

        public QueueMembersResponseBody() {
            this.extensions = new List<String>();
            this.status = '';
            this.errorCode = '';
            this.errorMessage = '';
        }
    }
}