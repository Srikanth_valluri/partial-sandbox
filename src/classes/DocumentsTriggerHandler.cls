public without sharing class DocumentsTriggerHandler{
    public void afterUpdate(map<Id,SR_Attachments__c> newMap, map<Id,SR_Attachments__c> oldMap){
        set<Id> setCaseIds = new set<Id>();
        set<Id> setDocIds = new set<Id>();
        for(SR_Attachments__c objSR : newMap.values()){
            if(objSR.IsValid__c && objSR.Attachment_URL__c != null && objSR.Case__c != null
            && (objSR.IsValid__c != oldMap.get(objSR.Id).IsValid__c
            || objSR.Attachment_URL__c != oldMap.get(objSR.Id).Attachment_URL__c)){
                setDocIds.add(objSR.Id);
            }
        }
        if(!setDocIds.isEmpty()){
            for(SR_Attachments__c objSR : [Select Id
                                            , IsValid__c
                                            , Name
                                            , Case__c
                                            , Case__r.RecordTypeId
                                            , Case__r.RecordType.Name
                                            , Case__r.RecordType.DeveloperName
                                            , Attachment_URL__c 
                                       from SR_Attachments__c 
                                       where Id IN : setDocIds
                                       and isValid__c = true
                                       and Attachment_URL__c != null
                                       and Case__c != null
                                       and Case__r.RecordTypeId != null
                                       and Case__r.RecordType.DeveloperName = 'Handover'
                                       and Name Like '%Signed Handover Checklist%']){
                // (Name Like '%Signed Letter of Discharge%' or Name Like '%Signed Handover Checklist%')
                setCaseIds.add(objSR.Case__c);
            } // end of for loop
            
            list<Case> lstCaseToUpdate = new list<Case>();
            for(Case objCase : [Select Id
                                     , CaseNumber
                                     , (Select Id
                                             , IsValid__c
                                             , Name
                                             , Case__c
                                             , Case__r.RecordTypeId
                                             , Case__r.RecordType.Name
                                             , Case__r.RecordType.DeveloperName
                                             , Attachment_URL__c 
                                        from SR_Attachments__r
                                        where isValid__c = true
                                        and Attachment_URL__c != null) 
                                from Case where Id IN : setCaseIds]){
                Integer i = 0;
                for(SR_Attachments__c objSR : objCase.SR_Attachments__r){
                    /*
                    if(objSR.Name.contains('Signed Letter of Discharge')){
                        i++;
                    }
                    */
                    if(objSR.Name.contains('Signed Handover Checklist')){
                        i++;
                    }
                }
                // earlier this was i>=2
                if(i>=1){
                    objCase.Documents_Uploaded__c = true;
                    lstCaseToUpdate.add(objCase);
                }
            }
            if(!lstCaseToUpdate.isEmpty()){
                update lstCaseToUpdate;
            }
        }
    }
}