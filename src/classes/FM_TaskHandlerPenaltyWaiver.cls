public without sharing class FM_TaskHandlerPenaltyWaiver {
    private static list<SR_Attachments__c> lstCustomAttachment = new list<SR_Attachments__c>();
    private static map<Id,list<Error_Log__c>> mapCaseIdlstErrorsGenerated = new map<Id,list<Error_Log__c>>();
    
    @InvocableMethod 
    public static void sendPWApprovalMail( list<Task> lstApprovedTasks ) {
        System.debug('lstApprovedTasks-----'+lstApprovedTasks);
        //set<Id> setApprovedTasksId = new set<Id>();
        Set<Id> setFMCaseWhatId = new Set<Id>();
        for( Task objTask : lstApprovedTasks ) {
            if( isRelatedToFMCase( objTask ) ) {
                //setApprovedTasksId.add( objTask.Id );
                setFMCaseWhatId.add( objTask.whatId );
            }
        }
        
        if( !setFMCaseWhatId.isEmpty() ) {
            sendPWApprovalMail(setFMCaseWhatId);
        }
    }
    
    @future(callout=true)
    public static void sendPWApprovalMail( set<Id> setFMCaseWhatId ) {
        list<UploadMultipleDocController.MultipleDocRequest> wrapperLst = new list<UploadMultipleDocController.MultipleDocRequest>();
        list<FM_Case__c> lstCasesToBeUpdated = new list<FM_Case__c>();
        
        list<FM_Case__c > lstFMCaseBURegId = getFMCaseDetails(setFMCaseWhatId);
        
        for( FM_Case__c fmCaseInstance : lstFMCaseBURegId ){
        	
            if( String.isNotEmpty( fmCaseInstance.Booking_Unit__r.Registration_ID__c ) ){
                wrapperLst = addSOAToWrapperList( fmCaseInstance, wrapperLst );                
            }
            else {
            	addErrorLogs( NULL, fmCaseInstance.Id, 'No Registration Id on the unit.' );
        	}
            
            if( !wrapperLst.isEmpty() ) {
                UploadMultipleDocController.data objResponse = PenaltyWaiverService.uploadDocumentsOnCentralRepo( wrapperLst, 'FM' );
                if( objResponse != NULL ) {
                    if( objResponse.data != NULL && String.isNotBlank( objResponse.status ) ) {
	                    insertCustomAttachment( fmCaseInstance, objResponse, fmCaseInstance.Unit_Name__c + ' Statement Of Account'   );
	                    wrapperLst.clear();
                	}
	                else if( String.isNotBlank( objResponse.status ) && objResponse.status.equalsIgnoreCase( 'Exception' ) ) {
	                	addErrorLogs( NULL, fmCaseInstance.Id, objResponse.message );
	                } 
                }
            }
            
            if( !mapCaseIdlstErrorsGenerated.containsKey( fmCaseInstance.Id ) ) {
                  lstCasesToBeUpdated.add( new FM_Case__c( Id = fmCaseInstance.Id , 
                  										   Status__c='Closed', 
                  										   Approval_Status__c='Approved' ) );
            }
        }
        
        if( !lstCustomAttachment.isEmpty() ) {
            system.debug( '==lstCustomAttachment==' +lstCustomAttachment );
            insert lstCustomAttachment ;
        }
        
        if( !mapCaseIdlstErrorsGenerated.isEmpty() ) {
            system.debug( '==mapCaseIdlstErrorsGenerated==' +mapCaseIdlstErrorsGenerated );
        
            list<Error_Log__c> lstErrorLogs = new list<Error_Log__c>();
            for( list<Error_Log__c> lstErrors : mapCaseIdlstErrorsGenerated.values() ) {
                  lstErrorLogs.addAll( lstErrors );
            }
            insert lstErrorLogs ;
        }
      
        if( !lstCasesToBeUpdated.isEmpty() ) {
            update lstCasesToBeUpdated;
        }
    }
    
    @testVisible 
    private static boolean isRelatedToFMCase( Task objTask ) {
        if( String.isNotBlank( objTask.WhatId ) && 
            String.valueOf( objTask.WhatId ).startsWith( FM_Utility.getObjectKeyPrefix('FM_Case__c') ) ) {
            return true ;
        }
        else {
            return false ;
        }
    }

    private static List<FM_Case__c> getFMCaseDetails(Set<Id> setFMCaseWhatId){
        return [select Id
                     , Unit_Name__c
                     , Name
                     , Account__c
                     , Booking_Unit__r.Registration_ID__c
                  from FM_Case__c
                 where id IN :setFMCaseWhatId];
    }

    @testVisible 
    private static list<UploadMultipleDocController.MultipleDocRequest> addSOAToWrapperList( FM_case__c fmCaseInstance,
                                                                                             list<UploadMultipleDocController.MultipleDocRequest> wrapperLst ) {
        try {
            String strURL = FmIpmsRestServices.getUnitSoaByRegistrationId( fmCaseInstance.Booking_Unit__r.Registration_ID__c ) ;
            system.debug( '==strURL==' +strURL );
            if( String.isNotBlank( strURL ) ) {
                wrapperLst.add( FM_Utility.makeWrapperObject( getDocumentBodyAsString( strURL ) ,
                                                              'SOA : ' + fmCaseInstance.Unit_Name__c + '.pdf',
                                                              'SOA : ' + fmCaseInstance.Unit_Name__c + '.pdf',
                                                              fmCaseInstance.Name, String.valueOf( wrapperLst.size() + 1 ) ) );
            }
            else {
                addErrorLogs( NULL, String.valueOf(fmCaseInstance.id), 'No SOA returned from the service for : '+fmCaseInstance.Unit_Name__c );
            }
        }
        catch( Exception e ) {
            addErrorLogs( NULL, String.valueOf(fmCaseInstance.id), 'Exception while generating the SOA : ' + e.getMessage() );
        }
        return wrapperLst ;
    }

    private static String getDocumentBodyAsString( String strURL ) {
        if( String.isNotBlank( strURL ) ) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint( strURL );
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/pdf');
            req.setTimeout(60000);
            HTTPResponse res = new Http().send( req );
            //return res.getBody();
            String strBody = EncodingUtil.base64Encode( res.getBodyAsBlob() );
            return strBody ;
        }
        return '';
    }
    
    @testVisible
    private static void insertCustomAttachment( FM_Case__c objFMCase, UploadMultipleDocController.data objResponse, String strDocumentName ) {
        for( UploadMultipleDocController.MultipleDocResponse objFile : objResponse.data ) {
            SR_Attachments__c objCustAttach = new SR_Attachments__c();
            objCustAttach.Account__c = objFMCase.Account__c ;
            objCustAttach.Attachment_URL__c = objFile.url;
            //objCustAttach.Booking_Unit__c = objFMCase.Booking_Unit__c ;
            objCustAttach.FM_Case__c = objFMCase.Id ;
            objCustAttach.Name = strDocumentName ;
            objCustAttach.isValid__c = true ;
            objCustAttach.Is_sent_in_email__c = true ;
            lstCustomAttachment.add( objCustAttach );
        }
    }
    
    @testVisible
    private static void addErrorLogs( String strAccountId,
                      String strCaseId,
                      String strMessage ) {
      Error_Log__c objLog = FM_Utility.createErrorLog( strAccountId, 
                                 NULL, 
                                 strCaseId, 
                                 'Penalty Waiver', 
                                 strMessage );
      if( mapCaseIdlstErrorsGenerated.containsKey( strCaseId ) ) {
        mapCaseIdlstErrorsGenerated.get( strCaseId ).add( objLog );
      }
      else {
        mapCaseIdlstErrorsGenerated.put( strCaseId , new list<Error_Log__c> { objLog } );
      }
    }
    
}