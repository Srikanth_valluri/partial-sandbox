global class Damac_InquiryOwnerReAssignSchedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new InquiryOwnerReshufflingBatch (new List <ID> ()), 1);
    }
}