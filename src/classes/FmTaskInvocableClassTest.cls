/**
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/25/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
private class FmTaskInvocableClassTest{
    private static testMethod void positiveOthersTest(){
        Account objA = TestDataFactoryFM.createAccount();
        insert objA;
        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objA);
        insert objSR;
        Booking__c objBooking = TestDataFactoryFM.createBooking(objA,objSR);
        insert objBooking;
        Booking_Unit__c objBU = TestDataFactoryFM.createBookingUnit(objA,objBooking);
        insert objBU;
        list<FM_Case__c> lstC = TestDataFactoryFM.createFMCase(1);
        lstC[0].Account__c = objA.Id;
        lstC[0].Booking_Unit__c = objBU.Id;
        lstC[0].Request_Type__c = 'Change of Contact Details';
        insert lstC;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());

        Task objT = new Task();
        objT.WhatId = lstC[0].Id;
        objT.Status = 'In Progress';
        objT.Assigned_User__c = 'FM Finance';
        objT.ActivityDate = date.today();
        objT.Subject = ' Verify COD Fee';
        objT.Priority = 'High';
        objT.Pushed_To_IPMS__c = false;
        objT.Process_Name__c = 'Change of Contact Details';
        insert objT;

        FmTaskInvocableClass.invokeTask( new list<Task>{ objT } );

        test.StopTest();
    }

    private static testMethod void positiveDTPCTest(){
        Account objA = TestDataFactoryFM.createAccount();
        insert objA;
        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objA);
        insert objSR;
        Booking__c objBooking = TestDataFactoryFM.createBooking(objA,objSR);
        insert objBooking;
        Booking_Unit__c objBU = TestDataFactoryFM.createBookingUnit(objA,objBooking);
        objBU.Unit_Name__c = 'DTPC/32/3212';
        insert objBU;
        list<FM_Case__c> lstC = TestDataFactoryFM.createFMCase(1);
        lstC[0].Account__c = objA.Id;
        lstC[0].Booking_Unit__c = objBU.Id;
        lstC[0].Request_Type__c = 'Proof Of Payment';
        insert lstC;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());

        Task objT = new Task();
        objT.WhatId = lstC[0].Id;
        objT.Status = 'In Progress';
        objT.Assigned_User__c = 'FM Finance';
        objT.ActivityDate = date.today();
        objT.Subject = ' Verify COD Fee';
        objT.Priority = 'High';
        objT.Pushed_To_IPMS__c = false;
        objT.Process_Name__c = 'Change of Contact Details';
        insert objT;

        FmTaskInvocableClass.invokeTask( new list<Task>{ objT } );

        test.StopTest();
    }

    // private static testMethod void positivePopTest(){
    //     Account objA = TestDataFactoryFM.createAccount();
    //     insert objA;
    //     NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objA);
    //     insert objSR;
    //     Booking__c objBooking = TestDataFactoryFM.createBooking(objA,objSR);
    //     insert objBooking;
    //     Booking_Unit__c objBU = TestDataFactoryFM.createBookingUnit(objA,objBooking);
    //     insert objBU;
    //     list<FM_Case__c> lstC = TestDataFactoryFM.createFMCase(1);
    //     lstC[0].Account__c = objA.Id;
    //     lstC[0].Booking_Unit__c = objBU.Id;
    //     lstC[0].Request_Type__c = 'Proof Of Payment';
    //     insert lstC;
    //     list<SR_Booking_Unit__c> lstSRBU = TestDataFactoryFM.createSRBookingUnits(objBU.Id, lstC[0].Id,1);
    //     insert lstSRBU;

    //     test.StartTest();
    //     Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
    //     Task objT = new Task();
    //     objT.WhatId = lstC[0].Id;
    //     objT.Status = 'In Progress';
    //     objT.Assigned_User__c = 'FM Finance';
    //     objT.ActivityDate = date.today();
    //     objT.Subject = Label.POP_Task_Subject_FM_Finance;
    //     objT.Priority = 'High';
    //     objT.Pushed_To_IPMS__c = false;
    //     objT.Process_Name__c = 'Proof Of Payment';
    //     insert objT;

    //     FmTaskInvocableClass.invokeTask( new list<Task>{ objT } );

    //     test.StopTest();
    // }

    // private static testMethod void positiveBulkTest(){
    //     Account objA = TestDataFactoryFM.createAccount();
    //     insert objA;
    //     NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objA);
    //     insert objSR;
    //     Booking__c objBooking = TestDataFactoryFM.createBooking(objA,objSR);
    //     insert objBooking;
    //     Booking_Unit__c objBU = TestDataFactoryFM.createBookingUnit(objA,objBooking);
    //     insert objBU;
    //     list<FM_Case__c> lstC = TestDataFactoryFM.createFMCase(1);
    //     lstC[0].Account__c = objA.Id;
    //     lstC[0].Booking_Unit__c = objBU.Id;
    //     lstC[0].Request_Type__c = 'Change of Contact Details';
    //     insert lstC;

    //     test.StartTest();
    //     Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(2));
    //     list<Task> lstT = new list<Task>();
    //     for(Integer i = 0; i < 110; i++){
    //         Task objT = new Task();
    //         objT.WhatId = lstC[0].Id;
    //         objT.Status = 'In Progress';
    //         objT.Assigned_User__c = 'FM Finance';
    //         objT.ActivityDate = date.today();
    //         objT.Subject = ' Verify COD Fee';
    //         objT.Priority = 'High';
    //         objT.Pushed_To_IPMS__c = false;
    //         objT.Process_Name__c = 'Change of Contact Details';
    //         lstT.add(objT);
    //     }
    //     insert lstT;

    //     FmTaskInvocableClass.invokeTask( lstT );

    //     test.StopTest();
    // }

    private static testMethod void negativeOthersTest(){
        Account objA = TestDataFactoryFM.createAccount();
        insert objA;
        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objA);
        insert objSR;
        Booking__c objBooking = TestDataFactoryFM.createBooking(objA,objSR);
        insert objBooking;
        Booking_Unit__c objBU = TestDataFactoryFM.createBookingUnit(objA,objBooking);
        insert objBU;
        list<FM_Case__c> lstC = TestDataFactoryFM.createFMCase(1);
        lstC[0].Account__c = objA.Id;
        lstC[0].Booking_Unit__c = objBU.Id;
        lstC[0].Request_Type__c = 'Change of Contact Details';
        insert lstC;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(1));

        Task objT = new Task();
        objT.WhatId = lstC[0].Id;
        objT.Status = 'In Progress';
        objT.Assigned_User__c = 'FM Finance';
        objT.ActivityDate = date.today();
        objT.Subject = ' Verify COD Fee';
        objT.Priority = 'High';
        objT.Pushed_To_IPMS__c = false;
        objT.Process_Name__c = 'Change of Contact Details';
        insert objT;

        FmTaskInvocableClass.invokeTask( new list<Task>{ objT } );

        test.StopTest();
    }

    private static testMethod void exceptionTest(){
        Account objA = TestDataFactoryFM.createAccount();
        insert objA;
        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objA);
        insert objSR;
        Booking__c objBooking = TestDataFactoryFM.createBooking(objA,objSR);
        insert objBooking;
        Booking_Unit__c objBU = TestDataFactoryFM.createBookingUnit(objA,objBooking);
        insert objBU;
        list<FM_Case__c> lstC = TestDataFactoryFM.createFMCase(1);
        lstC[0].Account__c = objA.Id;
        lstC[0].Booking_Unit__c = objBU.Id;
        lstC[0].Request_Type__c = 'Change of Contact Details';
        insert lstC;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(2));

        Task objT = new Task();
        objT.WhatId = lstC[0].Id;
        objT.Status = 'In Progress';
        objT.Assigned_User__c = 'FM Finance';
        objT.ActivityDate = date.today();
        objT.Subject = ' Verify COD Fee';
        objT.Priority = 'High';
        objT.Pushed_To_IPMS__c = false;
        objT.Process_Name__c = 'Change of Contact Details';
        insert objT;

        FmTaskInvocableClass.invokeTask( new list<Task>{ objT } );

        test.StopTest();
    }

    private static testMethod void noResponseTest(){
        Account objA = TestDataFactoryFM.createAccount();
        insert objA;
        NSIBPM__Service_Request__c objSR = TestDataFactoryFM.createServiceRequest(objA);
        insert objSR;
        Booking__c objBooking = TestDataFactoryFM.createBooking(objA,objSR);
        insert objBooking;
        Booking_Unit__c objBU = TestDataFactoryFM.createBookingUnit(objA,objBooking);
        insert objBU;
        list<FM_Case__c> lstC = TestDataFactoryFM.createFMCase(1);
        lstC[0].Account__c = objA.Id;
        lstC[0].Booking_Unit__c = objBU.Id;
        lstC[0].Request_Type__c = 'Change of Contact Details';
        insert lstC;

        test.StartTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment(3));

        Task objT = new Task();
        objT.WhatId = lstC[0].Id;
        objT.Status = 'In Progress';
        objT.Assigned_User__c = 'FM Finance';
        objT.ActivityDate = date.today();
        objT.Subject = ' Verify COD Fee';
        objT.Priority = 'High';
        objT.Pushed_To_IPMS__c = false;
        objT.Process_Name__c = 'Change of Contact Details';
        insert objT;

        FmTaskInvocableClass.invokeTask( new list<Task>{ objT } );

        test.StopTest();
    }
}