@isTest
public class SendAvailableAppointmentsToMObileAppTest {
    public  testmethod static void method()
    {
    
                    TriggerOnOffCustomSetting__c callingTriggerInst =new TriggerOnOffCustomSetting__c ();
                    callingTriggerInst.OnOffCheck__c=true;
                    callingTriggerInst.name='CallingListTrigger';
                    insert callingTriggerInst; 
                    
                    
                    
                    
                    Location__c l=new Location__c();
                    l.Name='test1234';
                     l.Location_ID__c='test1234';
                     insert l;
                     
                     Inventory__c inv=new Inventory__c();
                     
                    inv.Building_Location__c=l.id;
                    insert inv;
                    
                   NSIBPM__Service_Request__c nb=new NSIBPM__Service_Request__c();
                   insert nb; 
                    
                    Booking__c bk=new Booking__c();
                    bk.Deal_SR__c=nb.id;
                    insert bk;
                    
                    
                    
                    Booking_Unit__c bu=new Booking_Unit__c();
                    bu.Unit_Name__c='test1234';
                    bu.Inventory__c=inv.id;
                    bu.Booking__c=bk.id;
                    insert bu;
                    
                     Appointment__c ap=new Appointment__c();
                    ap.Appointment_Date__c=System.today() + 5;
                    ap.Process_Name__c='Generic';
                    ap.Sub_Process_Name__c='Head Office Appointment';
                    ap.Time_Slot_Sub__c =8;
                    ap.Slots__c='15:00 - 16:00';
                    ap.Building__c=l.id;
                    insert ap;
                     Appointment__c ap1=new Appointment__c();
                    ap1.Appointment_Date__c=System.today() + 5;
                    ap1.Process_Name__c='Generic';
                    ap1.Sub_Process_Name__c='Head Office Appointment';
                    ap1.Time_Slot_Sub__c =8;
                    ap1.Slots__c='15:00 - 16:00';
                    insert ap1;
                    Calling_List__c objCallList = new Calling_List__c();
                    objCallList.Service_Type__c = '01-Handover';
                    objCallList.Sub_Purpose__c ='Unit Viewing';
                    objCallList.Appointment_Date__c = System.today() + 2;
                    objCallList.Appointment__c= ap.id;
                    objCallList.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList.Appointment_Origin__c='Mobile';
                    insert objCallList;
                    Calling_List__c objCallList1 = new Calling_List__c();
                    objCallList1.Service_Type__c = '01-Handover';
                    objCallList1.Sub_Purpose__c ='Unit Viewing';
                    objCallList1.Appointment_Date__c = System.today() + 2;
                   // objCallList1.Appointment__c= ap.id;
                    objCallList1.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList1.Appointment_Origin__c='Mobile';
                    insert objCallList1;
                    
                   
      SendAvailableAppointmentsToMObileApp.getAppointments('01-Handover','Unit Viewing','2018-12-09',l.Location_ID__c);
    }
    
     public  testmethod static void method1()
    {
    
                    TriggerOnOffCustomSetting__c callingTriggerInst =new TriggerOnOffCustomSetting__c ();
                    callingTriggerInst.OnOffCheck__c=true;
                    callingTriggerInst.name='CallingListTrigger';
                    insert callingTriggerInst; 
                    
                    Location__c l=new Location__c();
                    l.Name='test1234';
                     l.Location_ID__c='test1234';
                     insert l;
                     
                      
                     Inventory__c inv=new Inventory__c();
                    inv.Building_Location__c=l.id;
                    insert inv;
                    
                     NSIBPM__Service_Request__c nb=new NSIBPM__Service_Request__c();
                   insert nb; 
                    
                    Booking__c bk=new Booking__c();
                    bk.Deal_SR__c=nb.id;
                    insert bk;
                    
                    
                    
                    Booking_Unit__c bu=new Booking_Unit__c();
                    bu.Unit_Name__c='test1234';
                    bu.Inventory__c=inv.id;
                    bu.Booking__c=bk.id;
                    insert bu;
                    
                     Appointment__c ap=new Appointment__c();
                    ap.Appointment_Date__c=System.today() + 5;
                    ap.Process_Name__c='Generic';
                    ap.Sub_Process_Name__c='Head Office Appointment';
                    ap.Time_Slot_Sub__c =8;
                    ap.Slots__c='15:00 - 16:00';
                    ap.Building__c=l.id;
                    insert ap;
                     Appointment__c ap1=new Appointment__c();
                    ap1.Appointment_Date__c=System.today() + 5;
                    ap1.Process_Name__c='Generic';
                    ap1.Sub_Process_Name__c='Head Office Appointment';
                    ap1.Time_Slot_Sub__c =8;
                    ap1.Slots__c='15:00 - 16:00';
                    insert ap1;
                    Calling_List__c objCallList = new Calling_List__c();
                    objCallList.Service_Type__c = '01-Handover';
                    objCallList.Sub_Purpose__c ='Unit Viewing';
                    objCallList.Appointment_Date__c = System.today() + 2;
                    objCallList.Appointment__c= ap.id;
                    objCallList.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList.Appointment_Origin__c='Mobile';
                    insert objCallList;
                    Calling_List__c objCallList1 = new Calling_List__c();
                    objCallList1.Service_Type__c = '01-Handover';
                    objCallList1.Sub_Purpose__c ='Unit Viewing';
                    objCallList1.Appointment_Date__c = System.today() + 2;
                   // objCallList1.Appointment__c= ap.id;
                    objCallList1.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList1.Appointment_Origin__c='Mobile';
                    insert objCallList1;
                    
                   
      SendAvailableAppointmentsToMObileApp.getAppointments('01-Handover','Unit Viewing','2018-12-08',l.Location_ID__c);
    }
    
    
     public  testmethod static void method3()
    {
    
                    TriggerOnOffCustomSetting__c callingTriggerInst =new TriggerOnOffCustomSetting__c ();
                    callingTriggerInst.OnOffCheck__c=true;
                    callingTriggerInst.name='CallingListTrigger';
                    insert callingTriggerInst; 
                    
                    Location__c l=new Location__c();
                    l.Name='test1234';
                     l.Location_ID__c='test1234';
                     insert l;
                     Appointment__c ap=new Appointment__c();
                    ap.Appointment_Date__c=System.today() + 5;
                    ap.Process_Name__c='Generic';
                    ap.Sub_Process_Name__c='Head Office Appointment';
                    ap.Time_Slot_Sub__c =8;
                    ap.Slots__c='15:00 - 16:00';
                    ap.Building__c=l.id;
                    insert ap;
                     Appointment__c ap1=new Appointment__c();
                    ap1.Appointment_Date__c=System.today() + 5;
                    ap1.Process_Name__c='Generic';
                    ap1.Sub_Process_Name__c='Head Office Appointment';
                    ap1.Time_Slot_Sub__c =8;
                    ap1.Slots__c='15:00 - 16:00';
                    insert ap1;
                    Calling_List__c objCallList = new Calling_List__c();
                    objCallList.Service_Type__c = '01-Handover';
                    objCallList.Sub_Purpose__c ='Unit Viewing';
                    objCallList.Appointment_Date__c = System.today() + 2;
                    objCallList.Appointment__c= ap.id;
                    objCallList.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList.Appointment_Origin__c='Mobile';
                    insert objCallList;
                    Calling_List__c objCallList1 = new Calling_List__c();
                    objCallList1.Service_Type__c = '01-Handover';
                    objCallList1.Sub_Purpose__c ='Unit Viewing';
                    objCallList1.Appointment_Date__c = System.today() + 2;
                   // objCallList1.Appointment__c= ap.id;
                    objCallList1.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList1.Appointment_Origin__c='Mobile';
                    insert objCallList1;
                    
                   
      SendAvailableAppointmentsToMObileApp.getAppointments('12-Cheque Collection','Unit Viewing','2018-12-09',l.id);
    }
      public  testmethod static void method2()
    {
    
                    TriggerOnOffCustomSetting__c callingTriggerInst =new TriggerOnOffCustomSetting__c ();
                    callingTriggerInst.OnOffCheck__c=true;
                    callingTriggerInst.name='CallingListTrigger';
                    insert callingTriggerInst; 
                    
                    Location__c l=new Location__c();
                    l.Name='test1234';
                     l.Location_ID__c='test1234';
                     insert l;
                     Appointment__c ap=new Appointment__c();
                    ap.Appointment_Date__c=System.today() + 5;
                    ap.Process_Name__c='Generic';
                    ap.Sub_Process_Name__c='Head Office Appointment';
                    ap.Time_Slot_Sub__c =8;
                    ap.Slots__c='15:00 - 16:00';
                    ap.Building__c=l.id;
                    insert ap;
                     Appointment__c ap1=new Appointment__c();
                    ap1.Appointment_Date__c=System.today() + 5;
                    ap1.Process_Name__c='Generic';
                    ap1.Sub_Process_Name__c='Head Office Appointment';
                    ap1.Time_Slot_Sub__c =8;
                    ap1.Slots__c='15:00 - 16:00';
                    insert ap1;
                    Calling_List__c objCallList = new Calling_List__c();
                    objCallList.Service_Type__c = '01-Handover';
                    objCallList.Sub_Purpose__c ='Unit Viewing';
                    objCallList.Appointment_Date__c = System.today() + 2;
                    objCallList.Appointment__c= ap.id;
                    objCallList.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList.Appointment_Origin__c='Mobile';
                    insert objCallList;
                    Calling_List__c objCallList1 = new Calling_List__c();
                    objCallList1.Service_Type__c = '01-Handover';
                    objCallList1.Sub_Purpose__c ='Unit Viewing';
                    objCallList1.Appointment_Date__c = System.today() + 2;
                   // objCallList1.Appointment__c= ap.id;
                    objCallList1.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList1.Appointment_Origin__c='Mobile';
                    insert objCallList1;
                    
                   
      SendAvailableAppointmentsToMObileApp.getAppointments('12-Cheque Collection','Unit Viewing','2018-12-08',l.id);
    }
    
     public  testmethod static void method4()
    {
    
                    TriggerOnOffCustomSetting__c callingTriggerInst =new TriggerOnOffCustomSetting__c ();
                    callingTriggerInst.OnOffCheck__c=true;
                    callingTriggerInst.name='CallingListTrigger';
                    insert callingTriggerInst; 
                    
                    Location__c l=new Location__c();
                    l.Name='test1234';
                     l.Location_ID__c='test1234';
                     insert l;
                     Appointment__c ap=new Appointment__c();
                    ap.Appointment_Date__c=System.today() + 5;
                    ap.Process_Name__c='Generic';
                    ap.Sub_Process_Name__c='Head Office Appointment';
                    ap.Time_Slot_Sub__c =8;
                    ap.Slots__c='15:00 - 16:00';
                    ap.Building__c=l.id;
                    insert ap;
                     Appointment__c ap1=new Appointment__c();
                    ap1.Appointment_Date__c=System.today() + 5;
                    ap1.Process_Name__c='Generic';
                    ap1.Sub_Process_Name__c='Head Office Appointment';
                    ap1.Time_Slot_Sub__c =8;
                    ap1.Slots__c='15:00 - 16:00';
                    insert ap1;
                    Calling_List__c objCallList = new Calling_List__c();
                    objCallList.Service_Type__c = '01-Handover';
                    objCallList.Sub_Purpose__c ='Unit Viewing';
                    objCallList.Appointment_Date__c = System.today() + 2;
                    objCallList.Appointment__c= ap.id;
                    objCallList.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList.Appointment_Origin__c='Mobile';
                    insert objCallList;
                    Calling_List__c objCallList1 = new Calling_List__c();
                    objCallList1.Service_Type__c = '01-Handover';
                    objCallList1.Sub_Purpose__c ='Unit Viewing';
                    objCallList1.Appointment_Date__c = System.today() + 2;
                   // objCallList1.Appointment__c= ap.id;
                    objCallList1.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList1.Appointment_Origin__c='Mobile';
                    insert objCallList1;
                    
                   
      SendAvailableAppointmentsToMObileApp.getAppointments('08-Rental Pool','Unit Viewing','2018-12-09',l.id);
    }
      public  testmethod static void method5()
    {
    
                    TriggerOnOffCustomSetting__c callingTriggerInst =new TriggerOnOffCustomSetting__c ();
                    callingTriggerInst.OnOffCheck__c=true;
                    callingTriggerInst.name='CallingListTrigger';
                    insert callingTriggerInst; 
                    
                    Location__c l=new Location__c();
                    l.Name='test1234';
                     l.Location_ID__c='test1234';
                     insert l;
                     Appointment__c ap=new Appointment__c();
                    ap.Appointment_Date__c=System.today() + 5;
                    ap.Process_Name__c='Generic';
                    ap.Sub_Process_Name__c='Head Office Appointment';
                    ap.Time_Slot_Sub__c =8;
                    ap.Slots__c='15:00 - 16:00';
                    ap.Building__c=l.id;
                    insert ap;
                     Appointment__c ap1=new Appointment__c();
                    ap1.Appointment_Date__c=System.today() + 5;
                    ap1.Process_Name__c='Generic';
                    ap1.Sub_Process_Name__c='Head Office Appointment';
                    ap1.Time_Slot_Sub__c =8;
                    ap1.Slots__c='15:00 - 16:00';
                    insert ap1;
                    Calling_List__c objCallList = new Calling_List__c();
                    objCallList.Service_Type__c = '01-Handover';
                    objCallList.Sub_Purpose__c ='Unit Viewing';
                    objCallList.Appointment_Date__c = System.today() + 2;
                    objCallList.Appointment__c= ap.id;
                    objCallList.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList.Appointment_Origin__c='Mobile';
                    insert objCallList;
                    Calling_List__c objCallList1 = new Calling_List__c();
                    objCallList1.Service_Type__c = '01-Handover';
                    objCallList1.Sub_Purpose__c ='Unit Viewing';
                    objCallList1.Appointment_Date__c = System.today() + 2;
                   // objCallList1.Appointment__c= ap.id;
                    objCallList1.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList1.Appointment_Origin__c='Mobile';
                    insert objCallList1;
                    
                   
      SendAvailableAppointmentsToMObileApp.getAppointments('08-Rental Pool','Unit Viewing','2018-12-08',l.id);
    }
    
    
     public  testmethod static void method6()
    {
    
                    TriggerOnOffCustomSetting__c callingTriggerInst =new TriggerOnOffCustomSetting__c ();
                    callingTriggerInst.OnOffCheck__c=true;
                    callingTriggerInst.name='CallingListTrigger';
                    insert callingTriggerInst; 
                    
                    Location__c l=new Location__c();
                    l.Name='test1234';
                     l.Location_ID__c='test1234';
                     insert l;
                     Appointment__c ap=new Appointment__c();
                    ap.Appointment_Date__c=System.today() + 5;
                    ap.Process_Name__c='Generic';
                    ap.Sub_Process_Name__c='Head Office Appointment';
                    ap.Time_Slot_Sub__c =8;
                    ap.Slots__c='15:00 - 16:00';
                    ap.Building__c=l.id;
                    insert ap;
                     Appointment__c ap1=new Appointment__c();
                    ap1.Appointment_Date__c=System.today() + 5;
                    ap1.Process_Name__c='Generic';
                    ap1.Sub_Process_Name__c='Head Office Appointment';
                    ap1.Time_Slot_Sub__c =8;
                    ap1.Slots__c='15:00 - 16:00';
                    insert ap1;
                    Calling_List__c objCallList = new Calling_List__c();
                    objCallList.Service_Type__c = '01-Handover';
                    objCallList.Sub_Purpose__c ='Unit Viewing';
                    objCallList.Appointment_Date__c = System.today() + 2;
                    objCallList.Appointment__c= ap.id;
                    objCallList.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList.Appointment_Origin__c='Mobile';
                    insert objCallList;
                    Calling_List__c objCallList1 = new Calling_List__c();
                    objCallList1.Service_Type__c = '01-Handover';
                    objCallList1.Sub_Purpose__c ='Unit Viewing';
                    objCallList1.Appointment_Date__c = System.today() + 2;
                   // objCallList1.Appointment__c= ap.id;
                    objCallList1.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList1.Appointment_Origin__c='Mobile';
                    insert objCallList1;
                    
                   
      SendAvailableAppointmentsToMObileApp.getAppointments('Mortgage','Unit Viewing','2018-12-07',l.id);
    }
      public  testmethod static void method7()
    {
    
                    TriggerOnOffCustomSetting__c callingTriggerInst =new TriggerOnOffCustomSetting__c ();
                    callingTriggerInst.OnOffCheck__c=true;
                    callingTriggerInst.name='CallingListTrigger';
                    insert callingTriggerInst; 
                    
                    Location__c l=new Location__c();
                    l.Name='test1234';
                     l.Location_ID__c='test1234';
                     insert l;
                     Appointment__c ap=new Appointment__c();
                    ap.Appointment_Date__c=System.today() + 5;
                    ap.Process_Name__c='Generic';
                    ap.Sub_Process_Name__c='Head Office Appointment';
                    ap.Time_Slot_Sub__c =8;
                    ap.Slots__c='15:00 - 16:00';
                    ap.Building__c=l.id;
                    insert ap;
                     Appointment__c ap1=new Appointment__c();
                    ap1.Appointment_Date__c=System.today() + 5;
                    ap1.Process_Name__c='Generic';
                    ap1.Sub_Process_Name__c='Head Office Appointment';
                    ap1.Time_Slot_Sub__c =8;
                    ap1.Slots__c='15:00 - 16:00';
                    insert ap1;
                    Calling_List__c objCallList = new Calling_List__c();
                    objCallList.Service_Type__c = '01-Handover';
                    objCallList.Sub_Purpose__c ='Unit Viewing';
                    objCallList.Appointment_Date__c = System.today() + 2;
                    objCallList.Appointment__c= ap.id;
                    objCallList.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList.Appointment_Origin__c='Mobile';
                    insert objCallList;
                    Calling_List__c objCallList1 = new Calling_List__c();
                    objCallList1.Service_Type__c = '01-Handover';
                    objCallList1.Sub_Purpose__c ='Unit Viewing';
                    objCallList1.Appointment_Date__c = System.today() + 2;
                   // objCallList1.Appointment__c= ap.id;
                    objCallList1.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList1.Appointment_Origin__c='Mobile';
                    insert objCallList1;
                    
                   
      SendAvailableAppointmentsToMObileApp.getAppointments('Mortgage','Unit Viewing','2018-12-08',l.id);
    }
    
     public  testmethod static void method8()
    {
    
                    TriggerOnOffCustomSetting__c callingTriggerInst =new TriggerOnOffCustomSetting__c ();
                    callingTriggerInst.OnOffCheck__c=true;
                    callingTriggerInst.name='CallingListTrigger';
                    insert callingTriggerInst; 
                    
                    Location__c l=new Location__c();
                    l.Name='test1234';
                     l.Location_ID__c='test1234';
                     insert l;
                     Appointment__c ap=new Appointment__c();
                    ap.Appointment_Date__c=System.today() + 5;
                    ap.Process_Name__c='Generic';
                    ap.Sub_Process_Name__c='Head Office Appointment';
                    ap.Time_Slot_Sub__c =8;
                    ap.Slots__c='15:00 - 16:00';
                    ap.Building__c=l.id;
                    insert ap;
                     Appointment__c ap1=new Appointment__c();
                    ap1.Appointment_Date__c=System.today() + 5;
                    ap1.Process_Name__c='Generic';
                    ap1.Sub_Process_Name__c='Head Office Appointment';
                    ap1.Time_Slot_Sub__c =8;
                    ap1.Slots__c='15:00 - 16:00';
                    insert ap1;
                    Calling_List__c objCallList = new Calling_List__c();
                    objCallList.Service_Type__c = '01-Handover';
                    objCallList.Sub_Purpose__c ='Unit Viewing';
                    objCallList.Appointment_Date__c = System.today() + 2;
                    objCallList.Appointment__c= ap.id;
                    objCallList.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList.Appointment_Origin__c='Mobile';
                    insert objCallList;
                    Calling_List__c objCallList1 = new Calling_List__c();
                    objCallList1.Service_Type__c = '01-Handover';
                    objCallList1.Sub_Purpose__c ='Unit Viewing';
                    objCallList1.Appointment_Date__c = System.today() + 2;
                   // objCallList1.Appointment__c= ap.id;
                    objCallList1.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList1.Appointment_Origin__c='Mobile';
                    insert objCallList1;
                    
                   
      SendAvailableAppointmentsToMObileApp.getAppointments('','','2018-12-09',l.id);
    }
     public  testmethod static void method9()
    {
    
                    TriggerOnOffCustomSetting__c callingTriggerInst =new TriggerOnOffCustomSetting__c ();
                    callingTriggerInst.OnOffCheck__c=true;
                    callingTriggerInst.name='CallingListTrigger';
                    insert callingTriggerInst; 
                    
                    Location__c l=new Location__c();
                    l.Name='test1234';
                     l.Location_ID__c='test1234';
                     insert l;
                     Appointment__c ap=new Appointment__c();
                    ap.Appointment_Date__c=System.today() + 5;
                    ap.Process_Name__c='Generic';
                    ap.Sub_Process_Name__c='Head Office Appointment';
                    ap.Time_Slot_Sub__c =8;
                    ap.Slots__c='15:00 - 16:00';
                    ap.Building__c=l.id;
                    insert ap;
                     Appointment__c ap1=new Appointment__c();
                    ap1.Appointment_Date__c=System.today() + 5;
                    ap1.Process_Name__c='Generic';
                    ap1.Sub_Process_Name__c='Head Office Appointment';
                    ap1.Time_Slot_Sub__c =8;
                    ap1.Slots__c='15:00 - 16:00';
                    insert ap1;
                    Calling_List__c objCallList = new Calling_List__c();
                    objCallList.Service_Type__c = '01-Handover';
                    objCallList.Sub_Purpose__c ='Unit Viewing';
                    objCallList.Appointment_Date__c = System.today() + 2;
                    objCallList.Appointment__c= ap.id;
                    objCallList.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList.Appointment_Origin__c='Mobile';
                    insert objCallList;
                    Calling_List__c objCallList1 = new Calling_List__c();
                    objCallList1.Service_Type__c = '01-Handover';
                    objCallList1.Sub_Purpose__c ='Unit Viewing';
                    objCallList1.Appointment_Date__c = System.today() + 2;
                   // objCallList1.Appointment__c= ap.id;
                    objCallList1.Appointment_Status__c = 'Requested';
                   // objCallList.Handover_Team_Email__c = hoTeamEmail;
                    objCallList1.Appointment_Origin__c='Mobile';
                    insert objCallList1;
                    
                   
      SendAvailableAppointmentsToMObileApp.getAppointments('','','2018-12-07',l.id);
    }
 }