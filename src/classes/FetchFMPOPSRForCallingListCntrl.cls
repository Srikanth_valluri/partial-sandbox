public without sharing class FetchFMPOPSRForCallingListCntrl {
    
    public String callingListId                     {get;set;}
    public Id caseRecordTypeId                      {get;set;}
    public Id callingListRecordTypeId               {get;set;}
    public List<Calling_List__c> callingList        {get;set;}
    public List<Id>    accList                      {get;set;}
    public List<FM_Case__c>  caseList               {get;set;}
    
    public FetchFMPOPSRForCallingListCntrl(ApexPages.StandardController controller) {
        fetchPOPSRs();
    }
    
    /*This Method is used to fetch all the FM POP cases*/
    public void fetchPOPSRs(){
        
        callingList = new List<Calling_List__c>();
        accList = new List<Id>();
        caseList = new List<FM_Case__c>();
        caseRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Proof Of Payment').getRecordTypeId();
        callingListRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collections').RecordTypeId;
        callingListId = ApexPages.currentPage().getParameters().get('id');
        callingListId = String.valueOf(callingListId).substring(0, 15);
        
        callingList = [ SELECT Id,
                            Name,
                            Account__r.Name,
                            Account__c,
                            RecordTypeId,
                            Case__c,
                            RecordType.Name
                       FROM Calling_List__c
                      WHERE Id = :callingListId
                        AND RecordTypeId = :callingListRecordTypeId ];

        for( Calling_List__c callingInst : callingList ) {
            if( callingInst.Account__c != null ) {
                accList.add(callingInst.Account__c);
            }
        }
        
        System.debug('accList::::'+accList);
        
        if( accList != null && accList.isEmpty() == false ) {
                caseList = [SELECT Id,
                               Name,Total_Amount__c,
                               Status__c ,Origin__c,
                               RecordType.Name,
                               Payment_Date__c ,
                               Payment_Mode__c ,
                               Request_Type_DeveloperName__c 
                          FROM FM_Case__c
                         WHERE Account__c IN :accList
                           AND RecordTypeId = :caseRecordTypeId
                      ORDER BY CreatedDate desc limit 950 ];
                      
             System.debug('caseList::in pop::'+caseList);
        
        }//if
   
   }//fetchCases
}