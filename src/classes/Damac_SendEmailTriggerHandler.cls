Public without sharing class Damac_SendEmailTriggerHandler {
    
    public static void sendEmailSync (List <Id> newRecordIds) {
        sendEmail (newRecordIds);
    }
    @Future (callout = TRUE)
    public static void sendEmailAsync (List <Id> newRecordIds) {
        sendEmail (newRecordIds);
    }    
    
    public static void sendEmail (List <Id> newRecordIds) {
        String query = ' SELECT Account__r.Email__c , Inquiry__r.Email__c, Email_Request__r.Substitution_Tags__c, Email_Request__r.Email_Template__c, '
                        +getAllFields ('Email_Metrics__c')
                        +' FROM Email_Metrics__c WHERE ID IN: newRecordIds';
        
        Sendgrid_Credentials__c credentials = Sendgrid_Credentials__c.getInstance (UserInfo.getUserID ());
        Map <ID, String> responseMap = new Map <ID, String> ();
        Map <ID, String> errorResponseMap = new Map <ID, String> ();
        List <Email_Metrics__c> emailMetricsList = new List <Email_Metrics__c> ();
        emailMetricsList = Database.query (query);
        Set <ID> inqIds = new Set <ID> ();
        Set <ID> accIds = new Set <ID> ();
        Set <ID> projectDetailRecIds = new Set <ID> ();
        
        for (Email_Metrics__c metrics: emailMetricsList) {
            if (metrics.Inquiry__c != NULL)
                inqIds.add (metrics.Inquiry__c);
            if (metrics.Account__c != NULL)
                accIds.add (metrics.Account__c);
            
            projectDetailRecIds.add(metrics.Share_Project_Details__c);
        }
        Map <ID, Inquiry__c> inqMap = new Map <ID, Inquiry__c> (); 
        Map <ID, Account> accMap = new Map <ID, Account> ();               
        if (inqIds.size () > 0) {
            String inqQuery = 'SELECT '+getAllFields ('Inquiry__c')+' FROM Inquiry__c WHERE ID IN :inqIds ';
            
            for (Inquiry__c inq: Database.Query (inqQuery)) {
                inqMap.put (inq.ID, inq);
            } 
        }
        if (accIds.size () > 0) {
            String accQuery = 'SELECT '+getAllFields ('Account')+' FROM Account WHERE ID IN :accIds ';
            
            for (Account acc: Database.Query (accQuery)) {
                accMap.put (acc.ID, acc);
            } 
        }
        // Project Details       
        String pdQuery = 'SELECT '+getAllFields ('Share_Project_Details__c')+' FROM Share_Project_Details__c WHERE ID IN :projectDetailRecIds';
        Map <ID, Share_Project_Details__c> projDetailsMap = new Map <ID, Share_Project_Details__c> ();
        for (Share_Project_Details__c pd: Database.Query (pdQuery)) {
            projDetailsMap.put (pd.ID, pd);
        }
        
        for (Email_Metrics__c metrics: emailMetricsList) {
            String email = '';
            if (metrics.Inquiry__c != NULL)
                email = metrics.Inquiry__r.Email__c;
            
            if (metrics.Account__c != NULL)
                email = metrics.Account__r.Email__c;
            if (Email != NULL) {
                String substitutionValues = '';
                Map <String, String> substitutionTagsMap = new Map <String, String> ();
                if (metrics.Email_Request__r.Substitution_Tags__c != NULL) {
                    
                    for (String key :DamacUtility.splitMutliSelect(metrics.Email_Request__r.Substitution_Tags__c)) {
                        String apiName = key.removeStart ('-').removeEnd ('-');
                        
                        String val = '';
                        if (metrics.Inquiry__c != NULL && key != '-Email_Metrics_ID-')
                            val = String.valueOf (inqMap.get (metrics.Inquiry__c).get (apiName));
                        if (metrics.Account__c != NULL && key != '-Email_Metrics_ID-')
                            val = String.valueOf (accMap.get (metrics.Account__c).get (apiName));
                            
                        if (val == NULL)
                            val = '';
                        substitutionTagsMap.put (key, val);
                    }
                    // Add Comments
                    String pdCommentsVal = '';
                    if (metrics.Share_Project_Details__c != NULL) {
                        if (projDetailsMap.containskey (metrics.Share_Project_Details__c)) {
                            if (projDetailsMap.get (metrics.Share_Project_Details__c).get ('Comments__c') != NULL)
                                pdCommentsVal = String.valueOf (projDetailsMap.get (metrics.Share_Project_Details__c).get ('Comments__c'));                        
                        }
                        
                    }
                    substitutionTagsMap.put('-Comments__c-', pdCommentsVal);
                    
                }        
                JSONGenerator gen = formatRequestBody (metrics.ID, Email, '', 
                                    Credentials.From_name__c, credentials.From_Email__c,
                                    '', '', metrics.Email_Request__r.Email_Template__c, substitutionTagsMap, 
                                    metrics.SalesOffer_Record__c);
                                    
                HTTPRequest req = new HTTPRequest ();
                req.setEndpoint (credentials.Endpoint_URL__c);
                req.setMethod ('POST');
                System.Debug ('Request Body :::: '+gen.getAsString());
                req.setBody(gen.getAsString());
                req.setHeader('Authorization', 'Bearer '+credentials.API_Key__c);
                req.setHeader('content-type', 'application/json');
                HTTP http = new HTTP ();
                HTTPResponse res = new HTTPresponse ();
                if (!Test.isRunningTest ())
                    res = http.send (req);
                if (Test.isRunningTest ()) {
                    res.setStatusCode (202);
                    res.setheader ('X-Message-Id', 'dfadsfasf11fsdfa');
                }
                if (res.getStatusCode() == 202) {
                    responseMap.put (metrics.ID, res.getHeader('X-Message-Id'));
                } else {
                    SendGridErrorHandler errorData = SendGridErrorHandler.parse(res.getBody());
                    if (errorData != null) {
                        if ( errorData.errors != null) 
                            errorResponseMap.put (metrics.ID, errorData.errors[0].message);
                    }
                }
            }
        }  
        List <Email_Metrics__c> metricsToUpdate = new List <Email_Metrics__c> ();
        for (ID key :responseMap.keySet ()) {
            Email_Metrics__c metric = new Email_Metrics__c ();
            metric.ID = key;
            metric.Email_Sent_Date_Time__c = DateTime.Now ();
            metric.Sendgrid_Email_Message_Id__c = responseMap.get (key);
            metric.Error_Response__c = NULL;
            metric.Email_Send__c = TRUE;
            metricsToUpdate.add (metric);
        }
        for (ID key :errorResponseMap.keySet ()) {
            Email_Metrics__c metric = new Email_Metrics__c ();
            metric.ID = key;
            metric.Email_Send__c = TRUE;
            metric.Error_Response__c = errorResponseMap.get (key);
            metricsToUpdate.add (metric);
        }
        Damac_Constants.skip_EmailMetricsTrigger = TRUE;
        Update metricsToUpdate;
        
    }
    public static JSONGenerator formatRequestBody (ID uniqueId, String toAddresses, String name, 
                                        String senderName, String senderEmail, String replyTo, 
                                        String subject, String templateId, 
                                        Map <String, String> substitutionTags, ID offerID) {
        
        System.Debug ('JSON Creation ::::');
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
            gen.writeFieldName('personalizations');
                gen.writeStartArray();
                    gen.writeStartObject();
                        gen.writeFieldName('to');
                            gen.writeStartArray();
                                gen.writeStartObject();
                                    gen.writeStringField('email', toAddresses);
                                    //gen.writeStringField('name', name);
                                gen.writeEndObject();
                            gen.writeEndArray();
                        gen.writeStringField('subject', Subject);
                        
                        gen.writeFieldName('substitutions');
                        gen.writeStartObject();
                        for (String key :substitutionTags.keySet ()) {

                            String val = substitutionTags.get (key);
                            if (key == '-Email_Metrics_ID-') {
                                val = uniqueId;
                            }
                            /*
                            if (key == '-Email__c-') {
                                if (val != NULL) {
                                    if (val.contains ('@')) {
                                        String temp = val.split ('@')[0];
                                        Integer length = temp.length ();
                                        String splChars = '';
                                        for (integer i = 0; i < length; i++)
                                            splChars += '*'; 
                                        temp = temp.subString (0, 1)+splChars+temp.subString (length-1, length);
                                        val = temp+'@'+val.split ('@')[1];
                                        System.Debug (val);
                                        
                                    }
                                }
                            }
                            if (key == '-Mobile_Phone__c-') {
                                if (val != NULL) {
                                    String temp = val;
                                    Integer length = temp.length ();
                                    String splChars = '';
                                    if (length > 6) {
                                        for (integer i = 0; i < length-6; i++)
                                            splChars += '*';
                                        temp = temp.subString (0, 3)+splChars+temp.subString (length-3, length); 
                                    }
                                    if (length <= 6) {
                                        
                                        splChars = '****';
                                        temp = temp.subString (0, 1)+splChars+temp.subString (length-1, length); 
                                    }
                                    
                                    
                                    val = temp;
                                    System.Debug (val);
                                        
                                    
                                }
                            }*/
                            if (val != NULL)
                                gen.writeStringField(key, val);                          
                            else
                                gen.writeStringField(key, '');                          
                        
                        }
                        gen.writeEndObject();
                    gen.writeEndObject();
                gen.writeEndArray();
            gen.writeFieldName('from');
            gen.writeStartObject();
            gen.writeStringField('email', senderEmail);
            gen.writeStringField('name', senderName);
            gen.writeEndObject();
            
            
            gen.writeFieldName('tracking_settings');
            gen.writeStartObject();
                gen.writeFieldName('click_tracking');
                gen.writeStartObject();
                    gen.writeBooleanField('enable', true);
                gen.writeEndObject();
            gen.writeEndObject();
            
            if (replyTo != NULL && replyTo.trim() != '') {
                gen.writeFieldName('reply_to');
                gen.writeStartObject();
                gen.writeStringField('email', replyTo);
                gen.writeEndObject();
            }
            
            gen.writeFieldName('custom_args');
            gen.writeStartObject();
            gen.writeStringField('uniqueId', UniqueId);
            gen.writeEndObject();
            if (offerID != NULL) {
                String attachmentBody;
               
                PageReference redirectPage = Page.Damac_InventoryDetail_Clone;
                redirectPage.getParameters().put('recID', offerid);
                if (!Test.isRunningTest ()) {
                    Blob mailattachment = redirectPage.getContentAsPDF();
                    attachmentBody = EncodingUtil.base64Encode (mailattachment);
                }
                if (Test.isRunningTest ()) {
                    attachmentBody = EncodingUtil.base64Encode (blob.valueOf ('eafsafafas'));
                }
            
                //Attachments
                gen.writeFieldName('attachments');
                gen.writeStartArray();
                    gen.writeStartObject();
                        gen.writeStringField('content', attachmentBody);
                        gen.writeStringField('type', 'application/pdf');
                        gen.writeStringField('filename','Damac Sales Offer.pdf');
                    gen.writeEndObject();
                gen.writeEndArray();
            }
            gen.writeStringField ('template_id', templateID);
        gen.writeEndObject();
        System.Debug ('JSON Creation ::::'+gen);
        return gen;
    }
    public static string getAllfields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        }
        for (string f :objectType.getDescribe ().fields.getMap ().keySet ())
            fields += f+ ', ';
        return fields.removeEnd(', '); 
        
    }
}