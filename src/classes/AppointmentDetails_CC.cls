public without sharing class  AppointmentDetails_CC{
    public string strSelectedAccount {get;set;}
    public Account objAcc {get;set;}
    public List<Calling_List__c > lstCalling {get;set;}
    public String callingListID {get;set;}
    public String redirect {get;set;}
    public boolean blnShowDetails {get;set;}

    
    public String getName() {       
        System.debug('--getNamest strSelectedAccount ---'+strSelectedAccount );       
        if(String.isNotBlank(strSelectedAccount)){            
            init();
        }
        else{
             ApexPages.addmessage(new ApexPages.message(
             ApexPages.severity.Error, 'Account details not present'));
        }
        return strSelectedAccount;
    }
    
    public void init(){
        objAcc = new Account();
        redirect = 'No'; 
        blnShowDetails = false;       
            objAcc = [Select id,name from Account where id =: strSelectedAccount];
            lstCalling = new List<Calling_List__c>();
            lstCalling   = [Select id,name,OwnerID, Account__c,Appointment_Date__c,Account_Email__c,
                             Account__r.Name,Account__r.Email__c,Account__r.PersonEmail,
                             Appointment_Slot__c,Account_Name_for_Walk_In__c,Appointment_Status__c,Booking_Unit_Name__c,
                             Registration_ID__c,
                             RecordType.DeveloperName, Assigned_CRE__c,Assigned_CRE__r.name,Service_Type__c, 
                             Sub_Purpose__c,
                             Assigned_CRE__r.id from Calling_List__c where Account__c =: strSelectedAccount 
                             AND RecordType.DeveloperName = 'Appointment_Scheduling' order By name Desc ];
                                                              
          if(lstCalling.Size()>0){
              blnShowDetails = true;       
          }
          else{
              ApexPages.addmessage(new ApexPages.message(
              ApexPages.severity.Info, 'No Appointment is currently scheduled'));
          }
    }
    
    public void cancelAppointment(){
        System.debug('-----cancelAppointment---'+callingListID);
        try{
            redirect = 'No';
            if(String.isNotBlank(callingListID)){
                Calling_List__c obj = new Calling_List__c();
                obj = [Select id,name, Appointment_Status__c                      
                      from Calling_List__c 
                      where id =: callingListID 
                      AND Appointment_Status__c != 'Cancelled' ];
                      
                if(obj != null){
                    obj.Appointment_Status__c = 'Cancelled';
                    update obj;
                }    
                System.debug('---obj---'+obj);
                 
                Event objEvent = new Event();
                objEvent = [Select id, status__c from Event where whatId =: callingListID limit 1];
                                   
                    System.debug('--objEvent ---'+objEvent );
                    
                    if(objEvent != null){
                        System.debug('---objEvent inside---');
                        objEvent.Status__c  = 'Cancelled';
                        update objEvent;
                    }
                                    
                    
                    redirect = 'Yes';
                }      
        }
            catch(exception ex){
                 ApexPages.addmessage(new ApexPages.message(
                 ApexPages.severity.Error, ex.getMessage()));
            }
        }
        
        
    
}