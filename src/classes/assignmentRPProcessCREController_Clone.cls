/*-------------------------------------------------------------------------------------------------
Description: Controller for RentalPoolAssignmentCRE_Clone

============================================================================================================================
    Version | Date(DD-MM-YYYY) | Comments
----------------------------------------------------------------------------------------------------------------------------
    1.0     | 12-02-2018       | Draft for Rental Pool Assignment functionality
=============================================================================================================================
*/

public without sharing class assignmentRPProcessCREController_Clone extends assignmentRPProcessCREController{

    public assignmentRPProcessCREController_Clone () {
        super(false);
        if (CustomerCommunityUtils.isCurrentView('rentalpoolassignment')) {
            super();
        }
    }

    public override void init(){
        system.debug('init called*********************');
        objUnit = new Booking_Unit__c();
        objBooking = new Booking__c();
        objWrap = null;
        objPaymentPlan = null;
        blnSectionOne = false;
        blnSectionTwo = false;
        blnSectionThree = false;
        blnSectionFour = false;
        blnSectionAddDocs = false;
        blnSectionDocs = false;
        nocIssuedInLast15Days = false;
        blnShowDocs = false;
        blnBuyerPOA = false;
        blnSellerPOA = false;
        blnAddDocsPanel = false;
        blnEnableReadOnlyMode = false;
        blnLoaSubmitted = false;
        listPaymentPlanObj = new list<Payment_Plan__c>();
        lstSrAttachment = new list<SR_Attachments__c>();
        mapId_BookingUnit = new map<Id,Booking_Unit__c>();
        if(strAccoundId == null) {
            strAccoundId = '0017E00000aqdaS';
        }
        lstUnits = new List<SelectOption>();
        if(String.isNotBlank(strCaseId)){
            system.debug('aya kya re andara******');
            list<Case> lstC = [Select Id
                                    , CaseNumber
                                    , Origin
                                    , Status
                                    , Assignment_Fee__c
                                    , LOA_Submitted__c
                                    , Buyer_Type__c
                                    , Relationship_with_Seller__c
                                    , Case_Type__c
                                    , Buyer_Info__c
                                    , Seller_POA__c
                                    , Buyer_POA_Country_Code__c
                                    , Buyer_POA_Phone__c
                                    , Seller_POA_Country_Code__c
                                    , Seller_POA_Phone__c
                                    , POA_Name_Seller__c
                                    , Purpose_of_POA_Seller__c
                                    , POA_Issued_By_Seller__c
                                    , Buyer_POA__c
                                    , POA_Name__c
                                    , Purpose_of_POA__c
                                    , POA_Issued_By__c
                                    , POA_Expiry_Date__c
                                    , POA_Expiry_Date_Seller__c
                                    , Seller__c
                                    , Buyer__c
                                    , Buyer__r.Booking__c
                                    , Buyer__r.Organisation_Name__c
                                    , Buyer__r.CR_Registration_Expiry_Date__c
                                    , Buyer__r.CR_Registration_Place__c
                                    , Buyer__r.Organisation_Name_Arabic__c
                                    , Buyer__r.CR_Number__c
                                    , Buyer__r.CR_Registration_Place_Arabic__c
                                    , Buyer__r.Title__c
                                    , Buyer__r.First_Name__c
                                    , Buyer__r.Last_Name__c
                                    , Buyer__r.Nationality__c
                                    , Buyer__r.Passport_Number__c
                                    , Buyer__r.Place_of_Issue__c
                                    , Buyer__r.Address_Line_1__c
                                    , Buyer__r.Address_Line_2__c
                                    , Buyer__r.Address_Line_3__c
                                    , Buyer__r.Address_Line_4__c
                                    , Buyer__r.Country__c
                                    , Buyer__r.City__c
                                    , Buyer__r.Date_of_Birth__c
                                    , Buyer__r.Title_Arabic__c
                                    , Buyer__r.First_Name_Arabic__c
                                    , Buyer__r.Last_Name_Arabic__c
                                    , Buyer__r.Nationality_Arabic__c
                                    , Buyer__r.Passport_Expiry_Date__c
                                    , Buyer__r.Place_of_Issue_Arabic__c
                                    , Buyer__r.Address_Line_1_Arabic__c
                                    , Buyer__r.Address_Line_2_Arabic__c
                                    , Buyer__r.Address_Line_3_Arabic__c
                                    , Buyer__r.Address_Line_4_Arabic__c
                                    , Buyer__r.Country_Arabic__c
                                    , Buyer__r.City_Arabic__c
                                    , Buyer__r.Phone_Country_Code__c
                                    , Buyer__r.Phone__c
                                    , Buyer__r.Email__c
                                    , Booking_Unit__c
                                    , Booking_Unit__r.CurrencyIsoCode
                                    , Booking_Unit__r.Booking__r.Deal_SR__c
                                    , Booking_Unit__r.Registration_ID__c
                                    , Booking_Unit__r.Name
                                    , Booking_Unit__r.Booking__c
                                    , Booking_Unit__r.Unit_Details__c
                                    , Booking_Unit__r.Unit_Type__c
                                    , Booking_Unit__r.Booking__r.Account__c
                                    , Booking_Unit__r.Inventory__c
                                    , Booking_Unit__r.Inventory__r.Property__r.Name
                                    , Booking_Unit__r.Inventory__r.Unit__c
                                    , Booking_Unit__r.Inventory__r.Property_City__c
                                    , Booking_Unit__r.Inventory__r.Unit_Type__c
                                    , Booking_Unit__r.Inventory__r.Property_Status__c
                                    , Booking_Unit__r.Inventory__r.Building_Code__c
                                    , Booking_Unit__r.Inventory__r.Bedroom_Type__c
                                    , Booking_Unit__r.Permitted_Use__c
                                    , Booking_Unit__r.Rental_Pool__c
                                    , Booking_Unit__r.Requested_Price__c
                                    , Booking_Unit__r.Booking_Undergoing_Termination__c
                                    , Booking_Unit__r.NOC_Issued_Date__c
                                    , Booking_Unit__r.Dispute__c
                                    , Booking_Unit__r.Enforcement__c
                                    , Booking_Unit__r.Litigation__c
                                    , Booking_Unit__r.Counter_Case__c
                                    , Booking_Unit__r.Mortgage__c
                                    , Booking_Unit__r.Re_Assigned__c
                                    , Booking_Unit__r.Penalty_Amount__c
                                    , Booking_Unit__r.Token_Amount_val__c
                                    , Booking_Unit__r.Booking_Type__c
                                    , Booking_Unit__r.DP_Overdue__c
                                    , Booking_Unit__r.Plot_Price__c
                                    , Booking_Unit__r.Booking__r.CreatedDate
                                    , Booking_Unit__r.Agreement_Date__c
                                    , Booking_Unit__r.JOPD_Area__c
                                    , Booking_Unit__r.Area__c
                                    , Booking_Unit__r.DP_OK__c
                                    , Booking_Unit__r.Doc_OK__c
                                    , Booking_Unit__r.OQOOD_Reg_Flag__c
                                    , Booking_Unit__r.Early_Handover__c
                                    , Booking_Unit__r.Handover_Flag__c
                                    , Booking_Unit__r.PCC_Release__c
                                    , Booking_Unit__r.Construction_Status__c
                                    , Booking_Unit__r.Area_Varied__c
                                    , Booking_Unit__r.Title_Deed__c
                                    , Booking_Unit__r.Under_Assignment__c
                                    , Booking_Unit__r.Regular_Bulk_Flag__c
                                    , Booking_Unit__r.Registration_DateTime__c
                                    , Booking_Unit__r.Manager_Name__c
                                    , Booking_Unit__r.HOS_Name__c
                                    , Booking_Unit__r.Property_Consultant__c
                                    , Booking_Unit__r.Inventory__r.Floor_Plan__c
                                    , Booking_Unit__r.Inventory__r.Unit_Plan__c
                                    , Booking_Unit__r.Finance_Flag__c
                                    from Case
                                    where Id =: strCaseId
                                    and Booking_Unit__r.Inventory__r.Unit__c != null
                                    and Booking_Unit__r.Inventory__r.Property__c != null
                                    limit 1];
            //system.debug('lstC****************'+lstC);
            if(lstC != null && !lstC.isEmpty()){
                objCase = lstC[0];

                if(!lstC[0].LOA_Submitted__c){
                    blnEnableReadOnlyMode = false;
                }else{
                    blnEnableReadOnlyMode = true;
                }
                if(lstC[0].Buyer__c != null){
                    objBuyer = lstC[0].Buyer__r;
                }else{
                    //system.debug('*****aya bhai*****');
                    objBuyer = new Buyer__c();
                }
                if(lstC[0].Seller__c != null){
                    strAccoundId = lstC[0].Seller__c;
                    //fetchSeller();
                }
                if(lstC[0].Booking_Unit__c != null){
                    objUnit = lstC[0].Booking_Unit__r;
                    //system.debug('objUnit*******objUnit*****'+objUnit);
                    strSelectedUnit = objUnit.Id;
                    if(lstC[0].Buyer__r.Booking__c != null){
                        objBooking.Id = lstC[0].Buyer__r.Booking__c;
                    }
                    lstUnits.add(new selectOption(objUnit.Id, objUnit.Inventory__r.Unit__c));
                    mapId_BookingUnit.put(objUnit.Id, objUnit);
                    fetchUnitDetails();
                    viewPaymentDetails();
                }else{
                    fetchCustomerUnits();
                }
                if(objCase.POA_Expiry_Date__c != null){
                    Time myTime = Time.newInstance(0, 0, 0, 0);
                    DateTime dt = DateTime.newInstanceGMT(objCase.POA_Expiry_Date__c, myTime);
                    system.debug('buyer poa date formatted********'+dt.format('dd/MM/yyyy'));
                    buyerPoaDate = String.valueOf(dt.format('dd/MM/yyyy'));
                }
                if(objCase.POA_Expiry_Date_Seller__c != null){
                    Time myTime = Time.newInstance(0, 0, 0, 0);
                    DateTime dt = DateTime.newInstanceGMT(objCase.POA_Expiry_Date_Seller__c, myTime);
                    system.debug('seller poa date formatted********'+dt.format('dd/MM/yyyy'));
                    sellerPoaDate = String.valueOf(dt.format('dd/MM/yyyy'));
                }
                blnSectionOne = true;
                blnSectionTwo = true;
                if(objCase.Buyer__c != null){
                    blnSectionThree = true;
                    blnSectionFour = true;
                }
                blnSectionAddDocs = true;
                blnSectionDocs = true;
                lstAdditionalDocs = new List<docInfo>();
            }

        }else{
            system.debug('New case will be ccreated************');
            objCase = new Case();
            objCase.Is_Batch_Processing_Pending__c = true;
            objCase.Origin = 'Portal';
            strCaseID = objCase.id;
            objBuyer = new Buyer__c();
            fetchCustomerUnits();
            system.debug('>>>>>>strCaseID'+strCaseID);
            system.debug('mapId_BookingUnit*********'+mapId_BookingUnit);
        }
        //fetchUnitDetails();
        fetchSeller();
        //buyerPOA = new Attachment();
        //sellerPOA = new Attachment();
        if(String.isNotBlank(strCaseId)){
            fetchInsertedDocs();
        }
        //objCase.Status = 'New';
        system.debug('objCase>>>>>'+objCase);
        strCaseID = objCase.id;
        system.debug('>>>>>>strCaseID'+strCaseID);
        if((objCase != null)
        || String.isNotBlank(strCaseId) && objCase != null){
            List <Recordtype> lstCase_RT = fetchRecordtype('Case', 'Rental_Pool_Assignment');
            objCase.RecordtypeId = lstCase_RT[0].Id;
            objCase.Seller__c = strAccoundId;

            system.debug('objUnit***************'+objUnit);
            lstOptions = new list<selectoption>();
            lstOptions.add(new SelectOption('true','Yes'));
            lstOptions.add(new SelectOption('false','No'));

            lstTotalJointBuyers = new list<SelectOption>();
            if(String.isNotBlank(strCaseId)
            && String.isNotBlank(objCase.Buyer_Info__c)){
                list<string> lstSt = objCase.Buyer_Info__c.split(',');
                system.debug('*****'+lstSt[0]);
                strJBCount = lstSt[0];
                // below line uncommented by Vivian on 07/01/2018
                //createJointBuyers();
                fetchNewJB();
            }else{
                strJBCount = '0';
            }
            for(Integer i = 0; i<=9; i++){
                lstTotalJointBuyers.add(new SelectOption(String.valueOf(i),String.valueOf(i)));
            }
            lstCategories = new list<SelectOption>();
            lstCategories.add(new selectOption('Unit Details', 'Unit Details'));
            lstCategories.add(new selectOption('Flags', 'Flags'));
            lstCategories.add(new selectOption('Unit Status', 'Unit Status'));
            lstCategories.add(new selectOption('Joint Buyers', 'Joint Buyers'));
            lstCategories.add(new selectOption('Open SRs', 'Open SRs'));
            lstCategories.add(new selectOption('Closed SRs', 'Closed SRs'));
            strSelectedCategory = 'Unit Details';
            strSoAURL = '';
            strFMSoAURL = '';

            strHelpPageId = FetchDocumentURL.fetchDocURL('Assignment_Help');
            strUndertakingLetterId = FetchDocumentURL.fetchDocURL('Signature_Undertaking_Letter');
            strRPUndertakingLetterId = FetchDocumentURL.fetchDocURL('Rental_Pool_Undertaking_Letter');
        }
    }

    public override void viewPaymentDetails(){
        try{
            errorMessage = '';
            system.debug('View reqn called***************');

            String strFeeResponse = assignmentEndpoints.fetchAssignmentFees(objUnit, objCase, 'Assignment');
            system.debug('strFeeResponse==='+strFeeResponse);
            if(String.isNotBlank(strFeeResponse)
            && !strFeeResponse.contains('Exception occured')){
                map<String,String> mapKey_Value = new map<String,String>();
                map<String,Object> mapDeserializeFees = new map<String,Object>();
                mapKey_Value = parseRuleEngine(strFeeResponse, 'Fees');
                mapDeserializeFees = (map<String,Object>)JSON.deserializeUntyped(strFeeResponse);
                system.debug('mapKey_Value==='+mapKey_Value);
                paymentInfo objFee;
                //Decimal assignmentFee = 0;
                if(mapDeserializeFees.containsKey('allowed')
                && mapDeserializeFees.get('allowed') == 'Yes'){
                    if(!mapKey_Value.isEmpty()){
                        objFee = new paymentInfo();
                        objFee.strType = 'Admin Fee for Assignment';
                        decTotal = 0.00;
                        if(mapKey_Value.containsKey('adminFeeFlat')
                        && mapKey_Value.get('adminFeeFlat') != null
                        && mapKey_Value.get('adminFeeFlat') != 'null'){
                            decimal intVal = decimal.valueOf(mapKey_Value.get('adminFeeFlat').remove('AED').trim());
                            objFee.decAmount = intVal.setScale(2);
                            objCase.Assignment_Fee__c = intVal;
                            //assignmentFee = intVal;
                        }else if(mapKey_Value.containsKey('adminFeePsf')
                        && mapKey_Value.get('adminFeePsf') != null
                        && mapKey_Value.get('adminFeePsf') != 'null'){
                            decimal intVal = decimal.valueOf(mapKey_Value.get('adminFeePsf').remove('AED').trim());
                            objFee.decAmount = (intVal * objUnit.Area__c).setScale(2);
                            objCase.Assignment_Fee__c = objFee.decAmount;
                            //assignmentFee = objFee.decAmount;
                        }else if(mapKey_Value.containsKey('adminFeePercentage')
                        && mapKey_Value.get('adminFeePercentage') != null
                        && mapKey_Value.get('adminFeePercentage') != 'null'){
                            decimal intVal = decimal.valueOf(mapKey_Value.get('adminFeePercentage').remove('AED').trim());
                            objFee.decAmount = (intVal * decimal.valueOf(objWrap.strPrice)).setScale(2);
                            objCase.Assignment_Fee__c = objFee.decAmount;
                            //assignmentFee = objFee.decAmount;
                        }
                        if(objFee.decAmount != null) {
                            decTotal = (decTotal + objFee.decAmount).setScale(2);
                        }
                        else {
                            //decTotal = decTotal.setScale(2);
                            objFee.decAmount = 0.00;
                        }
                        system.debug('objP*****'+objFee);
                        system.debug('decTotal*****'+decTotal);
                    }
                    //fetchDocumentsList() && fetchApprovers() are now called separately
                    if(fetchPDCDetails()){
                        String strDueResponse = assignmentEndpoints.fetchAssignmentDues(objUnit);
                        system.debug('strDueResponse==='+strDueResponse);
                        if(String.isNotBlank(strDueResponse)){
                            map<String,Object> mapDeserializeDue = (map<String,Object>)JSON.deserializeUntyped(strDueResponse);
                            if(mapDeserializeDue.get('status') == 'S'){
                                strDueResponse = strDueResponse.remove('{');
                                strDueResponse = strDueResponse.remove('}');
                                strDueResponse = strDueResponse.remove('"');
                                lstPayments = new List<paymentInfo>();
                                system.debug('after all replacements******'+strDueResponse);
                                for(String st : strDueResponse.split(',')){
                                    String strKey = st.substringBefore(':').trim();
                                    if(!strKey.equalsIgnoreCase('Status')) {
                                        system.debug('st*****'+st);
                                        paymentInfo objP = new paymentInfo();
                                        objP.strType = strKey;
                                        if(Decimal.valueOf(st.subStringAfter(':').trim()) > 0) {
                                           objP.decAmount = Decimal.valueOf(st.subStringAfter(':').trim());
                                        }
                                        else {
                                           objP.decAmount = 0.00;
                                        }
                                        decTotal = decTotal + objP.decAmount;
                                        system.debug('objP*************88'+objP);
                                        lstPayments.add(objP);
                                    }
                                }
                                lstPayments.add(objFee);
                                system.debug('objCase.Assignment_Fee__c***********'+objCase.Assignment_Fee__c);
                                if(objCase.Id == null){
                                    objCase.CurrencyIsoCode = objUnit.CurrencyIsoCode;
                                    objCase.Booking_Unit__c = strSelectedUnit;
                                    system.debug('Buyer_Info__c**********'+objCase.Buyer_Info__c);
                                    upsertCase();
                                    list<Case> lstC = [Select Id, CaseNumber from Case where Id =: objCase.Id];
                                    if(lstC != null && !lstC.isEmpty()){
                                        caseNumber = lstC[0].CaseNumber;
                                    }
                                }else{
                                    caseNumber = objCase.CaseNumber;
                                }
                                blnSectionTwo = true;
                            }else if(mapDeserializeDue.get('status') == 'E'
                            && mapDeserializeDue.containsKey('message')){
                                errorMessage = 'Error : '+mapDeserializeDue.get('message');
                                Error_Log__c objErr = createErrorLogRecord(strAccoundId, strSelectedUnit, objCase.Id);
                                objErr.Error_Details__c = errorMessage;
                                //insert objErr;
                                insertErrorLog(objErr);
                            }
                        }
                        else {
                            errorMessage = 'Error : Unable to get response from IPMS for Payment Dues. Please try again later.';
                            Error_Log__c objErr = createErrorLogRecord(strAccoundId, strSelectedUnit, objCase.Id);
                            objErr.Error_Details__c = errorMessage;
                            //insert objErr;
                            insertErrorLog(objErr);
                        }
                    } // PDC details response was returned
                }else if(mapDeserializeFees.containsKey('allowed')
                && mapDeserializeFees.get('message') != null
                && mapDeserializeFees.get('message') != 'null'
                && mapDeserializeFees.get('allowed') == 'No') {
                    errorMessage = 'Error : '+mapDeserializeFees.get('message');
                    Error_Log__c objErr = createErrorLogRecord(strAccoundId, strSelectedUnit, objCase.Id);
                    objErr.Error_Details__c = errorMessage;
                    //insert objErr;
                    insertErrorLog(objErr);
                }
                else {
                    system.debug('Error 01');
                    errorMessage = 'Error : Unable to get response from the Rule Engine for Assignment fees. Please try again later.';
                    Error_Log__c objErr = createErrorLogRecord(strAccoundId, strSelectedUnit, objCase.Id);
                    objErr.Error_Details__c = errorMessage;
                    //insert objErr;
                    insertErrorLog(objErr);
                }
            }else{
                system.debug('Error 02');
                errorMessage = 'Error : Unable to get response from the Rule Engine for Assignment fees. Please try again later.';
                Error_Log__c objErr = createErrorLogRecord(strAccoundId, strSelectedUnit, objCase.Id);
                objErr.Error_Details__c = errorMessage;
                //insert objErr;
                insertErrorLog(objErr);
            }
        }catch(Exception ex){
            errorMessage = 'Error : '+ex.getMessage();
        }
    }

    public override pagereference saveAsDraft(){
        Pagereference pgRef;
        //upsertCase();
        errorMessage = '';

        system.debug('strSelectedUnit*****************'+strSelectedUnit);
        objCase.Booking_Unit__c = strSelectedUnit;
        //updatePoADetails();
        system.debug('blnSectionAddDocs****************'+blnSectionAddDocs);
        system.debug('blnSectionThree****************'+blnSectionThree);
        system.debug('blnSectionerrorMessageThree****************'+errorMessage);
        /*
        if(blnSectionAddDocs){
            insertSupportingDocs();
        }

        if(blnSectionThree && errorMessage == ''){
            upsertBuyer();
        }
        */

        if(blnSectionFour){
            printNOC();
            //upsert objCase;
        }
        if(blnSectionThree){
            upsertBuyer();
        }
        objCase = [Select Id
                                    , CaseNumber
                                    , Origin
                                    , Status
                                    , Assignment_Fee__c
                                    , LOA_Submitted__c
                                    , Buyer_Type__c
                                    , Relationship_with_Seller__c
                                    , Case_Type__c
                                    , Buyer_Info__c
                                    , Seller_POA__c
                                    , Buyer_POA_Country_Code__c
                                    , Buyer_POA_Phone__c
                                    , Seller_POA_Country_Code__c
                                    , Seller_POA_Phone__c
                                    , POA_Name_Seller__c
                                    , Purpose_of_POA_Seller__c
                                    , POA_Issued_By_Seller__c
                                    , Buyer_POA__c
                                    , POA_Name__c
                                    , Purpose_of_POA__c
                                    , POA_Issued_By__c
                                    , POA_Expiry_Date__c
                                    , POA_Expiry_Date_Seller__c
                                    , Seller__c
                                    , Buyer__c
                                    , OwnerId
                                    , Buyer__r.Booking__c
                                    , Buyer__r.Organisation_Name__c
                                    , Buyer__r.CR_Registration_Expiry_Date__c
                                    , Buyer__r.CR_Registration_Place__c
                                    , Buyer__r.Organisation_Name_Arabic__c
                                    , Buyer__r.CR_Number__c
                                    , Buyer__r.CR_Registration_Place_Arabic__c
                                    , Buyer__r.Title__c
                                    , Buyer__r.First_Name__c
                                    , Buyer__r.Last_Name__c
                                    , Buyer__r.Nationality__c
                                    , Buyer__r.Passport_Number__c
                                    , Buyer__r.Place_of_Issue__c
                                    , Buyer__r.Address_Line_1__c
                                    , Buyer__r.Address_Line_2__c
                                    , Buyer__r.Address_Line_3__c
                                    , Buyer__r.Address_Line_4__c
                                    , Buyer__r.Country__c
                                    , Buyer__r.City__c
                                    , Buyer__r.Date_of_Birth__c
                                    , Buyer__r.Title_Arabic__c
                                    , Buyer__r.First_Name_Arabic__c
                                    , Buyer__r.Last_Name_Arabic__c
                                    , Buyer__r.Nationality_Arabic__c
                                    , Buyer__r.Passport_Expiry_Date__c
                                    , Buyer__r.Place_of_Issue_Arabic__c
                                    , Buyer__r.Address_Line_1_Arabic__c
                                    , Buyer__r.Address_Line_2_Arabic__c
                                    , Buyer__r.Address_Line_3_Arabic__c
                                    , Buyer__r.Address_Line_4_Arabic__c
                                    , Buyer__r.Country_Arabic__c
                                    , Buyer__r.City_Arabic__c
                                    , Buyer__r.Phone_Country_Code__c
                                    , Buyer__r.Phone__c
                                    , Buyer__r.Email__c
                                    , Booking_Unit__c
                                    , Booking_Unit__r.CurrencyIsoCode
                                    , Booking_Unit__r.Booking__r.Deal_SR__c
                                    , Booking_Unit__r.Registration_ID__c
                                    , Booking_Unit__r.Name
                                    , Booking_Unit__r.Booking__c
                                    , Booking_Unit__r.Unit_Details__c
                                    , Booking_Unit__r.Unit_Type__c
                                    , Booking_Unit__r.Booking__r.Account__c
                                    , Booking_Unit__r.Inventory__c
                                    , Booking_Unit__r.Inventory__r.Property__r.Name
                                    , Booking_Unit__r.Inventory__r.Unit__c
                                    , Booking_Unit__r.Inventory__r.Property_City__c
                                    , Booking_Unit__r.Inventory__r.Unit_Type__c
                                    , Booking_Unit__r.Inventory__r.Property_Status__c
                                    , Booking_Unit__r.Inventory__r.Building_Code__c
                                    , Booking_Unit__r.Inventory__r.Bedroom_Type__c
                                    , Booking_Unit__r.Permitted_Use__c
                                    , Booking_Unit__r.Rental_Pool__c
                                    , Booking_Unit__r.Requested_Price__c
                                    , Booking_Unit__r.Booking_Undergoing_Termination__c
                                    , Booking_Unit__r.NOC_Issued_Date__c
                                    , Booking_Unit__r.Dispute__c
                                    , Booking_Unit__r.Enforcement__c
                                    , Booking_Unit__r.Litigation__c
                                    , Booking_Unit__r.Counter_Case__c
                                    , Booking_Unit__r.Mortgage__c
                                    , Booking_Unit__r.Re_Assigned__c
                                    , Booking_Unit__r.Penalty_Amount__c
                                    , Booking_Unit__r.Token_Amount_val__c
                                    , Booking_Unit__r.Booking_Type__c
                                    , Booking_Unit__r.DP_Overdue__c
                                    , Booking_Unit__r.Plot_Price__c
                                    , Booking_Unit__r.Booking__r.CreatedDate
                                    , Booking_Unit__r.Agreement_Date__c
                                    , Booking_Unit__r.JOPD_Area__c
                                    , Booking_Unit__r.Area__c
                                    , Booking_Unit__r.DP_OK__c
                                    , Booking_Unit__r.Doc_OK__c
                                    , Booking_Unit__r.OQOOD_Reg_Flag__c
                                    , Booking_Unit__r.Early_Handover__c
                                    , Booking_Unit__r.Handover_Flag__c
                                    , Booking_Unit__r.PCC_Release__c
                                    , Booking_Unit__r.Construction_Status__c
                                    , Booking_Unit__r.Area_Varied__c
                                    , Booking_Unit__r.Title_Deed__c
                                    , Booking_Unit__r.Under_Assignment__c
                                    , Booking_Unit__r.Regular_Bulk_Flag__c
                                    , Booking_Unit__r.Registration_DateTime__c
                                    , Booking_Unit__r.Manager_Name__c
                                    , Booking_Unit__r.HOS_Name__c
                                    , Booking_Unit__r.Property_Consultant__c
                                    , Booking_Unit__r.Inventory__r.Floor_Plan__c
                                    , Booking_Unit__r.Inventory__r.Unit_Plan__c
                                    , Booking_Unit__r.Finance_Flag__c
                                    from Case
                                    where Id =: objCase.id];
        Task objTask = TaskUtility.getTask((SObject)objCase,
                    'Verify Case Details', 'CRE', 'Rental Pool Assignment',
                    system.today().addDays(1));
                    objTask.OwnerId = Label.DefaultCaseOwnerId;
                    //objTask.Status = 'Completed';
                    insert objTask;
        system.debug('errorMessage**********'+errorMessage);
        if(errorMessage == ''){
           // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Request will be serviced only if you submit the request by clicking the SUBMIT button'));
            pgRef = redirectToCase();
            return pgRef;
        }
        //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Request will be serviced only if you submit the request by clicking the SUBMIT button'));
        return pgRef;
    }

    public override pageReference submitSR(){
        objCase.Is_Batch_Processing_Pending__c = true;
        objCase.Status = 'Submitted';
        objCase.LOA_Submitted__c = true;
        if(objCase.Case_Type__c == 'Succession'){
            objCase.Payment_Verified__c = true;
        }
        return saveAsDraft();
    }

    public override pagereference redirectToCase(){
        Pagereference pgRef;
        if(objCase != null && objCase.Id != null){
            //pgRef = new Pagereference('/'+objCase.Id);
            pgRef= new PageReference('/Customer');
        }else{
            //pgRef = new Pagereference('/500');
            pgRef= new PageReference('/Customer');
        }
        return null;
    }

    @RemoteAction
    public static list<Account> fetchIndividualBuyer_Portal(String strSearch, String strCou, String strNat){
        list<Account> lstA = [Select a.Id, a.PersonTitle, a.Title_Arabic__pc, a.FirstName, a.First_Name_Arabic__pc,
                             a.LastName, a.Last_Name_Arabic__pc, a.RecordType.DeveloperName,
                             a.Nationality__pc, a.Nationality_Arabic__pc, a.Passport_Number__pc, a.Passport_Expiry_Date__pc,
                             a.Party_Id__pc,
                             a.Passport_Issue_Place__pc, a.Passport_Issue_Place_Arabic__pc, a.Address_Line_1__pc,
                             a.Address_Line_2__pc,
                             a.Address_Line_3__pc, a.Address_Line_4__pc, a.Address_Line_1_Arabic__pc, a.Address_Line_2_Arabic__pc,
                             a.Address_Line_3_Arabic__pc, a.Address_Line_4_Arabic__pc, a.Country__pc, a.Country_Arabic__pc,
                             a.City__pc, a.City_Arabic__pc, a.Mobile_Country_Code__pc, a.PersonMobilePhone, a.Date_of_Birth__pc,
                             a.PersonEmail, a.Party_ID__c From Account a
                             where (a.FirstName LIKE : strSearch+'%'
                             or a.LastName Like : strSearch+'%'
                             or a.Passport_Number__pc Like : '%'+strSearch+'%')
                             and a.Country__pc =: strCou
                             and a.Nationality__pc =: strNat
                             and a.RecordType.DeveloperName = 'PersonAccount'
                             and a.Party_ID__c != null];
        system.debug('lstA Individual Buyer=='+lstA);
        return lstA;
    }

    @RemoteAction
    public static list<Contact> fetchCorporateBuyer_Portal(String strSearch, String strCou, String strNat){
        list<Contact> lstC = [Select a.Id, a.Title, a.Title_Arabic__c, a.FirstName, a.First_Name_Arabic__c,
                             a.LastName, a.Last_Name_Arabic__c, a.RecordType.DeveloperName,
                             a.Nationality__c, a.Nationality_Arabic__c, a.Passport_Number__c, a.Passport_Expiry_Date__c,
                             a.Passport_Issue_Place__c, a.Passport_Issue_Place_Arabic__c, a.Address_Line_1__c,
                             a.Address_Line_2__c,
                             a.Address_Line_3__c, a.Address_Line_4__c, a.Address_Line_1_Arabic__c, a.Address_Line_2_Arabic__c,
                             a.Address_Line_3_Arabic__c, a.Address_Line_4_Arabic__c, a.Country__c, a.Country_Arabic__c,
                             a.City__c, a.City_Arabic__c, a.Mobile_Country_Code__c, a.MobilePhone, a.Date_of_Birth__c,
                             a.Email__c, a.AccountId, a.Account.Name, a.Account.Name_Arabic__c, a.Account.CR_Registration_Expiry_Date__c,
                             a.Account.CR_Number__c, a.Account.CR_Registration_Place__c, a.Account.CR_Registration_Place_Arabic__c,
                             a.Account.Party_ID__c, a.Account.RecordType.DeveloperName
                             From Contact a
                             where (a.FirstName LIKE : strSearch+'%'
                             or a.LastName Like : strSearch+'%'
                             or a.Passport_Number__c Like : '%'+strSearch+'%'
                             or a.Account.Name LIKE : strSearch+'%'
                             or a.Account.CR_Number__c LIKE : strSearch+'%')
                             and a.Country__c =: strCou
                             and a.Nationality__c =: strNat
                             and a.Account.RecordType.DeveloperName = 'Business_Account'
                             and a.Account.Party_ID__c != null
                             and a.Account.CR_Number__c != null];
        system.debug('lstA Corporate Buyer=='+lstC);
        return lstC;
    }

    public override void upsertCase(){
        //update registration status
        try{

            upsert objCase;
            strCaseID = objCase.id;
            system.debug('>>>>>>strCaseID'+strCaseID);
        }catch(Exception ex){
            errorMessage = 'Error : '+ ex.getMessage();
            system.debug('Upsert Case ex*****'+ex);
        }
    }
}