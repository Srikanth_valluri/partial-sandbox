global class UpdateCommUserEmailUserName implements Schedulable{
    
    global List<String> PartyId;
    
    @InvocableMethod
    global static void UpdateUserNameEmail(List<String> PartyId) {
        system.debug('party id'+PartyId);
        
        UpdateCommUserEmailUserName m = new UpdateCommUserEmailUserName();
        m.PartyId = PartyId;
        DateTime dt = system.now().addMinutes(1);
        String day = string.valueOf(dt.day());
        String month = string.valueOf(dt.month());
        String hour = string.valueOf(dt.hour());
        String minute = string.valueOf(dt.minute());
        String second = string.valueOf(dt.second());
        String year = string.valueOf(dt.year());
        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
    
        String jobID = system.schedule('User Updation Job '+PartyId[0] + system.now(), strSchedule, m);
    }

    global void execute(SchedulableContext ctx) {
        UpdUser(PartyId);
    }
    
  //@future
  global static void UpdUser(List<String> PartyId)
  {
        Set<String> setUserid = new set<String>();
        map<string, Account> AcntMap= new map<string, Account>();
        system.debug('party id'+PartyId);
        for(Account acnt: [select id, party_id__c,Email__pc from account where party_id__c IN: PartyId])
        {
            AcntMap.put(acnt.Party_Id__c, acnt);
        }
                      system.debug('map'+AcntMap);
      List<User> lstUser = new List<User>();
      List<User> UserRec = [Select Id, UserName,Party_Id__c, Email from User where Party_Id__c IN: AcntMap.keySet() limit 1];
              system.debug('user rec'+UserRec );
      /*for( User Usr : UserRec )
      {
          //Usr.UserName=AcntMap.get(Usr.Party_Id__c).Email__pc;
          //Usr.Email=AcntMap.get(Usr.Party_Id__c).Email__pc;lstUser.add(Usr);
          setUserid.add(String.valueOf(Usr.id).substring(0, 15));
      }*/
      //system.debug('Record_Id'+setUserid); system.debug('final_list'+lstUser);

      //List<User> FinalUserLst = [Select Id, UserName,Party_Id__c, Email from User where Id=:setUserid limit 1];
       //     system.debug('final list'+FinalUserLst);
      for(User USER : UserRec)
      {
          USER.UserName=AcntMap.get(USER.Party_Id__c).Email__pc;
          USER.Email=AcntMap.get(USER.Party_Id__c).Email__pc;
          lstUser.add(USER);
      }
                  system.debug('let us upd'+lstUser);
      update lstUser;
  }
}