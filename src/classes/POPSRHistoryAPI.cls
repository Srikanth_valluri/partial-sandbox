/**********************************************************************************************************************
Description: This API is used for getting list of POP SRs (upto five) for an account
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By      | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   15-12-2020    | Anand Venkitakrishnan | Created the initial draft
***********************************************************************************************************************/

@RestResource(urlMapping='/popSrHistory/*')
global class POPSRHistoryAPI {
    public static final String SUCCESS = 'Successful';
    public static String responseMessage;
    public static final String PROPERTY_INSTALLMENT = 'installment';
    public static final String SERVICE_CHARGE = 'service_charge';
    public static final String commonErrorMsg = 'This may be because of technical error that we\'re working to get fixed. Please try again later.';
    
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong',
        7 => 'Service charges overdue!'
    };
    
    @HttpGet
    global static FinalReturnWrapper getPopSRHistory() {
    
        FinalReturnWrapper retunResponse = new FinalReturnWrapper();
        cls_meta_data objMeta = new cls_meta_data();
    
        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);
    
        if(!r.params.containsKey('accountId')) {
            objMeta.message = commonErrorMsg;
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = 'Missing parameter : accountId';
            
            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        else if(r.params.containsKey('accountId') && String.isBlank(r.params.get('accountId'))) {
            objMeta.message = commonErrorMsg;
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = 'Missing parameter value: accountId';
      
            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        else if(!r.params.containsKey('popType')) {
            objMeta.message = commonErrorMsg;
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = 'Missing parameter : popType';
            
            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        else if(r.params.containsKey('popType') && String.isBlank(r.params.get('popType'))) {
            objMeta.message = commonErrorMsg;
            objMeta.status_code = 3;
            objMeta.title = mapStatusCode.get(3);
            objMeta.developer_message = 'Missing parameter value: popType';
      
            retunResponse.meta_data = objMeta;
            return retunResponse;
        }
        
        String accountId = r.params.get('accountId');
        String popType = r.params.get('popType');
        
        List<cls_data> popCaseList = new List<cls_data>();
        String message=SUCCESS,errorMsg='';
        Integer statusCode = 1;
        
        try {
            popCaseList = fetchCases(accountId,popType);
        }
        catch(Exception e) {
            message = commonErrorMsg;
            statusCode = 6;
            errorMsg = 'Error while fetching SRs';
            System.debug('Exception while fetching SRs:'+e.getMessage());
        }
        
        System.debug('popCaseList:'+popCaseList);
        
        objMeta.message = message;
        objMeta.status_code = statusCode;
        objMeta.title = mapStatusCode.get(objMeta.status_code);
        objMeta.developer_message = errorMsg;
        
        retunResponse.data = popCaseList;
        retunResponse.meta_data = objMeta;
        
        System.debug('retunResponse:'+retunResponse);
    
        return retunResponse;
    }
    
    public static List<cls_data> fetchCases(String accountId,String popType) {
        List<cls_data> popCaseList = new List<cls_data>();
        
        if(popType.equals(PROPERTY_INSTALLMENT)) {
            popCaseList = getCaseList(accountId);
        }
        else if(popType.equals(SERVICE_CHARGE)) {
            popCaseList = getFMCaseList(accountId);
        }
        
        return popCaseList;        
    }
    
    public static List<cls_data> getCaseList(String accountId) {
        List<cls_data> caseList = new List<cls_data>();
        
        for(Case caseDetails: [SELECT Id,CaseNumber,Payment_Date__c,Payment_Mode__c,Payment_Allocation_Details__c,Payment_Currency__c,Total_Amount__c, 
                               (SELECT Id,Booking_Unit__r.Unit_Name__c,Booking_Unit__r.Inventory__r.Building_Location__r.Building_Name__c FROM SR_Booking_Units__r),
                               (SELECT Id,Attachment_url__c,Name,Type__c FROM SR_Attachments__r WHERE Type__c = 'POP') 
                               FROM Case 
                               WHERE Id IN (SELECT Case__c FROM SR_Booking_Unit__c) 
                               AND AccountId = :accountId 
                               AND RecordType.DeveloperName = 'Proof_of_Payment_SR' 
                               AND Payment_Date__c != NULL AND Payment_Mode__c != NULL AND Payment_Currency__c != NULL AND Total_Amount__c != NULL 
                               ORDER BY CreatedDate DESC
                               LIMIT 5]) {
          cls_data caseData = new cls_data();
             caseData.id = caseDetails.Id;
          caseData.case_number = caseDetails.CaseNumber;
            caseData.payment_date = caseDetails.Payment_Date__c != null ? Datetime.newInstance(caseDetails.Payment_Date__c.year(),caseDetails.Payment_Date__c.month(),caseDetails.Payment_Date__c.day()).format('yyyy-MM-dd') : '';
          caseData.payment_mode = caseDetails.Payment_Mode__c;
          caseData.payment_remarks = caseDetails.Payment_Allocation_Details__c;
            
            Map<String,String> currencyMap = getCurrencyMap('Case','CurrencyIsoCode');
          
          if(currencyMap != NULL && !currencyMap.isEmpty() && currencyMap.containsKey(caseDetails.Payment_Currency__c)) {
              caseData.payment_currency = currencyMap.get(caseDetails.Payment_Currency__c);
          }
          else {
              caseData.payment_currency = caseDetails.Payment_Currency__c;
          }
            
          caseData.total_amount = caseDetails.Total_Amount__c;
          
            System.debug('caseDetails.SR_Booking_Units__r:'+caseDetails.SR_Booking_Units__r);
            
            List<cls_unit_details> unitsList = new List<cls_unit_details>();
            
          if(caseDetails.SR_Booking_Units__r.size() > 0) {
              for(SR_Booking_Unit__c obj : caseDetails.SR_Booking_Units__r) {
                  cls_unit_details objUnit = new cls_unit_details();
                  objUnit.unit_name = obj.Booking_Unit__r.Unit_Name__c;
                  objUnit.building_name = obj.Booking_Unit__r.Inventory__r.Building_Location__r.Building_Name__c;
                  
                  unitsList.add(objUnit);
              }
          }
          
          caseData.unit_details = unitsList;
            
            System.debug('caseDetails.SR_Attachments__r:'+caseDetails.SR_Attachments__r);
            
            List<cls_attachment> attachmentsList = new List<cls_attachment>();
            
          if(caseDetails.SR_Attachments__r.size() > 0) {
              for(SR_Attachments__c obj : caseDetails.SR_Attachments__r) {
                    String fileName = obj.Name;
                    
                  cls_attachment objAttach = new cls_attachment();
                  objAttach.id = obj.id;
                  objAttach.name = fileName;
                    
                    String idValue = obj.Attachment_URL__c.substringAfter('id=');
                    
                    if(idValue != NULL && String.isNotBlank(idValue) && !Test.isRunningTest()) {
                        Office365RestService.ResponseWrapper objWrap = Office365RestService.downloadFromOffice365ById(idValue ,'','','','','');
                      
                        if(objWrap != NULL) {
                            objAttach.attachment_url = objWrap.downloadUrl;
                            attachmentsList.add(objAttach);
                        }
                    }
              }
          }
          
          caseData.attachments = attachmentsList;
          
          caseList.add(caseData);
        }
        
        return caseList;
    }
    
    public static List<cls_data> getFMCaseList(String accountId) {
        List<cls_data> fmCaseList = new List<cls_data>();
        
        for(FM_Case__c caseDetails: [SELECT Id,Name,Payment_Date__c,Payment_Mode__c,Payment_Allocation_Details__c,Currency__c,Total_Amount__c, 
                                     (SELECT Id,Booking_Unit__r.Unit_Name__c,Booking_Unit__r.Inventory__r.Building_Location__r.Building_Name__c FROM Booking_Units__r),
                                     (SELECT Id,Attachment_url__c,Name,Type__c FROM Documents__r WHERE Type__c = 'POP') 
                                     FROM FM_Case__c 
                                     WHERE Id IN (SELECT FM_Case__c FROM SR_Booking_Unit__c)
                                     AND Account__c = :accountId 
                                     AND RecordType.DeveloperName = 'Proof_Of_Payment' 
                                     AND Payment_Date__c != NULL AND Payment_Mode__c != NULL AND Currency__c != NULL AND Total_Amount__c != NULL 
                                     ORDER BY CreatedDate DESC
                                      LIMIT 5]) {
          cls_data caseData = new cls_data();
             caseData.id = caseDetails.Id;
          caseData.case_number = caseDetails.Name;
            caseData.payment_date = caseDetails.Payment_Date__c != null ? Datetime.newInstance(caseDetails.Payment_Date__c.year(),caseDetails.Payment_Date__c.month(),caseDetails.Payment_Date__c.day()).format('yyyy-MM-dd') : '';
          caseData.payment_mode = caseDetails.Payment_Mode__c;
          caseData.payment_remarks = caseDetails.Payment_Allocation_Details__c;
            
            Map<String,String> currencyMap = getCurrencyMap('Case','CurrencyIsoCode');
          
          if(currencyMap != NULL && !currencyMap.isEmpty() && currencyMap.containsKey(caseDetails.Currency__c)) {
              caseData.payment_currency = currencyMap.get(caseDetails.Currency__c);
          }
          else {
              caseData.payment_currency = caseDetails.Currency__c;
          }
            
          caseData.total_amount = caseDetails.Total_Amount__c;
          
            System.debug('caseDetails.Booking_Units__r:'+caseDetails.Booking_Units__r);
            
            List<cls_unit_details> unitsList = new List<cls_unit_details>();
            
          if(caseDetails.Booking_Units__r.size() > 0) {
              for(SR_Booking_Unit__c obj : caseDetails.Booking_Units__r) {
                  cls_unit_details objUnit = new cls_unit_details();
                  objUnit.unit_name = obj.Booking_Unit__r.Unit_Name__c;
                  objUnit.building_name = obj.Booking_Unit__r.Inventory__r.Building_Location__r.Building_Name__c;
                  
                  unitsList.add(objUnit);
              }
          }
          
          caseData.unit_details = unitsList;
            
            System.debug('caseDetails.Documents__r:'+caseDetails.Documents__r);
            
            List<cls_attachment> attachmentsList = new List<cls_attachment>();
            
          if(caseDetails.Documents__r.size() > 0) {
              for(SR_Attachments__c obj : caseDetails.Documents__r) {
                    String fileName = obj.Name;
                    
                  cls_attachment objAttach = new cls_attachment();
                  objAttach.id = obj.id;
                  objAttach.name = obj.Name;
                    
                  String idValue = obj.Attachment_URL__c.substringAfter('id=');
                    
                    if(idValue != NULL && String.isNotBlank(idValue) && !Test.isRunningTest()) {
                        Office365RestService.ResponseWrapper objWrap = Office365RestService.downloadFromOffice365ById(idValue ,'','','','','');
                      
                        if(objWrap != NULL) {
                            objAttach.attachment_url = objWrap.downloadUrl;
                            attachmentsList.add(objAttach);
                        }
                    }
              }
          }
          
          caseData.attachments = attachmentsList;
          
          fmCaseList.add(caseData);
        }
        
        return fmCaseList;
    }
    
    public static Map<String,String> getCurrencyMap(String sObjectName, string fieldName) {  
        Map<String,String> currencyMap = new Map<String,String>();
        
        Schema.sObjectType objType = Schema.getGlobalDescribe().get(sObjectName);
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        List<Schema.PicklistEntry> values = fieldMap.get(fieldName).getDescribe().getPickListValues();  
        
        for(Schema.PicklistEntry a: values) {
            currencyMap.put(a.getLabel(),a.getValue());
        }
        System.debug('currencyMap:'+currencyMap);
        
        return currencyMap;
    }
    
    global class FinalReturnWrapper {
        public cls_data[] data;
        public cls_meta_data meta_data;
    }
  
    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }
    
    public class cls_data {
        public String id;
        public String case_number;
        public String payment_date;
        public String payment_mode;
        public String payment_remarks;
        public String payment_currency;
        public Decimal total_amount;
        
        public cls_unit_details[] unit_details;
        
        public cls_attachment[] attachments;
    }
    
    public class cls_unit_details {
        public String unit_name;
        public String building_name;
    }
    
    public class cls_attachment {
        public String id;
        public String name;
        public String attachment_url;
    }
}