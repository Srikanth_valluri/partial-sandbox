@isTest
public class NoShowCntrlTest{
    static testMethod void Test1() {
        
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User managerUser = TestDataFactory_CRM.createUser('Manager', adminProfile, userRoleObj.Id);
        insert managerUser;
        System.runAs( managerUser ) {
            Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
            objAcc.Email__pc = 'new@no.com';
            objAcc.Mobile_Phone_Encrypt__pc  = '00971559270964';
            insert objAcc;
                    
            NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
            //objSR.Account__c = objAcc.id ;
            objSR.Deal_ID__c = '12345';
            objSR.Reinstatement_Status__c = 'Not Applicable';
            objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
            objSR.Booking_Wizard_Level__c = 'Level 4';
            objSR.Approval_Level_Required__c = 'Level 1';
            insert objSR;
                    
            Booking__c objBooking = new Booking__c();
            objBooking.Account__c = objAcc.id;
            objBooking.Deal_SR__c = objSR.id;
            insert objBooking;
            
            Booking_Unit__c objBookingUnit = new Booking_Unit__c();
            objBookingUnit.Registration_ID__c  = '12345';
            objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
            objBookingUnit.Booking__c  = objBooking.id;
            objBookingUnit.Mortgage__c = true;
            //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
            objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
            insert objBookingUnit;
            
            
            TriggerOnOffCustomSetting__c objOff = new TriggerOnOffCustomSetting__c ();
            objOff.Name = 'CallingListTrigger';
            objOff.OnOffCheck__c = true;
            insert objOff;
            
            Id walkinRecType = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Walk In Calling List').getRecordTypeId();
            Calling_list__c objCL = new Calling_list__c();
            objCL.RecordTypeId = walkinRecType;
            objCL.Account__c = objAcc.id;
            objCL.Customer_Name__c = objAcc.Name;
            objCL.Appointment_Slot__c = '10:00';
            objCL.Appointment_Date__c = date.today();
            objCL.Service_Type__c = 'Collection';
            objCL.Assigned_CRE__c = managerUser.Id;
            objCL.Booking_Unit__c = objBookingUnit.id;
            objCL.Account_Email__c = 'test@test.com';
            
            insert objCL;
            
            
            test.startTest();
                PageReference pageRef = Page.NoShow;
                pageRef.getParameters().put('id', String.valueOf(objCL.Id)); 
                Test.setCurrentPage(pageRef);
                Test.setMock(HttpCalloutMock.class, new MockHttpResponseSMSService(1));
                ApexPages.StandardController sc = new ApexPages.StandardController(objCL);       
                NoShowCntrl obj = new NoShowCntrl(sc);
                //pageRef = obj.init();
                obj.callingListId  = objCL.id;
                obj.createAndUpdateCallBackReqCL();
            test.stopTest();
        }
    }
    static testMethod void Test2() {
        
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User managerUser = TestDataFactory_CRM.createUser('Manager', adminProfile, userRoleObj.Id);
        insert managerUser;
        System.runAs( managerUser ) {
            Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
            objAcc.Email__pc = 'new@no.com';
            insert objAcc;
                    
            NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
            //objSR.Account__c = objAcc.id ;
            objSR.Deal_ID__c = '12345';
            objSR.Reinstatement_Status__c = 'Not Applicable';
            objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
            objSR.Booking_Wizard_Level__c = 'Level 4';
            objSR.Approval_Level_Required__c = 'Level 1';
            insert objSR;
                    
            Booking__c objBooking = new Booking__c();
            objBooking.Account__c = objAcc.id;
            objBooking.Deal_SR__c = objSR.id;
            insert objBooking;
            
            Booking_Unit__c objBookingUnit = new Booking_Unit__c();
            objBookingUnit.Registration_ID__c  = '12345';
            objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
            objBookingUnit.Booking__c  = objBooking.id;
            objBookingUnit.Mortgage__c = true;
            //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
            objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
            insert objBookingUnit;
            
            
            TriggerOnOffCustomSetting__c objOff = new TriggerOnOffCustomSetting__c ();
            objOff.Name = 'CallingListTrigger';
            objOff.OnOffCheck__c = true;
            insert objOff;
            
            Id walkinRecType = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Walk In Calling List').getRecordTypeId();
            Calling_list__c objCL = new Calling_list__c();
            objCL.RecordTypeId = walkinRecType;
            objCL.Account__c = objAcc.id;
            objCL.Customer_Name__c = objAcc.Name;
            objCL.Appointment_Slot__c = '10:00';
            objCL.Appointment_Date__c = date.today();
            objCL.Service_Type__c = 'Collection';
            objCL.Assigned_CRE__c = managerUser.Id;
            objCL.Booking_Unit__c = objBookingUnit.id;
            objCL.Account_Email__c = 'test@test.com';
            
            insert objCL;
            
            
            test.startTest();
                PageReference pageRef = Page.NoShow;
                pageRef.getParameters().put('id', String.valueOf(objCL.Id)); 
                Test.setCurrentPage(pageRef);
                Test.setMock(HttpCalloutMock.class, new MockHttpResponseSMSService());
                ApexPages.StandardController sc = new ApexPages.StandardController(objCL);       
                NoShowCntrl obj = new NoShowCntrl(sc);
                //pageRef = obj.init();
                obj.callingListId  = objCL.id;
                obj.createAndUpdateCallBackReqCL();
            test.stopTest();
        }
    }
    static testMethod void Test3() {
        
        UserRole userRoleObj = new UserRole(Name = 'CEO', DeveloperName = 'MyCustomRole');
        insert userRoleObj;
        Id adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User managerUser = TestDataFactory_CRM.createUser('Manager', adminProfile, userRoleObj.Id);
        insert managerUser;
        System.runAs( managerUser ) {
            Id personAccRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            Account objAcc = new Account(RecordTypeId = personAccRTId, FirstName='Test FirstName', LastName='Test LastName', Type='Person',party_ID__C='123456');
            objAcc.Email__pc = 'new@no.com';
            objAcc.Mobile_Phone_Encrypt__pc  = '00971559270964';
            insert objAcc;
                    
            NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
            //objSR.Account__c = objAcc.id ;
            objSR.Deal_ID__c = '12345';
            objSR.Reinstatement_Status__c = 'Not Applicable';
            objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
            objSR.Booking_Wizard_Level__c = 'Level 4';
            objSR.Approval_Level_Required__c = 'Level 1';
            insert objSR;
                    
            Booking__c objBooking = new Booking__c();
            objBooking.Account__c = objAcc.id;
            objBooking.Deal_SR__c = objSR.id;
            insert objBooking;
            
            Booking_Unit__c objBookingUnit = new Booking_Unit__c();
            objBookingUnit.Registration_ID__c  = '12345';
            objBookingUnit.Unit_Name__c  = 'JNU/ABC/234';
            objBookingUnit.Booking__c  = objBooking.id;
            objBookingUnit.Mortgage__c = true;
            //objBookingUnit.Registration_Status__c = 'Agreement Rejected By Sales Admin';
            objBookingUnit.Registration_Status__c = 'Agreement executed by DAMAC'; 
            insert objBookingUnit;
            
            
            TriggerOnOffCustomSetting__c objOff = new TriggerOnOffCustomSetting__c ();
            objOff.Name = 'CallingListTrigger';
            objOff.OnOffCheck__c = true;
            insert objOff;
            
            Id walkinRecType = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Walk In Calling List').getRecordTypeId();
            Calling_list__c objCL = new Calling_list__c();
            objCL.RecordTypeId = walkinRecType;
            objCL.Account__c = objAcc.id;
            objCL.Customer_Name__c = objAcc.Name;
            objCL.Appointment_Slot__c = '10:00';
            objCL.Appointment_Date__c = date.today();
            objCL.Service_Type__c = 'Collection';
            objCL.Booking_Unit__c = objBookingUnit.id;
            objCL.Account_Email__c = 'test@test.com';
            
            insert objCL;
            
            
            test.startTest();
                PageReference pageRef = Page.NoShow;
                pageRef.getParameters().put('id', String.valueOf(objCL.Id)); 
                Test.setCurrentPage(pageRef);
                Test.setMock(HttpCalloutMock.class, new MockHttpResponseSMSService(2));
                ApexPages.StandardController sc = new ApexPages.StandardController(objCL);       
                NoShowCntrl obj = new NoShowCntrl(sc);
                //pageRef = obj.init();
                obj.callingListId  = objCL.id;
                obj.createAndUpdateCallBackReqCL();
            test.stopTest();
        }
    }   
}