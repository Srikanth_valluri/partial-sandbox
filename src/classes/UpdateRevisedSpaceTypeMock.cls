@isTest
global class UpdateRevisedSpaceTypeMock implements HttpCalloutMock{
    
    public static final string SAMPLE_RESPONSE= ' { "P_REQUEST_NUMBER": "U1231", "P_SOURCE_SYSTEM": "SFDC","P_REQUEST_NAME": "UPDATE_REVISED_SPACE_TYPE",' +
                        ' "P_REQUEST_MESSAGE": {"P_REQUEST_MESSAGE_ITEM": [{ "PARAM_ID": "1001795","ATTRIBUTE1": "DELUXE_SERVICED_APARTMENTS" } ]}}';
    
    global HTTPResponse respond(HTTPRequest req) {
       
        System.assertEquals('http://151.253.15.117:8050/webservices/rest/XXDC_PROCESS_SERVICE_WS/process/', req.getEndpoint());
        
        req.setMethod('POST');
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(SAMPLE_RESPONSE);
        res.setStatusCode(200);
        return res;
    }
}