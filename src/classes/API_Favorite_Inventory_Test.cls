@isTest
public class API_Favorite_Inventory_Test {

    @testSetup 
    static void createDataTestSetup() {
        Location__c loc ; 
        Inventory__c inventory ; 
        Address__c addressDetail ; 
        Property__c propertyDetail ; 
        
	    loc = InitialiseTestData.getLocationDetails('1234','Building');
	    insert loc ;
	    loc = InitialiseTestData.getLocationDetails('2345','Floor');
	    insert loc ;
	    loc = InitialiseTestData.getLocationDetails('3456','Unit');
	    insert loc ;
	    addressDetail = InitialiseTestData.getAddressDetails(9086);
	    //insert addressDetail ; 
	    propertyDetail = InitialiseTestData.getPropertyDetails(7650);
	    insert propertyDetail ; 
	    inventory = InitialiseTestData.getInventoryDetails('3456','1234','2345',9086,7650); 
	    insert inventory ; 
	    inventory.Status__c = 'Released';
	    update inventory ; 

    }
    
    @isTest 
    static void doGet_Test() {
        API_Favorite_Inventory.doGet();
    }
    
    @isTest 
    static void doPost_Test() {
        
        List<API_Favorite_Inventory.FavoriteInventoryRequestDTO> requestList = new List<API_Favorite_Inventory.FavoriteInventoryRequestDTO>();
        
        for(Inventory__c inv : [SELECT Id FROM Inventory__c]) {
            API_Favorite_Inventory.FavoriteInventoryRequestDTO requestDTO = new API_Favorite_Inventory.FavoriteInventoryRequestDTO();
            requestDTO.inventoryId = inv.Id;
            requestDTO.isFavorite = true;
            requestList.add(requestDTO);
        }
        
        RestContext.response = new RestResponse();
        
        API_Favorite_Inventory.doPost(requestList);
    }
}