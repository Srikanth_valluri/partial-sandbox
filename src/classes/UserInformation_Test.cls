//Description: Test class for UserInformation

@isTest
public class UserInformation_Test{
public static testmethod void getMethod(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/user/*'; 
        req.httpMethod = 'Get';      
        RestContext.request = req;
        RestContext.response= res;
        
        Account ac = new Account(name ='Grazitti') ;
        insert ac; 
       
        Contact con = new Contact(LastName ='testCon',AccountId = ac.Id);
        insert con; 
       profile p= [SELECT Id FROM Profile WHERE Name ='Property Consultant'];
        User promoter = new User(
         ProfileId = p.Id,
         LastName = 'last',
         Email = 'puser000@amamama.com',
         Username = 'puser000@amamama.com' + System.currentTimeMillis(),
         CompanyName = 'TEST',
         Title = 'title',
         Alias = 'alias',
         contactId = con.id,
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US'
         
        );
        UserInformation.getNewFundraiser();
        }
  }