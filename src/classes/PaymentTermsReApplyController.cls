public with sharing class PaymentTermsReApplyController 
{

  public Id caseId;

  //Constructor
  public PaymentTermsReApplyController(ApexPages.StandardController controller) 
  {
    caseId = ApexPages.currentPage().getParameters().get('id');
  }

  // method used to re apply payment terms in IPMS if clicked on Re-Apply button
  public PageReference applyPaymentTerms()
  { 
    system.debug('applyPaymentTerms called');
    if(String.isNotBlank(caseId))
    {
      system.debug('Case ID '+caseId);
      Case objCase = [ SELECT
                       Id,Status,Approval_Status__c
                      ,Offer_Acceptance_Letter_Generated__c
                      ,O_A_Signed_Copy_Uploaded__c
                      FROM Case WHERE Id =:caseId
                     ];
      Set<String> caseStatusSet = new Set<String>();
      caseStatusSet.add('Submitted');
      caseStatusSet.add('Approved');
      caseStatusSet.add('Closed');
      caseStatusSet.add('Rejected');

      if( objCase.Status != null  && !caseStatusSet.contains(objCase.Status))
      {
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Error :You cannot re-apply Payment Terms now as SR is not submitted.'));
          return null;
      }

      if( String.isBlank(objCase.Approval_Status__c) || (String.isNotBlank(objCase.Approval_Status__c) && objCase.Approval_Status__c != 'Approved'))
      {
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Error :You cannot re-apply Payment Terms now as approval is pending.'));
         return null; 
      }

      if(!objCase.Offer_Acceptance_Letter_Generated__c)
      {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Error :You cannot re-apply payment terms as Offer & Acceptance Letter is not generated.'));
        return null; 
      }

      if(objCase.Offer_Acceptance_Letter_Generated__c && !objCase.O_A_Signed_Copy_Uploaded__c)
      {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Error :You cannot re-apply payment terms as Offer & Acceptance Letter is generated but signed letter is not uploaded.'));
        return null; 
      }

      //make callout for applying new Payment terms
      AOPTServiceRequestHandler.makeCallout(caseId);
      PageReference  pg = new PageReference ('/'+caseId);
      pg.setRedirect(true);
      return pg;
    }
    else
    {
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Error :Case Record is not present.'));
      return null; 
    }
    
  }
}