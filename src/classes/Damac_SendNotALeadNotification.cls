public without sharing class Damac_SendNotALeadNotification
{


    @InvocableMethod
    public static void sendNotificationEmail (List <ID> taskId) {
        sendEmail(taskId);
    }
    
    @Future
    public static void sendEmail(List <ID> taskId) {
        List <Messaging.SingleEmailMessage> mails = new List <Messaging.SingleEmailMessage>();
        String additionalEmails = Label.Disqualified_EMails;
        EmailTemplate template = new EMailTemplate ();
        try {
            template = [SELECT Body, Subject FROM EmailTemplate WHERE DeveloperName = 'Show_Disqualified'];
        } catch (Exception e) {
            
        }
                
        Set <ID> inqIds = new Set <ID> ();
        List <Task> tasks = new List <Task> ();
        
        tasks = [SELECT whatId, Description, Activity_Type_3__c, Activity_Outcome__c FROM Task WHERE ID IN : taskId AND IsInquiry__c = TRUE];
        
        for (Task t : tasks) {
            inqIds.add (t.whatId);
        }
        
        Map <ID, List <String>> taskEmailAddresses = new Map <ID, List <String>> ();
        Map <ID, Inquiry__c> inqDetails = new Map <ID, Inquiry__c> ();
        
        for (Inquiry__c inq :[SELECT Name, Owner_Email__c, DOs_Email__c, HOS_Email__c, 
                                Telesales_Executive__c, Telesales_Executive__r.Email, 
                                Telesales_Executive__r.TSA_TL__c, Telesales_Executive__r.TSA_TL__r.Email, 
                                Telesales_Executive__r.ManagerId, Telesales_Executive__r.Manager.Email
                                FROM Inquiry__c
                                WHERE ID IN: inqIds]) 
        {
            inqDetails.put (inq.Id, inq);
            List <String> toAddresses = new List <String> ();
            toAddresses.add (inq.Owner_Email__c);
            
            if (inq.DOs_Email__c != null)
                toAddresses.add(inq.DOs_Email__c);
                
            if (inq.HOS_Email__c != null)
                toAddresses.add(inq.HOS_Email__c);
            
            if (inq.Telesales_Executive__c != null) {
                toAddresses.add(inq.Telesales_Executive__r.Email);
                
                if (inq.Telesales_Executive__r.TSA_TL__c != null) {
                    toAddresses.add(inq.Telesales_Executive__r.TSA_TL__r.Email);
                }
                if (inq.Telesales_Executive__r.ManagerId != null) {
                    toAddresses.add(inq.Telesales_Executive__r.Manager.Email);
                }
            }
            if (additionalEmails != null) {
                if (additionalEmails.contains (',')) {
                    for (String additionalEmail : additionalEmails.split (',')) {
                        toAddresses.add (additionalEmail);
                    }
                } else {
                    toAddresses.add (additionalEmails);
                }    
            }
            taskEmailAddresses.put (inq.Id, toAddresses);
        }
        for (Task t : tasks ) {
            List <String> toAddresses = taskEmailAddresses.get (t.whatId);
            Messaging.SingleEmailMessage msg = Messaging.renderStoredEmailTemplate (template.Id, null, t.Id);
            
            try {
                String subject = msg.getSubject ();
                subject = subject.replace ((String.valueOf (t.whatId).subString (0, 15)), inqDetails.get (t.whatId).Name);
                msg.setSubject (subject);
                
                String htmlBody = msg.getHTMLBOdy();
                htmlBody = htmlBody.replace ((String.valueOf (t.whatId).subString (0, 15)), inqDetails.get (t.whatId).Name);
                msg.setHTMLBody (htmlBody);
            } catch (Exception e) {
                System.debug (e.getMessage()+'-'+e.getLineNumber());
            }
            
            msg.setToAddresses (toAddresses);
            msg.setSaveAsActivity(false);
            
            mails.add(msg);
        }
        
        Messaging.sendEmailResult[] results = Messaging.sendEmail(mails);
        system.debug (results);
    }
}