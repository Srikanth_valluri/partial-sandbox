@isTest
public class AmendmentTriggerHandlerTest {
    @isTest static void test_method_1(){
        NSIBPM__SR_Template__c srTemplate = InitializeSRDataTest.createSRTemplate('Agent Registration');
            insert srTemplate;
            
            NSIBPM__Service_Request__c SR = InitializeSRDataTest.getSerReq('Agent Registration',false,null);
            sr.ID_Type__c = null;
            sr.Agency_Type__c = 'Individual';
            sr.NSIBPM__SR_Template__c = srTemplate.id;
            sr.Agency__c = null;
            insert sr;
            Amendment__c newAmd = new Amendment__c();
            newAmd.First_Name__c = 'Test';
            newAmd.Service_Request__c = SR.id;
            insert newAmd;
    }
     @isTest static void test_method_2(){
        Id agenTeamRT = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Agent Team').getRecordTypeId();
        Inquiry__c inqObj= new Inquiry__c(RecordTypeId=agenTeamRT,Inquiry_Source__c='Agent Referral',Mobile_Phone_Encrypt__c='456123',Mobile_CountryCode__c='American Samoa: 001684',Mobile_Phone__c='1234',Email__c='mk@gmail.com',First_Name__c='Test',Last_Name__c='Last',CR_Number__c='0987',ORN_Number__c='7842',Agency_Type__c='Corporate',Organisation_Name__c = 'Oliver',isDuplicate__c=false);
        insert inqObj; 
            Amendment__c newAmd = new Amendment__c();
            newAmd.First_Name__c = 'Test';
            newAmd.Inquiry__c = inqObj.id;
            insert newAmd;
    }
}