@isTest
private class CallingListHandlerTest {

    @isTest static void test_callingListHandlerApproval1() {
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Bounced Cheque SR').getRecordTypeId();
        Id bouncedChequeRTIdCallingList = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Bounced Cheque').getRecordTypeId();
        List<Account> lstAccounts = new List<Account>();
        List<Case> lstCases = new List<Case>();
        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        Id adminProfId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User userObj = [SELECT Id FROM User where ProfileId=: adminProfId AND isActive=true LIMIT 1];
        Id SVP_OperationsRoleId,CRM_CommitteeRoleId;
        List<UserRole> lstRole = [SELECT Id, DeveloperName FROM UserRole WHERE DeveloperName = 'SVP_Operations' LIMIT 1];
        if(lstRole.size()>0) {
            SVP_OperationsRoleId = lstRole[0].Id;
        }
        else {
            UserRole SVP_OperationsRole = new UserRole(DeveloperName = 'SVP_Operations', Name = 'SVP Operations');
            insert SVP_OperationsRole;
            SVP_OperationsRoleId = SVP_OperationsRole.Id;
        }

            User SVP_OperationsUser = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser000@amamama.com',
             Username = 'puser000@amamama.com1' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US',
             UserRoleId = SVP_OperationsRoleId
             );
             insert SVP_OperationsUser;
             List<UserRole> lstRoleCommittee = [SELECT Id, DeveloperName FROM UserRole WHERE Name = 'CRM - Committee' LIMIT 1];
             if(lstRoleCommittee.size()>0) {
                CRM_CommitteeRoleId = lstRoleCommittee[0].Id;
            }
            else {
                UserRole CRM_CommitteeRole = [SELECT Id FROM UserRole WHERE Name = 'CRM - Committee' LIMIT 1];
                insert CRM_CommitteeRole;
                CRM_CommitteeRoleId = CRM_CommitteeRole.Id;
            }

            User CRM_CommitteeUser = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser000@amamama.com',
             Username = 'puser000@amamama.com1' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US',
             UserRoleId = CRM_CommitteeRoleId
             );
             insert CRM_CommitteeUser;

        System.runAs(SVP_OperationsUser) {
        // UserRole CRM_CommitteeRole = [SELECT Id FROM UserRole WHERE Name = 'CRM - Committee' LIMIT 1];
            lstAccounts = TestDataFactory_CRM.createCustomers(1);
            insert lstAccounts;

            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
            settingLst2.add(newSetting1);
            insert settingLst2;

            for(Account acc : lstAccounts) {
                lstCases.add(TestDataFactory_CRM.createCase(acc.Id, caseRecordTypeId));
            }
            insert lstCases;
            for(Case objCase : lstCases) {
                lstCallingLists.add(new Calling_List__c(Case__c = objCase.Id, RecordTypeId = bouncedChequeRTIdCallingList));
            }
            Test.startTest();

            insert lstCallingLists;
            for(Calling_List__c objCallList : lstCallingLists) {
                objCallList.Call_Outcome__c = 'Refusing to Pay';
            }
            update lstCallingLists;
            Test.stopTest();
       }
    }

    @isTest static void test_callingListHandlerApproval2() {
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Bounced Cheque SR').getRecordTypeId();
        Id bouncedChequeRTIdCallingList = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Bounced Cheque').getRecordTypeId();
        List<Account> lstAccounts = new List<Account>();
        List<Case> lstCases = new List<Case>();
        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        Id adminProfId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User userObj = [SELECT Id FROM User where ProfileId=: adminProfId AND isActive=true LIMIT 1];
        Id SVP_OperationsRoleId,CRM_CommitteeRoleId;
        List<UserRole> lstRole = [SELECT Id, DeveloperName FROM UserRole WHERE DeveloperName = 'SVP_Operations' LIMIT 1];
        if(lstRole.size()>0) {
            SVP_OperationsRoleId = lstRole[0].Id;
        }
        else {
            UserRole SVP_OperationsRole = new UserRole(DeveloperName = 'SVP_Operations', Name = 'SVP Operations');
            insert SVP_OperationsRole;
            SVP_OperationsRoleId = SVP_OperationsRole.Id;
        }

            User SVP_OperationsUser = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser000@amamama.com',
             Username = 'puser000@amamama.com1' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US',
             UserRoleId = SVP_OperationsRoleId
             );
             insert SVP_OperationsUser;
             List<UserRole> lstRoleCommittee = [SELECT Id, DeveloperName FROM UserRole WHERE Name = 'CRM - Committee' LIMIT 1];
             if(lstRoleCommittee.size()>0) {
                CRM_CommitteeRoleId = lstRoleCommittee[0].Id;
            }
            else {
                UserRole CRM_CommitteeRole = [SELECT Id FROM UserRole WHERE Name = 'CRM - Committee' LIMIT 1];
                insert CRM_CommitteeRole;
                CRM_CommitteeRoleId = CRM_CommitteeRole.Id;
            }

            User CRM_CommitteeUser = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser000@amamama.com',
             Username = 'puser000@amamama.com1' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US',
             UserRoleId = CRM_CommitteeRoleId
             );
             insert CRM_CommitteeUser;

        System.runAs(CRM_CommitteeUser) {
        // UserRole CRM_CommitteeRole = [SELECT Id FROM UserRole WHERE Name = 'CRM - Committee' LIMIT 1];
            lstAccounts = TestDataFactory_CRM.createCustomers(1);
            insert lstAccounts;

            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
            settingLst2.add(newSetting1);
            insert settingLst2;

            for(Account acc : lstAccounts) {
                lstCases.add(TestDataFactory_CRM.createCase(acc.Id, caseRecordTypeId));
            }
            insert lstCases;

            for(Case objCase : lstCases) {
                lstCallingLists.add(new Calling_List__c(Case__c = objCase.Id, RecordTypeId = bouncedChequeRTIdCallingList));
            }

            insert lstCallingLists;
            for(Calling_List__c objCallList : lstCallingLists) {
                objCallList.Call_Outcome__c = 'Refusing to Pay';
                objCallList.Approving_Authorities__c = 'test';
            }
            update lstCallingLists;

            Test.startTest();
            for(Calling_List__c objCallList : lstCallingLists) {
             objCallList.Call_Outcome__c = 'Refusing to Pay';
             objCallList.Approving_Authorities__c = 'CRM - Committee';
            }
            update lstCallingLists;
            Test.stopTest();
       }
    }
    //Added method to test CallingListBouncedChequeHelper()
  /* @isTest static void test_CallingListBouncedChequeHelper() {
        //Id callingListRecordTypeId = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Bounced Cheque').getRecordTypeId();
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Bounced Cheque SR').getRecordTypeId();
        List<Booking__c> lstBookings = new List<Booking__c>();
        List<SR_Booking_Unit__c> lstSRBookingUnits = new List<SR_Booking_Unit__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();

        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
         TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);

        settingLst2.add(newSetting1);
        insert settingLst2;

        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc;
        //NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        //dealSR = TestDataFactory_CRM.createServiceRequest();
        //insert dealSR;
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        objSR.Deal_ID__c = '12345';
        objSR.Reinstatement_Status__c = 'Not Applicable';
        objSR.RecordTypeId = PenaltyWaiverUtility.getRecordTypeId( 'Deal', 'NSIBPM__Service_Request__c' );
        objSR.Booking_Wizard_Level__c = 'Level 4';
        objSR.Approval_Level_Required__c = 'Level 1';
        insert objSR;
        Case objCase = TestDataFactory_CRM.createCase(objAcc.ID, caseRecordTypeId);
        insert objCase;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, objSR.Id, 5);
        insert lstBookings;
        lstBookingUnits = TestDataFactory_CRM.createBookingUnits(lstBookings, 20);
        insert lstBookingUnits;
        lstSRBookingUnits = TestDataFactory_CRM.createSRBookingUnis(objCase.Id,lstBookingUnits);
        insert lstSRBookingUnits;
        Calling_List__c objCallList = new Calling_List__c();
        //objCallList.RecordTypeId = callingListRecordTypeId;
        objCallList.Case__c = objCase.Id;
        objCallList.Account__c = objAcc.Id;
        objCallList.Calling_List_Type__c = 'BC Calling';
        objCallList.Calling_List_Status__c = 'New';
        objCallList.Call_Back_Date__c = system.now();
        objCallList.Customer_Flag__c = true;
        insert objCallList;
        objCallList.Call_Outcome__c = 'AOPT Under process';
        Test.startTest();
        try {
            update objCallList;
        }
        catch(System.DmlException excp) {

        }
        Test.stopTest();
    }*/

    @isTest
    static void test_NotRespondingCallOutcome() {

        insert new IpmsRestServices__c(
                   SetupOwnerId = UserInfo.getOrganizationId(),
                   BaseUrl__c = 'http://83.111.194.181:8045/webservices/rest',
                   Username__c = 'oracle_user',
                   Password__c = 'crp1user',
                   Client_Id__c = '8MABLQM-KJ8I8-1XA58-WWCM1S1',
                   BearerToken__c = 'eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1MzAwtTMwsDIx2l1IoCqIC5oRFIoLQ4tSgvMTcVqM_C19HJJ9BX19vLwtNC1zDC0dRCNzzc2dcw2FCpFgBXRb-1XQAAAA.6Ym224Vwr9AniBeq6gL8OM9u4vGnUB_vbEUVjWojg14',
                   Timeout__c = 120000
                   );

        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();

        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                        OnOffCheck__c = true);
        insert newSetting1;

        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__pc = '0088888123';
        objAcc.Email__pc = 'test@eternussolutions.com.com';
        insert objAcc;

        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = lstBookings[0].Id,
                                      Registration_ID__c = '3901');
        insert bookingUnit;

        Id collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
        Calling_List__c objCallList = new Calling_List__c();
        objCallList.Account__c = objAcc.Id;
        objCallList.Booking_Unit__c = bookingUnit.Id;
        objCallList.RecordTypeId = collectioncallingRecordTypeId;
        objCallList.IsHideFromUI__c = false;
        //objCallList.Calling_List_Status__c = 'New';
        insert objCallList;
        objCallList.Call_Outcome__c = 'Not Responding';
        Test.startTest();
        try {
            update objCallList;
        }
        catch(System.DmlException excp) {

        }

        FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
         Test.stopTest();
    }

    @isTest
    static void test_NotRespondingCallOutcomeWithSOA() {

        List<Booking__c> lstBookings = new List<Booking__c>();
        List<Booking_Unit__c> lstBookingUnits = new List<Booking_Unit__c>();

        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                        OnOffCheck__c = true);
        insert newSetting1;

        insert new IpmsRestServices__c(
                   SetupOwnerId = UserInfo.getOrganizationId(),
                   BaseUrl__c = 'http://83.111.194.181:8045/webservices/rest',
                   Username__c = 'oracle_user',
                   Password__c = 'crp1user',
                   Client_Id__c = '8MABLQM-KJ8I8-1XA58-WWCM1S1',
                   BearerToken__c = 'eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSKs5IBKK8bCUdpczEEiUrQ1MzAwtTMwsDIx2l1IoCqIC5oRFIoLQ4tSgvMTcVqM_C19HJJ9BX19vLwtNC1zDC0dRCNzzc2dcw2FCpFgBXRb-1XQAAAA.6Ym224Vwr9AniBeq6gL8OM9u4vGnUB_vbEUVjWojg14',
                   Timeout__c = 120000
                   );

        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        objAcc.Mobile_Phone_Encrypt__pc = '0088888123';
        objAcc.Email__pc = 'test@eternussolutions.com.com';
        insert objAcc;

        NSIBPM__Service_Request__c dealSR = new NSIBPM__Service_Request__c();
        dealSR = TestDataFactory_CRM.createServiceRequest();
        insert dealSR;

        lstBookings = TestDataFactory_CRM.createBookingForAccount(objAcc.ID, dealSR.Id, 1);
        insert lstBookings;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = lstBookings[0].Id,
                                      Registration_ID__c = '3901');
        insert bookingUnit;

        Id collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
        Calling_List__c objCallList = new Calling_List__c();
        objCallList.Account__c = objAcc.Id;
        objCallList.Booking_Unit__c = bookingUnit.Id;
        objCallList.RecordTypeId = collectioncallingRecordTypeId;
        objCallList.IsHideFromUI__c = false;
        //objCallList.Calling_List_Status__c = 'New';
        insert objCallList;
        objCallList.Call_Outcome__c = 'Not Responding - SOA Sent';
        Test.startTest();
        try {
            update objCallList;
        }
        catch(System.DmlException excp) {

        }

        String blobStr = 'Test Blob Response';

        FmHttpCalloutMock.Response unitSoaMockResponse = new FmHttpCalloutMock.Response(200, 'Success', '   {  '  +
         '       "responseId": "3072019-10305298",  '  +
         '       "responseTime": "Wed Jul 03 10:30:52 GMT+04:00 2019",  '  +
         '       "status": "S",  '  +
         '       "responseMessage": "Process Completed",  '  +
         '       "elapsedTimeMs": 8497,  '  +
         '       "responseLines": [  '  +
         '           {  '  +
         '               "documentName": "DPSOA",  '  +
         '               "language": "EN",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/e1054bf2d703bca1e8fe101d3ac5efcd"  '  +
         '           },  '  +
         '           {  '  +
         '               "documentName": "DPSOA_AR",  '  +
         '               "language": "AR",  '  +
         '               "url": "https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a"  '  +
         '           }  '  +
         '       ],  '  +
         '       "complete": true  '  +
         '  }  ');

        String UNIT_SOA_str = String.format(FmIpmsRestCoffeeServices.SF_REST_BASEURL+FmIpmsRestCoffeeServices.UNIT_SOA, new List<String> {'3901'});

        FmHttpCalloutMock.Response blobResponse = new FmHttpCalloutMock.Response(200, 'Success', blobStr);

        FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
        Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
            UNIT_SOA_str => unitSoaMockResponse,
            'https://ptctest.damacgroup.com/COFFEE/apex/document/view/2e855f9489df0712b4bd8ea9e2848c5a' => blobResponse,
            'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };

        Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
        Test.stopTest();
    }
    
    @isTest static void test_callingListHandlerApprovalNullApproval() {
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Bounced Cheque SR').getRecordTypeId();
        Id bouncedChequeRTIdCallingList = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Bounced Cheque').getRecordTypeId();
        List<Account> lstAccounts = new List<Account>();
        List<Case> lstCases = new List<Case>();
        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        Id adminProfId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User userObj = [SELECT Id FROM User where ProfileId=: adminProfId AND isActive=true LIMIT 1];
        Id SVP_OperationsRoleId,CRM_CommitteeRoleId;
        List<UserRole> lstRole = [SELECT Id, DeveloperName FROM UserRole WHERE DeveloperName = 'SVP_Operations' LIMIT 1];
        if(lstRole.size()>0) {
            SVP_OperationsRoleId = lstRole[0].Id;
        }
        else {
            UserRole SVP_OperationsRole = new UserRole(DeveloperName = 'SVP_Operations', Name = 'SVP Operations');
            insert SVP_OperationsRole;
            SVP_OperationsRoleId = SVP_OperationsRole.Id;
        }

            User SVP_OperationsUser = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser000@amamama.com',
             Username = 'puser000@amamama.com1' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US',
             UserRoleId = SVP_OperationsRoleId
             );
             insert SVP_OperationsUser;
             List<UserRole> lstRoleCommittee = [SELECT Id, DeveloperName FROM UserRole WHERE Name = 'CRM - Committee' LIMIT 1];
             if(lstRoleCommittee.size()>0) {
                CRM_CommitteeRoleId = lstRoleCommittee[0].Id;
            }
            else {
                UserRole CRM_CommitteeRole = [SELECT Id FROM UserRole WHERE Name = 'CRM - Committee' LIMIT 1];
                insert CRM_CommitteeRole;
                CRM_CommitteeRoleId = CRM_CommitteeRole.Id;
            }

            User CRM_CommitteeUser = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser000@amamama.com',
             Username = 'puser000@amamama.com1' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US',
             UserRoleId = CRM_CommitteeRoleId
             );
             insert CRM_CommitteeUser;

        System.runAs(CRM_CommitteeUser) {
        // UserRole CRM_CommitteeRole = [SELECT Id FROM UserRole WHERE Name = 'CRM - Committee' LIMIT 1];
            lstAccounts = TestDataFactory_CRM.createCustomers(1);
            insert lstAccounts;

            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
            settingLst2.add(newSetting1);
            insert settingLst2;

            for(Account acc : lstAccounts) {
                lstCases.add(TestDataFactory_CRM.createCase(acc.Id, caseRecordTypeId));
            }
            insert lstCases;

            for(Case objCase : lstCases) {
                lstCallingLists.add(new Calling_List__c(Case__c = objCase.Id, RecordTypeId = bouncedChequeRTIdCallingList));
            }

            insert lstCallingLists;
            for(Calling_List__c objCallList : lstCallingLists) {
                objCallList.Call_Outcome__c = 'Early Settlement';
                objCallList.Approving_Authorities__c = 'test';
            }
            update lstCallingLists;

            Test.startTest();
            for(Calling_List__c objCallList : lstCallingLists) {
             //objCallList.Call_Outcome__c = 'AOPT Under process';
             objCallList.Approving_Authorities__c = '';
            }
            update lstCallingLists;
             FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
                'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
    
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            Test.stopTest();
       }
    }
    
    @isTest static void test_callingListHandlerApprovalNullApproval1() {
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Bounced Cheque SR').getRecordTypeId();
        Id bouncedChequeRTIdCallingList = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Bounced Cheque').getRecordTypeId();
        List<Account> lstAccounts = new List<Account>();
        List<Case> lstCases = new List<Case>();
        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        Id adminProfId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User userObj = [SELECT Id FROM User where ProfileId=: adminProfId AND isActive=true LIMIT 1];
        Id SVP_OperationsRoleId,CRM_CommitteeRoleId;
        List<UserRole> lstRole = [SELECT Id, DeveloperName FROM UserRole WHERE DeveloperName = 'SVP_Operations' LIMIT 1];
        if(lstRole.size()>0) {
            SVP_OperationsRoleId = lstRole[0].Id;
        }
        else {
            UserRole SVP_OperationsRole = new UserRole(DeveloperName = 'SVP_Operations', Name = 'SVP Operations');
            insert SVP_OperationsRole;
            SVP_OperationsRoleId = SVP_OperationsRole.Id;
        }

            User SVP_OperationsUser = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser000@amamama.com',
             Username = 'puser000@amamama.com1' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US',
             UserRoleId = SVP_OperationsRoleId
             );
             insert SVP_OperationsUser;
             List<UserRole> lstRoleCommittee = [SELECT Id, DeveloperName FROM UserRole WHERE Name = 'CRM - Committee' LIMIT 1];
             if(lstRoleCommittee.size()>0) {
                CRM_CommitteeRoleId = lstRoleCommittee[0].Id;
            }
            else {
                UserRole CRM_CommitteeRole = [SELECT Id FROM UserRole WHERE Name = 'CRM - Committee' LIMIT 1];
                insert CRM_CommitteeRole;
                CRM_CommitteeRoleId = CRM_CommitteeRole.Id;
            }

            User CRM_CommitteeUser = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser000@amamama.com',
             Username = 'puser000@amamama.com1' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US',
             UserRoleId = CRM_CommitteeRoleId
             );
             insert CRM_CommitteeUser;

        System.runAs(CRM_CommitteeUser) {
        // UserRole CRM_CommitteeRole = [SELECT Id FROM UserRole WHERE Name = 'CRM - Committee' LIMIT 1];
            lstAccounts = TestDataFactory_CRM.createCustomers(1);
            insert lstAccounts;

            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
            settingLst2.add(newSetting1);
            insert settingLst2;

            for(Account acc : lstAccounts) {
                lstCases.add(TestDataFactory_CRM.createCase(acc.Id, caseRecordTypeId));
            }
            insert lstCases;

            for(Case objCase : lstCases) {
                lstCallingLists.add(new Calling_List__c(Case__c = objCase.Id, RecordTypeId = bouncedChequeRTIdCallingList));
            }

            insert lstCallingLists;
            for(Calling_List__c objCallList : lstCallingLists) {
                objCallList.Call_Outcome__c = 'Negotiating';
                objCallList.Approving_Authorities__c = 'test';
            }
            update lstCallingLists;

            Test.startTest();
            for(Calling_List__c objCallList : lstCallingLists) {
             objCallList.Call_Outcome__c = 'Refusing to Pay';
             objCallList.Approving_Authorities__c = '';
            }
            update lstCallingLists;
            FmHttpCalloutMock.Response sendGridResponse = new FmHttpCalloutMock.Response(200, 'Accepted', ' ');
            Map<String, FmHttpCalloutMock.Response > responseMap = new Map<String, FmHttpCalloutMock.Response> {
                'https://api.sendgrid.com/v3/mail/send' => sendGridResponse };
    
            Test.setMock(HttpCalloutMock.class, new FmHttpCalloutMock(responseMap));
            Test.stopTest();
       }
    }
    
        @isTest static void test_callingListHandlerSVP() {
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Bounced Cheque SR').getRecordTypeId();
        Id bouncedChequeRTIdCallingList = Schema.SObjectType.Calling_List__c.getRecordTypeInfosByName().get('Bounced Cheque').getRecordTypeId();
        List<Account> lstAccounts = new List<Account>();
        List<Case> lstCases = new List<Case>();
        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        Id adminProfId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User userObj = [SELECT Id FROM User where ProfileId=: adminProfId AND isActive=true LIMIT 1];
        Id SVP_OperationsRoleId,CRM_CommitteeRoleId;
        List<UserRole> lstRole = [SELECT Id, DeveloperName FROM UserRole WHERE DeveloperName = 'SVP_Operations' LIMIT 1];
        if(lstRole.size()>0) {
            SVP_OperationsRoleId = lstRole[0].Id;
        }
        else {
            UserRole SVP_OperationsRole = new UserRole(DeveloperName = 'SVP_Operations', Name = 'SVP Operations');
            insert SVP_OperationsRole;
            SVP_OperationsRoleId = SVP_OperationsRole.Id;
        }

            User SVP_OperationsUser = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser000@amamama.com',
             Username = 'puser000@amamama.com1' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US',
             UserRoleId = SVP_OperationsRoleId
             );
             insert SVP_OperationsUser;
             List<UserRole> lstRoleCommittee = [SELECT Id, DeveloperName FROM UserRole WHERE Name = 'CRM - Committee' LIMIT 1];
             if(lstRoleCommittee.size()>0) {
                CRM_CommitteeRoleId = lstRoleCommittee[0].Id;
            }
            else {
                UserRole CRM_CommitteeRole = [SELECT Id FROM UserRole WHERE Name = 'CRM - Committee' LIMIT 1];
                insert CRM_CommitteeRole;
                CRM_CommitteeRoleId = CRM_CommitteeRole.Id;
            }

            User CRM_CommitteeUser = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser000@amamama.com',
             Username = 'puser000@amamama.com1' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US',
             UserRoleId = CRM_CommitteeRoleId
             );
             insert CRM_CommitteeUser;

        System.runAs(CRM_CommitteeUser) {
        // UserRole CRM_CommitteeRole = [SELECT Id FROM UserRole WHERE Name = 'CRM - Committee' LIMIT 1];
            lstAccounts = TestDataFactory_CRM.createCustomers(1);
            insert lstAccounts;

            List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
            TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
            settingLst2.add(newSetting1);
            insert settingLst2;

            for(Account acc : lstAccounts) {
                lstCases.add(TestDataFactory_CRM.createCase(acc.Id, caseRecordTypeId));
            }
            insert lstCases;

            for(Case objCase : lstCases) {
                lstCallingLists.add(new Calling_List__c(Case__c = objCase.Id, RecordTypeId = bouncedChequeRTIdCallingList));
            }

            insert lstCallingLists;
            for(Calling_List__c objCallList : lstCallingLists) {
                objCallList.Call_Outcome__c = 'Negotiating';
                objCallList.Approving_Authorities__c = 'test';
            }
            update lstCallingLists;

            Test.startTest();
            for(Calling_List__c objCallList : lstCallingLists) {
             objCallList.Call_Outcome__c = 'Refusing to Pay';
             objCallList.Approving_Authorities__c = 'SVP Operations';
            }
            update lstCallingLists;
            Test.stopTest();
       }
    }




}