public class SendEmailUtility {
    public static Messaging.SingleEmailMessage createEmail(String mailBody,String[] toAddresses,String subject,OrgWideEmailAddress[] owea){
        Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
        singleMail.setsubject(subject);
        singleMail.setHtmlBody(mailBody);
        singleMail.setToAddresses(toAddresses);
        if ( owea.size() > 0 ) {
    			singleMail.setOrgWideEmailAddressId(owea.get(0).Id);
		}
        return singleMail;
    }
}