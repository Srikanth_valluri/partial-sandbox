/**************************************************************************************************
* Name               : AgentAgentPropertyBookingControllerTest                                              *
* Description        : Test class for AgentPropertyBookingController.                                  *
* Created Date       : 20/04/2017                                                                 *
* Created By         : NSI                                                                        *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR                 DATE            COMMENTS                                     *
* 1.0         Accely-Naresh          27-09-2017      Initial drafts
==================================================================================================*
* 1.1         Accely- Pratiksha      28-09-2017      changes in code to increase code coverage -- *
*                                                    Current code coverage -26%                   *
**************************************************************************************************/
@isTest 
private class AgentPropertyBookingControllerTest {
    
    public static list<buyer__c> buyerLst = new list<buyer__c>();
    public static list<Payment_Plan__c> pplist = new list<Payment_Plan__c>();
    public Static AgentPropertyBookingController obj =  new AgentPropertyBookingController();
    public static deal_team__c dt = new deal_team__c();
    @testsetup
    public static void SetupData(){
        
         Trg_Ctrl_Var__c active = new Trg_Ctrl_Var__c();
        active.name = 'trg_Campaign';
        active.Activate__c = true;
        insert active;
        
        
        Id RecTypeid = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Standalone').getRecordTypeId();
        Campaign__c c = new Campaign__c();
        c.Campaign_Name__c = 'Standalone';
        C.End_Date__c = system.today();
        C.Marketing_End_Date__c = system.today();
        C.Marketing_Start_Date__c = system.today();
        C.Start_Date__c = system.today();
        insert c;
            
        Promotion__c pro = new Promotion__c();
        pro.Start_Date__c= system.today().adddays(-10);
        pro.end_date__c = system.today().adddays(10);
        pro.Promotion_Title__c = 'abc';
        pro.campaign__c = c.id;
        insert pro;
        
        Location__c objLoc = InitializeSRDataTest.createLocation('123','Building');
        objLoc.Name = 'DEB/7/703' ;
        insert objLoc;   
        
        
        Inventory__c lstInv = new Inventory__c();
        lstInv.Property_Country__c = 'United Arab Emirates';
        lstInv.Property_City__c = 'Dubai';
        lstInv.Inventory_ID__c = '345wer';
        lstInv.Status__c = 'Test' ;
        lstInv.List_Price__c = 2564 ;
        lstInv.Floor_Package_ID__c = '45' ;
        lstInv.Floor_Package_Type__c = 'Floor' ;
        lstInv.Floor_Price_List__c = '2568' ;
        lstInv.Status__c = 'Released' ;
        lstInv.Unit_Location__c= objLoc.Id;
        lstInv.Special_Price__c = 2212;
        insert lstInv;  
        
        Id RecordTypeIdAgency = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        Account Acc =new Account();
        Acc.Name='testAccP';
        Acc.RecordTypeId=RecordTypeIdAgency;
        insert Acc;
        
        NSIBPM__Service_Request__c sr = InitializeSRDataTest.getSerReq('Deal',true,null);
        sr.Agency__c = Acc.ID; 
        sr.Booking_Wizard_Level__c ='Level 1';
        insert sr;
        
        System.debug('...sr...'+sr.Name);
        
        Booking__c book = new booking__c();
        book.Deal_SR__c = sr.id;
        book.Booking_Channel__c = 'Web';
        book.Unique_Key__c = 'H430924032 - a0M3E000001RPnQUAW';
        insert book;
        
        Booking_Unit__c bu = new Booking_Unit__c();
        bu.Booking__c = book.id;
        bu.Unique_Key__c = 'test1234';
        bu.Payment_Method__c = 'Cash';
        bu.Primary_Buyer_s_Email__c = 'raviteja@nsiglobal.com';
        bu.Primary_Buyer_s_Name__c = 'testNSI';
        bu.Primary_Buyer_s_Nationality__c = 'test';
        bu.Inventory__c = lstInv.id;
        insert bu;
        ID RecordTypeIdAGENT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Agency').getRecordTypeId();
        Account a = new Account();
        a.recordtypeid=RecordTypeIdAGENT;
        a.Name = 'TestAccount';
        a.Agency_Short_Name__c = 'testShrName';
        insert a;
        
        Contact con = new contact();
        con.LastName = 'testlnme';
        con.AccountId = a.Id ;
        con.Agent_Representative__c=true;
        insert con;
        
         profile p= [select Id from Profile where name='Customer Community - Admin'];
         //user 
         User portalUser = new User();
         portalUser = new User(alias = 'test456', email='testusr1@test.com',
                emailencodingkey='UTF-8', lastname='User 456', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.id, country='United Arab Emirates',IsActive =true,
                ContactId = con.Id,timezonesidkey='America/Los_Angeles', username='testusr1@test.com');
                
         insert portalUser;
                 
        
            dt.Associated_PC__c = portalUser.id;
            dt.Associated_Deal__c  = sr.id;
            insert dt;
        
        Set<Id> BUid =  new Set<Id>();
        BUid.add(bu.Id);
       
        Option__c OpList = new Option__c();
        OpList.Booking_Unit__c = bu.Id ;
        OpList.PromotionName__c  = 'Test' ;
        insert OpList ;
        
        
        Inquiry__c inquiryRecord = new Inquiry__c();
        inquiryRecord.By_Pass_Validation__c = true;
        inquiryRecord.Party_ID__c = '12345';
        inquiryRecord.Title__c = 'MR.';
        inquiryRecord.Title_Arabic__c ='MR.';
        inquiryRecord.First_Name__c = 'Test';
        inquiryRecord.First_Name_Arabic__c ='Test';
        inquiryRecord.Last_Name__c ='Test';
        inquiryRecord.Last_Name_Arabic__c = 'Test';
        insert inquiryRecord;
        
        
        
        buyer__c b = new buyer__c();
        b.Buyer_Type__c =  'Individual';
        b.Address_Line_1__c =  'Ad1';
        b.Country__c =  'United Arab Emirates';
        b.Account__c = Acc.Id;
        b.Inquiry__c = inquiryRecord.Id ;
        b.City__c = 'Dubai' ;
       // b.Date_of_Birth__c = string.valueof(system.today().addyears(-30)) ;
        b.Date_of_Birth__c='09/09/1993';
        
        b.Email__c = 'test@test.com';
        b.First_Name__c = 'firstname' ;
        b.Last_Name__c =  'lastname';
        b.Nationality__c = 'Indian' ;
        //b.Passport_Expiry_Date__c = string.valueof(system.today().addyears(20)) ;
        b.Passport_Expiry_Date__c = '09/09/2022';
        b.Passport_Number__c = 'J0565556' ;
        b.Phone__c = '569098767' ;
        b.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        b.Place_of_Issue__c =  'India';
        b.Title__c = 'Mr';
        b.booking__c = book.id;
        b.Primary_Buyer__c =true;
        insert b;
        
        
        buyer__c b1 = new buyer__c();
        b1.Buyer_Type__c =  'Individual';
        b1.Address_Line_1__c =  'Ad1';
        b1.Country__c =  'United Arab Emirates';
        b1.City__c = 'Dubai' ;
        b1.Date_of_Birth__c = string.valueof(system.today().addyears(-30)) ;
        b1.Email__c = 'test@test.com';
        b1.First_Name__c = 'firstname' ;
        b1.Last_Name__c =  'lastname';
        b1.Nationality__c = 'Indian' ;
        b1.Account__c = Acc.Id;
        b1.Inquiry__c = inquiryRecord.Id ;
        b1.Passport_Expiry_Date__c ='09/09/2022' ;
        b1.Passport_Number__c = 'J0565556' ;
        b1.Phone__c = '569098767' ;
        b1.Phone_Country_Code__c = 'United Arab Emirates: 00971' ;
        b1.Place_of_Issue__c =  'India';
        b1.Title__c = 'Mr';
        b1.booking__c = book.id;
        insert b1;
           
        
        buyerlst.add(b);
        buyerlst.add(b1);
        
        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Booking_Unit__c = bu.id;
        pp.Payment_Term__c = 'Payment Plan';
        pp.Effective_From__c = system.today().adddays(-4);
        pp.Effective_To__c = system.today().adddays(7); 
        pp.Building_Location__c =  objLoc.id;      
        insert pp;
        
        Payment_Terms__c pt = new Payment_Terms__c();
        pt.Payment_Plan__c = pp.id;
        pt.Booking_Unit__c = bu.id;
        pt.Percent_Value__c = '5';
        insert pt;
        
        pplist.add(pp);
        
    }
    
    // To cover  getSrDetails() & setWizardLevel() , prePopulateData()  , createBookingRecords()
    public static testmethod void setWizardLevel1(){
        Test.startTest();
        NSIBPM__Service_Request__c  sr = [select id ,Booking_Wizard_Level__c from NSIBPM__Service_Request__c  where Agency_Email_2__c  = 'test2@gmail.com'];
        String Id = sr.Id ;
        obj.setWizardLevel(sr);
        obj.getSrDetails(Id);
        obj.prePopulateData(Id);
        Set<String> uniqueKeySet = New Set<String>();
        uniqueKeySet.add('uniqueKeySet');
        obj.createBookingRecords(sr,uniqueKeySet);
        
        
       
        
        NSIBPM__Service_Request__c  sr1 = [select id ,Booking_Wizard_Level__c from NSIBPM__Service_Request__c  where Agency_Email_2__c  = 'test2@gmail.com'];
        sr.Booking_Wizard_Level__c = 'Level 2';
        update sr ;
        obj.setWizardLevel(sr1);
         
        
         NSIBPM__Service_Request__c  sr2 = [select id ,Booking_Wizard_Level__c from NSIBPM__Service_Request__c  where Agency_Email_2__c  = 'test2@gmail.com'];
        sr.Booking_Wizard_Level__c = 'Level 3';
        update sr ;
        obj.setWizardLevel(sr2);
        
        
         NSIBPM__Service_Request__c  sr3 = [select id ,Booking_Wizard_Level__c from NSIBPM__Service_Request__c  where Agency_Email_2__c  = 'test2@gmail.com'];
        sr.Booking_Wizard_Level__c = 'Level 4';
        update sr ;
        obj.setWizardLevel(sr3);
        
        NSIBPM__Service_Request__c  sr4 = [select id , Booking_Wizard_Level__c from NSIBPM__Service_Request__c  where Agency_Email_2__c  = 'test2@gmail.com'];
        sr.Booking_Wizard_Level__c = 'Level 5';
        update sr ;
        obj.setWizardLevel(sr4);
        
        
        NSIBPM__Service_Request__c  sr5 = [select id , Booking_Wizard_Level__c from NSIBPM__Service_Request__c  where Agency_Email_2__c  = 'test2@gmail.com'];
        sr.Booking_Wizard_Level__c = 'Level 9';
        update sr ;
        obj.setWizardLevel(sr5);
        
        
        Set<Id> teamMemberIdSet = new Set<Id>();
        teamMemberIdSet.add(dt.Associated_PC__c);
        obj.createDealTeam(sr,teamMemberIdSet); // Later Id Excpetion
        Test.stopTest();  
    }
    
    
    // to cover getInventoryDetails()  , checkInventoryAvailable() , closeErrorBox() , getCampaignRelatedBookingUnit() , getPackageRelatedInventories() Inventory Related 
    public static testMethod void getInventoryDetailsTest(){
        Test.startTest();
         Inventory__c lstInv =  [Select id ,Property_City__c ,Floor_Package_ID__c, Unit_Location__c from Inventory__c where Property_City__c = 'Dubai'];
         Set<Id> SetofId  =  new Set<Id>();
         SetofId.add(lstInv.Id);
         obj.getInventoryDetails(SetofId);
         obj.checkInventoryAvailable(SetofId);
         obj.closeErrorBox();
         
         obj.getCampaignRelatedBookingUnit(SetofId);  
        
         
        UtilityWrapperManager UWM = new UtilityWrapperManager();
        List<UtilityWrapperManager> UWMList = new List<UtilityWrapperManager>();
        UtilityWrapperManager.SharingWrapper ser = new UtilityWrapperManager.SharingWrapper('test', 'test', 'test', 'test', 'test');
        UtilityWrapperManager.CampaignWrapper cam = new UtilityWrapperManager.CampaignWrapper(false, new Campaign__c (), new List<Promotion__c> ());
        UtilityWrapperManager.BookingWrapper BW = new UtilityWrapperManager.BookingWrapper('Damac','Start');
        UtilityWrapperManager.PriceRageWrapper pt = new UtilityWrapperManager.PriceRageWrapper(12.3, 23.2);
        UtilityWrapperManager.InventoryDetailsWrapper idw = new UtilityWrapperManager.InventoryDetailsWrapper( '1233245324','1231','test','123432423','Damac','dsfw','test','one','2','2','marketing',12.1,23.2,21.2);
        List<UtilityWrapperManager.InventoryBuyerWrapper> ibw = new List<UtilityWrapperManager.InventoryBuyerWrapper>(); 
        
        UtilityWrapperManager.InventoryBuyerWrapper ibwn = new UtilityWrapperManager.InventoryBuyerWrapper(true,false, false, 32.2,3432432.234,'test','test','test','test',3,lstInv,new Booking_Unit__c(),new Map<String, Payment_Plan__c>(),buyerlst);
        ibw.add(ibwn);
        AgentPropertyBookingController.validateBuyerFields(ibw,true,true);
         
         
         // to cover getPackageRelatedInventories() Keep this method in Last 
         Set<String> PackageName = new Set<String>();
        PackageName.add('45');
        obj.getPackageRelatedInventories(PackageName); 
        Test.stopTest(); 
        
    }
    
    // To cover getPaymentPlanDetails() & createBookingUnitRecords() , getInquiryRelatedCampaign() , getAssociatedCampaign()
    
    public static testMethod void BookingUnitTest(){
         Test.startTest();
         Booking_Unit__c bu  = [select id , Inventory__c , Unique_Key__c,Booking__c from Booking_Unit__c where Primary_Buyer_s_Name__c = 'testNSI'];
         Set<Id> SetofId  =  new Set<Id>();
         SetofId.add(bu.Id);
         obj.getPaymentPlanDetails(SetofId);
           
         
         // to cover createBookingUnitRecords  
         Booking__c   bb  = [select id , Unique_Key__c from Booking__c   where Booking_Channel__c = 'Web'];
         Map<String, List<Booking_Unit__c>> unitUniqueMap = new Map<String, List<Booking_Unit__c>>();
         Map<String, Booking__c> bookingUniqueKeyMap =  new Map<String, Booking__c> ();
         bookingUniqueKeyMap.put(bb.id ,bb);
         List<Booking_Unit__c>  buList =  new List<Booking_Unit__c>();
         buList.add(bu);
         unitUniqueMap.put('Test' ,buList);
        
        
        
        // to cover  getInquiryRelatedCampaign()
         obj.getInquiryRelatedCampaign(unitUniqueMap);
         
         // to cover  getAssociatedCampaign()
         obj.getAssociatedCampaign(unitUniqueMap);
         
         obj.createBookingUnitRecords(bookingUniqueKeyMap,unitUniqueMap); // Later Some Exception
         
         Test.stopTest();
        
    }
    
    
      // To cover getSelectedPaymentPlanDetails() & createPaymentTerms() , createPaymentPlans()
    public static testMethod void PaymentPlanDetailsTest(){
         Test.startTest();
         Payment_Plan__c  pp  = [select id ,TERM_ID__c from Payment_Plan__c  where Payment_Term__c = 'Payment Plan'];
         Set<Id> SetofId  =  new Set<Id>();
         SetofId.add(pp.Id);
         obj.getSelectedPaymentPlanDetails(SetofId);
         
         // to cover createPaymentTerms
         Map<String, String> insertedPaymentPlanMap = new Map<String, String>();
         insertedPaymentPlanMap.put(pp.Id ,'Test');
         insertedPaymentPlanMap.put(pp.Id,'Test');
         Map<String, List<Payment_Terms__c>> inventoryPaymentTermsMap =  new  Map<String, List<Payment_Terms__c>>();
         Payment_Terms__c pt = [Select id ,Line_ID__c from Payment_Terms__c where Percent_Value__c = '5'];
         List<Payment_Terms__c> ptn =  new  List<Payment_Terms__c>();
         ptn.add(pt);
         inventoryPaymentTermsMap.put('Test' ,ptn );
         obj.createPaymentTerms(insertedPaymentPlanMap,inventoryPaymentTermsMap);
         
         
         
         // To cover createPaymentPlans()
            Booking_Unit__c bu  = [select id,Unique_Key__c from Booking_Unit__c where Primary_Buyer_s_Name__c = 'testNSI'];
          Map<String, Payment_Plan__c> inventoryPaymentPlanMap =  new  Map<String, Payment_Plan__c>();
          Map<String, String> bookingUnitInventoryMap = new Map<String, String>();
          bookingUnitInventoryMap.put( bu.Id , 'Test');
          inventoryPaymentPlanMap.put('Test' , pp);
          obj.createPaymentPlans(bookingUnitInventoryMap,inventoryPaymentPlanMap,bookingUnitInventoryMap);
          Test.stopTest();
        
    } 
    
    
    
        /* To cover getBookingDetails() , 
          validateEmailAddress() , 
          validatePhone() , validateDateOfBirth() ,getBuyerFields() , getRoadshowCampaign()  Method
        */
    public static testMethod void BookingTest(){
         Test.startTest();
         Booking__c   bb  = [select id from Booking__c   where Booking_Channel__c = 'Web'];
         Set<Id> SetofId  =  new Set<Id>();
         SetofId.add(bb.Id);
         obj.getBookingDetails(SetofId);
         
         obj.getRoadshowCampaign();
         AgentPropertyBookingController.validateEmailAddress('Test@Test.com');
         AgentPropertyBookingController.validatePhone('9925486527');
         AgentPropertyBookingController.validateDateOfBirth(system.today().addyears(-30));
         AgentPropertyBookingController.getBuyerFields();
         Test.stopTest();
        
    }
    
    
    
    // To cover Void Method i.e 
    public static testMethod void createPaymentTermsTest(){
        
        Test.startTest();
        AgentPropertyBookingController  onTest =  new AgentPropertyBookingController();
        onTest.selectedCampaignId = 'Test';
        onTest.mode = 'mode';
        onTest.inventoryIdsList = null ;
        
        obj.getPhoneCodeValueMap(); 
        obj.clearIds();
        obj.saveBooking();  
        obj.submitBooking();  
        obj.selectPaymentMethod();
        obj.selectPaymentPlan();
        obj.createPortfolio();
        obj.selectUnits();
        obj.verifyOPT();
        obj.init();
        Test.stopTest();
        
        
    }
    
    // To cover Method i.e createBuyerRecords() , updateBuyerRecords()
    public static testMethod void createBuyerRecordsTest(){
        
        Test.startTest();
        // to cover createBuyerRecords
        Map<String, Booking__c> bookingUniqueKeyMap =  new Map<String, Booking__c>();
        Set<String> uniqueKeySet =  new Set<String>();
        Map<String, List<Buyer__c>> newBuyerUniqueMap =  new Map<String, List<Buyer__c>>();
        
        Booking__c   bb  = [select id from Booking__c   where Booking_Channel__c = 'Web'];
        bookingUniqueKeyMap.put('Test' ,bb);
        uniqueKeySet.add('uniqueKeySet');
        uniqueKeySet.add('Test');
        
        buyer__c b1 = [select id , CR_Number__c , Passport_Number__c  from buyer__c where Country__c =  'United Arab Emirates' limit 1];
        List<Buyer__c> buyer = new List<Buyer__c>();
        buyer.add(b1);
        
        newBuyerUniqueMap.put('uniqueKeySet' ,buyer);
        obj.createBuyerRecords(bookingUniqueKeyMap,uniqueKeySet,newBuyerUniqueMap);
        
        
        // To cover updateBuyerRecords
        
        Map<String, Buyer__c> BuyerMap =  new Map<String, Buyer__c>();
        BuyerMap.put(b1.id ,b1);
        Map<String, Map<String, Buyer__c>> updateBuyerUniqueMap =  new Map<String, Map<String, Buyer__c>> ();
        updateBuyerUniqueMap.put(b1.Id , BuyerMap);  
        
       //obj.updateBuyerRecords(bookingUniqueKeyMap,updateBuyerUniqueMap);  // Later Some exception
        
        
        Test.stopTest();
        
        
    }
    
   // To cover  createAssociatedCampaign()
    public static testMethod void createAssociatedCampaignTest(){
        Test.startTest();
        // Ro Cover createAssociatedCampaign()
        Map<Id, Inquiry__c> buyerInquiryMap =  new  Map<Id, Inquiry__c>();
        Inquiry__c inquiryRecord = [select id from Inquiry__c where Party_ID__c = '12345'];
        buyerInquiryMap.put(inquiryRecord.id ,inquiryRecord);
        obj.createAssociatedCampaign(buyerInquiryMap);  
        Test.stopTest();
    }
    
    
       // To cover  validateSelectedUnits()
    public static testMethod void validateSelectedUnitsTest(){
        Test.startTest();
         
         Account Acc  =  [select id from Account Where Name='testAccP'];
         Inventory__c lstInv =  [Select id ,Property_City__c ,Floor_Package_ID__c, Unit_Location__c from Inventory__c where Property_City__c = 'Dubai'];
         Set<Id> SetofId  =  new Set<Id>();
         SetofId.add(lstInv.Id);
         
         Agent_Site__c site = new Agent_Site__c();
         site.Agency__c = Acc.Id ;
         site.Name = 'UAE' ;
         site.Org_ID__c = '81' ;
         insert site ;
         AgentPropertyBookingController.validateSelectedUnits(Acc.Id ,SetofId);
         
        Test.stopTest();   
    }  
    
      
          // To cover  createInquiry()
    public static testMethod void createInquiryTest(){  
         Test.startTest();
          Buyer__c b = [select id, Date_of_Birth__c,Passport_Expiry_Date__c from Buyer__c  where  Country__c =  'United Arab Emirates' limit 1];
         // b.Date_of_Birth__c = String.valueOf(Date.newInstance(1985, 12, 9));
          b.Account__c = null;
          update b ;
          
          List<Buyer__c> buyerList =  new List<Buyer__c>();
          buyerList.add(b);
         
          obj.createInquiry(buyerList);  //later Some Date forate Issue Only 
         
        Test.stopTest();
    }  
    
    
}