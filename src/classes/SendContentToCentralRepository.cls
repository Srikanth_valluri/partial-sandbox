/**
 * @File Name          : SendContentToCentralRepository.cls
 * @Description        : Send Content To CentralRepository to get the Public URL
 * @Author             : Dipika Rajput
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 6/27/2019, 6:43:25 PM
 * @Modification Log   : 
 *==============================================================================================
 * Ver         Date                     Author                    Modification
 * 
 *==============================================================================================
 * 1.0    6/27/2019                   Dipika Rajput           Initial Version
**/
public without sharing class SendContentToCentralRepository { 
   public static list<DP_Invoices__c> lstSoaDPInvoices = new list<DP_Invoices__c>();
   public static map <Id,list<string>> mapAttachURLwithDPInvoice;
    
  //@future(Callout=true)
   public static void getPublicUrlsOfSOAForDpInvoice( map<DP_Invoices__c,List<Blob>> dpnvoiceFileBlobsMap ) {
      List<UploadMultipleDocController.MultipleDocRequest> lstMultipleDocRequest;
      system.debug(' dpnvoiceFileBlobsMap' + dpnvoiceFileBlobsMap );
      lstMultipleDocRequest = new  List<UploadMultipleDocController.MultipleDocRequest>();
      integer intIncrementor = 0;
      
      for(DP_Invoices__c objDPInvoice : dpnvoiceFileBlobsMap.keyset()){
         for(blob objBlob : dpnvoiceFileBlobsMap.get(objDPInvoice)){
            system.debug(' Map keyset objDPInvoice' + objDPInvoice );
            system.debug(' list values : '+ objBlob);
            UploadMultipleDocController.MultipleDocRequest objDocRequest = 
            new UploadMultipleDocController.MultipleDocRequest();
            objDocRequest.category =  'Document';
            objDocRequest.entityName = 'Damac Service Requests'; 
            objDocRequest.fileDescription  =  'SampleAttachment';
            intIncrementor++;
            String stType = extractType('samplename.pdf');
            String coverLetterAttachmentName = extractName('samplename.pdf');
            coverLetterAttachmentName = coverLetterAttachmentName.replaceAll(' ','_');
            coverLetterAttachmentName = coverLetterAttachmentName.replaceAll('/','_');
            system.debug('coverLetterAttachmentName************'+coverLetterAttachmentName);                                
            
            objDocRequest.fileId = objDPInvoice.id+
                                    '-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+
                                    '.'+stType;
            objDocRequest.fileName = objDPInvoice.Id+
                                       '-'+String.valueOf(System.currentTimeMillis()+intIncrementor)+
                                       '.'+stType;
            
            System.debug('---System.currentTimeMillis()--'+System.currentTimeMillis());
            System.debug('---intIncrementor--'+intIncrementor);
            System.debug('---objDocRequest.fileName--'+objDocRequest.fileName);
            
            if(objDPInvoice.BookingUnits__c != NULL && 
               objDPInvoice.BookingUnits__r.Registration_Id__c != NULL){
                  objDocRequest.registrationId =  objDPInvoice.BookingUnits__r.Registration_Id__c;
                  objDocRequest.sourceFileName =  'IPMS-'+objDPInvoice.BookingUnits__r.Registration_Id__c+
                                                   '-'+coverLetterAttachmentName+'.'+stType;
                        objDocRequest.sourceId  =  'IPMS-'+objDPInvoice.BookingUnits__r.Registration_Id__c+
                                                   '-'+coverLetterAttachmentName;
            } 
            objDocRequest.base64Binary =  EncodingUtil.base64Encode(objBlob);
            lstMultipleDocRequest.add(objDocRequest);
         }
      
      
      
         system.debug('lstMultipleDocRequest : ' +lstMultipleDocRequest );
         if( lstMultipleDocRequest.Size() > 0 ) {
            UploadMultipleDocController.data MultipleDocData = new UploadMultipleDocController.data();
            if( !Test.isRunningTest() ) { 
               MultipleDocData = UploadMultipleDocController.getMultipleDocUrl(lstMultipleDocRequest);
            }
            else {
               List<UploadMultipleDocController.MultipleDocResponse> data = 
                     new List<UploadMultipleDocController.MultipleDocResponse>();
               UploadMultipleDocController.MultipleDocResponse objMultipleDocResponse = 
                     new UploadMultipleDocController.MultipleDocResponse();
               objMultipleDocResponse.PROC_STATUS = 'S';
               objMultipleDocResponse.url = 'www.google.com';
               data.add(objMultipleDocResponse);
               MultipleDocData.Data = data;
            }
            System.debug('--MultipleDocData--'+MultipleDocData);
            
            if( MultipleDocData != null && MultipleDocData.status == 'Exception' ) {
               system.debug('exception====MultipleDocData.message'+MultipleDocData.message);
                     errorLogger(MultipleDocData.message,'',objDPInvoice.Id);
            }                                   
            if(MultipleDocData != null && (MultipleDocData.Data == null || MultipleDocData.Data.Size() == 0) ) {
               system.debug('====MultipleDocData.message'+MultipleDocData.message);
                     errorLogger(MultipleDocData.message,'',objDPInvoice.Id);
            }
            mapAttachURLwithDPInvoice = new map <Id,list<string>>();
            if( MultipleDocData != null && objDPInvoice.Id != NULL) {
                  list<UploadMultipleDocController.MultipleDocResponse> lstRes = new list<UploadMultipleDocController.MultipleDocResponse>();
                  lstRes = MultipleDocData.Data;
                  system.debug('====lstRes'+lstRes);
                  for(UploadMultipleDocController.MultipleDocResponse objData: lstRes){
                     if( objData.PROC_STATUS == 'S' && String.isNotBlank(objData.url) ) {
                        system.debug('objData.url===='+objData.url);
                        if(!mapAttachURLwithDPInvoice.containsKey(objDPInvoice.Id)){
                              mapAttachURLwithDPInvoice.put(objDPInvoice.Id, new list<String>{objData.url});
                              system.debug('in if===='+mapAttachURLwithDPInvoice);
                        }else{
                              mapAttachURLwithDPInvoice.get(objDPInvoice.Id).add(objData.url);
                              system.debug('in else===='+mapAttachURLwithDPInvoice);
                        }
                     }
                  }
                  system.debug('mapAttachURLwithDPInvoice==: ' +mapAttachURLwithDPInvoice);
            }
         }
      }
   }

   @TestVisible
    private static String extractName(String strName) {
        return strName.substring(strName.lastIndexOf('\\') + 1);
    }
    @TestVisible
    private static String extractType( String strName ) {
        strName = strName.substring( strName.lastIndexOf('\\')+1 );
        return strName.substring( strName.lastIndexOf('.')+1 ) ;
    }   
    private static void errorLogger(string strErrorMessage, string strCaseID,string strDPInvoiceID){
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        if(String.isNotBlank(strDPInvoiceID)){
            objError.DP_Invoices__c = strDPInvoiceID;
        }
        insert objError;
    } 
}