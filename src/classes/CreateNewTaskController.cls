/****************************************************************************************************
* Name          : CreateNewTaskController                                                           *
* Description   : Create Task Related to Inquiries                                                  *
* Created Date  : 26/06/2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VERSION     AUTHOR        DATE            COMMENTS                                                *
* 1.0         Twinkle P     26/06/2018      Initial Draft.                                         *
****************************************************************************************************/

public with sharing class CreateNewTaskController {
	public Task objTask				{get; set;}
    public CreateNewTaskController(ApexPages.StandardController stdController) {
		objTask = new Task();
		objTask.OwnerId = UserInfo.getuserid();
		objTask.whatid = ApexPages.currentPage().getParameters().get('inqId');

    }
	/**
	 * Method to Save the Task and redirect to new Page
	 */
	public PageReference saveTask() {
        objTask.Priority = 'Normal';
		insert objTask;
		return cancelRecord();
	}
    
	/**
	 * Method to redirect to the Previous Page 
	 */
	public PageReference cancelRecord() {
		String recordId = ApexPages.currentPage().getParameters().get('inqId');
		PageReference inquiriesPage = new PageReference('/apex/ActivityTimeline?id=' +recordId+ '&view=global');
		inquiriesPage.setRedirect(true);
		return inquiriesPage;
	}
}