@isTest
private class CaseTriggerHandlerHOTest{
    static list<RuleEngineDocuments__c> lstREDocs;
    static Account objA, objA1, objA2;
    static list<Booking__c> lstBookings;
    static list<Booking_Unit__c> lstBU;
    static Id recTypeId;
    static list<Buyer__c> lstBuy;
    static NSIBPM__Service_Request__c objSR;
    
    static void init(){
        lstREDocs = TestDataFactory_CRM.createRuleDocs();
        insert lstREDocs;
        objA = TestDataFactory_CRM.createPersonAccount();
        objA.Nationality__pc = 'Afghanistan';
        objA.Country__pc = 'Afghanistan';
        objA.PersonEmail = 'vivian@test.com';
        objA.Email__c = 'vivi@test.com';
        insert objA;
        //objA1 = TestDataFactory_CRM.createPersonAccount();
        //insert objA1;
        objA2 = TestDataFactory_CRM.createBusinessAccount();
        objA2.Email__c = 'vivi@test.com';
        insert objA2;
        objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        lstBookings = TestDataFactory_CRM.createBookingForAccount(objA.Id, objSR.Id, 2);
        insert lstBookings;
        lstBU = TestDataFactory_CRM.createBookingUnits(lstBookings,2);
        for(Booking_Unit__c objB : lstBU){
            objB.Unit_Name__c = 'DFA/12/1204';
            objB.Finance_Flag__c = 'Y';
        }
        insert lstBU;
        
        Location__c objL = new Location__c();
        objL.Name = 'DFA';
        objL.Location_ID__c = '12081989';
        objL.Document_01__c = 'A';
        objL.Document_02__c = 'A';
        objL.Document_03__c = 'A';
        objL.Document_04__c = 'A';
        insert objL;
        
        lstBuy = TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 1, objA.Id);
        lstBuy[0].Primary_Buyer__c = true;
        lstBuy.addAll(TestDataFactory_CRM.createBuyer(lstBookings[0].Id, 1, objA2.Id));
        insert lstBuy;
        recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Handover').getRecordTypeId(); 
    }
    
    private static testMethod void parentCase1(){
        init();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockHO());
        Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAOPT(1));
        
        Case objC = TestDataFactory_CRM.createCase(objA.Id, recTypeId);
        objC.Booking_Unit__c = lstBU[0].Id;
        insert objC;
        
        list<Case> lstCase = new list<Case>();
        Case objC1 = new Case();
        objC1.AccountId = objA.Id;
        objC1.Booking_Unit__c = lstBU[0].Id;
        objC1.ParentId = objC.Id;
        objC1.RecordTypeId = recTypeId;
        lstCase.add(objC1);
        
        Case objC2 = new Case();
        objC2.AccountId = objA.Id;
        objC2.Booking_Unit__c = lstBU[1].Id;
        objC2.ParentId = objC.Id;
        objC2.RecordTypeId = recTypeId;
        lstCase.add(objC2);
        insert lstCase;
        
        lstCase[0].PRC_Document__c = true;
        update lstCase;
        Test.stopTest();
    }
}