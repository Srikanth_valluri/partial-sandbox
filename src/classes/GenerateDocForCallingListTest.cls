/*
* Description - Test class developed for GenerateDocForCallingList
*
* Version            Date            Author            Description
* 1.0              26/11/2017        Arjun Khatri      Initial Draft
*/
@isTest (SeeAllData = true)
private class GenerateDocForCallingListTest  {
    static Id collectioncallingRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('Collections Calling List').RecordTypeId;
    static testMethod void testRest1() {
        

        GenerateDPSoaRest.responseMsg = 'Test Url';
        List<Calling_List__c> lstCallingLists = [ SELECT
                                                    Id
                                                    , Booking_Unit__c
                                                    , Account__c
                                                    , Registration_ID__c
                                                    , Booking_Unit__r.Registration_Status__c
                                                    , Booking_Unit__r.Unit_Active__c
                                                    , Booking_Unit__r.Registration_ID__c 
                                                FROM 
                                                    Calling_List__c 
                                                WHERE
                                                    Booking_Unit__c != null
                                                AND
                                                    Registration_ID__c != null
                                                AND
                                                    ( NOT Booking_Unit__r.Registration_Status__c like '%Rejected%' )
                                                AND
                                                    Booking_Unit__r.Unit_Active__c = 'Active'
                                                LIMIT 1];
        
        if( lstCallingLists != null && !lstCallingLists.isEmpty() ) {
            Test.startTest();
                ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
                GenerateDocForCallingList objController = new GenerateDocForCallingList(sc);
            
                Test.setMock(HttpCalloutMock.class, new DPSoaHttpMock(1));
                objController.generateStatAccount();
                GenerateDPSoaRest.responseMsg = 'Error :';
                objController.generateSOAUsingRest();
                GenerateDPSoaRest.responseMsg = '';
                objController.generateSOAUsingRest();

            Test.stopTest();
        }
    }
    
    static testMethod void testSoap1() {
     
       List<Calling_List__c> lstCallingLists = [ SELECT
                                                                    Id
                                                                    , Booking_Unit__c
                                                                    , Account__c
                                                                    , Registration_ID__c
                                                                    , Booking_Unit__r.Registration_Status__c
                                                                    , Booking_Unit__r.Unit_Active__c
                                                                    , Booking_Unit__r.Registration_ID__c 
                                                                FROM 
                                                                    Calling_List__c 
                                                                WHERE
                                                                    Booking_Unit__c != null
                                                                AND
                                                                    Registration_ID__c != null
                                                                AND
                                                                    ( NOT Booking_Unit__r.Registration_Status__c like '%Rejected%' )
                                                                AND
                                                                    Booking_Unit__r.Unit_Active__c = 'Active'
                                                                LIMIT 1];

        if( lstCallingLists != null && !lstCallingLists.isEmpty() ) {
            Test.startTest();
                ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
                GenerateDocForCallingList objController = new GenerateDocForCallingList(sc);
                SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateSOAService.GenCustomerStatementResponse_element>();
                GenerateSOAService.GenCustomerStatementResponse_element response = new GenerateSOAService.GenCustomerStatementResponse_element();
                response.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338397 and Request Id :40187735 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"92061","REQUEST_ID":"40187735","STAGE_ID":"338397","URL":"https://sftest.deeprootsurface.com/docs/e/40187735_92061_SOA.pdf"}';
                SOAPCalloutServiceMock.returnToMe.put('response_x', response);
                Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
                
                objController.generateSOAUsingSoap();
                
                response.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338397 and Request Id :40187735 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"92061","REQUEST_ID":"40187735","STAGE_ID":"338397","URL":"google"}';
                SOAPCalloutServiceMock.returnToMe.put('response_x', response);
                Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
                //Test.setMock(HttpCalloutMock.class, new DPSoaHttpMock(1));
                objController.generateSOAUsingSoap();
            Test.stopTest();
        }
    }
    
    static testMethod void testSoap2() {
        List<Calling_List__c> lstCallingLists = [ SELECT
                        Id
                        , Booking_Unit__c
                        , Account__c
                        , Registration_ID__c
                        , Booking_Unit__r.Registration_Status__c
                        , Booking_Unit__r.Unit_Active__c
                        , Booking_Unit__r.Registration_ID__c 
                    FROM 
                        Calling_List__c 
                    WHERE
                        Booking_Unit__c != null
                    AND
                        Registration_ID__c != null
                    AND
                        ( NOT Booking_Unit__r.Registration_Status__c like '%Rejected%' )
                    AND
                        Booking_Unit__r.Unit_Active__c = 'Active'
                    LIMIT 1];

        if( lstCallingLists != null && !lstCallingLists.isEmpty() ) {
            Test.startTest();
                ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
                GenerateDocForCallingList objController = new GenerateDocForCallingList(sc);
                SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateSOAService.GenCustomerStatementResponse_element>();
                GenerateSOAService.GenCustomerStatementResponse_element response = new GenerateSOAService.GenCustomerStatementResponse_element();
                response.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338397 and Request Id :40187735 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"92061","REQUEST_ID":"40187735","STAGE_ID":"338397","URL":"https://sftest.deeprootsurface.com/docs/e/40187735_92061_SOA.pdf"}';
                SOAPCalloutServiceMock.returnToMe.put('response_x', response);
                Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
                objController.generateSOAUsingSoap();
                
                response.return_x = '{"Status":"E","PROC_STATUS":"null","PROC_MESSAGE":"null,"URL":"null"}';
                SOAPCalloutServiceMock.returnToMe.put('response_x', response);
                Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
                objController.generateSOAUsingSoap();
            Test.stopTest();
        }
    }
    
    static testMethod void testRejectedUnit() {
        
        GenerateDPSoaRest.responseMsg = 'Error :';
        List<Calling_List__c> lstCallingLists = [ SELECT
                                                    Id
                                                    , Booking_Unit__c
                                                    , Account__c
                                                    , Registration_ID__c
                                                    , Booking_Unit__r.Registration_Status__c
                                                    , Booking_Unit__r.Unit_Active__c
                                                    , Booking_Unit__r.Registration_ID__c 
                                                FROM 
                                                    Calling_List__c 
                                                WHERE
                                                    Booking_Unit__c != null
                                                AND
                                                    Registration_ID__c != null
                                                AND
                                                     Booking_Unit__r.Registration_Status__c like '%Rejected%'
                                                LIMIT 1];
        if( lstCallingLists != null && !lstCallingLists.isEmpty() ) {
            Test.startTest();
                ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
                GenerateDocForCallingList objController = new GenerateDocForCallingList(sc);
                Test.setMock(HttpCalloutMock.class, new DPSoaHttpMock(2));
                objController.generatestataccount();
                GenerateDPSoaRest.responseMsg = '';
            Test.stopTest();
        }
    }
    
    static testMethod void testNullRegId() {
        
        GenerateDPSoaRest.responseMsg = 'Error :';
        List<Calling_List__c> lstCallingLists = [ SELECT
                                                    Id
                                                    , Booking_Unit__c
                                                    , Account__c
                                                    , Registration_ID__c
                                                    , Booking_Unit__r.Registration_Status__c
                                                    , Booking_Unit__r.Unit_Active__c
                                                    , Booking_Unit__r.Registration_ID__c 
                                                FROM 
                                                    Calling_List__c 
                                                WHERE
                                                    Booking_Unit__c != null
                                                AND
                                                    Registration_ID__c = null
                                                AND 
                                                    Booking_Unit__r.Registration_ID__c = null
                                                LIMIT 1];
        if( lstCallingLists != null && !lstCallingLists.isEmpty() ) {
            Test.startTest();
                ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
                GenerateDocForCallingList objController = new GenerateDocForCallingList(sc);
                Test.setMock(HttpCalloutMock.class, new DPSoaHttpMock(2));
                objController.generatestataccount();
                GenerateDPSoaRest.responseMsg = '';
            Test.stopTest();
        }
    }

    static testMethod void testInactiveUnit() {
        
        GenerateDPSoaRest.responseMsg = 'Error :';
        List<Calling_List__c> lstCallingLists = [ SELECT
                                                    Id
                                                    , Booking_Unit__c
                                                    , Account__c
                                                    , Registration_ID__c
                                                    , Booking_Unit__r.Registration_Status__c
                                                    , Booking_Unit__r.Unit_Active__c
                                                    , Booking_Unit__r.Registration_ID__c 
                                                FROM 
                                                    Calling_List__c 
                                                WHERE
                                                    Booking_Unit__c != null
                                                AND
                                                    Registration_ID__c != null
                                                AND
                                                     Booking_Unit__r.Unit_Active__c = 'Inactive'
                                                LIMIT 1];
        if( lstCallingLists != null && !lstCallingLists.isEmpty() ) {
            Test.startTest();
                ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
                GenerateDocForCallingList objController = new GenerateDocForCallingList(sc);
                Test.setMock(HttpCalloutMock.class, new DPSoaHttpMock(2));
                objController.generatestataccount();
                GenerateDPSoaRest.responseMsg = '';
            Test.stopTest();
        }
    }
    
    static testMethod void test_generatestatofpenalty() {
    
        List<Calling_List__c> lstCallingLists = [ SELECT
                                                    Id
                                                    , Booking_Unit__c
                                                    , Account__c
                                                    , Registration_ID__c
                                                    , Booking_Unit__r.Registration_Status__c
                                                    , Booking_Unit__r.Unit_Active__c
                                                    , Booking_Unit__r.Registration_ID__c 
                                                FROM 
                                                    Calling_List__c 
                                                WHERE
                                                    Booking_Unit__c != null
                                                AND
                                                    Registration_ID__c != null
                                                AND
                                                     Booking_Unit__r.Unit_Active__c = 'Inactive'
                                                LIMIT 1];
        
        if( lstCallingLists != null && !lstCallingLists.isEmpty() ) {
            test.startTest();
            ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
            GenerateDocForCallingList objController = new GenerateDocForCallingList(sc);
            SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateSOPService.GeneratePenaltyStatmentResponse_element>();
            GenerateSOPService.GeneratePenaltyStatmentResponse_element response1 = new GenerateSOPService.GeneratePenaltyStatmentResponse_element();
            response1.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338396 and Request Id :40185767 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"78152","REQUEST_ID":"40185767","STAGE_ID":"338396","URL":"https://sftest.deeprootsurface.com/docs/e/40185767_78152_PENALTY.pdf"}';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            objController.generatestatofpenalty();

            response1.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338396 and Request Id :40185767 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"78152","REQUEST_ID":"40185767","STAGE_ID":"338396","URL":""}';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            
            objController.generatestatofpenalty();
            test.stopTest();
        }
    }

   static testMethod void test_generatestatofpenalty1() {
    
        List<Calling_List__c> lstCallingLists = [ SELECT
                        Id
                        , Booking_Unit__c
                        , Account__c
                        , Registration_ID__c
                        , Booking_Unit__r.Registration_Status__c
                        , Booking_Unit__r.Unit_Active__c
                        , Booking_Unit__r.Registration_ID__c 
                    FROM 
                        Calling_List__c 
                    WHERE
                        Booking_Unit__c != null
                    AND
                        Registration_ID__c != null
                    AND
                        Booking_Unit__r.Unit_Active__c = 'Inactive'
                    LIMIT 1];
        
        if( lstCallingLists != null && !lstCallingLists.isEmpty() ) {
            test.startTest();
            ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
            GenerateDocForCallingList objController = new GenerateDocForCallingList(sc);
            SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateSOPService.GeneratePenaltyStatmentResponse_element>();
            GenerateSOPService.GeneratePenaltyStatmentResponse_element response1 = new GenerateSOPService.GeneratePenaltyStatmentResponse_element();
            response1.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338396 and Request Id :40185767 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"78152","REQUEST_ID":"40185767","STAGE_ID":"338396","URL":"https://sftest.deeprootsurface.com/docs/e/40185767_78152_PENALTY.pdf"}';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            objController.generatestatofpenalty();

            response1.return_x = '{"Status":"E","PROC_STATUS":"null","PROC_MESSAGE":"null","URL":"null"}';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            
            objController.generatestatofpenalty();
            test.stopTest();
        }
    }

    static testMethod void test_generatestatofpenalty2() {
        List<Calling_List__c> lstCallingLists = [ SELECT
                                                Id
                                                , Booking_Unit__c
                                                , Account__c
                                                , Registration_ID__c
                                                , Booking_Unit__r.Registration_Status__c
                                                , Booking_Unit__r.Unit_Active__c
                                                , Booking_Unit__r.Registration_ID__c 
                                            FROM 
                                                Calling_List__c 
                                            WHERE
                                                Booking_Unit__c != null
                                            AND
                                                Registration_ID__c != null
                                            AND
                                                Booking_Unit__r.Unit_Active__c = 'Inactive'
                                            LIMIT 1];
        if( lstCallingLists != null && !lstCallingLists.isEmpty() ) {
            test.startTest();
            ApexPages.StandardController sc = new ApexPages.standardController( lstCallingLists[0] );
            GenerateDocForCallingList objController = new GenerateDocForCallingList(sc);
            SOAPCalloutServiceMock.returnToMe = new Map<String, GenerateSOPService.GeneratePenaltyStatmentResponse_element>();
            GenerateSOPService.GeneratePenaltyStatmentResponse_element response1 = new GenerateSOPService.GeneratePenaltyStatmentResponse_element();
            response1.return_x = '{"Status":"S","PROC_STATUS":"S","PROC_MESSAGE":"[SUCCESS] Process with Stage Id=338396 and Request Id :40185767 ...","Message":"Process Completed Returning 1 Response Message(s)...","PARAM_ID":"78152","REQUEST_ID":"40185767","STAGE_ID":"338396","URL":"https://sftest.deeprootsurface.com/docs/e/40185767_78152_PENALTY.pdf"}';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            objController.generatestatofpenalty();

            response1.return_x = 'null';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            
            objController.generatestatofpenalty();
            test.stopTest();
        }
    }
    /*
     @ Description : To create calling list reocord
     @ Return      : list of calling list to be created
    */
    public static List<Calling_List__c> createCallingList( Id RecordTypeIdCollection, Integer counter , List<Booking_Unit__c> lstBookingUnits ) {
        List<Calling_List__c> lstCallingLists = new List<Calling_List__c>();
        for( Integer i=0; i<counter; i++ ) {
            lstCallingLists.add(new Calling_List__c( Registration_ID__c = lstBookingUnits[i].Registration_ID__c , Inv_Due__c = 0, DM_Due_Amount__c = 0 , RecordTypeId = RecordTypeIdCollection ) );
        }
        return lstCallingLists;
    }
}