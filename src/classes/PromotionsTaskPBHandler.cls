public without sharing class PromotionsTaskPBHandler 
{
    @InvocableMethod
    public static void createTasksForPromotions(List<Task> lstSR)
    {
        system.debug('createTasksForPromotions for method called PromotionsTaskPBHandler');

        if(lstSR != null && lstSR.size() > 0)
        {
          Task objTask = lstSR[0];
          system.debug('objTask '+objTask );
          Case objCase = new Case();
          objCase = [ Select 
                      Id
                      ,OwnerId
                      ,Booking_unit__c,Total_Amount__c
                      ,Submit_for_Approval__c 
                      FROM
                      Case
                      Where Id =: objTask.WhatId
                    ];
          if(objTask.Subject == 'Upload the Signed Promotions Offer Letter')
          {
            system.debug('objCase - Upload the Signed Promotions Offer Letter');
            postPromotionsDetails(objCase.Id);
          }
        }
        else
        {
          system.debug('lstSR is empty');
        }
    }

    //post Promotions amount to IPMS
    @future(callout=true)
    public static void postPromotionsDetails(String strCaseId)
    { 
      system.debug('strCaseId postPromotionsDetails'+strCaseId);
      try
      {
        if(String.isNotBlank(strCaseId))
        { 
          Case objCase = [ SELECT 
                             Id
                            ,Booking_Unit__r.Registration_ID__c
                            ,Total_Amount__c 
                            ,CaseNumber 
                      FROM Case WHERE Id =: strCaseId
                    ];
          system.debug('Registration_ID__c '+objCase.Booking_Unit__r.Registration_ID__c);
          system.debug('objCase.Total_Amount__c '+objCase.Total_Amount__c);

          processXxdcProcessServiceWsPlsqlPromotio.APPSXXDC_PROCESS_SERX1794747X1X5[] objRegTermsList = new 
                                           processXxdcProcessServiceWsPlsqlPromotio.APPSXXDC_PROCESS_SERX1794747X1X5[1];
          processXxdcProcessServiceWsPlsqlPromotio.APPSXXDC_PROCESS_SERX1794747X1X5 objRegTerm = new 
                                           processXxdcProcessServiceWsPlsqlPromotio.APPSXXDC_PROCESS_SERX1794747X1X5();

          objRegTerm.PARAM_ID = (objCase.Booking_Unit__r.Registration_ID__c);
          objRegTerm.ATTRIBUTE1 = objCase.Total_Amount__c != null ? String.valueOf(objCase.Total_Amount__c) : '';
          objRegTerm.ATTRIBUTE2 = 'Promotion Recharges';
          objRegTerm.ATTRIBUTE3 = '3rd Party Re-charges';
          objRegTerm.ATTRIBUTE4 = 'PromotionRecharges'+objCase.Booking_Unit__r.Registration_ID__c;
          Datetime objDateTime = System.now();
          objRegTerm.ATTRIBUTE5 = String.valueOf(objDateTime.format('dd-MMM-yyyy').toUpperCase());
          objRegTermsList[0] = objRegTerm;

          PromotionsAmountPostingWSDL.ProcessHttpSoap11Endpoint objEndpoint = new PromotionsAmountPostingWSDL.ProcessHttpSoap11Endpoint();
          objEndpoint.timeout_x = 120000;
          String response = objEndpoint.CREATE_DM_CM( '2-'+objCase.CaseNumber
                                                                      ,'CREATE_DM_CM'
                                                                      ,'SFDC'
                                                                      , objRegTermsList
                                                                    );
          system.debug('response***********'+response);
          Map<String, Object> mapResponse = (Map<String, Object>)JSON.deserializeUntyped(response);
          if(mapResponse.get('message')!= null && mapResponse.get('status') != null)
          {
              system.debug('status '+(String)mapResponse.get('status'));
              if( String.valueOf(mapResponse.get('status')).equalsIgnoreCase('S'))
              {
                  errorLoggerNew('Success :Promotion Amount Posting To SOA '+response, objCase.Id, '');
                  //update status of Case to Closed if Amount is Posted successfully to IPMS in SOA
                  objCase.Status = 'Closed';
                  update objCase;
              }
              else
              {
                  errorLoggerNew('Error :Promotion Amount Posting To SOA '+response, objCase.Id, '');
              }
          }
        }
      }
      catch(Exception exp)
      {
          errorLoggerNew('Error :Promotion SOA Posting :'+exp.getStackTraceString() +' - '+exp.getMessage(), strCaseId, '');
      }
    }


    public static void errorLoggerNew(string strErrorMessage, string strCaseID,string strBookingUnitID)
    {
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        objError.Process_Name__c = 'Promotions';
        if(String.isNotBlank(strCaseID) && strCaseID.startsWith('500')){
            objError.Case__c = strCaseID;
        }
        if(String.isNotBlank(strBookingUnitID)){
            objError.Booking_unit__c = strBookingUnitID;
        }
        insert objError;
    }
}