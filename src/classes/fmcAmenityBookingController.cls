/********************************************************************************************************************************
 * Description : Controller of fmcAmenityBookingComponent
 *===============================================================================================================================
 * Ver      Date-DD/MM/YYYY     Author              Modification
 *===============================================================================================================================
 * 2.0      29/09/2020          Komal Shitole       Added null check for resource
*********************************************************************************************************************************/
public without sharing class fmcAmenityBookingController extends FMAmenityBookingController{

    public  List<Booking_Unit__c>   objUnitList            {get; set;}
    public  map<Id,Booking_Unit__c> mapUnitsOwned          {get; set;} 
    public  map<Id,Booking_Unit__c> mapMyUnits             {get; set;}
    public  List<Id>                objUnitOwner           {get; set;}
    public  List<Id>                objUnitRes             {get; set;}
    public  List<SelectOption>      lstUnits               {get; set;}
    public  String                  termsLink              {get; set;}
    public  Booking_Unit__c         objUnit;
    public  Boolean                 shwDueMsg              {get; set;}

    public fmcAmenityBookingController() {
        super(false);
        shwDueMsg = false;
        PageReference currentPage = ApexPages.currentPage();
        if (FmcUtils.isCurrentView('BookAmenity')
            || (currentPage != NULL && currentPage.getUrl().containsIgnoreCase('BookPublicAmenity'))
        ) {
            super();
            strAccountId = CustomerCommunityUtils.customerAccountId;
            strFMCaseId = ApexPages.currentPage().getParameters().get('Id');
            objFMCase = new FM_Case__c();
            strSRType = 'Amenity_Booking';
            objFMCase.Request_Type_DeveloperName__c = 'Amenity_Booking' ;
            objFMCase.Request_Type__c = 'Amenity Booking';
            objFMCase.Origin__c = 'Portal';
            objFMCase.Mobile_Country_Code__c = 'United Arab Emirates: 00971';
            objUnit = new Booking_Unit__c();
            System.debug('>>strSRType : '+strSRType);
            system.debug('== strAccountId =='+strAccountId );
            if (!isGuestBooking) {
                fetchUnits();
            }
            objResource = new Resource__c();
            termsLink = '';
        }
    }

    protected override Boolean isBookingPublicAmenity() {
        return 'Guest'.equalsIgnoreCase(UserInfo.getUserType());
    }

     public void fetchUnits() {
        Set<String> activeStatusSet= Booking_Unit_Active_Status__c.getall().keyset();
        mapUnitsOwned = new map<Id,Booking_Unit__c>();
        objUnitOwner = new List<Id>();
        objUnitRes = new List<Id>();
        mapMyUnits = new map<Id,Booking_Unit__c>();
        lstUnits = new list<SelectOption>();
        lstUnits.add(new selectOption('None','--None--'));
        objUnitList = [SELECT Id
                           , Owner__c
                           , Booking__r.Account__c
                           , Booking__r.Account__r.Name
                           , Owner__r.IsPersonAccount
                           , Tenant__r.IsPersonAccount
                           , Owner__r.Name
                           , Tenant__c
                           , Tenant__r.Name
                           , Resident__c
                           , Resident__r.Name
                           , Unit_Name__c
                           , Registration_Id__c
                           , Property_Name__c
                           , Property_City__c
                       FROM Booking_Unit__c
                       WHERE (    Resident__c           = :strAccountId
                               OR Booking__r.Account__c = :strAccountId
                               OR Tenant__c             = :strAccountId )
                       AND   (    Dummy_Booking_Unit__c     = true
                               OR ((    Handover_Flag__c    = 'Y'
                                     OR Early_Handover__c   = true 
                                     OR Lease_Handover__c   = true)
                                    AND Registration_Status__c IN :activeStatusSet)) ];

        System.debug('objUnitList---- : ' +objUnitList);
        if(!objUnitList.isEmpty() && objUnitList != NULL) {
            for( Booking_Unit__c objBU : objUnitList ) {
                if( objBU.Owner__c == strAccountId ) {
                    objUnitOwner.add(objBU.Id);
                    mapUnitsOwned.put(objBU.Id,objBU);
                }

                if( ( objBU.Tenant__c == strAccountId || objBU.Resident__c == strAccountId )
                            && objBU.Owner__c != strAccountId ) {
                    objUnitRes.add(objBU.Id);
                    mapMyUnits.put(objBU.Id,objBU);
                }
                lstUnits.add(new selectOption(objBU.Id,objBU.Unit_Name__c));
                System.debug('lstUnits--- : '+lstUnits);
            }
        }else{
            ApexPages.addmessage(new ApexPages.message(
                    ApexPages.severity.Error,'No Booking Units available'));
        }
        system.debug('==lstUnits=='+lstUnits);
    }

    public void init() {
        System.debug('strUnitId------>'+strUnitId);
        if(String.isNotBlank(strUnitId)) {
            List<Booking_Unit__c> lstBU = new List<Booking_Unit__c>();
            lstBU = [SELECT Id,
                            Booking__r.Account__c,
                            Unit_Name__c
                    FROM Booking_Unit__c
                    WHERE ID =: strUnitId];
            System.debug('lstBU--------------->'+lstBU);

            if(lstBU.size() > 0) {
                objBU = lstBU[0];
                System.debug('objBU--------------->'+objBU);
                objFMCase.Booking_Unit__c = lstBU[0].Id;
                System.debug('objFMCase.Booking_Unit__c ------------->'+objFMCase.Booking_Unit__c );

                System.debug('lstBU[0].Unit_Name__c----------->'+lstBU[0].Unit_Name__c);
                if(String.isNotBlank(lstBU[0].Unit_Name__c)) {
                    String buildingName = lstBU[0].Unit_Name__c.split('/')[0];
                    List<Location__c> lstBuildings = new List<Location__c>();
                    lstBuildings = [SELECT Id
                                           , Name
                                           , Property_Name__c
                                           , (SELECT FM_User__c
                                                     , FM_User__r.Email
                                                     , FM_Role__c
                                                     , FM_User__r.Name
                                               FROM FM_Users__r
                                               WHERE FM_Role__c = 'FM Admin'
                                               OR FM_Role__c = 'Master Community Admin'
                                               LIMIT 1)
                                    FROM Location__c
                                    WHERE Name =: buildingName
                                    AND RecordType.DeveloperName = 'Building'
                                    LIMIT 1 ];
                    System.debug('>>>lstBuildings : '+lstBuildings);
                    if(lstBuildings.size() >0) {
                        if(ApexPages.currentPage().getUrl().containsIgnoreCase('BookPublicAmenity')) {
                            getAvailableResources(lstBuildings[0].Property_Name__c, lstBuildings[0].Id, true);
                        }else {
                            getAvailableResources(lstBuildings[0].Property_Name__c, lstBuildings[0].Id, false);
                        }
                        system.debug('>>>lstResources : '+lstResources);
                        if(lstBuildings[0].FM_Users__r.size() > 0) {
                            objFMCase.Admin__c = taskToCollectFee.OwnerId =
                                                        lstBuildings[0].FM_Users__r[0].FM_User__c;
                        }
                    }
                }
            }
        }
        if(String.isNotBlank(strAccountId)) {
            List<Account> lstAccount = new List<Account>();
            lstAccount = [ SELECT Id
                                , Email__pc
                                , Mobile_Phone_Encrypt__pc
                           FROM Account
                           WHERE Id =: strAccountId ];
            System.debug('lstAccount---------->'+lstAccount);
            System.debug('objBU----------->'+objBU);
            System.debug('objBU.Booking__r.Account__c--------->'+objBU.Booking__r.Account__c);
            if(lstAccount.size() >0 && String.isNotBlank(objBU.Booking__r.Account__c)) {
                isTenant = objBU.Booking__r.Account__c == lstAccount[0].Id ? false : true;
            }
            objFMCase.Initiated_by_tenant_owner__c = isTenant ? 'Tenant' : 'Owner';
            if(isTenant && lstAccount.size() >0) {
                objFMCase.Tenant__c = lstAccount[0].Id;
                objFMCase.Account__c = objBU.Booking__r.Account__c;
            }
            else if(lstAccount.size() >0) {
                objSelectedAcc = lstAccount[0];
                objFMCase.Account__c = objFMCase.Tenant__c = lstAccount[0].Id;
            }
            objFMCase.Tenant_Email__c = lstAccount[0].Email__pc;
            System.debug('=isTenant====' +isTenant);
        }
        objFMCase.Request_Type_DeveloperName__c = isTenant ? 'Amenity_Booking_T' : 'Amenity_Booking';
        System.debug('objFMCase.Request_Type_DeveloperName__c------->'
                                    +objFMCase.Request_Type_DeveloperName__c);
        System.debug('objResource========>'+objResource);
    }

    public pageReference getUnitDetailsNew() {
        system.debug('strUnitId : '+strUnitId);
        if (String.isBlank(strUnitId)) {
            return NULL;
        }

        list<Booking_Unit__c> objBU = [SELECT id,
                                              Registration_Id__c,
                                              Booking__r.Account__r.Party_ID__c,
                                              Property_Name__c,
                                              CM_Units__c,
                                              ByPassFMReminder__c,
                                              SC_From_RentalIncome__c,
                                              Allow_Amenity_Booking__c
                                       FROM Booking_Unit__c
                                       WHERE id =: strUnitId];
        System.debug('objBU : '+objBU);
        if(objBU.size() > 0) {
            FmIpmsRestServices.DueInvoicesResult dues = FmIpmsRestServices.getDueInvoices(objBU[0].Registration_Id__c, objBU[0].Booking__r.Account__r.Party_ID__c, objBU[0].Property_Name__c);
            System.debug('-->> Invoice dues.....'+ dues.dueAmountExceptCurrentQuarter);
            
            //Added the CM_Units, ByPassFMReminder__c, SC_From_RentalIncome__c check - Shubham 11/03/2020
            System.debug('due in label :'+label.ServiceChargeDueLimitInAmenityBooking);

            Decimal dueLimit = Decimal.valueOf(label.ServiceChargeDueLimitInAmenityBooking);
            System.debug('dueLimit: ' + dueLimit);
            if(dues.dueAmountExceptCurrentQuarter == null || dues.dueAmountExceptCurrentQuarter < dueLimit || (objBU[0].Allow_Amenity_Booking__c || objBU[0].ByPassFMReminder__c || objBU[0].SC_From_RentalIncome__c || objBU[0].CM_Units__c == 'CM' || objBU[0].CM_Units__c == 'BM') ) {
                init();
            }
            else {
                System.debug('-->> Invoice Zero AYA.....');
                shwDueMsg = true;
                return null;
            }
        }

        //init();
        return NULL;
    }

     public override PageReference bookAmenity() {
        try {
            objFMCase.RecordTypeId = devRecordTypeId;
            objFMCase.Origin__c='Portal';
            objFMCase.Amenity_Booking_Status__c = 'Booking Confirmed';
            objFMCase.Status__c = 'Closed';
            System.debug('=====strSelectedSlot===='+strSelectedSlot);
            System.debug('objResource========>'+objResource);
            List<String> lstSlots = strSelectedSlot.split('-');
            objFMCase.Booking_Start_Time_p__c = lstSlots[0].trim();
            objFMCase.Booking_End_Time_p__c = lstSlots[1].trim();
            if(lstSlots.size() == 3) {      //CDBS
                objFMCase.Resource_SlotID__c = lstSlots[2].trim();
            }
            if(lstSlots.size() == 4) {
                objFMCase.Resource_SlotID__c = lstSlots[2].trim()+'-'+lstSlots[3].trim();
            }

            objFMCase.Resource_Booking_Type__c = 'FM Amenity';
            objFMCase.Contact_Email__c = objResource.Contact_Person_Email__c;
            objFMCase.Contact_Mobile__c = objResource.Contact_Person_Phone__c;
            objFMCase.Contact_person__c = objResource.Contact_Person_Name__c;
            if(objResource.Deposit__c != NULL && objResource.Deposit__c != 0
                    || (objResource.Chargeable_Fee_Slot__c !=NULL
                        && objResource.Chargeable_Fee_Slot__c != 0)) {
                if(objResource.No_of_deposit_due_days__c != NULL) {
                    objFMCase.Amenity_Booking_Status__c = 'On Hold';
                    objFMCase.Status__c = 'Submitted';
                    objFMCase.Resource_Booking_Due_Date__c = Date.today().addDays(
                                    Integer.valueOf(objResource.No_of_deposit_due_days__c));
                }
            }
            if(objFMCase.Resource_Booking_Due_Date__c != NULL
                    && objFMCase.Resource_Booking_Due_Date__c > objFMCase.Booking_Date__c) {
                objFMCase.Resource_Booking_Due_Date__c = objFMCase.Booking_Date__c;
            }
            System.debug('objFMCase before inserting: ' + objFMCase);
            insert objFMCase;
            //Creating task for FM Admin if fee is involved
            if(objResource.Deposit__c !=NULL && objResource.Deposit__c != 0
                    ||( objResource.Chargeable_Fee_Slot__c !=NULL
                        && objResource.Chargeable_Fee_Slot__c != 0)) {
                taskToCollectFee.whatId = objFMCase.Id;
                taskToCollectFee.Subject = Label.FM_CollectFeeTaskLabel;
                taskToCollectFee.Status = 'Not Started';
                taskToCollectFee.Priority = 'Normal';
                taskToCollectFee.Process_Name__c = 'Amenity Booking';
                taskToCollectFee.ActivityDate = Date.today().addDays(2);
                taskToCollectFee.Assigned_User__c = 'FM Admin';
                insert taskToCollectFee;
            }
            //return new PageReference('/' + objFMCase.Id);
            PageReference objPage = new PageReference('/' + objFMCase.Id );
            if (objPage != NULL) {
                String unitId = objPage.getUrl().substringAfterLast('/');
                objPage = new PageReference(ApexPages.currentPage().getUrl());
                objPage.getParameters().clear();
                objPage.getParameters().put('id', unitId);
                objPage.getParameters().put('view', 'CaseDetails');
                objPage.setRedirect(true);
            }
            System.debug('objPage : '+objPage);
            return objPage ;
        }//try
        catch(System.Exception excp) {
            insert new Error_Log__c(Process_Name__c = 'Amenity Booking',
                                    Account__c = strAccountId,
                                    Booking_Unit__c = strUnitId,
                                    Error_Details__c = excp.getMessage());
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,
                                                excp.getMessage() + excp.getLineNumber()));
        }
        return NULL;
    }

    public PageReference getTermsDocumentLink() {
        //System.debug('>>>---mapResId_CVId--- : '+mapResId_CVId);
        //System.debug('>>>---objResource Id--- : '+objResource.id);
        if(objResource != Null && objResource.id != Null) {
             ID cvId = mapResId_CVId.get(objResource.id);
        }
       
        //System.debug('>>>---cvId--- : '+cvId);

        ContentDistribution objContentDistribution  = new ContentDistribution();

        if(cvId != NULL) {
            objContentDistribution = [ SELECT DistributionPublicUrl
                                              , ContentDocumentId
                                       FROM ContentDistribution
                                       WHERE ContentVersionId = :cvId ];
        }

        if(objContentDistribution != NULL ) {
            termsLink = objContentDistribution.DistributionPublicUrl;
        }

        System.debug('>>>---termsLink--- : '+termsLink);
        return NULL;
    }
}