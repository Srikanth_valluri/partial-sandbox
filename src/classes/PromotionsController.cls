public virtual without sharing class PromotionsController {

    // all object properties & variables
    public Case objCase {get;set;}
    public Booking_Unit__c objUnit {get;set;}
    public Account objAccount;
    public WrapperBookingUnit objWrapperBookingUnit {get;set;}
    public UnitDetailsService.BookinUnitDetailsWrapper objBUDetailWrapper {get;set;}

    // all list properties & variables
    public List<SelectOption> lstUnits {get;set;}
    public List<SelectOption> lstCategories {get;set;}
    public list<Case> lstExistingCase {get;set;}
    public List<Case> lstOpenSRs {get;set;}
    public List<Case> lstClosedSRs {get;set;}
    public list<Buyer__c> lstExistingJB {get;set;}
    public List<PromotionsWrapper> lstPromotions {get;set;}


    // all Map propeties & variables
    public map<String, List<String>> mapPayPlanTerms {get;set;}
    map<Id,Booking_Unit__c> mapId_BookingUnit;
    public map<String, list<Parking__c>> mapOffPlanParkingType;

    // all String properties & variables
    public String strSelectedUnit {get;set;}
    public String strCaseID { get;set; }
    public String strAccoundId { get;set; }
    public String strAccParamId;
    public String strSRType {get;set;}
    public String strSRtypeURL;
    public String strUnitDetailType {get;set;}
    public String strSOAURL {get;set;}
    public String strPRFURL {get;set;}
    public String dtDateOfExpiry { get;set; }
    public String strCaseNumber;
    public String strCaseStatusSubmitted { get;set; }

    // all Boolean properties and variables
    public Boolean blnUnitSelected {get;set;}
    public Boolean blnIsSinglePromotionSelected {get;set;}

    // all Integer & Decimal properties
    public Decimal totalPrice {get;set;}
    public Integer rowToFetchPrice {get;set;}
    public string strHelpPageId {get;set;}

    // Constructor
    public PromotionsController()
    {
        strCaseID = ApexPages.currentPage().getParameters().get('caseID');
        strAccParamId = ApexPages.currentPage().getParameters().get('AccountId');
        strSRtypeURL = ApexPages.currentPage().getParameters().get('SRType');

        system.debug('strCaseID '+strCaseID);
        system.debug('strAccParamId '+strAccParamId);
        system.debug('strSRtypeURL '+strSRtypeURL);

        totalPrice = 0;

        if(String.isNotBlank(strAccParamId) && String.isBlank(strCaseID) && (String.isNotBlank(strSRtypeURL) && strSRtypeURL.equalsIgnoreCase('Promotions')))
        {
            system.debug('first time creating SR');
            init();
        }

        if(String.isNotBlank(strAccParamId) && String.isNotBlank(strCaseID) && (String.isNotBlank(strSRtypeURL) && strSRtypeURL.equalsIgnoreCase('Promotions')))
        {
            system.debug('SR opened in draft mode '+strSRtypeURL);
            init();
        }
    }

    protected PromotionsController(Boolean shouldCall) {}

    //getter method used for calling the init method
    public String getName()
    {
        system.debug('--getNamest AccountID ---'+strAccoundId);
        system.debug('--strSRType ---'+strSRType);
        system.debug('--case ID ---'+strCaseID);
        if(String.isNotBlank(strAccoundId) && String.isBlank(strCaseID) &&  String.isNotBlank(strSRType) && strSRType.equalsIgnoreCase('Promotions')){
            strAccParamId = strAccoundId;
            init();
        }
        return strAccParamId;
    }

    // init method for intianiting the start up process
    public void init()
    {
        blnUnitSelected = false;
        blnIsSinglePromotionSelected = true;
        objCase = new Case();
        try
        {
            lstUnits = new List<SelectOption>();
            mapId_BookingUnit = new map<Id,Booking_Unit__c>();
            lstUnits.add(new selectOption('None', '--None--'));

            set<String> setActiveStatus = new set<String>();
            for(Booking_Unit_Active_Status__c unitStatusInst: [select Id, Status_Value__c
                                                               from Booking_Unit_Active_Status__c
                                                               where Status_Value__c != null]) {
               if(String.isNotBlank(unitStatusInst.Status_Value__c)){
                   setActiveStatus.add(unitStatusInst.Status_Value__c);
               }
            }

            for(Booking_Unit__c objBU : [Select Id
                                                , Registration_ID__c
                                                , Name
                                                , Booking__c
                                                , Unit_Details__c
                                                , Unit_Type__c
                                                , Permitted_Use__c
                                                , Rental_Pool__c
                                                , Inventory_Area__c
                                                , Unit_Selling_Price__c
                                                , NOC_Issued_Date__c
                                                , Dispute__c
                                                , Enforcement__c
                                                , Litigation__c
                                                , Counter_Case__c
                                                , Mortgage__c
                                                , Re_Assigned__c
                                                , Penalty_Amount__c
                                                , Token_Amount_val__c
                                                , Booking_Type__c
                                                , DP_Overdue__c
                                                , Plot_Price__c
                                                , Booking__r.CreatedDate
                                                , Agreement_Date__c
                                                , JOPD_Area__c
                                                , DP_OK__c
                                                , Doc_OK__c
                                                , OQOOD_Reg_Flag__c
                                                , Early_Handover__c
                                                , Handover_Flag__c
                                                , PCC_Release__c
                                                , Construction_Status__c
                                                , Area_Varied__c
                                                , No_of_parking__c
                                                , Type_of_Parking__c
                                                , Unit_Name__c
                                                , HOS_Name__c,Manager_Name__c,Property_Consultant__c
                                                , Selling_Price_Sq__c,CurrencyIsoCode,Title_Deed__c
                                                , Under_Assignment__c,Rebate_Given__c
                                                , Booking__r.Account__c
                                                , Inventory__r.Property__r.Name
                                                , Inventory__r.Property_City__c
                                                , Inventory__r.Unit_Type__c
                                                , Inventory__r.Property_Status__c
                                                , Inventory__r.Building_Code__c
                                                , Inventory__r.Building_ID__c
                                                , Inventory__r.Bedroom_Type__c
                                                , Inventory__r.Building_Location__c
                                                , Inventory__r.Floor_Plan__c
                                                , Inventory__r.Unit_Plan__c
                                                , Building_ID__c
                                                ,Building_Name__c
                                         From Booking_Unit__c
                                         Where Booking__r.Account__c =: strAccParamId
                                         AND Registration_Status__c IN: setActiveStatus
                                         and Re_Assigned__c = false])
            {
                system.debug('objBU.Unit_Name__c '+objBU.Unit_Name__c);
                system.debug('objBU.Id '+objBU.Id);
                lstUnits.add(new selectOption(objBU.Id, objBU.Unit_Name__c));
                mapId_BookingUnit.put(objBU.Id, objBU);
            }

            system.debug('lstUnits '+lstUnits);
            system.debug('blnUnitSelected '+blnUnitSelected);
            //SR is opened in draft mode
            if(String.isNotBlank(strCaseID) && String.isNotBlank(strAccParamId))
            {
                lstPromotions = new List<PromotionsWrapper>();
                //fetch case details
                  objCase = [ SELECT
                                    Id
                                    ,CaseNumber
                                    ,Case_Summary__c
                                    ,Status
                                    ,Approval_Status__c
                                    ,Pending_Amount__c ,Booking_Unit__c
                                    ,Total_Amount__c
                                    ,Booking_Unit__r.Building_Name__c
                              FROM Case WHERE ID =:strCaseID
                            ];

                  blnUnitSelected = true;
                  strSelectedUnit = objCase.Booking_Unit__c;
                  fetchUnitDetails();

                  if(objCase.Total_Amount__c != null)
                  {
                    totalPrice =  objCase.Total_Amount__c;
                    //query on Promotion Allocation to display already selected Promotion Package
                    //for specific Booking Unit and Case
                    Set<Id> setPromotionOffer = new Set<Id>();
                    for(Promotion_Package_Allocation__c objPromotionAllocate : [Select Promotion_Package__c
                                                                                FROM
                                                                                Promotion_Package_Allocation__c
                                                                                WHERE
                                                                                Case__c =:objCase.Id
                                                                                AND Booking_Unit__c =: objCase.Booking_Unit__c]
                    )
                    {
                        setPromotionOffer.add(objPromotionAllocate.Promotion_Package__c);
                    }

                    system.debug('setPromotionOffer '+setPromotionOffer.size());
                    system.debug('setPromotionOffer '+setPromotionOffer);

                    if(setPromotionOffer.size() > 0)
                    {
                        for(Promotion_Package_Offer__c objPromotion : [ Select Id,Booking_Unit__c,Booking_Unit_Name__c,Promotion_Package__c
                                                              ,Promotion_Package__r.Name,Promotion_Package__r.Type__c
                                                              ,Promotion_Package__r.Price__c,Promotion_Package__r.Promotion_Name__c
                                                               FROM Promotion_Package_Offer__c
                                                               WHERE Building_Name__c =: objCase.Booking_Unit__r.Building_Name__c
                                                            ]
                        )
                        {
                            PromotionsWrapper objPromotionWrap = new PromotionsWrapper();
                            if(setPromotionOffer.contains(objPromotion.Promotion_Package__c))
                            {
                                objPromotionWrap.blnIsSelected = true;
                            }
                            else
                            {
                                objPromotionWrap.blnIsSelected = false;
                            }
                            objPromotionWrap.objPromotionOffer = objPromotion;
                            lstPromotions.add(objPromotionWrap);
                        }
                    }
                  }
            }
            getCustomerPortfolio();
        }
        catch(Exception exp)
        {
            system.debug('exp '+exp.getStackTraceString());
            system.debug('exp customErrorMsg '+exp.getMessage());
            ApexPages.addmessage(
                                  new ApexPages.message(
                                  ApexPages.severity.ERROR,'Error Occured. Please contact System Admin '+exp.getMessage())
                                );
        }
        strHelpPageId = FetchDocumentURL.fetchDocURL('Promotions_Help');
    }

    // method used to retrieve customer portfolio details
    public void getCustomerPortfolio()
    {
      //system.debug('strAccParamId '+strAccParamId);
      objAccount = [ SELECT Id,Name,Title__c
                    ,PersonTitle,PersonMobilePhone
                    ,PersonEmail,FirstName,LastName
                    ,MiddleName,Party_ID__c,Nationality__pc
                    ,Party_Type__c,Passport_Number__c,CR_Number__c
                    FROM Account WHERE ID = : strAccParamId ];
    }

    // method used to get the unit detais of selected unit from endpoint
    public void fetchUnitDetails()
    {
        try
        {
            system.debug('strSelectedUnit '+strSelectedUnit);
            // re initializing list here because on unit change promotion for that specific units need to be fetched by CRE
            // by clicking on Fetch promotion buttons.
            lstPromotions = new List<PromotionsWrapper>();
            if(String.isNotBlank(strSelectedUnit) && !strSelectedUnit.equalsIgnoreCase('None'))
            {
                blnUnitSelected = true;
                // method used to get existing cases on selected booking unit
                String strExistingCases = '';
                List<Case> lstExistingCases = checkExistingSRExists();
                if(lstExistingCases.size() > 0)
                {
                    strExistingCases = 'Promotions SR cannot be initiated because below SR already exists on selected Booking Unit <br/>';
                    for(Case objCase : lstExistingCases)
                    {
                        //system.debug('objCase.Booking_Unit__r.Name '+objCase.Booking_Unit__r.Name);
                        //strExistingCases += objCase.Booking_Unit__r.Unit_Name__c+' - '+objCase.RecordType.DeveloperName+' - '+objCase.CaseNumber+'<br/>';
                        strExistingCases += mapId_BookingUnit.get(strSelectedUnit).Unit_Name__c+' - '+objCase.RecordType.DeveloperName+' - '+objCase.CaseNumber+'<br/>';
                    }
                }
                else
                {
                    strExistingCases = '';
                }
                system.debug('strExistingCases '+strExistingCases);

                if(String.isNotBlank(strExistingCases))
                {
                  blnUnitSelected = false;
                  ApexPages.addmessage(
                                      new ApexPages.message(
                                      ApexPages.severity.ERROR,strExistingCases)
                                    );
                }

                if(blnUnitSelected)
                {
                    Booking_Unit__c objBookingUnitToDisplay = new Booking_Unit__c();
                    objWrapperBookingUnit = new WrapperBookingUnit();

                    if(mapId_BookingUnit.containsKey(strSelectedUnit) && mapId_BookingUnit.get(strSelectedUnit) != null)
                    {
                        objWrapperBookingUnit.objBookingUnit = mapId_BookingUnit.get(strSelectedUnit);
                        objBookingUnitToDisplay = mapId_BookingUnit.get(strSelectedUnit);
                        objUnit = mapId_BookingUnit.get(strSelectedUnit);
                        //fetch booking unit details from IPMS
                        objBUDetailWrapper = UnitDetailsService.getBookingUnitDetails(objBookingUnitToDisplay.Registration_ID__c);
                    }
                    strUnitDetailType = 'Unit Details';

                    //get all open SRs
                    lstOpenSRs = new List<Case>();
                    lstClosedSRs = new List<Case>();

                    // for fetching all Open SR which are NOT using Junction object for storing Booking Unit details
                    for(Case objCase : [ SELECT Id,SR_Type__c
                                         ,RecordType.DeveloperName,CaseNumber
                                          FROM Case WHERE Status != 'Closed' AND Booking_Unit__c =:strSelectedUnit
                                       ])
                    {
                      lstOpenSRs.add(objCase);
                    }

                    // for fetching all Closed SR which are NOT using Junction object for storing Booking Unit details
                    for(Case objCase : [ SELECT Id,SR_Type__c
                                         ,RecordType.DeveloperName,CaseNumber
                                          FROM Case WHERE Status = 'Closed' AND Booking_Unit__c =:strSelectedUnit
                                       ])
                    {
                      lstClosedSRs.add(objCase);
                    }

                    Id bookingId = objBookingUnitToDisplay.Booking__c;
                    lstExistingJB = [Select b.Primary_Buyer__c
                                      , b.Last_Name__c
                                      , b.First_Name__c
                                      , b.Booking__c
                                      , b.Buyer_ID__c
                                     From Buyer__c b
                                     where b.Booking__c = :bookingId
                                     and b.Primary_Buyer__c = false];

                // create case as we need to pass the case number for uploading documents
                if(String.isBlank(strCaseID) && String.isNotBlank(strAccParamId))
                {
                  // inserting case here because we need to send the case number while uploading document to IPMS
                  Case objNewCase = new Case();
                  Id parkingRecordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Promotions').getRecordTypeId();
                  objNewCase.Status = 'New';
                  objNewCase.RecordTypeID = parkingRecordTypeID;
                  objNewCase.Subject = 'Promotions SR';
                  objNewCase.Booking_Unit__c = objUnit.Id;
                  objNewCase.AccountId = strAccParamId;
                  insert objNewCase;
                  objCase = objNewCase;
                  strCaseNumber = [Select CaseNumber FROM Case WHERE Id =: objCase.Id].CaseNumber;
                  system.debug('strCaseNumber '+strCaseNumber);
                }
                else // case is opened in draft mode
                {
                  strCaseNumber = [Select CaseNumber FROM Case WHERE Id =: objCase.Id].CaseNumber;
                }
              }
            }
            else
            {
                blnUnitSelected = false;
                ApexPages.addmessage(
                                     new ApexPages.message(
                                     ApexPages.severity.ERROR,'Please select atleast one Booking Unit.')
                                    );
            }
        }
        catch(Exception exp)
        {
            ApexPages.addmessage(
                                  new ApexPages.message(
                                  ApexPages.severity.ERROR,'Error Occured. Please contact System Admin '+exp.getMessage())
                                );
        }
    }

    // method used to retrieve parkign information from endpoint
    public void getPromotions()
    {
        system.debug('promotions information method called');
        lstPromotions = new List<PromotionsWrapper>();
        try
        {
          // if objUnit is not null then booking unit is selected
          if(objUnit != null)
          {
              for(Promotion_Package_Offer__c objPromotion : [ Select Id,Booking_Unit__c,Booking_Unit_Name__c,Promotion_Package__c
                                                              ,Promotion_Package__r.Name,Promotion_Package__r.Type__c
                                                              ,Promotion_Package__r.Price__c,Promotion_Package__r.Promotion_Name__c
                                                               FROM Promotion_Package_Offer__c
                                                               WHERE
                                                               Building_Name__c =: objUnit.Building_Name__c
                                                            ]
                 )
              {

                PromotionsWrapper objPromotionWrap = new PromotionsWrapper();
                objPromotionWrap.blnIsSelected = false;
                objPromotionWrap.objPromotionOffer = objPromotion;
                lstPromotions.add(objPromotionWrap);
              }
              system.debug('lstPromotions '+lstPromotions.size());
              if(lstPromotions != null && lstPromotions.size() == 0)
              {
                ApexPages.addmessage(new ApexPages.message(
                                  ApexPages.severity.INFO,'INFO - There are no Promotions available for selected unit.'));
              }
          }
          else
          {
              ApexPages.addmessage(new ApexPages.message(
                                  ApexPages.severity.Error,'Error - Please select atleast one booking unit.'));
          }
        }
        catch(Exception exp)
        {
            ApexPages.addmessage(new ApexPages.message(
                                      ApexPages.severity.Error,'Error - Fetching Promotions Informaiton : '+exp.getMessage()));
            errorLogger(exp.getMessage()+' - '+exp.getStackTraceString(), strCaseID,objUnit.Id);
        }
    }

    //method to calculate the total price for all promotions selected
    public Pagereference calculateTotalPrice()
    {
        try
        {   Integer intCounter = 0;

            for(PromotionsWrapper objPromotion : lstPromotions)
            {
                if(objPromotion.blnIsSelected)
                {
                    intCounter++;
                }
            }

            if(intCounter > 1)
            {
                blnIsSinglePromotionSelected = false;
                ApexPages.addmessage(new ApexPages.message(
                                      ApexPages.severity.Error,'Error - You cannot select more than one Promotion.'));
                return null;
            }
            else
            {
                blnIsSinglePromotionSelected = true;
            }

            system.debug('rowToFetchPrice '+rowToFetchPrice);
            if(rowToFetchPrice != null)
            {
                // get the price of the selected promotion
                PromotionsWrapper objPromotionDetailData = lstPromotions[rowToFetchPrice];

                system.debug('objPromotionDetailData '+objPromotionDetailData);
                system.debug('objPromotionDetailData price'+objPromotionDetailData.objPromotionOffer.Promotion_Package__r.Price__c);

                if(objPromotionDetailData.objPromotionOffer.Promotion_Package__r.Price__c != null)
                {
                    if(objPromotionDetailData.blnIsSelected)
                    {
                        totalPrice += objPromotionDetailData.objPromotionOffer.Promotion_Package__r.Price__c;
                    }
                    else if(!objPromotionDetailData.blnIsSelected)
                    {
                        totalPrice -= objPromotionDetailData.objPromotionOffer.Promotion_Package__r.Price__c;
                    }
                }
            }
        }
        catch(Exception exp)
        {
            ApexPages.addmessage(new ApexPages.message(
                                      ApexPages.severity.Error,'Error - Calculating Total Price : '+exp.getMessage()));
        }
        system.debug('totalPrice '+totalPrice);
        return null;
    }

    // this method is used to call the IPMS end point and generate SOA of selected unit
    public void GenarateSOA()
    {
        system.debug('Generate SOA method called');
        // this method will call the IPMS and fetch the SOA from there
        // then it will map the SOA to the SR created
        try
        {
            if(String.isNotBlank(strSelectedUnit) && !strSelectedUnit.equalsIgnoreCase('None'))
            {
                blnUnitSelected = true;
                Booking_Unit__c objBookingUnit = new Booking_Unit__c();

                if(mapId_BookingUnit.containsKey(strSelectedUnit) && mapId_BookingUnit.get(strSelectedUnit) != null)
                {
                    objBookingUnit = mapId_BookingUnit.get(strSelectedUnit);
                    if(String.isNotBlank(objBookingUnit.Registration_ID__c))
                    {
                      String strRegId = objBookingUnit.Registration_ID__c;
                      GenerateSOAController.soaResponse strResponse = GenerateSOAController.getSOADocument(strRegId);
                      system.debug('SOA generated status '+strResponse.status);
                      system.debug('SOA generated url'+strResponse.url);

                      if(String.isNotBlank(strResponse.url))
                      {
                        strSOAURL = strResponse.url;
                      }
                      else if(String.isNotBlank(strResponse.PROC_MESSAGE))
                      {
                        ApexPages.addmessage(
                                new ApexPages.message(
                                ApexPages.severity.ERROR,'Error - Generate SOA: '+strResponse.PROC_MESSAGE)
                            );
                      }
                    }
                }
                else
                {
                    blnUnitSelected = false;
                    ApexPages.addmessage(
                                     new ApexPages.message(
                                     ApexPages.severity.ERROR,'Please select atleast one Booking Unit.')
                                    );
                }
            }
        }
        catch(Exception exp)
        {
            system.debug('exp SOA '+exp.getStackTraceString());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error - Generate SOA: '+exp.getMessage()));
        }
    }


    // method used to check for SR Initiation matrix as provided by DAMAC
    public list<Case> checkExistingSRExists()
    {
        set<String> setAllowedSRTypes = new set<String>();
        setAllowedSRTypes.add('Parking');
        setAllowedSRTypes.add('Promotions');
        // added to by pass SR initiation error for testing need to remove when deploying
        setAllowedSRTypes.add('Assignment');
        // below as SR that if are raised then also Parking SR can be raised on same Unit. Hence checked not in in below query
        // to get list of all types of SR against which same SR cannot be raised.
        setAllowedSRTypes.add('Complaint');
        setAllowedSRTypes.add('Early_Handover');
        setAllowedSRTypes.add('Fund_Transfer');
        setAllowedSRTypes.add('Handover');
        setAllowedSRTypes.add('Mortgage');
        setAllowedSRTypes.add('NOC_For_Visa');
        setAllowedSRTypes.add('Penalty_Waiver');
        setAllowedSRTypes.add('Proof_of_Payment_SR');
        setAllowedSRTypes.add('Customer_Refund');
        setAllowedSRTypes.add('Token_Refund');
        setAllowedSRTypes.add('Rental_Pool_Agreement');
        setAllowedSRTypes.add('Rental_Pool_Assignment');
        setAllowedSRTypes.add('Rental_Pool_Termination');
        setAllowedSRTypes.add('Utility_Registration_SR');
        setAllowedSRTypes.add('Plot_Handover');
        setAllowedSRTypes.add('Cheque_Replacement_SR');
        lstExistingCase = new list<Case>();
        map<Id,Case> mapId_Case = new map<Id,Case>([Select c.Id
                                                         , c.Booking_Unit__c
                                                         , c.Booking_Unit__r.Unit_Name__c
                                                         , c.AccountId
                                                         , c.CaseNumber
                                                         , c.RecordType.DeveloperName
                                                         , c.RecordType.Name
                                                    From Case c
                                                    where c.Booking_Unit__c =: strSelectedUnit
                                                    and c.Status != 'Closed'
                                                    and c.Status != 'Rejected'
                                                    and c.Status != 'Cancelled'
                                                    and c.RecordType.DeveloperName NOT IN : setAllowedSRTypes]);
        if(mapId_Case != null && !mapId_Case.isEmpty())
        {
            lstExistingCase.addAll(mapId_Case.values());
        }
        for(SR_Booking_Unit__c objSBU : [Select s.Id
                                              , s.Case__c
                                              , s.Case__r.Status
                                              , s.Case__r.CaseNumber
                                              , s.Case__r.RecordType.DeveloperName
                                              , s.Case__r.RecordType.Name
                                              , s.Booking_Unit__c
                                               , s.Booking_Unit__r.Unit_Name__c
                                         From SR_Booking_Unit__c s
                                         where s.Booking_Unit__c =:strSelectedUnit
                                         and s.Case__r.Status != 'Closed'
                                         and s.Case__r.Status != 'Rejected'
                                         and s.Case__r.Status != 'Cancelled'
                                         and s.Case__r.RecordType.DeveloperName = 'AOPT']){
            if(!mapId_Case.containsKey(objSBU.Case__c))
            {
                Case objCase = objSBU.Case__r;
                objCase.Booking_Unit__c = objSBU.Booking_Unit__c;
                //objCase.Id = objSBU.Case__c;
                //objCase.CaseNumber = objSBU.Case__r.CaseNumber;
                lstExistingCase.add(objCase);
            }
        }
        system.debug('lstExistingCase*****'+lstExistingCase);
        return lstExistingCase;
    }

    //method used to create SR for Additional Parking with all required details
    public virtual Pagereference createSR()
    {
        system.debug('totalPrice=='+totalPrice);
        system.debug('objUnit.Id=='+objUnit.Id);
        String strStatus;

        try
        {
            objCase.Booking_Unit__c = objUnit.Id;
            objCase.Pending_Amount__c = totalPrice;
            objCase.Total_Amount__c = totalPrice;

            Id promotionRecordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Promotions').getRecordTypeId();
            if(String.isNotBlank(strCaseStatusSubmitted) && strCaseStatusSubmitted.equalsIgnoreCase('Submitted'))
            {
                objCase.Status = 'Submitted';
            }
            else
            {
                objCase.Status = 'New';
            }
            objCase.RecordTypeID = promotionRecordTypeID;
            objCase.Subject = 'Promotions SR';
            objCase.Origin = 'Walk-In';
            objCase.SR_Type__c = 'Promotions';

            upsert objCase;

            List<Promotion_Package_Allocation__c> lstPromotionAllocation = new List<Promotion_Package_Allocation__c>();
            Set<Id> promotionPackageSetForDelete = new Set<Id>();
            Set<Id> bookingUnitSetForDelete = new Set<Id>();

            if(lstPromotions != null && lstPromotions.size() > 0)
            {
                Map<Id,Promotion_Package_Allocation__c> mapPromotionAllocation = new Map<Id,Promotion_Package_Allocation__c>();
                // get all promotion allocated to Booking Unit previously
                for(Promotion_Package_Allocation__c objAllocate : [  SELECT Id,Promotion_Package__c
                                                                     FROM
                                                                     Promotion_Package_Allocation__c
                                                                     WHERE
                                                                     Case__c =: objCase.Id
                                                                     AND Booking_Unit__c =: objUnit.Id])
                {
                    mapPromotionAllocation.put(objAllocate.Promotion_Package__c,objAllocate);
                }

                for(PromotionsWrapper objPromotion : lstPromotions)
                {
                    system.debug('mapPromotionAllocation contains '+!mapPromotionAllocation.containsKey(objPromotion.objPromotionOffer.Promotion_Package__c));
                    system.debug('objPromotion.blnIsSelected '+objPromotion.blnIsSelected);
                    if(objPromotion.blnIsSelected && !mapPromotionAllocation.containsKey(objPromotion.objPromotionOffer.Promotion_Package__c))
                    {
                        Promotion_Package_Allocation__c objPromotionAllocate = new Promotion_Package_Allocation__c();
                        //objPromotionAllocate.Booking_Unit__c = objPromotion.objPromotionOffer.Booking_Unit__c;
                        objPromotionAllocate.Booking_Unit__c = objUnit.Id;
                        objPromotionAllocate.Promotion_Package__c = objPromotion.objPromotionOffer.Promotion_Package__c;
                        objPromotionAllocate.Case__c = objCase.Id;

                        lstPromotionAllocation.add(objPromotionAllocate);
                    }
                    else if(!objPromotion.blnIsSelected && mapPromotionAllocation.containsKey(objPromotion.objPromotionOffer.Promotion_Package__c))
                    {
                        promotionPackageSetForDelete.add(objPromotion.objPromotionOffer.Promotion_Package__c);
                        bookingUnitSetForDelete.add(objPromotion.objPromotionOffer.Booking_Unit__c);
                    }
                }

                system.debug('lstPromotionAllocation '+lstPromotionAllocation);
                insert lstPromotionAllocation;

                List<Promotion_Package_Allocation__c> lstPromotionAllocationToDelete = getPromotionAllocation(
                                                                                                       promotionPackageSetForDelete
                                                                                                       ,bookingUnitSetForDelete
                                                                                                       ,objCase.Id);
                system.debug('lstPromotionAllocationToDelete '+lstPromotionAllocationToDelete);
                if(lstPromotionAllocationToDelete != null && lstPromotionAllocationToDelete.size() > 0)
                {
                    delete lstPromotionAllocationToDelete;
                }
            }
            else
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error : Please select atleast one Promotion.'));
                return null;
            }

            // map data to case object fields.
            if(objCase.Id != null)
            {
              strCaseID = objCase.Id;
              Case objInsertedCase = [SELECT Id,CaseNumber,OwnerId FROM Case WHERE id =: strCaseID LIMIT 1];

              strStatus = 'SR record created successfully.'+' '+'SR No:'+objInsertedCase.CaseNumber;
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Success : '+strStatus));

              if(String.isNotBlank(strCaseStatusSubmitted) && strCaseStatusSubmitted.equalsIgnoreCase('Submitted'))
              {
                // if SR is submitted create first task and submit for approval
                PromotionsUtility.submitRecordForApproval(objInsertedCase);
                return new Pagereference('/' + objCase.Id);
              }
              else
              {
                system.debug('save as draft clicked');
                system.debug('strAccParamId '+strAccParamId);

                Pagereference pg = Page.PromotionsProcessRequestPage;
                pg.getParameters().put('AccountId',strAccParamId);
                pg.getParameters().put('caseID',objCase.Id);
                pg.getParameters().put('SRType','Promotions');
                pg.setRedirect(true);
                return pg;
              }
            }
            else
            {
                strStatus += 'SR record not created. <br/>';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error : '+strStatus));
                return null;
            }
        }
        catch(Exception exp)
        {
            ApexPages.addmessage(new ApexPages.message(
                                      ApexPages.severity.Error,'Error - Submitting SR : '+exp.getMessage()));
        }
        return null;
    }

    // method used to get Promotion Allocation for specific Booking Units and Promotion Package
    public List<Promotion_Package_Allocation__c> getPromotionAllocation(
                                                                          Set<Id> promotionPackageSetForDelete
                                                                         ,Set<Id> bookingUnitSetForDelete
                                                                         , ID caseId
                                                                        )
    {
        List<Promotion_Package_Allocation__c> lstPromotionAllocation = new List<Promotion_Package_Allocation__c>();
        if(promotionPackageSetForDelete.size() > 0 && bookingUnitSetForDelete.size() > 0)
        {
            lstPromotionAllocation = [   SELECT Id
                                         FROM
                                         Promotion_Package_Allocation__c
                                         WHERE
                                         Booking_Unit__c IN: bookingUnitSetForDelete
                                         AND
                                         Promotion_Package__c IN: promotionPackageSetForDelete
                                         AND Case__c =: caseId
                                     ];
            system.debug('lstPromotionAllocation '+lstPromotionAllocation.size());
            system.debug('lstPromotionAllocation '+lstPromotionAllocation);

        }

        return lstPromotionAllocation;
    }

  public static void errorLogger(string strErrorMessage, string strCaseID,string strBookingUnitID)
  {
    Error_Log__c objError = new Error_Log__c();
    objError.Error_Details__c = strErrorMessage;
    objError.Process_Name__c = 'Promotions';
    if(String.isNotBlank(strCaseID) && strCaseID.startsWith('500')){
        objError.Case__c = strCaseID;
    }
    if(String.isNotBlank(strBookingUnitID)){
        objError.Booking_unit__c = strBookingUnitID;
    }
    insert objError;
  }

  // wrappr class to store details for payment terms installment
    public class PromotionsWrapper
    {
        public Boolean blnIsSelected {get;set;}
        public Promotion_Package_Offer__c objPromotionOffer {get;set;}
    }

}