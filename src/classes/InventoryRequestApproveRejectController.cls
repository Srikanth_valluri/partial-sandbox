/***************************************************************************************************
 * @Name              : InventoryRequestApproveRejectController
 * @Test Class Name   : InventoryRequestApproveRejectTest 
 * @Description       : 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         19/08/2020       Created
****************************************************************************************************/
public class InventoryRequestApproveRejectController{
    public Id reqId {get;set;}
    public Boolean isApproval {get; set;}
    public String recipientType {get; set;}
    public String actionDone {get; set;}
    public boolean unitsExist {get; set;}
    public List<String> unitDetailsList {get; set;}
    public string recordLink {get; set;}
    public Deal_Exception_Request__c req{
        get{
        system.debug('Inside GET request');
        system.debug('reqId: ' +reqId);
        unitsExist = false;
        if(isApproval){
            actionDone = 'Approved';
        } else{
            actionDone = 'Rejected';
        }
        unitDetailsList = new List<String>();
        req = [SELECT Id, Name, Sales_Admin_Comments__c, Inventory_Request_Number__c, 
                RM__r.Name, RM__c, Sales_Admin_Manager_Comments__c, RM_Comments__c,
                ( SELECT Id, Inventory__r.Name, Inventory__r.Unit_Name__c FROM Deal_Exception_Units__r) 
               FROM Deal_Exception_Request__c WHERE Id =: reqId];
        system.debug('req: ' +req);
        if(isApproval && req.Deal_Exception_Units__r.size() > 0){
            unitsExist = true;
            for(Deal_Exception_Unit__c reqUnit: req.Deal_Exception_Units__r){
                unitDetailsList.add(reqUnit.Inventory__r.Name + ' (' +  reqUnit.Inventory__r.Unit_Name__c + ')');
            }
        }
        recordLink = System.URL.getSalesforceBaseUrl().toExternalForm() + '/apex/RequestInventory?id=' + req.Id; 
        return req;
        }  set;
    }
}