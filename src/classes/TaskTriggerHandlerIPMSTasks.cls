public without sharing class TaskTriggerHandlerIPMSTasks {
    public static  boolean firstRun = true;    
    public static void pushToIPMSOnAfterUpdate(map<Id,Task> newMap, map<Id,Task> oldMap) {
         System.debug('Calling After Update PushToIPMS');
         set<Id> caseIdSet = new set<Id>();
         set<Id> taskIdSet = new set<Id>();
         List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
         Credentials_details__c creds = Credentials_details__c.getInstance('Customer And EOI Refund');
         for(Task taskInst: newMap.values()) {
            if((taskInst.Subject != Null && taskInst.Subject == 'Update EOI Refund Details in IPMS' || taskInst.Subject == 'Update Refund Details in IPMS') &&
             taskInst.WhatId !=Null && taskInst.Status != oldMap.get(taskInst.Id).Status && taskInst.Status == 'Completed') {
                caseIdSet.add(taskInst.WhatId);
                taskIdSet.add(taskInst.Id);
             }
         }
         System.debug('caseIdSet>>>>>>>'+caseIdSet);
         System.debug('taskIdSet>>>>>>>'+taskIdSet);
        if((caseIdSet != Null && caseIdSet.size() > 0) &&  (taskIdSet != Null && taskIdSet.size() > 0)) {
            System.debug('Future Method getting called');
            if(TaskTriggerHandlerIPMSTasks.firstRun) {
             TaskTriggerHandlerIPMSTasks.firstRun = false;
             genericServiceToPushTOIPMSEOIAndCustomer(taskIdSet,caseIdSet);
            }
        }
    }
        
    @future(callout=true)
    public static void genericServiceToPushTOIPMSEOIAndCustomer(Set<Id> setTaskId, Set<Id> setCaseId) {
     System.debug('Inside future method');
     System.debug('caseIdSet>>>>>--->>'+setCaseId);
     System.debug('taskIdSet>>>>>---->>'+setTaskId);   
     Map<Id, Case> mapCase = IPMSTaskUtility.getCaseMap(setCaseId);
     System.debug('mapCase>>>>>>>'+mapCase);
     Map<Id, Case_Extension__c> mapCaseExtension = IPMSTaskUtility.getCaseExtensionMap(setCaseId);
     System.debug('mapCaseExtension>>>>>>>'+mapCaseExtension);
     List<Case> caseList = new List<Case>();
     List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
     List<Task> lstTaskToUpdate = new List<Task>();
     Credentials_details__c creds = Credentials_details__c.getInstance('Customer And EOI Refund');
     for(Task objTask: getTasks(setTaskId)) {            
            if(mapCase != Null && mapCase.containsKey(objTask.WhatId) != Null && mapCase.get(objTask.WhatId) != Null) {             
                Case_Extension__c objCaseExtension;
                Case objCase = mapCase.get(objTask.WhatId);                 
                if(mapCaseExtension != Null && mapCaseExtension.containsKey(objTask.WhatId) != Null && mapCaseExtension.get(objTask.WhatId) != Null) {
                  objCaseExtension = mapCaseExtension.get(objTask.WhatId);
                }
                if(objCase != Null && (objCase.RecordType.DeveloperName == 'EOI_Refund' ||  objCase.RecordType.DeveloperName == 'Customer_Refund') && objCaseExtension != Null && objCaseExtension.Payment_Mode__c == 'Transfer') {
                    String requestBody = requestBodyEOIAndCustomerRefund(objCase,objCaseExtension);
                    System.debug('requestBody>>>>>>>'+requestBody);
                    if(String.isNotBlank(requestBody) &&( creds != null && String.isNotBlank(creds.Endpoint__c) && String.isNotBlank(creds.Password__c) && String.isNotBlank(creds.User_Name__c))) {
                        HTTPResponse resp = genericServiceForEOIAndCustomerRefund(requestBody,creds);
                           if(resp != Null && resp.getStatusCode() == 200) {
                            TaskCreationIPMSResponse responseObj = ( TaskCreationIPMSResponse )System.JSON.deserialize( resp.getBody(), TaskCreationIPMSResponse.class );
                            if(responseObj != null && responseObj.OutputParameters != Null && responseObj.OutputParameters.X_RETURN_STATUS == 'S' 
                            && responseObj.OutputParameters.X_RETURN_MESSAGE.contains('Process Completed')) { 
                                objTask.Pushed_to_IPMS__c = true; 
                                 for(X_RESPONSE_MESSAGE_ITEM objResponseMessage : responseObj.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM) {
                                    if(objResponseMessage.ATTRIBUTE1 != Null) {
                                         objCase.Authorization_Id__c = 'AR Transaction ID is: '+ objResponseMessage.ATTRIBUTE1;
                                     }
                                     if(objResponseMessage.ATTRIBUTE2 != Null) {
                                         objCase.EOI_reference_Number__c = 'AP Transaction ID is: '+objResponseMessage.ATTRIBUTE2;
                                     }
                                     if(objResponseMessage.PROC_STATUS != Null) {
                                         objCase.SR_Status_Sbl__c = objResponseMessage.PROC_STATUS;
                                     }
                                      caseList.add(objCase);
                                 }
                            }
                            else {
                                objTask.Pushed_to_IPMS__c = false;
                                for(X_RESPONSE_MESSAGE_ITEM objResponseMessage : responseObj.OutputParameters.X_RESPONSE_MESSAGE.X_RESPONSE_MESSAGE_ITEM) {
                                    objTask.Task_Error_Details__c = String.valueOf(objResponseMessage.PROC_MESSAGE);
                                    //lstErrorLog.add(new Error_Log__c(Case__c = objCase.Id, Error_Details__c = String.valueOf(objResponseMessage.PROC_MESSAGE)));
                                }
                            }
                        }
                        Error_Log__c objE = GenericUtility.createErrorLog(resp.getBody(), '', 
                                                                                  '', '', 
                                                                                  objCase.Id);
                        objE.Service_Request_Body__c = requestBody;
                        objE.Service_Response_Body__c = resp.getBody();
                        lstErrorLog.add(objE);
                    }  
               }      
            }
     
        lstTaskToUpdate.add(objTask);
    }
    if(!caseList.isEmpty()) {
        update caseList;
    }
    if(!lstErrorLog.isEmpty()) {
        insert lstErrorLog;
    }
    if(!lstTaskToUpdate.isEmpty()) {
        update lstTaskToUpdate;
    }
        
}        
          //Method to get task details
        public static List<Task> getTasks( Set<Id> setTaskId ) {
            return new List<Task>( [ SELECT WhoId, WhatId, Type, Status, OwnerId, Id, Subject, CreatedDate, Description,
                                     Assigned_User__c, ActivityDate, Owner.Name, Document_URL__c, Booking_Unit__c,
                                     Booking_Unit__r.Registration_Id__c, POA_Right__c
                                     FROM Task
                                     WHERE Id IN : setTaskId ] );
        }
        public static HTTPResponse genericServiceForEOIAndCustomerRefund(String requestBody,Credentials_details__c creds ) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint(creds.Endpoint__c);                    
            req.setMethod('POST');
            req.setHeader('ACCEPT','application/json');
            req.setHeader('Content-Type', 'application/json');
            req.setTimeout(120000); 
            String headerValue = 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(creds.User_Name__c + ':' + creds.Password__c));
            req.setHeader('Authorization', headerValue);
            system.debug('headerValue '+headerValue);
            Http http = new Http();
            HTTPResponse res;
            if(!Test.isRunningTest()){
                res = http.send(req);
            }
            else {
                res = new HTTPResponse();
            }
            return res;
        }
        
        /*public static Map<Id, Case> getCaseMap(Set<Id> setCaseId) {
            return new Map<Id, Case>([Select Id, CaseNumber, RecordType.DeveloperName, RecordType.Name, POA_Name__c, POA_Expiry_Date__c,
                                      Purpose_of_POA__c, POA_Issued_By__c, Amount_Approved__c, Booking_Unit__c,
                                      Booking_Unit__r.Registration_Id__c, Account.Party_ID__c, Approved_Amount__c, CreatedDate,
                                      Actual_Approving_Percent__c, Status, Payment_Date__c, Payment_Mode__c,
                                      Payment_Allocation_Details__c, Total_Amount__c, Payment_Currency__c,
                                      Cheque_Number__c, Bounced_Cheque_Date_of_Expiry__c, Cheque_Amount__c, Cheque_Bank_Name__c,
                                      Receipt_Id__c, Approving_User_Name__c, Owner.Name, NewPaymentTermJSON__c,
                                      Customer_First_Name__c, Customer_Last_Name__c, Related_Unit_Name__c,Description,
                                      Refund_Amount__c, EOI_Date__c, EOI_Reference_Number__c, 
                                      Customer_Middle_Name__c, Sender_Name__c, Swift_Code__c,POA_Number__c, POA_Issued_By_Picklist__c, 
                                      POA_Valid_Till__c, New_CR__c, Source_of_Funds__c,
                                      (Select Id, Booking_Unit__c, Booking_Unit__r.Registration_Id__c,Booking_Unit__r.Unit_Selling_Price_AED__c,Booking_Unit__r.Party_Id__c,Booking_Unit__r.Inventory__r.Property_ID__c,Booking_Unit__r.Customer_Number__c
                                      From SR_Booking_Units__r Where Booking_Unit__c != null),
                                      (Select Id, New_Unit__c, New_Unit__r.Registration_Id__c, Allocated_Amount__c
                                      From Fund_Transfer_Units__r Where New_Unit__c != null),
                                      (Select Id, Attachment_URL__c From SR_Attachments__r Where Booking_Unit__c != null),
                                      (Select Id, Receipt_No__c, Days_to_Hold__c, New_Amount__c, New_Bank__c, New_cheque_number__c,
                                      New_maturity_date__c, Reason_For_Replacement__c, CurrencyIsoCode 
                                      From Case_Post_Dated_Cheques__r)
                                      From Case
                                      Where Id IN: setCaseId]);
        }*/
        /*public static Map<Id, Case_Extension__c> getCaseExtensionMap(Set<Id> setCaseId) {
            Map<Id, Case_Extension__c> mapOfCaseIdToCseExtension = new Map<Id,Case_Extension__c>();
            List<Case_Extension__c> caseExtensionList = [Select Id,Amount__c,Beneficiary_Account_Name__c,Beneficiary_Account_Number__c,Beneficiary_Add__c,Beneficiary_Bank_Branch__c,Beneficiary_Bank_Name__c,
                                     Beneficiary_Branch_Country__c,
                                     Beneficiary_IBAN__c,Beneficiary_Name__c,Case__c,Customer_name_Account_details__c,IFSC__c,Payment_Mode__c,Property_Name_Short_Code__c,
                                     Receipt_Number__c,Sort_Code__c,SR_Number__c,Supp_Corr_Bank_Branch_add1__c,Supp_Corr_Bank_Name__c,Supp_Corr_Bank_Swift_Code__c,Swift_Code__c                  
                                     From Case_Extension__c
                                     Where Case__c IN: setCaseId];
            for(Case_Extension__c objCaseExtension :caseExtensionList ) {
                mapOfCaseIdToCseExtension.put(objCaseExtension.Case__c,objCaseExtension);
            }
            return mapOfCaseIdToCseExtension;
        }*/
        public static string requestBodyEOIAndCustomerRefund(Case objCase,Case_Extension__c objCaseExtension) {            
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();     
            gen.writeFieldName('EOI_CUSTOMER_REFUND_DM');
            gen.writeStartObject();
            gen.writeFieldName('InputParameters');
            gen.writeStartObject();
            gen.writeStringField('P_REQUEST_NUMBER', String.isNotBlank(objCase.CaseNumber)?objCase.CaseNumber:'');           
            gen.writeStringField('P_SOURCE_SYSTEM', 'eBS');
            gen.writeStringField('P_REQUEST_NAME', 'CUSTOMER_REFUND_DM');
            gen.writeFieldName('P_REQUEST_MESSAGE');
            gen.writeStartObject();
            gen.writeFieldName('REQUEST_REC_TYPE');
            gen.writeStartArray();
            gen.writeStartObject();
                  if(objCase.SR_Booking_Units__r != null && !objCase.SR_Booking_Units__r.isEmpty()) {
                     for(SR_Booking_Unit__c objSRBU: objCase.SR_Booking_Units__r) {
                            gen.writeStringField('PARAM_ID', String.isNotBlank(objSRBU.Booking_Unit__r.Registration_Id__c)?objSRBU.Booking_Unit__r.Registration_Id__c:'');
                            gen.writeStringField('ATTRIBUTE1', String.isNotBlank(objSRBU.Booking_Unit__r.Inventory__r.Property_ID__c)?objSRBU.Booking_Unit__r.Inventory__r.Property_ID__c:'');
                            gen.writeStringField('ATTRIBUTE2', String.isNotBlank(objSRBU.Booking_Unit__r.Customer_Number__c)?objSRBU.Booking_Unit__r.Customer_Number__c:'');
                      }
                    }
                    else {
                        gen.writeStringField('PARAM_ID', '');
                        gen.writeStringField('ATTRIBUTE1','');
                        gen.writeStringField('ATTRIBUTE2','');
                    }
                    gen.writeStringField('ATTRIBUTE3',String.isNotBlank(String.valueOf(objCase.Refund_Amount__c))?String.valueOf(objCase.Refund_Amount__c):'');
                    gen.writeStringField('ATTRIBUTE4', String.isNotBlank(objCase.Description)? objCase.Description :'');
                    gen.writeStringField('ATTRIBUTE5', String.isNotBlank(objCase.Receipt_Id__c)?objCase.Receipt_Id__c:'');
                    gen.writeStringField('ATTRIBUTE6', String.isNotBlank(objCase.CaseNumber)?objCase.CaseNumber:'');
                    if(objCaseExtension != Null) {
                        gen.writeStringField('ATTRIBUTE7', String.isNotBlank(objCaseExtension.Beneficiary_Name__c)?objCaseExtension.Beneficiary_Name__c:'');
                        gen.writeStringField('ATTRIBUTE8', String.isNotBlank(objCaseExtension.Beneficiary_Add__c)?objCaseExtension.Beneficiary_Add__c:'');
                        gen.writeStringField('ATTRIBUTE9', String.isNotBlank(objCaseExtension.Beneficiary_Bank_Name__c)?objCaseExtension.Beneficiary_Bank_Name__c:'');
                        gen.writeStringField('ATTRIBUTE10', String.isNotBlank(objCaseExtension.Beneficiary_Bank_Branch__c)?objCaseExtension.Beneficiary_Bank_Branch__c:'');
                        gen.writeStringField('ATTRIBUTE11', String.isNotBlank(objCaseExtension.Beneficiary_Branch_Country__c)?objCaseExtension.Beneficiary_Branch_Country__c:'');
                        gen.writeStringField('ATTRIBUTE12', String.isNotBlank(objCaseExtension.Beneficiary_Account_Name__c)?objCaseExtension.Beneficiary_Account_Name__c:'');
                        gen.writeStringField('ATTRIBUTE13', String.isNotBlank(objCaseExtension.Beneficiary_Account_Number__c)?objCaseExtension.Beneficiary_Account_Number__c:'');
                        gen.writeStringField('ATTRIBUTE14', String.isNotBlank(objCaseExtension.Beneficiary_IBAN__c)?objCaseExtension.Beneficiary_IBAN__c:'');
                        gen.writeStringField('ATTRIBUTE15', String.isNotBlank(objCaseExtension.Swift_Code__c)?objCaseExtension.Swift_Code__c:'');
                        gen.writeStringField('ATTRIBUTE16', String.isNotBlank(objCaseExtension.Sort_Code__c)?objCaseExtension.Sort_Code__c:'');
                        gen.writeStringField('ATTRIBUTE16', String.isNotBlank(objCaseExtension.IFSC__c)?objCaseExtension.IFSC__c:'');
                        gen.writeStringField('ATTRIBUTE18', String.isNotBlank(objCaseExtension.Supp_Corr_Bank_Name__c)?objCaseExtension.Supp_Corr_Bank_Name__c:'');
                        gen.writeStringField('ATTRIBUTE19', String.isNotBlank(objCaseExtension.Supp_Corr_Bank_Branch_add1__c)?objCaseExtension.Supp_Corr_Bank_Branch_add1__c:'');
                        gen.writeStringField('ATTRIBUTE20', String.isNotBlank(objCaseExtension.Supp_Corr_Bank_Swift_Code__c)?objCaseExtension.Supp_Corr_Bank_Swift_Code__c:'');
                    }
            gen.writeEndObject();
            gen.writeEndArray();
            gen.writeEndObject();
            gen.writeEndObject();
            gen.writeEndObject();
            String requestBody = gen.getAsString();
            
          
          return requestBody;
    }
    
    public class OutputParameters {
        public String appxmlns;
        public String appxmlnsxsi;
        public X_RESPONSE_MESSAGE X_RESPONSE_MESSAGE;
        public String X_RETURN_STATUS;
        public String X_RETURN_MESSAGE;
    }
    
    public class TaskCreationIPMSResponse { 
        public OutputParameters OutputParameters;
    }
    
    public class X_RESPONSE_MESSAGE_ITEM {
        public String PARAM_ID;
        public String PROC_STATUS;
        public String PROC_MESSAGE;
        public String ATTRIBUTE1;
        public String ATTRIBUTE2;
        public String ATTRIBUTE3;
        public String ATTRIBUTE4;
    }
    
    public class X_RESPONSE_MESSAGE {
        public List<X_RESPONSE_MESSAGE_ITEM> X_RESPONSE_MESSAGE_ITEM;
    }
}