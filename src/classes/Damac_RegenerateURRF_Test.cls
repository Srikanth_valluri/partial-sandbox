@isTest
public class Damac_RegenerateURRF_Test{
    private static final Id DEAL_SR_RECORD_TYPE_ID = DamacUtility.getRecordTypeId('NSIBPM__Service_Request__c', 'Deal');
    testmethod static void regenerateURRF(){
        Account acc = new Account ();
        acc.LastName = 'test';
        acc.Agency_Type__c = 'Corporate';
        acc.Vendor_ID__c = '767676';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        insert acc;
        NSIBPM__Service_Request__c serviceRequest = new NSIBPM__Service_Request__c();
        serviceRequest.DP_ok__c = true;
        serviceRequest.Doc_ok__c = true;
        serviceRequest.Agency__c = acc.Id;
        serviceRequest.Registration_Date__c = Date.newInstance(2017,5,25);
        serviceRequest.NSIBPM__Customer__c = acc.Id;
        serviceRequest.RecordTypeId = DEAL_SR_RECORD_TYPE_ID;
        insert serviceRequest; 
    
        Form_Request__c form = new Form_Request__c();
        form.Service_request__c = serviceRequest.Id;
        insert form;
        Form_Request_Inventories__c formInv = new Form_Request_Inventories__c();
        formInv.Form_Request__c = form.id;
        insert formInv;
        
        Test.startTest();
            Damac_RegenerateURRF.generateURRF(form.id,serviceRequest.id);
        Test.stopTest();
    }
}