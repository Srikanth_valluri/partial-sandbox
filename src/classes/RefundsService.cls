/* Class used for performing callouts related to Refunds process
 */
public with sharing class RefundsService {
    
    //Method to perform callout to fetch token amount
    public static RefundResponse getTokenRefund(String strRegId) {
        Refunds.CustomerFundAndTokenHttpSoap11Endpoint objReq = new Refunds.CustomerFundAndTokenHttpSoap11Endpoint();
        objReq.timeout_x = 120000;
        
        try {
            System.debug('--strRegId---'+strRegId);
            List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
            String strResponse = objReq.getTokenRefunds('2-'+strRegId, 'TOKEN_REF_AMOUNTS', 'SFDC', strRegId);
            System.debug('--strResponse ---'+strResponse );
            Map<String, Object> mapResponse = (Map<String, Object>)JSON.deserializeUntyped(strResponse);
            if(String.isNotBlank(strResponse)) {
                
                if(mapResponse.get('Status') != null) {
                    String strStatus = (String)mapResponse.get('Status');
                    if(String.isNotBlank(strStatus) && strStatus.equalsIgnoreCase('S')) { //&&
                       /*((mapResponse.get('Amount_Paid') != null && mapResponse.get('Amount_Paid') != 'null') ||
                       (mapResponse.get('Amount_COCA') != null && mapResponse.get('Amount_COCA') != 'null'))) {*/
                        System.debug('--mapResponse Amount_Paid--'+mapResponse.get('Amount_Paid'));
                        System.debug('--mapResponse Amount_COCA--'+mapResponse.get('Amount_COCA'));
                        //Decimal.valueOf(String.valueOf(mapResponse.get('Excess_Amount')));
                        Decimal decAmount = 0;
                        decAmount += mapResponse.get('Amount_COCA') != null ? Decimal.valueOf((String)mapResponse.get('Amount_COCA')) : 0;
                        decAmount += mapResponse.get('Amount_Paid') != null ? Decimal.valueOf((String)mapResponse.get('Amount_Paid')) : 0;
                        System.debug('--decAmount --'+decAmount );
                        return new RefundResponse('Success', decAmount, null);
                    }
                    else {
                        String strMessage = mapResponse.get('Message') != null ? (String)mapResponse.get('Message') : null;
                        /*if(mapResponse.get('Message') != null) {
                            lstErrorLog.add(new Error_Log__c(Error_Details__c = (String)mapResponse.get('Message')));
                            insert lstErrorLog;
                        }*/
                        return new RefundResponse('Error', null, strMessage);
                    }
                }
            }
            else {
                return new RefundResponse('Error', null, null);
            }
        }
        catch(Exception excGen) {
            return new RefundResponse('Error', null, excGen.getStackTraceString());
        }
        return null;
    }
    
    //Method to perform callout to fetch excess amount
    public static RefundResponse getExcessAmount(String strRegId) {
        Refunds.CustomerFundAndTokenHttpSoap11Endpoint objReq = new Refunds.CustomerFundAndTokenHttpSoap11Endpoint();
        objReq.timeout_x = 120000;
        
        try {
            List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
            String strResponse = objReq.getExcessAmount('2-'+strRegId, 'GET_EXCESS_AMOUNT', 'SFDC', strRegId);
            System.debug('--strResponse getExcessAmount---'+strResponse );
            Map<String, Object> mapResponse = (Map<String, Object>)JSON.deserializeUntyped(strResponse);
            if(String.isNotBlank(strResponse)) {
                
                if(mapResponse.get('Status') != null) {
                    String strStatus = (String)mapResponse.get('Status');
                    if(String.isNotBlank(strStatus) && strStatus.equalsIgnoreCase('S') &&
                       mapResponse.get('Excess_Amount') != null) {
                        Decimal decAmount = Decimal.valueOf((String)mapResponse.get('Excess_Amount'));
                        //Decimal decAmount = Decimal.valueOf(String.valueOf(mapResponse.get('Excess_Amount')));
                        return new RefundResponse('Success', decAmount, null);
                    }
                    else {
                        String strMessage = mapResponse.get('Message') != null ? (String)mapResponse.get('Message') : null;
                        /*if(mapResponse.get('Message') != null) {
                            lstErrorLog.add(new Error_Log__c(Error_Details__c = (String)mapResponse.get('Message')));
                            insert lstErrorLog;
                        }*/
                        return new RefundResponse('Error', null, strMessage);
                    }
                }
            }
            else {
                return new RefundResponse('Error', null, null);
            }
        }
        catch(Exception excGen) {
            return new RefundResponse('Error', null, excGen.getStackTraceString());
        }
        return null;
    }
    
    //Method to perform callout to rule engine and fetch approving authorities 
    public static RefundRuleResponse getApprovingAuthorities(String strRegId, String strProcessName, String strSubProcessName,
        String strProjectCity, String strProject, String strBuildingCode, String strBedroomType, String strUnitType,
        String strPermittedUse, String strUnits, String strReadyOffPlan, String strEHOFlag, String strHOFlag, String strNationality,
        String strCustClassification, String strPortfolioValue, String strRefundValue) {
        
        RefundsRule.TokenRefundsTransfersRuleHttpSoap11Endpoint objReq = new RefundsRule.TokenRefundsTransfersRuleHttpSoap11Endpoint();
        objReq.timeout_x = 120000;
        
        System.debug('--strRegId--'+strRegId);
        System.debug('--strProcessName--'+strProcessName);
        System.debug('--strSubProcessName--'+strSubProcessName);
        System.debug('--strProjectCity--'+strProjectCity);
        System.debug('--strProject--'+strProject);
        System.debug('--strBuildingCode--'+strBuildingCode);
        System.debug('--strBedroomType--'+strBedroomType);
        System.debug('--strUnitType--'+strUnitType);
        System.debug('--strPermittedUse--'+strPermittedUse);
        System.debug('--strUnits--'+strUnits);
        System.debug('--strReadyOffPlan--'+strReadyOffPlan);
        System.debug('--strEHOFlag--'+strEHOFlag);
        System.debug('--strHOFlag--'+strHOFlag);
        System.debug('--strNationality--'+strNationality);
        System.debug('--strCustClassification--'+strCustClassification);
        System.debug('--strPortfolioValue--'+strPortfolioValue);
        System.debug('--strRefundValue--'+strRefundValue);
        
        try {
            String strResponse = objReq.TokenRefundsTransfers(strRegId, strProcessName, strSubProcessName, strProjectCity,
                strProject, strBuildingCode, strBedroomType, strUnitType, strPermittedUse, strUnits, strReadyOffPlan, strEHOFlag, 
                strHOFlag, strNationality, strCustClassification, strPortfolioValue, strRefundValue, '', '', '', '', '', '', '', '');
            System.debug('--strResponse--'+strResponse);    
            if(String.isNotBlank(strResponse)) {
                Map<String, Object> mapResponse = (Map<String, Object>)JSON.deserializeUntyped(strResponse);
                if(mapResponse.get('allowed') != null) {
                    String strAllowed = (String)mapResponse.get('allowed');
                    if(String.isNotBlank(strAllowed) && strAllowed.equalsIgnoreCase('Yes')) {
                        String strAppAuth = '';
                        
                        //strAppAuth = RoleUtility.getSalesforceRole(strAppAuth);
                        
                        String strRecAuth1 = (String)mapResponse.get('recommendingAuthorityOne');
                        String strRecAuth2 = (String)mapResponse.get('recommendingAuthorityTwo');
                        String strRecAuth3 = (String)mapResponse.get('recommendingAuthorityThree');
                        String strRecAuth4 = (String)mapResponse.get('recommendingAuthorityFour');
                        String strAppAuth1 = (String)mapResponse.get('approvingAuthorityOne');
                        String strAppAuth2 = (String)mapResponse.get('approvingAuthorityTwo');
                        String strAppAuth3 = (String)mapResponse.get('approvingAuthorityThree');
                        
                        if(String.isNotBlank(strRecAuth1)) {
                            //strRecAuth1 = RoleUtility.getSalesforceRole(strRecAuth1);
                            strAppAuth = strRecAuth1 + ',';
                        }
                        
                        if(String.isNotBlank(strRecAuth2)) {
                            //strRecAuth2 = RoleUtility.getSalesforceRole(strRecAuth2);
                            strAppAuth += strRecAuth2 + ',';
                        }
                        
                        if(String.isNotBlank(strRecAuth3)) {
                            //strRecAuth3 = RoleUtility.getSalesforceRole(strRecAuth3);
                            strAppAuth += strRecAuth3 + ',';
                        }
                        
                        if(String.isNotBlank(strRecAuth4)) {
                            //strRecAuth4 = RoleUtility.getSalesforceRole(strRecAuth4);
                            strAppAuth += strRecAuth4 + ',';
                        }
                        if(String.isNotBlank(strAppAuth1)) {
                            //strAppAuth1 = RoleUtility.getSalesforceRole(strAppAuth1);
                            strAppAuth += strAppAuth1 + ',';
                        }
                        
                        if(String.isNotBlank(strAppAuth2)) {
                            //strAppAuth2 = RoleUtility.getSalesforceRole(strAppAuth2);
                            strAppAuth += strAppAuth2 + ',';
                        }
                        
                        if(String.isNotBlank(strAppAuth3)) {
                            //strAppAuth3 = RoleUtility.getSalesforceRole(strAppAuth3);
                            strAppAuth += strAppAuth3 + ',';
                        }
                        
                        strAppAuth = strAppAuth.removeEnd(',');
                        strAppAuth = strAppAuth.replace('CRM-', '');
                        
                        String strMessage = mapResponse.get('message') != null ? (String)mapResponse.get('message') : '';
                        return new RefundRuleResponse(strAllowed, strMessage, strAppAuth);
                    }
                    else if(String.isNotBlank(strAllowed) && strAllowed.equalsIgnoreCase('No')) {
                        String strMessage = (String)mapResponse.get('message');
                        return new RefundRuleResponse('No', strMessage, null);
                    }
                    else {
                        return new RefundRuleResponse('Error', null, null);
                    }
                }
            }
            else {
                return new RefundRuleResponse('Error', null, null);
            }
        }
        catch(Exception excGen) {
            System.debug('--excGen.getMessage()---'+excGen.getMessage());
            return new RefundRuleResponse('Error', excGen.getMessage(), null);
        }
        return null;
    }
    
    public Class RefundResponse {
        public String strStatus;
        public Decimal decAmount;
        public String strMessage;
        
        public RefundResponse(String strStatus, Decimal decAmount,String strMessage) {
            this.strStatus = strStatus;
            this.decAmount = decAmount;
            this.strMessage = strMessage;
        }
    }
    
    public Class RefundRuleResponse {
        public String strAllowed;
        public String strMessage;
        public String strApprovingAuthority;
        
        public RefundRuleResponse(String strAllowed, String strMessage, String strApprovingAuthority) {
            this.strAllowed = strAllowed;
            this.strMessage = strMessage;
            this.strApprovingAuthority = strApprovingAuthority;
        }
    }
}