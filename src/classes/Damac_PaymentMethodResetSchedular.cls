/**************************************************************************************************
* Name               : Damac_PaymentMethodResetSchedular                                          *
* Description        : Scheduler class to reset the payment method values in the metadata         *
* Created Date       : 15/12/2020                                                                 *
* Created By         : Srikanth V                                                                 *
* ----------------------------------------------------------------------------------------------- *
* VERSION     AUTHOR            DATE            COMMENTS                                          *
* 1.0         Srikanth V     15/12/2020      Initial Draft.                                       *
**************************************************************************************************/
public class Damac_PaymentMethodResetSchedular implements Schedulable {
    public void execute(SchedulableContext SC) {
        
        
        Damac_paymentMethodResethelper.resetTimeOut ();
        
    }
}