/************************************************************************************************************************
* Name               : AP_FileUploadControllerTest
* Description        : Test Class for AP_FileUploadController
* Author             : QBurst    
* Date               : 13/05/2020      
*************************************************************************************************************************
* VERSION   AUTHOR          DATE            COMMENTS                
                                                                          
                                             
* 1.0                  
*************************************************************************************************************************/


@IsTest
public with sharing class AP_FileUploadControllerTest {
@IsTest
    public static void  testmethod1(){
        Account acc = new Account(
                      Name = 'Test Corporate');
        insert acc;
        Contact con = new Contact(
                        FirstName = 'John',
                        LastName = 'Doe',
                        AccountId = acc.Id
                    );
        insert con;
        Campaign__c c = new Campaign__c(
                        
                        Campaign_Name__c = 'MPD.RS-AgntEvt-ZMB-Lus-Jun27.RS-AgntEvt-Wlkin-Nov20',
                        start_Date__c = system.today().addDays(-1),
                        End_Date__c = system.today().addDays(1),
                        Marketing_Start_Date__c = system.today().addDays(-1),
                        Marketing_End_Date__c = system.today().addDays(1),
                        RecordTypeID = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows').getRecordTypeId(),
                        Marketing_Active__c = TRUE,
                        Credit_Control_Active__c = TRUE,
                        Sales_Admin_Active__c = TRUE,
                        Eligible_for_Agent_Portal__c =true
                        
                        );
        insert c;
       
        Assigned_Agent__c ac = new Assigned_Agent__c(
            
                               Campaign__c = c.Id,
                               User__c = UserInfo.getUserId(),
                               Agency__c = acc.Id,
                               Contact__c = con.Id
            
                             );
        insert ac;
        
        User RMUser = new User ();
        RMUser.ProfileId = [SELECT ID FROM Profile where Name LIKE '%Property Consultant%' LIMIT 1].ID;
        RMUser.UserName = 'testEmail@testclass.com';
        RMUser.Email = 'testEmail@testclass.com';
        RMUser.Alias = 'tCE';
        RMUser.EmailEncodingKey='UTF-8';
        RMUser.LastName='Testing';
        RMUser.LanguageLocaleKey='en_US';
        RMUser.LocaleSidKey='en_US';
        RMUser.TimeZoneSidKey='America/Los_Angeles';
        RMUser.IsActive =true;
        insert RMUser;
        
        Bulk_Upload_Request__c b = new Bulk_Upload_Request__c();
        b.Agency__c = acc.Id;
        b.Agent__c = con.Id;
        b.Comments__c = 'test';
        b.RM_Email__c = 'ashim.das@damacgroup.com';
        b.Status__c = 'Submitted';
        insert b;
        
        AP_FileUploadController fc = new AP_FileUploadController();
        apexpages.currentpage().getparameters().put('langCode','en_US');
        fc.agencyName = c.Agency_Name__c;
        fc.agentName = ac.Contact__r.Name;   
        fc.marketingCampaign = c.Id;
        fc.strSelectedLanguage = 'en_US';
        apexpages.currentpage().getparameters().put('langCode','en_US');
        fc.filename = 'testfile.xls';
        fc.conType = 'application/vnd.ms-excel';
        fc.file = blob.valueof('Unit.test');
        string xyz = fc.xlsHeader;
 
        fc.getMarketingCampaigns();
        fc.validateRM(RMUser.Email);
        fc.validateRMEmail();

        PageReference pageRef = Page.AP_BulkUploadAlter;
        Test.setCurrentPage(pageRef);
        fc.sendDocAttach();
    }
    
    
    @IsTest
    public static void  testmethod2(){
        Account acc = new Account(
                      Name = 'Test Corporate');
        insert acc;
        Contact con = new Contact(
                        FirstName = 'John',
                        LastName = 'Doe',
                        AccountId = acc.Id
                    );
        insert con;
        Campaign__c c = new Campaign__c(
                        
                        Campaign_Name__c = 'MPD.RS-AgntEvt-ZMB-Lus-Jun27.RS-AgntEvt-Wlkin-Nov20',
                        start_Date__c = system.today().addDays(-1),
                        End_Date__c = system.today().addDays(1),
                        Marketing_Start_Date__c = system.today().addDays(-1),
                        Marketing_End_Date__c = system.today().addDays(1),
                        RecordTypeID = Schema.SObjectType.Campaign__c.getRecordTypeInfosByName().get('Roadshows').getRecordTypeId(),
                        Marketing_Active__c = TRUE,
                        Credit_Control_Active__c = TRUE,
                        Sales_Admin_Active__c = TRUE,
                        Eligible_for_Agent_Portal__c =true
                        
                        );
        insert c;
       
        Assigned_Agent__c ac = new Assigned_Agent__c(
            
                               Campaign__c = c.Id,
                               User__c = UserInfo.getUserId(),
                               Agency__c = acc.Id,
                               Contact__c = con.Id
            
                             );
        insert ac;
        
        User RMUser = new User ();
        RMUser.ProfileId = [SELECT ID FROM Profile where Name LIKE '%system administrator%' LIMIT 1].ID;
        RMUser.UserName = 'testEmail@testclass.com';
        RMUser.Email = 'testEmail@testclass.com';
        RMUser.Alias = 'tCE';
        RMUser.EmailEncodingKey='UTF-8';
        RMUser.LastName='Testing';
        RMUser.LanguageLocaleKey='en_US';
        RMUser.LocaleSidKey='en_US';
        RMUser.TimeZoneSidKey='America/Los_Angeles';
        RMUser.IsActive =true;
        insert RMUser;
        
        Bulk_Upload_Request__c b = new Bulk_Upload_Request__c();
        b.Agency__c = acc.Id;
        b.Agent__c = con.Id;
        b.Marketing_Campaign__c = c.Id;
        b.Comments__c = 'test';
        b.RM_Email__c = 'ashim.das@damacgroup.com';
        b.Status__c = 'Submitted';
        insert b;
        
        AP_FileUploadController fc = new AP_FileUploadController();
        apexpages.currentpage().getparameters().put('langCode',NULL);
        fc.agencyName = c.Agency_Name__c;
        fc.agentName = ac.Contact__r.Name;   
        fc.marketingCampaign = c.Id;
               
        fc.filename = '';
        fc.conType = 'application/vnd.ms-excel';
        fc.file = blob.valueof('Unit.test');
        string xyz = fc.xlsHeader;
        
        fc.getMarketingCampaigns();
        fc.validateRM(RMUser.Email);
        fc.validateRMEmail();

        PageReference pageRef = Page.AP_BulkUploadAlter;
        Test.setCurrentPage(pageRef);
        fc.sendDocAttach();
    }


}