/***********************************************************************************
* Description - Test class developed for CallHandoverMQServices
*
* Version            Date            Author                    Description
* 1.0                14/12/2017      Ashish Agarwal             Initial Draft
*************************************************************************************/
@isTest
private class CallHandoverMQServicesTest {

    static testMethod void testCallHandoverMQServiceName() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        Case objCase = new Case();
        objCase.Status = 'New';
        objCase.Origin = 'Email';
        objCase.Booking_Unit__c = lstBookingUnits[0].Id;
        insert objCase;
        
        test.startTest();
            SOAPCalloutServiceMock.returnToMe = new Map<String, HandoverMQServices.generatePCCResponse_element>();
            HandoverMQServices.generatePCCResponse_element response1 = new HandoverMQServices.generatePCCResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            CallHandoverMQServices.CallHandoverMQServiceName( lstBookingUnits[0].Registration_Id__c, objCase.Id);
        test.stopTest();
        
    }
    
    static testMethod void testCallHandoverMQServiceKeyReleaseForm() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        test.startTest();
            SOAPCalloutServiceMock.returnToMe = new Map<String, HandoverMQServices.GenerateKeyReleaseFormResponse_element>();
            HandoverMQServices.GenerateKeyReleaseFormResponse_element response1 = new HandoverMQServices.GenerateKeyReleaseFormResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            CallHandoverMQServices.CallHandoverMQServiceKeyReleaseForm( lstBookingUnits[0].Registration_Id__c );
        test.stopTest();
    }
    
    static testMethod void testCallHandoverMQServiceLetterofDischarge() {
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest();
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c> lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 5 );
        insert lstBookings ;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings, 5 );
        for( Booking_Unit__c objUnit : lstBookingUnits ) {
           objUnit.Registration_status__c = 'Agreement executed by DAMAC'; 
        }
        insert lstBookingUnits;
        
        test.startTest();
            SOAPCalloutServiceMock.returnToMe = new Map<String, HandoverMQServices.LetterofDischargeResponse_element>();
            HandoverMQServices.LetterofDischargeResponse_element response1 = new HandoverMQServices.LetterofDischargeResponse_element();
            response1.return_x = '';
            SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
            Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock() );
            CallHandoverMQServices.CallHandoverMQServiceLetterofDischarge( lstBookingUnits[0].Registration_Id__c );
        test.stopTest();
        
    }
}