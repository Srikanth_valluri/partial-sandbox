@isTest
private class CreatePortalUserTest {

    static testMethod void test_createuser1 () {
        Id profileId = [select id from profile where name = 'Customer Community Login User(Use this)'].id;

        Account objAccount = CommunityTestDataFactory.CreatePersonAccount();
        objAccount.Email__pc = 'test@user.com';
        objAccount.PersonEmail = '';
        insert objAccount;
        
        Account acc = [Select Party_ID__c,PersonContactId From Account Where Id = :objAccount.Id];
         
        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = profileId, country='United States',IsActive =true,
                ContactId = acc.PersonContactId,Party_Id__c=acc.Party_ID__c,
                timezonesidkey='America/Los_Angeles', username='tester@noemail1.com');

        insert user;
        
        System.runAs(user) {
            Test.startTest();
                createportaluser.createuser(new List<String>{acc.Id});
            Test.stopTest();
        }     
    }
    
    static testMethod void test_createuser2 () {
        Id profileId = [select id from profile where name = 'Customer Community Login User(Use this)'].id;

        Account objAccount = CommunityTestDataFactory.CreatePersonAccount();
        objAccount.Email__pc = 'test@user.com';
        objAccount.PersonEmail = '';
        insert objAccount;
        
        Account acc = [Select Party_ID__c,PersonContactId From Account Where Id = :objAccount.Id];
         
        Test.startTest();
            createportaluser.createuser(new List<String>{acc.Id});
        Test.stopTest();
    }
}