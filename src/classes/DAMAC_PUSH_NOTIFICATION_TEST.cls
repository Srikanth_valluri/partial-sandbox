@istest
public class DAMAC_PUSH_NOTIFICATION_TEST{

    static testmethod void testNotify(){
        GoogleFirebase_credentials__c creds = new GoogleFirebase_credentials__c();
        creds.Endpoint__c = 'asdassa';
        creds.Server_Key__c = 'asdsadsa';
        insert creds;
        
        announcement__c ann = new announcement__c();
        ann.Activate_on_Mobile__c = true;
        ann.Start_Date__c = system.today();
        ann.End_Date__c = system.today().adddays(20);
        ann.Description__c = 'This is a Smple';
        ann.title__c = 'ttile';
        insert ann;
        
        announcement__c a = [select id from announcement__c where id=:ann.id limit 1];
        a.Push_Notification_to__c = 'All';
        update a;
        
        list<id> annId = new list<id>();
        annId.add(a.id);
        DAMAC_PUSH_NOTIFICATION.sendNotification(annid);
    }
    static testmethod void testPushNotify(){
        GoogleFirebase_credentials__c creds = new GoogleFirebase_credentials__c();
        creds.Endpoint__c = 'asdassa';
        creds.Server_Key__c = 'asdsadsa';
        insert creds;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
    	User u = new User(Alias = 'standt', Email='standarduserishere@testorg.com', 
        					EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        					LocaleSidKey='en_US', ProfileId = p.Id, Device_Token_ID__c = 'testDeviceTokenId',
        					TimeZoneSidKey='America/Los_Angeles', UserName='standarduserishere@testorg.com');
        insert u;

        Push_Notifications__c notification = new Push_Notifications__c();
		notification.Email__c = 'damac@testdamac.com';
		notification.Template_Id__c = 0;
		notification.Message__c = 'test';
		notification.Title__c = 'test';
		notification.detail_type__c = 'test';
		notification.Source_Application_Id__c = 10;
		notification.Dest_Application_Id__c = 6;
        notification.Firebase__c = true;
        notification.user__c = u.Id;        
        notification.Message__c = 'This is a Simple';
        notification.Notification_Type__c = 'SR Status Buyer Confirmation';
        notification.title__c = 'title';
        insert notification;

        /*
		Account acc = new Account ();
        acc.Agency_Type__c = 'Corporate';
        acc.LastName = 'Test Account';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Agency').getRecordTypeId();
        acc.Blacklisted__c = false;
        acc.Terminated__c = false;
        acc.Vendor_ID__c = '999988887777';
        insert acc;

        Location__c loc = InitializeSRDataTest.createLocation('123', 'Building');
        loc.property_id__c = '123';
        insert loc;

        Property__c newProperty = new Property__c();
        newProperty.Property_ID__c = 1;
        newProperty.Property_Code__c = 'VIR';
        newProperty.Property_Name__c = 'VIRIDIS @ AKOYA OXYGEN';
        newProperty.District__c = 'AL YUFRAH 2';
        newProperty.AR_Transaction_Type__c = 'INV VIR';
        newProperty.Penalty_Transaction_Type__c = 'DM-PENALTIES-VIR';
        newProperty.Brokerage_Distribution_Set__c = '11600';
        newProperty.Sales_Commission_Dist_Set__c = '11601';
        newProperty.Currency_Of_Sale__c = 'AED';
        newProperty.Signature_Col_Customer_Stmt__c = 'Front Line Investment Management Co. LLC';
        newProperty.EOI_Enabled__c = true;
        insert newProperty;

        List<Inventory__c> lstInv = new List<Inventory__c>();
        lstInv.add(InitializeSRDataTest.createInventory(loc.Id));
        lstInv[0].Property_Country__c = 'United Arab Emirates';
        lstInv[0].Property_City__c = 'Dubai';
        lstInv[0].Unit_Location__c = loc.Id;
        lstInv[0].building_location__c = loc.Id;
        lstInv[0].property_id__c = newProperty.Id;
        insert lstInv;
        
        Payment_Plan__c pp = new Payment_Plan__c();
        pp.Effective_From__c = system.today();
        pp.Effective_To__c = system.today(); 
        pp.Building_Location__c =  loc.Id;
        pp.term_id__c = '1234';
        insert pp;

        Payment_Terms__c pt = new Payment_Terms__c();
        pt.Payment_Plan__c = pp.Id;
        pt.Percent_Value__c = '5';
        insert pt;
        
        Agency_PC__c agencyPC = new Agency_PC__c();
        agencyPC.user__c = userinfo.getUserId();
        agencyPC.Agency__c = acc.id;
        insert agencyPC;
       
        
        DealExceptionRequest_Remote.getAgencyDetails ('test');        
        DealExceptionRequest_Remote.getCorporateAgents (acc.id);
        DealExceptionRequest_Remote.getInquiryDetails ('test');
        DealExceptionRequest_Remote.getAccountDetails (acc.LastName);
        String derId = DealExceptionRequest_Remote.createDealExpRequest(lstInv[0].Id);



		DAMAC_Central_Push_Notifications__c obj = new DAMAC_Central_Push_Notifications__c();
		obj.Email__c = 'test@test.com';
		obj.Password__c = '1232112';
		obj.device_source__c = 'test';
		obj.device_os_version__c = 'test';
		obj.app_version__c = '1.2';
		obj.device_model__c = 'test';
		obj.Api_Token__c = 'test';
		obj.app_id__c = 1;
		obj.is_authorization_required__c = false;
		insert obj;

		Push_Notifications__c notifications = new Push_Notifications__c();
		notifications.Email__c = 'damac@testdamac.com';
		notifications.Template_Id__c = 0;
		notifications.Message__c = 'test';
		notifications.Title__c = 'test';
		notifications.detail_type__c = 'test';
		notifications.Source_Application_Id__c = 10;
		notifications.Dest_Application_Id__c = 6;
		notifications.Deal_Exception_Request__c = derId;
		insert notifications;
        
        list<id> pushId = new list<id>();
        pushId.add(notification.id);
        test.startTest();
        DAMAC_PUSH_NOTIFICATION.sendNotificationaAsync(pushId);
        test.stopTest();*/
	}


}