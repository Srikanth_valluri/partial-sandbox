@isTest
public class GetAllBuildingsForTenantReg_APITest {
    @isTest
    static void getPropertyTest(){
    
        NSIBPM__Service_Request__c objSR = new NSIBPM__Service_Request__c();
        insert objSR;
        
        Booking__c objBooking = new Booking__c();
        objBooking.Deal_SR__c = objSR.Id;
        insert objBooking;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        locObj.Building_Name__c = 'test';
        locObj.Location_Type__c = 'Building';
        locObj.Location_ID__c = '12345';
        locObj.UAEProp__c = true;
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        invObj.Property_Country__c = 'UNITED ARAB EMIRATES';
        invObj.Marketing_Name_Doc__c = invObj.Id;
        invObj.Building_Location__c = locObj.Id;
        invObj.Property_Status__c = 'Ready'; 
        insert invObj;
        
        Booking_Unit__c objBU = new Booking_Unit__c();
        objBU.DLP_End_Date__c = Date.today().addDays(2);
        objBU.Registration_ID__c = '3901';
        objBU.Booking__c = objBooking.Id;
        objBU.Inventory__c = invObj.Id;
        objBU.Unit_Name__c='VRD/SD698/XR1198B';
        objBU.Dummy_Booking_Unit__c = true;
        objBU.Registration_Status_Code__c = 'LE';
        insert objBU;
        system.debug('objBU==='+objBU);
       
       
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/getAllBuildings';  
        
        req.httpMethod = 'POST';
        req.addParameter('buildingId', '12345');
        RestContext.request = req;
        RestContext.response = res;
        
        GetAllBuildingsForTenantReg_API.getProperty();
    }
}