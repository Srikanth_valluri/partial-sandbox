@RestResource(urlMapping='/wplookups/*')
global  class WPLookupAPI {
    public static final String commonErrorMsg = 'This may be because of technical error that we\'re working to get fixed. Please try again later.';
    public static Integer statusCode;
    public static final String securityChequeDepositAmountMessage = 'Security Deposit Cheque for each Unit.\nCheque to be made in favor of Luxury facilities Management Co. LLC. - Without Date.';
    public static final String temporaryPowerConnectionAmountMessage = 'By Cheque for temporary power and water connection for each unit monthly.';
    public static final String garbageDisposalAmountMessage = 'By Cheque for garbage disposal for each unit (monthly and Excluding the constructional waste).';
    public static final String srType = 'Apply_for_Work_Permits';
    
    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };
    
    @HttpGet
    global static FinalReturnWrapper getWPLookups() {
        
        RestRequest r = RestContext.request;
        System.debug('Request Headers:'+r.headers);
        System.debug('Request params:'+r.params);
        
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        cls_meta_data objMeta = new cls_meta_data();
        cls_data objData = new cls_data();
        
        if(!r.params.containsKey('accountId') || (r.params.containsKey('accountId') && String.isBlank(r.params.get('accountId')))) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'No account_id found in request',3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(!r.params.containsKey('bookingUnitId') || (r.params.containsKey('bookingUnitId') && String.isBlank(r.params.get('bookingUnitId')))) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'No bookingUnitId found in request',3);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        
        List<String> listPersonToCollect = new List<String>();
        List<String> listPermitToWorkFor = new List<String>();
        List<String> listContractorType = new List<String>();
        List<String> listNumberOfEmployees = new List<String>();
        
        List<WorkPermitTypeWrapper> listWorkPermitTypeWrapper = new List<WorkPermitTypeWrapper>();
        
        try {
            String accountId = r.params.get('accountId');
            String bookingUnitId = r.params.get('bookingUnitId');
            
            listPersonToCollect = HDApp_Utility.getPicklistOptions('FM_Case__c','Person_To_Collect__c');
            listPermitToWorkFor = HDApp_Utility.getPicklistOptions('FM_Case__c','Permit_To_Work_For__c');
            listContractorType = HDApp_Utility.getPicklistOptions('FM_Case__c','Contractor_Type__c');
            listNumberOfEmployees = HDApp_Utility.getPicklistOptions('FM_Case__c','Number_of_Employees__c');
            
            List<Booking_Unit__c> listBookingUnit = HDApp_Utility.getUnitsFromBUId(bookingUnitId);	//add error if BU not found
            
            Map<String,List<FM_Documents__mdt>> mapDocumentsMetaData = new Map<String,List<FM_Documents__mdt>>();
            
            for(FM_Documents__mdt objDocMeta : FM_Utility.getDocumentsList(srType,listBookingUnit[0].Property_City__c )) {
                if(mapDocumentsMetaData.containsKey(objDocMeta.NOC_Type__c)) {
                    mapDocumentsMetaData.get(objDocMeta.NOC_Type__c).add(objDocMeta);
                }
                else {
                    mapDocumentsMetaData.put(objDocMeta.NOC_Type__c,new List<FM_Documents__mdt>{objDocMeta});
            	}
            }
            
            List<String> listWorkPermitType = HDApp_Utility.getPicklistOptions('FM_Case__c','Work_Permit_Type__c');
            Map<String,List<DocumentWrapper>> mapDocumentWrapper = new Map<String,List<DocumentWrapper>>();
            
           	for(String strWorkPermitType : listWorkPermitType) {
                for(String key : mapDocumentsMetaData.keyset()) {
                    if(key.contains(strWorkPermitType)) {
                        if(mapDocumentWrapper.containsKey(strWorkPermitType)) {
                            mapDocumentWrapper.get(strWorkPermitType).addAll(getDocumentWrapperList(mapDocumentsMetaData.get(key),mapDocumentWrapper.get(strWorkPermitType).size()+1));
                        }
                        else {
                            mapDocumentWrapper.put(strWorkPermitType,getDocumentWrapperList(mapDocumentsMetaData.get(key),1));
            			}
                    }
                }
            }
            
            Integer count = 1;
            
            for(String strWorkPermitType : listWorkPermitType) {
                WorkPermitTypeWrapper objWorkPermitTypeWrapper = new WorkPermitTypeWrapper();
                objWorkPermitTypeWrapper.type_name = strWorkPermitType;
                objWorkPermitTypeWrapper.type_id = String.valueOf(count);
                
                List<ApplicableDataWrapper> listApplicableDataWrapper = new List<ApplicableDataWrapper>();
                ApplicableDataWrapper objApplicableDataWrapper = new ApplicableDataWrapper();
                
                //Amount to be paid - only for WP with Fit Out
                List<AmountToBePaidWrapper> listAmountToBePaidWrapper = new List<AmountToBePaidWrapper>();
                //List of closed NOC SRs - only for WP with Fit Out
                List<NocSRWrapper> listNocSRWrapper = new List<NocSRWrapper>();
                
                if(strWorkPermitType.equals('Work Permit - Including Fit Out')) {
                    listAmountToBePaidWrapper = getAmountToBePaidWrapperList(listBookingUnit[0]);
                    listNocSRWrapper = getNocSRList(accountId);
                }
                
                objApplicableDataWrapper.amount_to_be_paid = listAmountToBePaidWrapper;
                objApplicableDataWrapper.noc_sr_list = listNocSRWrapper;
                
                //Documents required
                List<DocumentWrapper> listDocumentWrapper = new List<DocumentWrapper>();
                
                if(mapDocumentWrapper.containsKey(strWorkPermitType)) {
                    listDocumentWrapper = mapDocumentWrapper.get(strWorkPermitType);
                }
                
                objApplicableDataWrapper.documents_required = listDocumentWrapper;
                
                listApplicableDataWrapper.add(objApplicableDataWrapper);
                objWorkPermitTypeWrapper.applicable_data = listApplicableDataWrapper;
                
                listWorkPermitTypeWrapper.add(objWorkPermitTypeWrapper);
                
                count++;
            }
            
            statusCode = 1;
        }
        catch(Exception e) {
            System.debug('Exception:'+e.getMessage());
            
            statusCode = 6;
        }
        
        if(statusCode == 1) {
            objMeta = ReturnMetaResponse('Successful','', 1);
            
            objData.person_to_collect = listPersonToCollect;
            objData.permit_to_work = listPermitToWorkFor;
            objData.contractor_type = listContractorType;
            objData.number_of_employees = listNumberOfEmployees;
            objData.work_permit_type = listWorkPermitTypeWrapper;
        }
        else {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Error in fetching lookup data',6);
        }
        
        System.debug('objData:'+objData);
        
        returnResponse.data = objData;
        returnResponse.meta_data = objMeta;
        
        System.debug('returnResponse:'+returnResponse);
        
        return returnResponse;
    }
  	
    public static List<AmountToBePaidWrapper> getAmountToBePaidWrapperList(Booking_Unit__c bookingUnit) {
        List<Location__c> listLocation = new List<Location__c>();
        
        if(bookingUnit.Inventory__r.Building_Location__c != NULL) {
            listLocation = [SELECT Property_name__r.Name,Security_Deposit_Cheque_Amount__c,Temporary_power_water_connection_Amount__c,Garbage_disposal_Amount__c
                            FROM Location__c
                            WHERE Id =: bookingUnit.Inventory__r.Building_Location__c];
        }
        
        System.debug('listLocation:'+listLocation);
        
        List<AmountToBePaidWrapper> listAmountToBePaidWrapper = new List<AmountToBePaidWrapper>();
        
        if(listLocation != NULL && !listLocation.isEmpty()) {
            String securityChequeDepositAmount = listLocation[0].Security_Deposit_Cheque_Amount__c != NULL ? String.valueof(listLocation[0].Security_Deposit_Cheque_Amount__c) : '';
            if(String.isNotBlank(securityChequeDepositAmount)) {
                listAmountToBePaidWrapper.add(getAmountToBePaidWrapperObj(securityChequeDepositAmount,securityChequeDepositAmountMessage));
            }
            
            String temporaryPowerConnectionAmount = listLocation[0].Temporary_power_water_connection_Amount__c!= NULL ? String.valueof(listLocation[0].Temporary_power_water_connection_Amount__c) : '';
            if(String.isNotBlank(temporaryPowerConnectionAmount)) {
                listAmountToBePaidWrapper.add(getAmountToBePaidWrapperObj(temporaryPowerConnectionAmount,temporaryPowerConnectionAmountMessage));
            }
            
            String garbageDisposalAmount = listLocation[0].Garbage_disposal_Amount__c!= NULL ? String.valueof(listLocation[0].Garbage_disposal_Amount__c) : '';
            if(String.isNotBlank(garbageDisposalAmount)) {
                listAmountToBePaidWrapper.add(getAmountToBePaidWrapperObj(garbageDisposalAmount,garbageDisposalAmountMessage));
            }
        }
        
        return listAmountToBePaidWrapper;
    }
   	
    public static AmountToBePaidWrapper getAmountToBePaidWrapperObj(String amount,String description) {
        AmountToBePaidWrapper objAmountToBePaidWrapper = new AmountToBePaidWrapper();
        objAmountToBePaidWrapper.amount = amount;
        objAmountToBePaidWrapper.description = description;
        
        return objAmountToBePaidWrapper;
    }
    
    public static List<NocSRWrapper> getNocSRList(String accountId) {
        List<NocSRWrapper> listNocSRWrapper = new List<NocSRWrapper>();
        
        //Need clarification for this based on person to collect and type of NOC and query conditions
        for(FM_Case__c objFMCase : [SELECT Id,Name,Closure_Datetime__c,Type_of_Alteration__c,Person_To_Collect__c,
                                    Contact_person__c,Email__c,Mobile_Country_Code__c,Mobile_no__c,
                                    Contact_person_contractor__c,Email_2__c,Mobile_Country_Code_2__c,Mobile_no_contractor__c 
                                    FROM FM_Case__c 
                                   	WHERE Account__c =: accountId 
                                   	AND Type_of_NOC__c = 'NOC for Fit-out' 
                                    AND Status__c = 'Closed']) {
            NocSRWrapper objNocSRWrapper = new NocSRWrapper();
            objNocSRWrapper.sr_number = objFMCase.Name;
           	objNocSRWrapper.sr_id = objFMCase.Id;
            objNocSRWrapper.close_date = objFMCase.Closure_Datetime__c.format('yyyy-MM-dd');
           	objNocSRWrapper.type_of_work = objFMCase.Type_of_Alteration__c;
            objNocSRWrapper.person_to_collect = objFMCase.Person_To_Collect__c;
            
            if(objFMCase.Person_To_Collect__c == 'Consultant') {
                objNocSRWrapper.contractor_name = objFMCase.Contact_person__c;
                objNocSRWrapper.contractor_email = objFMCase.Email__c;
                objNocSRWrapper.contractor_mobile_code = objFMCase.Mobile_Country_Code__c;
                objNocSRWrapper.contractor_mobile_number = objFMCase.Mobile_no__c;
            }
          	else {
                objNocSRWrapper.contractor_name = objFMCase.Contact_person_contractor__c;
                objNocSRWrapper.contractor_email = objFMCase.Email_2__c;
                objNocSRWrapper.contractor_mobile_code = objFMCase.Mobile_Country_Code_2__c;
                objNocSRWrapper.contractor_mobile_number = objFMCase.Mobile_no_contractor__c;
            }
            
           	listNocSRWrapper.add(objNocSRWrapper);
        }
        
        return listNocSRWrapper;
    }
    
    public static List<DocumentWrapper> getDocumentWrapperList(List<FM_Documents__mdt> listDocMeta,Integer documentId) {
        List<DocumentWrapper> listDocumentWrapper = new List<DocumentWrapper>();
       	
        for(FM_Documents__mdt objDocMeta : listDocMeta) {
            DocumentWrapper objDocumentWrapper = new DocumentWrapper();
            objDocumentWrapper.document_name = objDocMeta.Document_Name__c;
            objDocumentWrapper.document_id = documentId++;
            objDocumentWrapper.is_mandatory = objDocMeta.Mandatory__c;
            listDocumentWrapper.add(objDocumentWrapper);
        }
        
        return listDocumentWrapper;
    }
    
    public static cls_meta_data ReturnMetaResponse(String message, String devMessage, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data();
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = devMessage;
        return retMeta;
    }
    
    global class FinalReturnWrapper {
        public cls_data data;
        public cls_meta_data meta_data;
    }
	
    public class AmountToBePaidWrapper {
        public String amount;
        public String description;
    }
    
    public class NocSRWrapper {
        public String sr_number;
        public String sr_id;
        public String close_date;
        public String type_of_work;
        public String person_to_collect;
        public String contractor_name;
        public String contractor_email;
        public String contractor_mobile_code;
        public String contractor_mobile_number;
    }
    
    public class DocumentWrapper {
        public String document_name;
        public Integer document_id;
        public Boolean is_mandatory;
    }
    
    public class ApplicableDataWrapper {
        public List<AmountToBePaidWrapper> amount_to_be_paid;
        public List<NocSRWrapper> noc_sr_list;
        public List<DocumentWrapper> documents_required;
    }
    
    public class WorkPermitTypeWrapper {
        public String type_name;
        public String type_id;
        public List<ApplicableDataWrapper> applicable_data;
    }
    
    public class cls_data {
        public List<String> person_to_collect;
        public List<String> permit_to_work;
        public List<String> contractor_type;
        public List<String> number_of_employees;
        public WorkPermitTypeWrapper[] work_permit_type;
    }
  	
    public class cls_meta_data {
        public String message; 
        public Integer status_code;
        public String title; 
        public String developer_message; 
    }
}