/****************************************************************************************************
* Name          : GenerateInvoiceController                                                         *
* Description   : GenerateInvoice Page Controller                                                   *
* Created Date  : 28-08-2018                                                                        *
* Created By    : ESPL                                                                              *
* --------------------------------------------------------------------------------------------------*
* VER   AUTHOR              DATE            COMMENTS                                                *
* 1.0   Craig Lobo          28-08-2018      Initial Draft.                                          *
****************************************************************************************************/
public with sharing class GenerateInvoiceController {

    public String dealSRId;
    public String invoiceAmount                                                         {get; set;}
    public String invoiceRemarks                                                        {get; set;}
    public String invoiceAddress                                                        {get; set;}
    public String chequeDate                                                            {get; set;}
    private NSIBPM__Service_Request__c dealSRObj;

    public  GenerateInvoiceController(ApexPages.StandardController controller) {
        dealSRId = controller.getRecord().Id;
        invoiceAmount = '';
        invoiceAddress = '';
        invoiceRemarks = '';
        chequeDate = '';
    }

    public PageReference saveReceiptDetails() {
        saveAttachments();
        return null;

    }

    public void saveAttachments() {
            List<Attachment> lstAttachment = new List<Attachment>();
            PageReference invoicePage = Page.CommercialInvoiceChina;
            invoicePage.getparameters().put('srid', dealSRId);
            invoicePage.getparameters().put('add', invoiceAddress);
            invoicePage.getparameters().put('rmrk', invoiceRemarks);
            invoicePage.getparameters().put('date', chequeDate);
            invoicePage.getparameters().put('amount', invoiceAmount);
            Attachment attachmentObj = new Attachment();
            Blob invoiceBody;

            NSIBPM__Service_Request__c dealSR = [ SELECT Id, Name, Token_Amount_AED__c 
                                                    FROM NSIBPM__Service_Request__c 
                                                   WHERE Id = :dealSRId 
                                                   LIMIT 1
            ];

            String invNum = dealSR.Name;
            invNum = invNum.replace('-', '');
            invNum = invNum.replace('SR', '');
            String dateTimeStr = String.valueOf(System.now());
            dateTimeStr = dateTimeStr.replace(':', '');
            dateTimeStr = dateTimeStr.replace(' ', '');
            dateTimeStr = dateTimeStr.replace('-', '');

            try {
                invoiceBody = invoicePage.getContentAsPDF();
                System.debug('body should be fine');
            } catch (Exception e) {
                System.debug('in the catch block');
                invoiceBody = Blob.valueOf('Some Text');
            }

            attachmentObj.Body = invoiceBody;
            attachmentObj.Name = 'Invoice' + invNum + dateTimeStr + '.pdf';
            attachmentObj.IsPrivate = false;
            attachmentObj.ContentType='application/pdf';
            attachmentObj.ParentId = dealSRId;
            lstAttachment.add(attachmentObj);

            PageReference receiptPage = Page.ReceiptChina;
            Attachment attachmentObjForReceipt = new Attachment();
            receiptPage.getparameters().put('srid', dealSRId);
            receiptPage.getparameters().put('add', invoiceAddress);
            receiptPage.getparameters().put('rmrk', invoiceRemarks);
            receiptPage.getparameters().put('date', chequeDate);
            receiptPage.getparameters().put('amount', invoiceAmount);
            Blob receiptBody;

            try {
                receiptBody = receiptPage.getContentAsPDF();
                System.debug('body should be fine');

            } catch (Exception e) {
                System.debug('in the catch block');
                receiptBody = Blob.valueOf('Some Text');
            }

            attachmentObjForReceipt.Body = receiptBody;
            attachmentObjForReceipt.Name = 'Receipt' + invNum + dateTimeStr + '.pdf';
            attachmentObjForReceipt.IsPrivate = false;
            attachmentObjForReceipt.ContentType='application/pdf';
            attachmentObjForReceipt.ParentId = dealSRId;
            lstAttachment.add(attachmentObjForReceipt);

            if(!lstAttachment.isEmpty()) {
                insert lstAttachment;
            }
            System.debug('attachmentObj >>>> ' + attachmentObj);
    }

}