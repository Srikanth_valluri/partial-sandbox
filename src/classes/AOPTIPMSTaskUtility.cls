/*
 * Description - Class used for creating task in IPMS when a task is created in Salesforce
 *
 * Version        Date            Author            Description
 * 1.0            03/04/18        Vivek Shinde      Initial Draft
 */
public with sharing class AOPTIPMSTaskUtility implements Queueable, Database.AllowsCallouts {
    
    public List<Task> lstTask;

    @InvocableMethod
    public static void createTaskInIPMS(List<Task> lstTask) {
        system.debug('--lstTask:--'+lstTask);
        AOPTIPMSTaskUtility m = new AOPTIPMSTaskUtility();
        m.lstTask = lstTask;
        DateTime dt = system.now().addMinutes(1);
        String day = string.valueOf(dt.day());
        String month = string.valueOf(dt.month());
        String hour = string.valueOf(dt.hour());
        String minute = string.valueOf(dt.minute());
        String second = string.valueOf(dt.second());
        String year = string.valueOf(dt.year());
        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
        
        //String jobID = system.schedule('AOPT IPMS Task '+ system.now(), strSchedule, m);
        System.enqueueJob(m);
    }

    public void execute(QueueableContext context) {
        Task objTask = lstTask[0];
        if(String.valueOf(objTask.WhatId).startsWith('500') && String.isNotBlank(objTask.Subject) &&
           objTask.Assigned_User__c != null && (objTask.Status != 'Closed' || objTask.Status != 'Completed') && !objTask.Pushed_to_IPMS__c) {
            if(objTask.Subject.equals('Update AOPT Details in IPMS')) {
                createTask(objTask.WhatId, objTask.Id);
            }
        }
    }

    /*public void execute(SchedulableContext ctx) {
        Task objTask = lstTask[0];
        if(String.valueOf(objTask.WhatId).startsWith('500') && String.isNotBlank(objTask.Subject) &&
           objTask.Assigned_User__c != null && (objTask.Status != 'Closed' || objTask.Status != 'Completed') && !objTask.Pushed_to_IPMS__c) {
            if(objTask.Subject.equals('Update AOPT Details in IPMS')) {
                createTask(objTask.WhatId, objTask.Id);
            }
        }
    }*/
    
    //Method to perform callout to IPMS to create task
    @future(callout=true)
    public static void createTask(Id idCase, Id idTask) {
        Case objCase = [SELECT Id, NewPaymentTermJSON__c,CaseNumber, Owner.Name, OwnerId
                         ,RecordType.Name,Status,Account.Party_ID__c,CreatedDate
                         ,(Select Id,Booking_Unit__c,Booking_Unit__r.Registration_ID__c FROM SR_Booking_Units__r)
                         FROM Case WHERE ID =: idCase];
        
        Task objTask = [Select Id, Status, Subject, CreatedDate, ActivityDate, Description From Task Where Id =: idTask];
                                
        List<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5> lstObjBeans =
            new List<TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5>();

        TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objHeaderBean =
            new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
        objHeaderBean = IPMSTaskUtility.setCommonBeanFields(objHeaderBean);
        objHeaderBean = IPMSTaskUtility.setHeaderBean(objHeaderBean, objCase);
        lstObjBeans.add(objHeaderBean);

        TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objTaskBean =
            new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
        objTaskBean = IPMSTaskUtility.setCommonBeanFields(objTaskBean);
        objTaskBean = IPMSTaskUtility.setTaskBean(objTaskBean, objCase, objTask);
        objTaskBean.ATTRIBUTE4 = 'FINANCEEXECUTIVE';
        lstObjBeans.add(objTaskBean);

        if(objCase.SR_Booking_Units__r != null && !objCase.SR_Booking_Units__r.isEmpty()) {
            for(SR_Booking_Unit__c objSRBU: objCase.SR_Booking_Units__r) {
                if(objSRBU.Booking_Unit__r.Registration_Id__c != null) {
                    //strRegIds += objSRBU.Booking_Unit__r.Registration_Id__c + ',';
                    TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5 objUnitBean =
                        new TaskCreationWSDLplsql.APPSXXDC_PROCESS_SERX1794747X1X5();
                    objUnitBean = IPMSTaskUtility.setCommonBeanFields(objUnitBean);
                    objUnitBean = IPMSTaskUtility.setUnitBean(objUnitBean, objCase, objSRBU.Booking_Unit__r.Registration_Id__c);
                    lstObjBeans.add(objUnitBean);
                }
            }
            //strRegIds = strRegIds.removeEnd(',');
        }

        TaskCreationWSDL.TaskHttpSoap11Endpoint objClass = new TaskCreationWSDL.TaskHttpSoap11Endpoint();
        objClass.timeout_x = 120000;
        String strResponse;
        
        if(!Test.isRunningTest())
            strResponse = objClass.SRDataToIPMSMultiple('2-' + String.valueOf(System.currentTimeMillis()), 'CREATE_SR', 'SFDC', lstObjBeans);
        else
            strResponse = '{"message":"Process Completed Returning 1 Response Message(s)...","status":"S"}';

        system.debug('strResponse*****'+strResponse);
        List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
        IPMSTaskUtility.TaskCreationResponse objRes = (IPMSTaskUtility.TaskCreationResponse)JSON.deserialize(strResponse, IPMSTaskUtility.TaskCreationResponse.class);
        system.debug('objRes*****'+objRes);
        if(objRes != null && String.isNotBlank(objRes.status) && objRes.status.equalsIgnoreCase('S')) {
            objTask.Pushed_to_IPMS__c = true;
        }else {
            objTask.Pushed_to_IPMS__c = false;
            objTask.Task_Error_Details__c = objRes != null && String.isNotBlank(objRes.message) ?
                (objRes.message.length() > 255 ? objRes.message.substring(0, 254) : objRes.message) : '';
            if(objRes != null && String.isNotBlank(objRes.message)) {
                lstErrorLog.add(new Error_Log__c(Case__c = objCase.Id, Error_Details__c = objRes.message));
            }
        }
        update objTask;
        insert lstErrorLog;
    }
}