public class FmcViewMaintenanceRequestController {
    private static final Map<String, String> RECEIPT_ATTRIBUTE_MAP = new Map<String, String> {
        'Name' => 'SRNumber',
        'Task_Id__c' => 'TaskId',
        'CAFM_Location__r.Description__c' => 'Location',
        'Short_Description__c' => 'ShortDescription',
        'Raised_Date__c' => 'RaisedDate',
        'CAFM_Task_State__c' => 'TaskState',
        'RefreshStatus' => 'RefreshStatus'
    };
    public List<SelectOption> lstUnit {get; set;}
    public transient List<Booking_Unit__c> bookingUnitList {get;set;}
    public String strUnitId{get;set;}

    public FmcViewMaintenanceRequestController() {
        if (FmcUtils.isCurrentView('ViewHelpdeskRequests')) {
          lstUnit = getUnitOptions();
        }
    }

    private static List<SelectOption> getUnitOptions() {
        List<SelectOption> lstOptions = new List<SelectOption>{new SelectOption('', 'Select', false)};
        for (Booking_Unit__c unit : FmcUtils.queryOwnedUnitsForAccount(
            CustomerCommunityUtils.customerAccountId, 'Id, Name, Unit_Name__c'
        )) {
            lstOptions.add(new SelectOption(unit.Id, unit.Unit_Name__c));
        }
        return lstOptions;
    }

    @RemoteAction
    public static list<FM_Case__c> fetchFMCases(String strBoookingUnitId){
        List<FM_Case__c> lstHelpDeskFMCases = new List<FM_Case__c>();
        ID helpDeskRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('Helpdesk').getRecordTypeId();

        System.debug('>>>strBoookingUnitId : '+strBoookingUnitId);
        if(strBoookingUnitId != NULL && String.isNotBlank(strBoookingUnitId)){
            lstHelpDeskFMCases = [Select Name
                                       , Task_Id__c
                                       , CAFM_Location__c
                                       , CAFM_Location__r.Description__c
                                       , Short_Description__c
                                       , Raised_Date__c
                                       , CAFM_Task_State__c
                                       , Booking_Unit__c
                                       , Unit_Name__c
                                       , Id
                                  FROM FM_Case__c
                                  Where Booking_Unit__c =: strBoookingUnitId
                                  AND Task_Id__c != NULL
                                  AND RecordTypeId =: helpDeskRecordTypeId];
            System.debug('>>>>lstHelpDeskFMCases : '+lstHelpDeskFMCases);
        }
        return lstHelpDeskFMCases;
    }

    @RemoteAction
    public static string updateCaseStatus(Integer taskId, String taskState) {
        String sessionId;
        System.debug('>>>taskId'+taskId);
        System.debug('>>taskState : '+taskState);

        //Get session id
        sessionId = getSessionIDfromCAFM();
        schemasDatacontractOrg200407FsiConc_t32.TaskDtoV3 CAFMTaskObj = new schemasDatacontractOrg200407FsiConc_t32.TaskDtoV3();
        wwwFsiCoUkServicesEvolution0409_t32.TaskServiceV3 objTaskService = new wwwFsiCoUkServicesEvolution0409_t32.TaskServiceV3();
        CAFMTaskObj = objTaskService.GetTaskById(sessionId,taskId);
        System.debug('>>CAFMTaskObj : '+CAFMTaskObj);
        System.debug('>>CAFMTaskObj.state : '+CAFMTaskObj.State);
        if(CAFMTaskObj.State != NULL && String.isNotBlank(CAFMTaskObj.State) && CAFMTaskObj.State != taskState){
            List<FM_Case__c> lstFMCase = [Select Name
                                           , Task_Id__c
                                           , CAFM_Location__c
                                           , CAFM_Location__r.Description__c
                                           , Short_Description__c
                                           , Raised_Date__c
                                           , CAFM_Task_State__c
                                           , Booking_Unit__c
                                           , Unit_Name__c
                                           , Id
                                      FROM FM_Case__c
                                      Where Task_Id__c =: taskId
                                      LIMIT 1
                                    ];
            FM_Case__c objFMCase = new FM_Case__c();
            objFMCase = lstFMCase[0];
            objFMCase.CAFM_Task_State__c = CAFMTaskObj.State;

            update objFMCase;

            return CAFMTaskObj.State;
        }
        return taskState;
    }

    private static String getSessionIDfromCAFM() {
      try{
          wwwFsiCoUkServicesEvolution0409.SecurityService1 obj = new wwwFsiCoUkServicesEvolution0409.SecurityService1();
          obj.timeout_x = 120000;
          schemasDatacontractOrg200407FsiPlat.AuthenticationDto result = obj.Authenticate(Label.CAFM_Username,Label.CAFM_Password);
          System.debug('>>result>>>'+result);
          if(result != NULL) {
              if(result.OperationResult == 'Succeeded') {
                  System.debug('===result.SessionId=='+result);
                  return result.SessionId;
              }
          }

      }
      catch(System.Exception excp) {
          System.debug('==excp==SESSION ID='+excp);
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,excp.getMessage() + excp.getLineNumber()));
      }
     return NULL;
  }
}