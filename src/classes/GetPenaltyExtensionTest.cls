/**
 * Description: Test class for GetPenaltyExtension
 */
@isTest
private class GetPenaltyExtensionTest {

    /*
     * Description : Test method to check whether the penalty is generated or not.
     */
    @isTest static void testGeneratePenalty() {

        // Insert Account        
        Account objAcc = TestDataFactory_CRM.createPersonAccount();
        insert objAcc ;
        
        //Insert Service Request
        NSIBPM__Service_Request__c objSR = TestDataFactory_CRM.createServiceRequest( );
        insert objSR ;
        
        //Insert Bookings
        List<Booking__c>  lstBookings = TestDataFactory_CRM.createBookingForAccount( objAcc.Id, objSR.Id, 1);
        insert lstBookings ;
        
        Property__c objProp = TestDataFactory_CRM.createProperty();
        insert objProp;
        
        Inventory__c  objInv = TestDataFactory_CRM.createInventory(objProp.Id);
        objInv.Property_Country__c = 'UAE';
        objInv.Property_City__c = 'Dubai';
        objInv.Property_Status__c = 'Ready';
        objInv.Bedroom_Type__c = ' 1 BR';
        insert objInv;
        
        //Insert Booking Units
        List<Booking_Unit__c> lstBookingUnits = TestDataFactory_CRM.createBookingUnits( lstBookings , 1);
        lstBookingUnits[0].Registration_Status__c = 'Agreement Executed by Damac';
        lstBookingUnits[0].Inventory__c = objInv.Id;
        lstBookingUnits[0].Area__c = 35;
        lstBookingUnits[0].Handover_Flag__c = 'Y';
        lstBookingUnits[0].Registration_ID__c = String.valueOf(System.currentTimeMillis());
        insert lstBookingUnits;

        PageReference redirectionUrl;
        Test.startTest();
            Test.setMock(WebServiceMock.class, new SOAPCalloutServiceMockAssignment());
            ApexPages.StandardController standardControllerInstance = new ApexPages.StandardController(lstBookingUnits[0]);
            GetPenaltyExtension objController = new GetPenaltyExtension(standardControllerInstance);
            Test.setCurrentPageReference(new PageReference('Page.GetPenaltyPage'));
            System.currentPageReference().getParameters().put('Id', lstBookingUnits[0].Id);
            objController.generatePenalty();
            redirectionUrl = objController.redirectToDetailPage();
        Test.stopTest();

        // Check whether redirected to detail page
        System.assert(redirectionUrl != null);
        System.assertEquals(redirectionUrl.getURL(), '/'+lstBookingUnits[0].Id);
    }

}