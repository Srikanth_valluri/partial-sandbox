public without sharing class MortgageAutoCloseTaskInSF{

    @InvocableMethod
    public static void UpdateTaskInSF( List<SR_Attachments__c> lstSR_Attachments ) {
        system.debug('lstSR_Attachments : '+lstSR_Attachments);
    
        Set<Id> setFinalOfferCaseId = new Set<Id>();
        Set<Id> setOriginalLetterCaseId = new Set<Id>();
        Set<Id> setLienLetterCaseId = new Set<Id>();
        Set<Id> setNOCCaseId = new Set<Id>();
        
        for( SR_Attachments__c objSR_Attachments : lstSR_Attachments ){
            if( objSR_Attachments.Case__c != null ){
                if( (objSR_Attachments.Name).equalsIgnoreCase('Final Offer Letter') ) {
                    setFinalOfferCaseId.add(objSR_Attachments.Case__c);
                }
                if( (objSR_Attachments.Name).equalsIgnoreCase('Original Letter of Guarantee') ) {
                    setOriginalLetterCaseId.add(objSR_Attachments.Case__c);
                }
                if( (objSR_Attachments.Name).equalsIgnoreCase('Lien Letter') ) {
                    setLienLetterCaseId.add(objSR_Attachments.Case__c);
                }
                if( (objSR_Attachments.Name).equalsIgnoreCase('NOC') ) {
                    setNOCCaseId.add(objSR_Attachments.Case__c);
                }
            }
        }
        system.debug(setFinalOfferCaseId);
        system.debug(setOriginalLetterCaseId);
        system.debug(setLienLetterCaseId);
        system.debug(setNOCCaseId);
        
        if( !setFinalOfferCaseId.isEmpty() ){
            updateFinalLetterTask( setFinalOfferCaseId );
        }

        if( !setOriginalLetterCaseId.isEmpty() ){
            updateOriginalLetterTask( setOriginalLetterCaseId );
        }

        if( !setLienLetterCaseId.isEmpty() ){
            updateLienLetterask( setLienLetterCaseId );
        }

        if( !setNOCCaseId.isEmpty() ){
            updateGenerateNocTask(setNOCCaseId);
        }
    }

    public static void updateFinalLetterTask( Set<Id> caseId ){ 
        system.debug('====id==='+caseID);
         List<Task> lsTaskToUpdate = new List<Task>();
         map<Id, Case> mapCaseDetails = getCaseMap( caseId );
        system.debug('===='+getTasks(caseID));
         for( Task objTask : getTasks( caseId ) ) {
            if(  mapCaseDetails.get(objTask.WhatId).Booking_Unit__c != null
             &&  objTask.Subject == System.Label.Mortgage_10 ) {
                objTask.Status = 'Closed';
                objTask.Completed_DateTime__c = System.Now();
            }
            lsTaskToUpdate.add(objTask);
         }
         if(!lsTaskToUpdate.isEmpty() ){
            update lsTaskToUpdate;
         }
    }
    public static map<Id, Case> getCaseMap( set<Id> setCaseIds ) {
        return new map<Id, Case>( [ SELECT Id
                                        , Status
                                        , Account.Party_ID__c
                                        , Account.Name
                                        , Owner.Name
                                        , POA_Name__c
                                        , POA_Expiry_Date__c
                                        , POA_Relation_With_Owner__c
                                        , CaseNumber
                                        , POA_Issued_By__c
                                        , Mortgage_Bank_Name__c
                                        , Mortgage_Transaction_Type__c
                                        , Mortgage_Value__c
                                        , Purpose_of_POA__c
                                        , POA_File_URL__c
                                        , Booking_Unit__c
                                      FROM Case 
                                     WHERE Id IN :setCaseIds ] );
    }

    public static void updateOriginalLetterTask( Set<Id> caseId ){  
         List<Task> lsTaskToUpdate = new List<Task>();
         for( Task objTask : getTasks( caseId ) ) {
            if( objTask.Subject == System.Label.Mortgage_12 ) {
                objTask.Status = 'Closed';
                objTask.Completed_DateTime__c = System.Now();
                
            }
            lsTaskToUpdate.add(objTask);
         }
         if(!lsTaskToUpdate.isEmpty() ){
            update lsTaskToUpdate;
         }
    }
    

    public static void updateLienLetterask( Set<Id> caseId ){   
         List<Task> lsTaskToUpdate = new List<Task>();
         for( Task objTask : getTasks( caseId ) ) {
            if( objTask.Subject == System.Label.Mortgage_13 ) {
                objTask.Status = 'Closed';
                objTask.Completed_DateTime__c = System.Now();
            }
            lsTaskToUpdate.add(objTask);
         }
         if(!lsTaskToUpdate.isEmpty() ){
            update lsTaskToUpdate;
         }
    }   

    public static void updateGenerateNocTask( Set<Id> caseId ){ 
         List<Task> lsTaskToUpdate = new List<Task>();
         for( Task objTask : getTasks( caseId ) ) {
            if( objTask.Subject == System.Label.Mortgage_22 ) {
                objTask.Status = 'Closed';
                objTask.Completed_DateTime__c = System.Now();
            }
            lsTaskToUpdate.add(objTask);
         }
         if(!lsTaskToUpdate.isEmpty() ){
            update lsTaskToUpdate;
         }
    }   
    
    public static list<Task> getTasks( set<Id> caseId ) {
        system.debug('====caseId'+caseID);
        return new list<Task>( [ SELECT WhoId
                                      , WhatId
                                      , Type
                                      , Status
                                      , OwnerId
                                      , Id
                                      , Subject
                                      , CreatedDate
                                      , Description
                                      , Assigned_User__c
                                      , ActivityDate
                                      , Owner.Name 
                                   FROM Task
                                  WHERE WhatId IN : caseId ] );
    }   
    
}