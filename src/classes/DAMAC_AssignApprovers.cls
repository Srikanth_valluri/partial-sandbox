/*
    Created By              : Srikanth V
    Created Date            : 04/03/2020
    Desription              : Do assign approvers for SR based on assignment Rules
    
*/

public class DAMAC_AssignApprovers {
    
    /**** This method returns all fields of SR Object **********/
    public static string getAllfields (String objectName) {
        string fields = '';
        sObjectType objectType = Schema.getGlobalDescribe ().get (objectName);
        if (objectType == null) {
            return fields;
        }
        for (string f :objectType.getDescribe ().fields.getMap ().keySet ()) {
            if (f.toLowerCase () != 'lastvieweddate')
                fields += f+ ', ';
        }
        return fields.removeEnd(', '); 
        
    }
        
    // To get the eiligible rule for the service request and assigning approvers to SR
    public static void getEligibleRule (List <NSIBPM__Service_Request__c> newSRs) {
        
        Map <String, List<NSIBPM__Service_Request__c>> assignmentMap = new Map <String, List<NSIBPM__Service_Request__c>>();
        
        String fields = getAllfields('Inquiry_Assignment_Rules__c');
        system.debug (fields );
        
        String recordTypeName = 'Approval Rules';
        String queryCondition = 'SELECT ' + fields + ' from Inquiry_Assignment_Rules__c '
                              + ' WHERE Fields_to_Apply_Rule__c != null '
                              + ' AND Active__c = True AND RecordType.Name =: recordTypeName '
                              + ' ORDER by Priority__c Asc';
        system.debug(queryCondition + 'queryCondition---');
        List<Inquiry_Assignment_Rules__c> assignmentRules = database.query(queryCondition);
        system.debug(assignmentRules + 'assignmentRules---');
        if (assignmentRules.size() > 0) {
            Set <String> rmEmails = new Set <String> ();
            Map <String, User> rmUsers = new Map <String, User> ();
            for (NSIBPM__Service_Request__c sr : newSRs) {
                if (sr.Rm_email__c != NULL)
                    rmEmails.add (sr.Rm_Email__c);
            }
            if (rmEmails.size () > 0) {
                for (User U :[SELECT Email, ManagerId, manager.managerId, manager.manager.managerId, 
                                    manager.manager.manager.managerId FROM User 
                            WHERE Email IN :rmEmails])
                {
                    rmUsers.put (u.Email, u);
                }
            }
            
            for (NSIBPM__Service_Request__c sr : newSRs) {
                Integer tempFlag = 0;
                
                for (Inquiry_Assignment_Rules__c inqAssignment : assignmentRules) {
                    Set <String> rulesList = DamacUtility.splitMutliSelect (inqAssignment.Fields_to_Apply_Rule__c);
                    Integer flag = 0;
                    for (String field : rulesList) {
                        field = field.trim();
                        System.debug (field);
                        if (field != 'Token_Amount_AED__c') {
                            
                            try {
                                String field1 = field;
                                if (field == 'NSIBPM__Record_Type_Name__c') {
                                    field1 = 'Inquiry_Record_Type__c';
                                }
                                System.debug (sr.get (field));
                                System.debug (inqAssignment.get (field1));
                            
                                if (sr.get (field) != null && inqAssignment.get (field1) != null) { 
                                    if (sr.get (field) == inqAssignment.get (field1)){
                                        flag = 1;
                                    } else {
                                        flag = 0;
                                        break;
                                    }
                                } else {
                                    flag = 0;
                                    break;
                                }
                            } catch (Exception e) {
                                break;
                            }
                        }
                        
                        if (field == 'Token_Amount_AED__c') {
                            if (sr.get (field) != NULL && inqAssignment.get (field) != null) {
                                Decimal srAmount = (Decimal) sr.get (field);
                                String ruleAmountStr = String.valueOf (inqAssignment.get(field));
                                if (String.valueOf (inqAssignment.get(field)).startsWith ('<=')) {
                                    ruleAmountStr = ruleAmountStr.right(ruleAmountStr.length()-2);
                               
                                    Decimal ruleAmount = Decimal.valueOf (ruleAmountStr);
                                    System.debug (srAmount+'==='+ruleAmount);
                                    if (srAmount <= ruleAmount) {
                                        flag = 1;
                                    } else {
                                        flag = 0;
                                        break;
                                    }    
                                }
                                
                                if (String.valueOf (inqAssignment.get(field)).startsWith ('>=')) {
                                    ruleAmountStr = ruleAmountStr.right(ruleAmountStr.length()-2);
                               
                                    Decimal ruleAmount = Decimal.valueOf (ruleAmountStr);
                                    System.debug (srAmount+'==='+ruleAmount);
                                    
                                    if (srAmount >= ruleAmount) {
                                        flag = 1;
                                    } else {
                                        flag = 0;
                                        break;
                                    }     
                                }
                                
                                if (String.valueOf (inqAssignment.get(field)).startsWith ('=')) {
                                    ruleAmountStr = ruleAmountStr.right(ruleAmountStr.length()-1);
                               
                                    Decimal ruleAmount = Decimal.valueOf (ruleAmountStr);
                                    System.debug (srAmount+'==='+ruleAmount);
                                    if (srAmount == ruleAmount) {
                                        flag = 1;
                                    } else {
                                        flag = 0;
                                        break;
                                    }    
                                }
                                if (String.valueOf (inqAssignment.get(field)).startsWith ('<')) {
                                    ruleAmountStr = ruleAmountStr.right(ruleAmountStr.length()-1);
                               
                                    Decimal ruleAmount = Decimal.valueOf (ruleAmountStr);
                                    System.debug (srAmount+'==='+ruleAmount);
                                    if (srAmount < ruleAmount) {
                                        flag = 1;
                                    } else {
                                        flag = 0;
                                        break;
                                    }    
                                }
                                
                                
                                
                                if (String.valueOf (inqAssignment.get(field)).startsWith ('>')) {
                                    ruleAmountStr = ruleAmountStr.right(ruleAmountStr.length()-1);
                                    System.debug (ruleAmountStr);
                                    Decimal ruleAmount = Decimal.valueOf (ruleAmountStr);
                                    System.debug (srAmount+'==='+ruleAmount);
                                    if (srAmount > ruleAmount) {
                                        flag = 1;
                                    } else {
                                        flag = 0;
                                        break;
                                    }    
                                }
                                
                                
                            }
                        }
                        
                        
                    }
                    
                    
                    tempFlag = flag;
                    if (flag == 1) {
                        sr.Assignment_Rules__c = inqAssignment.id;
                        if (inqAssignment.Assign_To_Queue__c == false) {
                            
                            sr.Approver1__c = null;
                            sr.Approver2__c = null;
                            sr.Approver3__c = null;
                            sr.Approver4__c = null;
                            
                            List <ID> excludeUserIds = new List <ID> ();
                            if (inqAssignment.Exclude__c != NULL) {
                                if (inqAssignment.Exclude__c.contains (';'))
                                    excludeUserIds = inqAssignment.Exclude__c.split (';');
                                else
                                    excludeUserIds.add (inqAssignment.Exclude__c);
                            }
                            ID manager1Id = null;
                            
                            ID manager2Id = null;
                            
                            ID manager3Id = null;
                            
                            ID manager4Id = null;
                            
                            if (sr.Rm_Email__c == NULL) {
                                manager1Id = sr.createdBy.managerId;
                                manager2Id = sr.createdBy.manager.managerId;
                                manager3Id = sr.createdBy.manager.manager.managerId;
                                manager4Id = sr.createdBy.manager.manager.manager.managerId;
                            }
                            if (sr.Rm_Email__c != NULL) {
                                User u = rmUsers.get (sr.Rm_email__c);
                                manager1Id = u.managerId;
                                manager2Id = u.manager.managerId;
                                manager3Id = u.manager.manager.managerId;
                                manager4Id = u.manager.manager.manager.managerId;
                                System.debug(u);
                            }
                            
                            List <ID> managerIds = new List <ID> ();
                            if (!excludeUserIds.contains (manager1Id))
                                managerIds.add (manager1Id);
                            
                            if (!excludeUserIds.contains (manager2Id))
                                managerIds.add (manager2Id);
                                
                            if (!excludeUserIds.contains (manager3Id))
                                managerIds.add (manager3Id);
                                
                            if (!excludeUserIds.contains (manager4Id))
                                managerIds.add (manager4Id);
                            
                            if (managerIds.size () > 0)
                                sr.Approver1__c = managerIds[0];
                                
                            if (managerIds.size () >= 1)
                                sr.Approver2__c = managerIds[1];
                                
                            if (managerIds.size () > 2)
                                sr.Approver3__c = managerIds[2];
                                
                            if (managerIds.size () > 3)
                                sr.Approver4__c = managerIds[3];                        
                                
                            if (sr.Approver1__c == null && sr.Rm_Email__c == NULL)
                                sr.Approver1__c = sr.createdBy.managerId;
                            
                                
                            
                        }
                        break;
                    
                    } 
                }
                System.debug (SR);
            }     
            
        }
        
        
    }    
}