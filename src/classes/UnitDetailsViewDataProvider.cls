public without sharing class UnitDetailsViewDataProvider implements DataProvider {
    public DataDisplayConfig getData(DataDisplayConfig config) {
        List<Booking_Unit__c> lstBookingUnit = new List<Booking_Unit__c>();
        if (String.isNotBlank(config.recordId)) {
            lstBookingUnit = [
                SELECT  Id,
                        Name,
                        Unit_Location__c,
                        Registration_DateTime__c,
                        Area__c,
                        Agreement_Date__c,
                        Unit_Selling_Price__c,
                        Requested_Price__c,
                        Unit_Name__c,
                        Registration_Status__c,
                        Unit_Type__c,
                        Construction_Status__c,
                        RERA_Project__c
                FROM    Booking_Unit__c
                WHERE   Id = :config.recordId
            ];
        }

        config.dataList = DataRendererUtils.wrapData(config, lstBookingUnit);
        return config;
    }
}