public class Damac_DLD_PaymentPlans {
     
    public static List <Payment_Terms__c> retrievePayPlan (Id buId){
        List <Payment_Terms__c> terms = new List <Payment_Terms__c> (); 
        IPMS_Integration_Settings__mdt ipms = [SELECT Endpoint_URL__c,
                                               Password__c,
                                               Username__c 
                                               FROM IPMS_Integration_Settings__mdt 
                                               WHERE DeveloperName = 'IPMS_webservice'];
        HTTPRequest req = new HTTPRequest();
        req.setMethod('POST');
        AsyncReceiptWebservice.getIPMSsetting ('IPMS_webservice');
        String reqXML = AsyncReceiptWebservice.prepareRetrievePayPlan(buId);
        reqXML = reqXML.replaceAll('null', '');
        reqXML = reqXML.trim();
        System.Debug (reqXML);
        req.setbody (reqXML);
        req.setEndpoint(ipms.Endpoint_URL__c);
        req.setHeader ('Content-Type','text/xml');
        req.setTimeout (120000);
        HTTP http = new HTTP();
        if(!Test.isrunningTest()){
            try{
                HTTPResponse res = http.send(req);
                System.debug('>>>>>Response>>>>>>'+res.getbody());
                terms = parsePayPlanResponse(res.getBody());
            }
            catch(Exception ex) {
                System.debug('Callout error: '+ ex);
                Log__c objLog = new Log__c();
                objLog.Description__c ='Ids=='+buId+'-Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage();
                objLog.Type__c = 'Webservice Callout For Retrieve Payment Plan';
                insert objLog;             
            }  
        }  
        return terms;
    }
    /**********************Parsing the response received for Receipt Doc request*********************************/
    
    public static List <Payment_Terms__c> parsePayPlanResponse(string body){
        
        String Regid,termid,BUid,status,statusmsg,stageId,docurl = '';
        
        List <Payment_Terms__c> PTList = new List<Payment_Terms__c>();
        
        DOM.Document xmlDOC = new DOM.Document ();
        xmlDOC.load(body);
        DOM.XMLNode rootElement = xmlDOC.getRootElement ();
        
        for (Dom.XMLNode child1: rootElement.getChildElements ()){
            for (Dom.XMLNode child2: child1.getChildElements()){
                for(Dom.XMLNode child3: child2.getChildElements()){
                    for(Dom.XMLNode child4: child3.getChildElements()){
                        Payment_Terms__c PT = new Payment_Terms__c();
                        for(Dom.XMLNode child5: child4.getChildElements()){
                            if(child5.getName()=='ATTRIBUTE1') 
                                Regid= child5.getText();
                            if(child5.getName()=='ATTRIBUTE2') 
                                termid= child5.getText();
                            if(child5.getName()=='ATTRIBUTE3') 
                                PT.Line_ID__c= child5.getText();
                            if(child5.getName()=='ATTRIBUTE4') 
                                PT.Installment__c= child5.getText();
                            if(child5.getName()=='ATTRIBUTE5') 
                                PT.Percent_Value__c= child5.getText();
                           
                            if(child5.getName()=='ATTRIBUTE8') 
                                PT.Milestone_Event__c= child5.getText();
                            if(child5.getName()=='ATTRIBUTE9') 
                                PT.Payment_Amount__c= child5.getText();
                            if(child5.getName()=='ATTRIBUTE10') 
                                PT.Description__c= child5.getText();
                            if(child5.getName()=='ATTRIBUTE11') 
                                PT.Event_Days__c= child5.getText();
                            if(child5.getName()=='ATTRIBUTE12') 
                                PT.Milestone_Event_Arabic__c= child5.getText();
                            if(child5.getName()=='ATTRIBUTE13') 
                                PT.Seq_No__c= child5.getText();                            
                        }  
                        PTList.add(PT);                           
                        
                    }
                }
            }
        }
        System.Debug (PTList); 
        return PTList;
    }
    
}