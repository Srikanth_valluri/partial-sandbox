@isTest
private class HDApp_AccessCardSR_APITest {

  @isTest
  static void createDraftAccessCardSRTest1() {

    NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1210',
                                      Registration_ID__c = '3901');
        insert bookingUnit;

        String jsonString =  '   {  '  + 
    '       "action": "draft",  '  + 
    '       "id": null,  '  + 
    '       "booking_unit_id": "'+bookingUnit.id+'",  '  + 
    '       "account_id" : "'+account.id+'",  '  + 
    '       "type_of_request" : "New card",  '  + 
    '       "selected_access_card_details" : [  '  + 
    '           {  '  + 
    '               "card_id" : 2,  '  + 
    '               "number_of_cards" : 1  '  + 
    '           },  '  + 
    '           {  '  + 
    '               "card_id" : 3,  '  + 
    '               "number_of_cards" : 1  '  + 
    '           }  '  + 
    '       ]  '  + 
    '  }  ' ; 

     Test.startTest();

          RestRequest req = new RestRequest();
          RestResponse res = new RestResponse();
          req.requestURI = '/accessCardSROps';  
          req.requestBody = Blob.valueOf(jsonString);
          req.httpMethod = 'PUT';
          RestContext.request = req;
          RestContext.response = res;

       HDApp_AccessCardSR_API.createDraftAccessCardSR();
        Test.stopTest();

  }

  @isTest
  static void createDraftAccessCardSRTest2() {
    Test.startTest();

      String jsonString = '';
          RestRequest req = new RestRequest();
          RestResponse res = new RestResponse();
          req.requestURI = '/accessCardSROps';  
          req.requestBody = Blob.valueOf(jsonString);
          req.httpMethod = 'PUT';
          RestContext.request = req;
          RestContext.response = res;

       HDApp_AccessCardSR_API.createDraftAccessCardSR();
        Test.stopTest();
  }

  @isTest
  static void createDraftAccessCardSRTest3() {
    Test.startTest();

      String jsonString = '   {  '  + 
      '       "action": "",  '  + 
      '       "id": null,  '  + 
      '       "booking_unit_id": "'+'1234'+'",  '  + 
      '       "account_id" : "'+'1234'+'",  '  + 
      '       "type_of_request" : "New card",  '  + 
      '       "selected_access_card_details" : [  '  + 
      '           {  '  + 
      '               "card_id" : 2,  '  + 
      '               "number_of_cards" : 1  '  + 
      '           },  '  + 
      '           {  '  + 
      '               "card_id" : 3,  '  + 
      '               "number_of_cards" : 1  '  + 
      '           }  '  + 
      '       ]  '  + 
      '  }  ' ; 
          RestRequest req = new RestRequest();
          RestResponse res = new RestResponse();
          req.requestURI = '/accessCardSROps';  
          req.requestBody = Blob.valueOf(jsonString);
          req.httpMethod = 'PUT';
          RestContext.request = req;
          RestContext.response = res;

       HDApp_AccessCardSR_API.createDraftAccessCardSR();
        Test.stopTest();
  }

  @isTest
  static void createDraftAccessCardSRTest4() {
    Test.startTest();

      String jsonString = '   {  '  + 
      '       "action": "draft",  '  + 
      '       "id": null,  '  + 
      '       "booking_unit_id": "'+'124'+'",  '  + 
      '       "account_id" : "'+'1234'+'",  '  + 
      '       "type_of_request" : "New card",  '  + 
      '       "selected_access_card_details" : [  '  + 
      '           {  '  + 
      '               "card_id" : 2,  '  + 
      '               "number_of_cards" : 1  '  + 
      '           },  '  + 
      '           {  '  + 
      '               "card_id" : 3,  '  + 
      '               "number_of_cards" : 1  '  + 
      '           }  '  + 
      '       ]  '  + 
      '  }  ' ; 
          RestRequest req = new RestRequest();
          RestResponse res = new RestResponse();
          req.requestURI = '/accessCardSROps';  
          req.requestBody = Blob.valueOf(jsonString);
          req.httpMethod = 'PUT';
          RestContext.request = req;
          RestContext.response = res;

       HDApp_AccessCardSR_API.createDraftAccessCardSR();
        Test.stopTest();
  }

  /****************************************************************/

  @isTest
  static void submitAccessCardSRTest1() {

       

    NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account( LastName = 'Test Account',
                                       Party_ID__c = '63062',
                                       RecordtypeId = rtId,
                                       Email__pc = 'test@mailinator.com');
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        Booking_Unit__c bookingUnit = new Booking_Unit__c(
                                      Booking__c = booking.Id,
                                      Unit_Name__c = 'BD4/12/1210',
                                      Registration_ID__c = '3901');
        insert bookingUnit;

        FM_Case__c objFM = new FM_Case__c();
        objFM.isHelloDamacAppCase__c = true;
    objFM.Origin__c='Portal';
    objFM.Status__c='Draft Request';
    Id devRecordTypeId = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get(
                                'Request For Access Card').getRecordTypeId();
        objFM.RecordTypeId = devRecordTypeId;
        objFM.Request_Type_DeveloperName__c = 'Request_for_Access_cards';
        objFM.Tenant__c = account.Id;
        objFM.Booking_Unit__c  = bookingUnit.id;
        objFM.Request_Type__c = 'Request For Access Card';
        insert objFM;

        String jsonString =  '   {  '  + 
    '       "action": "submit",  '  + 
    '       "id": "'+objFM.id+'",  '  + 
    '       "receipt_number" : "TRNX001254546",  '  + 
    '       "receipt_url" : "https://xyz.com"  '  + 
    '  }  ' ;

    Test.startTest();
        RestRequest req = new RestRequest();
          RestResponse res = new RestResponse();
          req.requestURI = '/accessCardSROps';  
          req.requestBody = Blob.valueOf(jsonString);
          req.httpMethod = 'PATCH';
          RestContext.request = req;
          RestContext.response = res;

       HDApp_AccessCardSR_API.submitAccessCardSR();
    Test.stopTest();
  }

  @isTest
  static void submitAccessCardSRTest2() {

    String jsonString =  '   {  '  + 
    '       "action": "submit",  '  + 
    '       "id": "'+''+'",  '  + 
    '       "receipt_number" : "TRNX001254546",  '  + 
    '       "receipt_url" : "https://xyz.com"  '  + 
    '  }  ' ;

    Test.startTest();
        RestRequest req = new RestRequest();
          RestResponse res = new RestResponse();
          req.requestURI = '/accessCardSROps';  
          req.requestBody = Blob.valueOf(jsonString);
          req.httpMethod = 'PATCH';
          RestContext.request = req;
          RestContext.response = res;

       HDApp_AccessCardSR_API.submitAccessCardSR();
    Test.stopTest();
  }

  @isTest
  static void submitAccessCardSRTest3() {

    String jsonString =  '   {  '  + 
    '       "action": "",  '  + 
    '       "id": "'+'1234'+'",  '  + 
    '       "receipt_number" : "TRNX001254546",  '  + 
    '       "receipt_url" : "https://xyz.com"  '  + 
    '  }  ' ;

    Test.startTest();
        RestRequest req = new RestRequest();
          RestResponse res = new RestResponse();
          req.requestURI = '/accessCardSROps';  
          req.requestBody = Blob.valueOf(jsonString);
          req.httpMethod = 'PATCH';
          RestContext.request = req;
          RestContext.response = res;

       HDApp_AccessCardSR_API.submitAccessCardSR();
    Test.stopTest();
  }

  @isTest
  static void submitAccessCardSRTest4() {

    String jsonString =  '' ;

    Test.startTest();
        RestRequest req = new RestRequest();
          RestResponse res = new RestResponse();
          req.requestURI = '/accessCardSROps';  
          req.requestBody = Blob.valueOf(jsonString);
          req.httpMethod = 'PATCH';
          RestContext.request = req;
          RestContext.response = res;

       HDApp_AccessCardSR_API.submitAccessCardSR();
    Test.stopTest();
  }

  /****************************************************************/

  @isTest
  static void getAccessCardAmountDetailsTest1() {

    String jsonString =  '   {  '  + 
     '       "booking_unit_id" : "'+'1234'+'",  '  + 
     '       "type_of_request" : "New card",  '  + 
     '       "selected_access_card_details" : [  '  + 
     '           {  '  + 
     '               "card_id" : 2,  '  + 
     '               "number_of_cards" : 1  '  + 
     '           },  '  + 
     '           {  '  + 
     '               "card_id" : 3,  '  + 
     '               "number_of_cards" : 1  '  + 
     '           }  '  + 
     '       ]  '  + 
     '   }  '  + 
     '    ' ; 

     Test.startTest();
        RestRequest req = new RestRequest();
          RestResponse res = new RestResponse();
          req.requestURI = '/accessCardSROps';  
          req.requestBody = Blob.valueOf(jsonString);
          req.httpMethod = 'POST';
          RestContext.request = req;
          RestContext.response = res;

       HDApp_AccessCardSR_API.getAccessCardAmountDetails();
    Test.stopTest();
  }

  @isTest
  static void getAccessCardAmountDetailsTest2() {

    String jsonString =  ''; 

     Test.startTest();
        RestRequest req = new RestRequest();
          RestResponse res = new RestResponse();
          req.requestURI = '/accessCardSROps';  
          req.requestBody = Blob.valueOf(jsonString);
          req.httpMethod = 'POST';
          RestContext.request = req;
          RestContext.response = res;

       HDApp_AccessCardSR_API.getAccessCardAmountDetails();
    Test.stopTest();
  }

  @isTest
  static void getAccessCardAmountDetailsTest3() {

    String jsonString =  '   {  '  + 
     '       "booking_unit_id" : "'+''+'",  '  + 
     '       "type_of_request" : "New card",  '  + 
     '       "selected_access_card_details" : [  '  + 
     '           {  '  + 
     '               "card_id" : 2,  '  + 
     '               "number_of_cards" : 1  '  + 
     '           },  '  + 
     '           {  '  + 
     '               "card_id" : 3,  '  + 
     '               "number_of_cards" : 1  '  + 
     '           }  '  + 
     '       ]  '  + 
     '   }  '  + 
     '    ' ; 

     Test.startTest();
        RestRequest req = new RestRequest();
          RestResponse res = new RestResponse();
          req.requestURI = '/accessCardSROps';  
          req.requestBody = Blob.valueOf(jsonString);
          req.httpMethod = 'POST';
          RestContext.request = req;
          RestContext.response = res;

       HDApp_AccessCardSR_API.getAccessCardAmountDetails();
    Test.stopTest();
  }

  @isTest
  static void getAccessCardAmountDetailsTest4() {

    String jsonString =  '   {  '  + 
     '       "booking_unit_id" : "'+'1234'+'",  '  + 
     '       "type_of_request" : "",  '  + 
     '       "selected_access_card_details" : [  '  + 
     '           {  '  + 
     '               "card_id" : 2,  '  + 
     '               "number_of_cards" : 1  '  + 
     '           },  '  + 
     '           {  '  + 
     '               "card_id" : 3,  '  + 
     '               "number_of_cards" : 1  '  + 
     '           }  '  + 
     '       ]  '  + 
     '   }  '  + 
     '    ' ; 

     Test.startTest();
        RestRequest req = new RestRequest();
          RestResponse res = new RestResponse();
          req.requestURI = '/accessCardSROps';  
          req.requestBody = Blob.valueOf(jsonString);
          req.httpMethod = 'POST';
          RestContext.request = req;
          RestContext.response = res;

       HDApp_AccessCardSR_API.getAccessCardAmountDetails();
    Test.stopTest();
  }

}