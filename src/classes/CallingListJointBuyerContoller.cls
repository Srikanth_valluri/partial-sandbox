public with sharing class CallingListJointBuyerContoller {
    public List<Buyer__c> lstJointBuyers                       {get; set;}
    public Calling_List__c currentCalling_List;
    public String callingListId                                {get; set;}
    
    public CallingListJointBuyerContoller(ApexPages.StandardController controller) {
        callingListId = ApexPages.currentPage().getParameters().get('id');
        callingListId = String.valueOf(callingListId).substring(0, 15);
        system.debug('----------callingListId  : '+ callingListId );
    }
    
    public void init() {
        lstJointBuyers = new List<Buyer__c>();
        currentCalling_List =  getCalling_List(callingListId);
        system.debug('----------currentCalling_List : '+ currentCalling_List );
        if( currentCalling_List.Booking_Unit__c != null ) {
            for (Buyer__c objBuyer : [SELECT Id, 
                                             Name, 
                                             First_Name__c, 
                                             Last_Name__c, 
                                             Account__c, 
                                             Buyer_ID__c,
                                             Booking__c, 
                                             Primary_Buyer__c,
                                             Account__r.Party_Id__c,
                                             Party_ID__c,
                                             Passport_Number__c,
                                             Email__c,
                                             Nationality__c,
                                             Buyer_Type__c
                                        FROM Buyer__c
                                       WHERE Account__c != null 
                                         AND Booking__c = :currentCalling_List.Booking_Unit__r.Booking__r.Id
                                         AND Primary_Buyer__c = false ]
            ) {
                System.Debug('=====objBuyer : ' + objBuyer);
                lstJointBuyers.add(objBuyer);
            }
            
            System.Debug('=====lstJointBuyers : ' + lstJointBuyers);
            
            if( lstJointBuyers.size() <= 0 ) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'No Joint buyers to display.'));
            }
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please Populate booking Unit on Calling List'));
        }
        
    }
    
    public Calling_List__c getCalling_List( String callingListId ) {
        Calling_List__c currentCalling_List = [ SELECT Id,
                                                      Name,
                                                      Booking_Unit__c,
                                                      Account__c,
                                                      Registration_ID__c,
                                                      Property_Name__c,
                                                      Unit_Name__c,
                                                      Booking_Unit__r.Booking__c,
                                                      Booking_Unit__r.Booking__r.Id,
                                                      Booking_Unit__r.Booking__r.Name,
                                                      Booking_Unit__r.Booking__r.Account__c,
                                                      Booking_Unit__r.Booking__r.Account__r.Email__c,
                                                      Booking_Unit__r.Booking__r.Account__r.Name
                                                 FROM Calling_List__c
                                                WHERE Id = :callingListId
                                                  ];
        return currentCalling_List;
    }
}