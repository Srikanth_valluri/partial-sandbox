@isTest
public class API_Search_User_Test {
    @testSetup static void setup() {
        Account adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        adminAccount.Agency_Type__c = 'Individual';
        insert adminAccount;
        
        Contact adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
        adminContact.Owner__c = true;
        adminContact.Agent_Representative__c= true;
        adminContact.Portal_Administrator__c= true;
        adminContact.FirstName = 'test1';
        adminContact.LastName = 'test2';
        insert adminContact;
        
        User portalUser = InitialiseTestData.getPortalUser('agentEmail@damacgroup.com.fullcopy', adminContact.Id, 'Admin');
        insert portalUser;
        
        Profile[] pcProfile = [ Select Name from Profile where name = 'Property Consultant' LIMIT 1];
        User PC = new User(Alias = 'standt3', Email='rmEmail@damacgroup.com.fullcopy', EmailEncodingKey='UTF-8', LastName='Testing', 
                            LanguageLocaleKey='en_US', isActive = true,LocaleSidKey='en_US', 
                            ProfileId = pcProfile[0].Id,TimeZoneSidKey='America/Los_Angeles', 
                            UserName='rmEmail@damacgroup.com.fullcopy');
                            
        insert pc;
        
    }
    @isTest static void searchRMorAgency () {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/API_Search_User/searchAgent';  
        req.addParameter('email', 'agentEmail@damacgroup.com.fullcopy');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        API_Search_User.searchRMorAgency();
    
    }
    
    @isTest static void searchRMorAgencyCatch () {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/API_Search_User/searchAgent';  
        req.addParameter('email', 'agentEmail1@damacgroup.com.fullcopy');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        API_Search_User.searchRMorAgency();
    
    }
    
    @isTest static void searchRM () {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/API_Search_User/searchRM';  
        req.addParameter('email', 'rmEmail@damacgroup.com.fullcopy');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        API_Search_User.searchRMorAgency();
    
    }
    
    @isTest static void searchRMCatch () {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/API_Search_User/searchRM';  
        req.addParameter('email', 'rmEmail1@damacgroup.com.fullcopy');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        API_Search_User.searchRMorAgency();
    
    }
}