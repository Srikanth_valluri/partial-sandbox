/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
 /********************************************************************************************************************************
 * Description : An apex page controller that supports self registration of users in communities that allow self registration
 *===============================================================================================================================
 * Ver      Date-DD/MM/YYYY     Author              Modification
 *===============================================================================================================================
 * 2.0      17/09/2020          Komal Shitole       Null check added for user
 * 2.1      29/12/2020          Shubham Suryawanshi Added account unblocking functionality in forgotPassword() for user whose account gets locked due to several invalid attempts from Damac Living App
*********************************************************************************************************************************/
public class CommunitiesSelfRegController {

    /*public CommunitiesSelfRegController() {

    }*/


    //TODO: update module name
    public static String MODULE_NAME = 'Change of Details';

    public AccountSearchModel model {set;get;}
    public String username  {get; set;}
    public String password  {get; set;}
    public string toastrError {get; set;}
    public string toastrMessage {get; set;}
    public Boolean showPartyId {get; set;}
    public Boolean showInfo {get; set;}
    public String partyId{get;set;}
    public String firstName {get; set;}
    public String lastName {get; set;}
    public String email {get; set;}
    public String phone {get; set;}
    public List<Network> lstNetwork = new List<Network> ();
    public List<User> lstUser =new List<User>();
    public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
    public String communityNickname {get; set { communityNickname = value == null ? value : value.trim(); } }
    public String docu{get;set;}
    public Boolean isTenant {get; set;}
    public String targetPortal {get; set;}

    public CommunitiesSelfRegController() {
        showPartyId = false;
        showInfo = true;
        isTenant = false;
        targetPortal = 'customer';
        toastrError = '';
        toastrMessage = '';
        model =  new AccountSearchModel();
    }

    public PageReference getUrlParams() {
        Map<String, String> pageParams = Apexpages.currentPage().getParameters();
        System.debug('pageParams = ' + pageParams);
        model.username = pageParams.get('username');
        toastrError = pageParams.get('error');
        toastrMessage = pageParams.get('message');
        targetPortal = pageParams.get('target');
        return NULL;
    }

    private boolean isValidPassword() {
        system.debug('model.password--'+model.firstpassword);
        system.debug('model.confirmPassword--'+model.confirmPassword);
        return model.firstpassword == model.confirmPassword;
    }
    //Method to register a user if a account is available in salesforce
    //with matching email and phone number
    public PageReference registerUser() {
        toastrError = '';
        system.debug('registerUser');
        list<Contact> lstContact = new list<Contact>();
        Id accountId;
        Id contactId;

        if (!isValidPassword()) {
            toastrError = Label.site.passwords_dont_match;
            return null;
        }if(model.partyId != null){
            system.debug('model.partyId'+model.partyId);
            lstContact = [SELECT Id,
                                 Account.Party_ID__c,
                                 AccountId
                            FROM Contact
                           WHERE Account.Party_ID__c =: model.partyId
                             AND (Is_Primary__c = true
                                 OR
                                 Account.isPersonAccount = true)];
            if(!lstContact.isEmpty() && lstContact.size() == 1){
                    Accountid = lstContact[0].AccountId;
                    partyId = lstContact[0].Account.Party_ID__c;
                    contactId = lstContact[0].id;
              }
              else{
                toastrError = 'There is no account associated with this party Id. Please enter your registered Party Id . If you have any issues please contact DAMAC Customer Service , Email DAMAC Customer Service  atyourservice@damacproperties.com or Call +9714 2375000';
                system.debug('toastrError---');
                return null;
              }
        }else{
            system.debug('model.email'+model.email);
            system.debug('model.phone'+model.phone);
            if(model.email!= null && model.phone != null ) {
                lstContact = [SELECT id,
                                     Account.Party_ID__c,
                                     AccountId
                                FROM Contact
                               WHERE (Account.Email__pc =: model.email
                                 AND Account.Mobile_Phone_Encrypt__pc =: model.phone
                                 AND Account.isPersonAccount = true)
                                 OR
                                 (Account.Email__c =: model.email
                                 AND Account.Mobile__c =: model.phone
                                 AND Is_Primary__c = true)];

            }
            if(!lstContact.isEmpty() && lstContact.size() == 1 ){
                Accountid = lstContact[0].Accountid;
                partyId = lstContact[0].Account.Party_ID__c;
                ContactId = lstContact[0].id;
            }
             if(!lstContact.isEmpty() && lstContact.size() > 1 ){
                lstContact  = [SELECT id,
                                      Account.Party_ID__c,
                                      AccountId
                                FROM contact
                               WHERE Firstname =:model.firstName
                                 AND LastName =: model.lastName
                                 AND ((Account.Email__pc =: model.email
                                       AND Account.Mobile_Phone_Encrypt__pc =: model.phone
                                       AND Account.isPersonAccount = true)
                                       OR
                                       (Account.Email__c =: model.email
                                       AND Account.Mobile__c =: model.phone
                                       AND Is_Primary__c = true))];
                if(!lstContact.isEmpty() && lstContact.size() == 1){
                    Accountid = lstContact[0].Accountid;
                    partyId = lstContact[0].Account.Party_ID__c;
                    ContactId = lstContact[0].id;
                }
                else{
                    model.hasMulipleAccounts = true;
                    system.debug('enter party id');
                    toastrError = 'Please enter the Party id :';
                    showPartyId = true;
                    showInfo = false;
                    return null;
                }
            }
        }
        system.debug('model'+model);
        //String profileId = '';//00e0Y000000R20O
        list<Profile> lstProfile;
        CustomerCommunitySettings__c customerCommunitySettings = CustomerCommunitySettings__c.getInstance();
        if(customerCommunitySettings.ProfileName__c != null){
             lstProfile =[SELECT Id,
                                 Name
                            FROM Profile
                           WHERE Name =: customerCommunitySettings.ProfileName__c];
        }
        User u = new User();
        u.Username = model.email;
        u.Email = model.email;
        u.FirstName = model.firstName;
        u.LastName = model.lastName;
        system.debug('ContactId----'+ContactId);
        u.ContactId = ContactId;
        system.debug('u.ContactId----'+u.ContactId);
        if(accountId == null){
          toastrError = 'There is no account associated with the email address and Phone number. Please enter your registered email account and phone number . If you have any issues please contact DAMAC Customer Service , Email DAMAC Customer Service  atyourservice@damacproperties.com or Call +9714 2375000';
          return null;
        }
        u.CommunityNickname = partyId+'name';
        system.debug('u.CommunityNickname---------'+u.CommunityNickname);
        if(lstProfile!= null && !lstProfile.isEmpty()){
            u.ProfileId = lstProfile[0].id;
        }else{
            toastrError = 'Profile is not available Please contact CRE.';
            return null;
        }
        u.Phone = model.phone;
        u.party_id__c = partyId;
        u.MobilePhone = model.phone;
        u.LanguageLocaleKey='en_US';
        u.LocaleSidKey='en_GB';
        u.TimeZoneSidKey='Asia/Dubai';
        u.Alias = partyId;
        u.EmailEncodingKey = 'UTF-8';
        String userId;


        try {
            userId = Site.createPortalUser(u, accountId, model.password, true);
            //System.debug('Message '+userId);
            if (userId != null) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Confirm,'An email has been sent to your registered Email Id. Please check your mail to login to Community.'));
            }
            //return null;
        } catch(Site.ExternalUserCreateException ex) {
           /* System.debug('Error1');
            List<String> errors = ex.getDisplayMessages();
            for (String error : errors)  {
                toastrError = error;
            }
            System.debug(ex.getMessage());
            return null;*/
        }
        catch (Exception e){
            System.debug('Error2');
        }
        if (userId != null) {
            if (model.password != null && model.password.length() > 1) {
               // return Site.login(u.Username, model.password, ApexPages.currentPage().getParameters().get('startURL'));
            }
            else {
                PageReference page = System.Page.CommunitiesSelfRegConfirm;
                page.setRedirect(true);
               // return page;
            }
        }
        return null;
    }


    //public PageReference registerUser() {
    //    system.debug('registerUser');
    //    list<Profile> lstProfile;
    //    list<Account> lstAccount = new list<Account>();
    //    Id Accountid;
    //    toastrError = '';

    //    system.debug('model.partyId'+model.partyId);
    //    if (!isValidPassword()) {
    //        toastrError = Label.site.passwords_dont_match;
    //        return null;
    //    }if(model.partyId != null){
    //        system.debug('model.partyId'+model.partyId);
    //        lstAccount = [SELECT id
    //                        FROM account
    //                       WHERE Party_ID__c=: model.partyId ];
    //        if(!lstAccount.isEmpty() && lstAccount.size() == 1){
    //                Accountid = lstAccount[0].id;
    //          }
    //          else{
    //            toastrError = 'There is no account associated with this party Id. Please enter your registered Party Id . If you have any issues please contact DAMAC Customer Service , Email DAMAC Customer Service  atyourservice@damacproperties.com or Call +9714 2375000';
    //            system.debug('toastrError---');
    //            return null;
    //          }
    //    }else{
    //        system.debug('model.email'+model.email);
    //        system.debug('model.phone'+model.phone);
    //        if(model.email!= null && model.phone != null ) {
    //            lstAccount = [SELECT id,
    //                                 Party_ID__c,
    //                                 Email__pc,
    //                                 Mobile_Phone_Encrypt__pc
    //                            FROM account
    //                           WHERE (Email__pc =: model.email
    //                             AND Mobile_Phone_Encrypt__pc =: model.phone
    //                             AND isPersonAccount = true)
    //                             OR (Email__c =: model.email
    //                             AND Mobile__c =: model.phone)];

    //        }
    //        system.debug('lstAccount'+lstAccount);
    //        if(!lstAccount.isEmpty() && lstAccount.size() == 1 ){
    //            Accountid = lstAccount[0].id;
    //            model.partyId = lstAccount[0].Party_ID__c;
    //            system.debug('lstAccount'+lstAccount);
    //        }
    //         if(!lstAccount.isEmpty() && lstAccount.size() > 1 ){
    //            lstAccount  = [SELECT id,
    //                                 Party_ID__c
    //                            FROM account
    //                           WHERE Email__pc =: model.email
    //                             AND Mobile_Phone_Encrypt__pc =: model.phone
    //                             AND firstname =:model.firstName
    //                             AND lastName =: model.lastName];
    //            if(!lstAccount.isEmpty() && lstAccount.size() == 1){
    //                Accountid = lstAccount[0].id;
    //                model.partyId = lstAccount[0].Party_ID__c;
    //            }
    //            else{
    //                model.hasMulipleAccounts = true;
    //                system.debug('enter party id');
    //                toastrError = 'Please enter the Party id :';
    //                showPartyId = true;
    //                showInfo = false;
    //                return null;
    //            }
    //        }
    //    }
    //    CustomerCommunitySettings__c customerCommunitySettings = CustomerCommunitySettings__c.getInstance();
    //    if(customerCommunitySettings.ProfileName__c != null){
    //         lstProfile =[SELECT Id,
    //                             Name
    //                        FROM Profile
    //                       WHERE Name =: customerCommunitySettings.ProfileName__c];
    //    }
    //    User u = new User();
    //    u.Username = model.email;
    //    u.Email = model.email;
    //    u.FirstName = model.firstName;
    //    u.LastName = model.lastName;
    //    u.Phone = model.phone;
    //    u.MobilePhone = model.phone;
    //    u.CommunityNickname = model.partyId+'name';
    //    if(accountId == null){
    //      toastrError = 'There is no account associated with the email address and Phone number. Please enter your registered email account and phone number . If you have any issues please contact DAMAC Customer Service , Email DAMAC Customer Service  atyourservice@damacproperties.com or Call +9714 2375000';
    //      return null;
    //    }
    //    if(lstProfile!= null && !lstProfile.isEmpty()){
    //        u.ProfileId = lstProfile[0].id;
    //    }else{
    //        toastrError = 'Profile is not available Please contact CRE.';
    //        return null;
    //    }
    //    String userId;
    //    try {
    //        userId = Site.createExternalUser(u, accountId, model.firstpassword, false);
    //        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Confirm,'An email has been sent to your registered Email Id. Please check your mail to login to Community.'));
    //        return null;
    //    } catch(Site.ExternalUserCreateException ex) {
    //        List<String> errors = ex.getDisplayMessages();
    //        for (String error : errors)  {
    //            toastrError = error;
    //        }
    //        System.debug(ex.getMessage());
    //        return null;
    //    }
    //    return null;
    //}

    //Method to login into community through username or partyid
    public pageReference doLogin() {
        toastrError = '';
        string strUsername = '';
        system.debug('model.username----'+model.username);
        if(model.username != null && model.username.contains('@')){
            User user = findUserByUsername(model.username);
            if(user == NULL){
                toastrError = 'The entered username is wrong please enter a correct username/PartyId';
                return null;
            } else {
                //System.debug('user.Profile.Name = ' + user.Profile.Name);
                //System.debug('TenantProfileName__c = ' + LoamsCommunitySettings__c.getInstance().TenantProfileName__c);
                if (user.Profile.Name.equalsIgnoreCase(LoamsCommunitySettings__c.getInstance().TenantProfileName__c)) {
                    isTenant = true;
                    model.password = EncodingUtil.convertToHex(Crypto.encryptWithManagedIV(
                        'AES128',
                        Crypto.generateDigest('MD5', Blob.valueOf(PaymentGateway__c.getInstance('LOAMS Portal').EncryptionKey__c)),
                        Blob.valueOf(model.password)
                    ));
                    return NULL;
                }
                strUsername = model.username;
            }
        }else{
            strUsername = fetchUsername(model.username);
            system.debug('strUsername---'+strUsername);
            if(strUsername == null){
              toastrError = 'Please enter a correct Party Id.';
              return null;
            }
        }
        Id networkId = Network.getNetworkId();
        List<Network> lstNetwork = [SELECT  Id
                                         , Name
                                     FROM  Network
                                    WHERE  Name = 'Damac Customer Community Portal'
                                        AND Id = NULL];
        if (!lstNetwork.isEmpty()) {
            networkId = lstNetwork[0].Id;
        }
        PageReference pageRef;
        if(strUsername != null && model.password != null) {
            system.debug('site login');
            system.debug('strUsername'+strUsername);
            system.debug('model.password'+model.password);
            system.debug('startURL'+ApexPages.currentPage().getParameters().get('startURL'));
            //System.debug('targetPortal = ' + targetPortal);
            //pageRef = Site.login(strUsername, model.password, '/apex/Customer');
            String startUrl = ApexPages.currentPage().getParameters().get('startURL');
            /*if ('community'.equalsIgnoreCase(targetPortal)) {
                startUrl = '/GotoCommunityPortal';
                //pageRef.getParameters().put('target', targetPortal.toLowerCase());
            }*/
            pageRef = Site.login(strUsername, model.password, startUrl);
            if(pageRef==null){
               toastrError = 'Password is wrong, Please enter a correct password';
            }
        }
        system.debug('pageRef'+pageRef);
        return pageRef;
    }//End of doLogin

    // Method to reset the password
    public PageReference forgotPassword() {
        toastrError = '';
        boolean success = false;
        string strUsername;
        system.debug('--username'+model.username);
        if(model.username == null || string.isempty(model.username)){
          toastrError = 'Please enter username';
          return null;
        }
        if (model.username != null) {
            if (model.username.contains('@')) {
                if (!checkUsername(model.username)) {
                    toastrError = 'The entered username is wrong please enter a correct username/PartyId';
                    return null;
                }
                username = model.username;
            } else {
                username = fetchUsername(model.username);
            }
            if (username!= null) {
                User user = findUserByUsername(model.username);
                if(user != null) { // null check added on 17/09/2020
                    //
                    if(!user.is_Reset_Password_Initiated__c) {
                        user.is_Reset_Password_Initiated__c = true;    //used post account unblocking due to invalid attempts - Damac Living App
                        user.Login_Invalid_Attempt_Timestamp__c = null;
                        user.Community_User_Invalid_Login_Attempts__c = 0;
                        update user;
                    }
                    if (user.Profile.Name.equalsIgnoreCase(LoamsCommunitySettings__c.getInstance().TenantProfileName__c)) {
                        isTenant = true;
                        return NULL;
                    }
                }
                toastrError = 'An email is sent to your registered email Id';
                system.debug('username---'+username);
                success = Site.forgotPassword(username);
                system.debug('success'+success);
            }
        }
        //PageReference pr = new PageReference(ApexPages.currentPage().getParameters().get('startURL'));
        //PageReference pr = new PageReference('https://devpro-servicecloudtrial-155c0807bf-1580afc5db1.cs86.force.com/Customer');
       // pr.setRedirect(true);
        if (success) {
            ApexPages.addMessage(new ApexPages.message(
                ApexPages.Severity.CONFIRM, 'An email is sent to your registered email id'
            ));
            //system.debug('pr---'+pr);
        } else {
           toastrError = 'Please enter a valid username/PartyId.';
        }
        //system.debug('pr---'+pr);
        return null;
    }//End of forgotPassword

    //Method to fetch the username if ucer enters a party Id.
    public String fetchUsername(String strPartyId){
        List<User> listuser = [SELECT Id,
                                      Username,
                                      Email,
                                      Contact.Account.Party_ID__c
                                FROM  User
                                WHERE IsActive = TRUE
                                  AND Contact.Account.Party_ID__c = :strPartyId];
        System.debug('listuser--'+listUser);
        if (listuser.isEmpty()) {
            return null;
        } else{
            return listuser[0].Username;
        }
    }//End of fetchUsername

    public User findUserByUsername(string username){
        List<User> lstUser = [
            SELECT  Id
                  , Username
                  , Profile.Name
                  , is_Reset_Password_Initiated__c
                  , Login_Invalid_Attempt_Timestamp__c
                  , Community_User_Invalid_Login_Attempts__c
            FROM    User
            WHERE   Username=:username
        ];
        if(lstUser.isEmpty()){
            return NULL;
        }else{
            return lstUser[0];
        }
    }

    public Boolean checkUsername(string username){
        System.debug('username-----'+username);
        return ![
            SELECT  Id
                    , Username
            FROM    User
            WHERE   Username = :username
        ].isEmpty();
        //List<User> listUser = [
        //    SELECT  Id
        //            , Username
        //    FROM    User
        //    WHERE   Username = :username
        //];
        //if (listUser.isEmpty()) {
        //    return false;
        //} else {
        //    system.debug('model.username----'+listUser);
        //    return true;
        //}
    }//End of checkUsername

    public PageReference gotoGuestPayment() {
        return Page.GuestMakePayment;
    }

    public PageReference reDirectToPdf(){
        PortalHelpSettings__c portalSettLst = [SELECT Document_API_Name__c
                                                 FROM PortalHelpSettings__c
                                                WHERE Name= :MODULE_NAME ];
       Document  doc = [SELECT  Id,
                                Name,
                                body ,
                                Description
                           FROM Document
                           WHERE Name = :portalSettLst.Document_API_Name__c
                           LIMIT 1
                       ];
        docu= doc.Body.toString();
        system.debug('**************'+docu);
        PageReference pageRef = new PageReference('/servlet/servlet.FileDownload?file='+doc.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }

    @RemoteAction
    public static String getOffersData() {
        return DamacDataCommunicator.getOffersData();
    }//End of getOffersData


    public class AccountSearchModel{
        public String firstName {set;get;}
        public String lastName {set;get;}
        public String email {set;get;}
        public String phone {set;get;}
        public String password {get; set {password = value == null ? value : value.trim(); } }
        public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
        public String communityNickname {get; set { communityNickname = value == null ? value : value.trim(); } }
        public String partyId {set;get;}
        public boolean hasMulipleAccounts  {get; set;}
        public Account chosenAccount {set;get;}
        public Boolean hasError;
        public String  errorMessage;
        public string username        {get;set;}
        public string password1 {get;set;}
        public string firstpassword {get;set;}

        {
            hasError = false;
        }
    }
}