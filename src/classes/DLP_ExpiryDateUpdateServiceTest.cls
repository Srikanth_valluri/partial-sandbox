@isTest
public Class DLP_ExpiryDateUpdateServiceTest {
    
    @isTest
    public static void test1() {
    
        NSIBPM__Service_Request__c sr = new NSIBPM__Service_Request__c(
                                                Agency_ID__c = '1234');
        insert sr;

        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account account = new Account(Name = 'Test Account', Email__c = 'test321@ttt.com',Send_Bulk_Email__c = true);
        insert account;

        Booking__c booking = new Booking__c(
                             Deal_SR__c = sr.Id,
                             Account__c = account.Id);
        insert booking;

        //String fmAmount = String.valueOf(Decimal.valueOf(Label.Robo_Call_FM_Outstanding_Amount) + 100.00);
        Booking_unit__c objBU = new Booking_Unit__c (Booking__c = booking.Id,
                                                     Owner__c = account.Id
                                                     ,Send_Bulk_Email__c =true
                                                     ,Unit_Name__c  = 'test/121/2424'
                                                     , DLP_End_Date__c = system.today());
        insert objBU;
        insert new CAFM_Location__c( Location_Id__c = '3232'
                                     , Description__c = 'test/121/2424'
                                     
                                   );
        insert new Credentials_Details__c( Name = 'DLP Expiry Date Update Service'
                                         , Endpoint__c = 'bc1@z.com' );
        Test.StartTest();
        Test.setMock( HttpCalloutMock.Class, new DLP_DateHttpMock( 1 ) );                             
        DLP_ExpiryDateUpdateService.getRecord( new List<String>{'test/121/2424'} );                                 
        Test.stopTest();
    }
}