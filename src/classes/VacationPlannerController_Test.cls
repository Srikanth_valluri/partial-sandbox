/*
* Name : Rahul
* Date : 21 june 2017
* Purpose : Test class for Vacation Planner Controller
* Company : NSI Gulf
* 
*/
@isTest
public class VacationPlannerController_Test {

    public static testMethod void UnitTest()
    {
        PageReference ref=page.VacationPlanner;
        VacationPlannerController controller=new VacationPlannerController();
         controller.searchUsers();
        List<user> userRecordList = TestDataFactory.createTestUserRecords(new List<User>{ new User(Managerid=system.UserInfo.getUserId(),firstname='test',lastname='test') }); 
        
        system.RunAs(new User(Id = UserInfo.getUserId())){
            controller=new VacationPlannerController();
            
            controller.saveDetails();
            controller.userList.add(new user());
            controller.saveDetails();
            controller.userSearchString='test';
            controller.searchUsers();
        }

    }
}