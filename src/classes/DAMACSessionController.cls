/**************************************************************************************************
* Name               : DamacHomeController                                               
* Description        : An apex page controller for                                              
* Created Date       : NSI - Diana                                                                        
* Created By         : 17/Jan/2017                                                                 
* Last Modified Date :                                                                            
* Last Modified By   :                                                                            
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                                                                    
* 1.0         NSI - Diana          17/Jan/2017                                                               
**************************************************************************************************/
public class DAMACSessionController{
    
    /**************************************************************************************************
            Variables used in the class
    **************************************************************************************************/
    public User uRec{get; set;}
    public Account acc{get; set;}
    public Contact con{get; set;}
    public String inquiryObjectPrefix{set;get;}
    public String caseObjectPrefix{set;get;}
    public String propertyObjectPrefix{set;get;}
    public Boolean hideTabs{set;get;}
    
    /**************************************************************************************************
    Method:         DAMACSessionController
    Description:    Constructor executing model of the class 
                    fetc user record, account, contact. to load respective image, fetch object prefix for comaprison 
                    in standard pages
                    if the RERA or TradeLicense has expired then show only home and company profile
**************************************************************************************************/
    public DAMACSessionController(){
        
       
        uRec = [SELECT id, name, accountId, account.Name, Contact.Name,Contact.FirstName,Contact.LastName,
                ContactId FROM User WHERE Id=: userInfo.getuserId()];
        if(uRec.accountId != null){
            acc = [SELECT id, name,Eligible_For_Tier_Program__c,Agency_Tier__c,Trade_License_Expiry_Date__c,
                   RERA_Expiry_Date__c
                   FROM Account WHERE id=:uRec.AccountId];
            con = [SELECT id, name, email, phone FROM Contact WHERE id=:uRec.ContactId];    
        }
        
        inquiryObjectPrefix = DamacUtility.getObjectPrefix('Inquiry__c');
        caseObjectPrefix = DamacUtility.getObjectPrefix('Case__c');
        propertyObjectPrefix = DamacUtility.getObjectPrefix('Inventory__c');

        //IF the Users Trade License or RERA is expired dont allow them to use any portal feature
        //other than Company Profile. Hiding all the tabs except Company Profile.
        if(null !=  acc && (acc.Trade_License_Expiry_Date__c < System.today() || acc.RERA_Expiry_Date__c < System.today())){
            hideTabs = true;
        }else{
            hideTabs = false;
        }

        
    }
    

}