@isTest
public with sharing class FMNotesCreationTest {

    public static testMethod void createNotes(){

        List<TriggerOnOffCustomSetting__c>settingLst2 = new List<TriggerOnOffCustomSetting__c>();
        TriggerOnOffCustomSetting__c newSetting1 = new TriggerOnOffCustomSetting__c(Name= 'CallingListTrigger',
                                                                         OnOffCheck__c = true);
          
        settingLst2.add(newSetting1);
        insert settingLst2;
        
        Id callingListRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collections').RecordTypeId;
        
        
        Account accObj = new Account(Name = 'Miss. Madina Alieva');
        insert accObj;
        
        Calling_List__c callObj = new Calling_List__c(Account__c = accObj.Id,Registration_ID__c = '12851');
        insert callObj;
        
        Calling_List__c fmCLObj = new Calling_List__c(Account__c = accObj.Id,Registration_ID__c = '84357',IsHideFromUI__c =false,
                                             RecordTypeId =callingListRecordTypeId);
        insert fmCLObj;

        Test.startTest();
            fmCLObj.CE_Comments__c = 'test remarks';
            update fmCLObj;
        Test.stopTest();
    }
    
}