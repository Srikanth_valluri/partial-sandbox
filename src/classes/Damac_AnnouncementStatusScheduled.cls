global class Damac_AnnouncementStatusScheduled implements Schedulable {
   global void execute(SchedulableContext sc) {
      Database.executebatch (new Damac_AnnouncementStatus ());
   }
}