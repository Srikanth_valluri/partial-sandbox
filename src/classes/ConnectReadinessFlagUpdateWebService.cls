/*
    - Class to integrate with Connect system to send list of units and get the readiness flag corresponding to individuals unit

    Endpoint : /services/apexrest/FlagUpdate
    Request : {
                "strBuildingCode" : "OS"
              }
    Response : [ {
                  "strUnitName" : "OS/22/2207",
                  "strPropertyCode" : "OS",
                  "ID" : "a1G25000000RmhQEAS",
                  "blnRedinessFlag" : false
                }, {
                  "strUnitName" : "OS/4/404",
                  "strPropertyCode" : "OS",
                  "ID" : "a1G25000000RmhREAS",
                  "blnRedinessFlag" : false
                }, {
                  "strUnitName" : "OS/8/801A",
                  "strPropertyCode" : "OS",
                  "ID" : "a1G0Y000000WMYcUAO",
                  "blnRedinessFlag" : true
                }, {
                  "strUnitName" : "OS/8/808A",
                  "strPropertyCode" : "OS",
                  "ID" : "a1G0Y000000WMYdUAO",
                  "blnRedinessFlag" : false
                }]
*/

@RestResource(urlMapping='/FlagUpdate/*')
global with sharing class ConnectReadinessFlagUpdateWebService {
    
    
    @HttpPost
    global static List<UnitDetailsResponseWrapper> getUnitsDetails( String strBuildingCode ) {
        
        List<UnitDetailsResponseWrapper> lstUnitDetailsResponseWrapper = new List<UnitDetailsResponseWrapper>();
        
        System.debug('-->>strBuildingCode: '+ strBuildingCode);

        
        List<Inventory__c> lstInv = [ SELECT Building_Location__r.Name
                                               , Unit_Name__c
                                               , Id
                                               , Unit_Ok_to_Send_HO_EHO_Notice__c
                                            FROM Inventory__c 
                                           WHERE Building_Location__r.Name = :strBuildingCode ];
        //String strTranscript = (String)System.JSON.deserialize(TRANSCRIPT, String.class);
     
        System.debug('-->>lstInv: '+ lstInv );
        
        if( !lstInv.isEmpty() ) {
            for( Inventory__c objInv : lstInv ) {
                if( objInv != null && String.isNotBlank( objInv.Building_Location__r.Name ) ) {
                    lstUnitDetailsResponseWrapper.add( new  UnitDetailsResponseWrapper( objInv.Id 
                                                          , objInv.Building_Location__r.Name
                                                          , objInv.Unit_Ok_to_Send_HO_EHO_Notice__c
                                                          , objInv.Unit_Name__c ) );
                }
            }
        }else {
            lstUnitDetailsResponseWrapper.add( new  UnitDetailsResponseWrapper( 'No records found against the requested Property Code.' 
                                                          , ''
                                                          , null
                                                          , '' ) );
        }
        
        //TranscriptDataFormat objTranscriptDataFormat = new TranscriptDataFormat(CALL_ID, MEETING_ID);
        
        return lstUnitDetailsResponseWrapper;
    }
    
    
    /*Request : {"arrayOfInventory": [ {
                      "strUnitName" : "OS/22/2207",
                      "strPropertyCode" : "OS",
                      "ID" : "a1G25000000RmhQEAS",
                      "blnRedinessFlag" : true
                    }, {
                      "strUnitName" : "OS/4/404",
                      "strPropertyCode" : "OS",
                      "ID" : "a1G25000000RmhREAS",
                      "blnRedinessFlag" : true
                    }, {
                      "strUnitName" : "OS/8/801A",
                      "strPropertyCode" : "OS",
                      "ID" : "a1G0Y000000WMYcUAO",
                      "blnRedinessFlag" : false
                    }, {
                      "strUnitName" : "OS/8/808A",
                      "strPropertyCode" : "OS",
                      "ID" : "a1G0Y000000WMYdUAO",
                      "blnRedinessFlag" : true
                    }]
                }
    
    Response : 
        {"statusCode":"200", "status":"Inventory Updated Sucessfully",
            "errorMessage":"",
            "arrayOfInventory":[{"attributes":{"type":"Inventory__c","url":"/services/data/v44.0/sobjects/Inventory__c/a1G25000000RmhQEAS"},
            "Property_Code__c":"OS",
            "Unit_Name__c":"OS/22/2207",
            "Id":"a1G25000000RmhQEAS",
            "Unit_Ok_to_Send_HO_EHO_Notice__c":false}]}
    
    
    */
    
    
    @HttpPatch
    global static void stampReadinessFlagInSF( List<UnitDetailsResponseWrapper> arrayOfInventory ) {
        
        map<String,Boolean> mapIdFlag = new map<String,Boolean>();
        ResponseWrapper objResponseWrapper = new ResponseWrapper();
        System.debug('-->>arrayOfInventory: '+ arrayOfInventory); 
        
        for( UnitDetailsResponseWrapper objData : arrayOfInventory ) {
            if( String.isNotBlank ( objData.ID ) ) {
                mapIdFlag.put(objData.ID,objData.blnRedinessFlag);
            }
        }
        if( !mapIdFlag.isEmpty() ) {
            List<Inventory__c> lstInvToUpdate = new List<Inventory__c>();
    
        
            for( Inventory__c objInv : [ SELECT Building_Location__r.Name
                                                   , Unit_Name__c
                                                   , Id
                                                   , Unit_Ok_to_Send_HO_EHO_Notice__c
                                                FROM Inventory__c 
                                               WHERE Id = :mapIdFlag.keySet() ] ) {
                if( mapIdFlag.containsKey( objInv.Id ) ) {
                    objInv.Unit_Ok_to_Send_HO_EHO_Notice__c = mapIdFlag.get( objInv.Id );
                    lstInvToUpdate.add(objInv);
                }
            }
            if( !lstInvToUpdate.isEmpty() && lstInvToUpdate.size() > 0 ) {
                try{
                    Database.update(lstInvToUpdate);
                    objResponseWrapper.status = 'Inventory Updated Sucessfully';
                    objResponseWrapper.arrayOfInventory = lstInvToUpdate;
                    objResponseWrapper.statusCode = '200';
                    RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                    Restcontext.response.statusCode = 200;                 
                } catch( Exception ex ) {
                    system.debug( ex.getMessage() );
                    objResponseWrapper.errorMessage = ex.getMessage();
                    //objResponseWrapper.arrayOfInventory = lstBuToUpdate;
                    objResponseWrapper.statusCode = '400';
                    RestContext.response.responseBody = Blob.valueOf(JSON.serialize(objResponseWrapper));
                    Restcontext.response.statusCode = 400;
                    System.debug('============= response : ' + JSON.serialize(objResponseWrapper));
                    return;                 
                }   
            }
        }
    }

    /**
     * Wrapper class for sending response after stamp ReadinessFlag In SF
     */
    public class ResponseWrapper {
        public String status;
        public String statusCode;
        public String errorMessage;
        public List<Inventory__c> arrayOfInventory;

        public ResponseWrapper() {
            this.status = '';
            this.statusCode = '';
            this.errorMessage = '';
            this.arrayOfInventory = new List<Inventory__c>();
        }
    }   
    
    global class UnitDetailsResponseWrapper{
    
        String ID;
        String strPropertyCode;
        Boolean blnRedinessFlag;
        String strUnitName;

        global UnitDetailsResponseWrapper(String ID, String strPropertyCode, Boolean blnRedinessFlag,String strUnitName){
            this.ID = ID;
            this.strPropertyCode = strPropertyCode;
            this.blnRedinessFlag = blnRedinessFlag;
            this.strUnitName = strUnitName;
        }
    }
}