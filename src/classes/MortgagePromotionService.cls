/****************************************************************************************
Description: Update staycation when Stacae/RP is
              true on Inventory record.
----------------------------------------------------------------------------------------
Version     Date           Author               Description                                 
1.0       3/9/2020    Ruchika Choudhary       Initial Draft
*****************************************************************************************/
global with sharing class MortgagePromotionService{
/*****************************************************************************************************************
 * Description  : Invokable Method to call getPromotionName
 * Parameter(s) : lstCase - case coming from PB
 * Return       : void
 *****************************************************************************************************************/
    @InvocableMethod
    public static void callingGetPromotionName(List<Case> lstCase){
        if(lstCase != null && !lstCase.isEmpty()){ 
            Case objCase = lstCase[0];
            if(objCase != null){
                MortgagePromotionService.getPromotionName(objCase.Registration_ID__c);
            }
        }
    }
    
/*****************************************************************************************************************
 * Description  : Method to send Htp Request to IPMS
 * Parameter(s) : paramId - Unit Id on Inventory
 *                attribute1 - Y or N value
 * Return       : void
 *****************************************************************************************************************/
    @future (Callout = true)
    global static void getPromotionName(String paramId ){
        String promotionName = 'Mortgage Interest Waiver Promotion';
        Credentials_Details__c creds = getCredentials();
        System.debug('lstCreds ' + creds);
        String userName = creds.User_Name__c;
        String password = creds.Password__c; 
        String headerValue = 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(userName + ':' + password));
        try {
            Http http = new Http();
            HttpResponse response = new HttpResponse();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(creds.Endpoint__c);
            request.setMethod('POST');
            request.setHeader('Accept', 'application/json');
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Accept-Language', 'en-US');
            request.setHeader('Authorization', headerValue);
            request.setBody('{ "PROCESS_Input": { "RESTHeader": { "Responsibility": "ONT_ICP_SUPER_USER", "RespApplication": "ONT", "SecurityGroup": "STANDARD", "NLSLanguage": "AMERICAN" }, "InputParameters": { "P_REQUEST_NUMBER": "U1231", "P_SOURCE_SYSTEM": "SFDC", "P_REQUEST_NAME": "ADD_PROMOTION", "P_REQUEST_MESSAGE": { "P_REQUEST_MESSAGE_ITEM":  { "PARAM_ID": "'+ paramId +'", "ATTRIBUTE1": "Mortgage Interest Waiver Promotion" }  } } } }');
            response = http.send(request);
            System.debug('Req Body -----' + request.getBody());
            System.debug('Response -----' + response.getBody());
            System.debug('Response -----' + response.getStatusCode());
        } catch (Exception e) {
            System.debug('Exception --- ' + e);
        }
    }

/*****************************************************************************************************************
 * Description  : Method to fetch IPMS creds and endpoint from custom setting
 * Return       : void
 *****************************************************************************************************************/
    Public Static Credentials_Details__c getCredentials() {
        List<Credentials_Details__c> lstCreds = [ SELECT
                                                 Id
                                                 , Name
                                                 , User_Name__c
                                                 , Password__c
                                                 , Endpoint__c
                                                 FROM
                                                 Credentials_Details__c
                                                 WHERE
                                                 Name = 'Promotion Name' ];
                                                 
        if( lstCreds != null && !lstCreds.isEmpty()) {
            return lstCreds[0];
        } else {
            return null;
        }
    }
}