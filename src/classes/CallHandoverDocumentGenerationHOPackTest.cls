/***********************************************************************************************
* Description - Test class developed for CallHandoverDocumentGenerationHOPack
*
* Version            Date            Author                    Description
* 1.0                17/12/17        Naresh Kaneriya (Accely)   Initial Draft
*********************************************************************************************/

@isTest
public class CallHandoverDocumentGenerationHOPackTest{

  @isTest static void getTest(){
    HandoverDocumentGenerationDtoComXsd.DocGenDTO reg =  new HandoverDocumentGenerationDtoComXsd.DocGenDTO();
    CallHandoverDocumentGenerationHOPack.HandoverDocumentGenerationResponse  obj =  new CallHandoverDocumentGenerationHOPack.HandoverDocumentGenerationResponse();
    obj.P_Template_Name = 'Test';
    obj.regist= reg ;
    
      Test.startTest();
      SOAPCalloutServiceMock.returnToMe = new Map<String,HandoverDocumentGeneration.DocGenerationResponse_element>();
      HandoverDocumentGeneration.DocGenerationResponse_element response1 = new HandoverDocumentGeneration.DocGenerationResponse_element();
      response1.return_x = 'S';
      SOAPCalloutServiceMock.returnToMe.put('response_x', response1);
      Test.setMock( WebServiceMock.class, new SOAPCalloutServiceMock());
      
       
       CallHandoverDocumentGenerationHOPack.CallHandoverDocumentGenerationHOPackName('regId','projectName','strBuildingName',
        'strUnitName','strCustomerName','strAddCity','strPassport','strBank','strProjCity','strBedroomType'); 
       Test.stopTest();

  }
}