/*******************************************************************************************************
Description : Class to send walk-in survey email and sms
--------------------------------------------------------------------------------------------------------
Version | Date(DD-MM-YYYY)  | Last Modified By   | Comments
--------------------------------------------------------------------------------------------------------
1.0     |                   |                    | Initial Draft

1.1     | 07-09-2020        |Aishwarya Todkar    | Marked duplicate on survey feedback
********************************************************************************************************/
public Class Walkin_Survey_Ctrl {

    public String CxExperience      { get; set; }
    public String emailBody      { get; set; }
    public Integer selectedRating   { get; set; }
    String parentId;
    String creId;
    String accountId;
    String templateName;

    public Walkin_Survey_Ctrl () {

        emailBody = '';

        parentId = ApexPages.currentPage().getParameters().get('id');
        //creId = ApexPages.currentPage().getParameters().get('creid');
        accountId = ApexPages.currentPage().getParameters().get('AccountId');
        templateName = ApexPages.currentPage().getParameters().get('Template');
        if( String.isNotBlank( templateName ) )
            emailBody = getSurveyEmailBody();
    }

    public String getSurveyEmailBody() {

        String strEmailBody = '';
        System.debug('templateName = ' + templateName);

        //Get Email Template
        List<EmailTemplate> listEmailTemplate = new List<EmailTemplate>( [SELECT 
                                        Id,Name, Subject, Body, HtmlValue, TemplateType
                                    FROM 
                                        EmailTemplate 
                                    WHERE 
                                        Name =: templateName limit 1
                                        ] );
        if( listEmailTemplate != null && listEmailTemplate.size() > 0 && String.isNotBlank( accountId ) ) {
            List<EmailMessage> lstEmails = new List<EmailMessage>( [ SELECT
                                                                    Id 
                                                                    , TextBody
                                                                FROM
                                                                    EmailMessage
                                                                WHERE
                                                                    Subject =: listEmailTemplate[0].Subject
                                                                AND
                                                                    RelatedToId =: accountId
                                                                Order By 
                                                                    CreatedDate DESC] );
            System.debug('lstEmails = ' + lstEmails);
            if( lstEmails!= null && lstEmails.size() > 0)
                strEmailBody = lstEmails[0].TextBody;

            System.debug( 'strEmailBody -- ' + strEmailBody);
        }
        return strEmailBody;
    }

    public PageReference submitCrmSurvey() {
        System.debug('selectedRating -' + selectedRating ); 
        
        System.debug('parentId -' + parentId ); 
        //System.debug('creId-' + creId); 
        System.debug('CxExperience -' + CxExperience );
        
        String sObjName = Id.valueOf( parentId ).getsobjecttype().getDescribe().getName();
        sObjName = sObjName.endsWith('__c') ? sObjName : sObjName + '__c';
        System.debug('sObjName -' + sObjName );
        
        CRE_Survey_Feedback__c objSurveyRes = new CRE_Survey_Feedback__c();
        objSurveyRes.put( sObjName, parentId );
        //objSurveyRes.CRE__c = creId;
        objSurveyRes.Customer_Experience__c = CxExperience;
        objSurveyRes.Rating__c = selectedRating;
        objSurveyRes.Account__c = accountId;
        objSurveyRes.CRM_Survey_Email__c = true;
        objSurveyRes.CRM_Survey_Type__c = 'Walk-In Survey';
        
        String query = 'SELECT Id FROM CRE_Survey_Feedback__c WHERE CRM_Survey_Email__c = true AND ' + sObjName + ' = \'' + parentId + '\' AND CRM_Survey_Type__c = \'Walk-In Survey\'  Limit 1';
        List<CRE_Survey_Feedback__c> listDuplicateSurveyRes = Database.query( query );
        if( listDuplicateSurveyRes != null && listDuplicateSurveyRes.size() > 0 )
            objSurveyRes.Duplicate_Feedback__c = true;
        insert objSurveyRes;
        
        System.debug( 'objSurveyRes = ' + objSurveyRes );
        return null;
    }
}