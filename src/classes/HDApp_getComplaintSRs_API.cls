/**********************************************************************************************************************
Description: This API is used for getting all the past Complaints/Suggestion SR raised by given account
=======================================================================================================================
Version | Date(DD-MM-YYYY)  | Last Modified By    | Comments
-----------------------------------------------------------------------------------------------------------------------
1.0     |   16-12-2020      | Shubham Suryawanshi | Created initial draft
***********************************************************************************************************************/
@RestResource(urlMapping='/complaintSR/getPastComplaints/*')
global class HDApp_getComplaintSRs_API {

    public static Map<Integer, String> mapStatusCode = new Map<Integer, String>{
        1 => 'Success',
        2 => 'Failure',
        3 => 'Wrong Request',
        4 => 'Username is not correct',
        5 => 'Password is not correct',
        6 => 'Something went wrong'
    };
    public static final String NO_COMPLAINTS_FOUND = 'No Complaints found'; 
    public static final String commonErrorMsg = 'This may be because of technical error that we\'re working to get fixed. Please try again later.';


    /**********************************************************************************************************************
    Description : Method to get the past complaints/suggestions
    Parameter(s):  NA
    Return Type : FinalReturnWrapper
    **********************************************************************************************************************/
    @httpGet
    global static FinalReturnWrapper getComplaintsSR() {

        RestRequest r = RestContext.request;
        System.debug('Request Headers:' + r.headers);
        System.debug('Request params:' + r.params);
        
        FinalReturnWrapper returnResponse = new FinalReturnWrapper();
        List<cls_data> objDataLst = new List<cls_data>();
        cls_meta_data objMeta = new cls_meta_data();

        if(!r.params.containsKey('accountId')) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Missing paramater : accountId', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }
        else if(r.params.containsKey('accountId') && String.isBlank(r.params.get('accountId'))) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'Missing paramater value : accountId', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;   
        }

        List<Account> account = HDAPP_Utility.getAccountFromAccountId(r.params.get('accountId'));
        if(account.isEmpty()) {
            objMeta = ReturnMetaResponse(commonErrorMsg,'No account found for given accountId', 6);
            returnResponse.meta_data = objMeta;
            return returnResponse;
        }

        //for case record
        for(Case objCase : getComplaintCases( r.params.get('accountId') )) {
            cls_data objData = new cls_data();
            
            objData.sr_number = objCase.CaseNumber;
            objData.sr_id =  objCase.id;
            objData.sr_submission_date = objCase.CreatedDate != null ? Datetime.newInstance(objCase.CreatedDate.year(), objCase.CreatedDate.month(), objCase.CreatedDate.day()).format('yyyy-MM-dd') : '';
            objData.sr_status = objCase.Status != null && objCase.Status.equalsIgnoreCase('closed') ? 'Closed' : 'Pending';
            objData.unit_name = objCase.Booking_Unit__r.Unit_Name__c;
            objData.unit_id = objCase.Booking_Unit__c;
            objData.building_name = objCase.Booking_Unit__r.Inventory__r.Building_Location__r.Building_Name__c;
            objData.complaint_type = objCase.Complaint_Type__c;
            objData.complaint_sub_type = objCase.Complaint_Sub_Type__c;
            objData.remark = objCase.Status != null && objCase.Status.equalsIgnoreCase('closed') ? 'Your request is closed' : 'Your request is in progress';

            objDataLst.add(objData);
        }

        //for Fm-Case records
        for(FM_Case__c objFMCase : getFMCaseSuggestions( r.params.get('accountId') )) {
            cls_data objData = new cls_data();
            
            objData.sr_number = objFMCase.Name;
            objData.sr_id = objFMCase.id;
            objData.sr_submission_date = objFMCase.Submission_Date__c != null ? Datetime.newInstance(objFMCase.Submission_Date__c.year(), objFMCase.Submission_Date__c.month(), objFMCase.Submission_Date__c.day()).format('yyyy-MM-dd') : '';
            objData.sr_status = objFMCase.Status__c != null && objFMCase.Status__c.equalsIgnoreCase('closed') ? 'Closed' : 'Pending';
            objData.unit_name = objFMCase.Booking_Unit__r.Unit_Name__c;
            objData.unit_id = objFMCase.Booking_Unit__c;
            objData.building_name = objFMCase.Booking_Unit__r.Inventory__r.Building_Location__r.Building_Name__c;
            objData.complaint_type = objFMCase.Suggestion_Type__c;
            objData.complaint_sub_type = objFMCase.Suggestion_Sub_Type__c;
            objData.remark = objFMCase.Status__c != null && objFMCase.Status__c.equalsIgnoreCase('closed') ? 'Your request is closed' : 'Your request is in progress';

            objDataLst.add(objData);
        }
        System.debug('objDataLst:: ' + objDataLst);
        if(objDataLst.isEmpty() || objDataLst.size() < 1) {
            objMeta = ReturnMetaResponse(NO_COMPLAINTS_FOUND, NO_COMPLAINTS_FOUND, 1);
            returnResponse.meta_data = objMeta;
            return returnResponse;    
        }

        objMeta = objMeta = ReturnMetaResponse('success', '', 1);
        //sorting as per submission date;
        objDataLst.sort();
        returnResponse.data = objDataLst;
        returnResponse.meta_data = objMeta;

        return returnResponse;
    }

    public static List<Case> getComplaintCases(String accountId) {
        List<Case> lstCaseComplaints = [SELECT id 
                                             , CaseNumber 
                                             , CreatedDate 
                                             , Status 
                                             , AccountId
                                             , Origin 
                                             , isHelloDamacAppCase__c 
                                             , Booking_Unit__c 
                                             , Booking_Unit__r.Unit_Name__c 
                                             , Booking_Unit__r.Inventory__r.Building_Location__r.Building_Name__c
                                             , Complaint_Type__c 
                                             , Complaint_Sub_Type__c
                                        FROM Case
                                        WHERE AccountId =: accountId 
                                        AND RecordType.Name = 'Complaint'];
        System.debug('lstCaseComplaints: ' + lstCaseComplaints);
        return lstCaseComplaints;
    }

    public static List<FM_Case__c> getFMCaseSuggestions(String accountId) {
        //Where Type__c = 'Complaint' is used to exclude 'Suggestion' from the Past Comaplaints listing
        List<FM_Case__c> lstFmSugestions = [SELECT id 
                                                 , Name 
                                                 , Account__c
                                                 , Origin__c
                                                 , Type__c
                                                 , isHelloDamacAppCase__c
                                                 , Approval_Status__c
                                                 , Booking_Unit__c 
                                                 , Booking_Unit__r.Unit_Name__c 
                                                 , Booking_Unit__r.Inventory__r.Building_Location__r.Building_Name__c
                                                 , Submission_Date__c 
                                                 , Status__c 
                                                 , Suggestion_Type__c
                                                 , Suggestion_Sub_Type__c
                                            FROM FM_Case__c
                                            WHERE Account__c =: accountId
                                            AND Type__c = 'Complaint'];
        System.debug('lstFmSugestions: ' + lstFmSugestions);
        return lstFmSugestions;

    }

    public static cls_meta_data ReturnMetaResponse(String message, String devMsg, Integer statusCode) {
        cls_meta_data retMeta = new cls_meta_data();
        retMeta.message = message;
        retMeta.status_code = statusCode;
        retMeta.title = mapStatusCode.get(statusCode);
        retMeta.developer_message = devMsg;
        return retMeta;
    }

    global class FinalReturnWrapper {
        public cls_data[] data;
        public cls_meta_data meta_data;
    }

    public class cls_meta_data {
        public String message;
        public Integer status_code;
        public String title;
        public String developer_message;   
    }

    public class cls_data implements Comparable {
        public String sr_number;
        public String sr_id;
        public String sr_submission_date;
        public String sr_status;
        public String unit_name;
        public String unit_id;
        public String building_name;
        public String complaint_type;
        public String complaint_sub_type;
        public String remark;

        public Integer compareTo(Object compareTo) {
            cls_data compareInstance = (cls_data)compareTo;
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if (Date.valueOf(sr_submission_date) > Date.ValueOf(compareInstance.sr_submission_date)) {
                // Set return value to a positive value.
                returnValue = -1;
            } else if (Date.valueOf(sr_submission_date) < Date.ValueOf(compareInstance.sr_submission_date)) {
                // Set return value to a negative value.
                returnValue = 1;
            }
            return returnValue;       
        }
    }
}