/*
 * Description: Class used to create FM Calling list for unit 
    - which is handed over OR early handed  AND
    - which are active AND
    - whose total due is more than 5k
 */
 
global class FMCallingListCreationBatch implements Database.Batchable<sObject>,Database.AllowsCallouts {
    
    global static Id FMCollectionRecordTypeId = Schema.SobjectType.Calling_List__c.RecordTypeInfosByName.get('FM Collections').RecordTypeId;
    String query;
    
    global Database.QueryLocator start(Database.BatchableContext BC) {

        //Getting Active status values for booking units
        List<Booking_Unit_Active_Status__c> csActiveValues = Booking_Unit_Active_Status__c.getall().values();
        List<String> lstActiveStatus = new List<String>();
        for(Booking_Unit_Active_Status__c cs : csActiveValues) {
            lstActiveStatus.add(cs.Status_Value__c);
        }
    
        query = 'SELECT Id ,Registration_ID__c FROM Booking_Unit__c WHERE ( Handover_Flag__c = \'Y\' OR Early_Handover__c = true) AND Registration_Status__c IN : lstActiveStatus ';
        
        //AND Id = \'a0x25000000B93S\'
        
        system.debug( ' query : ' + query );
        
        return Database.getQueryLocator(query);
    }   

    global void execute( Database.BatchableContext BC, List<Booking_Unit__c> lstBU ) {
        system.debug( ' lstBU : ' + lstBU );
        List<Calling_List__c> lstFMCL = new List<Calling_List__c>();
        if( lstBU != null && lstBU.size() > 0 ) {
            Id FM_Collection_QueueId = EmailMessageTriggerHandler.getGroupIdFromDeveloperName( 'FM_Collection_Queue','Queue' ); 
            for( Booking_Unit__c objBooking_Unit : lstBU ) {
                if( String.isNotBlank( objBooking_Unit.Registration_ID__c ) ) {
                    FmIpmsRestServices.DueInvoicesResult dues = fetchFmDues(objBooking_Unit.Registration_ID__c , objBooking_Unit.Id );
                    system.debug( ' dues : ' + dues );
                    if( dues.totalDueAmount >= Label.Due_Amount_For_Creation_of_FM_CL  ) {
                        //create FM CL
                        Calling_List__c objFMCL = new Calling_List__c();
                        objFMCL.RecordTypeId = FMCollectionRecordTypeId;
                        objFMCL.Registration_ID__c = objBooking_Unit.Registration_ID__c;
                        objFMCL.Calling_List_Type__c = 'FM Calling List';
                        objFMCL.OwnerId =  FM_Collection_QueueId;
                        
                        objFMCL = populateInvDues( objFMCL , dues.lstDueInvoice,true);
                        
                        lstFMCL.add(objFMCL);
                    }
                }
            }
            if( lstFMCL != null && lstFMCL.size() > 0 ) {
                insert lstFMCL;
            }
            
        }
    }
    
    public static Calling_List__c populateInvDues( Calling_List__c objFMCL 
                                                 , List<FmIpmsRestServices.DueInvoice> lstDueInvoice
                                                 , Boolean isCLInsert) {
        system.debug('<<<<<<<<<<<<< objFMCL : ' + objFMCL );
        system.debug('<<<<<<<<<<<<< lstDueInvoice : ' + lstDueInvoice );
        Decimal due0To30Days = 0.00;
        Decimal due30To60Days = 0.00;
        Decimal due60To90Days = 0.00;
        Decimal due90To180Days = 0.00;
        Decimal due180To360Days = 0.00;
        Decimal dueMoreThan360Days = 0.00;
        Decimal INV_DUE = 0.00;
        Decimal Total_Charges = 0.00;
        Decimal notDue = 0.00;
        Date today = Date.today();
        Integer currentMonth = today.month();

        for( FmIpmsRestServices.DueInvoice objDueInv : lstDueInvoice ) {
            if( objDueInv.dueRemaining != null ) {
                if(  objDueInv.dueDays >= 0 &&  objDueInv.dueDays <= 30 ) {
                    //due0To30Days += objDueInv.dueRemaining;
                    system.debug('<<<<<<<<<<<<< due0To30Days : ' + due0To30Days );
                    system.debug('<<<<<<<<<<<<< objDueInv.dueDate : ' + objDueInv.dueDate );
                    if( String.isNotBlank(objDueInv.dueDate) ) {
                        system.debug('<<<<<<<<<<<<< currentMonth : ' + currentMonth );
                        system.debug('<<<<<<<<<<<<< objDueInv.dueRemaining : ' + objDueInv.dueRemaining );
                        
                        due0To30Days = getDueFutureWithCurrent(due0To30Days,objDueInv.dueDate,currentMonth,objDueInv.dueRemaining);
                    }
                    objFMCL.DUE_0_30_DAYS__c = due0To30Days;
                }else if (  objDueInv.dueDays > 30 &&  objDueInv.dueDays <= 60 ) {
                    //due30To60Days += objDueInv.dueRemaining;
                    if( String.isNotBlank(objDueInv.dueDate) ) {
                        due30To60Days = getDueFutureWithCurrent(due30To60Days,objDueInv.dueDate,currentMonth,objDueInv.dueRemaining);
                    }
                    objFMCL.DUE_30_60_DAYS__c = due30To60Days;
                }else if (  objDueInv.dueDays > 60 &&  objDueInv.dueDays <= 90 ) {
                    //due60To90Days += objDueInv.dueRemaining;
                    if( String.isNotBlank(objDueInv.dueDate) ) {
                        due60To90Days = getDueFutureWithCurrent(due60To90Days,objDueInv.dueDate,currentMonth,objDueInv.dueRemaining);
                    }           
                    objFMCL.DUE_60_90_DAYS__c = due60To90Days;
                }else if (  objDueInv.dueDays > 90 &&  objDueInv.dueDays <= 180 ) {
                    //due90To180Days += objDueInv.dueRemaining;
                    if( String.isNotBlank(objDueInv.dueDate) ) {
                        due90To180Days = getDueFutureWithCurrent(due90To180Days,objDueInv.dueDate,currentMonth,objDueInv.dueRemaining);
                    }
                    objFMCL.DUE_90_180_DAYS__c = due90To180Days;
                }else if (  objDueInv.dueDays > 180 &&  objDueInv.dueDays <= 360 ) {
                    //due180To360Days += objDueInv.dueRemaining;
                    if( String.isNotBlank(objDueInv.dueDate) ) {
                        due180To360Days = getDueFutureWithCurrent(due180To360Days,objDueInv.dueDate,currentMonth,objDueInv.dueRemaining);
                    }
                    objFMCL.DUE_180_360_DAYS__c = due180To360Days;
                }else if (  objDueInv.dueDays  > 360 ) {
                    //dueMoreThan360Days += objDueInv.dueRemaining;
                    if( String.isNotBlank(objDueInv.dueDate) ) {
                        dueMoreThan360Days = getDueFutureWithCurrent(dueMoreThan360Days,objDueInv.dueDate,currentMonth,objDueInv.dueRemaining);
                    }                   
                    objFMCL.DUE_MORE_THAN_360_DAYS__c = dueMoreThan360Days;
                } else if( objDueInv.dueDays  < 0 ) {
                    notDue += objDueInv.dueRemaining;
                    objFMCL.NOT_DUE__c = notDue;
                }
                
                if( objDueInv.dueDays >= -30 ) {
                    INV_DUE += objDueInv.dueRemaining;
                }
                
                objFMCL.Inv_Due__c = INV_DUE;
                if( isCLInsert ) {
                    objFMCL.Invoice_Original_Due__c = INV_DUE ;
                    objFMCL.Collection_Percent__c = ((INV_DUE - INV_DUE) * 100 / INV_DUE );
                }else {
                    if( objFMCL.Invoice_Original_Due__c != null ) {
                        objFMCL.Collection_Percent__c = ((objFMCL.Invoice_Original_Due__c - INV_DUE) * 100 / objFMCL.Invoice_Original_Due__c );
                    }
                }
            }
            if( objDueInv.paidAmount != null ) {
                objFMCL.Amount_Paid__c = Integer.valueOf( objDueInv.paidAmount );
            }
            if( objDueInv.chargeAmount != null ) {
                Total_Charges += objDueInv.chargeAmount;
                objFMCL.Total_Charges__c = Total_Charges;
            }
        }
        return objFMCL;
    }

    public static Decimal getDueFutureWithCurrent( Decimal decVal , String dueDates , Integer currentMonth ,Decimal dueRemaining ) {
        
        Date dueDate = CustomerCommunityUtils.parseIpmsDateString(dueDates);
        system.debug('<<<<<<<<<<<<< dueDate : ' + dueDate );
        if ( dueDate != NULL ) {
            Integer dueMonth = dueDate.month();
            system.debug('<<<<<<<<<<<<< dueMonth : ' + dueMonth );
            Date nextMonthDate = Date.today().addMonths(1);
            system.debug('<<<<<<<<<<<<< nextMonthDate : ' + nextMonthDate );
            system.debug('<<<<<<<<<<<<< Math.Mod(currentMonth, 3) : ' + Math.Mod(currentMonth, 3) );
            if (dueMonth <= currentMonth
                || (Math.Mod(currentMonth, 3) == 0
                    && dueMonth == nextMonthDate.month() && dueDate.year() == nextMonthDate.year()
                )
            ) {
                decVal += dueRemaining;
            }
        }
        system.debug('<<<<<<<<<<<<< decVal : ' + decVal );
        return decVal;
    }
    
    public static FmIpmsRestServices.DueInvoicesResult fetchFmDues( String strRegId , Id buId) {
        FmIpmsRestServices.DueInvoicesResult dues;
        
        if( String.isNotBlank( strRegId ) ) {
            try {
                dues = FmIpmsRestServices.getDueInvoices(strRegId, '', '');
            } catch(Exception excp) {
                System.debug('excp = ' + excp);
                errorLogger( excp.getMessage()+' at ' +String.valueOf( excp.getLineNumber() ) , buId );
            }
        }
        return dues;
    }   
    
    private static void errorLogger(string strErrorMessage,Id strBookingUnitID){
        Error_Log__c objError = new Error_Log__c();
        objError.Error_Details__c = strErrorMessage;
        if( strBookingUnitID != null ){
            objError.Booking_unit__c = strBookingUnitID;
        }
        insert objError;
    }
    
    global void finish(Database.BatchableContext BC) {

    }
}