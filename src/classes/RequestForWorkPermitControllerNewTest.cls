/**
 * @File Name          : RequestForWorkPermitControllerNewTest.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 8/14/2019, 3:27:37 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    8/14/2019, 2:45:53 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
public class RequestForWorkPermitControllerNewTest {
    public static testmethod void createFMCase(){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;
        
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;
        
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;
         
        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;
        
        PageReference myVfPage = Page.RequestForWorkPermitProcessPage;
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Contractor_Type__c='Cleaning Contractor';
        fmCaseObj.Account__c = acctIns.id;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Apply_for_Work_Permits');
        //ApexPages.currentPage().getParameters().put('Id',Opp.Id);
        RequestForWorkPermitControllerNew obj=new RequestForWorkPermitControllerNew();
        obj.objFMCase=fmCaseObj;
        obj.createRequestForWorkPermit();
    }
    
    public static testmethod void submitFMCase(){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;
        
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
       
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;
        
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;
         
        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        fmUser.fm_role__c ='FM Manager';
        insert fmUser;
        
        PageReference myVfPage = Page.RequestForWorkPermitProcessPage;
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Email__c = 'test@test.com';
        fmCaseObj.Contractor_Type__c='Cleaning Contractor';
        fmCaseObj.Request_Type_DeveloperName__c='Apply_for_Work_Permits';
        fmCaseObj.Permit_To_Work_For__c='Hot Works;Confined Spaces';
        insert fmCaseObj;

        Contractor_Information__c contractorObj = new Contractor_Information__c();
        contractorObj.Employee_Name__c = 'test';
        contractorObj.Emirates_ID__c = '123121';
        contractorObj.Employee_Mobile_Number__c = '212';
        contractorObj.Company_Name__c = 'Test';
        insert contractorObj;
        
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Apply_for_Work_Permits');
        ApexPages.currentPage().getParameters().put('noOfEmp','1');
        //ApexPages.currentPage().getParameters().put('Id',Opp.Id);
        RequestForWorkPermitControllerNew  obj=new RequestForWorkPermitControllerNew();
        obj.objFMCase.id=fmCaseObj.id;
        obj.createContractorInformation();
        obj.submitRequestForWorkPermit();
        //obj.returnBackToCasePage();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';
        obj.createCaseShowUploadDoc();
        
    }
    
    public static testmethod void createFMCaseExistingCase(){
        Id RecTypeFMCase = Schema.SObjectType.FM_Case__c.getRecordTypeInfosByName().get('NOC For FitOut').getRecordTypeId();
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;
        
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;
        
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;
         
        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;
        
        PageReference myVfPage = Page.RequestForWorkPermitProcessPage;
        FM_Case__c fmCaseIns=new Fm_Case__c();
        fmCaseIns.recordtypeid=RecTypeFMCase;
        fmCaseIns.Account__c=acctIns.id;
        fmCaseIns.Booking_Unit__c=buIns.id;
        fmCaseIns.status__c='Closed';
        fmCaseIns.Approval_Status__c='Approved';
        //fmCaseIns.Request_Type__c='Work Permit';
        fmCaseIns.Permit_To_Work_For__c='Hot Works;Confined Spaces';
        insert fmCaseIns;
        //Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contractor_Type__c='Cleaning Contractor';
        fmCaseObj.Contact_person_contractor__c='test';
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Apply_for_Work_Permits');
         //ApexPages.currentPage().getParameters().put('noOfEmp','1');
        //ApexPages.currentPage().getParameters().put('Id',Opp.Id);
        RequestForWorkPermitControllerNew obj=new RequestForWorkPermitControllerNew();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';
        //obj.objFMCase=fmCaseObj;
        test.startTest();
        obj.strFMCaseId=fmCaseObj.id;
        //obj.uploadDocument();

        obj.notifyContractorConsultant();
        obj.createRequestForWorkPermit();
        test.stoptest();
    }
    
    public static testmethod void testInsertAttachment(){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        Account acctIns = TestDataFactoryFM.createAccount();
        insert acctIns;
        
        NSIBPM__Service_Request__c sr = TestDataFactoryFM.createServiceRequest(acctIns);
        insert sr;
        
        Booking__c  bk = TestDataFactoryFM.createBooking(acctIns,sr);
        insert bk;
        
        Location__c locObj = TestDataFactoryFM.createLocation();
        insert locObj;
        
        Inventory__c invObj = TestDataFactoryFM.createInventory(locObj);
        insert invObj;
        
        Booking_Unit__c buIns = TestDataFactoryFM.createBookingUnit(acctIns,bk);
        buIns.Inventory__c = invObj.id;
        insert buIns;
        
        User u = TestDataFactoryFM.createUser(profileId.id);
        insert u;
         
        FM_User__c fmUser= TestDataFactoryFM.createFMUser(u,locObj);
        insert fmUser;
        
        PageReference myVfPage = Page.RequestForWorkPermitProcessPage;
        FM_Case__c fmCaseObj=new FM_Case__c();
        fmCaseObj.Issue_Date__c=Date.today();
        fmCaseObj.Contact_person__c='test';
        fmCaseObj.Description__c='test';
        fmCaseObj.Contact_person_contractor__c='test';
        fmCaseObj.Contractor_Type__c='Cleaning Contractor';
        fmCaseObj.Request_Type_DeveloperName__c='Apply_for_Work_Permits';
        fmCaseObj.Permit_To_Work_For__c='Hot Works;Confined Spaces';
        insert fmCaseObj;

        Contractor_Information__c contractorObj = new Contractor_Information__c();
        contractorObj.Employee_Name__c = 'test';
        contractorObj.Emirates_ID__c = '123121';
        contractorObj.Employee_Mobile_Number__c = '212';
        contractorObj.Company_Name__c = 'Test';
        insert contractorObj;
        
        Test.setMock( WebServiceMock.class, new MoveOutMockClass() );
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('AccountId',acctIns.Id);
        ApexPages.currentPage().getParameters().put('UnitId',buIns.Id);
        ApexPages.currentPage().getParameters().put('SRType','Apply_for_Work_Permits');
         ApexPages.currentPage().getParameters().put('noOfEmp','1');
        //ApexPages.currentPage().getParameters().put('Id',Opp.Id);
        RequestForWorkPermitControllerNew  obj=new RequestForWorkPermitControllerNew();
        obj.strDocumentBody='ZGF0YTp0ZXh0L2h0bWw7YmFzZTY0LFBDRk';
        obj.strDocumentName='Test_Document_Work_Permit.htm';
        test.startTest();
        obj.strFMCaseId=fmCaseObj.id;
        obj.objFMCase=fmCaseObj;
        obj.uploadDocument();
        obj.createContractorInformation();
        obj.submitRequestForWorkPermit();
        test.stoptest();
    }
}