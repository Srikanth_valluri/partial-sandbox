/**************************************************************************************************
* Name               : AgentPortalProfileControllerTest
* Description        :                                               
* Created Date       : Naresh(Accely)                                                                       
* Created By         : 1/09/2017                                                                 
* Last Modified Date :                                                                            
* Last Modified By   :                                                                            
* ----------------------------------------------------------------------------------------------- 
* VERSION     AUTHOR                    DATE                                                                    
* 1.1         Naresh kaneriya      1/09/2017                                                             
**************************************************************************************************/
@isTest(seeAllData=false)
public class AgentPortalProfileControllerTest{

    public static User portalUser;
    public static Contact adminContact;
    public static Account adminAccount;
    public static User portalOnlyAgent;

    static void init(){
        adminAccount = InitialiseTestData.getAdminAccount(50000000,'Test agency');
        insert adminAccount;
        
        adminContact = InitialiseTestData.getAdminContact('Test Contact',adminAccount.Id);
        insert adminContact;
        
        Contact agentContact = InitialiseTestData.getAgentContact('Test Agent', adminAccount.Id);
        insert agentContact;
        
        portalUser = InitialiseTestData.getPortalUser('test098@test.com', adminContact.Id, 'Admin');
        portalOnlyAgent = InitialiseTestData.getPortalUser('test109@test.com', agentContact.Id, 'Agent');
    }


    @isTest static void showCILForAdmin(){
        Test.startTest();
        init();
        System.runAs(portalUser){
           AgentPortalProfileController  obj = new AgentPortalProfileController();
           obj.isSRPending = true;
           obj.showdetailsNone = true;
           obj.serviceRequestID ='Test';
           ApexPages.currentPage().getParameters().put('Accid',adminAccount.Id);  
           AgentPortalProfileController.init();
        }
        Test.stopTest();
    }

    @isTest static void testMethod1(){
        Test.startTest();
        init();
        System.runAs(portalOnlyAgent){
           AgentPortalProfileController  obj = new AgentPortalProfileController();
           obj.isSRPending = true;
           obj.showdetailsNone = true;
           obj.serviceRequestID ='Test';
           ApexPages.currentPage().getParameters().put('Accid',adminAccount.Id);  
           AgentPortalProfileController.init();
        }
        Test.stopTest();
    }
}