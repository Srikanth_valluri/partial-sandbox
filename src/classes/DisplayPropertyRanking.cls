/**************************************************************************************************
* Name               : DisplayPropertyRanking
* Description        : An apex page controller for DisplayPropertyRanking
* Created Date       : 12/08/2018
* Created By         : Nikhil Pote
* Last Modified Date :
* Last Modified By   : 
* -----------------------------------------------------------------------------------------------
* VERSION     AUTHOR                    DATE
* 1.0         Nikhil Pote               12/08/2018
**************************************************************************************************/

public class DisplayPropertyRanking {

    public list<Property_Ranking__c> lstPropRankingDisplay{get;set;}
    public list<AnnouncementsWrapper> recordsList { get; set;}
    public list<AnnouncementsWrapper> tempWrapperList = new list<AnnouncementsWrapper>();
    public string selectedValue{get;set;}

    /*Contructor to get Properties based on Rank & ignore ranking calculation criteria*/
    
    public DisplayPropertyRanking(AgentPortalNewLaunches controller) {
        system.debug('selectedValue--'+selectedValue);
        recordsList = new list<AnnouncementsWrapper>();
        list<Property_Ranking__c> lstPropRanking = new list<Property_Ranking__c>();
        lstPropRanking = [SELECT id, Inventory__c,Inventory__r.Bedroom_Type__c,Inventory__r.Marketing_Name_Doc__r.Primary_Image__c,Inventory__r.Marketing_Name_Doc__r.name,Inventory__r.Marketing_Name_Doc__r.Marketing_Plan__c,Inventory__r.Property__r.Property_Plan__c,name, Rank__c,Ignore_Calculation__c,createddate 
                          FROM  Property_Ranking__c 
                          WHERE CreatedDate = LAST_N_DAYS:7 Order By createddate ASC];
        system.debug('lstPropRanking++++++++'+lstPropRanking);
        
        list<Property_Ranking__c> lstPropRankingIgnCal = new list<Property_Ranking__c>();
        if(!lstPropRanking.isEmpty()) {
            for(Property_Ranking__c objPropRank: lstPropRanking) {
                if(objPropRank.Ignore_Calculation__c){
                    lstPropRankingIgnCal.add(objPropRank);
                }
            }
        }
        system.debug('lstPropRankingIgnCal++++++++'+lstPropRankingIgnCal);
        
        list<Property_Ranking__c> lstPropRankingRank = new list<Property_Ranking__c>();
        lstPropRankingRank = [SELECT id, Inventory__c,Inventory__r.Bedroom_Type__c,Inventory__r.Marketing_Name_Doc__r.Primary_Image__c,Inventory__r.Marketing_Name_Doc__r.name,Inventory__r.Marketing_Name_Doc__r.Marketing_Plan__c,Inventory__r.Property__r.Property_Plan__c,name,Rank__c ,Ignore_Calculation__c,createddate 
                              FROM Property_Ranking__c 
                              WHERE id in:lstPropRanking 
                              AND Ignore_Calculation__c = False 
                              AND CreatedDate = LAST_N_DAYS:7 
                              Order By Rank__c ASC];
        system.debug('lstPropRankingRank ++++++++'+lstPropRankingRank );
        lstPropRankingDisplay = new list<Property_Ranking__c>();
        lstPropRankingDisplay.addAll(lstPropRankingIgnCal);  
        system.debug('lstPropRankingIgnCal========'+lstPropRankingIgnCal.size());
        system.debug('lstPropRankingRank size ++++++++'+lstPropRankingRank.size());
        Integer PropertyRankingTortalSize = lstPropRankingIgnCal.size() + lstPropRankingRank.size();
        system.debug('PropertyRankingTortalSize========'+PropertyRankingTortalSize);
        Integer PropertyRankingSize = PropertyRankingTortalSize < 20 ? PropertyRankingTortalSize: 20;
        system.debug('PropertyRankingSize========'+PropertyRankingSize);
        Integer counter = PropertyRankingSize - lstPropRankingIgnCal.size();
        system.debug('counter========'+counter);
        if(!lstPropRankingRank.isEmpty()) {
            for(Integer i=0; i<counter; i++) {
                lstPropRankingDisplay.add(lstPropRankingRank[i]);
            }
        }
        system.debug('lstPropRankingDisplay========'+lstPropRankingDisplay.size()); 
        
        if(!lstPropRankingDisplay.isEmpty()) {
            for(Property_Ranking__c objPR: lstPropRankingDisplay) {
                AnnouncementsWrapper objWrapper = new AnnouncementsWrapper();
                if(objPR.Inventory__r.Marketing_Name_Doc__r.Primary_Image__c){
                    objWrapper.image = objPR.Inventory__r.Marketing_Name_Doc__r.Marketing_Plan__c;
                } else {
                    objWrapper.image = objPR.Inventory__r.Property__r.Property_Plan__c;
                }
                objWrapper.dateToDisplay = date.valueof(objPR.CreatedDate);
                objWrapper.type = 'Popular Properties';
                objWrapper.name = objPR.Inventory__r.Marketing_Name_Doc__r.name;
                objWrapper.bedroomType = objPR.Inventory__r.Bedroom_Type__c;
                recordsList.add(objWrapper);
            }   
        }     
        system.debug('recordsList----------'+recordsList);
        
        List<User> userList = [SELECT Id,ContactId, AccountId, Profile.Name,
                                    Account.Broker_Class__c,
                                    Account.recordTypeId, 
                                    Account.Country__c,
                                    Account.Agency_Corporate_Type__c,
                                    Account.Agency_Type__c
                                FROM User 
                                WHERE ID =:UserInfo.getUserId() 
                                LIMIT 1];                       

        list<Announcement__c> lstAnnouncementNewLaunches = new list<Announcement__c>();
        lstAnnouncementNewLaunches = [SELECT
                                Id,
                                Start_Date__c,
                                End_Date__c,
                                Name,
                                CreatedDate,
                                Title__c,
                                Type__c,
                                Description__c
                            FROM 
                                Announcement__c
                            WHERE
                                Start_Date__c <= today                                

                            AND 
                                End_Date__c >= today

                            AND Type__c = 'New Launches'

                            AND 
                                Active__c = true 
                            AND (
                                Account__r.Id =: userList[0].AccountId
                                OR
                                (
                                    Account__r.Agency_Type__c = :userList[0].Account.Agency_Type__c AND
                                    Account__r.Agency_Corporate_Type__c =: userList[0].Account.Agency_Corporate_Type__c AND
                                    Account__r.Country__c =: userList[0].Account.Country__c AND
                                    Account__r.Broker_Class__c =: userList[0].Account.Broker_Class__c AND
                                    Account__r.recordTypeId =: userList[0].Account.recordTypeId
                                )
                            ) limit 20];
        system.debug('lstAnnouncementNewLaunches---'+lstAnnouncementNewLaunches.size());
        if(!lstAnnouncementNewLaunches.isEmpty()) {
            for(Announcement__c objAnn: lstAnnouncementNewLaunches) {
                AnnouncementsWrapper objWrapper = new AnnouncementsWrapper();
                system.debug('objAnn.Description__c'+objAnn.Description__c);
                if(objAnn.Description__c !=NULL) {
                    String temp = objAnn.Description__c.substringAfter('src=');
                    system.debug('tem---1'+temp);
                    temp = temp.substringBefore('%20style');
                    system.debug('tem---2'+temp);
                    temp = temp.substringBetween('"');
                    system.debug('tem---3'+temp);
                    temp = temp.replace('&amp;', '&');
                    system.debug('tem---4'+temp);
                    objWrapper.image = temp;
                } else {
                    objWrapper.image = '';
                }
                objWrapper.dateToDisplay = objAnn.Start_Date__c;
                objWrapper.type = objAnn.Type__c;
                objWrapper.name = objAnn.Title__c;
                objWrapper.bedroomType = '';
                recordsList.add(objWrapper);
            }
        }
        system.debug('recordsList----------'+recordsList);

        list<Announcement__c> lstAnnouncementOffers = new list<Announcement__c>();
        lstAnnouncementOffers = [SELECT
                                Id,
                                Start_Date__c,
                                End_Date__c,
                                Name,
                                Type__c,
                                CreatedDate,
                                Title__c,
                                Description__c
                            FROM 
                                Announcement__c
                            WHERE
                                Start_Date__c <= today                                

                            AND 
                                End_Date__c >= today

                            AND Type__c = 'Offer'

                            AND 
                                Active__c = true 
                            AND (
                                Account__r.Id =: userList[0].AccountId
                                OR
                                (
                                    Account__r.Agency_Type__c = :userList[0].Account.Agency_Type__c AND
                                    Account__r.Agency_Corporate_Type__c =: userList[0].Account.Agency_Corporate_Type__c AND
                                    Account__r.Country__c =: userList[0].Account.Country__c AND
                                    Account__r.Broker_Class__c =: userList[0].Account.Broker_Class__c AND
                                    Account__r.recordTypeId =: userList[0].Account.recordTypeId
                                )
                            ) limit 20];
        system.debug('lstAnnouncementOffers---'+lstAnnouncementOffers.size());
        if(!lstAnnouncementOffers.isEmpty()) {
            for(Announcement__c objAnn: lstAnnouncementOffers) {
                AnnouncementsWrapper objWrapper = new AnnouncementsWrapper();
                if(objAnn.Description__c !=NULL) {
                    String temp = objAnn.Description__c.substringAfter('src=');
                    system.debug('tem---1'+temp);
                    temp = temp.substringBefore('%20style');
                    system.debug('tem---2'+temp);
                    temp = temp.substringBetween('"');
                    system.debug('tem---3'+temp);
                    temp = temp.replace('&amp;', '&');
                    system.debug('tem---4'+temp);
                    objWrapper.image = temp;
                } else {
                    objWrapper.image = '';
                }
                objWrapper.dateToDisplay = objAnn.Start_Date__c;
                objWrapper.type = objAnn.Type__c;
                objWrapper.name = objAnn.Title__c;
                objWrapper.bedroomType = '';
                recordsList.add(objWrapper);
            }
        }
        system.debug('recordsList----------'+recordsList.size());
        tempWrapperList.addAll(recordsList);
    }

    /****************************************************************************************
    * Method : To get Announcement dropdown values
    ****************************************************************************************/
    public List<SelectOption> getDropdownItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('All Announcements','All Announcements'));
        options.add(new SelectOption('Offer','Offer'));
        options.add(new SelectOption('New Launches','New Launches'));
        options.add(new SelectOption('Popular Properties','Popular Properties'));
        return options;
    }

    /****************************************************************************************
    * Method : Wrapper to store Announcements data based on type
    ****************************************************************************************/
    public class AnnouncementsWrapper{
        public string image { get; set;}
        public date dateToDisplay  { get; set;}
        public string type { get; set;}
        public string name { get; set;}
        public string bedroomType { get; set;}        
    }

    /****************************************************************************************
    * Method : To populated announcements based on selected value
    ****************************************************************************************/
    public void populateSelectedAnnouncements() {
        system.debug('selectedValue--'+selectedValue);
        recordsList = new list<AnnouncementsWrapper>();
        if(selectedValue.equals('All Announcements')) {
                recordsList.addAll(tempWrapperList);
                system.debug('recordsList type-------All---'+recordsList.size());
        } else {
            if(!tempWrapperList.isEmpty()) {
                for(AnnouncementsWrapper objWrapp: tempWrapperList) {
                    if(selectedValue.equals(objWrapp.type)){
                        recordsList.add(objWrapp);
                    }
                }
            }
        }
        system.debug('recordsList type----------'+recordsList.size());
        //return recordsList;
    }
}