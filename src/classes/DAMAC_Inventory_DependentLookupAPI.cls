/***************************************************************************************************
 * @Name              : DAMAC_Inventory_DependentLookupAPI
 * @Test Class Name   : DAMAC_Inventory_DependentLookupAPI_Test
 * @Description       : 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         29/06/2020       Created
****************************************************************************************************/
@RestResource(urlMapping='/queryInventoryDependentLookup')
global class DAMAC_Inventory_DependentLookupAPI{
    
    @HttpGet
    global static void getResults(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        GetResponseWrapper resp = new GetResponseWrapper();
        String jsonBody = '';
        String unitType = req.requestURI.substringAfter('queryInventoryDependentLookup/');
        
        try{        
            if(unitType == ''){
                Map<String, Map<String, Map<String, List<String>>>> cityMap = new Map<String, Map<String, Map<String, List<String>>>>();
                
                List<Inventory__c> inventoryList = [SELECT Id, Name, Property_City__c, district__c, Status__c, View_Type__c,
                                        Marketing_Name_Doc__c, Marketing_Name_Doc__r.Name,
                                        IPMS_Bedrooms__c,No_of_bedrooms__c,Space_Type_Lookup_Code__c, Property_Status__c, Anticipated_Completion_Date__c,
                                        Area_sft__c, Price_Per_Sqft__c
                                FROM Inventory__c 
                                WHERE ((Property_City__c != NULL) OR (View_Type__c != NULL) OR (district__c != NULL))
                                AND Status__c = 'Released' ORDER BY Marketing_Name_Doc__r.Name ASC]; //AND View_Type__c != NULL];
                Map<Id, String> projectNameMap= new Map<Id, String>(); 
                system.debug('inventoryList: ' + inventoryList);                                                             
                for(Inventory__c inv: inventoryList){
                    system.debug('Inv Name: ' + inv.Name);
                    String city = inv.Property_City__c.toUpperCase();
                    Map<String, Map<String, List<String>>> districtMap = new Map<String, Map<String, List<String>>>();
                    if(cityMap.get(city) != null){
                        districtMap = cityMap.get(city);
                    }
                    if(inv.district__c != null && inv.district__c != ''){
                        String district = inv.district__c.toUpperCase();
                        Map<String, List<String>> projectMap = new Map<String, List<String>>();
                        if(districtMap.get(district) != null){
                            projectMap = districtMap.get(district);
                        }
                        if(inv.Marketing_Name_Doc__c != null && inv.Marketing_Name_Doc__r.Name != ''){
                            String projectName = inv.Marketing_Name_Doc__r.Name.toUpperCase();
                            //String projectId = inv.Marketing_Name_Doc__c;
                            projectNameMap.put(inv.Marketing_Name_Doc__c,inv.Marketing_Name_Doc__r.Name);
                            String viewType = '';
                            List<String> viewTypeList = new List<String>();
                            if(projectMap.get(projectName) != null){
                                viewTypeList = projectMap.get(projectName);
                            }
                            if(inv.View_Type__c != null && inv.View_Type__c != ''){
                                viewType = inv.View_Type__c.toUpperCase();
                                system.debug('viewType: ' + viewType);
                                if(!viewTypeList.contains(viewType)){
                                    viewTypeList.add(viewType);
                                }
                            }
                            projectMap.put(projectName, viewTypeList);
                        }
                        districtMap.put(district, projectMap);
                        system.debug('districtMap: ' + districtMap);
                    }
                    cityMap.put(city, districtMap);
                    system.debug('cityMap: ' + cityMap);
                }
                system.debug('cityMap: ' + cityMap);
                

                String cityJsonBody ='';
                Map<String, Map<String, List<String>>> districtMap = new Map<String, Map<String, List<String>>>();
                Map<String, List<String>> projectMap = new Map<String, List<String>>();
                List<String> viewTypeList = new List<String>();
                jsonBody = '{"Data": {"City": [';               
                for(String city : cityMap.keySet()){
                    districtMap = cityMap.get(city);
                    if(cityJsonBody != ''){
                        cityJsonBody += ', ';
                    }
                    cityJsonBody += ' {"city_Value": "' + city + '", "District": [';
                    String distJsonBody='';
                    for(String district : districtMap.keySet()){
                        projectMap = districtMap.get(district);
                        if(distJsonBody != ''){
                            distJsonBody += ', ';
                        }
                        distJsonBody += ' {"district_Value": "' + district + '", "Project": [';
                        String projectJsonBody='';
                        for(String project : projectMap.keySet()){
                            for(String projectId : projectNameMap.keySet()){
                                if(projectNameMap.get(projectId) == project ){
                                    //String projectId = projectNameMap
                                    viewTypeList = projectMap.get(project);
                                    if(projectJsonBody != ''){
                                        projectJsonBody += ', ';
                                    }
                                    projectJsonBody += ' {"project_Value": "' + project + '", "project_Id": "' + projectId + '", "View_Type": [';
                                    String viewTypeJsonBody='';
                                    for(String viewType : viewTypeList){
                                        if(viewTypeJsonBody != ''){
                                            viewTypeJsonBody += ', ';
                                        }
                                        viewTypeJsonBody += '{'+ '"Label":' +'"' + viewType + '",' + '"Value":' +'"' + viewType + '"' + '}';
                                    }
                                    projectJsonBody += viewTypeJsonBody + ']}' ;
                                }
                            }
                        }
                        distJsonBody += projectJsonBody + ']}'; 
                    }
                    cityJsonBody += distJsonBody + ']}'; 
                }
                jsonBody += cityJsonBody + '],'; 
                  
                //json body for bedrooms, project status, anticipated Completion Date, Area and Price
                /***************************************************/
                
                Map <String, List<Inventory_App_Layouts__c>> layoutsMap = new Map<String, List<Inventory_App_Layouts__c>>();
                 
                for (Inventory_App_Layouts__c layout:[SELECT Order__c, Search_Field__c, 
                                                    Search_Label__c, Search_Section__c,
                                                    Value_Type__c 
                                                    FROM Inventory_App_Layouts__c 
                                                    WHERE Active__c = TRUE]){
                    if(layoutsMap.containsKey(layout.Search_Section__c))
                        layoutsMap.get(layout.Search_Section__c).add(layout);
                    else
                        layoutsMap.put(layout.Search_Section__c, new List<Inventory_App_Layouts__c>{layout});
                }
                list<inventory__c> invS = new list<inventory__c>();
                
                invS  = [SELECT Floor_Package_Name__c,Selling_Price_excl_VAT__c,
                    Unit_Plan_SF_Link__c,Floor_Plan_SF_Link__c, Plot_Plan_SF_Link__c,
                    Property_city__c, Bedroom_Type__c,IPMS_Bedrooms__c, No_of_bedrooms__c,
                    Space_Type_Lookup_Code__c,Property_Status__c,
                    Unit_Type__c,Unit__c, Property_name__c, Property__c,
                    Anticipated_Completion_Date__c,View_Type__c,
                    District__c,Marketing_Name_Doc__r.Name, Unit_Categorization__c,
                    Marketing_Name_Doc__c 
                    FROM Inventory__c 
                    WHERE status__c='Released' 
                    AND Is_Assigned__c = FALSE AND Space_type_lookup_code__c != NULL and Unit_Categorization__c != null
                    Order BY Unit_Released_Date_from_IPMS__c DESC, Marketing_Name__c, 
                    Unit_Name__c, District__c, 
                    Property_City__c, Price__c, Area_sft__c DESC
                    LIMIT 40000 ];
                    
                Map<String,id> productName_idmap = new Map<String,Id>();
                Map <string, ID> propertyNames = new Map<string, ID>();
                Set<string> propStatuses = new Set<string>();
                Set<string> unitTypes = new Set<string>();
                Set<string> unitss = new Set<string>();
                Set<string> acd = new Set<string>();
                Set<string> bedrooms = new Set<string>();
                Set<string> viewTypes = new Set<string>();
                Set<string> pkgNames = new Set<string>();
                Set<string> prjctFtrs = new Set<string>();
                Set<string> district = new Set<String>();
                Set<string> cities = new Set<String>();
                Set<string> categories = new Set<String>();//new
                Map<string, set<string>> mpCategorization = new map<string, set<string>>();
                if(invS.size() > 0){
                    for (Inventory__c inv:invs){
                        
                        if(inv.Property_Status__c != null){
                            propStatuses.add(inv.Property_Status__c.toUpperCase());
                        }
                        
                        if(!mpCategorization.containsKey(inv.Unit_Categorization__c))
                            mpCategorization.put(inv.Unit_Categorization__c, new Set<string>{inv.Space_Type_Lookup_Code__c});
                        else{
                            set<string> existing = mpCategorization.get(inv.Unit_Categorization__c);
                            existing.add(inv.Space_Type_Lookup_Code__c);
                            mpCategorization.put(inv.Unit_Categorization__c, existing);
                        }

                        
                        if(inv.Anticipated_Completion_Date__c !=null){
                            acd.add(inv.Anticipated_Completion_Date__c.toUpperCase());
                        }
                        if(inv.Space_type_lookup_code__c !=null){
                            categories.add(inv.Space_type_lookup_code__c.toUpperCase());
                        }
                        
                       /* if(inv.No_of_bedrooms__c!=null){
                            //bedrooms.add(inv.Bedroom_Type__c.toUpperCase() );
                            //bedrooms.add(inv.IPMS_Bedrooms__c.toUpperCase() );
                            bedrooms.add(inv.No_of_bedrooms__c.toUpperCase() );
                        }  */
                       
                    }
                }
               // bedrooms.add(inv.No_of_bedrooms__c.toUpperCase() );
                for(String bed : System.Label.Damac_Agent_Bedroom_Type.split(',')){
                    bed = bed.toUpperCase();
                    bedrooms.add(bed);
                }
                
                
                List<String> productnamelst = new List<String>();
                for (String productname : productname_idmap.keyset()){
                    productnamelst.add(productname);
                }
                productnamelst.sort();
        
                List<String> propertynamessort = new List<String>();
                
                for (String productname : propertyNames.keyset()){
                    propertynamessort.add(productName);
                }
                propertynamessort.sort();
        
                List<String> unittypesort = new List<String>();
                unittypesort.addall(unitTypes);
                unittypesort.sort(); 
        
                List<String> bedroomsort = new List<String>();
                bedroomsort.addall(bedrooms);
               // bedroomsort.sort();
        
                List<String> viewtypesort = new List<String>();
                viewtypesort.addall(viewTypes);
                viewtypesort.sort();
        
                List<String> districtlst = new List<String>();
                districtlst.addAll(district);
                districtlst.sort();
        
                List<String> pkgNamesLst = new List<String>();
                pkgNamesLst.addAll(pkgNames);
                pkgNamesLst.sort();
        
                List<String> acdlst = new List<String>();
                acdlst.addAll(acd);
                acdlst.sort();
        
                List<String> unitlst = new List<String>();
                unitlst.addAll(unitss);
                unitlst.sort();
        
                List<String> propStatuseslst = new List<String>();
                propStatuseslst.addAll(propStatuses);
                propStatuseslst.sort();
                //category
                List<String> categorieslst = new List<String>();
                categorieslst.addAll(categories);
                categorieslst.sort();
                
                List<String> citiesLst = new List<String>();
                citiesLst.addAll(cities);
                citiesLst.sort();
                
                System.Debug(districtLst);
                
                String jsonString = '';
                
                //Property Type
                String propertyTypeJsonBody = '';
                jsonString += '"Property_Type":[';
                propertyTypeJsonBody += '{"Label" : '+ '"Villas"'+','
                                            + '"Value" : '+ '"villa",';

                list<string> categories1 = new list<string>();
                if(mpCategorization.containsKey('Villa')){
                    set<string> VillaCategories = mpCategorization.get('Villa');
                    categories1.addAll(villaCategories);
                }
                propertyTypeJsonBody += '"Categories" : [';
                for(string category: categories1){
                    propertyTypeJsonBody += '{';
                    propertyTypeJsonBody +=  '"Label" : "'+ category +'",'
                                        + '"Value" : "'+ category +'"' + '},';
                }
                propertyTypeJsonBody = propertyTypeJsonBody.removeEnd(',');
                jsonString += propertyTypeJsonBody + ']},';
                propertyTypeJsonBody = '';
                propertyTypeJsonBody += '{"Label" : '+ '"Apartments"'+','
                                            + '"Value" : '+ '"apartment",';

                list<string> categories2 = new list<string>();
                if(mpCategorization.containsKey('Apartment')){
                    set<string> apartmentCategories = mpCategorization.get('Apartment');
                    categories2.addAll(apartmentCategories);
                }
                string apartmentCategoryString = (categories2.isEmpty()? '' : string.join(categories2,','));
                propertyTypeJsonBody += '"Categories" : [';
                for(string category: categories2){
                    propertyTypeJsonBody += '{';
                    propertyTypeJsonBody +=  '"Label" : "'+ category +'",'
                                        + '"Value" : "'+ category +'"' + '},';
                }
                propertyTypeJsonBody = propertyTypeJsonBody.removeEnd(',');
                jsonString += propertyTypeJsonBody + ']}],';
                
                
                //Category
                String categoryJsonBody = '';
                categoryJsonBody += '"Category":[';
                for(String categorybody : categorieslst){
                    categoryJsonBody += '{';
                    categoryJsonBody +=  '"Label" : "'+ categorybody  +'",'
                                        + '"Value" : "'+ categorybody +'"' + '},';   
                }
                categoryJsonBody = categoryJsonBody.removeEnd(',');
                jsonString += categoryJsonBody + '],';
                    
                
                //Bedroom
                String bedroomJsonBody = '';
                bedroomJsonBody += '"Bedroom_Type":[';
                for(String bedroombody : bedroomsort ){
                    bedroomJsonBody += '{';
                    bedroomJsonBody +=  '"Label" : "'+ bedroombody +'",'
                                        + '"Value" : "'+ bedroombody +'"' + '},';   
                
                }
                bedroomJsonBody = bedroomJsonBody.removeEnd(',');
                jsonString += bedroomJsonBody + '],'; 
              
                //Property Status
                String propertyStatusJsonBody = '';
                propertyStatusJsonBody += '"Project_Status":[';
                for(String propertyStatusBody : propStatuseslst){
                    propertyStatusJsonBody += '{';
                    propertyStatusJsonBody +=  '"Label" : "'+ propertyStatusBody +'",'
                                               + '"Value" : "'+ propertyStatusBody +'"' + '},';   
                
                }
                propertyStatusJsonBody = propertyStatusJsonBody.removeEnd(',');
                jsonString += propertyStatusJsonBody + '],';
                
                
                //Anticipated Completion Date
                String acdJsonBody = '';
                acdJsonBody += '"Anticipated_Completion_Date":[';
                for(String acdBody : acdlst){
                    acdJsonBody += '{';
                    acdJsonBody +=  '"Label" : "'+ acdBody +'",'
                                    + '"Value" : "'+ acdBody +'"' + '},';   
                
                }
                acdJsonBody = acdJsonBody.removeEnd(',');
                jsonString += acdJsonBody + '],';
                
                //Special Price
                String priceJsonBody = '';
                priceJsonBody += '"Price":';
                
                priceJsonBody +=    '{"min_value" : "0",'                                                                        
                                    + '"max_value" :' +'"20806000"' + '},';
                                    
                //Area
                String areaJsonBody = '';
                areaJsonBody += '"Area":';
                
                areaJsonBody +=    '{"min_value" : "100",'                                                                        
                                    + '"max_value" :' +'"12000"' + '}';
                                                       
               
               
               
               
                                                                
                
                jsonString += priceJsonBody + areaJsonBody ;
                //jsonString = jsonString.removeEnd(',');
                jsonString += '}';
                System.Debug(jsonString);
                jsonBody += jsonString + '}';

                
                /***************************************************/
                System.debug('jsonBody >>'+jsonBody );              
                
            }//end of URL check
            
            //Added by Charan - DAMAC
            else{
                
            }

        }//end of try block
        catch(Exception e){
             system.debug('exception');
             jsonBody = e.getMessage();
             jsonBody = jSON.serialize(jsonBody);   
        }
        
        RestResponse response = RestContext.response;
        response.statusCode = 200;
        response.addHeader('Content-Type', 'application/json');
        response.responseBody = Blob.valueOf(jsonBody);  
      //  return resp;
    }
    
    global class GetResponseWrapper{
        public Map<String, Map<String, Map<String, List<String>>>> cityMap = new Map<String, Map<String, Map<String, List<String>>>>();
    }

    global class AccessData{
        global string label;
        global string value;
        global AccessData(string l, string v){
            label = l;
            value = v;
        }
    }
}