/*
* Description - Class used for sending the eligible RP Termination case for approval.
*
* Version            Date            	Author            Description
* 1.0              06/02/2018        Ashish Agarwal      Initial Draft
*/

public class ApprovalProcessForRentalPoolCases { 
	
	public static void submitForApproval( list<Case> lstApprovalPendingCases ){
		for( Case caseObj : lstApprovalPendingCases ){
			 Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
	    	 req.setComments('Submitting request for approval');
	    	 req.setProcessDefinitionNameOrId('Rental_Pool_Termination_Process');
	    	 req.setObjectId(caseObj.Id);
	    	 req.setNextApproverIds(new Id[]{caseObj.Manager_Id__c});
	    	 Approval.ProcessResult result = Approval.process(req);
		}
	}
	
	@InvocableMethod 
	public static void submitCasesForApproval( list<Task> lstUpdatedTasks ) {
		set<Id> setApprovalPendingCasesIds = new set<Id>();
		
		for( Task objTask : lstUpdatedTasks ) {
			if( objTask.WhatId != null && String.valueOf( objTask.WhatId ).startsWith('500') && String.isNotBlank( objTask.Subject ) &&
                objTask.Subject.equalsIgnoreCase('Collect Rental Pool Agreement(RPA) from customer') &&
                objTask.Assigned_User__c != null && objTask.Assigned_User__c.equalsIgnoreCase('CRE') &&
                ( objTask.Status == 'Closed' || objTask.Status == 'Completed') && !Approval.isLocked( objTask.WhatId ) ) {
				setApprovalPendingCasesIds.add( objTask.WhatId );
			}
		}
		
		if( !setApprovalPendingCasesIds.isEmpty() ) {
			list<Case> lstApprovalPendingCases = [ SELECT Id
													    , Manager_Id__c
													    , Approval_Status__c
													    , Submit_for_Approval__c
													    , Rental_Pool_Termination_Status__c
												     FROM Case
												    WHERE Id IN :setApprovalPendingCasesIds ] ;
			for( Case caseObj : lstApprovalPendingCases ) {
				caseObj.Approval_Status__c = 'Pending';
				caseObj.Submit_for_Approval__c = true ;
				caseObj.Rental_Pool_Termination_Status__c = 'Sent for approval of Manager';
			}
			update lstApprovalPendingCases ;
			submitForApproval(lstApprovalPendingCases);
		}
	}
}