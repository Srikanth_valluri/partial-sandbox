/**
 * @File Name          : SendWorkPermitNotification.cls
 * @Description        : Called by "CreateTaskFMAdmin v1.0" process Builder
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 10/1/2019, 3:55:07 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    9/30/2019   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public class SendWorkPermitNotification {
    
    private static final Id WORK_PERMIT_RECORD_TYPE_ID = DamacUtility.getRecordTypeId('FM_Case__c', 'Work Permit');
    private static List<EmailMessage> lstEmails;
    // This method is used to send notification to Account Owner and Contractor 
    @InvocableMethod
    public static void initiateNotification(List<ID> fmCaseIds){
        System.debug('fmCaseIds:::::::::'+fmCaseIds);
        // Call method to send an email with all the abve attachment
        if(fmCaseIds != NULL){
            
            // Send fmcase Id list to send an email
            sendNotificationToContractorAndAccountOwner(fmCaseIds);
        }
    }
    
    // Used to send Notification to Contractor and Account Owner
    @future(callout=true)
    public static void sendNotificationToContractorAndAccountOwner(List<Id>caseIdList){
        
        //  Variable for Sendgrid
        String toAddress = '';
        String strCCAddress = '';
        String bccAddress = '';
        String contentValue = '';
        String fromAddress = Label.Hello_Damac_Email;
        FM_Case__c workPermitFmCase = new FM_Case__c();
        lstEmails = new List<EmailMessage>();
        // Fetch FM case
        if(caseIdList != NULL && !caseIdList.isEmpty()){
           workPermitFmCase = getFmCases(caseIdList);
        }
        System.debug('workPermitFmCase::::::::'+workPermitFmCase);
        // Send Email to Owner and Tenant Both
        if( String.isNotBlank(workPermitFmCase.Email_2__c) && 
            String.isNotBlank(workPermitFmCase.Account_Email__c)) {
            
            // Account Owner Email 
            toAddress = workPermitFmCase.Account_Email__c;

            // Contractor Email Id
            strCCAddress = workPermitFmCase.Email_2__c;
        }
        
        // form the body to send an email
        contentValue = 'Dear '+workPermitFmCase.Account__r.Name +',<br/><br/>'+
                       'Your request for Work Permit with reference number:'+workPermitFmCase.Name+','+
                       'has been approved.<br/><br/>'+
                       'Below are the details.<br/>'+
                       'Unit Name: '+workPermitFmCase.Unit_Name__c+'<br/>'+
                       'Resident:'+ workPermitFmCase.Account__r.Name +'<br/>'+
                       'Work Permit Type:'+workPermitFmCase.Work_Permit_Type__c+'<br/>'+
                       'Description:' +workPermitFmCase.Description__c+'<br/>'+
                       'Location of Work:'+workPermitFmCase.Location_of_Work__c+'<br/>'+
                       'Contractor Type:'+workPermitFmCase.Contractor_Type__c+'<br/>';
                       

        String strHTMLBody = '<br/>Please Find Employee Information Below.';
                strHTMLBody += ':<br/><br/><html><body>';
                strHTMLBody += '<table style="border:1px solid black; text-align:left; border-collapse:collapse;">'+
                  +'<tr><th style="border:1px solid black; text-align:center; border-collapse:collapse;">Sr.No</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:left;">Employee Name</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Emirates ID #</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Unit Name</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Company Name</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Contractor Mobile</th>'
                  + '<th style="border:1px solid black;border-collapse:collapse; text-align:center;">Employee Mobile Number</th></tr>';
                Integer serialNumber=1;

                // Iterate on Contractors of Fm case
                for( Contractor_Information__c empObj : workPermitFmCase.Contractor_Informations__r ) {
                      System.debug('workPermitFmCase:-------'+workPermitFmCase);
                      strHTMLBody += '<tr>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+serialNumber+'</td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;"><a href=\"'+System.URL.getSalesforceBaseUrl().toExternalForm() + '/'+empObj.Id+'\" target=\"_blank\">'+ empObj.Employee_Name__c+'</a></td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+empObj.Emirates_ID__c+'</td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+empObj.Unit_Name__c+'</td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+empObj.Company_Name__c+'</td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+empObj.Contractor_Mobile__c+'</td>';
                      strHTMLBody += '<td style="border:1px solid black;border-collapse:collapse;text-align:center;">'+empObj.Employee_Mobile_Number__c+'</td>';
                      strHTMLBody += '</tr>';
                      serialNumber++;
                }
                strHTMLBody += '</table></body></html><br/><br/>';
                contentValue +=strHTMLBody;
                System.debug('contentValue::::::::::'+contentValue);
                // Callout to sendgrid to send an email  
                SendGridEmailService.SendGridResponse objSendGridResponse =  
                    SendGridEmailService.sendEmailService(
                    toAddress,
                    '', 
                    strCCAddress, 
                    '', 
                    bccAddress, 
                    '', 
                    Label.Subject_for_Notification_Work_Permit_Request_Approved,
                    '',
                    fromAddress,
                    '',
                    Label.Service_Charges_replyTo_Address,
                    '',
                    'text/html',
                    contentValue,
                    '',
                    new list<Attachment> ()
                );
                system.debug('toAddress === ' + toAddress);
                system.debug('contentValue === ' + contentValue);
               // system.debug('finalAttachmentList === ' + finalAttachmentList);
                
                System.debug('objSendGridResponse:::::::::::'+objSendGridResponse);
                if (objSendGridResponse.ResponseStatus == 'Accepted') {
                EmailMessage mail = new EmailMessage();
                mail.Subject = Label.Subject_for_Notification_Work_Permit_Request_Approved;
                mail.MessageDate = System.Today();
                mail.Status = '3';//'Sent';
                mail.relatedToId = workPermitFmCase.Id;
                mail.Account__c  = workPermitFmCase.Account__c;
               // mail.Type__c = 'Work PErmit';
                mail.ToAddress = toAddress;
                mail.FromAddress = fromAddress;
                mail.TextBody = contentValue;//contentValue.replaceAll('\\<.*?\\>', '');
                mail.Sent_By_Sendgrid__c = true;
                mail.SentGrid_MessageId__c = objSendGridResponse.messageId;
                mail.Booking_Unit__c = workPermitFmCase.Booking_Unit__c;
                mail.CcAddress = strCCAddress;
                mail.BccAddress = bccAddress;
                lstEmails.add(mail);
                system.debug('Mail obj == ' + mail);
            }
            System.debug('lstEmails=== ' + lstEmails);
            if(lstEmails.size() > 0) {
                if(!Test.isRunningTest()) {
                    insert lstEmails;
                }
            }
        }

// Fetch fm case
private static FM_Case__c getFmCases(List<Id> caseIdList){
    List<FM_Case__c> fmViolationCaseRecord = new List<FM_Case__c>();

    // Fetch Fm  vioalition case
    fmViolationCaseRecord = [
        SELECT Id,
                Name,
                Notice_Type__c,
                Account__c,
                Account__r.Name,
                RecordTypeId,
                Tenant__c,
                Tenant_Email__c,
                Booking_Unit__c,
                Email__c,
                Email_2__c,
                Unit_Name__c,
                Work_Permit_Type__c,
                Approval_Status__c,
                Booking_Unit__r.Name,
                Tenant__r.Party_ID__c,
                Tenant__r.Mobile_Phone_Encrypt__pc,
                Account__r.Mobile_Phone_Encrypt__pc, 
                Property_Manager_Name__c,
                Property_Manager_Number__c,
                Booking_Unit__r.Unit_Name__c,
                Account_Email__c,
                Description__c,
                Location_of_Work__c,
                Building_Email__c,
                Contractor_Type__c,
                (
                    SELECT Id,
                        Name,
                        FM_Case__c,
                        Employee_Name__c,
                        Emirates_ID__c,
                        Company_Name__c,
                        Employee_Mobile_Number__c,
                        Unit_Name__c,
                        Contractor_Mobile__c
                    FROM Contractor_Informations__r
                    WHERE FM_Case__c IN : caseIdList
                )
            FROM FM_Case__c 
            WHERE RecordTypeId = :WORK_PERMIT_RECORD_TYPE_ID
            AND Id IN :caseIdList
        ];
        return fmViolationCaseRecord[0];
    }
}