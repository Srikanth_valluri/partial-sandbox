public without sharing class CaseTriggerHanderReAssignOwner{
    public void afterUpdate(map<Id,Case> newMap, map<Id,Case> oldMap){
        map<Id,Id> mapCaseId_OwnerId = new map<Id,Id>();
        for(Case objCase : newMap.values()){
            // Check if old owner of Case is Queue
            system.debug('New Owner*****'+objCase.OwnerId);
            system.debug('Old Owner*****'+oldMap.get(objCase.Id).OwnerId);
            if(objCase.OwnerId != oldMap.get(objCase.Id).OwnerId
            && string.valueOf(objCase.OwnerId).startsWith('005')
            && string.valueOf(oldMap.get(objCase.Id).OwnerId).startsWith('00G')){
                mapCaseId_OwnerId.put(objCase.Id,objCase.OwnerId);
            }
            system.debug('*****mapCaseId_OwnerId*****'+mapCaseId_OwnerId);
        }
        if(!mapCaseId_OwnerId.isEmpty()){
            list<Task> lstTaskToReassign = new list<Task>();
            // Find all Tasks related to the Case
            for(Task objTask : [Select Id, OwnerId, WhatId, IsClosed
                               from Task
                               where IsClosed = false
                               and WhatId IN : mapCaseId_OwnerId.keySet()
                               and OwnerId =: Label.DefaultCaseOwnerId]){
                objTask.OwnerId = mapCaseId_OwnerId.get(objTask.WhatId);
                lstTaskToReassign.add(objTask);
            }
            if(!lstTaskToReassign.isEmpty()){
                update lstTaskToReassign;
            }
        }
    }
}