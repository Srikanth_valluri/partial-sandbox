/************************************************************************************************
 * @Name              : DAMAC_OutlookEvents_JSON
 * @Test Class Name   : DAMAC_MSTeamsIntegration_Test
 * @Description       : JSON Class for Event Creation response from Microsoft Graph API 
 * Modification Log
 * VERSION     AUTHOR          DATE            Update Log     
 * 1.0         QBurst         26/03/2020       Created
***********************************************************************************************/
public class DAMAC_OutlookEvents_JSON{
    public String Id; //e6f12d9e-c55d-4fb1-b6d2-030cb86c9f43_19:meeting_OThhMzY0MmQtYzI5Mi00YmFkLTkwNjctZTM5YWJlNDEwYWI1@thread.v2
    public String subject; //Orientation
    public String bodyPreview; // Dana, this is the time you selected for our orientation.
    public Boolean isCancelled; 
    public String onlineMeetingUrl; 
    public String originalStart; 
    public String webLink; 
    public body_cls body;
    public end_cls end_x;
    public start_cls start;
    public list<attendees_cls> attendees;
    public attendees_cls organizer;

    public class body_cls{
        public string contentType;
        public string content;
    }

    public class start_cls{
        public string dateTime_x;
        public string timeZone;
    }

     public class end_cls{
        public string dateTime_x;
        public string timeZone;
    }

    public class attendees_cls{
        public string type;
        public status_cls status;
        public emailAddress_cls emailAddress;
    }

    public class status_cls{
        public string response;
        public string time_x;
    }

    public class emailAddress_cls{
        public string name_x;
        public string address;
    }

    public static DAMAC_OutlookEvents_JSON parse(String json){
        return (DAMAC_OutlookEvents_JSON) System.JSON.deserialize(json, DAMAC_OutlookEvents_JSON.class);
    }
}