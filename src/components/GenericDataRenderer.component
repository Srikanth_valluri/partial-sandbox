<apex:component id="genericDataRendererId"
                controller="GenericDataRendererController"
                allowDML="false"
                selfClosing="true"
                layout="block">

    <apex:attribute name="configDocumentName"
                    description="Document name containing config JSON"
                    type="String"
                    required="true" />

    <apex:attribute name="recordId"
                    description="Record id for detail view"
                    type="String"
                    required="false" />


    <apex:attribute name="filter"
                    description="Filter clause for the query"
                    type="String"
                    required="false" />

    <apex:attribute name="recordLimit"
                    description="Limit on number of records"
                    type="Integer"
                    required="false" />

    <apex:attribute name="paging"
                    description="Sets pagination for the table"
                    type="Boolean"
                    default="true" />

    <apex:attribute name="searching"
                    description="Sets search feature for the table"
                    type="Boolean"
                    default="true" />

    <apex:attribute name="callbackNamespace"
                    description="namespace variable name for the callback function"
                    type="String"
                    default="" />

    <apex:attribute name="callback"
                    description="callback function to call after render"
                    type="String"
                    default="" />

    <apex:attribute name="skipJQuery"
                    description="Component will not load JQuery"
                    type="Boolean"
                    required="false"
                    default="true"
                    />

    <apex:attribute name="skipUnderscore"
                    description="Component will not load Underscore"
                    type="Boolean"
                    required="false"
                    default="true"
                    />

    <apex:attribute name="skipBootstrap"
                    description="Component will not load Bootstrap"
                    type="Boolean"
                    required="false"
                    default="true"
                    />

    <apex:attribute name="skipBootstrapTable"
                    description="Component will not load Bootstrap"
                    type="Boolean"
                    required="false"
                    default="true"
                    />
    <apex:outputPanel rendered="{! NOT(skipJQuery) }">
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js" />
    </apex:outputPanel>

     <apex:outputPanel rendered="{! NOT(skipUnderscore) }">
        <script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js" />
    </apex:outputPanel>


    <apex:outputPanel rendered="{! NOT(skipBootstrap) }">
        <link rel="stylesheet"
            href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        </link>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js">
        </script>
    </apex:outputPanel>


<apex:outputPanel rendered="{!NOT(skipBootstrapTable)}">
    <link rel="stylesheet"
          href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css"></link>
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.css">
</link>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.js"></script>
</apex:outputPanel>


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.16/b-1.4.2/b-print-1.4.2/r-2.2.0/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.16/b-1.4.2/b-print-1.4.2/r-2.2.0/datatables.min.js">
</script>

<div id="{!configDocumentName}" class="table-responsive">

</div>

<style type="text/css">



    table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>th:first-child:before {
        box-shadow: none;
        border: 0px;
        top: 7px;
        left: 6px;
        font-weight: 600;
        line-height: 15px;
        height: 15px;
        width: 15px;
        font-family: 'Poppins', sans-serif !important;
    }


    table.dataTable.dtr-inline.collapsed>tbody>tr.parent>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr.parent>th:first-child:before {
        background-color: #337ab7 ;
    }

    table.dataTable>tbody>tr.child ul.dtr-details {
        width: 100%;
    }

    table.dataTable>tbody>tr.child span.dtr-title {
        margin-right: 10px;
        width: 150px;
    }


    table.dataTable>tbody>tr.child span.dtr-data {
        text-align: left;
    }



    table.dataTable {
        border: 0px;
        width: 100%!important;
    }

    table.dataTable tr th {
        border-right: 0px;
        border-top: 1px solid #ddd;
    }

    table.dataTable tr td {
        border-right: 0px;
        border-top: 0px;
    }

    table.dataTable {
        font-size: 13px;
    }

    table.dataTable tr th {
        padding-right: 10px;
    }

    table.dataTable tr th.sorting:after {
        opacity: 0.5;
        font-size: 10px;
    }

    table.dataTable tr th.sorting_asc:after {
        opacity: 0.5;
    }

    .pagination>li.paginate_button a {
        border: 1px solid #dde2e4;
        background-color: #FAFBFC;
    }

    .pagination>li.paginate_button.active a {
        background-color: #2dadfa;
        border-color: #2dadfa;
    }

    table.dataTable li span {
        word-wrap: break-word;
        white-space: normal;
    }
</style>

<script>

    if (!window.genericRenderer) {
        window.genericRenderer = {};
    }

    // jQuery(document).ready(function($) {
        (function(configJSONFileName, recordId, filter, callbackFnName, callbackNamespace) {
            // console.log('configJSONFileName', configJSONFileName);
            // if (ccUtils && ccUtils.eventBus && typeof ccUtils.eventBus.subscribe === 'function') {
                // ccUtils.eventBus.subscribe('reRender', function() {

            // remote         =  ;
            loadDataAction =  "{!$RemoteAction.GenericDataRendererController.getData}";
            remoteConfig   =   {};
            tableTemplate  =   _.template(
                $( "script.table-template" ).html()
            );


            // tableWrapperElement =$("#"+configJSONFileName);

            initializeRenderer = function(){
                window.genericRenderer[configJSONFileName] = {};
                window.genericRenderer[configJSONFileName].callback = callbackFnName;
                window.genericRenderer[configJSONFileName].callbackNamespace = callbackNamespace;
                rendererConfig = {};
                rendererConfig.configJSON   = configJSONFileName;
                rendererConfig.recordId     = recordId || '';
                rendererConfig.filter       = filter || '';
                rendererConfig.recordLimit  = parseInt({!IF(recordLimit != NULL, recordLimit, null)});
                loadData(rendererConfig);


            };

            loadData = function(rendererConfig){
                Visualforce.remoting.Manager.invokeAction(  loadDataAction
                                                          , rendererConfig
                                                          , renderData
                                                          , remoteConfig);

            };

            renderData = function(result,event){
                if (event.status) {
                    render(result);
                } else if (event.type === 'exception') {
                    //errors in event.message
                } else {
                    //errors in event.message
                }
            };

            render = function(result) {
                // console.log('result', result);
                result.detailFieldList = result.detailFieldList || [];
                configJsonName = result.configJSON || configJSONFileName;
                tableWrapperElement = $("#"+(configJsonName));
                tableWrapperElement.html(
                    tableTemplate( result )
                );
                if (  result.displayAs == 'recordDetail' ) {
                    var tableDisplayConfig = {};
                    tableDisplayConfig.columns = result.fieldList;
                    tableDisplayConfig.data    = result.dataList;
                    var wrappedTable = tableWrapperElement.find('table');
                    $('#' + configJSONFileName).removeClass('table-responsive');
                    // if (wrappedTable) {
                    //     wrappedTable.bootstrapTable(tableDisplayConfig);
                    // }
                } else if ( result.displayAs == 'table'  ) {
                    result.$table = tableWrapperElement.find("table").DataTable({
                        responsive: true,
                        "language": {
                            "emptyTable": "No records to display"
                        },
                        aaSorting: [],
                        order: [],
                        searching: {!searching},
                        paging: {!paging},
                        info: {!paging}
                    });
                }
                // debugger;

                if (window.genericRenderer[configJsonName].callback) {
                    if (window.genericRenderer[configJsonName].callbackNamespace) {
                        window[window.genericRenderer[configJsonName].callbackNamespace][window.genericRenderer[configJsonName].callback](result);
                    } else {
                        window[window.genericRenderer[configJsonName].callback](result);
                    }
                }
            };

            // $(document).ready(function(){
            //     initializeRenderer();
            // });
            // var rendererConfig = {};
            // rendererConfig.configJSON = configJSONFileName;
            // rendererConfig.recordId   = recordId || '';
            // rendererConfig.filter   = filter || '';
            // loadData(rendererConfig);



            // if (window.ccUtils && window.ccUtils.eventBus && typeof window.ccUtils.eventBus.subscribe === 'function') {
                // ccUtils.eventBus.subscribe('reRender', initializeRenderer);
            // } else {
                // $(document).ready(function(){
                    initializeRenderer();
                // });
            // }

        }(  "{!configDocumentName}"
          , "{!recordId}"
          , "{!filter}"
          , "{!callback}"
          , "{!callbackNamespace}"));
    // });

</script>



</apex:component>